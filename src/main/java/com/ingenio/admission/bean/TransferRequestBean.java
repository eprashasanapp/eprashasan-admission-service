package com.ingenio.admission.bean;

import java.util.List;

public class TransferRequestBean {


	private List<TransferBean> transfers;	

	public List<TransferBean> getTransfers() {
		return transfers;
	}
	public void setTransfers(List<TransferBean> transfers) {
		this.transfers = transfers;
	}
}
