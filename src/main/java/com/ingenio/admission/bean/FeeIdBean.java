package com.ingenio.admission.bean;

public class FeeIdBean {
	
	private Integer FeeId;
	private Integer Schoolid;
	private Integer headId;
	private Integer subheadId;
	private Double assignFee;
	private Double fine;
	private String dueDate;
	private Integer fineIncrement;
	
	public FeeIdBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public FeeIdBean(Integer headId, Integer subheadId, Double assignFee, Double fine,String dueDate,Integer fineIncrement) {
		super();
		this.headId = headId;
		this.subheadId = subheadId;
		this.assignFee = assignFee;
		this.fine = fine;
		this.dueDate=dueDate;
		this.fineIncrement=fineIncrement;
	}


	public Integer getFeeId() {
		return FeeId;
	}
	public void setFeeId(Integer feeId) {
		FeeId = feeId;
	}
	public Integer getSchoolid() {
		return Schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		Schoolid = schoolid;
	}
	public FeeIdBean(Integer feeId, Integer schoolid) {
		super();
		FeeId = feeId;
		Schoolid = schoolid;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public Integer getSubheadId() {
		return subheadId;
	}

	public void setSubheadId(Integer subheadId) {
		this.subheadId = subheadId;
	}

	public Double getAssignFee() {
		return assignFee;
	}

	public void setAssignFee(Double assignFee) {
		this.assignFee = assignFee;
	}

	public Double getFine() {
		return fine;
	}

	public void setFine(Double fine) {
		this.fine = fine;
	}


	public String getDueDate() {
		return dueDate;
	}


	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}


	public Integer getFineIncrement() {
		return fineIncrement;
	}


	public void setFineIncrement(Integer fineIncrement) {
		this.fineIncrement = fineIncrement;
	}
	
}
