package com.ingenio.admission.bean;

public class CheckBalanceBean {

	
	private String balance;
	private Integer sendingFlag;
	
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public Integer getSendingFlag() {
		return sendingFlag;
	}
	public void setSendingFlag(Integer sendingFlag) {
		this.sendingFlag = sendingFlag;
	}
	
	
}
