package com.ingenio.admission.bean;

import java.util.List;

public class StudentDetailsBean {

	private String studentName;
	private String studentRegNo;
	private String studentGender;
	private String studentCast;
	private String studentCategory;
	private String studentReligion;
	private Integer studentStatus;
	private String studentConcession;
	private Integer renewStudentId;
	private String studentStdandard;
	private String studentDivision;
	private String studentYear;
	private Integer studentRollNo;
	private Integer studentgrBookId;
	private String studentPhoto;
	private String regNo;
	private String rollNo;
	private String paidAmount;
	private String paidAmountInWords;
	private Integer studentId;
	private Integer standardId;
	private String standardName;
	private Integer divisionId;
	private String divisionName;
	private Integer yearId;
	private String yearName;
	private String MobileNo;
	private String admissionDate;
	private Integer schoolId;
	private String initialName;
	private String firstName;
	private String middleName;
	private String lastName;
	private Integer userId;
	private String collegeLeavingDate;
	private Integer monthId;
	private Integer oldYearId = 0;
	private Integer oldStandardId = 0;
	private String birth_date;
	private String motherName;
	private String renewAdmissionDate;
	private List<StudentDetailsBean> presentStudentList;
	private List<StudentDetailsBean> absentStudentList;
	private List<StudentDetailsBean> monthlyCatelogList;
	private List<StudentDetailsBean> NewStudentCatelogList;
//	new
	private String oldAdmissionDate;
//	new end
//	new 1
//	private List<StudentDetailsBean> studentList;
//	new 1 end
	
	public StudentDetailsBean() {
		super();
	}

	public StudentDetailsBean(String studentName, String studentRegNo, String studentGender, String studetnCast,
			String studentCategory, String studentReligion, Integer studentStatus, String studentConcession,
			Integer renewStudentId, String studentStdandard, String studetnDivision, String studetnYear) {
		super();
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentGender = studentGender;
		this.studentCast = studetnCast;
		this.studentCategory = studentCategory;
		this.studentReligion = studentReligion;
		this.studentStatus = studentStatus;
		this.studentConcession = studentConcession;
		this.renewStudentId = renewStudentId;
		this.studentStdandard = studentStdandard;
		this.studentDivision = studetnDivision;
		this.studentYear = studetnYear;

	}

	public StudentDetailsBean(String studentName, String studentRegNo, String studentGender, String studetnCast,
			Integer studentStatus, Integer studentgrBookId, String studentConcession, Integer renewStudentId,
			String studentStdandard, String studetnDivision, String studetnYear, Integer studentRollNo) {
		super();
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentGender = studentGender;
		this.studentCast = studetnCast;
		this.studentStatus = studentStatus;
		this.studentgrBookId = studentgrBookId;
		this.studentConcession = studentConcession;
		this.renewStudentId = renewStudentId;
		this.studentStdandard = studentStdandard;
		this.studentDivision = studetnDivision;
		this.studentYear = studetnYear;
		this.studentRollNo = studentRollNo;
	}

	public StudentDetailsBean(Integer renewStudentId, Integer standardId, String standardName, Integer divisionId,
			String divisionName, Integer yearId, String yearName) {
		super();
		this.renewStudentId = renewStudentId;
		this.standardId = standardId;
		this.standardName = standardName;
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.yearId = yearId;
		this.yearName = yearName;

	}

	public StudentDetailsBean(Integer studentId, String studentName, String studentRegNo, String studentGender,
			Integer studentRollNo) {
		super();
		this.setStudentId(studentId);
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentGender = studentGender;
		this.studentRollNo = studentRollNo;
	}

	public StudentDetailsBean(Integer studentId, String studentName, String studentRegNo, Integer studentRollNo,
			Integer studentgrBookId) {
		super();
		this.setStudentId(studentId);
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentRollNo = studentRollNo;
		this.studentgrBookId = studentgrBookId;
	}

	public StudentDetailsBean(Integer studentId, String studentName, String studentRegNo, Integer studentRollNo,
			Integer studentgrBookId, String MobileNo) {
		super();
		this.setStudentId(studentId);
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentRollNo = studentRollNo;
		this.studentgrBookId = studentgrBookId;
		this.MobileNo = MobileNo;
	}

	public StudentDetailsBean(Integer studentId, String studentName, String studentRegNo, Integer studentRollNo,
			Integer studentgrBookId, String MobileNo, Integer renewStudentId) {
		super();
		this.setStudentId(studentId);
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentRollNo = studentRollNo;
		this.studentgrBookId = studentgrBookId;
		this.MobileNo = MobileNo;
		this.renewStudentId = renewStudentId;
	}

	public StudentDetailsBean(String studentName) {
		super();
		this.studentName = studentName;
	}

	public StudentDetailsBean(String studentName, String MobileNo) {
		super();
		this.studentName = studentName;
		this.MobileNo = MobileNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentRegNo() {
		return studentRegNo;
	}

	public void setStudentRegNo(String studentRegNo) {
		this.studentRegNo = studentRegNo;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public String getStudetnCast() {
		return studentCast;
	}

	public void setStudetnCast(String studetnCast) {
		this.studentCast = studetnCast;
	}

	public String getStudentCategory() {
		return studentCategory;
	}
	

	public String getRenewAdmissionDate() {
		return renewAdmissionDate;
	}

	public void setRenewAdmissionDate(String renewAdmissionDate) {
		this.renewAdmissionDate = renewAdmissionDate;
	}

	public void setStudentCategory(String studentCategory) {
		this.studentCategory = studentCategory;
	}

	public String getStudentReligion() {
		return studentReligion;
	}

	public void setStudentReligion(String studentReligion) {
		this.studentReligion = studentReligion;
	}

	public Integer getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(Integer studentStatus) {
		this.studentStatus = studentStatus;
	}

	public String getStudentConcession() {
		return studentConcession;
	}

	public void setStudentConcession(String studentConcession) {
		this.studentConcession = studentConcession;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public Integer getRenewStudentId() {
		return renewStudentId;
	}

	public void setRenewStudentId(Integer renewStudentId) {
		this.renewStudentId = renewStudentId;
	}

	public String getStudentStdandard() {
		return studentStdandard;
	}

	public void setStudentStdandard(String studentStdandard) {
		this.studentStdandard = studentStdandard;
	}

	public String getStudetnDivision() {
		return studentDivision;
	}

	public void setStudetnDivision(String studetnDivision) {
		this.studentDivision = studetnDivision;
	}

	public String getStudetnYear() {
		return studentYear;
	}

	public void setStudetnYear(String studetnYear) {
		this.studentYear = studetnYear;
	}

	public Integer getStudentRollNo() {
		return studentRollNo;
	}

	public void setStudentRollNo(Integer studentRollNo) {
		this.studentRollNo = studentRollNo;
	}

	public List<StudentDetailsBean> getPresentStudentList() {
		return presentStudentList;
	}

	public void setPresentStudentList(List<StudentDetailsBean> presentStudentList) {
		this.presentStudentList = presentStudentList;
	}

	public List<StudentDetailsBean> getAbsentStudentList() {
		return absentStudentList;
	}

	public void setAbsentStudentList(List<StudentDetailsBean> absentStudentList) {
		this.absentStudentList = absentStudentList;
	}

	public Integer getStudentgrBookId() {
		return studentgrBookId;
	}

	public void setStudentgrBookId(Integer studentgrBookId) {
		this.studentgrBookId = studentgrBookId;
	}

	public String getStudentPhoto() {
		return studentPhoto;
	}

	public void setStudentPhoto(String studentPhoto) {
		this.studentPhoto = studentPhoto;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaidAmountInWords() {
		return paidAmountInWords;
	}

	public void setPaidAmountInWords(String paidAmountInWords) {
		this.paidAmountInWords = paidAmountInWords;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getStudentCast() {
		return studentCast;
	}

	public void setStudentCast(String studentCast) {
		this.studentCast = studentCast;
	}

	public String getStudentDivision() {
		return studentDivision;
	}

	public void setStudentDivision(String studentDivision) {
		this.studentDivision = studentDivision;
	}

	public String getStudentYear() {
		return studentYear;
	}

	public void setStudentYear(String studentYear) {
		this.studentYear = studentYear;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public StudentDetailsBean(Integer studentId, Integer renewStudentId, String studentName, String studentRegNo,
			String studentGender, Integer studentRollNo, Integer yearId, String yearName, Integer standardId,
			String standardName, Integer divisionId, String divisionName, String mobileNo) {
		super();
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentGender = studentGender;
		this.renewStudentId = renewStudentId;
		this.studentRollNo = studentRollNo;
		this.studentId = studentId;
		this.standardId = standardId;
		this.standardName = standardName;
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.yearId = yearId;
		this.yearName = yearName;
		this.MobileNo = mobileNo;
	}

	public StudentDetailsBean(Integer studentId, Integer renewStudentId, String studentName, String studentRegNo,
			String studentGender, Integer studentRollNo, Integer yearId, String yearName, Integer standardId,
			String standardName, Integer divisionId, String divisionName, String mobileNo, String collegeLeavingDate) {
		super();
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentGender = studentGender;
		this.renewStudentId = renewStudentId;
		this.studentRollNo = studentRollNo;
		this.studentId = studentId;
		this.standardId = standardId;
		this.standardName = standardName;
		this.divisionId = divisionId;
		this.divisionName = divisionName;
		this.yearId = yearId;
		this.yearName = yearName;
		this.MobileNo = mobileNo;
		this.collegeLeavingDate = collegeLeavingDate;
	}

	public List<StudentDetailsBean> getMonthlyCatelogList() {
		return monthlyCatelogList;
	}

	public void setMonthlyCatelogList(List<StudentDetailsBean> monthlyCatelogList) {
		this.monthlyCatelogList = monthlyCatelogList;
	}

	public String getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getInitialName() {
		return initialName;
	}

	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<StudentDetailsBean> getNewStudentCatelogList() {
		return NewStudentCatelogList;
	}

	public void setNewStudentCatelogList(List<StudentDetailsBean> newStudentCatelogList) {
		NewStudentCatelogList = newStudentCatelogList;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCollegeLeavingDate() {
		return collegeLeavingDate;
	}

	public void setCollegeLeavingDate(String collegeLeavingDate) {
		this.collegeLeavingDate = collegeLeavingDate;
	}

	public Integer getMonthId() {
		return monthId;
	}

	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}

	public Integer getOldYearId() {
		return oldYearId;
	}

	public void setOldYearId(Integer oldYearId) {
		this.oldYearId = oldYearId;
	}

	public Integer getOldStandardId() {
		return oldStandardId;
	}

	public void setOldStandardId(Integer oldStandardId) {
		this.oldStandardId = oldStandardId;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getOldAdmissionDate() {
		return oldAdmissionDate;
	}

	public void setOldAdmissionDate(String oldAdmissionDate) {
		this.oldAdmissionDate = oldAdmissionDate;
	}

//	public List<StudentDetailsBean> getStudentList() {
//		return studentList;
//	}
//
//	public void setStudentList(List<StudentDetailsBean> studentList) {
//		this.studentList = studentList;
//	}	
	
}
