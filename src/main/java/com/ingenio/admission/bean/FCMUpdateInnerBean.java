package com.ingenio.admission.bean;

public class FCMUpdateInnerBean {

	private Integer schoolId;
	private Integer userId;

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
