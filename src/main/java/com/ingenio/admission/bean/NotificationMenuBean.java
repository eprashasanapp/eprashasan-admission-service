package com.ingenio.admission.bean;

public class NotificationMenuBean {

	private Integer menuId;
	private Long unseenNotificationCount;
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public Long getUnseenNotificationCount() {
		return unseenNotificationCount;
	}
	public void setUnseenNotificationCount(Long unseenNotificationCount) {
		this.unseenNotificationCount = unseenNotificationCount;
	}
	public NotificationMenuBean(Integer menuId, Long unseenNotificationCount) {
		super();
		this.menuId = menuId;
		this.unseenNotificationCount = unseenNotificationCount;
	}
	
	
}
