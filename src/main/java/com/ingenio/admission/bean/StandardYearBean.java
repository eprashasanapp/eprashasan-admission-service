package com.ingenio.admission.bean;

public class StandardYearBean {
	
	
	private String yearName;
	private Integer yearId;
	private Integer standardId;
	private String standardName;
	private Integer renewId;
	
	
	
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public String getYearName() {
		return yearName;
	}
	public void setYearName(String yearName) {
		this.yearName = yearName;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public StandardYearBean(String yearName, Integer yearId,  String standardName,Integer standardId,Integer renewId) {
		super();
		this.yearName = yearName;
		this.yearId = yearId;
		this.standardId = standardId;
		this.standardName = standardName;
		this.renewId=renewId;
	}
	public StandardYearBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
