package com.ingenio.admission.bean;

import java.util.List;

public class PaymentRequestBean {
	private List<HeadFeeBean> headFeeBeanList;
	private String paidDate;
	private Integer payType;
	private Integer userID;
	private Integer schoolId;
	private String deviceType;
	private String ipAddress;
	private Double totalPaidAmount;
	private Double totalDiscount;
	private String discountPercentage;
	private String transactionId;
	private String paymentStatus;
	private String paymentMessage;
	private Double convenienceFee;
	private Integer paymentGatewayRateId;
	private Double payTypeCharges;
	private Double payTypeGst;
	private Double payTypeRouteCharges;
	private Double payTypeRouteGst;
	private Double epCommission;
	private Double schoolCommission;
	private Double tpCommission; 
	private Double epCommRouteCharges;
	private Double schCommRouteCharges;
	private Double tpCommRouteCharges;
	private Double epGst;
	private Double tpGst;
	private Double schGST;
	private Double epCommRouteGst;
	private Double tpCommRouteGst;
	private Double schCommRouteGst;
	private Integer studFeeId;
	private String orderId;
	private Integer processId;
	private String time;
	private String receiptNo;
	private Integer receiptId;
	private String mobileNo;
	
	
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public Integer getStudFeeId() {
		return studFeeId;
	}
	public void setStudFeeId(Integer studFeeId) {
		this.studFeeId = studFeeId;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public Integer getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}
	public List<HeadFeeBean> getHeadFeeBeanList() {
		return headFeeBeanList;
	}
	public void setHeadFeeBeanList(List<HeadFeeBean> headFeeBeanList) {
		this.headFeeBeanList = headFeeBeanList;
	}
	public String getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}
	public Integer getPayType() {
		return payType;
	}
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Double getTotalPaidAmount() {
		return totalPaidAmount;
	}
	public void setTotalPaidAmount(Double totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public String getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(String discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentMessage() {
		return paymentMessage;
	}
	public void setPaymentMessage(String paymentMessage) {
		this.paymentMessage = paymentMessage;
	}
	public Double getConvenienceFee() {
		return convenienceFee;
	}
	public void setConvenienceFee(Double convenienceFee) {
		this.convenienceFee = convenienceFee;
	}
	public Integer getPaymentGatewayRateId() {
		return paymentGatewayRateId;
	}
	public void setPaymentGatewayRateId(Integer paymentGatewayRateId) {
		this.paymentGatewayRateId = paymentGatewayRateId;
	}
	public Double getPayTypeCharges() {
		return payTypeCharges;
	}
	public void setPayTypeCharges(Double payTypeCharges) {
		this.payTypeCharges = payTypeCharges;
	}
	public Double getPayTypeGst() {
		return payTypeGst;
	}
	public void setPayTypeGst(Double payTypeGst) {
		this.payTypeGst = payTypeGst;
	}
	public Double getPayTypeRouteCharges() {
		return payTypeRouteCharges;
	}
	public void setPayTypeRouteCharges(Double payTypeRouteCharges) {
		this.payTypeRouteCharges = payTypeRouteCharges;
	}
	public Double getPayTypeRouteGst() {
		return payTypeRouteGst;
	}
	public void setPayTypeRouteGst(Double payTypeRouteGst) {
		this.payTypeRouteGst = payTypeRouteGst;
	}
	public Double getEpCommission() {
		return epCommission;
	}
	public void setEpCommission(Double epCommission) {
		this.epCommission = epCommission;
	}
	public Double getTpCommission() {
		return tpCommission;
	}
	public void setTpCommission(Double tpCommission) {
		this.tpCommission = tpCommission;
	}
	public Double getEpCommRouteCharges() {
		return epCommRouteCharges;
	}
	public void setEpCommRouteCharges(Double epCommRouteCharges) {
		this.epCommRouteCharges = epCommRouteCharges;
	}
	public Double getSchCommRouteCharges() {
		return schCommRouteCharges;
	}
	public void setSchCommRouteCharges(Double schCommRouteCharges) {
		this.schCommRouteCharges = schCommRouteCharges;
	}
	public Double getTpCommRouteCharges() {
		return tpCommRouteCharges;
	}
	public void setTpCommRouteCharges(Double tpCommRouteCharges) {
		this.tpCommRouteCharges = tpCommRouteCharges;
	}
	public Double getEpGst() {
		return epGst;
	}
	public void setEpGst(Double epGst) {
		this.epGst = epGst;
	}
	public Double getTpGst() {
		return tpGst;
	}
	public void setTpGst(Double tpGst) {
		this.tpGst = tpGst;
	}
	public Double getSchGST() {
		return schGST;
	}
	public void setSchGST(Double schGST) {
		this.schGST = schGST;
	}
	public Double getEpCommRouteGst() {
		return epCommRouteGst;
	}
	public void setEpCommRouteGst(Double epCommRouteGst) {
		this.epCommRouteGst = epCommRouteGst;
	}
	public Double getTpCommRouteGst() {
		return tpCommRouteGst;
	}
	public void setTpCommRouteGst(Double tpCommRouteGst) {
		this.tpCommRouteGst = tpCommRouteGst;
	}
	public Double getSchCommRouteGst() {
		return schCommRouteGst;
	}
	public void setSchCommRouteGst(Double schCommRouteGst) {
		this.schCommRouteGst = schCommRouteGst;
	}
	public Integer getStudFeeid() {
		return studFeeId;
	}
	public void setStudFeeid(Integer studFeeId) {
		this.studFeeId = studFeeId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Double getSchoolCommission() {
		return schoolCommission;
	}
	public void setSchoolCommission(Double schoolCommission) {
		this.schoolCommission = schoolCommission;
	}
	public Integer getProcessId() {
		return processId;
	}
	public void setProcessId(Integer processId) {
		this.processId = processId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	
}
