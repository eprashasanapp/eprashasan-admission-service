package com.ingenio.admission.bean;

import java.util.List;

public class OrderBean {

	private Long amount;
	private String receipt;
	private String currency;
	private Integer payment_capture;
	private List<TransferBean> transfers;
	
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public Integer getPayment_capture() {
		return payment_capture;
	}
	public void setPayment_capture(Integer payment_capture) {
		this.payment_capture = payment_capture;
	}
	public List<TransferBean> getTransfers() {
		return transfers;
	}
	public void setTransfers(List<TransferBean> transfers) {
		this.transfers = transfers;
	}
}
