package com.ingenio.admission.bean;

public class PaymentSettingBean {
	
	private Integer userID;
	private Integer schoolId;
	private Integer standardId;
	private Integer yearId;
	private String installment_wise_fee;
	private String minimum_fee_payment;
	private Integer payFeeSettingID;
	private String amountOrFormula;
	private String amountOrFormulaFlag;
	private String installmentTypeDescription;
	private String installmentTypeFlag;
	
	
	
	
	public String getInstallmentTypeDescription() {
		return installmentTypeDescription;
	}
	public void setInstallmentTypeDescription(String installmentTypeDescription) {
		this.installmentTypeDescription = installmentTypeDescription;
	}
	public String getInstallmentTypeFlag() {
		return installmentTypeFlag;
	}
	public void setInstallmentTypeFlag(String installmentTypeFlag) {
		this.installmentTypeFlag = installmentTypeFlag;
	}
	public PaymentSettingBean(Integer schoolId) {
		super();
		this.schoolId = schoolId;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public String getInstallment_wise_fee() {
		return installment_wise_fee;
	}
	public void setInstallment_wise_fee(String installment_wise_fee) {
		this.installment_wise_fee = installment_wise_fee;
	}
	public String getMinimum_fee_payment() {
		return minimum_fee_payment;
	}
	public void setMinimum_fee_payment(String minimum_fee_payment) {
		this.minimum_fee_payment = minimum_fee_payment;
	}
	public Integer getPayFeeSettingID() {
		return payFeeSettingID;
	}
	public void setPayFeeSettingID(Integer payFeeSettingID) {
		this.payFeeSettingID = payFeeSettingID;
	}
	public String getAmountOrFormula() {
		return amountOrFormula;
	}
	public void setAmountOrFormula(String amountOrFormula) {
		this.amountOrFormula = amountOrFormula;
	}
	public String getAmountOrFormulaFlag() {
		return amountOrFormulaFlag;
	}
	public void setAmountOrFormulaFlag(String amountOrFormulaFlag) {
		this.amountOrFormulaFlag = amountOrFormulaFlag;
	}
	public PaymentSettingBean(Integer schoolId, String installment_wise_fee, String minimum_fee_payment,
			String amountOrFormula, String amountOrFormulaFlag ,String installmentTypeFlag ,String installmentTypeDescription) {
		super();
		this.schoolId = schoolId;
		this.installment_wise_fee = installment_wise_fee;
		this.minimum_fee_payment = minimum_fee_payment;
		this.amountOrFormula = amountOrFormula;
		this.amountOrFormulaFlag = amountOrFormulaFlag;
		this.installmentTypeFlag=installmentTypeFlag;
		this.installmentTypeDescription=installmentTypeDescription;
	}
	
	
	
}
