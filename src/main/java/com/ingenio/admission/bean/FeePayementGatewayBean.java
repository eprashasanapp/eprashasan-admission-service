package com.ingenio.admission.bean;

import java.util.List;

public class FeePayementGatewayBean {
	
	private Double grandAssignedTotal;
	private Double grandPaidTotal;
	private Double grandRemainingTotal;
	private Double totalAssignedFee;
	private Double totalPaidFee;
	private Double totalRemainingFee;
	private Double assignedLateFee;
	private Double paidLateFee;
	private Double remainingLateFee;
	private Double assignedBounceCharges;
	private Double paidBounceCharges;
	private Double remainingBounceCharges;
	List<HeadFeeBean> headFeeBeanList;
	private List<DiscountBean> discountBean;
	
	public Double getGrandAssignedTotal() {
		return grandAssignedTotal;
	}
	public void setGrandAssignedTotal(Double grandAssignedTotal) {
		this.grandAssignedTotal = grandAssignedTotal;
	}
	public Double getGrandPaidTotal() {
		return grandPaidTotal;
	}
	public void setGrandPaidTotal(Double grandPaidTotal) {
		this.grandPaidTotal = grandPaidTotal;
	}
	public Double getGrandRemainingTotal() {
		return grandRemainingTotal;
	}
	public void setGrandRemainingTotal(Double grandRemainingTotal) {
		this.grandRemainingTotal = grandRemainingTotal;
	}
	public Double getTotalAssignedFee() {
		return totalAssignedFee;
	}
	public void setTotalAssignedFee(Double totalAssignedFee) {
		this.totalAssignedFee = totalAssignedFee;
	}
	public Double getTotalPaidFee() {
		return totalPaidFee;
	}
	public void setTotalPaidFee(Double totalPaidFee) {
		this.totalPaidFee = totalPaidFee;
	}
	public Double getTotalRemainingFee() {
		return totalRemainingFee;
	}
	public void setTotalRemainingFee(Double totalRemainingFee) {
		this.totalRemainingFee = totalRemainingFee;
	}
	public Double getAssignedLateFee() {
		return assignedLateFee;
	}
	public void setAssignedLateFee(Double assignedLateFee) {
		this.assignedLateFee = assignedLateFee;
	}
	public Double getPaidLateFee() {
		return paidLateFee;
	}
	public void setPaidLateFee(Double paidLateFee) {
		this.paidLateFee = paidLateFee;
	}
	public Double getRemainingLateFee() {
		return remainingLateFee;
	}
	public void setRemainingLateFee(Double remainingLateFee) {
		this.remainingLateFee = remainingLateFee;
	}
	public Double getAssignedBounceCharges() {
		return assignedBounceCharges;
	}
	public void setAssignedBounceCharges(Double assignedBounceCharges) {
		this.assignedBounceCharges = assignedBounceCharges;
	}
	public Double getPaidBounceCharges() {
		return paidBounceCharges;
	}
	public void setPaidBounceCharges(Double paidBounceCharges) {
		this.paidBounceCharges = paidBounceCharges;
	}
	public Double getRemainingBounceCharges() {
		return remainingBounceCharges;
	}
	public void setRemainingBounceCharges(Double remainingBounceCharges) {
		this.remainingBounceCharges = remainingBounceCharges;
	}
	public List<HeadFeeBean> getHeadFeeBeanList() {
		return headFeeBeanList;
	}
	public void setHeadFeeBeanList(List<HeadFeeBean> headFeeBeanList) {
		this.headFeeBeanList = headFeeBeanList;
	}
	public List<DiscountBean> getDiscountBean() {
		return discountBean;
	}
	public void setDiscountBean(List<DiscountBean> discountBean) {
		this.discountBean = discountBean;
	}
}
