/*package com.ingenio.admission.bean;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.ingenio.admission.model.User;

import java.util.Collection;
import java.util.stream.Collectors;

public class LoginUserDetails implements UserDetails {
    
	private static final long serialVersionUID = 1L;
	
	private User user;
    public LoginUserDetails(User user) {
        this.user = user;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getRoles().stream().map(authority -> new SimpleGrantedAuthority(authority.getRoleName().toString())).collect(Collectors.toList());
    }
    public int getId() {
        return user.getAppUserRoleId();
    }
    @Override
    public String getPassword() {
        return user.getPassword();
    }
    @Override
    public String getUsername() {
        return user.getUsername();
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
    public User getUserDetails() {
        return user;
    }
}*/