package com.ingenio.admission.bean;

public class StudentAdmissionBean {
	
	private Integer AcademicYear;
	private String 	AcademicYearText;
	private String RegistrationNumber; 
	private Integer GRBookName;
	private String AdmissionDate;
	private String ddAdmissionDate;
	private String mmAdmissionDate;
	private String yyAdmissionDate;
	private String FormNumber;
	private String Medium;
	private String MissMr;
	private String MiddleName;
	private String BirthDate;
	private String Gender;
	private Integer Category;
	private String Mothertongue;
	private String FirstName;
	private String LastName;
	private String BirthPlace; 
	private String ddBirthDate; 	
	private String mmBirthDate; 	
	private String yyBirthDate; 	
	private Integer Caste;
	private Integer Religion;
	private String Nationality;
	private String FatherName;
	private String MotherName; 
	private String Education;
	private String Occupation;
	private String Income;
	private String Age;
	private String ParentEmailID;
	private String LocalAddress;
	private String LocalDistrict;
	private String LocalPinCode;
	private String LocalState;
	private String LocalCountry;
	private String PermanentAddress;
	private String PermanentDistrict;
	private String PermanentPinCode;
	private String PermanentState;
	private String PermanentCountry;
	private String ContactNumber1ForSMS;
	private String ContactNumber2;
	private String EmailID;
	private String BankName;
	private String AccountNo;
	private String BranchName;
	private String BankIFSCNo;
	private String AadhaarCardNo;
	private String PreviousSchoolName;
	private String PreviousClass;
	private String PreviousYear;
	private String ReasonforLeavingPreviousSchool;
	private Integer schoolId;
	private Integer standard;
	private String standardText;
	private String[] dynamicTitleArr;
	private String[] dynamicValueArr;
	private String[] dynamicCheckTitleArr;
	private String[] dynamicCheckValueArr;
	private String[] dynamicTitleArr2;
	private String[] dynamicValueArr2;
	private String[] attachmentIdArr;
	private String[] attachmentIdNameArr;
	private String userName;
	private Integer studentId;
	private Integer appUserRoleId;
	private Integer studentRenewId;
	private String[] selectedSubjectArr;
	private String[] studentFileAttachmentIDArray;
	private String schoolKey;
	private String[] filePath;
	private String rollNo;
	private String religionName;
	private String categoryName;
	private String casteName;
	private String standardName;
	private String divName;
	private String mMobile;
	private String fMobile;
	private String studLName;
	private String studFName;
	private String studId;
	private String defaultFormNo;
	private String stdentFile;
	private String stdentAttachment;
	private String studImgFilePath;
	private String studRegNo;
	private String inputName;
	private String inputData;
	private String textbox1;
	private String textbox2;
	private String textbox3;
	private String textbox4;
	private String textbox5;
	private String combobox1;
	private String combobox2;
	private String combobox3;
	private String combobox4;
	private String combobox5;
	private String attachmentFileName;
	private String attachmentFilePath;
	private Integer userId;
	private String certificateName;
	private Integer serialNo;
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public StudentAdmissionBean() {
		super();
	}

	public StudentAdmissionBean(Integer studentId, Integer appUserRoleId,String RegistrationNumber) {
		super();
		this.studentId = studentId;
		this.appUserRoleId = appUserRoleId;
		this.RegistrationNumber=RegistrationNumber;
	}
	
	public StudentAdmissionBean(Integer studentRenewId) {
		super();
		this.studentRenewId = studentRenewId;
	}
	
	
	public StudentAdmissionBean(String RegistrationNumber) {
		super();
		this.RegistrationNumber = RegistrationNumber;
	}
	

	public String getAttachmentFileName() {
		return attachmentFileName;
	}


	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}

	public String getAttachmentFilePath() {
		return attachmentFilePath;
	}


	public void setAttachmentFilePath(String attachmentFilePath) {
		this.attachmentFilePath = attachmentFilePath;
	}




	public StudentAdmissionBean(String  attachmentFileName, String attachmentFilePath) {
		super();
		this.attachmentFileName = attachmentFileName;
		this.attachmentFilePath=attachmentFilePath;
	}

	public StudentAdmissionBean(Integer academicYear, String registrationNumber, Integer gRBookName, String admissionDate,
			String formNumber, String medium, String missMr, String middleName, String birthDate, String gender,
			Integer category, String mothertongue, String firstName, String lastName, String birthPlace, Integer caste,
			Integer religion, String nationality, String fatherName, String motherName, String education,
			String occupation, String income, String age, String parentEmailID, String localAddress, String localDistrict,
			String localPinCode, String localState, String localCountry, String permanentAddress,
			String permanentDistrict, String permanentPinCode, String permanentState, String permanentCountry,
			String contactNumber1ForSMS, String contactNumber2, String emailID, String bankName, String branchName, 
			String bankIFSCNo, String accountNo, String aadhaarCardNo, String previousSchoolName, String previousClass,
			String previousYear, String reasonforLeavingPreviousSchool, Integer standard,
			String userName, Integer studentId, Integer appUserRoleId, Integer studentRenewId) {
		super();
		AcademicYear = academicYear;
		RegistrationNumber = registrationNumber;
		GRBookName = gRBookName;
		AdmissionDate = admissionDate;
		FormNumber = formNumber;
		Medium = medium;
		MissMr = missMr;
		MiddleName = middleName;
		BirthDate = birthDate;
		Gender = gender;
		Category = category;
		Mothertongue = mothertongue;
		FirstName = firstName;
		LastName = lastName;
		BirthPlace = birthPlace;
		Caste = caste;
		Religion = religion;
		Nationality = nationality;
		FatherName = fatherName;
		MotherName = motherName;
		Education = education;
		Occupation = occupation;
		Income = income;
		Age = age;
		ParentEmailID = parentEmailID;
		LocalAddress = localAddress;
		LocalDistrict = localDistrict;
		LocalPinCode = localPinCode;
		LocalState = localState;
		LocalCountry = localCountry;
		PermanentAddress = permanentAddress;
		PermanentDistrict = permanentDistrict;
		PermanentPinCode = permanentPinCode;
		PermanentState = permanentState;
		PermanentCountry = permanentCountry;
		ContactNumber1ForSMS = contactNumber1ForSMS;
		ContactNumber2 = contactNumber2;
		EmailID = emailID;
		BankName = bankName;
		AccountNo = accountNo;
		BranchName = branchName;
		BankIFSCNo = bankIFSCNo;
		AadhaarCardNo = aadhaarCardNo;
		PreviousSchoolName = previousSchoolName;
		PreviousClass = previousClass;
		PreviousYear = previousYear;
		ReasonforLeavingPreviousSchool = reasonforLeavingPreviousSchool;
		this.standard = standard;
		this.userName = userName;
		this.studentId = studentId;
		this.appUserRoleId = appUserRoleId;
		this.studentRenewId = studentRenewId;
	}

	
	
	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getReligionName() {
		return religionName;
	}

	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCasteName() {
		return casteName;
	}

	public void setCasteName(String casteName) {
		this.casteName = casteName;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getDivName() {
		return divName;
	}

	public void setDivName(String divName) {
		this.divName = divName;
	}

	public String getmMobile() {
		return mMobile;
	}

	public void setmMobile(String mMobile) {
		this.mMobile = mMobile;
	}

	public String getfMobile() {
		return fMobile;
	}

	public void setfMobile(String fMobile) {
		this.fMobile = fMobile;
	}

	public String getTextbox1() {
		return textbox1;
	}

	public void setTextbox1(String textbox1) {
		this.textbox1 = textbox1;
	}

	public String getTextbox2() {
		return textbox2;
	}

	public void setTextbox2(String textbox2) {
		this.textbox2 = textbox2;
	}

	public String getTextbox3() {
		return textbox3;
	}

	public void setTextbox3(String textbox3) {
		this.textbox3 = textbox3;
	}

	public String getTextbox4() {
		return textbox4;
	}

	public void setTextbox4(String textbox4) {
		this.textbox4 = textbox4;
	}

	public String getTextbox5() {
		return textbox5;
	}

	public void setTextbox5(String textbox5) {
		this.textbox5 = textbox5;
	}

	public String getCombobox1() {
		return combobox1;
	}

	public void setCombobox1(String combobox1) {
		this.combobox1 = combobox1;
	}

	public String getCombobox2() {
		return combobox2;
	}

	public void setCombobox2(String combobox2) {
		this.combobox2 = combobox2;
	}

	public String getCombobox3() {
		return combobox3;
	}

	public void setCombobox3(String combobox3) {
		this.combobox3 = combobox3;
	}

	public String getCombobox4() {
		return combobox4;
	}

	public void setCombobox4(String combobox4) {
		this.combobox4 = combobox4;
	}

	public String getCombobox5() {
		return combobox5;
	}

	public void setCombobox5(String combobox5) {
		this.combobox5 = combobox5;
	}

	
	public Integer getAcademicYear() {
		return AcademicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		AcademicYear = academicYear;
	}

	public String getRegistrationNumber() {
		return RegistrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		RegistrationNumber = registrationNumber;
	}

	public Integer getGRBookName() {
		return GRBookName;
	}

	public void setGRBookName(Integer gRBookName) {
		GRBookName = gRBookName;
	}

	public String getAdmissionDate() {
		return AdmissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		AdmissionDate = admissionDate;
	}

	public String getDdAdmissionDate() {
		return ddAdmissionDate;
	}

	public void setDdAdmissionDate(String ddAdmissionDate) {
		this.ddAdmissionDate = ddAdmissionDate;
	}

	public String getMmAdmissionDate() {
		return mmAdmissionDate;
	}

	public void setMmAdmissionDate(String mmAdmissionDate) {
		this.mmAdmissionDate = mmAdmissionDate;
	}

	public String getYyAdmissionDate() {
		return yyAdmissionDate;
	}

	public void setYyAdmissionDate(String yyAdmissionDate) {
		this.yyAdmissionDate = yyAdmissionDate;
	}

	public String getFormNumber() {
		return FormNumber;
	}

	public void setFormNumber(String formNumber) {
		FormNumber = formNumber;
	}

	public String getMedium() {
		return Medium;
	}

	public void setMedium(String medium) {
		Medium = medium;
	}

	public String getMissMr() {
		return MissMr;
	}

	public void setMissMr(String missMr) {
		MissMr = missMr;
	}

	public String getMiddleName() {
		return MiddleName;
	}

	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}

	public String getBirthDate() {
		return BirthDate;
	}

	public void setBirthDate(String birthDate) {
		BirthDate = birthDate;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public Integer getCategory() {
		return Category;
	}

	public void setCategory(Integer category) {
		Category = category;
	}

	public String getMothertongue() {
		return Mothertongue;
	}

	public void setMothertongue(String mothertongue) {
		Mothertongue = mothertongue;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getBirthPlace() {
		return BirthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		BirthPlace = birthPlace;
	}

	public String getDdBirthDate() {
		return ddBirthDate;
	}

	public void setDdBirthDate(String ddBirthDate) {
		this.ddBirthDate = ddBirthDate;
	}

	public String getMmBirthDate() {
		return mmBirthDate;
	}

	public void setMmBirthDate(String mmBirthDate) {
		this.mmBirthDate = mmBirthDate;
	}

	public String getYyBirthDate() {
		return yyBirthDate;
	}

	public void setYyBirthDate(String yyBirthDate) {
		this.yyBirthDate = yyBirthDate;
	}

	public Integer getCaste() {
		return Caste;
	}

	public void setCaste(Integer caste) {
		Caste = caste;
	}

	public Integer getReligion() {
		return Religion;
	}

	public void setReligion(Integer religion) {
		Religion = religion;
	}

	public String getNationality() {
		return Nationality;
	}

	public void setNationality(String nationality) {
		Nationality = nationality;
	}

	public String getFatherName() {
		return FatherName;
	}

	public void setFatherName(String fatherName) {
		FatherName = fatherName;
	}

	public String getMotherName() {
		return MotherName;
	}

	public void setMotherName(String motherName) {
		MotherName = motherName;
	}

	public String getEducation() {
		return Education;
	}

	public void setEducation(String education) {
		Education = education;
	}

	public String getOccupation() {
		return Occupation;
	}

	public void setOccupation(String occupation) {
		Occupation = occupation;
	}

	public String getIncome() {
		return Income;
	}

	public void setIncome(String income) {
		Income = income;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getParentEmailID() {
		return ParentEmailID;
	}

	public void setParentEmailID(String parentEmailID) {
		ParentEmailID = parentEmailID;
	}

	public String getLocalAddress() {
		return LocalAddress;
	}

	public void setLocalAddress(String localAddress) {
		LocalAddress = localAddress;
	}

	public String getLocalDistrict() {
		return LocalDistrict;
	}

	public void setLocalDistrict(String localDistrict) {
		LocalDistrict = localDistrict;
	}

	public String getLocalPinCode() {
		return LocalPinCode;
	}

	public void setLocalPinCode(String localPinCode) {
		LocalPinCode = localPinCode;
	}

	public String getLocalState() {
		return LocalState;
	}

	public void setLocalState(String localState) {
		LocalState = localState;
	}

	public String getLocalCountry() {
		return LocalCountry;
	}

	public void setLocalCountry(String localCountry) {
		LocalCountry = localCountry;
	}

	public String getPermanentAddress() {
		return PermanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		PermanentAddress = permanentAddress;
	}

	public String getPermanentDistrict() {
		return PermanentDistrict;
	}

	public void setPermanentDistrict(String permanentDistrict) {
		PermanentDistrict = permanentDistrict;
	}

	public String getPermanentPinCode() {
		return PermanentPinCode;
	}

	public void setPermanentPinCode(String permanentPinCode) {
		PermanentPinCode = permanentPinCode;
	}

	public String getPermanentState() {
		return PermanentState;
	}

	public void setPermanentState(String permanentState) {
		PermanentState = permanentState;
	}

	public String getPermanentCountry() {
		return PermanentCountry;
	}

	public void setPermanentCountry(String permanentCountry) {
		PermanentCountry = permanentCountry;
	}

	public String getContactNumber1ForSMS() {
		return ContactNumber1ForSMS;
	}

	public void setContactNumber1ForSMS(String contactNumber1ForSMS) {
		ContactNumber1ForSMS = contactNumber1ForSMS;
	}

	public String getContactNumber2() {
		return ContactNumber2;
	}

	public void setContactNumber2(String contactNumber2) {
		ContactNumber2 = contactNumber2;
	}

	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getBankName() {
		return BankName;
	}

	public void setBankName(String bankName) {
		BankName = bankName;
	}

	public String getAccountNo() {
		return AccountNo;
	}

	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}

	public String getBranchName() {
		return BranchName;
	}

	public void setBranchName(String branchName) {
		BranchName = branchName;
	}

	public String getBankIFSCNo() {
		return BankIFSCNo;
	}

	public void setBankIFSCNo(String bankIFSCNo) {
		BankIFSCNo = bankIFSCNo;
	}

	public String getAadhaarCardNo() {
		return AadhaarCardNo;
	}

	public void setAadhaarCardNo(String aadhaarCardNo) {
		AadhaarCardNo = aadhaarCardNo;
	}

	public String getPreviousSchoolName() {
		return PreviousSchoolName;
	}

	public void setPreviousSchoolName(String previousSchoolName) {
		PreviousSchoolName = previousSchoolName;
	}

	public String getPreviousClass() {
		return PreviousClass;
	}

	public void setPreviousClass(String previousClass) {
		PreviousClass = previousClass;
	}

	public String getPreviousYear() {
		return PreviousYear;
	}

	public void setPreviousYear(String previousYear) {
		PreviousYear = previousYear;
	}

	public String getReasonforLeavingPreviousSchool() {
		return ReasonforLeavingPreviousSchool;
	}

	public void setReasonforLeavingPreviousSchool(String reasonforLeavingPreviousSchool) {
		ReasonforLeavingPreviousSchool = reasonforLeavingPreviousSchool;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getStandard() {
		return standard;
	}

	public void setStandard(Integer standard) {
		this.standard = standard;
	}

	public String[] getDynamicTitleArr() {
		return dynamicTitleArr;
	}

	public void setDynamicTitleArr(String[] dynamicTitleArr) {
		this.dynamicTitleArr = dynamicTitleArr;
	}

	public String[] getDynamicValueArr() {
		return dynamicValueArr;
	}

	public void setDynamicValueArr(String[] dynamicValueArr) {
		this.dynamicValueArr = dynamicValueArr;
	}

	public String[] getAttachmentIdArr() {
		return attachmentIdArr;
	}

	public void setAttachmentIdArr(String[] attachmentIdArr) {
		this.attachmentIdArr = attachmentIdArr;
	}

	public String[] getAttachmentIdNameArr() {
		return attachmentIdNameArr;
	}

	public void setAttachmentIdNameArr(String[] attachmentIdNameArr) {
		this.attachmentIdNameArr = attachmentIdNameArr;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public Integer getAppUserRoleId() {
		return appUserRoleId;
	}

	public void setAppUserRoleId(Integer appUserRoleId) {
		this.appUserRoleId = appUserRoleId;
	}

	public Integer getStudentRenewId() {
		return studentRenewId;
	}

	public void setStudentRenewId(Integer studentRenewId) {
		this.studentRenewId = studentRenewId;
	}

	public String[] getDynamicTitleArr2() {
		return dynamicTitleArr2;
	}

	public void setDynamicTitleArr2(String[] dynamicTitleArr2) {
		this.dynamicTitleArr2 = dynamicTitleArr2;
	}

	public String[] getDynamicValueArr2() {
		return dynamicValueArr2;
	}

	public void setDynamicValueArr2(String[] dynamicValueArr2) {
		this.dynamicValueArr2 = dynamicValueArr2;
	}

	public String getStandardText() {
		return standardText;
	}

	public void setStandardText(String standardText) {
		this.standardText = standardText;
	}

	public String getAcademicYearText() {
		return AcademicYearText;
	}

	public void setAcademicYearText(String academicYearText) {
		AcademicYearText = academicYearText;
	}

	public String[] getSelectedSubjectArr() {
		return selectedSubjectArr;
	}

	public void setSelectedSubjectArr(String[] selectedSubjectArr) {
		this.selectedSubjectArr = selectedSubjectArr;
	}

	public String[] getDynamicCheckTitleArr() {
		return dynamicCheckTitleArr;
	}

	public void setDynamicCheckTitleArr(String[] dynamicCheckTitleArr) {
		this.dynamicCheckTitleArr = dynamicCheckTitleArr;
	}

	public String[] getDynamicCheckValueArr() {
		return dynamicCheckValueArr;
	}

	public void setDynamicCheckValueArr(String[] dynamicCheckValueArr) {
		this.dynamicCheckValueArr = dynamicCheckValueArr;
	}

	public String[] getStudentFileAttachmentIDArray() {
		return studentFileAttachmentIDArray;
	}

	public void setStudentFileAttachmentIDArray(String[] studentFileAttachmentIDArray) {
		this.studentFileAttachmentIDArray = studentFileAttachmentIDArray;
	}

	public String getSchoolKey() {
		return schoolKey;
	}

	public void setSchoolKey(String schoolKey) {
		this.schoolKey = schoolKey;
	}

	public String[] getFilePath() {
		return filePath;
	}

	public void setFilePath(String[] filePath) {
		this.filePath = filePath;
	}

	public String getStudLName() {
		return studLName;
	}

	public void setStudLName(String studLName) {
		this.studLName = studLName;
	}

	public String getStudFName() {
		return studFName;
	}

	public void setStudFName(String studFName) {
		this.studFName = studFName;
	}

	public String getStudId() {
		return studId;
	}

	public void setStudId(String studId) {
		this.studId = studId;
	}

	public String getDefaultFormNo() {
		return defaultFormNo;
	}

	public void setDefaultFormNo(String defaultFormNo) {
		this.defaultFormNo = defaultFormNo;
	}

	public String getStudImgFilePath() {
		return studImgFilePath;
	}

	public void setStudImgFilePath(String studImgFilePath) {
		this.studImgFilePath = studImgFilePath;
	}

	public String getStdentAttachment() {
		return stdentAttachment;
	}

	public void setStdentAttachment(String stdentAttachment) {
		this.stdentAttachment = stdentAttachment;
	}

	public String getStdentFile() {
		return stdentFile;
	}

	public void setStdentFile(String stdentFile) {
		this.stdentFile = stdentFile;
	}

	public String getStudRegNo() {
		return studRegNo;
	}

	public void setStudRegNo(String studRegNo) {
		this.studRegNo = studRegNo;
	}




	public String getInputName() {
		return inputName;
	}




	public void setInputName(String inputName) {
		this.inputName = inputName;
	}




	public String getInputData() {
		return inputData;
	}




	public void setInputData(String inputData) {
		this.inputData = inputData;
	}




	public String getCertificateName() {
		return certificateName;
	}




	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}




	public Integer getSerialNo() {
		return serialNo;
	}




	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

}
