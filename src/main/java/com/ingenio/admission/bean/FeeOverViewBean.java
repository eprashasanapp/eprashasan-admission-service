package com.ingenio.admission.bean;

import java.util.List;

public class FeeOverViewBean {
	
	private List<HeadFeeBean> headBeanList;
	private List<PaidFeeOverviewBean> paidFeeOverviewBean;
	private Double grandTotal;
	private Double feeTotal;
	private Double lateFeeCharges;
	private Double chequeReturnCharges;
	public List<HeadFeeBean> getHeadBeanList() {
		return headBeanList;
	}
	public void setHeadBeanList(List<HeadFeeBean> headBeanList) {
		this.headBeanList = headBeanList;
	}
	public List<PaidFeeOverviewBean> getPaidFeeOverviewBean() {
		return paidFeeOverviewBean;
	}
	public void setPaidFeeOverviewBean(List<PaidFeeOverviewBean> paidFeeOverviewBean) {
		this.paidFeeOverviewBean = paidFeeOverviewBean;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public Double getFeeTotal() {
		return feeTotal;
	}
	public void setFeeTotal(Double feeTotal) {
		this.feeTotal = feeTotal;
	}
	public Double getLateFeeCharges() {
		return lateFeeCharges;
	}
	public void setLateFeeCharges(Double lateFeeCharges) {
		this.lateFeeCharges = lateFeeCharges;
	}
	public Double getChequeReturnCharges() {
		return chequeReturnCharges;
	}
	public void setChequeReturnCharges(Double chequeReturnCharges) {
		this.chequeReturnCharges = chequeReturnCharges;
	}
	
}