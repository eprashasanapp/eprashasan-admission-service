package com.ingenio.admission.bean;

import java.util.List;

public class ElectiveGroupBean {
	
	private Integer groupId;
	private String groupName;
	private String selectionType;
	private Integer minSubGroups;
	private Integer maxSubGroups;
	private List<SubGroupBean> subGroupList;
	private Integer totalSubjects;
	
	public ElectiveGroupBean(Integer groupId, String groupName, String selectionType, Integer minSubGroups,
			Integer maxSubGroups,Integer totalSubjects) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
		this.selectionType = selectionType;
		this.minSubGroups = minSubGroups;
		this.maxSubGroups = maxSubGroups;
		this.setTotalSubjects(totalSubjects);
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public List<SubGroupBean> getSubGroupList() {
		return subGroupList;
	}
	public void setSubGroupList(List<SubGroupBean> subGroupList) {
		this.subGroupList = subGroupList;
	}
	public String getSelectionType() {
		return selectionType;
	}
	public void setSelectionType(String selectionType) {
		this.selectionType = selectionType;
	}
	public Integer getMinSubGroups() {
		return minSubGroups;
	}
	public void setMinSubGroups(Integer minSubGroups) {
		this.minSubGroups = minSubGroups;
	}
	public Integer getMaxSubGroups() {
		return maxSubGroups;
	}
	public void setMaxSubGroups(Integer maxSubGroups) {
		this.maxSubGroups = maxSubGroups;
	}
	public Integer getTotalSubjects() {
		return totalSubjects;
	}
	public void setTotalSubjects(Integer totalSubjects) {
		this.totalSubjects = totalSubjects;
	}
	
}
