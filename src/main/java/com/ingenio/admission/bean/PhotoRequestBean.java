package com.ingenio.admission.bean;

public class PhotoRequestBean {
	
	private Integer studentId;
	private Integer renewId;
	private String registrationNumber;
	private Integer schoolId;
	private Integer userId;
	private String IpAddress;
	private Integer grBookId; 
	private String deviceType;
	
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getIpAddress() {
		return IpAddress;
	}
	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}
	public Integer getGrBookId() {
		return grBookId;
	}
	public void setGrBookId(Integer grBookId) {
		this.grBookId = grBookId;
	}
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String devideType) {
		this.deviceType = devideType;
	}
	
	

}
