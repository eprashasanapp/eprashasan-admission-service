package com.ingenio.admission.bean;

import java.util.Date;
import java.util.List;

public class AnnouncementResponseBean {
	
	private String roleName;
	
	private Integer announcementId;
	private String announcement;
	private String announcementTitle;
	private Date startDate;
	private Long totalStudentCount;
	private Long deliveredCount;
	private Long unseenCount;
	private Long seenCount;
	private Long completeCount;
	private Long noOfAttachments;
	private String announcedBy;
	private String announcedByRole;
	private String assignmentStatus;
	private Integer announcementAssignedtoId;
	private List<String> attachmentList;
	private String serverFilePath;
	private Integer schoolId;
	private String registrationNo;
	
	public AnnouncementResponseBean() {
		
	}
	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Long totalStudentCount, Long deliveredCount, Long unseenCount, Long seenCount,
			Long completeCount, Long noOfAttachments) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.totalStudentCount = totalStudentCount;
		this.deliveredCount = deliveredCount;
		this.unseenCount = unseenCount;
		this.seenCount = seenCount;
		this.completeCount = completeCount;
		this.noOfAttachments = noOfAttachments;
	}
	
	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Long noOfAttachments, String announcedBy, String announcedByRole, String assignmentStatus,
			Integer announcementAssignedtoId,String serverFilePath,Integer schoolId,String registrationNo) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.noOfAttachments = noOfAttachments;
		this.announcedBy = announcedBy;
		this.announcedByRole = announcedByRole;
		this.assignmentStatus = assignmentStatus;
		this.announcementAssignedtoId = announcementAssignedtoId;
		this.serverFilePath = serverFilePath;
		this.schoolId=schoolId;
		this.registrationNo=registrationNo;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}
	public String getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}
	public String getAnnouncementTitle() {
		return announcementTitle;
	}
	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Long getTotalStudentCount() {
		return totalStudentCount;
	}
	public void setTotalStudentCount(Long totalStudentCount) {
		this.totalStudentCount = totalStudentCount;
	}
	public Long getDeliveredCount() {
		return deliveredCount;
	}
	public void setDeliveredCount(Long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}
	public Long getUnseenCount() {
		return unseenCount;
	}
	public void setUnseenCount(Long unseenCount) {
		this.unseenCount = unseenCount;
	}
	public Long getSeenCount() {
		return seenCount;
	}
	public void setSeenCount(Long seenCount) {
		this.seenCount = seenCount;
	}
	public Long getCompleteCount() {
		return completeCount;
	}
	public void setCompleteCount(Long completeCount) {
		this.completeCount = completeCount;
	}
	public Long getNoOfAttachments() {
		return noOfAttachments;
	}
	public void setNoOfAttachments(Long noOfAttachments) {
		this.noOfAttachments = noOfAttachments;
	}
	public String getAnnouncedBy() {
		return announcedBy;
	}
	public void setAnnouncedBy(String announcedBy) {
		this.announcedBy = announcedBy;
	}
	public String getAssignmentStatus() {
		return assignmentStatus;
	}
	public void setAssignmentStatus(String assignmentStatus) {
		this.assignmentStatus = assignmentStatus;
	}
	public Integer getAnnouncementAssignedtoId() {
		return announcementAssignedtoId;
	}
	public void setAnnouncementAssignedtoId(Integer announcementAssignedtoId) {
		this.announcementAssignedtoId = announcementAssignedtoId;
	}
	public String getAnnouncedByRole() {
		return announcedByRole;
	}
	public void setAnnouncedByRole(String announcedByRole) {
		this.announcedByRole = announcedByRole;
	}
	public List<String> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<String> attachmentList) {
		this.attachmentList = attachmentList;
	}
	public String getServerFilePath() {
		return serverFilePath;
	}
	public void setServerFilePath(String serverFilePath) {
		this.serverFilePath = serverFilePath;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	
}
