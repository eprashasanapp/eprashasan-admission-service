package com.ingenio.admission.bean;

import java.util.Date;

public class DeviceCommunicationBean {
	private String staffOrStudentFlag;
	private Integer schoolId;
	private Integer staffId;
	private String createDate;
	private String DATE;
	private String inTime;
	private String outTime;
	private Integer presentFlag;
	private Integer STATUS;
	private Integer userId;
	private Date cDate;
	private String rfids;			// 1
	private String dots;
	private String attRfids;		// 2
	private Integer studentId;
	private Integer studRenewId;
	private Integer yearId;
	private Integer standardId;
	private Integer divisionId;
	private String rollNo;
	private String attandenceClasswiseId;
	private Integer monthId;
	private String attendanceDate;
	private String pFlag;
	private Integer pType;
	private Integer noOfTime;
	private String remark;
	private String hardwareType;
	private Integer inRfId;
	private Integer outRfId;
	private String dateOfTransection;
	private String currDate;
	private String studName;
	private String mobileNo;
	private Integer deviceId;
	private Integer organizationId;
	private Integer locationId;
	private String yearName;
	private String month;
	private Integer date;

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDates() {
		return dates;
	}

	public void setDates(String dates) {
		this.dates = dates;
	}

	private String dates;

	public String getAttandenceClasswiseId() {
		return attandenceClasswiseId;
	}

	public void setAttandenceClasswiseId(String attandenceClasswiseId) {
		this.attandenceClasswiseId = attandenceClasswiseId;
	}

	public Integer getMonthId() {
		return monthId;
	}

	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}

	public String getAttendanceDate() {
		return attendanceDate;
	}

	public void setAttendanceDate(String attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public String getpFlag() {
		return pFlag;
	}

	public void setpFlag(String pFlag) {
		this.pFlag = pFlag;
	}

	public Integer getpType() {
		return pType;
	}

	public void setpType(Integer pType) {
		this.pType = pType;
	}

	public Integer getNoOfTime() {
		return noOfTime;
	}

	public void setNoOfTime(Integer noOfTime) {
		this.noOfTime = noOfTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getHardwareType() {
		return hardwareType;
	}

	public void setHardwareType(String hardwareType) {
		this.hardwareType = hardwareType;
	}

	public Integer getInRfId() {
		return inRfId;
	}

	public void setInRfId(Integer inRfId) {
		this.inRfId = inRfId;
	}

	public Integer getOutRfId() {
		return outRfId;
	}

	public void setOutRfId(Integer outRfId) {
		this.outRfId = outRfId;
	}

	public Integer getStudRenewId() {
		return studRenewId;
	}

	public void setStudRenewId(Integer studRenewId) {
		this.studRenewId = studRenewId;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getStaffOrStudentFlag() {
		return staffOrStudentFlag;
	}

	public void setStaffOrStudentFlag(String staffOrStudentFlag) {
		this.staffOrStudentFlag = staffOrStudentFlag;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}

	public Integer getPresentFlag() {
		return presentFlag;
	}

	public void setPresentFlag(Integer presentFlag) {
		this.presentFlag = presentFlag;
	}

	public Integer getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(Integer sTATUS) {
		STATUS = sTATUS;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public String getAttRfids() {
		return attRfids;
	}

	public void setAttRfids(String attRfids) {
		this.attRfids = attRfids;
	}

	public String getDots() {
		return dots;
	}

	public void setDots(String dots) {
		this.dots = dots;
	}

	public String getRfids() {
		return rfids;
	}

	public void setRfids(String rfids) {
		this.rfids = rfids;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getDateOfTransection() {
		return dateOfTransection;
	}

	public void setDateOfTransection(String dateOfTransection) {
		this.dateOfTransection = dateOfTransection;
	}

	public String getCurrDate() {
		return currDate;
	}

	public void setCurrDate(String currDate) {
		this.currDate = currDate;
	}

	public String getStudName() {
		return studName;
	}

	public void setStudName(String studName) {
		this.studName = studName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}

	public DeviceCommunicationBean(String rfids, Integer studentId, Date cDate) {
		super();
		this.rfids = rfids;
		this.studentId = studentId;
		this.cDate = cDate;
	}

	public DeviceCommunicationBean() {
		super();
	}

	
	
}
