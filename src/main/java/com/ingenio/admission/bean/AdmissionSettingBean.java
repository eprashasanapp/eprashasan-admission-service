package com.ingenio.admission.bean;

import java.util.List;

public class AdmissionSettingBean {

	private int tabsId; 
	private String tabsName;
	private int fieldId;
	private String fieldName;
	private String fieldTypeId; 
	private int legendId;
	private String legendName;
	private String isRequired;
	private String isDisplay;
	private String isyearly;
	private int appuserroleid;
	private int schoolid;
	private String ipAddress;
	private String macAddress;
	private String isStaticOrDynamic;
	private List<String> isDisplayArray;
	private List<String> isRequiredArray;
	private List<String> isYearlyArray;
	private List<String> fieldNameArray;
	private List<String> fieldIdArray;
	private List<String> isDisplayArray2;
	private Integer dynamicCurrentLegendId;
	private List<String> isStaticOrDynamicArray;
	
	private List<String> dynamicIsDisplayArray2;
	private List<String> dynamicFieldNameArray;
	private List<String> dynamicIsRequiredArray;
	private List<String> dynamicIsYearlyArray;
	private List<String> dynamicFieldType;
	private List<String> dynamicPriority;
	private List<Integer> dynamicFieldTypeArray;
	private List<Integer> dynamicPriorityArray;
	private List<String> dynamicFieldValueAray;
	private String[] dynamicFieldValuePOpUpArray;
	private Integer dynamicFieldId;
	private String fieldValue;
	private List<String> noOfPriorityArray;
	private Integer priority;
	private List<String> dynamicFieldValuePOpUpArrayList;
	private Integer currentTabName;
	private Integer subjectCategoryId;
	private String subjectCategoryName;
	private Integer subjectId;
	private String subjectName;

	private List<String> oldFieldNameArray;
	private List<String> oldFieldIdArray;
	private List<String> isOldDisplayArray2;
	private List<String> isOldRequiredArray;
	private List<String> isOldYearlyArray;
	private List<String> isOldPriorityArray;
	private Integer currentLegendId;
	private Integer currentLegendIdFinal;
	private List<String> dynamicFieldValuePOpUpArrayList2;
	private String dynamicFieldComboValue;
	private List<String> getoldDynamicValue;
	private List<String> fieldTypeIdArray2;
	private String validationFlag; 
	private List<String> validationFlagArray;
	private String dynamicvalidationFlag; 
	private List<String> dynamicvalidationFlagArray;
	private Integer schoolId;
	private String isTabDisplayOrNot;
	private Integer isPreviousOrNot;
	private String[] updateisYearlyVal;
	private String[] updateDynamicFieldVal;
	private String[] updateisRequiredVal;
	private String[] updateIsRequiredDynamicFieldVal;
	private String[] updateisDisplayVal;
	private String[] updateIsDisplayValDynamicFieldVal;
	public AdmissionSettingBean() {
		super();
	}
	
	public AdmissionSettingBean(int tabsId, String tabsName,int legendId, String legendName, int fieldId, String fieldName, String fieldTypeId,
			String isRequired,String isStaticOrDynamic,String validationFlag) {
		super();
		this.tabsId = tabsId;
		this.tabsName = tabsName;
		this.fieldId = fieldId;
		this.fieldName = fieldName;
		this.fieldTypeId = fieldTypeId;
		this.legendId = legendId;
		this.legendName = legendName;
		this.isRequired = isRequired;
		this.isStaticOrDynamic = isStaticOrDynamic;
		this.validationFlag = validationFlag;
		
	}
	
	public AdmissionSettingBean(Integer subjectCategoryId, String subjectCategoryName, Integer subjectId,
			String subjectName) {
		super();
		this.subjectCategoryId = subjectCategoryId;
		this.subjectCategoryName = subjectCategoryName;
		this.subjectId = subjectId;
		this.subjectName = subjectName;
	}

	public AdmissionSettingBean(Integer dynamicFieldId, String fieldValue) {
		super();
		this.dynamicFieldId = dynamicFieldId;
		this.fieldValue = fieldValue;
	}

	
	public AdmissionSettingBean(String tabsName, String legendName) {
		super();
		this.tabsName = tabsName;
		this.legendName = legendName;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Integer getSubjectCategoryId() {
		return subjectCategoryId;
	}

	public void setSubjectCategoryId(Integer subjectCategoryId) {
		this.subjectCategoryId = subjectCategoryId;
	}

	public String getSubjectCategoryName() {
		return subjectCategoryName;
	}

	public void setSubjectCategoryName(String subjectCategoryName) {
		this.subjectCategoryName = subjectCategoryName;
	}

	public int getTabsId() {
		return tabsId;
	}

	public void setTabsId(int tabsId) {
		this.tabsId = tabsId;
	}

	public String getTabsName() {
		return tabsName;
	}

	public void setTabsName(String tabsName) {
		this.tabsName = tabsName;
	}

	public int getFieldId() {
		return fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldTypeId() {
		return fieldTypeId;
	}

	public void setFieldTypeId(String fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}

	public int getLegendId() {
		return legendId;
	}

	public void setLegendId(int legendId) {
		this.legendId = legendId;
	}

	public String getLegendName() {
		return legendName;
	}

	public void setLegendName(String legendName) {
		this.legendName = legendName;
	}

	public String getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(String isRequired) {
		this.isRequired = isRequired;
	}

	public String getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(String isDisplay) {
		this.isDisplay = isDisplay;
	}

	public String getIsyearly() {
		return isyearly;
	}

	public void setIsyearly(String isyearly) {
		this.isyearly = isyearly;
	}

	public int getAppuserroleid() {
		return appuserroleid;
	}

	public void setAppuserroleid(int appuserroleid) {
		this.appuserroleid = appuserroleid;
	}

	public int getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(int schoolid) {
		this.schoolid = schoolid;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getIsStaticOrDynamic() {
		return isStaticOrDynamic;
	}

	public void setIsStaticOrDynamic(String isStaticOrDynamic) {
		this.isStaticOrDynamic = isStaticOrDynamic;
	}

	public List<String> getIsDisplayArray() {
		return isDisplayArray;
	}

	public void setIsDisplayArray(List<String> isDisplayArray) {
		this.isDisplayArray = isDisplayArray;
	}

	public List<String> getIsRequiredArray() {
		return isRequiredArray;
	}

	public void setIsRequiredArray(List<String> isRequiredArray) {
		this.isRequiredArray = isRequiredArray;
	}

	public List<String> getIsYearlyArray() {
		return isYearlyArray;
	}

	public void setIsYearlyArray(List<String> isYearlyArray) {
		this.isYearlyArray = isYearlyArray;
	}

	public List<String> getFieldNameArray() {
		return fieldNameArray;
	}

	public void setFieldNameArray(List<String> fieldNameArray) {
		this.fieldNameArray = fieldNameArray;
	}

	public List<String> getFieldIdArray() {
		return fieldIdArray;
	}

	public void setFieldIdArray(List<String> fieldIdArray) {
		this.fieldIdArray = fieldIdArray;
	}

	public List<String> getIsDisplayArray2() {
		return isDisplayArray2;
	}

	public void setIsDisplayArray2(List<String> isDisplayArray2) {
		this.isDisplayArray2 = isDisplayArray2;
	}

	public List<String> getDynamicIsDisplayArray2() {
		return dynamicIsDisplayArray2;
	}

	public void setDynamicIsDisplayArray2(List<String> dynamicIsDisplayArray2) {
		this.dynamicIsDisplayArray2 = dynamicIsDisplayArray2;
	}

	public List<String> getDynamicFieldNameArray() {
		return dynamicFieldNameArray;
	}

	public void setDynamicFieldNameArray(List<String> dynamicFieldNameArray) {
		this.dynamicFieldNameArray = dynamicFieldNameArray;
	}

	public List<String> getDynamicIsRequiredArray() {
		return dynamicIsRequiredArray;
	}

	public void setDynamicIsRequiredArray(List<String> dynamicIsRequiredArray) {
		this.dynamicIsRequiredArray = dynamicIsRequiredArray;
	}

	public List<String> getDynamicIsYearlyArray() {
		return dynamicIsYearlyArray;
	}

	public void setDynamicIsYearlyArray(List<String> dynamicIsYearlyArray) {
		this.dynamicIsYearlyArray = dynamicIsYearlyArray;
	}

	public List<String> getDynamicFieldType() {
		return dynamicFieldType;
	}

	public void setDynamicFieldType(List<String> dynamicFieldType) {
		this.dynamicFieldType = dynamicFieldType;
	}

	public List<String> getDynamicPriority() {
		return dynamicPriority;
	}

	public void setDynamicPriority(List<String> dynamicPriority) {
		this.dynamicPriority = dynamicPriority;
	}

	public List<Integer> getDynamicFieldTypeArray() {
		return dynamicFieldTypeArray;
	}

	public void setDynamicFieldTypeArray(List<Integer> dynamicFieldTypeArray) {
		this.dynamicFieldTypeArray = dynamicFieldTypeArray;
	}

	public List<Integer> getDynamicPriorityArray() {
		return dynamicPriorityArray;
	}

	public void setDynamicPriorityArray(List<Integer> dynamicPriorityArray) {
		this.dynamicPriorityArray = dynamicPriorityArray;
	}

	public List<String> getDynamicFieldValueAray() {
		return dynamicFieldValueAray;
	}

	public void setDynamicFieldValueAray(List<String> dynamicFieldValueAray) {
		this.dynamicFieldValueAray = dynamicFieldValueAray;
	}

	public String[] getDynamicFieldValuePOpUpArray() {
		return dynamicFieldValuePOpUpArray;
	}

	public void setDynamicFieldValuePOpUpArray(String[] dynamicFieldValuePOpUpArray) {
		this.dynamicFieldValuePOpUpArray = dynamicFieldValuePOpUpArray;
	}

	public Integer getDynamicFieldId() {
		return dynamicFieldId;
	}

	public void setDynamicFieldId(Integer dynamicFieldId) {
		this.dynamicFieldId = dynamicFieldId;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public Integer getDynamicCurrentLegendId() {
		return dynamicCurrentLegendId;
	}

	public void setDynamicCurrentLegendId(Integer dynamicCurrentLegendId) {
		this.dynamicCurrentLegendId = dynamicCurrentLegendId;
	}

	public List<String> getNoOfPriorityArray() {
		return noOfPriorityArray;
	}

	public void setNoOfPriorityArray(List<String> noOfPriorityArray) {
		this.noOfPriorityArray = noOfPriorityArray;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public List<String> getDynamicFieldValuePOpUpArrayList() {
		return dynamicFieldValuePOpUpArrayList;
	}

	public void setDynamicFieldValuePOpUpArrayList(List<String> dynamicFieldValuePOpUpArrayList) {
		this.dynamicFieldValuePOpUpArrayList = dynamicFieldValuePOpUpArrayList;
	}

	public Integer getCurrentTabName() {
		return currentTabName;
	}

	public void setCurrentTabName(Integer currentTabName) {
		this.currentTabName = currentTabName;
	}

	public List<String> getOldFieldNameArray() {
		return oldFieldNameArray;
	}

	public void setOldFieldNameArray(List<String> oldFieldNameArray) {
		this.oldFieldNameArray = oldFieldNameArray;
	}

	public List<String> getOldFieldIdArray() {
		return oldFieldIdArray;
	}

	public void setOldFieldIdArray(List<String> oldFieldIdArray) {
		this.oldFieldIdArray = oldFieldIdArray;
	}

	public List<String> getIsOldDisplayArray2() {
		return isOldDisplayArray2;
	}

	public void setIsOldDisplayArray2(List<String> isOldDisplayArray2) {
		this.isOldDisplayArray2 = isOldDisplayArray2;
	}

	public List<String> getIsOldRequiredArray() {
		return isOldRequiredArray;
	}

	public void setIsOldRequiredArray(List<String> isOldRequiredArray) {
		this.isOldRequiredArray = isOldRequiredArray;
	}

	public List<String> getIsOldYearlyArray() {
		return isOldYearlyArray;
	}

	public void setIsOldYearlyArray(List<String> isOldYearlyArray) {
		this.isOldYearlyArray = isOldYearlyArray;
	}

	public List<String> getIsOldPriorityArray() {
		return isOldPriorityArray;
	}

	public void setIsOldPriorityArray(List<String> isOldPriorityArray) {
		this.isOldPriorityArray = isOldPriorityArray;
	}

	public Integer getCurrentLegendId() {
		return currentLegendId;
	}

	public void setCurrentLegendId(Integer currentLegendId) {
		this.currentLegendId = currentLegendId;
	}

	public Integer getCurrentLegendIdFinal() {
		return currentLegendIdFinal;
	}

	public void setCurrentLegendIdFinal(Integer currentLegendIdFinal) {
		this.currentLegendIdFinal = currentLegendIdFinal;
	}

	public List<String> getDynamicFieldValuePOpUpArrayList2() {
		return dynamicFieldValuePOpUpArrayList2;
	}

	public void setDynamicFieldValuePOpUpArrayList2(List<String> dynamicFieldValuePOpUpArrayList2) {
		this.dynamicFieldValuePOpUpArrayList2 = dynamicFieldValuePOpUpArrayList2;
	}

	public String getDynamicFieldComboValue() {
		return dynamicFieldComboValue;
	}

	public void setDynamicFieldComboValue(String dynamicFieldComboValue) {
		this.dynamicFieldComboValue = dynamicFieldComboValue;
	}

	public List<String> getGetoldDynamicValue() {
		return getoldDynamicValue;
	}

	public void setGetoldDynamicValue(List<String> getoldDynamicValue) {
		this.getoldDynamicValue = getoldDynamicValue;
	}

	public List<String> getFieldTypeIdArray2() {
		return fieldTypeIdArray2;
	}

	public void setFieldTypeIdArray2(List<String> fieldTypeIdArray2) {
		this.fieldTypeIdArray2 = fieldTypeIdArray2;
	}

	public String getValidationFlag() {
		return validationFlag;
	}

	public void setValidationFlag(String validationFlag) {
		this.validationFlag = validationFlag;
	}

	public List<String> getValidationFlagArray() {
		return validationFlagArray;
	}

	public void setValidationFlagArray(List<String> validationFlagArray) {
		this.validationFlagArray = validationFlagArray;
	}

	public String getDynamicvalidationFlag() {
		return dynamicvalidationFlag;
	}

	public void setDynamicvalidationFlag(String dynamicvalidationFlag) {
		this.dynamicvalidationFlag = dynamicvalidationFlag;
	}

	public List<String> getDynamicvalidationFlagArray() {
		return dynamicvalidationFlagArray;
	}

	public void setDynamicvalidationFlagArray(List<String> dynamicvalidationFlagArray) {
		this.dynamicvalidationFlagArray = dynamicvalidationFlagArray;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getIsTabDisplayOrNot() {
		return isTabDisplayOrNot;
	}

	public void setIsTabDisplayOrNot(String isTabDisplayOrNot) {
		this.isTabDisplayOrNot = isTabDisplayOrNot;
	}

	public List<String> getIsStaticOrDynamicArray() {
		return isStaticOrDynamicArray;
	}

	public void setIsStaticOrDynamicArray(List<String> isStaticOrDynamicArray) {
		this.isStaticOrDynamicArray = isStaticOrDynamicArray;
	}

	public Integer getIsPreviousOrNot() {
		return isPreviousOrNot;
	}

	public void setIsPreviousOrNot(Integer isPreviousOrNot) {
		this.isPreviousOrNot = isPreviousOrNot;
	}

	public String[] getUpdateisYearlyVal() {
		return updateisYearlyVal;
	}

	public void setUpdateisYearlyVal(String[] updateisYearlyVal) {
		this.updateisYearlyVal = updateisYearlyVal;
	}

	public String[] getUpdateDynamicFieldVal() {
		return updateDynamicFieldVal;
	}

	public void setUpdateDynamicFieldVal(String[] updateDynamicFieldVal) {
		this.updateDynamicFieldVal = updateDynamicFieldVal;
	}

	public String[] getUpdateisRequiredVal() {
		return updateisRequiredVal;
	}

	public void setUpdateisRequiredVal(String[] updateisRequiredVal) {
		this.updateisRequiredVal = updateisRequiredVal;
	}

	public String[] getUpdateIsRequiredDynamicFieldVal() {
		return updateIsRequiredDynamicFieldVal;
	}

	public void setUpdateIsRequiredDynamicFieldVal(String[] updateIsRequiredDynamicFieldVal) {
		this.updateIsRequiredDynamicFieldVal = updateIsRequiredDynamicFieldVal;
	}

	public String[] getUpdateIsDisplayValDynamicFieldVal() {
		return updateIsDisplayValDynamicFieldVal;
	}

	public void setUpdateIsDisplayValDynamicFieldVal(String[] updateIsDisplayValDynamicFieldVal) {
		this.updateIsDisplayValDynamicFieldVal = updateIsDisplayValDynamicFieldVal;
	}

	public String[] getUpdateisDisplayVal() {
		return updateisDisplayVal;
	}

	public void setUpdateisDisplayVal(String[] updateisDisplayVal) {
		this.updateisDisplayVal = updateisDisplayVal;
	}


}
