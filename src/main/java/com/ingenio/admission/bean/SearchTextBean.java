package com.ingenio.admission.bean;

import org.springframework.web.bind.annotation.RequestParam;

public class SearchTextBean {

	private Integer schoolId=0;
	private String searchFlag="";
	private String searchText="";
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getSearchFlag() {
		return searchFlag;
	}
	public void setSearchFlag(String searchFlag) {
		this.searchFlag = searchFlag;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	
	
}
