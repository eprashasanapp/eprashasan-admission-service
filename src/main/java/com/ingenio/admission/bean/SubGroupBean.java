package com.ingenio.admission.bean;

import java.util.List;

public class SubGroupBean {

	private int subGroupId;
	private String subGroupName;
	private String selectionType;
	private int minSubjects;
	private int maxSubjects;
	private List<SubjectBean> subjectList;
	
	public SubGroupBean(int subGroupId, String subGroupName, String selectionType, int minSubjects, int maxSubjects) {
		super();
		this.subGroupId = subGroupId;
		this.subGroupName = subGroupName;
		this.selectionType = selectionType;
		this.minSubjects = minSubjects;
		this.maxSubjects = maxSubjects;
	}

	public int getSubGroupId() {
		return subGroupId;
	}

	public void setSubGroupId(int subGroupId) {
		this.subGroupId = subGroupId;
	}

	public String getSubGroupName() {
		return subGroupName;
	}

	public void setSubGroupName(String subGroupName) {
		this.subGroupName = subGroupName;
	}

	public String getSelectionType() {
		return selectionType;
	}

	public void setSelectionType(String selectionType) {
		this.selectionType = selectionType;
	}

	public int getMinSubjects() {
		return minSubjects;
	}

	public void setMinSubjects(int minSubjects) {
		this.minSubjects = minSubjects;
	}

	public int getMaxSubjects() {
		return maxSubjects;
	}

	public void setMaxSubjects(int maxSubjects) {
		this.maxSubjects = maxSubjects;
	}

	public List<SubjectBean> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<SubjectBean> subjectList) {
		this.subjectList = subjectList;
	}
	
}
