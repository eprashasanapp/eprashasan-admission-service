package com.ingenio.admission.bean;

public class DiscountBean {

	private Integer discountId;
	private String discountPercent;
	private String minimumAmount;
	private String discountLabel;
	private String discountDate;
	private String type;
	private String percentFlag;
	
	public Integer getDiscountId() {
		return discountId;
	}
	public void setDiscountId(Integer discountId) {
		this.discountId = discountId;
	}
	public String getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(String discountPercent) {
		this.discountPercent = discountPercent;
	}
	public String getDiscountLabel() {
		return discountLabel;
	}
	public void setDiscountLabel(String discountLabel) {
		this.discountLabel = discountLabel;
	}
	public String getDiscountDate() {
		return discountDate;
	}
	public void setDiscountDate(String discountDate) {
		this.discountDate = discountDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMinimumAmount() {
		return minimumAmount;
	}
	public void setMinimumAmount(String minimumAmount) {
		this.minimumAmount = minimumAmount;
	}
	public String getPercentFlag() {
		return percentFlag;
	}
	public void setPercentFlag(String percentFlag) {
		this.percentFlag = percentFlag;
	}
}
