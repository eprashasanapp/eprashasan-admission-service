package com.ingenio.admission.bean;

public class DynamicFieldValueBean {

	private String fieldValue;
	private String fieldTypeId;  
	private Integer legendId;
	private Integer tabsId;  
	private Integer appuserroleid;
	private Integer schoolid;
	private String isDel="0";
	private String isEdit="0";
	private String sinkingFlag="0";
	private String isApproval="0";
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getFieldTypeId() {
		return fieldTypeId;
	}
	public void setFieldTypeId(String fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}
	public Integer getLegendId() {
		return legendId;
	}
	public void setLegendId(Integer legendId) {
		this.legendId = legendId;
	}
	public Integer getTabsId() {
		return tabsId;
	}
	public void setTabsId(Integer tabsId) {
		this.tabsId = tabsId;
	}
	public Integer getAppuserroleid() {
		return appuserroleid;
	}
	public void setAppuserroleid(Integer appuserroleid) {
		this.appuserroleid = appuserroleid;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	public String getIsApproval() {
		return isApproval;
	}
	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
}
