package com.ingenio.admission.bean;

import java.util.List;

public class PaymentResponseList {

	private List<PaymentResponseBean> paymentList;

	public List<PaymentResponseBean> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<PaymentResponseBean> paymentList) {
		this.paymentList = paymentList;
	}
}
