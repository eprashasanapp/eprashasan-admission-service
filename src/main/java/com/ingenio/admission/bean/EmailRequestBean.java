package com.ingenio.admission.bean;

public class EmailRequestBean {
	
	private String emailId;
	private Double amount;
	
	public EmailRequestBean(String emailId, Double amount) {
		super();
		this.emailId = emailId;
		this.amount = amount;
	}

	public String getEmailId() {
		return emailId;
	}
	
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public EmailRequestBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


}
