package com.ingenio.admission.bean;

import java.util.ArrayList;
import java.util.List;

public class FeeProspectBean {

	private Double prospectFee;
	private Double cgst;
	private Double sgst;
	private String studentName;
	private Double totalPaidFee;
	private Integer headId;
	private String headName;
	private Integer prospectFeesMasterId;
	private Integer schoolId;
	private Integer yearId;
	private Integer standardId;
	private Double totalFee;
	private Double allTotalFee;
	List<FeeProspectBean> prospectfeeList = new ArrayList();
	private Integer registrationFlag=0;
	
	

	public FeeProspectBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<FeeProspectBean> getProspectfeeList() {
		return prospectfeeList;
	}

	public void setProspectfeeList(List<FeeProspectBean> prospectfeeList) {
		this.prospectfeeList = prospectfeeList;
	}

	public FeeProspectBean(Double prospectFee, Double cGST, Double sGST,  Integer headId,
			String headName) {
		super();
		this.prospectFee = prospectFee;
		this.cgst = cGST;
		this.sgst = sGST;
		this.headId = headId;
		this.headName = headName;
	}
	
	
	public FeeProspectBean( Integer registrationFlag) {
		super();
		this.registrationFlag = registrationFlag;
	}

	public Double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}

	public Double getAllTotalFee() {
		return allTotalFee;
	}

	public void setAllTotalFee(Double allTotalFee) {
		this.allTotalFee = allTotalFee;
	}

	public Double getProspectFee() {
		return prospectFee;
	}

	public void setProspectFee(Double prospectFee) {
		this.prospectFee = prospectFee;
	}

	public Double getCgst() {
		return cgst;
	}

	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}

	public Double getSgst() {
		return sgst;
	}

	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Double getTotalPaidFee() {
		return totalPaidFee;
	}

	public void setTotalPaidFee(Double totalPaidFee) {
		this.totalPaidFee = totalPaidFee;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	public Integer getProspectFeesMasterId() {
		return prospectFeesMasterId;
	}

	public void setProspectFeesMasterId(Integer prospectFeesMasterId) {
		this.prospectFeesMasterId = prospectFeesMasterId;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getRegistrationFlag() {
		return registrationFlag;
	}

	public void setRegistrationFlag(Integer registrationFlag) {
		this.registrationFlag = registrationFlag;
	}
	
	
	

}
