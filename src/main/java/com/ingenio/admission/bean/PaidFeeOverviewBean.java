package com.ingenio.admission.bean;

import java.util.List;

public class PaidFeeOverviewBean {

	private List<PaidFeeBean> paidFeeBeanList;
	private Double receiptTotal;
	private String receiptDate;
	private Integer receiptId;
	private String receiptNo;
	private Double chequeReturnCharges;
	private Double lateFeeCharges;
	private String receivedBy;
	private String paidBy;
	
	
	public List<PaidFeeBean> getPaidFeeBeanList() {
		return paidFeeBeanList;
	}
	public void setPaidFeeBeanList(List<PaidFeeBean> paidFeeBeanList) {
		this.paidFeeBeanList = paidFeeBeanList;
	}
	public Double getReceiptTotal() {
		return receiptTotal;
	}
	public void setReceiptTotal(Double receiptTotal) {
		this.receiptTotal = receiptTotal;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public Integer getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(Integer receiptId) {
		this.receiptId = receiptId;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public Double getLateFeeCharges() {
		return lateFeeCharges;
	}
	public void setLateFeeCharges(Double lateFeeCharges) {
		this.lateFeeCharges = lateFeeCharges;
	}
	public Double getChequeReturnCharges() {
		return chequeReturnCharges;
	}
	public void setChequeReturnCharges(Double chequeReturnCharges) {
		this.chequeReturnCharges = chequeReturnCharges;
	}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
	public String getPaidBy() {
		return paidBy;
	}
	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}
}
