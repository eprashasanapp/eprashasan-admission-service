package com.ingenio.admission.bean;

import java.util.List;

public class PaidFeeBean {
	
	private List<HeadFeeBean> headBeanList;

	public List<HeadFeeBean> getHeadBeanList() {
		return headBeanList;
	}

	public void setHeadBeanList(List<HeadFeeBean> headBeanList) {
		this.headBeanList = headBeanList;
	}
}
