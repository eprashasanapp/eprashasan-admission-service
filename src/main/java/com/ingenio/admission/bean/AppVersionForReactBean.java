package com.ingenio.admission.bean;

public class AppVersionForReactBean {

	
	private  Integer versionID;
	private  Integer versionCode;
	private String versionName;
	private String instruction;
	private String forRole;
	private String forSchool;
	private String isMandatoryToRole;
	private String isMandatoryToSchool;
	private String isReact;

	private String ismandatory;
	private String noaction;
	public Integer getVersionID() {
		return versionID;
	}
	public void setVersionID(Integer versionID) {
		this.versionID = versionID;
	}
	public Integer getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(Integer versionCode) {
		this.versionCode = versionCode;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public String getForRole() {
		return forRole;
	}
	public void setForRole(String forRole) {
		this.forRole = forRole;
	}
	public String getForSchool() {
		return forSchool;
	}
	public void setForSchool(String forSchool) {
		this.forSchool = forSchool;
	}
	public String getIsMandatoryToRole() {
		return isMandatoryToRole;
	}
	public void setIsMandatoryToRole(String isMandatoryToRole) {
		this.isMandatoryToRole = isMandatoryToRole;
	}
	public String getIsMandatoryToSchool() {
		return isMandatoryToSchool;
	}
	public void setIsMandatoryToSchool(String isMandatoryToSchool) {
		this.isMandatoryToSchool = isMandatoryToSchool;
	}
	public String getIsReact() {
		return isReact;
	}
	public void setIsReact(String isReact) {
		this.isReact = isReact;
	}
	public String getIsmandatory() {
		return ismandatory;
	}
	public void setIsmandatory(String ismandatory) {
		this.ismandatory = ismandatory;
	}
	public String getNoaction() {
		return noaction;
	}
	public void setNoaction(String noaction) {
		this.noaction = noaction;
	}
	public AppVersionForReactBean(Integer versionID, Integer versionCode, String versionName, String instruction,
			String forRole, String forSchool, String isMandatoryToRole, String isMandatoryToSchool,String isReact) {
		super();
		this.versionID = versionID;
		this.versionCode = versionCode;
		this.versionName = versionName;
		this.instruction = instruction;
		this.forRole = forRole;
		this.forSchool = forSchool;
		this.isMandatoryToRole = isMandatoryToRole;
		this.isMandatoryToSchool = isMandatoryToSchool;
		this.isReact=isReact;
	}
	

	
}
