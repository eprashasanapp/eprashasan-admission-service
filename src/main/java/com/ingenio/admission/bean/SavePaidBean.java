package com.ingenio.admission.bean;

import java.util.List;

public class SavePaidBean {
	
	private String orderId;
	private String transactionId;
	private Integer schoolId;
	private List<FeeBean> feeBeanList;
	private String deviceType;
	private String ipAddress;
	private String processId;
	private String yearId;
	
	
	
	public String getYearId() {
		return yearId;
	}
	public void setYearId(String yearId) {
		this.yearId = yearId;
	}
	public SavePaidBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public List<FeeBean> getFeeBeanList() {
		return feeBeanList;
	}
	public void setFeeBeanList(List<FeeBean> feeBeanList) {
		this.feeBeanList = feeBeanList;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public SavePaidBean(Integer schoolId, String processId) {
		super();
		this.schoolId = schoolId;
		this.processId = processId;
	} 
	
	
	
}
