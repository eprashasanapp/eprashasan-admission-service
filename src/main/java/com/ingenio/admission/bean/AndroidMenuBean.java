package com.ingenio.admission.bean;

import java.util.List;

public class AndroidMenuBean {

	private Integer menuId;
	private String menuName;
	private String iconPath;
	private String groupName;
	private Integer groupdId;
	private String isWebViewForReact;
	private String isWebViewForAndroid;
	private String webViewURL;
	private Integer parentMenuId;
	private Integer userId;
	private Integer priority;
	private List<AndroidMenuBean> menuList ;
	

	public AndroidMenuBean(Integer groupdId, String groupName,Integer menuId, String menuName, String iconPath,String isWebViewForReact,String isWebViewForAndroid,String webViewURL,Integer parentMenuId) {
		super();
		this.groupdId=groupdId;
		this.groupName=groupName;
		this.menuId = menuId;
		this.menuName = menuName;
		this.iconPath = iconPath;
		this.isWebViewForReact=isWebViewForReact;
		this.isWebViewForAndroid=isWebViewForAndroid;
		this.webViewURL=webViewURL;
		this.parentMenuId=parentMenuId;
	}
	public AndroidMenuBean(Integer groupdId, String groupName,Integer menuId, String menuName, String iconPath) {
		super();
		this.groupdId=groupdId;
		this.groupName=groupName;
		this.menuId = menuId;
		this.menuName = menuName;
		this.iconPath = iconPath;
	}
	
	public AndroidMenuBean(Integer menuId, String menuName, String iconPath) {
		super();		
		this.menuId = menuId;
		this.menuName = menuName;
		this.iconPath = iconPath;
	}
	
	public AndroidMenuBean(Integer groupdId, String groupName,Integer priority) {
		super();
		this.groupdId=groupdId;
		this.groupName=groupName;
		this.priority=priority;
	}

	public AndroidMenuBean() {
		super();
	}

	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getGroupdId() {
		return groupdId;
	}

	public void setGroupdId(Integer groupdId) {
		this.groupdId = groupdId;
	}

	public List<AndroidMenuBean> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<AndroidMenuBean> menuList) {
		this.menuList = menuList;
	}

	public String getIsWebViewForReact() {
		return isWebViewForReact;
	}

	public void setIsWebViewForReact(String isWebViewForReact) {
		this.isWebViewForReact = isWebViewForReact;
	}

	public String getIsWebViewForAndroid() {
		return isWebViewForAndroid;
	}

	public void setIsWebViewForAndroid(String isWebViewForAndroid) {
		this.isWebViewForAndroid = isWebViewForAndroid;
	}

	public String getWebViewURL() {
		return webViewURL;
	}

	public void setWebViewURL(String webViewURL) {
		this.webViewURL = webViewURL;
	}
	public Integer getParentMenuId() {
		return parentMenuId;
	}
	public void setParentMenuId(Integer parentMenuId) {
		this.parentMenuId = parentMenuId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	
}
