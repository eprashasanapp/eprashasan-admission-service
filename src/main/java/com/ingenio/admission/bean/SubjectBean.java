package com.ingenio.admission.bean;

import java.util.List;

public class SubjectBean {
	
	private Integer subjectId;
	private String subjectName;
	private String subjectCode;
	private boolean isSelected;
	private Integer schoolid;
	private String iseligiblegrace;
	private String iseligibleGrade;
	private String standard;
	private String year;
	private String status;
	private String subjectCategory;
	private String courseCode;
	private String gradeType;
	private String practicaltheoryFlag;
	private String yearsemesterflag;
	private String credit;
	private String classOpenFlag;
	private String compulElectiveFlag;
	private String semester;
	private Integer semesterId;
	private Integer semesterType;
	private Integer groupId;
	private Integer subGroupId;
	private Integer examNameId;
	private String activeExamName; 
	private String activeExam;
	private String creditWithoutAttendance;
	private String rcCredit;
	private String wrcCredit;
	private Integer order;
	private Integer yearId;
	private Integer standardId;
	private Integer orderSequence;
	private int admissionTypeId;
	private String admissionType;
	private Integer examCreditSettingId;
	private Integer studentId;
	private String backlogFlag;
	private Integer appUserRoleId;
	private Integer assignSubjectId;
	private int rcrwc;
	private String groupName;
	private String subGroupName;
	private String gradeTypeStr;
	private String standardName;
	List<SubjectBean> semesterList;
	List<SubjectBean> ExamList;
	List<SubjectBean> admissionTypeList;
	private String yearIds;
	public SubjectBean() {
		super();
	}
	
	  public SubjectBean(Integer subjectId, String subjectName, String subjectCode,
	  boolean isSelected) 
	  { 
		  super(); 
		  this.subjectId = subjectId; 
		  this.subjectName =subjectName; 
		  this.subjectCode = subjectCode; 
		  this.isSelected = isSelected; 
	}
	 
	
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getSubjectCode() {
		return subjectCode;
	}
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	public String getIseligiblegrace() {
		return iseligiblegrace;
	}

	public void setIseligiblegrace(String iseligiblegrace) {
		this.iseligiblegrace = iseligiblegrace;
	}

	public String getIseligibleGrade() {
		return iseligibleGrade;
	}

	public void setIseligibleGrade(String iseligibleGrade) {
		this.iseligibleGrade = iseligibleGrade;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubjectCategory() {
		return subjectCategory;
	}

	public void setSubjectCategory(String subjectCategory) {
		this.subjectCategory = subjectCategory;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getGradeType() {
		return gradeType;
	}

	public void setGradeType(String gradeType) {
		this.gradeType = gradeType;
	}

	public String getPracticaltheoryFlag() {
		return practicaltheoryFlag;
	}

	public void setPracticaltheoryFlag(String practicaltheoryFlag) {
		this.practicaltheoryFlag = practicaltheoryFlag;
	}

	public String getYearsemesterflag() {
		return yearsemesterflag;
	}

	public void setYearsemesterflag(String yearsemesterflag) {
		this.yearsemesterflag = yearsemesterflag;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getClassOpenFlag() {
		return classOpenFlag;
	}

	public void setClassOpenFlag(String classOpenFlag) {
		this.classOpenFlag = classOpenFlag;
	}

	public String getCompulElectiveFlag() {
		return compulElectiveFlag;
	}

	public void setCompulElectiveFlag(String compulElectiveFlag) {
		this.compulElectiveFlag = compulElectiveFlag;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public Integer getSemesterType() {
		return semesterType;
	}

	public void setSemesterType(Integer semesterType) {
		this.semesterType = semesterType;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getSubGroupId() {
		return subGroupId;
	}

	public void setSubGroupId(Integer subGroupId) {
		this.subGroupId = subGroupId;
	}

	public Integer getExamNameId() {
		return examNameId;
	}

	public void setExamNameId(Integer examNameId) {
		this.examNameId = examNameId;
	}

	public String getActiveExamName() {
		return activeExamName;
	}

	public void setActiveExamName(String activeExamName) {
		this.activeExamName = activeExamName;
	}

	public String getActiveExam() {
		return activeExam;
	}

	public void setActiveExam(String activeExam) {
		this.activeExam = activeExam;
	}

	public String getCreditWithoutAttendance() {
		return creditWithoutAttendance;
	}

	public void setCreditWithoutAttendance(String creditWithoutAttendance) {
		this.creditWithoutAttendance = creditWithoutAttendance;
	}

	public String getRcCredit() {
		return rcCredit;
	}

	public void setRcCredit(String rcCredit) {
		this.rcCredit = rcCredit;
	}

	public String getWrcCredit() {
		return wrcCredit;
	}

	public void setWrcCredit(String wrcCredit) {
		this.wrcCredit = wrcCredit;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public Integer getOrderSequence() {
		return orderSequence;
	}

	public void setOrderSequence(Integer orderSequence) {
		this.orderSequence = orderSequence;
	}

	public int getAdmissionTypeId() {
		return admissionTypeId;
	}

	public void setAdmissionTypeId(int admissionTypeId) {
		this.admissionTypeId = admissionTypeId;
	}

	public String getAdmissionType() {
		return admissionType;
	}

	public void setAdmissionType(String admissionType) {
		this.admissionType = admissionType;
	}

	public Integer getExamCreditSettingId() {
		return examCreditSettingId;
	}

	public void setExamCreditSettingId(Integer examCreditSettingId) {
		this.examCreditSettingId = examCreditSettingId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getBacklogFlag() {
		return backlogFlag;
	}

	public void setBacklogFlag(String backlogFlag) {
		this.backlogFlag = backlogFlag;
	}

	public Integer getAppUserRoleId() {
		return appUserRoleId;
	}

	public void setAppUserRoleId(Integer appUserRoleId) {
		this.appUserRoleId = appUserRoleId;
	}

	public Integer getAssignSubjectId() {
		return assignSubjectId;
	}

	public void setAssignSubjectId(Integer assignSubjectId) {
		this.assignSubjectId = assignSubjectId;
	}

	public int getRcrwc() {
		return rcrwc;
	}

	public void setRcrwc(int rcrwc) {
		this.rcrwc = rcrwc;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSubGroupName() {
		return subGroupName;
	}

	public void setSubGroupName(String subGroupName) {
		this.subGroupName = subGroupName;
	}

	public String getGradeTypeStr() {
		return gradeTypeStr;
	}

	public void setGradeTypeStr(String gradeTypeStr) {
		this.gradeTypeStr = gradeTypeStr;
	}

	public List<SubjectBean> getSemesterList() {
		return semesterList;
	}

	public void setSemesterList(List<SubjectBean> semesterList) {
		this.semesterList = semesterList;
	}

	public List<SubjectBean> getExamList() {
		return ExamList;
	}

	public void setExamList(List<SubjectBean> examList) {
		ExamList = examList;
	}

	public List<SubjectBean> getAdmissionTypeList() {
		return admissionTypeList;
	}

	public void setAdmissionTypeList(List<SubjectBean> admissionTypeList) {
		this.admissionTypeList = admissionTypeList;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getYearIds() {
		return yearIds;
	}

	public void setYearIds(String yearIds) {
		this.yearIds = yearIds;
	}
	
	
}
