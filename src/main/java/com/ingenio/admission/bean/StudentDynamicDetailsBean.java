package com.ingenio.admission.bean;

public class StudentDynamicDetailsBean {
	
	private Integer studentAttachmentId;
	private String subjectId;
	private Integer attachmentId;
	private String fileName;
	private String filePath;
	private String dynamicTitle;
	private String dynamicValue;
	private String studentDocImageString;

	public StudentDynamicDetailsBean() {
		super();
	}

	public StudentDynamicDetailsBean(String subjectId) {
		super();
		this.subjectId = subjectId;
	}
	
	public StudentDynamicDetailsBean(Integer studentAttachmentId,Integer attachmentId, String fileName, String filePath) {
		super();
		this.studentAttachmentId=studentAttachmentId;
		this.attachmentId = attachmentId;
		this.fileName = fileName;
		this.filePath = filePath;
	}

	public StudentDynamicDetailsBean(String dynamicTitle, String dynamicValue) {
		super();
		this.dynamicTitle = dynamicTitle;
		this.dynamicValue = dynamicValue;
	}

	public Integer getAttachmentId() {
		return attachmentId;
	}


	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getDynamicTitle() {
		return dynamicTitle;
	}

	public void setDynamicTitle(String dynamicTitle) {
		this.dynamicTitle = dynamicTitle;
	}

	public String getDynamicValue() {
		return dynamicValue;
	}

	public void setDynamicValue(String dynamicValue) {
		this.dynamicValue = dynamicValue;
	}

	public Integer getStudentAttachmentId() {
		return studentAttachmentId;
	}

	public void setStudentAttachmentId(Integer studentAttachmentId) {
		this.studentAttachmentId = studentAttachmentId;
	}

	public String getStudentDocImageString() {
		return studentDocImageString;
	}

	public void setStudentDocImageString(String studentDocImageString) {
		this.studentDocImageString = studentDocImageString;
	}

	
	
	

	
}
