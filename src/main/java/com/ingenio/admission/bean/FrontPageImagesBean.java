package com.ingenio.admission.bean;

import java.util.List;

public class FrontPageImagesBean {

	 List<BannerImages> schoolWiseImagesList ;
	 List<BannerImages> commonImagesList ;
	 
	public List<BannerImages> getSchoolWiseImagesList() {
		return schoolWiseImagesList;
	}
	public void setSchoolWiseImagesList(List<BannerImages> schoolWiseImagesList) {
		this.schoolWiseImagesList = schoolWiseImagesList;
	}
	public List<BannerImages> getCommonImagesList() {
		return commonImagesList;
	}
	public void setCommonImagesList(List<BannerImages> commonImagesList) {
		this.commonImagesList = commonImagesList;
	}
	 
	 
	
}
