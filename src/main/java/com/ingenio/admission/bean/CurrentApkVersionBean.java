package com.ingenio.admission.bean;

public class CurrentApkVersionBean {
	private Integer androidVersionId;
	private Integer versionCode;
	private String versionName;
	private String instructions;
	private String isMandatory;
	private Integer userId;
	private Integer schoolId;
	private String date;
	private String roleName;
	private String noaction;
	private String compulsary;
	private String Notcompulsary;
	
	public Integer getAndroidVersionId() {
		return androidVersionId;
	}
	public void setAndroidVersionId(Integer androidVersionId) {
		this.androidVersionId = androidVersionId;
	}
	public Integer getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(Integer versionCode) {
		this.versionCode = versionCode;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public String getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public CurrentApkVersionBean(Integer androidVersionId,Integer schoolId, Integer versionCode, String versionName, String instructions,
			String isMandatory, String roleName ) {
		super();
		this.androidVersionId = androidVersionId;
		this.schoolId = schoolId;
		this.versionCode = versionCode;
		this.versionName = versionName;
		this.instructions = instructions;
		this.isMandatory = isMandatory;
		this.roleName = roleName;
		
	}
	
	public CurrentApkVersionBean(Integer androidVersionId, Integer versionCode, String versionName, String instructions,
			String isMandatory, String roleName ) {
		super();
		this.androidVersionId = androidVersionId;
		this.versionCode = versionCode;
		this.versionName = versionName;
		this.instructions = instructions;
		this.isMandatory = isMandatory;
		this.roleName = roleName;
		
	}
	public CurrentApkVersionBean() {
		// TODO Auto-generated constructor stub
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getNoaction() {
		return noaction;
	}
	public void setNoaction(String noaction) {
		this.noaction = noaction;
	}
	public String getCompulsary() {
		return compulsary;
	}
	public void setCompulsary(String compulsary) {
		this.compulsary = compulsary;
	}
	public String getNotcompulsary() {
		return Notcompulsary;
	}
	public void setNotcompulsary(String notcompulsary) {
		Notcompulsary = notcompulsary;
	}
	
	
}
