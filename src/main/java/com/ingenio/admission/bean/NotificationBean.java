package com.ingenio.admission.bean;

import java.util.Date;
import java.util.List;

public class NotificationBean {

	private String notificationType;
	private String title;
	private String message;
	private Date notificationDate;
	private String notificationTime;
	private List<NotificationBean>todaysList;
	private List<NotificationBean>yesterDaysList;
	private List<NotificationBean>earlierDaysList;
	private String notificationhour;
	private String dateDiffrenceValue;
	private Integer userId;
	
	public String getDateDiffrenceValue() {
		return dateDiffrenceValue;
	}
	public void setDateDiffrenceValue(String dateDiffrenceValue) {
		this.dateDiffrenceValue = dateDiffrenceValue;
	}
	public String getNotificationhour() {
		return notificationhour;
	}
	public void setNotificationhour(String notificationhour) {
		this.notificationhour = notificationhour;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getNotificationDate() {
		return notificationDate;
	}
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}
	public String getNotificationTime() {
		return notificationTime;
	}
	public void setNotificationTime(String notificationTime) {
		this.notificationTime = notificationTime;
	}
	public List<NotificationBean> getTodaysList() {
		return todaysList;
	}
	public void setTodaysList(List<NotificationBean> todaysList) {
		this.todaysList = todaysList;
	}
	public List<NotificationBean> getYesterDaysList() {
		return yesterDaysList;
	}
	public void setYesterDaysList(List<NotificationBean> yesterDaysList) {
		this.yesterDaysList = yesterDaysList;
	}
	public List<NotificationBean> getEarlierDaysList() {
		return earlierDaysList;
	}
	public void setEarlierDaysList(List<NotificationBean> earlierDaysList) {
		this.earlierDaysList = earlierDaysList;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
	
}
