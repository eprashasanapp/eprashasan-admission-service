package com.ingenio.admission.bean;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class UserDetailsBean {
	
	private Integer schoolId;
	private String schoolName;
	private String schoolSansthaKey;
	private Integer userId;
	private String roleName;
	private String userName;
	private String activeFlag;
	private String IMEICode;
	private String deviceType;
	private String appRegDate;
	private String appStartDate;
	private String appEndDate;
	private Integer currentYear;
	private String currentYearText;
	//private List<AppRoleBean> roleList;
	
	private String registartionNo;
	private Integer studentId;
	private String initialName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private String caste;
	private String category;
	private String religion;
	private String medium;
	private String mobileNumber;
	private String birthDate;
	private String birthPlace;
	private String nationality;
	private String joiningDate;
	private Integer renewId;
	private Integer staffId;
	private String designation;
	private Integer departmentId;
	private String department;
	private Integer isClassTeacher;
	private String studentPhotoUrl;
	private Integer grBookId;
	private String fatherName;
	private String motherName;
	private String standardName;
	private String divisionName;
	private Integer rollNo;
	private String schoolLogoUrl;
	private String roleNameForDisplay;
	private String rollColor;
	private Integer standardId;
	private String admissionDate;
	private String studentFormNo;
	private String mothertonge;
	private String localAddress;
	private String isOnlineAdmission;
	private String onlineAdmissionApprovalFlag;
	private Integer divisionId;
	private String userActiveOrInActiveStatus;
	private String userActiveInActiceStatusName;
	private String isOrganization;
	private Integer unseenNotificationCount;
	private List<NotificationMenuBean> menuWiseUnseenNotificationList;
	
	private List<DynamicLabelValueBean> dynamicLabelValueBeanList;
	private List<AndroidMenuBean> androidMenuList;
	private List<StandardDivisionBean>standardDivisionList;
	private AndroidMenuBean androidMenuBean;
	
	public UserDetailsBean() {
		super();
	}
	public UserDetailsBean(Integer schoolId, String schoolName,String schoolSansthaKey, Integer userId, String userName,String roleName,
			String activeFlag,String firstName,String lastName) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, IOException {
		super();
		this.schoolId = schoolId;
		this.schoolName = schoolName;
		this.userId = userId;
		this.userName = userName;
		this.roleName=roleName;
		this.activeFlag = activeFlag;
		this.firstName= firstName;
		this.lastName = lastName;
		this.schoolSansthaKey=schoolSansthaKey;
	}
	
	
	public UserDetailsBean(String registartionNo, Integer studentId, String initialName, String firstName,
			String middleName, String lastName, String gender, String caste, String category, String religion,
			String medium, String mobileNumber, String birthDate, String birthPlace, String nationality,
			String joiningDate,Integer renewId,Integer grBookId,String fatherName,String motherName,String standardName,String divisionName,
			Integer rollNo ,Integer standardId) {
		super();
		this.registartionNo = registartionNo;
		this.studentId = studentId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.medium = medium;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.renewId=renewId;
		this.grBookId=grBookId;
		this.fatherName=fatherName;
		this.motherName=motherName;
		this.standardName=standardName;
		this.divisionName=divisionName;
		this.rollNo=rollNo;
		this.standardId =standardId;
	}
	
	
	public UserDetailsBean(String registartionNo, Integer studentId, String initialName, String firstName,
			String middleName, String lastName, String gender, String caste, String category, String religion,
			String medium, String mobileNumber, String birthDate, String birthPlace, String nationality,
			String joiningDate,Integer renewId,Integer grBookId,String fatherName,String motherName,String standardName,String divisionName,
			Integer rollNo ,Integer standardId,String isonlineAdmission,String onlineAdmissionApprovalFlag) {
		super();
		this.registartionNo = registartionNo;
		this.studentId = studentId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.medium = medium;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.renewId=renewId;
		this.grBookId=grBookId;
		this.fatherName=fatherName;
		this.motherName=motherName;
		this.standardName=standardName;
		this.divisionName=divisionName;
		this.rollNo=rollNo;
		this.standardId =standardId;
		this.isOnlineAdmission = isonlineAdmission;
		this.onlineAdmissionApprovalFlag=onlineAdmissionApprovalFlag;
	}
	
	public UserDetailsBean(String registartionNo, Integer studentId, String initialName, String firstName,
			String middleName, String lastName, String gender, String caste, String category, String religion,
			String medium, String mobileNumber, String birthDate, String birthPlace, String nationality,
			String joiningDate,Integer renewId,Integer grBookId,String fatherName,String motherName,String standardName,String divisionName,
			Integer rollNo ,Integer standardId,String isonlineAdmission,String onlineAdmissionApprovalFlag,Integer renewStudentId) {
		super();
		this.registartionNo = registartionNo;
		this.studentId = studentId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.medium = medium;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.renewId=renewId;
		this.grBookId=grBookId;
		this.fatherName=fatherName;
		this.motherName=motherName;
		this.standardName=standardName;
		this.divisionName=divisionName;
		this.rollNo=rollNo;
		this.standardId =standardId;
		this.isOnlineAdmission = isonlineAdmission;
		this.onlineAdmissionApprovalFlag=onlineAdmissionApprovalFlag;
		this.renewId=renewStudentId;
	}
	
	public UserDetailsBean(String registartionNo, Integer studentId, String initialName, String firstName,
			String middleName, String lastName, String gender, String caste, String category, String religion,
			String medium, String mobileNumber, String birthDate, String birthPlace, String nationality,
			String joiningDate,Integer renewId,Integer grBookId,String fatherName,String motherName,String standardName,String divisionName,
			Integer rollNo ,Integer standardId,String isonlineAdmission,String onlineAdmissionApprovalFlag,Integer renewStudentId,Integer divisionId) {
		super();
		this.registartionNo = registartionNo;
		this.studentId = studentId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.medium = medium;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.renewId=renewId;
		this.grBookId=grBookId;
		this.fatherName=fatherName;
		this.motherName=motherName;
		this.standardName=standardName;
		this.divisionName=divisionName;
		this.rollNo=rollNo;
		this.standardId =standardId;
		this.isOnlineAdmission = isonlineAdmission;
		this.onlineAdmissionApprovalFlag=onlineAdmissionApprovalFlag;
		this.renewId=renewStudentId;
		this.divisionId = divisionId;
	}
	
	public String getOnlineAdmissionApprovalFlag() {
		return onlineAdmissionApprovalFlag;
	}
	public void setOnlineAdmissionApprovalFlag(String onlineAdmissionApprovalFlag) {
		this.onlineAdmissionApprovalFlag = onlineAdmissionApprovalFlag;
	}
	public UserDetailsBean(String registartionNo, Integer studentId, String initialName, String firstName,
			String middleName, String lastName, String gender, String caste, String category, String religion,
			String medium, String mobileNumber, String birthDate, String birthPlace, String nationality,
			String joiningDate,Integer renewId,Integer grBookId,String fatherName,String motherName,String standardName,String divisionName,
			Integer rollNo ,Integer standardId,String admissionDate,String studentFormNo,String mothertonge,String localAddress) {
		super();
		this.registartionNo = registartionNo;
		this.studentId = studentId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.medium = medium;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.renewId=renewId;
		this.grBookId=grBookId;
		this.fatherName=fatherName;
		this.motherName=motherName;
		this.standardName=standardName;
		this.divisionName=divisionName;
		this.rollNo=rollNo;
		this.standardId =standardId;
		this.admissionDate=admissionDate;
		this.studentFormNo=studentFormNo;
		this.mothertonge=mothertonge;
		this.localAddress=localAddress;
	}
	
	public UserDetailsBean(Integer userId,Integer staffId, Integer schoolId,String roleName) {
		super();
		this.schoolId = schoolId;
		this.userId = userId;
		this.roleName = roleName;
		this.staffId = staffId;
	}
	public UserDetailsBean(String registartionNo,Integer staffId, String initialName, String firstName, String middleName,
			String lastName, String gender, String caste, String category, String religion, String mobileNumber,
			String birthDate, String birthPlace,String nationality, String joiningDate, Integer departmentId,
			String department,String designation) {
		super();
		this.registartionNo = registartionNo;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.staffId = staffId;
		this.designation = designation;
		this.department = department;
		this.departmentId=departmentId;
	}
	public UserDetailsBean(String registartionNo,Integer staffId, String initialName, String firstName, String middleName,
			String lastName, String gender, String caste, String category, String religion, String mobileNumber,
			String birthDate, String birthPlace,String nationality, String joiningDate,String designation,
			String department) {
		super();
		this.registartionNo = registartionNo;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.caste = caste;
		this.category = category;
		this.religion = religion;
		this.mobileNumber = mobileNumber;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.nationality = nationality;
		this.joiningDate = joiningDate;
		this.staffId = staffId;
		this.designation = designation;
		this.department = department;
	}
	public UserDetailsBean(String registartionNo, String initialName, String firstName, String middleName,
			String lastName, String gender,String mobileNumber, String birthDate, String joiningDate) {
		super();
		this.registartionNo = registartionNo;
		this.initialName = initialName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthDate = birthDate;
		this.mobileNumber=mobileNumber;
		this.joiningDate = joiningDate;
	}
	
	
	public UserDetailsBean(String firstName, String lastName,String mobileNumber,Integer registartionNo) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber=mobileNumber;
		this.registartionNo = ""+registartionNo;
	}
	
	public UserDetailsBean(Integer userId,Integer staffId, Integer schoolId,String roleName,String userActiveOrInActiveStatus) {
		super();
		this.schoolId = schoolId;
		this.userId = userId;
		this.roleName = roleName;
		this.staffId = staffId;
		this.userActiveOrInActiveStatus=userActiveOrInActiveStatus;
	}
	
	public UserDetailsBean(Integer userId,Integer staffId, Integer schoolId,String roleName,String userActiveOrInActiveStatus,String isOrganization) {
		super();
		this.schoolId = schoolId;
		this.userId = userId;
		this.roleName = roleName;
		this.staffId = staffId;
		this.userActiveOrInActiveStatus=userActiveOrInActiveStatus;
		this.isOrganization=isOrganization;
	}

	public String getIsOnlineAdmission() {
		return isOnlineAdmission;
	}
	public void setIsOnlineAdmission(String isOnlineAdmission) {
		this.isOnlineAdmission = isOnlineAdmission;
	}
	public String getAdmissionDate() {
		return admissionDate;
	}
	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}
	public String getStudentFormNo() {
		return studentFormNo;
	}
	public void setStudentFormNo(String studentFormNo) {
		this.studentFormNo = studentFormNo;
	}
	public String getMothertonge() {
		return mothertonge;
	}
	public void setMothertonge(String mothertonge) {
		this.mothertonge = mothertonge;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getRegistartionNo() {
		return registartionNo;
	}
	public void setRegistartionNo(String registartionNo) {
		this.registartionNo = registartionNo;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getInitialName() {
		return initialName;
	}
	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getIMEICode() {
		return IMEICode;
	}
	public void setIMEICode(String iMEICode) {
		IMEICode = iMEICode;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getAppRegDate() {
		return appRegDate;
	}
	public void setAppRegDate(String appRegDate) {
		this.appRegDate = appRegDate;
	}
	public String getAppStartDate() {
		return appStartDate;
	}
	public void setAppStartDate(String appStartDate) {
		this.appStartDate = appStartDate;
	}
	public String getAppEndDate() {
		return appEndDate;
	}
	public void setAppEndDate(String appEndDate) {
		this.appEndDate = appEndDate;
	}
	public Integer getCurrentYear() {
		return currentYear;
	}
	public void setCurrentYear(Integer currentYear) {
		this.currentYear = currentYear;
	}
	public String getCurrentYearText() {
		return currentYearText;
	}
	public void setCurrentYearText(String currentYearText) {
		this.currentYearText = currentYearText;
	}
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public List<AndroidMenuBean> getAndroidMenuList() {
		return androidMenuList;
	}
	public void setAndroidMenuList(List<AndroidMenuBean> androidMenuList) {
		this.androidMenuList = androidMenuList;
	}
	public Integer getIsClassTeacher() {
		return isClassTeacher;
	}
	public void setIsClassTeacher(Integer isClassTeacher) {
		this.isClassTeacher = isClassTeacher;
	}
	public List<DynamicLabelValueBean> getDynamicLabelValueBeanList() {
		return dynamicLabelValueBeanList;
	}
	public void setDynamicLabelValueBeanList(List<DynamicLabelValueBean> dynamicLabelValueBeanList) {
		this.dynamicLabelValueBeanList = dynamicLabelValueBeanList;
	}
	public String getStudentPhotoUrl() {
		return studentPhotoUrl;
	}
	public void setStudentPhotoUrl(String studentPhotoUrl) {
		this.studentPhotoUrl = studentPhotoUrl;
	}
	public Integer getGrBookId() {
		return grBookId;
	}
	public void setGrBookId(Integer grBookId) {
		this.grBookId = grBookId;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	public String getSchoolSansthaKey() {
		return schoolSansthaKey;
	}
	public void setSchoolSansthaKey(String schoolSansthaKey) {
		this.schoolSansthaKey = schoolSansthaKey;
	}
	public String getSchoolLogoUrl() {
		return schoolLogoUrl;
	}
	public void setSchoolLogoUrl(String schoolLogoUrl) {
		this.schoolLogoUrl = schoolLogoUrl;
	}
	public String getRoleNameForDisplay() {
		return roleNameForDisplay;
	}
	public void setRoleNameForDisplay(String roleNameForDisplay) {
		this.roleNameForDisplay = roleNameForDisplay;
	}
	public String getRollColor() {
		return rollColor;
	}
	public void setRollColor(String rollColor) {
		this.rollColor = rollColor;
	}
	public List<StandardDivisionBean> getStandardDivisionList() {
		return standardDivisionList;
	}
	public void setStandardDivisionList(List<StandardDivisionBean> standardDivisionList) {
		this.standardDivisionList = standardDivisionList;
	}
	public AndroidMenuBean getAndroidMenuBean() {
		return androidMenuBean;
	}
	public void setAndroidMenuBean(AndroidMenuBean androidMenuBean) {
		this.androidMenuBean = androidMenuBean;
	}
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	public String getUserActiveOrInActiveStatus() {
		return userActiveOrInActiveStatus;
	}
	public void setUserActiveOrInActiveStatus(String userActiveOrInActiveStatus) {
		this.userActiveOrInActiveStatus = userActiveOrInActiveStatus;
	}
	public String getUserActiveInActiceStatusName() {
		return userActiveInActiceStatusName;
	}
	public void setUserActiveInActiceStatusName(String userActiveInActiceStatusName) {
		this.userActiveInActiceStatusName = userActiveInActiceStatusName;
	}
	public String getIsOrganization() {
		return isOrganization;
	}
	public void setIsOrganization(String isOrganization) {
		this.isOrganization = isOrganization;
	}
	public Integer getUnseenNotificationCount() {
		return unseenNotificationCount;
	}
	public void setUnseenNotificationCount(Integer unseenNotificationCount) {
		this.unseenNotificationCount = unseenNotificationCount;
	}
	public List<NotificationMenuBean> getMenuWiseUnseenNotificationList() {
		return menuWiseUnseenNotificationList;
	}
	public void setMenuWiseUnseenNotificationList(List<NotificationMenuBean> menuWiseUnseenNotificationList) {
		this.menuWiseUnseenNotificationList = menuWiseUnseenNotificationList;
	}
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	
	
}
