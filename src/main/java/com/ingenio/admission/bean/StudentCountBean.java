package com.ingenio.admission.bean;

public class StudentCountBean {

	private Long totalCount;
	private Long girlsCount;
	private Long boysCount;
	private String standardName;
	private Integer standardId;
	private String divisionName;
	private Integer divisionId;
	private String totalCountInWords;
	private String girlsCountInWords;
	private String boysCountInWords;
	private String standardDivisionName;
	private Integer renewId;


	public StudentCountBean() {
		super();
	}

	

	public StudentCountBean(Integer renewId) {
		super();
		this.renewId = renewId;
	}



	public StudentCountBean(String studentStdandard, Integer stdandardId, String studentDivision, Integer divisionId,
			Long totalCount, Long boysCount, Long girlsCount) {
		super();
		this.standardName = studentStdandard;
		this.standardId = stdandardId;
		this.divisionName = studentDivision;
		this.divisionId = divisionId;
		this.totalCount = totalCount;
		this.boysCount = boysCount;
		this.girlsCount = girlsCount;
	
	}

	
	
	
	public Integer getRenewId() {
		return renewId;
	}



	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}



	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getGirlsCount() {
		return girlsCount;
	}

	public void setGirlsCount(Long girlsCount) {
		this.girlsCount = girlsCount;
	}

	public Long getBoysCount() {
		return boysCount;
	}

	public void setBoysCount(Long boysCount) {
		this.boysCount = boysCount;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	
	public String getTotalCountInWords() {
		return totalCountInWords;
	}

	public void setTotalCountInWords(String totalCountInWords) {
		this.totalCountInWords = totalCountInWords;
	}

	public String getGirlsCountInWords() {
		return girlsCountInWords;
	}

	public void setGirlsCountInWords(String girlsCountInWords) {
		this.girlsCountInWords = girlsCountInWords;
	}

	public String getBoysCountInWords() {
		return boysCountInWords;
	}

	public void setBoysCountInWords(String boysCountInWords) {
		this.boysCountInWords = boysCountInWords;
	}

	public String getStandardDivisionName() {
		return standardDivisionName;
	}

	public void setStandardDivisionName(String standardDivisionName) {
		this.standardDivisionName = standardDivisionName;
	}

}
