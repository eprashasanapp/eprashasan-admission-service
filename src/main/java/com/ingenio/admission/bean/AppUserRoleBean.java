package com.ingenio.admission.bean;

public class AppUserRoleBean {

	private Integer AppUserRoleId;
	private String  RoleName;
	private String userName;
	private String password;
	private String firstName;
	private String lastName;
	
	
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAppUserRoleId() {
		return AppUserRoleId;
	}

	public void setAppUserRoleId(Integer appUserRoleId) {
		AppUserRoleId = appUserRoleId;
	}

	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
