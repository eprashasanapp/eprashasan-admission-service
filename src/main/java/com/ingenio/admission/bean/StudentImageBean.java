package com.ingenio.admission.bean;

public class StudentImageBean {
	
	
	private String fileName;
	private String filePath;
	

	public StudentImageBean() {
		super();
	}

	
	
	public StudentImageBean(String fileName, String filePath) {
		super();

		this.fileName = fileName;
		this.filePath = filePath;
	}



	public String getFileName() {
		return fileName;
	}



	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public String getFilePath() {
		return filePath;
	}



	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}



	
	
	

	
}
