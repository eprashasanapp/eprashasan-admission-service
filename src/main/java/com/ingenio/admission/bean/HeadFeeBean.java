package com.ingenio.admission.bean;

import java.util.List;

public class HeadFeeBean {

	private Integer headId;
	private String headName;
	private Double headWiseFeeTotal;
	private Double headWisePaidFeeTotal;
	private Double headWiseRemainingFeeTotal;
	private List<SubHeadBean> subHeadList;

	public Integer getHeadId() {
		return headId;
	}
	public void setHeadId(Integer headId) {
		this.headId = headId;
	}
	public String getHeadName() {
		return headName;
	}
	public void setHeadName(String headName) {
		this.headName = headName;
	}
	public Double getHeadWiseFeeTotal() {
		return headWiseFeeTotal;
	}
	public void setHeadWiseFeeTotal(Double headWiseFeeTotal) {
		this.headWiseFeeTotal = headWiseFeeTotal;
	}
	public List<SubHeadBean> getSubHeadList() {
		return subHeadList;
	}
	public void setSubHeadList(List<SubHeadBean> subHeadList) {
		this.subHeadList = subHeadList;
	}
	public Double getHeadWisePaidFeeTotal() {
		return headWisePaidFeeTotal;
	}
	public void setHeadWisePaidFeeTotal(Double headWisePaidFeeTotal) {
		this.headWisePaidFeeTotal = headWisePaidFeeTotal;
	}
	public Double getHeadWiseRemainingFeeTotal() {
		return headWiseRemainingFeeTotal;
	}
	public void setHeadWiseRemainingFeeTotal(Double headWiseRemainingFeeTotal) {
		this.headWiseRemainingFeeTotal = headWiseRemainingFeeTotal;
	}
	
}
