package com.ingenio.admission.bean;

public class FeePayTypeBean {

	private Integer payTypeId;
	private String payTypeName;
	private Integer paymentGatewayRateId;
	private String convinienceFee;
	private Double payTypeCharges;
	private Double payTypeGst;
	private Double payTypeRouteCharges;
	private Double payTypeRouteGst;
	private Double epCommission;
	private Double schoolCommission;
	private Double tpCommission; 
	private Double epCommRouteCharges;
	private Double schCommRouteCharges;
	private Double tpCommRouteCharges;
	private Double epGst;
	private Double tpGst;
	private Double schGST;
	private Double epCommRouteGst;
	private Double tpCommRouteGst;
	private Double schCommRouteGst;
	public Integer getPayTypeId() {
		return payTypeId;
	}
	public void setPayTypeId(Integer payTypeId) {
		this.payTypeId = payTypeId;
	}
	public String getPayTypeName() {
		return payTypeName;
	}
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}
	public Integer getPaymentGatewayRateId() {
		return paymentGatewayRateId;
	}
	public void setPaymentGatewayRateId(Integer paymentGatewayRateId) {
		this.paymentGatewayRateId = paymentGatewayRateId;
	}
	public String getConvinienceFee() {
		return convinienceFee;
	}
	public void setConvinienceFee(String convinienceFee) {
		this.convinienceFee = convinienceFee;
	}
	public Double getPayTypeCharges() {
		return payTypeCharges;
	}
	public void setPayTypeCharges(Double payTypeCharges) {
		this.payTypeCharges = payTypeCharges;
	}
	public Double getPayTypeGst() {
		return payTypeGst;
	}
	public void setPayTypeGst(Double payTypeGst) {
		this.payTypeGst = payTypeGst;
	}
	public Double getPayTypeRouteCharges() {
		return payTypeRouteCharges;
	}
	public void setPayTypeRouteCharges(Double payTypeRouteCharges) {
		this.payTypeRouteCharges = payTypeRouteCharges;
	}
	public Double getPayTypeRouteGst() {
		return payTypeRouteGst;
	}
	public void setPayTypeRouteGst(Double payTypeRouteGst) {
		this.payTypeRouteGst = payTypeRouteGst;
	}
	public Double getEpCommission() {
		return epCommission;
	}
	public void setEpCommission(Double epCommission) {
		this.epCommission = epCommission;
	}
	public Double getTpCommission() {
		return tpCommission;
	}
	public void setTpCommission(Double tpCommission) {
		this.tpCommission = tpCommission;
	}
	public Double getEpCommRouteCharges() {
		return epCommRouteCharges;
	}
	public void setEpCommRouteCharges(Double epCommRouteCharges) {
		this.epCommRouteCharges = epCommRouteCharges;
	}
	public Double getSchCommRouteCharges() {
		return schCommRouteCharges;
	}
	public void setSchCommRouteCharges(Double schCommRouteCharges) {
		this.schCommRouteCharges = schCommRouteCharges;
	}
	public Double getTpCommRouteCharges() {
		return tpCommRouteCharges;
	}
	public void setTpCommRouteCharges(Double tpCommRouteCharges) {
		this.tpCommRouteCharges = tpCommRouteCharges;
	}
	public Double getEpGst() {
		return epGst;
	}
	public void setEpGst(Double epGst) {
		this.epGst = epGst;
	}
	public Double getTpGst() {
		return tpGst;
	}
	public void setTpGst(Double tpGst) {
		this.tpGst = tpGst;
	}
	public Double getSchGST() {
		return schGST;
	}
	public void setSchGST(Double schGST) {
		this.schGST = schGST;
	}
	public Double getEpCommRouteGst() {
		return epCommRouteGst;
	}
	public void setEpCommRouteGst(Double epCommRouteGst) {
		this.epCommRouteGst = epCommRouteGst;
	}
	public Double getTpCommRouteGst() {
		return tpCommRouteGst;
	}
	public void setTpCommRouteGst(Double tpCommRouteGst) {
		this.tpCommRouteGst = tpCommRouteGst;
	}
	public Double getSchCommRouteGst() {
		return schCommRouteGst;
	}
	public void setSchCommRouteGst(Double schpCommRouteGst) {
		this.schCommRouteGst = schpCommRouteGst;
	}
	public Double getSchoolCommission() {
		return schoolCommission;
	}
	public void setSchoolCommission(Double schoolCommission) {
		this.schoolCommission = schoolCommission;
	}
}
