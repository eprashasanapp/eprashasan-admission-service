package com.ingenio.admission.bean;

public class SchoolDetailBean {

	
	private Integer schoolId;
	private String schoolName;
	private String groupofSachoolName;
	private String userName;
	private String principalName;
	private String photoUrl;
	private String regNo;
	private String schoolAddress;
	private String schoolKey;

	public SchoolDetailBean( Integer schoolId,String schoolName, String groupofSachoolName,String userName,String principalName ,
			String regNo,String schoolAddress,String schoolKey) {
		super();
		this.schoolId = schoolId;
		this.schoolName = schoolName;
		this.groupofSachoolName = groupofSachoolName;		
		this.userName=userName;
		this.principalName=principalName;
        this.regNo=regNo;
        this.schoolAddress=schoolAddress;
        this.schoolKey=schoolKey;
	}
	
	
	public SchoolDetailBean( Integer schoolId,String schoolName) {
		super();
		this.schoolId = schoolId;
		this.schoolName = schoolName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getGroupofSachoolName() {
		return groupofSachoolName;
	}

	public void setGroupofSachoolName(String groupofSachoolName) {
		this.groupofSachoolName = groupofSachoolName;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPrincipalName() {
		return principalName;
	}

	public void setPrincipalName(String principalName) {
		this.principalName = principalName;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getSchoolAddress() {
		return schoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		this.schoolAddress = schoolAddress;
	}

	public String getSchoolKey() {
		return schoolKey;
	}

	public void setSchoolKey(String schoolKey) {
		this.schoolKey = schoolKey;
	}
}
