package com.ingenio.admission.bean;

import java.math.BigInteger;

public class SubHeadBean {

	private Integer subheadId;
	private String subHeadName;
	private Double fee;
	private Double paidFee;
	private Double remainingFee;
	private Double paidFine;
	private Double remainingFine;
	private BigInteger feeSetMultiID;
	private Integer studFeeID;
	
	public Integer getSubheadId() {
		return subheadId;
	}
	public void setSubheadId(Integer subheadId) {
		this.subheadId = subheadId;
	}
	public String getSubHeadName() {
		return subHeadName;
	}
	public void setSubHeadName(String subHeadName) {
		this.subHeadName = subHeadName;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	public Double getPaidFee() {
		return paidFee;
	}
	public void setPaidFee(Double paidFee) {
		this.paidFee = paidFee;
	}
	public Double getRemainingFee() {
		return remainingFee;
	}
	public void setRemainingFee(Double remainingFee) {
		this.remainingFee = remainingFee;
	}
	public BigInteger getFeeSetMultiID() {
		return feeSetMultiID;
	}
	public void setFeeSetMultiID(BigInteger feeSetMultiID) {
		this.feeSetMultiID = feeSetMultiID;
	}
	public Integer getStudFeeID() {
		return studFeeID;
	}
	public void setStudFeeID(Integer studFeeID) {
		this.studFeeID = studFeeID;
	}
	public Double getPaidFine() {
		return paidFine;
	}
	public void setPaidFine(Double paidFine) {
		this.paidFine = paidFine;
	}
	public Double getRemainingFine() {
		return remainingFine;
	}
	public void setRemainingFine(Double remainingFine) {
		this.remainingFine = remainingFine;
	}
 }
