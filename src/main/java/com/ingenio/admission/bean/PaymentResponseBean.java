package com.ingenio.admission.bean;

public class PaymentResponseBean {
	

	private Integer schoolSansthaId;
	private String schoolSansthaName;
	private String  schoolSansthaKey;
	private Integer paymentGatewayRateId;
	private String  paymentType;
	private Double amount;
	private Integer gstPercentage;
	private String charge;
	private String compareType;
	private String amountPercentFlag;
	private Integer paymentGateWayId;
	private String gatewayName;
	private String gatewayDescription;
	private Integer scRoutingMasterId;
	private String accountId;
	private String commissionType;
	

	public PaymentResponseBean() {
		super();
	}
	
	public String getAmountPercentFlag() {
		return amountPercentFlag;
	}
	public void setAmountPercentFlag(String amountPercentFlag) {
		this.amountPercentFlag = amountPercentFlag;
	}
	public Integer getSchoolSansthaId() {
		return schoolSansthaId;
	}
	public void setSchoolSansthaId(Integer schoolSansthaId) {
		this.schoolSansthaId = schoolSansthaId;
	}
	public String getSchoolSansthaName() {
		return schoolSansthaName;
	}
	public void setSchoolSansthaName(String schoolSansthaName) {
		this.schoolSansthaName = schoolSansthaName;
	}
	public String getSchoolSansthaKey() {
		return schoolSansthaKey;
	}
	public void setSchoolSansthaKey(String schoolSansthaKey) {
		this.schoolSansthaKey = schoolSansthaKey;
	}
	public Integer getPaymentGatewayRateId() {
		return paymentGatewayRateId;
	}
	public void setPaymentGatewayRateId(Integer paymentGatewayRateId) {
		this.paymentGatewayRateId = paymentGatewayRateId;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Integer getGstPercentage() {
		return gstPercentage;
	}
	public void setGstPercentage(Integer gstPercentage) {
		this.gstPercentage = gstPercentage;
	}
	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public Integer getPaymentGateWayId() {
		return paymentGateWayId;
	}
	public void setPaymentGateWayId(Integer paymentGateWayId) {
		this.paymentGateWayId = paymentGateWayId;
	}
	public String getGatewayName() {
		return gatewayName;
	}
	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}
	public String getGatewayDescription() {
		return gatewayDescription;
	}
	public void setGatewayDescription(String gatewayDescription) {
		this.gatewayDescription = gatewayDescription;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}

	public String getCompareType() {
		return compareType;
	}

	public void setCompareType(String compareType) {
		this.compareType = compareType;
	}

	public Integer getScRoutingMasterId() {
		return scRoutingMasterId;
	}

	public void setScRoutingMasterId(Integer scRoutingMasterId) {
		this.scRoutingMasterId = scRoutingMasterId;
	}
}
