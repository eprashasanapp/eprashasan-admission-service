package com.ingenio.admission.bean;

public class CommonUtilityBean {

	private Integer yearId;
	private String yearName;
	private String standardId;
	private String standardName;
	private String categoryId;
	private String categoryName;
	private String casteId;
	private String casteName;
	private String minorityId;
	private String minorityType;
	private String religionId;
	private String religionName;
	private String divisionId;
	private String divisionName;
	private String grBookId;
	private String grBookName;
	private String attachmentId;
	private String attachmentName;
	private String flag;
	private byte[] imageData;
	private String otp;
	private Integer subjectId;
	private String subjectName;
	private Integer subjectCategoryId;
	private String subjectCategoryName;
	private String mobileNo;
	private String fromMonth;
	private String letterHeadImg;
	private String currentAcademicYear;
	
	public String getStandardId() {
		return standardId;
	}
	public void setStandardId(String standardId) {
		this.standardId = standardId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public String getYearName() {
		return yearName;
	}
	public void setYearName(String yearName) {
		this.yearName = yearName;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCasteId() {
		return casteId;
	}
	public void setCasteId(String casteId) {
		this.casteId = casteId;
	}
	public String getCasteName() {
		return casteName;
	}
	public void setCasteName(String casteName) {
		this.casteName = casteName;
	}
	public String getMinorityId() {
		return minorityId;
	}
	public void setMinorityId(String minorityId) {
		this.minorityId = minorityId;
	}
	public String getMinorityType() {
		return minorityType;
	}
	public void setMinorityType(String minorityType) {
		this.minorityType = minorityType;
	}
	public String getReligionId() {
		return religionId;
	}
	public void setReligionId(String religionId) {
		this.religionId = religionId;
	}
	public String getReligionName() {
		return religionName;
	}
	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}
	public String getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public String getGrBookId() {
		return grBookId;
	}
	public void setGrBookId(String grBookId) {
		this.grBookId = grBookId;
	}
	public String getGrBookName() {
		return grBookName;
	}
	public void setGrBookName(String grBookName) {
		this.grBookName = grBookName;
	}
	public String getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public byte[] getImageData() {
		return imageData;
	}
	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Integer getSubjectCategoryId() {
		return subjectCategoryId;
	}
	public void setSubjectCategoryId(Integer subjectCategoryId) {
		this.subjectCategoryId = subjectCategoryId;
	}
	public String getSubjectCategoryName() {
		return subjectCategoryName;
	}
	public void setSubjectCategoryName(String subjectCategoryName) {
		this.subjectCategoryName = subjectCategoryName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getFromMonth() {
		return fromMonth;
	}
	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}
	public String getLetterHeadImg() {
		return letterHeadImg;
	}
	public void setLetterHeadImg(String letterHeadImg) {
		this.letterHeadImg = letterHeadImg;
	}
	public String getCurrentAcademicYear() {
		return currentAcademicYear;
	}
	public void setCurrentAcademicYear(String currentAcademicYear) {
		this.currentAcademicYear = currentAcademicYear;
	}
	
	
}
