package com.ingenio.admission.bean;

import java.util.List;

public class FCMUpdateBean {

	private String mobileNo;
	private String fcmToken;
	private List<FCMUpdateInnerBean> innerDetails;

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public List<FCMUpdateInnerBean> getInnerDetails() {
		return innerDetails;
	}

	public void setInnerDetails(List<FCMUpdateInnerBean> innerDetails) {
		this.innerDetails = innerDetails;
	}

}
