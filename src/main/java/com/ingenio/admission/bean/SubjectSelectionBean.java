package com.ingenio.admission.bean;

import java.util.List;

public class SubjectSelectionBean {

	private Integer studentId;
	private Integer standardId;
	private Integer yearId;
	private List<SubjectBean> cumpolsrySubjectList;
	private List<ElectiveGroupBean> electiveGroupBean;
	private List<StudentDynamicDetailsBean> subjectsForEdit;
	private int totalSubjects;
	
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public List<SubjectBean> getCumpolsrySubjectList() {
		return cumpolsrySubjectList;
	}
	public void setCumpolsrySubjectList(List<SubjectBean> cumpolsrySubjectList) {
		this.cumpolsrySubjectList = cumpolsrySubjectList;
	}
	public List<ElectiveGroupBean> getElectiveGroupBean() {
		return electiveGroupBean;
	}
	public void setElectiveGroupBean(List<ElectiveGroupBean> electiveGroupBean) {
		this.electiveGroupBean = electiveGroupBean;
	}
	public int getTotalSubjects() {
		return totalSubjects;
	}
	public void setTotalSubjects(int totalSubjects) {
		this.totalSubjects = totalSubjects;
	}
	public List<StudentDynamicDetailsBean> getSubjectsForEdit() {
		return subjectsForEdit;
	}
	public void setSubjectsForEdit(List<StudentDynamicDetailsBean> subjectsForEdit) {
		this.subjectsForEdit = subjectsForEdit;
	}
}
