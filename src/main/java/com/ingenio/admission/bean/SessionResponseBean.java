package com.ingenio.admission.bean;

public class SessionResponseBean {
	
	private Integer schoolId;
	private Integer renewId;
	private Integer yearId;
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getRenewId() {
		return renewId;
	}
	public void setRenewId(Integer renewId) {
		this.renewId = renewId;
	}
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	public SessionResponseBean( Integer renewId,Integer schoolId, Integer yearId) {
		super();
		this.schoolId = schoolId;
		this.renewId = renewId;
		this.yearId = yearId;
	}
	
	

}
