package com.ingenio.admission.bean;

import java.util.List;

public class HomeScreenBean {

	private String flag;
	private String iconUrl;
	private String title;
	private String completedTitle;
	private String completedCount;
	private String remainingTitle;
	private String remainingCount;
	private String message;
	private String completedColor;
	private String remainingColor;
	private String type;
	private String date;
	private String postedBy;
	private String postedByImgUrl;
	private String description;
	private String postedbyRole;
	private List<String> iconUrlList;
	
	private String message1;

	
	public HomeScreenBean() {
		super();
	
	}
	public HomeScreenBean(String flag, String title, String completedTitle, String remainingTitle, String message,
			String completedColor, String remainingColor, String type) {
		super();
		this.flag = flag;
		this.title = title;
		this.completedTitle = completedTitle;
		this.remainingTitle = remainingTitle;
		this.message = message;
		this.completedColor = completedColor;
		this.remainingColor = remainingColor;
		this.type = type;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCompletedTitle() {
		return completedTitle;
	}
	public void setCompletedTitle(String completedTitle) {
		this.completedTitle = completedTitle;
	}
	public String getCompletedCount() {
		return completedCount;
	}
	public void setCompletedCount(String completedCount) {
		this.completedCount = completedCount;
	}
	public String getRemainingTitle() {
		return remainingTitle;
	}
	public void setRemainingTitle(String remainingTitle) {
		this.remainingTitle = remainingTitle;
	}
	public String getRemainingCount() {
		return remainingCount;
	}
	public void setRemainingCount(String remainingCount) {
		this.remainingCount = remainingCount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCompletedColor() {
		return completedColor;
	}
	public void setCompletedColor(String completedColor) {
		this.completedColor = completedColor;
	}
	public String getRemainingColor() {
		return remainingColor;
	}
	public void setRemainingColor(String remainingColor) {
		this.remainingColor = remainingColor;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPostedBy() {
		return postedBy;
	}
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	public String getPostedByImgUrl() {
		return postedByImgUrl;
	}
	public void setPostedByImgUrl(String postedByImgUrl) {
		this.postedByImgUrl = postedByImgUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPostedbyRole() {
		return postedbyRole;
	}
	public void setPostedbyRole(String postedbyRole) {
		this.postedbyRole = postedbyRole;
	}
	public List<String> getIconUrlList() {
		return iconUrlList;
	}
	public void setIconUrlList(List<String> iconUrlList) {
		this.iconUrlList = iconUrlList;
	}
	public String getMessage1() {
		return message1;
	}
	public void setMessage1(String message1) {
		this.message1 = message1;
	}
	

	
}
