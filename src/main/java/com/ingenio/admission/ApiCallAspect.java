package com.ingenio.admission;

import java.util.HashMap; 
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class ApiCallAspect {

//	System.out.println();
	
	private Map<String, Integer> apiCallCounts = new HashMap<>();

	
	
//    @Pointcut("execution(* your.package.name.*Controller.*(..))")
	@Pointcut("execution(* com.ingenio.admission.controller.*Controller.*(..))")
//	@Pointcut("execution(com.ingenio.admission.UserRoleController.*(..))")
    public void apiPointcut() {}

    @AfterReturning(pointcut = "apiPointcut()", returning = "result")
    public void countApiCalls(Object result) {
        String endpoint = getEndpoint();
        apiCallCounts.put(endpoint, apiCallCounts.getOrDefault(endpoint, 0) + 1);
        System.out.println("API called: " + endpoint + ". Total calls: " + apiCallCounts.get(endpoint));
        System.out.println("\n ");
    }

    private String getEndpoint() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        return request.getRequestURI(); 
    }
}
