package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.BonafideTCImageModel;

@Repository
public interface BonafideTCImgRepository extends JpaRepository<BonafideTCImageModel, Integer> {

	@Query(value = "select a from BonafideTCImageModel a where a.schoolMasterModel.schoolid=?1 and a.isDel='0' and a.flag='1' ")
	List<BonafideTCImageModel> findLetterHeadImage(Integer schoolId);


}
