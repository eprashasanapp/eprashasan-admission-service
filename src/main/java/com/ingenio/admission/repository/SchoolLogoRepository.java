package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.SchoolLogo;

public interface SchoolLogoRepository  extends JpaRepository<SchoolLogo, Integer>{
	@Query("select COALESCE(a.imagePath,'abc') from SchoolLogo a where a.schoolId=:schoolId and a.isDel='0'  ")
	String getSchoolLogoPath(Integer schoolId);
}
