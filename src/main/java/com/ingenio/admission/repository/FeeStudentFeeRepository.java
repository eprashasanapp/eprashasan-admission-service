package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeeStudentFeeModel;

public interface FeeStudentFeeRepository extends JpaRepository<FeeStudentFeeModel, Integer> {

}
