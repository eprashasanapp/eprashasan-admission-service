package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.DeviceCommunicationBean;
import com.ingenio.admission.model.AttendanceRFIDPunchModel;

@Repository
public interface AttendanceRFIDPunchRepository extends JpaRepository<AttendanceRFIDPunchModel, Integer>{

	@Query("SELECT new com.ingenio.admission.bean.DeviceCommunicationBean(a.rfid, a.attendanceRfid, a.dateOfTransection) "
			+ "FROM AttendanceRFIDPunchModel a "
			+ "WHERE a.schoolId = :orgId AND "
			+ "(str_to_date(a.dateOfTransection, '%Y-%m-%d') = STR_TO_DATE(:date, '%Y-%m-%d'))")
	List<DeviceCommunicationBean> getAttendanceRfidDataBySchoolId(Integer orgId, String date);
//	void getAttendanceRfidDataBySchoolId(Integer orgId, String fromDate, String toDate);

	@Query("SELECT new com.ingenio.admission.bean.DeviceCommunicationBean(a.rfid, a.attendanceRfid, a.dateOfTransection) "
			+ "FROM AttendanceRFIDPunchModel a "
			+ "WHERE a.schoolId = :orgId AND "
			+ "(str_to_date(a.dateOfTransection, '%Y-%m-%d') >= STR_TO_DATE(:fromDate, '%Y-%m-%d')) "
			+ "AND (str_to_date(a.dateOfTransection, '%Y-%m-%d') <= STR_TO_DATE(:toDate, '%Y-%m-%d')) ")
	List<DeviceCommunicationBean> getAttendanceRfidDataBySchoolId(Integer orgId, String fromDate, String toDate);
	
}
