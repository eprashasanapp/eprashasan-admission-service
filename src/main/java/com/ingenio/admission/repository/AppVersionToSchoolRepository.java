package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.AppVersionToSchoolModel;


public interface AppVersionToSchoolRepository extends JpaRepository< AppVersionToSchoolModel, Integer>{

	
	@Query("select a.schoolMasterModel.schoolid from AppVersionToSchoolModel a where a.appVersionMasterModel.versionID=:versionID  "
			+ "and a.schoolMasterModel.schoolid=:schoolId")
	public Integer findSchool(Integer versionID, Integer schoolId);

	
}
