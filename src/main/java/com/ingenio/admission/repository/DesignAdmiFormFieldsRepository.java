package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.DesignAdmiFormFieldsModel;


@Repository
public interface DesignAdmiFormFieldsRepository extends JpaRepository<DesignAdmiFormFieldsModel, Integer> {
	
	@Query(value = "select c from DesignAdmiFormFieldsModel c WHERE c.schoolMasterModel.schoolid=?1 and c.isDel='0' ")
	List<DesignAdmiFormFieldsModel> getStaticTabsForAdmissionForm(Integer schoolId);

	@Query(value = "select c from DesignAdmiFormFieldsModel c WHERE c.isStaticOrDynamic='2' and c.schoolMasterModel.schoolid=?1 and c.isDel='0' ")
	List<DesignAdmiFormFieldsModel> getAllOnlyOldDynamicFeildsNames(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.fieldId),0)+1 from DesignAdmiFormFieldsModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
