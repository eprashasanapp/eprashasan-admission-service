package com.ingenio.admission.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.StudentAdmissionBean;
import com.ingenio.admission.bean.SubjectBean;

@Repository
public class PrintExcelStudentRepositoryImpl {
	 @PersistenceContext
	  private EntityManager entitymanager;
		
	 @SuppressWarnings({ "unchecked", "deprecation" })
		public List<StudentAdmissionBean> getExeFolderPath() {
		 StringBuffer sql=new StringBuffer();
			sql.append(" select exePath as FirstName from ingenio_setup.exe_path  ");
			
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<StudentAdmissionBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(StudentAdmissionBean.class ) )
											 .getResultList();
			return list;	
		}
	 
		@SuppressWarnings({ "unchecked", "deprecation" })
		public List<StudentAdmissionBean>  getstudValues(String studRegNo,Integer schoolId) {
			StringBuffer sql=new StringBuffer();
			sql.append(" select IFNULL(b.nationlity,'') as nationality,IFNULL(b.fIncome,'') as income, IFNULL(b.fEducation,'') as education,CAST(IFNULL(b.stud_id,'') as char) as studId, cast(IFNULL(a.rollNo,'') as char) as rollNo, IFNULL(b.Student_RegNo,'') as RegistrationNumber, concat(Ifnull(b.studInitialName,''),' ',Ifnull(b.studLName,''),' ',b.studFName,' ',Ifnull(b.studMName,'')) as FirstName, ");
			sql.append(" IFNULL(c.religionName1,'') as religionName, IFNULL(d.categoryName,'') as categoryName, IFNULL(e.cast_name,'') as casteName, IFNULL(b.stud_gender,'') as Gender, IFNULL(b.motherName,'') as MotherName, IFNULL(b.fatherName,'') as FatherName, IFNULL(b.birth_date,'') as BirthDate, ");
			sql.append(" IFNULL(i.year,'') as AcademicYearText, IFNULL(f.standardName,'') as standardName, IFNULL(g.divisionName,'') as divName, IFNULL(b.mMobile,'') as mMobile, IFNULL(b.pAddress,'') as PermanentAddress, IFNULL(b.mTelephone,'') as fMobile, IFNULL(b.fOccupation,'') as Occupation, ");
			sql.append(" Ifnull(h.textbox1,'') as textbox1, Ifnull(h.textbox2,'') as textbox2, Ifnull(h.textbox3,'') as textbox3, Ifnull(h.textbox4,'') as textbox4, Ifnull(h.textbox5,'') as textbox5, ");
			sql.append(" Ifnull(h.combobox1,'') as combobox1,Ifnull( h.combobox2,'') as combobox2, Ifnull(h.combobox3,'') as combobox3, Ifnull(h.combobox4,'') as combobox4, Ifnull(h.combobox5,'') as combobox5,a.renewstudentId as studentRenewId, ");
			sql.append(" IFNULL(b.studLName,'') as studLName,IFNULL(b.studFName,'') as studFName,IFNULL(b.mothertonge,'') as Mothertongue,IFNULL(b.birth_place,'') as BirthPlace,IFNULL(b.pState,'') as PermanentState,IFNULL(b.pPincode,'') as PermanentPinCode,IFNULL(b.lState,'') as LocalState,IFNULL(b.lPincode,'') as LocalPinCode,IFNULL(b.lAddress,'') AS LocalAddress, ");
			sql.append(" IFNULL(b.fEmail,'') AS ParentEmailID,IFNULL(b.mEmail,'') AS EmailID,IFNULL(b.mTelephone,'') AS AadhaarCardNo, ");
			sql.append(" IFNULL(b.mEducation,'') AS BankName,IFNULL(b.mOccupation,'') AS BranchName,IFNULL(b.mAge,'') AS BankIFSCNo,IFNULL(b.mIncome,'') AS AccountNo,IFNULL(b.Student_formNo,'') AS FormNumber,b.userId  as userId,b.medium as medium ");
			sql.append(" from student_standard_renew a ");
			sql.append(" left outer join student_master b on b.stud_id=a.studentId and b.isDel='0' and b.schoolid='"+schoolId+"' and b.isAdmissionCancelled='1' ");
			sql.append(" left outer join religion_master_1 c on c.religionMasterId1=b.religionId and c.isDel='0' and c.schoolid='"+schoolId+"'");
			sql.append(" left outer join category d on d.categoryId=b.categoryId and d.isDel='0'  and d.schoolid='"+schoolId+"' ");
			sql.append(" left outer join castemaster e on e.casteId=b.caste_id and e.isDel='0' and e.schoolid='"+schoolId+"' ");
			sql.append(" left outer join standardmaster f on f.standardId=a.standardId and f.isDel='0' and f.schoolid='"+schoolId+"' ");
			sql.append(" left outer join division g on g.divisionId=a.divisionId and g.isDel='0' and g.schoolid='"+schoolId+"' ");
			sql.append(" left outer join dynamicfieldstudinformation h on h.studId=b.stud_id and h.isDel='0' and h.schoolid='"+schoolId+"' ");
			sql.append(" left outer join yearmaster i on i.yearId=a.yearId and i.isDel='0' and i.schoolid='"+schoolId+"' ");
			sql.append(" left outer join app_user_role j ON j.APPUSERROLEID=a.userId AND j.schoolid='"+schoolId+"' ");
			sql.append(" where a.schoolid='"+schoolId+"' and a.isDel='0' and b.Student_RegNo ='"+studRegNo+"'  ");
			sql.append(" GROUP BY b.Student_RegNo ");
			sql.append(" order by a.rollNo");
			
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<StudentAdmissionBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(StudentAdmissionBean.class ) )
											 .getResultList();
			return list;
		}
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		public List<StudentAdmissionBean>  getNewDynamicFieldsValues(String studRegNo,Integer schoolId, String studentId) {
			StringBuffer sql=new StringBuffer();
			sql.append(" select a.fieldName as FirstName,IFNULL(b.dynamicValue,'') as Gender from design_admi_form_tabs_multi a  ");
			sql.append(" left join student_dynamic_details b on b.dynamicTitle=a.fieldName and b.schoolID='"+schoolId+"' AND b.isDel='0' ");
			sql.append(" where a.schoolID='"+schoolId+"' AND a.isDel='0' and a.isStaticOrDynamic='1' and b.studentId="+Integer.parseInt(studentId)+" ");
			sql.append(" GROUP BY a.fieldId ");
			sql.append(" order by a.fieldId");
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<StudentAdmissionBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(StudentAdmissionBean.class ) )
											 .getResultList();
			return list;
		}
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		public List<StudentAdmissionBean>  getAssignedSubjectToStudent(String studRegNo,Integer schoolId,String yearComboVal,String std) {
			StringBuffer sql=new StringBuffer();
			sql.append(" select b.subject_name as FirstName from assign_subject_to_student a  ");
			sql.append(" left join subjects b on b.subject_id=a.subject_id and b.schoolid='"+schoolId+"' and b.isDel=0 ");
			sql.append(" left join student_master c on c.stud_id=a.student_id and c.schoolid='"+schoolId+"' and c.isDel=0  and c.isAdmissionCancelled='1' ");
			sql.append(" where a.schoolid='"+schoolId+"' and a.isDel=0 and c.Student_RegNo='"+studRegNo+"'  ");
			sql.append(" GROUP BY a.subject_id ");
			sql.append(" order by a.subject_id");
			
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<StudentAdmissionBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(StudentAdmissionBean.class ) )
											 .getResultList();
			return list;
		}

		public List<SubjectBean> getSubjectListForPrint(Integer schoolId, String studId) {
			StringBuffer sql=new StringBuffer();
			sql.append("select g.subject_name as subjectName,g.course_code as courseCode,b.year as yearIds,k.standardName as standardName,d.semesterName as semester,h.activeExamName as activeExamName,e.groupName as groupName,f.subGroupName as subGroupName,cast(IFNULL(j.rcCredit,0) as char) AS rcCredit, cast(IFNULL(j.wrcCredit,0) as char) as wrcCredit,a.rc_rwc as rcrwc,cast(a.regular_backlog as char) as backlogFlag,cast(g.is_eligible_grace as char) as iseligiblegrace,cast(g.is_eligible_Grade as char) as iseligibleGrade,cast(g.status as char) status,ifnull(g.grade_type,'') as gradeType,g.practicaltheoryFlag as practicaltheoryFlag,g.credit as credit,ifnull(g.credit_w_attendance,'') as creditWithoutAttendance,g.compulElectiveFlag as compulElectiveFlag,g.yearsemesterflag as yearsemesterflag ");
			sql.append("from assign_subject_to_student a  ");   
			sql.append("left join subjects g on g.subject_id = a.subject_id  ");   
			sql.append("left join yearmaster b on g.year=b.yearId   ");   
			sql.append("left join exam_semester_master d on g.semesterId=d.semesterId  ");   
			sql.append("left join exam_group_properties e on g.groupId=e.groupId  ");   
			sql.append("left join exam_subgroup_properties f on g.subGroupId=f.subGroupId  ");   
			sql.append("left join exam_examname h on a.examNameId=h.examNameId  ");   
			sql.append("left join exam_credit_setting j on j.examNameId=h.examNameId  ");   
			sql.append("left join standardmaster k on g.standard=k.standardId  ");   
			sql.append("where a.schoolid='"+schoolId+"' and a.isDel='0' and a.student_id='"+studId+"'  ");   
			
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<SubjectBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(SubjectBean.class ) )
											 .getResultList();
			return list;
		}

		public List<StudentAdmissionBean> getInputList(String globalCertificateId, Integer schoolId, Integer serialNo) {
			StringBuffer sql=new StringBuffer();
			sql.append("select a.inputName as inputName,b.inputData as inputData,c.certificate_name as certificateName   ");
			sql.append("from certificate_type_user_input a "); 
			sql.append("left join certificate_user_input_data b on a.userInputId=b.userInputId left join certificate_global_type_master c on c.globalCertificateId=a.globalCertificateId ");
			sql.append("left join certificate_serial_no d on b.certificateSerialNoId=d.certificateNoId ");
			sql.append("where a.globalCertificateId="+globalCertificateId+" and d.certificate_srNo="+serialNo+" ");
			
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<StudentAdmissionBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(StudentAdmissionBean.class ) )
											 .getResultList();
			return list;
		}

		public List<StudentAdmissionBean> getSerialNo(Integer studentRenewId, Integer schoolId, String globalCertificateId) {
			StringBuffer sql=new StringBuffer();
			sql.append("select max(a.certificate_srNo) as serialNo from certificate_serial_no a    ");
			sql.append("left join certificate_type_master b on a.certificateTypeId=b.certificateTypeMasterId "); 
			sql.append("where a.schoolid='"+schoolId+"' and a.studRenewId='"+studentRenewId+"' and b.globalCertificateId="+globalCertificateId+" "); 
			
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<StudentAdmissionBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(StudentAdmissionBean.class ) )
											 .getResultList();
			return list;
		}

		public Integer getExcelSchoolType(Integer schoolId, String certificateName) {
			try{
				StringBuffer qu=new StringBuffer();
				qu.append("select a.cerExcelSchoolWiseId as studentRenewId ");
				qu.append("from certificate_excel_schoolwise a ");
				qu.append("left join certificate_type_master b on a.certificateTypeMasterId=b.certificateTypeMasterId ");
				qu.append("where a.schoolid="+schoolId+" and b.certificate_type='"+certificateName+"' ");
				Query query = entitymanager.createNativeQuery(qu.toString());
				List<StudentAdmissionBean> list = query.unwrap( org.hibernate.query.Query.class )
												 .setResultTransformer(Transformers.aliasToBean(StudentAdmissionBean.class ) )
												 .getResultList();
				if(CollectionUtils.isNotEmpty(list)){
					return Integer.parseInt(""+list.get(0));
				}			
			} catch(Exception e) {
				e.printStackTrace();
			}
			return 0;
		}

		public List getAdmissionExcelFilePath(Integer yearId, int standardId, Integer schoolId, String roasterFlag) {
			// TODO Auto-generated method stub
			try {
				String queryStr="SELECT a.filePath as filePath FROM img_addmission_form_format_excel a WHERE "
						+ "a.yearId="+yearId+" AND a.isDel='0' and a.standardId="+standardId+" and a.schoolid="+schoolId+" and a.roasterFormFlag="+roasterFlag+" ";
				Query query = entitymanager.createNativeQuery(queryStr);
				return query.getResultList();
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}

}
