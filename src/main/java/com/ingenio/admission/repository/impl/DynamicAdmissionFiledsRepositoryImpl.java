package com.ingenio.admission.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.StudentDetailsBean;

@Repository
public class DynamicAdmissionFiledsRepositoryImpl {
  
  @PersistenceContext
  private EntityManager entitymanager;
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<AdmissionSettingBean>  getComboValues(Integer schoolId, Integer fieldId, String fieldtype) {
		String id = fieldtype.substring(8);
		Query query = entitymanager.createNativeQuery("Select a.comboId"+id+" as dynamicFieldId ,a.comboValue as fieldValue from dynamic"+fieldtype+" a where a.schoolid='"+schoolId+"' and a.fieldId='"+fieldId+"' ");
		List<AdmissionSettingBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(AdmissionSettingBean.class ) )
										 .getResultList();
		
		
		return list;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<StudentDetailsBean> getStudentList(Integer schoolId, String searchText) {
		// TODO Auto-generated method stub
		List<StudentDetailsBean> studentMasterBeans = new ArrayList<>();
		try {
			StringBuffer getStudentNameQuery = new StringBuffer();
//			getStudentNameQuery.append("select a.stud_id as studentId,concat(Ifnull(a.studInitialName,''),' ',a.studFName,' ',a.studMName,' ',a.studLName,'-',c.standardName,'') as studentName, ");
			getStudentNameQuery.append("select a.stud_id as studentId,concat(Ifnull(a.studInitialName,''),' ',Ifnull(a.studFName,''),' ',Ifnull(a.studMName,''),' ',Ifnull(a.studLName,''),'-',Ifnull(c.standardName,''),'') as studentName, ");
			getStudentNameQuery.append("b.renewstudentId as renewStudentId from student_master a ");
			getStudentNameQuery.append("left outer join student_standard_renew b on b.studentId=a.stud_id and b.isDel='0' and b.schoolid='"+schoolId+"'  ");
			getStudentNameQuery.append("left outer join standardmaster c on c.standardId=b.standardId and c.isDel='0' and c.schoolid='"+schoolId+"'  ");
			getStudentNameQuery.append("where b.schoolid='"+schoolId+"' and b.isDel='0' and b.alreadyRenew = 1 and a.isAdmissionCancelled='1' and a.isApproval='0' and CONCAT(IFNULL(a.studInitialName,''),' ',ifnull(a.studLName,''),' ',ifnull(a.studFName,''),' ',ifnull(a.studMName,'')) LIKE CONCAT('%','"+searchText+"','%') group by a.stud_id");
			Query query = entitymanager.createNativeQuery(getStudentNameQuery.toString());

			studentMasterBeans =query.unwrap( org.hibernate.query.Query.class )
			 .setResultTransformer(Transformers.aliasToBean(StudentDetailsBean.class ) )
			 .getResultList();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return studentMasterBeans;
	}
}
