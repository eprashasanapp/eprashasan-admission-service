package com.ingenio.admission.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.transform.Transformers;

import com.ingenio.admission.bean.AndroidMenuBean;
import com.ingenio.admission.bean.NotificationBean;
import com.ingenio.admission.bean.StandardDivisionBean;


public class FCMPushNotificationRepositoryImpl {

	 
	  @PersistenceContext
	  private EntityManager entitymanager;
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		public List<NotificationBean> getAllNotificationsForStudents(Integer renewId, String role, Integer offset) {
			StringBuffer notifyquery = new StringBuffer();
			notifyquery.append("select * from ( " ) 
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a.notificationDate) as char) AS dateDiffrenceValue, a.notificationType ,a.title,a.message,a.notificationDate,a.notificationTime ")
					.append("from android_notification_status a ") 
					.append("left join student_homework_assign_to c on c.assignHomeworkId=a.joinId ")
					.append("where c.renewStudentId='"+renewId+"'  and a.notificationType='HomeWork' and  DATEDIFF(CURDATE(),a.notificationDate)  ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a1.cDate),1,2),' ',' hours ago'),0,'') as notificationhour , cast(DATEDIFF(CURDATE(),a1.notificationDate) as char ) AS dateDiffrenceValue, a1.notificationType ,a1.title,a1.message,a1.notificationDate,a1.notificationTime ")
					.append("from android_notification_status a1 ")
					.append("left join  event b1 on b1.eventId=a1.joinId ")
					.append("left join event_assign_to c1 on c1.eventId=b1.eventId ")
					.append("where c1.staffStudentId='"+renewId+"' and c1.role='Parent' and a1.notificationType='New Event' and  DATEDIFF(CURDATE(),a1.notificationDate)  ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a2.cDate),1,2),' ',' hours ago'),0,'') as notificationhour ,  cast(DATEDIFF(CURDATE(),a2.notificationDate) as char) AS dateDiffrenceValue, a2.notificationType ,a2.title,a2.message,a2.notificationDate,a2.notificationTime  ")
					.append("from android_notification_status a2 ") 
					.append("left join  event b2 on b2.eventId=a2.joinId ")
					.append("left join event_assign_to c2 on c2.eventId=b2.eventId ")
					.append("where c2.staffStudentId='"+renewId+"' and c2.role='Parent' and a2.notificationType='Event-Reminder' and  DATEDIFF(CURDATE(),a2.notificationDate)  ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a3.cDate),1,2),' ',' hours ago'),0,'') as notificationhour ,  cast(DATEDIFF(CURDATE(),a3.notificationDate) as char) AS dateDiffrenceValue ,a3.notificationType ,a3.title,a3.message,a3.notificationDate,a3.notificationTime ")
					.append("from android_notification_status a3 ")
					.append("left join  gallery b3 on b3.galleryId=a3.joinId ")
					.append("left join gallery_assign_to c3 on c3.galleryId=b3.galleryId ")
					.append("where c3.staffStudentId='"+renewId+"' and c3.role='Parent' and a3.notificationType='Gallery' and  DATEDIFF(CURDATE(),a3.notificationDate)  ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a4.cDate),1,2),' ',' hours ago'),0,'') as notificationhour , cast(DATEDIFF(CURDATE(),a4.notificationDate) as char) AS dateDiffrenceValue,  a4.notificationType ,a4.title,a4.message,a4.notificationDate,a4.notificationTime  ") 
					.append("from android_notification_status a4 ")
					.append("left join  announcement b4 on b4.announcementId=a4.joinId ")
					.append("left join announcement_assign_to c4 on c4.announcementId=b4.announcementId ")
					.append("where c4.staffStudentId='"+renewId+"' and c4.role='Parent' and a4.notificationType='Announcement' and  DATEDIFF(CURDATE(),a4.notificationDate)  ) ")
					.append("Union all ") 
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a5.cDate),1,2),' ',' hours ago'),0,'') as notificationhour , cast(DATEDIFF(CURDATE(),a5.notificationDate) as char) AS dateDiffrenceValue, a5.notificationType ,a5.title,a5.message,a5.notificationDate,a5.notificationTime ") 
					.append("from android_notification_status a5 ") 
					.append("left join attendance b5 on b5.attendance_id=a5.joinId ")
					.append("WHERE ((FIND_IN_SET('"+renewId+"', b5.absent_student_id) > 0) or (FIND_IN_SET('"+renewId+"',b5.present_student_id) > 0) or (FIND_IN_SET('"+renewId+"',b5.exempted_student_id) > 0)) ")
					.append("and a5.notificationType='Attendance' and  DATEDIFF(CURDATE(),a5.notificationDate) < 10 ) " )
					.append("Union all ") 
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a6.cDate),1,2),' ',' hours ago'),0,'') as notificationhour ,  cast(DATEDIFF(CURDATE(),a6.notificationDate) as char) AS dateDiffrenceValue, a6.notificationType ,a6.title,a6.message,a6.notificationDate,a6.notificationTime ") 
					.append("from android_notification_status a6 ") 
					.append("WHERE a6.joinId='"+renewId+"' and a6.notificationType='Fee Reminder' and  DATEDIFF(CURDATE(),a6.notificationDate) < 10 ) ")
					.append(")q1 order by  q1.notificationDate  desc ");
					
	
			Query query = entitymanager.createNativeQuery(notifyquery.toString());
			
				List<NotificationBean> list = query.unwrap( org.hibernate.query.Query.class )
					 .setResultTransformer(Transformers.aliasToBean(NotificationBean.class ) )
					 .getResultList();
			
			return list;
		}
		
		
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		public List<NotificationBean> getAllNotificationsForTeachers(Integer renewId, String role, String joinId, Integer offset) {
			StringBuffer notifyquery = new StringBuffer();
			notifyquery.append("select * from (  ")
			         .append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a2.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a2.notificationDate) as char) AS dateDiffrenceValue,a2.notificationType ,a2.title,a2.message,a2.notificationDate,a2.notificationTime  ")
					.append("from android_notification_status a2 ") 
					.append("left join  event b2 on b2.eventId=a2.joinId ")
					.append("left join event_assign_to c2 on c2.eventId=b2.eventId ")
					.append("where c2.staffStudentId='"+renewId+"' and c2.role='Teacher' and a2.notificationType='Event-Reminder' and  DATEDIFF(CURDATE(),a2.notificationDate) < 10 ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a1.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a1.notificationDate) as char) AS dateDiffrenceValue,a1.notificationType ,a1.title,a1.message,a1.notificationDate,a1.notificationTime ")
					.append("from android_notification_status a1 ")
					.append("left join  event b1 on b1.eventId=a1.joinId ")			
					.append("where b1.staffId='"+renewId+"' and a1.notificationType='New Event' and  DATEDIFF(CURDATE(),a1.notificationDate) < 10 ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a3.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a3.notificationDate) as char) AS dateDiffrenceValue,a3.notificationType ,a3.title,a3.message,a3.notificationDate,a3.notificationTime ")
					.append("from android_notification_status a3 ")
					.append("left join  gallery b3 on b3.galleryId=a3.joinId ")					
					.append("where b3.staffId='"+renewId+"'  and a3.notificationType='Gallery' and  DATEDIFF(CURDATE(),a3.notificationDate) < 10 ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a4.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a4.notificationDate) as char) AS dateDiffrenceValue,a4.notificationType ,a4.title,a4.message,a4.notificationDate,a4.notificationTime  ") 
					.append("from android_notification_status a4 ")
					.append("left join  announcement b4 on b4.announcementId=a4.joinId ")
					.append("left join announcement_assign_to c4 on c4.announcementId=b4.announcementId ")
					.append("where c4.staffStudentId='"+renewId+"' and c4.role='Teacher' and a4.notificationType='Announcement' and  DATEDIFF(CURDATE(),a4.notificationDate) < 10 ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a.notificationDate) as char) AS dateDiffrenceValue, a.notificationType ,a.title,a.message,a.notificationDate,a.notificationTime ")
					.append("from android_notification_status a ") 
					.append("left join student_assign_homework c on c.assignHomeworkId=a.joinId ")
					.append("where c.staffId='"+renewId+"'  and a.notificationType='HomeWork' and  DATEDIFF(CURDATE(),a.notificationDate) < 10 ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a.notificationDate) as char) AS dateDiffrenceValue, a.notificationType ,a.title,a.message,a.notificationDate,a.notificationTime ")
					.append("from android_notification_status a ") 
					.append("left join student_assign_assignment c on c.assignAssignmentId=a.joinId ")
					.append("where c.staffId='"+renewId+"'  and a.notificationType='Assignment' and  DATEDIFF(CURDATE(),a.notificationDate) < 10 ) ")
					.append("Union all ")
					.append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a5.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a5.notificationDate) as char) AS dateDiffrenceValue,a5.notificationType ,a5.title,a5.message,a5.notificationDate,a5.notificationTime  ") 
					.append("from android_notification_status a5 ")
					.append("where a5.joinId like '"+joinId+"%' and a5.notificationType='Attendance Reminder' and  DATEDIFF(CURDATE(),a5.notificationDate) < 10  ) ")
					.append(")q1 order by q1.notificationDate desc ");
					
					
			Query query = entitymanager.createNativeQuery(notifyquery.toString());
			
				List<NotificationBean> list = query.unwrap( org.hibernate.query.Query.class )
					 .setResultTransformer(Transformers.aliasToBean(NotificationBean.class ) )
					 .getResultList();
			
			return list;
		}


		@SuppressWarnings({ "unchecked", "deprecation" })
		public List<StandardDivisionBean> getStandardDivisionList(Integer renewId) {
			StringBuffer notifyquery = new StringBuffer();
			notifyquery.append("select a.divId as divisionId ,a.stdId as standardId  from staff_subjectdetails a where a.staffId='"+renewId+"' ");
//			notifyquery.append("select a.divId as divisionId ,a.stdId as standardId  from staff_subjectdetails a where a.staffId='"+renewId+"' and a.isClassTeacher='1' ");
		
					Query query = entitymanager.createNativeQuery(notifyquery.toString());
			
				List<StandardDivisionBean> list = query.unwrap( org.hibernate.query.Query.class )
					 .setResultTransformer(Transformers.aliasToBean(StandardDivisionBean.class ) )
					 .getResultList();
			
			return list;
		}
	/*
	 * notifyquery.append("select * ," )
	 * .append("case when SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),q1.notificationhour),1,2) > 1 "
	 * ) .append("then " )
	 * .append("REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),q1.notificationhour),1,2),' ',' hours ago'),0,'') "
	 * ) .append("else " )
	 * .append("REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),q1.notificationhour),3,3),' ',' minutes ago'),':','') "
	 * ) .append("end from (  ")
	 * .append("(select REPLACE(CONCAT(SUBSTRING(TIMEDIFF(CURRENT_TIMESTAMP(),a.cDate),1,2),' ',' hours ago'),0,'') as notificationhour,  cast(DATEDIFF(CURDATE(),a.notificationDate) as char) AS dateDiffrenceValue, a.notificationType ,a.title,a.message,a.notificationDate,a.notificationTime "
	 * ) .append("from android_notification_status a ")
	 */



		public List<AndroidMenuBean> findBySchoolMasterModel(Integer schoolId, String reverseRoles, int i,
				String isReactFlag) {
			// TODO Auto-generated method stub
			String queryStr="SELECT c.androidMenuGroupId as groupdId,c.androidMenuGroupName as groupName,b.masterMenuId as menuId,b.menuName as menuName,b.imageFilePath as iconPath, b.isWebViewForReact,b.isWebViewForAndroid,b.webViewURL,b.parentMenuId "
									+ "FROM android_menu a "
									+ "LEFT JOIN android_menu_master b ON a.masterMenuId=b.masterMenuId "
									+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=b.androidMenuGroupId "
									+ "WHERE a.schoolID="+schoolId+" AND a.roleName='"+reverseRoles+"' and c.androidMenuGroupId="+i+" AND b.isReact IN ("+isReactFlag+")  ";
								Query query = entitymanager.createNativeQuery(queryStr);
								
								List<AndroidMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
									 .setResultTransformer(Transformers.aliasToBean(AndroidMenuBean.class ) )
									 .getResultList();
								return list;
		}
//		public List<AndroidMenuBean> findBySchoolMasterModelForReact(Integer schoolId, String reverseRoles, int i,
//				String isReactFlag) {
//			// TODO Auto-generated method stub
//			String queryStr="SELECT c.androidMenuGroupId as groupdId,c.androidMenuGroupName as groupName,b.masterMenuId as menuId,b.menuName as menuName,b.imageFilePath as iconPath,b.isWebViewForReact,b.isWebViewForAndroid,b.webViewURL "
//									+ "FROM android_menu a "
//									+ "LEFT JOIN android_menu_master b ON a.masterMenuId=b.masterMenuId "
//									+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=b.androidMenuGroupId "
//									+ "WHERE a.schoolID="+schoolId+" AND a.roleName='"+reverseRoles+"' and c.androidMenuGroupId="+i+" AND b.isReact IN ("+isReactFlag+") and b.isWebViewForReact='1' ";
//								Query query = entitymanager.createNativeQuery(queryStr);
//								
//								List<AndroidMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
//									 .setResultTransformer(Transformers.aliasToBean(AndroidMenuBean.class ) )
//									 .getResultList();
//								return list;
//		}

		public List<AndroidMenuBean> GlobalMenuListCategory(String globalRole,String reverseRoles, Integer categoryMasterMenuId) {
			String queryStr="SELECT c.androidMenuGroupId as groupdId,c.androidMenuGroupName as groupName,b.android_menu_id as menuId,"
//					+ "d.menuName as menuName,d.imageFilePath as iconPath, d.isWebViewForReact,d.isWebViewForAndroid,d.webViewURL ,b.principal_parent_menu_id AS parentMenuId "
					+ "d.menuName as menuName,d.imageFilePath as iconPath, b.principal_parent_menu_id AS parentMenuId "
					+ "FROM android_menu_master_category b "
					+ "LEFT JOIN android_menu_master d ON b.android_menu_id = d.masterMenuId "
					//+ "LEFT JOIN android_menu_master b ON a.masterMenuId=b.masterMenuId "
					+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=b.group_id "
					+ "WHERE b.global_for_role IN ("+ globalRole +") and b.menu_category_id = "+categoryMasterMenuId+"  ";
				Query query = entitymanager.createNativeQuery(queryStr);
				
				List<AndroidMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
					 .setResultTransformer(Transformers.aliasToBean(AndroidMenuBean.class ) )
					 .getResultList();
				return list;

		}
		


		public Integer getUnseenNotificationCount(Integer studentId, String typeFlag) {
			// TODO Auto-generated method stub
			try{
				String queryStr="SELECT COUNT(*) FROM save_send_notification a WHERE a.renewId_staffId="+studentId+" AND a.typeFlag="+typeFlag+" AND a.seenUnseenStatusFlag=0";
				Integer count = (Integer) entitymanager.createNativeQuery(queryStr).getResultList().get(0);
//				Object count = entitymanager.createNativeQuery(queryStr).getResultList().get(0);
//				String a = count.toString();
//				Integer a1 = Integer.parseInt(a);
//				return a1;
				return count;
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return 0;
			}
			
		}


		@Transactional
		public void updateTokenNoInAppUser(String token, String mobileNo, Integer schoolId) {
			StringBuffer sql=new StringBuffer();
			String queryStr="SELECT a.APPUSERROLEID AS userId FROM app_user a WHERE a.USERNAME="+mobileNo+"  AND a.schoolid='"+schoolId+"' AND a.tokenInitialDate IS null AND a.RoleName='ROLE_PARENT1'";
			Query query1 = entitymanager.createNativeQuery(queryStr);
			
			List<AndroidMenuBean> list = query1.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AndroidMenuBean.class ) )
				 .getResultList();
			
			if(list.size()>0) {
				sql.append("UPDATE app_user a SET a.token='"+token+"',a.tokenInitialDate=CURRENT_TIMESTAMP()  WHERE a.USERNAME='"+mobileNo+"' AND a.schoolid='"+schoolId+"' AND a.RoleName='ROLE_PARENT1' ");
			}else {
				sql.append("UPDATE app_user a SET a.token='"+token+"',a.tokenUpdateDate=CURRENT_TIMESTAMP()  WHERE a.USERNAME='"+mobileNo+"' AND a.schoolid='"+schoolId+"' AND a.RoleName='ROLE_PARENT1' ");
			}
			
			int query = entitymanager.createNativeQuery(sql.toString()).executeUpdate();
		}



		@SuppressWarnings("unchecked")
		public List<AndroidMenuBean> GlobalMenuList1(String globalRole, String reverseRoles, Integer staffId,
				String isReactFlag) {

			String queryStr="SELECT a.androidMenuGroupId as groupdId, a.androidMenuGroupName as groupName, p.principalMenuId AS menuId, p.menuName AS menuName, "
//					+ "p.imageFilePath as iconPath, p.isWebViewForReact, p.isWebViewForAndroid, p.webViewURL, p.parentMenuId, p.priority "
					+ "pma.iconPath as iconPath, p.isWebViewForReact, p.isWebViewForAndroid, p.webViewURL, p.parentMenuId, p.priority "
					+ "FROM principal_menu_master p "
					+ "LEFT JOIN principal_menu_assignto_staff pma ON p.principalMenuId = pma.principalMenuMasterId "
					+ "LEFT JOIN android_menu_master_group a ON p.androidMenuGroupId = a.androidMenuGroupId "
//					+ "WHERE  b.globalForRole IN ("+ globalRole +") and b.isGlobal='1' and c.androidMenuGroupId = 27 AND b.isReact IN ("+isReactFlag+")  ";
//					+ "WHERE  pma.staffId = '" + staffId + "' AND p.isReact IN ("+isReactFlag+")  ";
					+ "WHERE  pma.staffId = '" + staffId + "' AND p.isReact IN ("+isReactFlag+") AND pma.isDel = 0 ";
				Query query = entitymanager.createNativeQuery(queryStr);
				
				@SuppressWarnings("deprecation")
				List<AndroidMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
					 .setResultTransformer(Transformers.aliasToBean(AndroidMenuBean.class ) )
					 .getResultList();
				return list;
				
//			return null;
		}

//		06-12-2024
		public List<AndroidMenuBean> GlobalMenuList(String globalRole,String reverseRoles, Integer groupdId, String isReactFlag) {
			String queryStr="SELECT c.androidMenuGroupId as groupdId,c.androidMenuGroupName as groupName,b.masterMenuId as menuId,b.menuName as menuName,b.imageFilePath as iconPath, b.isWebViewForReact,b.isWebViewForAndroid,b.webViewURL ,b.parentMenuId "
					+ "FROM android_menu_master b "
					//+ "LEFT JOIN android_menu_master b ON a.masterMenuId=b.masterMenuId "
					+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=b.androidMenuGroupId "
					+ "WHERE  b.globalForRole IN ("+ globalRole +") and b.isGlobal='1' and c.androidMenuGroupId="+groupdId+" AND b.isReact IN ("+isReactFlag+")  ";
				Query query = entitymanager.createNativeQuery(queryStr);
				
				List<AndroidMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
					 .setResultTransformer(Transformers.aliasToBean(AndroidMenuBean.class ) )
					 .getResultList();
				return list;

		}



		public List<Integer> getMenuMasterCategory(Integer typeOfManagementId, Integer menuPolicy, Integer specificMenuCategory,
				Integer sansthaKey) {

			StringBuffer que = new StringBuffer();
			
			que.append("SELECT a.menu_category_id ");
			que.append("FROM menu_master_category a ");
			que.append("WHERE ");
//			que.append("(:menuPolicy = 1 AND a.type_of_management_id = :typeOfManagementId) OR ");
//			que.append("(:menuPolicy = 3 AND a.sanstha_key = :sansthaKey) OR ");
//			que.append("(:menuPolicy = 4 AND a.menu_category_id = :specificMenuCategory) ");
			
			if (menuPolicy == 1) {
		        que.append("a.type_of_management_id = :typeOfManagementId ");
		    } else if (menuPolicy == 3) {
		        que.append("a.sanstha_key = :sansthaKey ");
		    } else if (menuPolicy == 4 && specificMenuCategory != null && specificMenuCategory != 0) {
		        que.append("a.menu_category_id = :specificMenuCategory ");
		    }
			
//			que.append(" ");
//			que.append(" ");
//			que.append(" ");
			
			Query query = entitymanager.createNativeQuery(que.toString());

			if(typeOfManagementId != 0 && typeOfManagementId != null)
				query.setParameter("typeOfManagementId", typeOfManagementId);
			
//			if(menuPolicy != 0)
//				query.setParameter("menuPolicy", menuPolicy);
			
//			if(specificMenuCategory != 0 && specificMenuCategory != null)
		    if (menuPolicy == 4 && specificMenuCategory != null && specificMenuCategory != 0)
				query.setParameter("specificMenuCategory", specificMenuCategory);
			
//			if(sansthaKey != 0 && sansthaKey != null)
		    if (sansthaKey != 0 && sansthaKey != null && menuPolicy == 3)
				query.setParameter("sansthaKey", sansthaKey);

//			List<Integer> list = query.unwrap( org.hibernate.query.Query.class )
//					 .setResultTransformer(Transformers.aliasToBean(Integer.class ) )
//					 .getResultList();
			
		    List<Integer> list = query.getResultList();
			
			return list;
			
		}
		
		

}