package com.ingenio.admission.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.EmailRequestBean;
import com.ingenio.admission.bean.FeeBean;

@Repository
@SuppressWarnings({ "unchecked", "deprecation" })
public class FeeReportsRepositoryImpl {
  
  @PersistenceContext
  private EntityManager entitymanager;
	
	
  public List<FeeBean> getTotalFee(Integer renewId) {
		StringBuilder feeReportQuery = new StringBuilder();
		/*
		 * feeReportQuery.
		 * append("SELECT feeSetMultiID,studFeeID,standardId, headID as headId,headName,subheadID as subheadId,subheadName,totalAssignFee as assignFee,fine as assignedFine "
		 * )
		 * .append(",feesClearance as clearanceFee, cast(IFNULL(fineClearance,0) as char)  as clearanceFine,cast(discount as char) as discount, "
		 * )
		 * .append("totalPaidFee as paidFee,cast(IFNULL(totalPaidFine,0) as char) as paidFine, "
		 * )
		 * .append("cast(IFNULL(totalRemainFee,0.0) as char) as remainingFee, cast(IFNULL(IFNULL(fine,0)- IFNULL(totalPaidFine,0.0) - IFNULL(fineClearance,0),0.0) as char) AS remainingFine "
		 * )
		 * .append("FROM(SELECT * FROM (SELECT studRenewID,headID,headName,standardId,subheadID,subheadName,feeSetMultiID,studFeeID,mobno,email, "
		 * )
		 * .append("CAST((IFNULL(clearanceFee,0)) AS CHAR(50)) AS feesClearance,totalAssignFee,fine, "
		 * )
		 * .append("totalAssignFee + fine AS totalFee, SUM(totalPaidFee) AS totalPaidFee,"
		 * )
		 * .append("(totalAssignFee)- SUM(totalPaidFee) - IFNULL(clearanceFee,0) AS totalRemainFee,SUM(discount) as discount "
		 * ) .append("FROM ( ")
		 * .append("SELECT IFNULL(k.clearanceFee,0) as clearanceFee,IFNULL(i.feesClearance,0) AS feesClearance,IFNULL(i.fineClearance,0) AS fineClearance, "
		 * )
		 * .append("d.yearID AS yearid,d.standardID as standardId, c.studRenewID AS studRenewID,a.mMobile AS mobno, "
		 * ) .append("a.mEmail AS email,a.Student_RegNo AS regno,a.stud_id AS stud_id, "
		 * )
		 * .append("g.headID AS headID,f.headName AS headName,g.subheadID AS subheadID, "
		 * ) .append("h.subheadName AS subheadName,g.feeSetMultiID AS feeSetMultiID, ")
		 * .append("c.studFeeID AS studFeeID,IFNULL(g.assignFee,0) AS totalAssignFee,  "
		 * )
		 * .append("IFNULL((CASE WHEN (IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1  "
		 * ) .append("AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE())  ")
		 * .append("THEN IFNULL(g.fine* DATEDIFF(CURRENT_DATE(), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0)  "
		 * )
		 * .append("WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE()  "
		 * )
		 * .append("THEN 0 WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND  "
		 * )
		 * .append("STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN  "
		 * )
		 * .append("IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE()  "
		 * )
		 * .append("THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND  "
		 * )
		 * .append("STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1  "
		 * )
		 * .append("AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0)  "
		 * ) .append("WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND  ")
		 * .append("STR_TO_DATE(g.dueDate,'%d-%m-%Y')< STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y')  "
		 * )
		 * .append("THEN IFNULL(g.fine* DATEDIFF(STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y'), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0)  "
		 * ) .append("WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND  ")
		 * .append("STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN 0 END),0) AS fine,  "
		 * )
		 * .append("IFNULL(i.payFee,0) AS totalPaidFee,IFNULL(i.discount,0) as discount "
		 * ) .append("FROM fee_setting_multi g ")
		 * .append("LEFT OUTER JOIN fee_setting d ON d.feeSettingID=g.feeSettingID ")
		 * .append("LEFT OUTER JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID ")
		 * .append("LEFT OUTER JOIN fee_head_master f ON f.headID=g.headID ")
		 * .append("LEFT OUTER JOIN fee_subhead_master h ON h.subheadID=g.subheadID ")
		 * .append("LEFT OUTER JOIN yearmaster e ON e.yearId=d.yearID ")
		 * .append("LEFT OUTER JOIN student_master a ON a.stud_id=c.studRenewID and a.isAdmissionCancelled='1'  "
		 * )
		 * .append("LEFT OUTER JOIN student_standard_renew b on a.stud_id=b.studentId and b.standardId=d.standardID and b.yearId=d.yearID "
		 * )
		 * .append("LEFT OUTER JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  "
		 * )
		 * .append("LEFT JOIN fee_clearancetable k on k.feeSetMultiID=g.feeSetMultiID ")
		 * .append("WHERE g.isConsolation=0 and ")
		 * .append("b.renewstudentId ='"+renewId+"')vg ")
		 * .append("GROUP BY studRenewID,headID,subheadID)gf ") .append("LEFT OUTER ")
		 * .append("JOIN ( ")
		 * .append("SELECT j.feeSetMultiID AS feeSetMultiID1, SUM(IFNULL(j.dueFee,0)) AS totalPaidFine,j.headID AS headid1, "
		 * ) .append("j.subheadID AS subheadid1,j.studFeeID AS studRenewID1,  ")
		 * .append("CAST((IFNULL(k.dueFee,0)) AS CHAR(50)) AS fineClearance ")
		 * .append("FROM fee_setting_multi g ")
		 * .append("LEFT OUTER JOIN fee_setting d ON d.feeSettingID=g.feeSettingID ")
		 * .append("LEFT OUTER JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID ")
		 * .append("LEFT OUTER JOIN fee_head_master f ON f.headID=g.headID ")
		 * .append("LEFT OUTER JOIN fee_subhead_master h ON h.subheadID=g.subheadID ")
		 * .append("LEFT OUTER JOIN yearmaster e ON e.yearId=d.yearID ")
		 * .append("LEFT OUTER JOIN student_master a ON a.stud_id=c.studRenewID and a.isAdmissionCancelled='1'  "
		 * )
		 * .append("LEFT OUTER JOIN student_standard_renew b on a.stud_id=b.studentId and b.standardId=d.standardID and b.yearId=d.yearID "
		 * )
		 * .append("LEFT OUTER JOIN fee_paid_duetable j ON j.feeSetMultiID=g.feeSetMultiID "
		 * )
		 * .append("LEFT OUTER JOIN fee_duetable k ON k.feeSetMultiID=g.feeSetMultiID WHERE g.isConsolation=0 AND b.renewstudentId ='"
		 * +renewId+"' ")
		 * .append("GROUP BY studRenewID1,headid1,subheadid1)kj ON kj.feeSetMultiID1=gf.feeSetMultiID AND  "
		 * ) .append("kj.headid1=headID AND kj.subheadid1=subheadID)ghd ")
		 * .append("ORDER BY headID,subheadID ASC ");
		 */
		
		
		feeReportQuery.append("SELECT feeSetMultiID,studFeeID,standardId, headID AS headId,headName,subheadID AS subheadId,subheadName,totalAssignFee AS assignFee,fine AS assignedFine ");
		feeReportQuery.append(",feesClearance AS clearanceFee, CAST(IFNULL(fineClearance,0) AS CHAR) AS clearanceFine, CAST(discount AS CHAR) AS discount,  ");
		feeReportQuery.append("totalPaidFee AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine, CAST(IFNULL(totalRemainFee,0.0) AS CHAR) AS remainingFee, CAST(IFNULL(IFNULL(fine,0)- IFNULL(totalPaidFine,0.0) - IFNULL(fineClearance,0),0.0) AS CHAR) AS remainingFine ");
		feeReportQuery.append("FROM( ");
		feeReportQuery.append("SELECT studRenewID,headID,headName,standardId,subheadID,subheadName,feeSetMultiID,studFeeID,mobno,email, CAST((IFNULL(clearanceFee,0))AS CHAR(50)) AS feesClearance,totalAssignFee,fine, ");
		feeReportQuery.append("totalAssignFee + fine AS totalFee, SUM(totalPaidFee) AS totalPaidFee, ");
		feeReportQuery.append("(totalAssignFee)- SUM(totalPaidFee) - IFNULL(clearanceFee,0) AS totalRemainFee,SUM(discount) as discount,sum(duefineClearance) as  fineClearance,sum(totalPaidFine) as totalPaidFine  ");
		feeReportQuery.append("FROM ( ");
		feeReportQuery.append("SELECT IFNULL(k.clearanceFee,0) as clearanceFee,IFNULL(i.feesClearance,0) AS feesClearance, ");
		feeReportQuery.append(" IFNULL(i.fineClearance,0) AS fineClearance, ");
		feeReportQuery.append(" d.yearID AS yearid,d.standardID as standardId, c.studRenewID AS studRenewID,a.mMobile AS mobno, ");
		feeReportQuery.append(" a.mEmail AS email,a.Student_RegNo AS regno,a.stud_id AS stud_id, ");
		feeReportQuery.append(" g.headID AS headID,f.headName AS headName,g.subheadID AS subheadID,  ");
		feeReportQuery.append(" h.subheadName AS subheadName,g.feeSetMultiID AS feeSetMultiID,  ");
		feeReportQuery.append(" c.studFeeID AS studFeeID,IFNULL(g.assignFee,0) AS totalAssignFee, ");
		feeReportQuery.append(" IFNULL((CASE WHEN (IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1  ");
		feeReportQuery.append(" AND STR_TO_DATE(g.dueDate,'%d-%m-% Y')< CURRENT_DATE())  ");
		feeReportQuery.append(" THEN IFNULL(g.fine* DATEDIFF(CURRENT_DATE(), STR_TO_DATE(g.dueDate,'%d-%m-% Y')),0)  ");
		feeReportQuery.append(" WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-% Y')>= CURRENT_DATE()  ");
		feeReportQuery.append(" THEN 0 WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND  ");
		feeReportQuery.append(" STR_TO_DATE(g.dueDate,'%d-%m-% Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN  ");
		feeReportQuery.append(" IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-% Y')>= CURRENT_DATE()  ");
		feeReportQuery.append(" THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND  ");
		feeReportQuery.append(" STR_TO_DATE(g.dueDate,'%d-%m-% Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1  ");
		feeReportQuery.append(" AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-% Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) ");
		feeReportQuery.append(" WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND ");
		feeReportQuery.append(" STR_TO_DATE(g.dueDate,'%d-%m-% Y')< STR_TO_DATE(i.paidfeeDate,'%d-%m-% Y') ");
		feeReportQuery.append(" THEN IFNULL(g.fine* DATEDIFF(STR_TO_DATE(i.paidfeeDate,'%d-%m-% Y'), STR_TO_DATE(g.dueDate,'%d-%m-% Y')),0) ");
		feeReportQuery.append(" WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND  ");
		feeReportQuery.append(" STR_TO_DATE(g.dueDate,'%d-%m-% Y')>= STR_TO_DATE(i.paidfeeDate,'%d-%m-% Y') THEN 0 END),0) AS fine, ");
		feeReportQuery.append("IFNULL(i.payFee,0) AS totalPaidFee, ");
		feeReportQuery.append(" IFNULL(i.discount,0) as discount  ,IFNULL(j.dueFee,0) AS totalPaidFine,CAST((IFNULL(m.dueFee,0)) AS CHAR(50)) AS duefineClearance  ");
		feeReportQuery.append(" FROM fee_setting_multi g  ");
		feeReportQuery.append(" LEFT OUTER JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  ");
		feeReportQuery.append(" LEFT OUTER JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  ");
		feeReportQuery.append(" LEFT OUTER JOIN fee_head_master f ON f.headID=g.headID  ");
		feeReportQuery.append(" LEFT OUTER JOIN fee_subhead_master h ON h.subheadID=g.subheadID  ");
		feeReportQuery.append(" LEFT OUTER JOIN yearmaster e ON e.yearId=d.yearID  ");
		feeReportQuery.append(" LEFT OUTER JOIN student_master a ON a.stud_id=c.studRenewID and a.isAdmissionCancelled='1' and a.isApproval='0'    ");
		feeReportQuery.append(" LEFT OUTER JOIN student_standard_renew b on a.stud_id=b.studentId and b.standardId=d.standardID and b.yearId=d.yearID   ");
		feeReportQuery.append(" LEFT OUTER JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  ");
		feeReportQuery.append(" LEFT JOIN fee_clearancetable k on k.feeSetMultiID=g.feeSetMultiID  ");
		feeReportQuery.append(" LEFT OUTER JOIN fee_paid_duetable j ON j.feeSetMultiID=g.feeSetMultiID  ");
		feeReportQuery.append("LEFT OUTER JOIN fee_duetable m ON m.feeSetMultiID=g.feeSetMultiID   ");
		feeReportQuery.append("left join scholarship_disbursement m1 on m1.studentId=a.stud_id ");
		feeReportQuery.append(" WHERE g.isConsolation=0 ");
		feeReportQuery.append(" and  b.renewstudentId ='" + renewId + "')vg  ");
		feeReportQuery.append(" GROUP BY studRenewID,headID,subheadID)ghd  ");
		feeReportQuery.append("ORDER BY headID,subheadID ASC  ");
		
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		List<FeeBean> list = query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		
		return list;
		
	}
	
  
  
	
	/*@SuppressWarnings({ "deprecation", "unchecked" })
	public List<FeeBean> getTotalPaidFee(Integer renewId) {
		StringBuilder feeReportQuery = new StringBuilder();
		feeReportQuery.append("SELECT headId,headName,subheadId,subheadName, IFNULL(payFee,0) as paidFee, IFNULL(dueFee,0) as dueFee,receiptId,receiptNo,paidFeeDate ")
		.append("FROM ( ")
		.append("SELECT a.headId AS headId,j.headName AS headName, a.subheadId AS subheadId,k.subheadName AS subheadName, ")
		.append("SUM(a.payFee) AS payFee,g.receiptId,g.receiptNo,a.paidfeeDate AS paidFeeDate ")
		.append("FROM fee_paidfeetable a ")
		.append("LEFT JOIN fee_receipt g ON g.receiptId=a.receiptId AND g.isDel='0' ")
		.append("LEFT JOIN fee_studentfee b ON a.studFeeId=b.studFeeId AND b.isDel='0' ") 
		.append("LEFT JOIN fee_setting c ON c.feeSettingId=b.feeSettingId AND c.isDel='0' ") 
		.append("LEFT JOIN student_standard_renew i ON i.studentId=b.studRenewID ")
		.append("AND c.standardId=i.standardId ")
		.append("AND c.yearId=i.yearId ")
		.append("LEFT JOIN fee_head_master j ON j.headId=a.headId ")
		.append("LEFT JOIN fee_subhead_master k ON k.subheadId=a.subheadId ") 
		.append("WHERE a.isDel='0' AND i.renewStudentId='"+renewId+"' AND a.isConsolation='0' ")
		.append("GROUP BY a.headId,a.subheadId ")
		.append(" UNION ")
		.append("SELECT a.headId AS headId,j.headName AS headName, a.subheadId AS subheadId, ") 
		.append("k.subheadName AS subheadName, SUM(a.dueFee) AS dueFee,g.receiptId,g.receiptNo,'' AS paidFeeDate ") 
		.append("FROM fee_paid_duetable a ")
		.append("LEFT JOIN fee_receipt g ON g.receiptId=a.receiptId AND g.isDel='0' ")
		.append("LEFT JOIN fee_studentfee b ON a.studFeeId=b.studFeeId AND b.isDel='0' ") 
		.append("LEFT JOIN fee_setting c ON c.feeSettingId=b.feeSettingId AND c.isDel='0' ") 
		.append("LEFT JOIN student_standard_renew i ON i.studentId=b.studRenewID ")
		.append("AND c.standardId=i.standardId AND c.yearId=i.yearId ")
		.append("LEFT JOIN fee_head_master j ON j.headId=a.headId ")
		.append("LEFT JOIN fee_subhead_master k ON k.subheadId=a.subheadId ") 
	    .append("WHERE a.isDel='0' AND i.renewStudentId='"+renewId+"' AND a.isConsolation='0' ")
	    .append("GROUP BY a.headId,a.subheadId ")
	    .append(")t ")
		.append("GROUP BY headId,subheadId ") 
		.append("ORDER BY receiptId,headId,subheadId ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		List<FeeBean> list = query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		
		return list;
	}*/
	
	
	public List<FeeBean> getTotalPaidFee(Integer renewId) {
		StringBuilder feeReportQuery = new StringBuilder();
		feeReportQuery.append("SELECT headId,headName,subheadId,subheadName, IFNULL(payFee,0) - IFNULL(discount,0) as paidFee,receiptId,receiptNo, ")
		.append("paidFeeDate,receivedBy,CONCAT(paidBy,' ',ddChequeNo) as paidBy ")
		.append("FROM ( ")
		.append("SELECT a.headId AS headId,j.headName AS headName, a.subheadId AS subheadId,k.subheadName AS subheadName, ")
		.append("SUM(a.payFee) AS payFee,SUM(IFNULL(a.discount,0)) as discount, g.receiptId,g.receiptNo,a.paidfeeDate AS paidFeeDate,CONCAT(m.FIRST_NAME,' ',m.LAST_NAME) AS receivedBy,p.payTypeName AS paidBy,IFNULL(a.DDCHQno,'') as ddChequeNo ")
		.append("FROM fee_paidfeetable a ")
		.append("LEFT JOIN fee_receipt g ON g.receiptId=a.receiptId AND g.isDel='0' ")
		.append("LEFT JOIN fee_studentfee b ON a.studFeeId=b.studFeeId AND b.isDel='0' ") 
		.append("LEFT JOIN fee_setting c ON c.feeSettingId=b.feeSettingId AND c.isDel='0' ") 
		.append("LEFT JOIN student_standard_renew i ON i.studentId=b.studRenewID ")
		.append("AND c.standardId=i.standardId ")
		.append("AND c.yearId=i.yearId ")
		.append("LEFT JOIN fee_head_master j ON j.headId=a.headId ")
		.append("LEFT JOIN fee_subhead_master k ON k.subheadId=a.subheadId ") 
		.append("LEFT JOIN app_user_role l ON a.userId=l.APPUSERROLEID ")
		.append("LEFT JOIN app_user m ON m.APPUSERROLEID=l.APPUSERROLEID ")
		.append("LEFT JOIN fee_pay_type p ON p.payTypeID = a.payType ")
		.append("WHERE a.isDel='0' AND i.renewStudentId='"+renewId+"' AND a.isConsolation='0' ")
		.append("GROUP BY a.headId,a.subheadId,a.receiptId ")
	    .append(")t ")
		.append("GROUP BY headId,subheadId,receiptId ") 
		.append("ORDER BY receiptId desc,headId,subheadId ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		List<FeeBean> list = query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		
		return list;
	}
	
	public Double getTotalPaidFine(Integer receiptId) {
		StringBuilder feeReportQuery = new StringBuilder();
		feeReportQuery.append("SELECT  SUM(IFNULL(a.dueFee,0)) AS dueFee FROM fee_paid_duetable a WHERE a.isDel='0' AND a.isConsolation='0' and a.receiptID='"+receiptId+"' ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		List<FeeBean> list = query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		
		return list.get(0).getDueFee();
	}

	public List<FeeBean> getPendingFees(Integer schoolId) {
		String feeReportQuery = "";
		feeReportQuery = "SELECT Student_RegNo AS studentRegNo,renewstudentId,studentName, standardId,standardName, "
				+ "IFNULL(rollNo,'') AS rollNo, CAST(IFNULL(divisionId,0) AS CHAR) AS divisionId, "
				+ "assignedFee AS assignFee, assignedFine, "
				+ "CAST(clearanceFee AS CHAR) AS clearanceFee, CAST(IFNULL(fineClearance,0) AS CHAR) AS clearanceFine,"
				+ "paidFee , CAST(discount AS CHAR) AS discount, "
				+ "CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine, "
				+ "CAST((assignedFee - (paidFee + discount) - clearanceFee) AS CHAR) AS remainingFee, "
				+ "CAST(IFNULL((assignedFine- totalPaidFine - fineClearance),0) AS CHAR) AS remainingFine,renewstudentId "
				+ "FROM (SELECT * FROM (SELECT renewstudentId,feeSetMultiID,studentId,Student_RegNo,grBookId,studentName,rollNo, "
				+ "divisionId,yearId,yearName,standardId,standardName, SUM(IFNULL(clearanceFee,0)) AS clearanceFee, SUM(IFNULL(totalAssignFee,0)) AS assignedFee,"
				+ " SUM(IFNULL(fine,0)) AS assignedFine, SUM(IFNULL(totalPaidFee,0)) AS paidFee, SUM(IFNULL(discount,0)) AS discount FROM ("
				+ "SELECT m.renewstudentId, m.studentId,n.Student_RegNo,n.grBookId, "
				+ "CONCAT(n.studFName,' ',n.studLName) AS studentName,m.rollNo, e.yearId,e.year AS yearName, "
				+ "l.standardId,l.standardName,m.divisionId, SUM(k.clearanceFee) AS clearanceFee,i.feesClearance AS feesClearance, "
				+ "i.fineClearance AS fineClearance, g.headID AS headID,f.headName AS headName, g.subheadID AS subheadID, h.subheadName AS subheadName,"
				+ "g.assignFee AS totalAssignFee, IFNULL((CASE WHEN (IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND "
				+ "STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE()) THEN IFNULL(g.fine* DATEDIFF(CURRENT_DATE(), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) "
				+ "WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 "
				+ "WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) "
				+ "WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN "
				+ "IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN "
				+ "IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN "
				+ "IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN "
				+ "IFNULL(g.fine* DATEDIFF(STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y'), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=1 "
				+ "AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN 0 END),0) AS fine, "
				+ "SUM(IFNULL(i.payFee,0) - IFNULL(i.discount,0)) AS totalPaidFee, SUM(IFNULL(i.discount,0)) AS discount,g.feeSetMultiID "
				+ "FROM fee_setting_multi g "
				+ "LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID "
				+ "LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID "
				+ "LEFT JOIN fee_head_master f ON f.headID=g.headID "
				+ "LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID "
				+ "LEFT JOIN yearmaster e ON e.yearId=d.yearID "
				+ "LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID "
				+ "LEFT JOIN fee_clearancetable k ON k.feeSetMultiID=g.feeSetMultiID "
				+ "LEFT JOIN standardmaster l ON l.standardId=d.standardID "
				+ "LEFT JOIN student_standard_renew m ON m.studentId=c.studRenewID AND m.standardId=d.standardID AND m.yearId=d.yearID "
				+ "LEFT JOIN student_master n ON n.stud_id=m.studentId and n.isAdmissionCancelled='1'  "
				+ "WHERE g.isConsolation=0 AND g.schoolid='"+schoolId+"' AND m.alreadyRenew=1 "
				+ "GROUP BY yearId,standardId,divisionId,headId,subheadId,renewstudentId)vg "
				+ "GROUP BY renewstudentId)gf "
				+ "LEFT JOIN (SELECT SUM(totalPaidFine) AS totalPaidFine, SUM(fineClearance) AS fineClearance, yearId1,standardId1,renewstudentId1 "
				+ "FROM (SELECT m.renewstudentId AS renewstudentId1, m.divisionId AS divisionId1,g.headID AS headId1,g.subheadID AS subheadId1, "
				+ "e.yearId AS yearId1,l.standardId AS standardId1, SUM(IFNULL(j.dueFee,0)) AS totalPaidFine, j.studFeeID AS studFeeID1, "
				+ "IFNULL(k.dueFee,0) AS fineClearance,g.feeSetMultiID AS feeSetMultiID1 "
				+ "FROM fee_setting_multi g LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID "
				+ "LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID "
				+ "LEFT JOIN fee_head_master f ON f.headID=g.headID "
				+ "LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID "
				+ "LEFT JOIN yearmaster e ON e.yearId=d.yearID "
				+ "LEFT JOIN fee_paid_duetable j ON j.feeSetMultiID=g.feeSetMultiID "
				+ "LEFT JOIN fee_duetable k ON k.feeSetMultiID=g.feeSetMultiID "
				+ "LEFT JOIN standardmaster l ON l.standardId=d.standardID "
				+ "LEFT JOIN student_standard_renew m ON m.studentId=c.studRenewID AND m.standardId=d.standardID AND m.yearId=d.yearID "
				+ "LEFT JOIN student_master n ON n.stud_id=m.studentId and n.isAdmissionCancelled='1'  "
				+ "WHERE g.isConsolation=0 AND g.schoolid='"+schoolId+"' AND m.alreadyRenew=1 "
				+ "GROUP BY yearId1,standardId1,divisionId1,headId1,subheadId1,renewstudentId1)a1 "
				+ "GROUP BY renewstudentId1)kj ON kj.renewstudentId1 =gf.renewstudentId)ghd "
				+ "ORDER BY yearId,standardId,divisionId,Student_RegNo ASC ";
		Query query = entitymanager.createNativeQuery(feeReportQuery);
	
		List<FeeBean> list = query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		
		
		return list;
	}


	public List<EmailRequestBean> getEmailDetails(String schoolkey, String razorpay_payment_id) {
		StringBuilder feeReportQuery = new StringBuilder();
		feeReportQuery.append("SELECT a.email as emailId , b.payFee as amount from challanbankdetails a "
				+ "left join fee_payment_gateway b on b.schoolID=a.schoolid WHERE a.schoolid='"+schoolkey+"' and b.transactionId='"+razorpay_payment_id+"' ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		List<EmailRequestBean> emailRequestList = query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(EmailRequestBean.class ) )
				 .getResultList();
		
		return emailRequestList ;
	}

}
