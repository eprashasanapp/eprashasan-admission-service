package com.ingenio.admission.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.text.DateFormat;
import java.util.Date;
import java.text.ParseException;
import com.ingenio.admission.bean.DeviceCommunicationBean;

@Repository
public class DeviceCommunicationRepositoryImpl {

	 @PersistenceContext
	  private EntityManager entitymanager;

	public List<DeviceCommunicationBean> getStaffIdFromStaffRfid(String rfid, int schoolId, String dateOfTran, int deviceId, int organizationId) {
		  try { 
			  StringBuffer sql=new StringBuffer();
		 
			sql.append("SELECT c.locationId as locationId, a.staffOrStudentFlag AS staffOrStudentFlag,a.schoolId AS schoolId,a.staffId AS staffId,a.studentId,b.renewstudentId AS studRenewId,b.yearId AS yearId,b.standardId AS standardId,b.divisionId AS divisionId,cast(ifnull(b.rollNo,'') AS CHAR) AS rollNo ");
			sql.append("FROM student_mapp_to_rfid a LEFT JOIN student_standard_renew b ON a.studentId=b.studentId AND b.alreadyRenew='1' LEFT JOIN rfid_school_setting_scheduler c ON c.schoolId=a.schoolId  "); 
			sql.append("WHERE a.rfidcardNo='"+rfid+"' and a.schoolId="+schoolId+" AND a.activeStatus='1' AND c.deviceId="+deviceId+" AND c.organizationId="+organizationId+" "); 
			
			Query query = entitymanager.createNativeQuery(sql.toString());
			List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
											 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
											 .getResultList();
			/*
			 * if(CollectionUtils.isEmpty(list)) { sql.
			 * append("UPDATE attendance_rfid_punch a SET a.badAttendance='1' WHERE a.rfid='"
			 * +rfid+"' AND a.dateOfTransection=STR_TO_DATE('"
			 * +dateOfTran+"','%d-%m-%Y %H:%i:%s') and a.schoolId="
			 * +organizationId+" and a.deviceId="+deviceId+" ");
			 * entitymanager.createNativeQuery(sql.toString()).executeUpdate(); }else {
			 * sql.append("UPDATE attendance_rfid_punch a SET a.locationId='"+list.get(0).
			 * getLocationId()+"' WHERE a.rfid='"
			 * +rfid+"' AND a.dateOfTransection=STR_TO_DATE('"
			 * +dateOfTran+"','%d-%m-%Y %H:%i:%s') and a.schoolId="
			 * +organizationId+" and a.deviceId="+deviceId+" ");
			 * entitymanager.createNativeQuery(sql.toString()).executeUpdate(); }
			 */
			return list;
		  } catch (Exception e) {
				e.printStackTrace();
				return null;
		}
	}

	public List<DeviceCommunicationBean> getAllStaffIdData(Integer staffId) {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT a.created_date AS createDate,a.date AS DATE,a.intime AS inTime,a.outtime AS outTime,a.present_flag AS presentFlag,a.staff_id AS staffId, ");
		sql.append("a.`status` AS STATUS,a.inTimeAttendanceRfid as inRfId,a.outTimeAttendanceRfid as outRfId  ");  
		//sql.append("a.deleteBy AS deleteBy,a.isEdit AS isEdit,a.editDate AS editDate,a.editBy AS editBy,a.deviceType AS deviceType,a.ipAddress AS ipAddress,a.macAddress AS macAddress,a.sinkingFlag AS sinkingFlag ");  
		sql.append("FROM staff_attendance a ");  
		sql.append("WHERE  a.staff_attendance_id="+staffId+"  ");  
		
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		return list;
	}

	public List<DeviceCommunicationBean> getAttendanceClassId(Integer studentId, Integer schoolId, String attenceDate,
			Integer studRenewId) {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT cast(a.attandenceClasswiseId as char) AS attandenceClasswiseId,a.stud_id AS studentId,a.renewstudentId AS studRenewId,a.monthId AS monthId,cast(a.rollNo as char) AS rollNo,a.attendanceDate AS attendanceDate,a.inTime AS inTime,a.outTime AS outTime,cast(a.p_AFlag as char) AS pFlag,a.p_AType AS pType,a.noOfTimeAttendance AS noOfTime,a.remark AS remark,a.hardwareType AS hardwareType,a.divId AS divisionId,a.yearId AS yearId,a.stdId AS standardId,a.userId AS userId,a.inTimeAttendanceRfid AS inRfId,a.outTimeAttendanceRfid AS outRfId ");
		sql.append("FROM attendance_classwise_catalog a WHERE a.schoolid="+schoolId+" AND a.stud_id ="+studentId+" AND a.renewstudentId ="+studRenewId+" AND a.attendanceDate= '"+attenceDate+"' ");
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		return list;}

	@Transactional
	public int updateOutTimeAttRfid(String rfid, String dot, int flag) {
		StringBuffer sql=new StringBuffer();
		if(flag==0) {
			sql.append("UPDATE attendance_rfid_punch a SET a.outTimeStatus='1' WHERE a.rfid='"+rfid+"' AND a.dateOfTransection=STR_TO_DATE('"+dot+"','%d-%m-%Y %H:%i:%s') ");
		}else {
			sql.append("UPDATE attendance_rfid_punch a SET a.inTimeStatus='1' WHERE a.rfid='"+rfid+"' AND a.dateOfTransection=STR_TO_DATE('"+dot+"','%d-%m-%Y %H:%i:%s') ");
		}
		
		int query = entitymanager.createNativeQuery(sql.toString()).executeUpdate();
		//int list = query.unwrap( org.hibernate.query.Query.class ).executeUpdate();
		return 0;
	}

	public List<DeviceCommunicationBean> getAbsentStudentList(Integer schoolId) {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT CONCAT(ifnull(c.studFName,''),' ',ifnull(c.studMName,''),' ',ifnull(c.studLName,'')) AS studName,b.studentId AS studentId,a.renewstudentId AS studRenewId,cast(a.rollNo as char) AS rollNo,b.rfidcardNo AS rfids,a.divisionId AS divisionId,a.yearId AS yearId,a.standardId AS standardId,  "); 
		sql.append("cast(DATE_FORMAT(CURDATE(), '%d-%m-%Y') as char) AS currDate,a.schoolid AS schoolId,c.mMobile AS mobileNo  "); 
		sql.append("FROM  student_mapp_to_rfid b "); 
		sql.append("LEFT JOIN student_standard_renew a ON a.studentId=b.studentId "); 
		sql.append("LEFT JOIN student_master c ON c.stud_id=b.studentId  "); 
		sql.append("WHERE b.schoolId="+schoolId+" and a.alreadyRenew=1 and (a.college_leavingdate is null or a.college_leavingdate='') AND c.isDel='0' AND c.isAdmissionCancelled='1'   "); 
		sql.append("AND (a.isOnlineAdmission='0' OR  (a.isOnlineAdmission='1' AND a.isApproval='1') OR (a.isRegistration='1' AND a.isRegistrationApprovedFlag='1'))  "); 
		sql.append("AND  b.studentId NOT IN  (SELECT a.stud_id AS studId1 FROM attendance_classwise_catalog a  "); 
		sql.append("WHERE  a.attendanceDate=DATE_FORMAT(CURDATE(), '%d-%m-%Y') AND a.isDel='0' and a.schoolId="+schoolId+") ");
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		return list;
	}

	public List<DeviceCommunicationBean> getAllSchoolDataForAbsentList() {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT distinct a.schoolId AS schoolId FROM rfid_school_setting_scheduler a  "); 
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		return list;
	}

	public List<DeviceCommunicationBean> getSchoolId(int deviceId, int organizationId) {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT a.schoolId AS schoolId FROM rfid_school_setting_scheduler a where a.deviceId="+deviceId+"  and a.organizationId="+organizationId+" "); 
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		return list;
	}

	public List<DeviceCommunicationBean> getAllSinglePunchRecord(Integer schoolId) {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT cast(DATE_FORMAT(CURDATE(), '%d-%m-%Y') as char) AS currDate,ifnull(a.inTime,'') AS inTime,ifnull(a.outTime,'') AS outTime,a.stud_id AS studentId,b.mMobile AS mobileNo,CONCAT(IFNULL(b.studFName,''),' ',IFNULL(b.studMName,''),' ',IFNULL(b.studLName,'')) AS studName ");
		sql.append("FROM attendance_classwise_catalog a ");
		sql.append("LEFT JOIN student_master b ON a.stud_id=b.stud_id ");
		sql.append("WHERE a.schoolid="+schoolId+" AND a.attendanceDate=DATE_FORMAT(CURDATE(), '%d-%m-%Y') AND a.outTime IS NULL  AND a.p_AFlag='P' ");
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		return list;
	}

	public int gettimeId(Integer schoolId, LocalDate date) {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT a.schoolId AS schoolId FROM maintain_Time_for_RFID_schedular a where a.schoolId="+schoolId+" and Date(a.Time)='"+date+"' "); 
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		if(CollectionUtils.isNotEmpty(list)) {
			return list.get(0).getSchoolId();
		}
		return 0;
	}

	public int getHolidayList(Integer schoolId, String mainDate, String month, String year) {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT FIND_IN_SET('"+mainDate+"',a.holidays) AS date FROM holidaysmanagement a  WHERE a.schoolid="+schoolId+" AND a.hyear='"+year+"' AND a.hmonth='"+month+"' "); 
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		if(CollectionUtils.isNotEmpty(list)) {
			return list.get(0).getDate();
		}
		return 0;
	}

	public List<DeviceCommunicationBean> getPerticularSchoolList() {
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT distinct a.schoolId AS schoolId FROM maintain_Time_for_RFID_schedular a "); 
		Query query = entitymanager.createNativeQuery(sql.toString());
		List<DeviceCommunicationBean> list = query.unwrap( org.hibernate.query.Query.class )
										 .setResultTransformer(Transformers.aliasToBean(DeviceCommunicationBean.class ) )
										 .getResultList();
		return list;
	}
	
	
}
