package com.ingenio.admission.repository;

import java.util.List;

import org.hibernate.boot.model.source.spi.JpaCallbackSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.AppVersionForReactBean;
import com.ingenio.admission.model.AppVersionMasterModel;



public interface AppVersionMasterRepository  extends JpaRepository<AppVersionMasterModel, Integer>{

	
	@Query("select new com.ingenio.admission.bean.AppVersionForReactBean( a.versionID, a.versionCode, a.versionName,a.instruction,a. forRole,a. forSchool,a. isMandatoryToRole, a.isMandatoryToSchool, a.isReact)  "
		+ "from AppVersionMasterModel a  where  a.versionCode= (select max(b.versionCode) from AppVersionMasterModel b where b.isReact=:isReact and b.appType=:appType) and a.isReact=:isReact and a.appType=:appType " )
	List<AppVersionForReactBean> getRecentVersion(String isReact, String appType);

	
	
	
}
//SELECT  id, first_name, last_name, grade
//FROM student
//WHERE grade = (SELECT MAX(grade) FROM student);