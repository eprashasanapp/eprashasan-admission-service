package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.SaveSendWhatsAppSMSModel;

public interface SaveSendWhatsAppSMSRepository extends JpaRepository<SaveSendWhatsAppSMSModel, Integer>{

}
