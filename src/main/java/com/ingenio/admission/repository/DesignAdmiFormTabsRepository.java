package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.model.DesignAdmiFormTabsModel;
import com.ingenio.admission.model.SchoolMasterModel;


@Repository
public interface DesignAdmiFormTabsRepository extends JpaRepository<DesignAdmiFormTabsModel, Integer>{

	@Query(value = "select a from DesignAdmiFormTabsModel a where a.schoolMasterModel.schoolid=?1 and a.isDel='0' and a.isTabDisplay='1' ")
	List<DesignAdmiFormTabsModel> getTabsCombo(Integer schoolId);

	@Transactional 
	@Modifying
	@Query(value = "update DesignAdmiFormTabsModel c set c.tabsName =?1 WHERE c.tabsId =?2 and c.schoolMasterModel.schoolid=?3")
	Integer updateTabsName(String tabName, Integer tabId, Integer schoolId);

	@Transactional 
	@Modifying
	@Query(value = "update DesignAdmiFormTabsModel c set  c.isTabDisplay =?3 WHERE c.tabsId =?2 and c.schoolMasterModel.schoolid=?1 ")
	Integer updateTabsDisplayOrNotFlag(Integer schoolId, Integer tabId, String isTabDisplayOrNot);

	List<DesignAdmiFormTabsModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

}
