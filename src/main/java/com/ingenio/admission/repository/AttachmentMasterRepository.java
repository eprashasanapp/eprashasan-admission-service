package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.AttachmentMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface AttachmentMasterRepository  extends JpaRepository<AttachmentMasterModel, Integer>{
	
	
	List<AttachmentMasterModel> findBySchoolMasterModelOrderByAttachmentMasterId(SchoolMasterModel schoolMasterModel);


}
