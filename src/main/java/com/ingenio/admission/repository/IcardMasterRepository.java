package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.StudentImageBean;
import com.ingenio.admission.model.IcardMaster;


public interface IcardMasterRepository extends  JpaRepository<IcardMaster, Integer> {
	//@Query("select new com.ingenio.admission.bean.UserDetailsBean(a.appUserRoleId,a.staffId,a.schoolid,a.roleName) from AppUserModel a where a.appUserRoleId=:userId ")

	
	@Query(value="select * from icard_master ic where ic.studId=?1 and ic.regno=?2 and ic.isDel='0'",nativeQuery = true)
	IcardMaster getIdcardMaster(Integer studentId, String regNo);

	@Query(value="SELECT COALESCE(MAX(a.icardId),0)+1 from IcardMaster a where a.schoolId.schoolid=:schoolId")
	String getMaxId(Integer schoolId);

	@Query("select new com.ingenio.admission.bean.StudentImageBean(a.originalName,a.imagePath ) from IcardMaster a "
			+ "where a.studId=:studentId and a.renewId=:studentRenewId and a.schoolId.schoolid=:schoolId ")
	List<StudentImageBean> getStudentPhotographForPrint(int studentId, Integer studentRenewId, Integer schoolId);
	
	

}
