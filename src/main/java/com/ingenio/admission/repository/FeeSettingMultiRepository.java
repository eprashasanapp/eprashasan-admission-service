package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeeSettingMultiModel;

public interface FeeSettingMultiRepository extends JpaRepository<FeeSettingMultiModel, Integer> {

}
