package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeePaidDueModel;

public interface PaidFineRepository extends JpaRepository<FeePaidDueModel, Integer> {

}
