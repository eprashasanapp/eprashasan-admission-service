package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.SMSAPiMasterModel;

public interface SmsApiMasterRepository extends JpaRepository<SMSAPiMasterModel, Integer> {
	
	List<SMSAPiMasterModel> findByMobNoId(Integer i);

}
