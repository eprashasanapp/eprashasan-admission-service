package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.AppVersionToRoleModel;


public interface AppVersionToRoleRepository extends JpaRepository<AppVersionToRoleModel, Integer>{

	@Query("select a.roleName from AppVersionToRoleModel a where a.appVersionMasterModel.versionID=:versionId and a.roleName=:roleName")
	String findUserPresent(Integer versionId,String roleName);

}
