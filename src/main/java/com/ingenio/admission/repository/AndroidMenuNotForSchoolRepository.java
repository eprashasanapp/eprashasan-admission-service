package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.AndroidMenuBean;
import com.ingenio.admission.model.AndroidMenuNotForSchoolModel;

public interface AndroidMenuNotForSchoolRepository extends JpaRepository<AndroidMenuNotForSchoolModel, Integer>{

	@Query("select a.masterMenuId from AndroidMenuNotForSchoolModel b "
			+ "left join AndroidMenuMasterModel a on a.masterMenuId=b.androidMenuMasterModel.masterMenuId "
			+ "where b.schoolid=:schoolId ")
	List<Integer> checkNotApplicableMenus( Integer schoolId);

}
