package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.GlobalWhatsAppSMSModel;
import com.ingenio.admission.model.SchoolWiseWhatsAppSMSModel;


public interface GlobalWhatsAppSMSRepository extends JpaRepository<GlobalWhatsAppSMSModel, Integer>{

	@Query("select a from SchoolWiseWhatsAppSMSModel a where a.schoolMasterModel.schoolid=:schoolId ")
	List<SchoolWiseWhatsAppSMSModel> getSchoolWiseSetting(Integer schoolId);

}
