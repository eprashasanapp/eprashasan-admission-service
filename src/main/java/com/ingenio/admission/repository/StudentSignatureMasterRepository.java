package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.StudentImageBean;
import com.ingenio.admission.model.IcardMaster;
import com.ingenio.admission.model.StudentSignatureMasterModel;

public interface StudentSignatureMasterRepository extends  JpaRepository<StudentSignatureMasterModel, Integer>  {

	@Query("select new com.ingenio.admission.bean.StudentImageBean(a.originalName,a.imagePath ) from StudentSignatureMasterModel a "
			+ "where a.studId=:studentId and a.renewId=:studentRenewId and a.schoolId.schoolid=:schoolId ")
	List<StudentImageBean> getStudentSignatureForPrint(int studentId, Integer studentRenewId, Integer schoolId);

}
