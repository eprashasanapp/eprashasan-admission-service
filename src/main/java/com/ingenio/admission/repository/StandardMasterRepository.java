package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StandardMasterModel;


@Repository
public interface StandardMasterRepository  extends JpaRepository<StandardMasterModel, Integer>{
	
	List<StandardMasterModel> findBySchoolMasterModelOrderByStandardPriority(SchoolMasterModel schoolMasterModel);

}
