package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.ReligionMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface ReligionMasterRepository  extends JpaRepository<ReligionMasterModel, Integer>{
	
	List<ReligionMasterModel> findBySchoolMasterModelOrderByReligionId(SchoolMasterModel schoolMasterModel);

}
