package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.CertificateImage;

@Repository
public interface ImageRepository extends JpaRepository<CertificateImage, Long> {
	@Query(value="SELECT imagePath FROM certificate_image a where  a.schoolid=?1 and a.flag='1' and a.isDel='0'",nativeQuery = true)
	String getFilePath(Integer schoolId);

	@Query(value="SELECT imagePath FROM icard_master a where a.schoolid=?1 and a.regNo=?2 and a.isDel='0'",nativeQuery = true)
	String getStudentImg(Integer schoolId, String regNo);
}
