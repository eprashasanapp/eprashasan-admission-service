package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.model.DesignAdmiDynamicFieldValueModel;


@Repository
public interface DesignAdmiFormDynamicFieldValueRepository extends JpaRepository<DesignAdmiDynamicFieldValueModel, Integer>{
	
	@Query("select new com.ingenio.admission.bean.AdmissionSettingBean(a.dynamicFieldId,a.fieldValue) from "
			 + "DesignAdmiDynamicFieldValueModel a where a.schoolMasterModel.schoolid=?1 and a.isDel='0' and a.designAdmiFormTabsMultiModel.fieldId=?2 order by a.dynamicFieldId ")
	List<AdmissionSettingBean> getValueToPopulateForDynamicField(@Param("schoolId") Integer schoolId,@Param("fieldId") Integer fieldId);
	
	@Transactional
	@Modifying
	@Query(value = "delete from DesignAdmiDynamicFieldValueModel d WHERE d.designAdmiFormTabsMultiModel.fieldId =?1 and d.schoolMasterModel.schoolid=?2 and d.isDel='0' ")
	Integer deleteDynamicFieldValue(Integer string, Integer schoolId);
	
	@Query(value = "select d from DesignAdmiDynamicFieldValueModel d where d.designAdmiFormTabsMultiModel.fieldId=?1 and d.schoolMasterModel.schoolid=?2 and d.isDel='0' " )
	List<DesignAdmiDynamicFieldValueModel> getFieldComboValue(Integer dynamicField, Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.dynamicFieldId),0)+1 from DesignAdmiDynamicFieldValueModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
