package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.AnnouncementResponseBean;
import com.ingenio.admission.model.AnnouncementModel;
import com.ingenio.admission.model.DummyRegistrationModel;

@Repository
public interface DummyRegistrationRepository extends JpaRepository<DummyRegistrationModel, Integer>{

	
	@Query("select a from DummyRegistrationModel a where a.mobileNo=:userID  ")
	DummyRegistrationModel findByMobileNo(String userID);


	@Query("select a from DummyRegistrationModel a where a.order_id=:razorpay_order_id  ")
	DummyRegistrationModel findByOrderId(String razorpay_order_id);

	
	
}
