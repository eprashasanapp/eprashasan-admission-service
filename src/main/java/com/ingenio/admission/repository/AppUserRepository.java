package com.ingenio.admission.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.model.AppUserModel;

@Repository
public interface AppUserRepository extends JpaRepository<AppUserModel, Integer>{
	
	@Transactional
	@Modifying
	@Query("update AppUserModel  a set a.password=:encryNewpassword where a.username=:userName and "
			+ "( a.roleName='ROLE_STUDENT' or a.roleName='ROLE_PARENT1' or a.roleName='ROLE_PARENT2' )  and a.schoolid=:schoolId and a.isDel='0' and a.password=:encryOldpassword ")
	Integer changePassword(String encryNewpassword,String userName, Integer schoolId, String encryOldpassword);

	@Transactional
	@Modifying
	@Query("update AppUserModel  a set a.password=:encryNewpassword where a.username=:userName and "
			+ "( a.roleName='ROLE_STUDENT' or a.roleName='ROLE_PARENT1' or a.roleName='ROLE_PARENT2' ) and a.schoolid=:schoolId and a.isDel='0'  ")
	Integer forgotPassword(String userName, String encryNewpassword,Integer schoolId);

	@Query("select a.appUserRoleId from AppUserModel a where a.roleName='ROLE_PRINCIPAL' and a.schoolid=:schoolId and a.isDel='0'  ")
	Integer getUserId(Integer schoolId);
	
	@Query("select a.roleName from AppUserRoleModel a where a.appUserRoleId=:userId")
	String getUserRole(Integer userId);

}
