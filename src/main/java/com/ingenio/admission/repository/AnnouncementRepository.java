package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.AnnouncementResponseBean;
import com.ingenio.admission.model.AnnouncementModel;

public interface AnnouncementRepository extends JpaRepository<AnnouncementModel, Integer>{

	@Query(value="select new com.ingenio.admission.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''), "
			+ "(select COALESCE(count(c1.announcementAttachmentId),0) from AnnouncementAttachmentModel c1 "
			+ "where c1.announcementModel.announcementId=a.announcementId "
			+ "group by c1.announcementModel.announcementId),COALESCE(CONCAT(e.firstname,' ',e.lastname),'') as staffName,"
			+ "f.roleName,b.announcementStatus,b.announcementAssignToId,c.serverFilePath,a.schoolMasterModel.schoolid,e.sregNo ) "
			+ "from AnnouncementModel a "
			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid = b.schoolMasterModel.schoolid  "
			+ "left join AnnouncementAttachmentModel c on c.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid = c.schoolMasterModel.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid = e.schoolMasterModel.schoolid "
			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole) OR a.staffId=:staffStudentId) "
			+ "and b.yearMasterModel.yearId=:yearId group by a.announcementId order by a.CDate desc ")
	List<AnnouncementResponseBean> getStudentAnnouncement(Integer staffStudentId, String profileRole,Integer yearId);
	
	@Query(value="select a.serverFilePath from AnnouncementAttachmentModel a where a.announcementModel.announcementId=:announcementId ")
	List<String> getAttachments(Integer announcementId);
	
}
