package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.StaffBasicDetailsModel;

public interface StaffBasicDetailsRepository extends JpaRepository<StaffBasicDetailsModel, Integer>{

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(b.sregNo,b.staffId,COALESCE(b.initialname,''),COALESCE(b.firstname,''),COALESCE(b.secondname,''),"
			+ "COALESCE(b.lastname,''),COALESCE(b.gender,''),COALESCE(c.castName,''),"
			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.contactnosms,''),COALESCE(b.birthdate,''),COALESCE(b.birthplace,''),"
			+ "COALESCE(b.nationality,''),COALESCE(b.joiningDate,''),b.departmentModel.departmentId,COALESCE(f.departmentName,''),COALESCE(g.designationName,'')) "
			+ "from StaffBasicDetailsModel b  "
			+ "left join CasteMasterModel c on c.casteId=b.casteMasterModel.casteId and c.isDel='0'   "
			+ "left join CategoryMasterModel d on d.categoryId=b.categoryMasterModel.categoryId and d.isDel='0'  "
			+ "left join ReligionMasterModel e on e.religionId=b.religionMasterModel.religionId and e.isDel='0' "
			+ "left join DepartmentModel f on f.departmentId=b.departmentModel.departmentId and f.isDel='0'    "
			+ "left join DesignationModel g on g.designationId=b.designationModel.designationId and g.isDel='0'   "	
			+ "where b.staffId=:staffId " )
	UserDetailsBean findStaffDetails(Integer staffId);

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(a.sansthaUserRegNo,COALESCE(a.initialName,''),"
			+ "COALESCE(a.firstName,''),COALESCE(a.secondName,''),COALESCE(a.lastName,'') , COALESCE(a.gender,''), "  
			+ "COALESCE(a.contactNoSms,''),COALESCE(a.birthDate,''),COALESCE(a.joiningDate,'')) "
			+ "from SansthaUserRegistrationModel a left join AppUserModel b on a.sansthaUserId=b.staffId where b.appUserRoleId=:userId ")
	UserDetailsBean findSansthaOfficerDetails(Integer userId);

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(a.firstName,a.lastName,a.username,a.schoolid) from AppUserModel a "
			+ "where a.appUserRoleId=:userId ")
	UserDetailsBean findSuperOfficerDetails(Integer userId);
	
	@Query("select new com.ingenio.admission.bean.UserDetailsBean(b.sregNo,b.staffId,COALESCE(b.initialname,''),COALESCE(b.firstname,''),COALESCE(b.secondname,''),"
			+ "COALESCE(b.lastname,''),COALESCE(b.gender,''),COALESCE(c.castName,''),"
			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.contactnosms,''),COALESCE(b.birthdate,''),COALESCE(b.birthplace,''),"
			+ "COALESCE(b.nationality,''),COALESCE(b.joiningDate,''),COALESCE(f.departmentName,''),COALESCE(g.designationName,'')) "
			+ "from StaffBasicDetailsModel b left join CasteMasterModel c on c.casteId=b.casteMasterModel.casteId and c.isDel='0' "
			+ "left join CategoryMasterModel d on d.categoryId=b.categoryMasterModel.categoryId and d.isDel='0'    "
			+ "left join ReligionMasterModel e on e.religionId=b.religionMasterModel.religionId and e.isDel='0'  "
			+ "left join DepartmentModel f on f.departmentId=b.departmentModel.departmentId and f.isDel='0'   "
			+ "left join DesignationModel g on g.designationId=b.designationModel.designationId and g.isDel='0'  "
			+ "left join StaffRelivingModel s on s.staffBasicDetailsModel.staffId=b.staffId and s.isDel='0' "
			+ "where b.staffId=:staffId and (s.relievingDate is null or  "
			+ "(str_to_date(s.relievingDate,'%Y-%m-%d') > str_to_date(:curdate,'%Y-%m-%d')))" )
	UserDetailsBean findStaffDetailsForFlag(Integer staffId, String curdate);

}
	