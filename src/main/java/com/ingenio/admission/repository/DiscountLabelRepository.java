package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.FeeDiscountLabelModel;



public interface DiscountLabelRepository extends JpaRepository<FeeDiscountLabelModel, Integer> {
	
	@Query("select a from FeeDiscountLabelModel a where a.schoolMasterModel.schoolid=:schoolId and a.standardMasterModel.standardId=:standardId "
			+ "and a.yearMasterModel.yearId=:yearId and (STR_TO_DATE(a.discountDate,'%d-%m-%Y')>=STR_TO_DATE(:date,'%d-%m-%Y') or a.discountDate is null) ")
	List<FeeDiscountLabelModel> findDiscountLabels(Integer schoolId,Integer standardId,Integer yearId,String date);
}
