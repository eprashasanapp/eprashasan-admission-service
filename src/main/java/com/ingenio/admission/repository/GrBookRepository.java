package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.GRBookNameModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface GrBookRepository  extends JpaRepository<GRBookNameModel, Integer>{
	
	List<GRBookNameModel> findBySchoolMasterModelOrderByGrBookId(SchoolMasterModel schoolMasterModel);

}
