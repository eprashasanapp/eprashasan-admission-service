package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.SansthaLogo;

public interface SansthaLogoRepository extends JpaRepository<SansthaLogo, Integer>{

	@Query("select a.imagePath from SansthaLogo a where a.sansthaKey=:sansthaKey and a.isDel='0'  ")
	String getSansthaLogoPath(String sansthaKey);
}
