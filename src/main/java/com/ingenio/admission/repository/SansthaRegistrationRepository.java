package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.SansthaRegistrationModel;

public interface SansthaRegistrationRepository extends JpaRepository<SansthaRegistrationModel, Integer> {
	
	SansthaRegistrationModel findBySansthaId(Integer sansthaId);

}
