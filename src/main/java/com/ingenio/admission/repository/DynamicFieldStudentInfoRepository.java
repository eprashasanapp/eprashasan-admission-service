package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.model.DynamicFieldStudentInformationModel;
import com.ingenio.admission.model.StudentMasterModel;

@Repository
public interface DynamicFieldStudentInfoRepository extends JpaRepository<DynamicFieldStudentInformationModel,Integer>{
	
	@Transactional
	@Modifying
	@Query("delete from DynamicFieldStudentInformationModel d WHERE d.studentMasterModel.studentId =?1 ")
	int deleteByStudentId(Integer studentId);

	List<DynamicFieldStudentInformationModel> findByStudentMasterModel(StudentMasterModel studentMasterModel);
	
	@Query(value="SELECT COALESCE(MAX(a.infoId),0)+1 from DynamicFieldStudentInformationModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
