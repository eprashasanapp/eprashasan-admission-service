package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.BannerImages;
import com.ingenio.admission.bean.CurrentApkVersionBean;
import com.ingenio.admission.bean.SchoolMappingBean;
import com.ingenio.admission.bean.StandardYearBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.AppUserModel;

@SuppressWarnings("unused")
@Repository
public interface UserRepository extends JpaRepository<AppUserModel, Integer>{

	@Query(value="SELECT COALESCE(MAX(a.appUserRoleId),0)+1 from AppUserModel a where a.schoolid=:schoolId ")
	int getMaxUserId(Integer schoolId);
	
	AppUserModel getUserByUsername(String str);

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(coalesce(a.schoolid,''),coalesce(c.schoolName,''),coalesce(c.schoolKey,''),a.appUserRoleId,a.username,a.roleName,a.activeFlag,a.firstName,a.lastName) from AppUserModel a "
			+ "left join AppUserRoleModel b on a.appUserRoleId=b.appUserRoleId left join SchoolMasterModel c "
			+ "on c.schoolid=a.schoolid where a.username=:username  order by a.schoolid ")
	 List<UserDetailsBean> getUserRoleAndSchoolDetails(String username);
	
	@Query("select new com.ingenio.admission.bean.UserDetailsBean(coalesce(d.sansthaId,''),coalesce(d.sansthaName,''),coalesce(d.sansthaKey,''),a.appUserRoleId,a.username,a.roleName,a.activeFlag,a.firstName,a.lastName) from AppUserModel a "
			+ "left join AppUserRoleModel b on a.appUserRoleId=b.appUserRoleId left join SanthaUserMappToSchoolModel c on a.appUserRoleId=c.sansthaUserMappId left join  "
			+ " SansthaRegistrationModel d on c.sansthaRegistrationModel.sansthaId=d.sansthaId "
			+ " where a.username=:username and a.appUserRoleId=:userId order by d.sansthaId ")
	 List<UserDetailsBean> getUserRoleAndSansthaOfficerDetails(String username,Integer userId);
	
	@Query("select new com.ingenio.admission.bean.UserDetailsBean(coalesce(d.sansthaId,''),coalesce(d.sansthaName,''),coalesce(d.sansthaKey,''),a.appUserRoleId,a.username,a.roleName,a.activeFlag,a.firstName,a.lastName) from AppUserModel a "
			+ "left join AppUserRoleModel b on a.appUserRoleId=b.appUserRoleId left join "
			+ "SansthaRegistrationModel d on d.sansthaId=a.appUserRoleId "
			+ " where a.username=:username and a.appUserRoleId=:userId order by d.sansthaId ")
	 List<UserDetailsBean> getUserRoleAndSuperOfficerDetails(String username,Integer userId);
	
	@Query("select new com.ingenio.admission.bean.UserDetailsBean(a.appUserRoleId,a.staffId,a.schoolid,"
			+ "a.roleName,a.userActiveOrInActiveStatus,b.isOrganization) from AppUserModel a "
			+ "left join SchoolMasterModel b on a.schoolid=b.schoolid "
			+ "where a.appUserRoleId=:userId and a.schoolid=:schoolId1 ")
	UserDetailsBean findByAppUserRoleId(Integer userId, Integer schoolId1);

	@Transactional
	@Modifying
	@Query("update AppUserModel a set a.IMEICode=:IMEICode , a.deviceType=:deviceId  where a.username=:userName ")
   void updateAppUser(String userName , String IMEICode, String deviceId);

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(a.appUserRoleId,a.staffId,a.schoolid,a.roleName) from AppUserModel a where a.appUserRoleId=:userId and a.IMEICode=:iMEICode and a.deviceType=:deviceId and a.activeFlag='0'  ")
	UserDetailsBean findByAppUserRoleId(Integer userId, String deviceId, String iMEICode);
	
	@Query("select new com.ingenio.admission.bean.UserDetailsBean(coalesce(a.schoolid,''),coalesce(c.schoolName,''),coalesce(c.schoolKey,'') , a.appUserRoleId,a.username,a.roleName,a.activeFlag,a.firstName,a.lastName) from AppUserModel a "
			+ "left join AppUserRoleModel b on a.appUserRoleId=b.appUserRoleId  "
			+ "left join SchoolMasterModel c "
			+ "on c.schoolid=a.schoolid where a.username=:username and a.schoolid=:schoolId  and ( a.roleName='ROLE_STUDENT' or a.roleName='ROLE_PARENT1' or a.roleName='ROLE_PARENT2' )  order by a.schoolid ")
	List<UserDetailsBean> checkExistingUser(String username,Integer schoolId);

	@Query("select a.currentVersion from AndroidConfigurationModel a   where a.androidId='1' ")
	String getCurrentApkVersion();

	@Query("select new com.ingenio.admission.bean.StandardYearBean(coalesce(b.year,''),coalesce(b.yearId,0),coalesce(g.standardName,''),coalesce(g.standardId,0),coalesce(a.renewStudentId,0)) from StudentStandardRenewModel a "
			+ "left join StandardMasterModel g on a.standardMasterModel.standardId=g.standardId and g.isDel='0'  "
			+ "left join YearMasterModel b on a.yearMasterModel.yearId=b.yearId and b.isDel='0'  "
			+ " where a.studentMasterModel.studentId=:studentId ")
	List<StandardYearBean> getStandardYear(Integer studentId);

	
	@Query(value="SELECT a.appUserRoleId from AppUserModel a where a.schoolid=:schoolId and a.roleName='ROLE_PRINCIPAL' ")
	Integer getprincipalUserId(Integer schoolId);

	@Query("select a.currentVersion from AndroidConfigurationModel a where a.androidId='2' ")
	String getCurrentApkVersionForCalendar();

	@Query(value="select new com.ingenio.admission.bean.BannerImages(COALESCE(a.bannerImagesId,0),COALESCE(a.fileName,''),"
			+ " COALESCE(a.imagePath,''),COALESCE(a.yearName,''),COALESCE(a.monthId,0) ) "
			+ " from BannerImagesModel a  where a.schoolMasterModel.schoolid=:schoolId  ")
	List<BannerImages> getAllBannerList(Integer schoolId);

	@Query(value="select new com.ingenio.admission.bean.BannerImages(COALESCE(a.eprashasanImageId,0),COALESCE(a.fileName,''),"
			+ " COALESCE(a.imagePath,''))"
			+ " from EprashsanImagesModel a   ")
	List<BannerImages> getAllCommonImagesList();

//	@Query("select new com.ingenio.admission.bean.SchoolMappingBean(coalesce(c.schoolid,0),"
//			+ "coalesce(c.schoolName,'')) from SchoolMappingOnlineAdmissionModel a "
//			+ "left join SchoolMasterModel c "
//			+ "on c.schoolid=a.schoolMasterModel.schoolid where a.schoolId=:schoolId ")
//	List<SchoolMappingBean> getAllSchoolMappingList(Integer schoolId);

	@Query("select new com.ingenio.admission.bean.SchoolMappingBean(coalesce(c.schoolid,0),"
			+ "coalesce(c.schoolName,'')) from SchoolMappingOnlineAdmissionModel a "
			+ "left join SchoolMasterModel c "
			+ "on c.schoolid=a.schoolMasterModel.schoolid where a.schoolId=:schoolId "
			+ "AND a.forOnlineAdmissionFlag = :onlineAdmissionFlag ")
	List<SchoolMappingBean> getAllSchoolMappingList(Integer schoolId, Integer onlineAdmissionFlag);
	
	@Query("select new com.ingenio.admission.bean.CurrentApkVersionBean(max(a.androidVersionId), "
			+ "a.versionCode,a.versionName,a.instructions,a.isMandatory,a.roleName) from AndroidVersionConfigurationModel a  ")
	List<CurrentApkVersionBean> getCurrentApkVersionDetails();

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(a.appUserRoleId,a.staffId,a.schoolid,a.roleName,a.userActiveOrInActiveStatus,b.isOrganization) from AppUserModel a "
			+ "left join SchoolMasterModel b on a.schoolid=b.schoolid "
			+ "where a.appUserRoleId=:userId ")
	UserDetailsBean findByAppUserRoleId(Integer userId);
	
}
