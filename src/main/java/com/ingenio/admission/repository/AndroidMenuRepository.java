package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.AndroidMenuBean;
import com.ingenio.admission.bean.NotificationMenuBean;
import com.ingenio.admission.model.AndroidMenuModel;

@Repository
public interface AndroidMenuRepository extends JpaRepository<AndroidMenuModel, Integer> {

	@Query(value = "select new com.ingenio.admission.bean.AndroidMenuBean(b.androidMenuGroupId,b.androidMenuGroupName,a.masterMenuId,coalesce(a.menuName,''),coalesce(a.imageFilePath,'')) "
				 + "from AndroidMenuModel c "
				 + "left join AndroidMenuMasterModel a on c.androidMenuMasterModel.masterMenuId = a.masterMenuId "
				 + "left join AndroidMenuMasterGroupModel b on b.androidMenuGroupId = a.androidMenuMasterGroupModel.androidMenuGroupId "
				 + "WHERE c.schoolMasterModel.schoolid =:schoolId and c.appRoleModel.roleName=:roleName and c.isDel='0' and b.androidMenuGroupId=:groupId ")
	List<AndroidMenuBean> findBySchoolMasterModel(Integer schoolId,String roleName, Integer groupId);

	
//	@Query(value = "select new com.ingenio.admission.bean.AndroidMenuBean(b.androidMenuGroupId,b.androidMenuGroupName,b.priority ) "
//			 + "from AndroidMenuMasterGroupModel b order by b.priority ")		 
//	List<AndroidMenuBean> getGroups();

	@Query(value = "select new com.ingenio.admission.bean.AndroidMenuBean(b.androidMenuGroupId,b.androidMenuGroupName, "
			+ "CASE WHEN :priorityType = 0 THEN b.priority ELSE b.newPriority END) "
			+ "from AndroidMenuMasterGroupModel b "
			+ "ORDER BY CASE WHEN :priorityType = 0 THEN b.priority ELSE b.newPriority END ")
	List<AndroidMenuBean> getGroups(Integer priorityType);

	@Query(value = "select new com.ingenio.admission.bean.AndroidMenuBean(a.masterMenuId,coalesce(a.menuName,''),coalesce(a.imageFilePath,'')) "
			 + "from AndroidMenuModel c "
			 + "left join AndroidMenuMasterModel a on c.androidMenuMasterModel.masterMenuId = a.masterMenuId "
			 + "WHERE c.schoolMasterModel.schoolid =:schoolId and c.appRoleModel.roleName=:roleName and c.isDel='0' ")
	List<AndroidMenuBean> findBySchoolMasterModel(Integer schoolId, String roleName);

	@Query(value = "select new com.ingenio.admission.bean.AndroidMenuBean(a.masterMenuId,coalesce(a.menuName,''),coalesce(a.imageFilePath,'')) "
			 + "from AndroidMenuModel c "
			 + "left join AndroidMenuMasterModel a on c.androidMenuMasterModel.masterMenuId = a.masterMenuId "
			 + "WHERE c.schoolMasterModel.schoolid =:schoolId  and c.menuId=:menuNo and  c.appRoleModel.roleName=:reverseRoles and  c.isDel='0' ")
	List<AndroidMenuBean> findBySchoolMasterModel(Integer schoolId, Integer menuNo,String reverseRoles);

	@Query(value = "select new com.ingenio.admission.bean.NotificationMenuBean(a.menuId,coalesce(count(a.seenUnseenStatusFlag),0)) "
			 + "from SaveSendNotificationModel a "
			 + "WHERE a.renewStaffId=:renewId and a.typeFlag=:typeFlag and a.seenUnseenStatusFlag=0 and a.successFailureFlag=1 group by a.menuId ")
	List<NotificationMenuBean> getMenuWiseUnseenNotificationCount(Integer renewId, String typeFlag);



}
