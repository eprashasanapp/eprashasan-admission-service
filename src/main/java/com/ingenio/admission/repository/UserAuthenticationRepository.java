package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.FeeSendSmsNumbersMstrModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface UserAuthenticationRepository extends JpaRepository<FeeSendSmsNumbersMstrModel, Integer>{
	
	List<FeeSendSmsNumbersMstrModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

}

