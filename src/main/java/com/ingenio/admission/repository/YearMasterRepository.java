package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.YearMasterModel;

@Repository
public interface YearMasterRepository extends JpaRepository<YearMasterModel, Integer>{

	List<YearMasterModel> findBySchoolMasterModelOrderByYear(SchoolMasterModel schoolMasterModel);

	YearMasterModel findByYearId(Integer currentYear);

	@Query("select a from YearMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.year=:academicYearText and a.isDel='0' ")
	YearMasterModel getYearId(String academicYearText, Integer schoolId);

	@Query("select a.year from YearMasterModel a where a.yearId=:yearId ")
	String getYear(Integer yearId);

	@Query("select a.academicStartDate from YearMasterModel a where a.yearId=:yearId ")
	String findDefaultRenewAdmissionDate(Integer yearId);

}
