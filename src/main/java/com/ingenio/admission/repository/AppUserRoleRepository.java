package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.model.AppUserRoleModel;

@Repository
public interface AppUserRoleRepository extends JpaRepository<AppUserRoleModel, Integer>{
	
	@Transactional
	@Modifying
	@Query(value = "delete from AppUserRoleModel d WHERE d.schoolid=:schoolId and "
			+ " d.username =:newRegUserName  ")
	void deleteRecords(String newRegUserName, Integer schoolId);

	@Query("select a.roleName from AppUserRoleModel a where a.appUserRoleId=:userId")
	String getUserRole(Integer userId);

}
