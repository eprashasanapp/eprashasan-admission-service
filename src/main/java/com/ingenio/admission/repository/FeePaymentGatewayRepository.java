package com.ingenio.admission.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.EmailRequestBean;
import com.ingenio.admission.bean.FeeBean;
import com.ingenio.admission.bean.FeeIdBean;
import com.ingenio.admission.bean.FeeProspectBean;
import com.ingenio.admission.bean.PaymentSettingBean;
import com.ingenio.admission.bean.SavePaidBean;
import com.ingenio.admission.bean.SchoolDetailBean;
import com.ingenio.admission.bean.SessionResponseBean;
import com.ingenio.admission.model.FeePaymentGatewayModel;

public interface FeePaymentGatewayRepository extends JpaRepository<FeePaymentGatewayModel, Integer> {

	@Query("Select new com.ingenio.admission.bean.FeeBean(COALESCE(a.transactionId,'-'),SUM(a.payFee) -SUM(a.discount) + SUM(a.paidFine) + SUM(a.convenienceFee),"
			+ "a.paidfeeDate,a.paymentStatus,a.processId,a.orderId, CONCAT(SUBSTRING(DATE_FORMAT(a.cDate, '%r'),1,5), " + 
			" SUBSTRING(DATE_FORMAT(a.cDate, '%r'),9,11)) ,COALESCE(b.receiptNo,'-'), COALESCE(b.receiptId,0) ) "
			+ "from FeePaymentGatewayModel a "
			+ "left join FeeReceiptModel b on a.feeReceiptModel.receiptId = b.receiptId "
			+ "where a.feeStudentFeeModel.studFeeId=:studentFeeId "
			+ "group by orderId order by processId desc")
	List<FeeBean> getTransactions(Integer studentFeeId);

	@Query("Select COALESCE(b.razorpayAccountId,0) from FeeBankToHeadSettingModel a "
			+ "left join BankToPaymentMappingModel b on "
			+ "a.feeChallanBankDetailsModel.bankId=b.bankId "
			+ "where a.feeHeadMasterModel.headId=:headId ")
	String findByFeeHeadMaster(Integer headId);

	@Query("Select new com.ingenio.admission.bean.FeeBean(a.feeSettingMultiModel.feeSetMultiId,a.feeStudentFeeModel.studFeeId,a.feeHeadMasterModel.headId,"
			+ "a.feeSubheadMasterModel.subheadId,a.payFee,a.discount,a.paidFine,a.paidfeeDate,a.payType,"
			+ "a.appUserRoleModel.appUserRoleId) "
			+ "FROM FeePaymentGatewayModel a where a.orderId = :orderId")
	List<FeeBean> findByOrderId(String orderId);

	@Query(value="SELECT coalesce(MAX(a.processId),0)+1 from FeePaymentGatewayModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxProcessId(Integer schoolId);

	@Modifying
	@Transactional
	@Query(value="Update FeePaymentGatewayModel a set a.orderId=:orderId where a.processId=:processId ")
	void updateOrderId(String orderId,Integer processId);

	@Modifying
	@Transactional
	@Query(value="Update FeePaymentGatewayModel a set a.transactionId=:transanctionId , a.paymentStatusMessage='Successful' , a.paymentStatus='Successful' , a.feeReceiptModel.receiptId=:receptId  where a.orderId=:orderId ")
	void updateTransactionId(String orderId, String transanctionId, Integer receptId);

	@Modifying
	@Transactional
	@Query(value="Update FeePaymentGatewayModel a set a.transferId=:transferId where a.orderId=:orderId ")
	void updateTransferId(String transferId, String orderId);

	@Modifying
	@Transactional
	@Query(value="Update FeePaymentGatewayModel a set a.paymentStatus=:status,a.paymentStatusCode=:statusCode,a.paymentStatusMessage=:statusMessage where a.processId=:processId and a.schoolMasterModel.schoolid=:schoolId ")
	void updateStatus(Integer schoolId, String processId, String statusCode, String status, String statusMessage);

	@Query("Select new com.ingenio.admission.bean.SavePaidBean(a.schoolMasterModel.schoolid, cast (a.processId as string ) ) "
			+ "FROM FeePaymentGatewayModel a where a.orderId = :orderId")
	List<SavePaidBean> getSchoolId(String orderId);

	@Query("Select new com.ingenio.admission.bean.FeeIdBean(a.studFeeId,a.schoolMasterModel.schoolid ) "
			+ "FROM FeeStudentFeeModel a "
			+ "left join StudentStandardRenewModel c on a.studentStandardRenewModel.renewStudentId=c.studentMasterModel.studentId "
			+ "where c.renewStudentId  = :renewStudentId")
	List<FeeIdBean> getStudentFeeId(Integer renewStudentId);

	@Query("select (sum(a.assignFee) + sum(coalesce(a.fine,0))) from FeeMultiSetting2Model a "
			+ "left join FeeTypeModel b on a.feeTypeModel.feeTypeId=b.feeTypeId " 
			+ "where a.standardMasterModel.standardId=:standardId and a.yearMasterModel.yearId=:yearId "
			+ "and a.schoolMasterModel.schoolid=:schoolId and b.feeType='Online Admission' ")
	Double getFeesForYearAndStandard(Integer schoolId, Integer standardId, Integer yearId);

	@Query("select settingFlag from FeesReceiptSettingModel a where a.schoolMasterModel.schoolid=:schoolId and a.remark='Is online payment applicable'")
	Integer getOnlineAdmissionSetting(Integer schoolId);

	@Query(value="SELECT coalesce(MAX(a.feeSettingId),0)+1 from FeeSettingModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxFeeSettingId(Integer schoolId);
	
	@Query("select  new com.ingenio.admission.bean.FeeIdBean(a.feeHeadMasterModel.headId,"
			+ "a.feeSubheadMasterModel.subheadId,a.assignFee,COALESCE(a.fine,0.0),COALESCE(a.dueDate,''),COALESCE(a.fineIncreament,0)) "
			+ "from FeeMultiSetting2Model a "
			+ "left join FeeTypeModel b on a.feeTypeModel.feeTypeId=b.feeTypeId " 
			+ "where a.standardMasterModel.standardId=:standardId and a.yearMasterModel.yearId=:yearId "
			+ "and a.schoolMasterModel.schoolid=:schoolId and b.feeType='Online Admission' ")
	List<FeeIdBean> getFeeStructureForYearAndStandard(Integer schoolId, Integer standardId, Integer yearId);

	@Query(value="SELECT coalesce(MAX(a.studFeeId),0)+1 from FeeStudentFeeModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxStudentFeeId(int schoolId);

	@Query(value="SELECT coalesce(MAX(a.feeSetMultiId),0)+1 from FeeSettingMultiModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxMultiId(int schoolId);

	@Query(value="SELECT coalesce((a.feeTypeId),0) from FeeTypeModel a where a.schoolMasterModel.schoolid=:schoolId and a.feeType='Online Admission' ")
	Integer getFeeTypeId(int schoolId);

	@Query("Select new com.ingenio.admission.bean.SchoolDetailBean(a.schoolid, a.schoolName ) "
			+ "FROM SchoolMasterModel a where a.schoolid = :schoolId")
	List<SchoolDetailBean> getSchoolDetails(Integer schoolId);

	@Query("select sum(a.assignFee) from FeeMultiSetting2Model a "
			+ "left join FeeTypeModel b on a.feeTypeModel.feeTypeId=b.feeTypeId " 
			+ "where a.standardMasterModel.standardId=:standardId and a.yearMasterModel.yearId=:yearId "
			+ "and a.schoolMasterModel.schoolid=:schoolId and b.feeType='Online Admission' ")
	Double getTotalAssignedFeeForYearAndStandard(Integer schoolId, Integer standardId, Integer yearId);

	@Query("select  new com.ingenio.admission.bean.SessionResponseBean(c.renewStudentId ,"
			+ "c.schoolMasterModel.schoolid,c.yearMasterModel.yearId ) "
			+ "from FeePaymentGatewayModel a "
			+ "left join FeeStudentFeeModel b on b.studFeeId=a.feeStudentFeeModel.studFeeId " 
			+ "left join StudentStandardRenewModel c on b.studentStandardRenewModel.renewStudentId=c.studentMasterModel.studentId " 
			+ "where a.orderId=:razorpay_order_id and a.transactionId=:razorpay_payment_id ")			
	List<SessionResponseBean> getSessionList(String razorpay_payment_id, String razorpay_order_id);

	@Query(value="SELECT coalesce(a.paymentGatewayId,0) from FeePaymentGatewayModel a where a.orderId=:orderId and a.transactionId=:transactionId ")
	Integer checkAlreadyPayment(String orderId, String transactionId);

	@Modifying
	@Transactional
	@Query(value="Update FeePaymentGatewayModel a set a.paymentStatus=:status where a.orderId=:orderId  ")
	void updateFailureStatus(String status, String orderId);

	@Query("select a.isApproval from StudentMasterModel a where a.studentId=:studentId ")
	String getStudentApprovalFlag(int studentId);

	@Query("select new com.ingenio.admission.bean.PaymentSettingBean(a.schoolMasterModel.schoolid , b.installment_wise_fee, "
			+ "b.minimum_fee_payment , a.amountOrFormula , a.amountOrFormulaFlag , a.installmentTypeFlag , a.installmentTypeDescription  ) from PayFeeSettingDetailsModel a "
			+ " left join PayFeeSettingModel b on b.payFeeSettingID = a.payFeeSettingModel.payFeeSettingID "
			+ "   where a.schoolMasterModel.schoolid=:schoolId and "
			+ " a.yearMasterModel.yearId=:yearId and a.standardMasterModel.standardId=:standardId  ")
	List<PaymentSettingBean> getPaymentSetting(Integer schoolId, Integer yearId, Integer standardId);

	
	
	@Query("select new com.ingenio.admission.bean.FeeProspectBean(coalesce(a.prospectFee,0.0) , coalesce(a.CGST,0.0), "
			+ "coalesce(a.SGST,0.0) , coalesce(a.headId,0) , coalesce(a.headName,'')  ) "
			+ " from FeeProspectFeesMasterModel a "
			+ "   where a.schoolMasterModel.schoolid=:schoolId and "
			+ " a.yearMasterModel.yearId=:yearId and a.standardMasterModel.standardId=:standardId  ")
	List<FeeProspectBean> getProspectFee(Integer schoolId, Integer yearId, Integer standardId);

	
	@Query("select new com.ingenio.admission.bean.FeeProspectBean(coalesce(a.registrationFlag,0)) "
			+ " from RegistrationSettingModel a "
			+ "   where a.schoolMasterModel.schoolid=:schoolId ")
	List<FeeProspectBean> getRegistrationSetting(Integer schoolId);


}
