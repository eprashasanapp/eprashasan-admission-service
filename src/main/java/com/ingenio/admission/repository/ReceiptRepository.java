package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeeReceiptModel;

public interface ReceiptRepository extends JpaRepository<FeeReceiptModel, Integer>{

}
