package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.model.StudentMasterModel;


@Repository
public interface StudentDetailsRepository extends JpaRepository<StudentMasterModel, Integer> {



	@Query(value = "select new com.ingenio.admission.bean.StudentDetailsBean(sm.studentId,CONCAT(COALESCE(sm.studInitialName,''),' ',COALESCE(sm.studFName,''),' ',COALESCE(sm.studMName,''),' ',COALESCE(sm.studLName,'')),sm.studentRegNo ,"
			+ "ssr.rollNo,sm.grBookName.grBookId,sm.mmobile,ssr.renewStudentId   )"
			+" from StudentMasterModel sm " 
			+"left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and ssr.schoolMasterModel.schoolid=sm.schoolMasterModel.schoolid) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+"and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 "
			+"and ssr.yearMasterModel.yearId=:yearId and ssr.standardMasterModel.standardId=:standardId and "
			+"ssr.divisionMasterModel.divisionId=:divisionId  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) order by ssr.rollNo,sm.studentRegNo  ")
	List<StudentDetailsBean> getDivisionwiseStudentDetails(Integer schoolId, Integer yearId, Integer standardId,
			Integer divisionId,String renewdate);
	

	
	


} 
