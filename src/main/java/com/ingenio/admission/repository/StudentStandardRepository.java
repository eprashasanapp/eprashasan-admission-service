package com.ingenio.admission.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.model.StudentStandardRenewModel;


@Repository
public interface StudentStandardRepository extends JpaRepository<StudentStandardRenewModel, Integer> {

	@Query("select y.yearId from StudentStandardRenewModel s left join YearMasterModel y on s.yearMasterModel.yearId=y.yearId "
			+ "where s.studentMasterModel.studentId=:studentId and s.alreadyRenew=1 ")
	Integer getCurentYear(Integer studentId);

//	@Transactional
//	@Modifying
//	@Query(value="update StudentStandardRenewModel a set a.divisionMasterModel.divisionId=:divisionId,a.rollNo=:rollNo,a.isEdit='1',a.editBy.appUserRoleId=:userId,a.editDate=CURRENT_DATE() where a.renewStudentId=:renewStudentId ")
//	void updateDivisionAndRollNo(Integer renewStudentId, Integer divisionId, Integer rollNo, Integer userId);

	@Transactional
	@Modifying
	@Query(value="update StudentStandardRenewModel a set a.divisionMasterModel.divisionId=:divisionId,a.rollNo=:rollNo,a.isEdit='1', a.editDate=CURRENT_DATE() where a.renewStudentId=:renewStudentId ")
	void updateDivisionAndRollNo(Integer renewStudentId, Integer divisionId, Integer rollNo);
	
	@Transactional
	@Modifying
	@Query(value="update StudentStandardRenewModel a set a.divisionMasterModel.divisionId=:divisionId,a.standardMasterModel.standardId=:standardId,a.rollNo=:rollNo,a.isEdit='1',a.editBy.appUserRoleId=:userId,a.editDate=CURRENT_DATE() where a.renewStudentId=:renewStudentId ")
	void updateStandardDivisionAndRollNo(Integer renewStudentId, Integer divisionId, Integer rollNo, Integer standardId, Integer userId);

	@Transactional
	@Modifying
	@Query(value="update StudentStandardRenewModel a set a.alreadyRenew='0',a.isEdit='1',a.editBy.appUserRoleId=:userId,a.editDate=CURRENT_DATE() where a.renewStudentId=:renewStudentId ")
	void updateAllreadyRenewFlag(Integer renewStudentId, Integer userId);
	
	@Query(value="select a from StudentStandardRenewModel a where a.renewStudentId=:renewStudentId and a.alreadyRenew=1 ")
	StudentStandardRenewModel getRenewModel(Integer renewStudentId);

	@Transactional
	@Modifying
	@Query(value="update StudentStandardRenewModel a set a.standardMasterModel.standardId=:standardId,a.isEdit='1',a.editBy.appUserRoleId=:userId,a.editDate=CURRENT_DATE() where a.renewStudentId=:renewStudentId ")
	void updateStandard(Integer renewStudentId, Integer standardId, Integer userId);

	@Transactional
	@Modifying
	@Query(value="update StudentStandardRenewModel a set a.collegeLeavingDate='',a.remark='',a.progress='',a.conduct='',a.reason='',a.editDate=CURRENT_DATE(),a.isEdit='1',a.editBy.appUserRoleId=:userId where a.renewStudentId=:renewStudentId ")
	void updateCollegeLeavingDate(Integer renewStudentId,Integer userId);

} 
