package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeePaidFeeModel;

public interface PaidFeeRepository extends JpaRepository<FeePaidFeeModel, Integer> {

}
