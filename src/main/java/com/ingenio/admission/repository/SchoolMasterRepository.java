package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.SchoolMappingBean;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface SchoolMasterRepository extends JpaRepository<SchoolMasterModel, Integer>{
	
	SchoolMasterModel findBySchoolid(Integer schoolId);

}
