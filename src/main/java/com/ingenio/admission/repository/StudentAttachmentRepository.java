package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.StudentDynamicDetailsBean;
import com.ingenio.admission.model.StudentAttachmentModel;

public interface StudentAttachmentRepository extends JpaRepository<StudentAttachmentModel,Integer>{
	
	@Query("select new com.ingenio.admission.bean.StudentDynamicDetailsBean(a.studentAttachmentId,a.attachmentMasterModel.attachmentMasterId,a.fileName,a.filePath) from StudentAttachmentModel a "
			+ "where a.studentMasterModel.studentId=?1 and a.isDel='0' ")
	List<StudentDynamicDetailsBean> getAttachmentDetails(Integer studentId);

	@Transactional
	@Modifying
	@Query(value = "delete from StudentAttachmentModel d WHERE d.studentMasterModel.studentId =?1 ")
	int deleteBy(Integer studentId);

	@Query(value="SELECT COALESCE(MAX(a.studentAttachmentId),0)+1 from StudentAttachmentModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
