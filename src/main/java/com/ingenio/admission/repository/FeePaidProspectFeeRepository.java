package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.FeePaidProspectFeesModel;
import com.ingenio.admission.model.FeePayTypeModel;
import com.ingenio.admission.model.SchoolMasterModel;

public interface FeePaidProspectFeeRepository extends JpaRepository<FeePaidProspectFeesModel, Integer> {

	
	
	@Query(value="SELECT COALESCE(MAX(a.feePaidProspectFeeId),0)+1 from FeePaidProspectFeesModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);
	


}
