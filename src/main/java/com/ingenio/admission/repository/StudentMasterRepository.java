package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.AppRoleBean;
import com.ingenio.admission.bean.StudentAdmissionBean;
import com.ingenio.admission.bean.StudentCountBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.CertificateExcelSchoolWiseModel;
import com.ingenio.admission.model.StudentMasterModel;

@Repository
public interface StudentMasterRepository extends JpaRepository<StudentMasterModel, Integer>{

	@Query("select new com.ingenio.admission.bean.StudentAdmissionBean(b.studentId,a.appUserRoleId,b.studentRegNo) from AppUserModel a "
		  + "left join StudentMasterModel b on a.staffId=b.studentId   "
		  + "left join StudentStandardRenewModel c on c.studentMasterModel.studentId=b.studentId "
		  + "where  a.username=?1 and a.schoolid=?2 and a.isDel='0' and a.roleName='ROLE_STUDENT' and b.isAdmissionCancelled='1' ")
	List<StudentAdmissionBean> findByUser(String userName,Integer schoolId);
	
	@Query("select new com.ingenio.admission.bean.StudentAdmissionBean(b.studentId,a.appUserRoleId,b.studentRegNo) from AppUserModel a "
			  + "left join StudentMasterModel b on a.staffId=b.studentId "
			  + "left join StudentStandardRenewModel c on c.studentMasterModel.studentId=b.studentId "
			  + "where  b.studentId=?1 and a.schoolid=?2 and a.isDel='0' and a.roleName='ROLE_STUDENT' and b.isAdmissionCancelled='1' ")
		List<StudentAdmissionBean> getByStudentId(Integer studId,Integer schoolId);

	@Query("select new com.ingenio.admission.bean.StudentAdmissionBean(c.renewStudentId) from  "
		  + "StudentStandardRenewModel c "
		  + "where  c.studentMasterModel.studentId=?1 and c.yearMasterModel.yearId=?2  and c.schoolMasterModel.schoolid=?3 and c.isDel='0' ")
	List<StudentAdmissionBean> findRenewId(Integer studentId, Integer academicYear,Integer schoolId);

	List<StudentMasterModel> findByStudentId(Integer studentId);

	@Query("select new com.ingenio.admission.bean.StudentAdmissionBean(c.yearMasterModel.yearId,COALESCE(b.studentRegNo,''),COALESCE(b.grBookName.grBookId,'0'),"
			+"COALESCE(b.admissionDate,''),COALESCE(b.studentFormNo,''),COALESCE(b.medium,''), "  
			+"COALESCE(b.studInitialName,''),COALESCE(b.studMName,''),COALESCE(b.birthDate,''),COALESCE(b.studGender,''),COALESCE(b.category.categoryId,''),"
			+ "COALESCE(b.mothertonge,''),COALESCE(b.studFName,''), " 
			+"COALESCE(b.studLName,''),COALESCE(b.birthPlace,''),b.castemaster.casteId,b.religion.religionId,"
			+ "COALESCE(b.nationlity,''),COALESCE(b.fatherName,''),COALESCE(b.motherName,''), " 
			+"b.feducation, b.foccupation,b.fincome,b.fage,b.femail,b.laddress,b.lcity,b.lpincode,b.lstate," 
			+"b.lcountry,b.paddress,b.pcity,b.ppincode,b.pstate,b.pcountry,b.mmobile,b.fmobile,b.memail, " 
			+"b.meducation,b.moccupation,b.mage,b.mincome,b.mtelephone,b.previousState,b.previousSchool, " 
			+"b.ftelephone,b.previousReason,c.standardMasterModel.standardId,a.username,b.studentId,a.appUserRoleId,c.renewStudentId) from AppUserModel a "
			+"left join StudentMasterModel b on a.staffId=b.studentId "
			+"left join StudentStandardRenewModel c on c.studentMasterModel.studentId=b.studentId "
			+"where  a.username=?1 and a.password=?2  and a.schoolid=?3 and a.isDel='0' and b.isAdmissionCancelled='1' ")
	List<StudentAdmissionBean> getStudentDetails(String userName, String password, Integer schoolId);

	@Query(value="select ifnull(max(cast(replace(a.provisionalRegNo,'P','') as unsigned)),0)+1 from student_master a where  a.schoolid=?1 and  a.isDel='0'",nativeQuery = true)
	String getMaxProvisionalNo(Integer schoolId);

	@Query("select new com.ingenio.admission.bean.StudentAdmissionBean(c.yearMasterModel.yearId,b.studentRegNo,b.grBookName.grBookId,"
			+"b.admissionDate,b.studentFormNo,b.medium, "  
			+"COALESCE(b.studInitialName,''),COALESCE(b.studMName,''),COALESCE(b.birthDate,''),COALESCE(b.studGender,''),"
			+ "b.category.categoryId,b.mothertonge,b.studFName, " 
			+"COALESCE(b.studLName,''),COALESCE(b.birthPlace,''),b.castemaster.casteId,b.religion.religionId,b.nationlity,b.fatherName,b.motherName, " 
			+"b.feducation, b.foccupation,b.fincome,b.fage,b.femail,b.laddress,b.lcity,b.lpincode,b.lstate," 
			+"b.lcountry,b.paddress,b.pcity,b.ppincode,b.pstate,b.pcountry,b.mmobile,b.fmobile,b.memail, " 
			+"b.meducation,b.moccupation,b.mage,b.mincome,b.mtelephone,b.previousSchool, " 
			+"b.ftelephone,b.previousState,b.previousReason,c.standardMasterModel.standardId,a.username,b.studentId,a.appUserRoleId,c.renewStudentId) "
			+ "from AppUserModel a "
			+"left join StudentMasterModel b on a.staffId=b.studentId "
			+"left join StudentStandardRenewModel c on c.studentMasterModel.studentId=b.studentId "
			+"where  b.studentId=?1 and a.isDel='0' and b.isAdmissionCancelled='1' ")
	List<StudentAdmissionBean> findDetailsByStudentId(Integer studentId);

	@Query(value="select ifnull(max(cast(replace(a.Student_formNo,'F','') as unsigned)),0)+1 from student_master a where  a.schoolid=?1 and a.isDel='0'",nativeQuery = true)
	String findMaxFormNumber(Integer schoolId);

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(COALESCE(b.studentRegNo,''),b.studentId,COALESCE(b.studInitialName,''),COALESCE(b.studFName,''),"
			+ "COALESCE(b.studMName,''),COALESCE(b.studLName,''),COALESCE(b.studGender,''),COALESCE(c.castName,''),"
			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.medium,''),COALESCE(b.mmobile,''),COALESCE(b.birthDate,''),"
			+ "COALESCE(b.birthPlace,''),COALESCE(b.nationlity,''),COALESCE(b.admissionDate,''),f.renewStudentId,b.grBookName.grBookId,"
			+ "COALESCE(b.fatherName,''),COALESCE(b.motherName,''),COALESCE(g.standardName,''),"
			+ "COALESCE(h.divisionName,''),COALESCE(f.rollNo,0),COALESCE(g.standardId,0),"
			+ "COALESCE(f.isOnlineAdmission,0),COALESCE(f.onlineAdmissionApprovalFlag,0),COALESCE(f.renewStudentId,0),COALESCE(h.divisionId,0)) "
			+ "from StudentMasterModel b "
			+ "left join CasteMasterModel c on c.casteId=b.castemaster.casteId and c.isDel='0' "
			+ "left join CategoryMasterModel d on d.categoryId=b.category.categoryId and d.isDel='0'  "
			+ "left join ReligionMasterModel e on e.religionId=b.religion.religionId and e.isDel='0'  "
			+ "left join StudentStandardRenewModel f on f.studentMasterModel.studentId=b.studentId and f.isDel='0'  "
			+ "left join StandardMasterModel g on f.standardMasterModel.standardId=g.standardId and g.isDel='0'  "
			+ "left join DivisionMasterModel h on f.divisionMasterModel.divisionId=h.divisionId and h.isDel='0'  "
			+ "where b.studentId=:studentId and f.alreadyRenew='1' and ((f.collegeLeavingDate is null or f.collegeLeavingDate='') or "
			+ "(str_to_date(f.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(b.admissionDate ,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and b.isDel='0' and b.isAdmissionCancelled=1 ")
	UserDetailsBean findStudentDetails(Integer studentId,String renewdate);
	
	
	@Query("select new com.ingenio.admission.bean.UserDetailsBean(COALESCE(b.studentRegNo,''),b.studentId,COALESCE(b.studInitialName,''),COALESCE(b.studFName,''),"
			+ "COALESCE(b.studMName,''),COALESCE(b.studLName,''),COALESCE(b.studGender,''),COALESCE(c.castName,''),"
			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.medium,''),COALESCE(b.mmobile,''),COALESCE(b.birthDate,''),"
			+ "COALESCE(b.birthPlace,''),COALESCE(b.nationlity,''),COALESCE(b.admissionDate,''),f.renewStudentId,b.grBookName.grBookId,"
			+ "COALESCE(b.fatherName,''),COALESCE(b.motherName,''),COALESCE(g.standardName,''),"
			+ "COALESCE(h.divisionName,''),COALESCE(f.rollNo,0),COALESCE(g.standardId,0)) "
			+ "from StudentMasterModel b "
			+ "left join CasteMasterModel c on c.casteId=b.castemaster.casteId and c.isDel='0' "
			+ "left join CategoryMasterModel d on d.categoryId=b.category.categoryId and d.isDel='0'  "
			+ "left join ReligionMasterModel e on e.religionId=b.religion.religionId and e.isDel='0'  "
			+ "left join StudentStandardRenewModel f on f.studentMasterModel.studentId=b.studentId and f.isDel='0'  "
			+ "left join StandardMasterModel g on f.standardMasterModel.standardId=g.standardId and g.isDel='0'  "
			+ "left join DivisionMasterModel h on f.divisionMasterModel.divisionId=h.divisionId and h.isDel='0'  "
			+ "where f.renewStudentId=:studentId and f.alreadyRenew='1' and ((f.collegeLeavingDate is null or f.collegeLeavingDate='') or "
			+ "(str_to_date(f.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(f.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and b.isDel='0' and b.isAdmissionCancelled=1 ")
	UserDetailsBean findStudentDetails1(Integer studentId,String renewdate);
	
	@Query("select new com.ingenio.admission.bean.UserDetailsBean(COALESCE(b.studentRegNo,''),b.studentId,COALESCE(b.studInitialName,''),COALESCE(b.studFName,''),"
			+ "COALESCE(b.studMName,''),COALESCE(b.studLName,''),COALESCE(b.studGender,''),COALESCE(c.castName,''),"
			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.medium,''),COALESCE(b.mmobile,''),COALESCE(b.birthDate,''),"
			+ "COALESCE(b.birthPlace,''),COALESCE(b.nationlity,''),COALESCE(b.admissionDate,''),f.renewStudentId,b.grBookName.grBookId,"
			+ "COALESCE(b.fatherName,''),COALESCE(b.motherName,''),COALESCE(g.standardName,''),"
			+ "COALESCE(h.divisionName,''),COALESCE(f.rollNo,0),COALESCE(g.standardId,0)) "
			+ "from StudentMasterModel b "
			+ "left join CasteMasterModel c on c.casteId=b.castemaster.casteId and c.isDel='0' and b.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid "
			+ "left join CategoryMasterModel d on d.categoryId=b.category.categoryId and d.isDel='0' and d.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join ReligionMasterModel e on e.religionId=b.religion.religionId and e.isDel='0' and e.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StudentStandardRenewModel f on f.studentMasterModel.studentId=b.studentId and f.isDel='0' and f.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
			+ "left join StandardMasterModel g on f.standardMasterModel.standardId=g.standardId and g.isDel='0' and f.schoolMasterModel.schoolid=g.schoolMasterModel.schoolid "
			+ "left join DivisionMasterModel h on f.divisionMasterModel.divisionId=h.divisionId and h.isDel='0' and f.schoolMasterModel.schoolid=h.schoolMasterModel.schoolid "
			+ "where b.studentId=?1 and f.alreadyRenew=1 and b.isAdmissionCancelled='1' ")
	UserDetailsBean findStudentDetailsFromRenewId(Integer renewId);

	@Query(value="SELECT COALESCE(MAX(a.renewStudentId),0)+1 from StudentStandardRenewModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxRenewId(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.studentId),0)+1 from StudentMasterModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	@Query(value="SELECT b.grBookId from StudentMasterModel a left join GRBookNameModel b on a.grBookName.grBookId=b.grBookId where a.studentId=:studentId ")
	String findGrName(Integer studentId);

	@Query(value="SELECT new com.ingenio.admission.bean.AppRoleBean(a.firstName,a.lastName) from AppUserModel a where "
			+ "a.username=:userName and a.password=:password and a.roleName='ROLE_PRINCIPAL' and a.schoolid=:schoolId ")
	AppRoleBean getAdminDetails(String userName, String password, Integer schoolId);

	@Query(value="SELECT new com.ingenio.admission.bean.AppRoleBean(a.firstName,a.lastName) from AppUserModel a where "
			+ "a.username=:userName and a.password=:password and ( a.roleName='ROLE_STUDENT' or a.roleName='ROLE_PARENT1' or a.roleName='ROLE_PARENT2' ) and a.schoolid=:schoolId ")
	List<AppRoleBean> getStudentInfoList(String userName, String password, Integer schoolId);

	

	@Query(value = "select new com.ingenio.admission.bean.StudentCountBean(std.standardName as standardName,std.standardId as standardId, Coalesce(div.divisionName,'NA') as divisionName,"
			+ "Coalesce(div.divisionId,0) as divisionId ,count(distinct sm.studentId) as totalCount,"
			+ "count(distinct case when sm.studGender='Male' then sm.studentId end) as boysCount, "
			+ "count(distinct case when sm.studGender='Female' then sm.studentId end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join StandardMasterModel std on (ssr.standardMasterModel.standardId=std.standardId and std.isDel='0') "
			+ "left join DivisionMasterModel div on (ssr.divisionMasterModel.divisionId=div.divisionId and div.isDel='0') "
			+ "left join StaffSubjectDetailsModel sb on (sb.divisionMasterModel.divisionId=div.divisionId and sb.isDel='0' and sb.standardMasterModel.standardId=std.standardId ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ " and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 "
			+ "and ssr.yearMasterModel.yearId=:yearId and sb.staffBasicDetailsModel.staffId=:staffId and sb.isClassTeacher='1' group by 1,2,3,4 order by divisionId")
	List<StudentCountBean> getStudentStrengthForTeacher(Integer schoolId, Integer yearId, String renewdate, Integer staffId);

	@Query(value="select ifnull(max(grBookId),0) from gr_book_name a where  a.schoolid=?1 and  a.isDel='0'",nativeQuery = true)
	String getMaxGrBookId(Integer schoolId);

	@Query("select new com.ingenio.admission.bean.UserDetailsBean(COALESCE(b.studentRegNo,''),b.studentId,COALESCE(b.studInitialName,''),COALESCE(b.studFName,''),"
			+ "COALESCE(b.studMName,''),COALESCE(b.studLName,''),COALESCE(b.studGender,''),COALESCE(c.castName,''),"
			+ "COALESCE(d.categoryName,''),COALESCE(e.religionName,''),COALESCE(b.medium,''),COALESCE(b.mmobile,''),COALESCE(b.birthDate,''),"
			+ "COALESCE(b.birthPlace,''),COALESCE(b.nationlity,''),COALESCE(b.admissionDate,''),f.renewStudentId,b.grBookName.grBookId,"
			+ "COALESCE(b.fatherName,''),COALESCE(b.motherName,''),COALESCE(g.standardName,''),"
			+ "COALESCE(h.divisionName,''),COALESCE(f.rollNo,0),COALESCE(g.standardId,0),COALESCE(b.admissionDate,''),"
			+ "COALESCE(b.studentFormNo,''),COALESCE(b.mothertonge,''),COALESCE(b.laddress,'')) "
			+ "from StudentMasterModel b "
			+ "left join CasteMasterModel c on c.casteId=b.castemaster.casteId and c.isDel='0' "
			+ "left join CategoryMasterModel d on d.categoryId=b.category.categoryId and d.isDel='0'  "
			+ "left join ReligionMasterModel e on e.religionId=b.religion.religionId and e.isDel='0'  "
			+ "left join StudentStandardRenewModel f on f.studentMasterModel.studentId=b.studentId and f.isDel='0'  "
			+ "left join StandardMasterModel g on f.standardMasterModel.standardId=g.standardId and g.isDel='0'  "
			+ "left join DivisionMasterModel h on f.divisionMasterModel.divisionId=h.divisionId and h.isDel='0'  "
			+ " where b.studentRegNo=:studRegNo and "
			+ " b.schoolMasterModel.schoolid=:schoolId  ")
	List<UserDetailsBean> getStudentInformation(String studRegNo, Integer schoolId);

	
	@Query("select new com.ingenio.admission.bean.StudentAdmissionBean(b.fileName , b.filePath ) "
			+ "from StudentMasterModel a "
			+"left join StudentAttachmentModel b on b.studentMasterModel.studentId=a.studentId "
			+"where  a.studentId=:studentId ")
	List<StudentAdmissionBean> getStudentAttachmentsforPrint(Integer studentId);

	@Query("select a from StudentMasterModel a where a.mmobile=?1 and  a.schoolMasterModel.schoolid=?2 ")
	List<StudentMasterModel> findByMobileNo(String mobileNo, Integer schoolId);

	@Transactional
	@Modifying
	@Query(value = "delete from StudentStandardRenewModel d WHERE d.schoolMasterModel.schoolid=:schoolId and "
			+ " d.studentMasterModel.studentId =:studentId  ")
	void deleteRenewRecord(Integer schoolId, Integer studentId);

	@Transactional
	@Modifying
	@Query(value = "delete from StudentMasterModel d WHERE d.schoolMasterModel.schoolid=:schoolId and "
			+ " d.studentId =:studentId  ")
	void deleteStudentIdRecord(Integer schoolId, Integer studentId);

	@Query(value = "select new com.ingenio.admission.bean.StudentCountBean(ssr.renewStudentId ) from  StudentStandardRenewModel  ssr  "
			+ "left join StudentMasterModel sm on (sm.studentId = ssr.studentMasterModel.studentId ) "
			+ "where ssr.yearMasterModel.yearId=:yearId and ssr.studentMasterModel.studentId=:studentId ")
	List<StudentCountBean> getrenewId(Integer studentId, Integer yearId);

	
	@Query("select new com.ingenio.admission.bean.StudentDetailsBean(sm.studentId,ssr.renewStudentId,CONCAT(COALESCE(sm.studInitialName,''),' ',COALESCE(sm.studFName,''),' ',COALESCE(sm.studMName,''),' ',COALESCE(sm.studLName,'')),sm.studentRegNo ,"
			+ "sm.studGender , ssr.rollNo,ym.yearId,ym.year,smm.standardId,smm.standardName,dm.divisionId,dm.divisionName,sm.mmobile,COALESCE(ssr.collegeLeavingDate,'') ) "
			+"from StudentStandardRenewModel ssr  " 
			+"left join StudentMasterModel sm on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+"left join YearMasterModel ym on (ssr.yearMasterModel.yearId=ym.yearId and ym.isDel='0' and sm.schoolMasterModel.schoolid = ym.schoolMasterModel.schoolid) "
			+"left join StandardMasterModel smm on (ssr.standardMasterModel.standardId=smm.standardId and smm.isDel='0' and sm.schoolMasterModel.schoolid = smm.schoolMasterModel.schoolid) "
			+"left join DivisionMasterModel dm on (ssr.divisionMasterModel.divisionId=dm.divisionId  and sm.schoolMasterModel.schoolid = dm.schoolMasterModel.schoolid) "
			+"where ssr.alreadyRenew='1' "
			+"and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1   "
			+"and sm.studentRegNo= :regNo  " )
	List<StudentDetailsBean> StudentSearchByRegNo(Integer schoolId, String regNo);

	@Query("select new com.ingenio.admission.bean.StudentDetailsBean(sm.studentId,ssr.renewStudentId,CONCAT(COALESCE(sm.studInitialName,''),' ',COALESCE(sm.studFName,''),' ',COALESCE(sm.studMName,''),' ',COALESCE(sm.studLName,'')),sm.studentRegNo ,"
			+ "sm.studGender , ssr.rollNo,ym.yearId,ym.year,smm.standardId,smm.standardName,dm.divisionId,dm.divisionName,sm.mmobile,COALESCE(ssr.collegeLeavingDate,'') ) "
			+"from StudentStandardRenewModel ssr  " 
			+"left join StudentMasterModel sm on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+"left join YearMasterModel ym on (ssr.yearMasterModel.yearId=ym.yearId and ym.isDel='0' and sm.schoolMasterModel.schoolid = ym.schoolMasterModel.schoolid) "
			+"left join StandardMasterModel smm on (ssr.standardMasterModel.standardId=smm.standardId and smm.isDel='0' and sm.schoolMasterModel.schoolid = smm.schoolMasterModel.schoolid) "
			+"left join DivisionMasterModel dm on (ssr.divisionMasterModel.divisionId=dm.divisionId  and sm.schoolMasterModel.schoolid = dm.schoolMasterModel.schoolid) "
			+"where ssr.alreadyRenew='1' "
			+"and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1   "
			+"and sm.studentRegNo= :regNo  " )
	StudentDetailsBean StudentSearchByRegNo1(Integer schoolId, String regNo);
	
	@Query("select new com.ingenio.admission.bean.StudentDetailsBean(sm.studentId,ssr.renewStudentId,CONCAT(COALESCE(sm.studInitialName,''),' ',COALESCE(sm.studFName,''),' ',COALESCE(sm.studMName,''),' ',COALESCE(sm.studLName,'')),sm.studentRegNo ,"
			+ "sm.studGender , ssr.rollNo,ym.yearId,ym.year,smm.standardId,smm.standardName,dm.divisionId,dm.divisionName,sm.mmobile,COALESCE(ssr.collegeLeavingDate,'') ) "
			+"from StudentStandardRenewModel ssr  " 
			+"left join StudentMasterModel sm on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+"left join YearMasterModel ym on (ssr.yearMasterModel.yearId=ym.yearId and ym.isDel='0' and sm.schoolMasterModel.schoolid = ym.schoolMasterModel.schoolid) "
			+"left join StandardMasterModel smm on (ssr.standardMasterModel.standardId=smm.standardId and smm.isDel='0' and sm.schoolMasterModel.schoolid = smm.schoolMasterModel.schoolid) "
			+"left join DivisionMasterModel dm on (ssr.divisionMasterModel.divisionId=dm.divisionId  and sm.schoolMasterModel.schoolid = dm.schoolMasterModel.schoolid) "
			+"where ssr.alreadyRenew='1' "
			+"and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1   "
			+"and ssr.renewStudentId=:renewId")
	List<StudentDetailsBean> StudentSearchByStudentName(Integer schoolId, Integer renewId);

	@Query("select new com.ingenio.admission.bean.StudentDetailsBean(sm.studentId,ssr.renewStudentId,CONCAT(COALESCE(sm.studInitialName,''),' ',COALESCE(sm.studFName,''),' ',COALESCE(sm.studMName,''),' ',COALESCE(sm.studLName,'')),sm.studentRegNo ,"
			+ "sm.studGender , ssr.rollNo,ym.yearId,ym.year,smm.standardId,smm.standardName,dm.divisionId,dm.divisionName,sm.mmobile,COALESCE(ssr.collegeLeavingDate,'') ) "
			+"from StudentStandardRenewModel ssr  " 
			+"left join StudentMasterModel sm on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+"left join YearMasterModel ym on (ssr.yearMasterModel.yearId=ym.yearId and ym.isDel='0' and sm.schoolMasterModel.schoolid = ym.schoolMasterModel.schoolid) "
			+"left join StandardMasterModel smm on (ssr.standardMasterModel.standardId=smm.standardId and smm.isDel='0' and sm.schoolMasterModel.schoolid = smm.schoolMasterModel.schoolid) "
			+"left join DivisionMasterModel dm on (ssr.divisionMasterModel.divisionId=dm.divisionId  and sm.schoolMasterModel.schoolid = dm.schoolMasterModel.schoolid) "
			+"where ssr.alreadyRenew='1' "
			+"and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1   "
			+"and ssr.renewStudentId=:renewId")
	StudentDetailsBean StudentSearchByStudentName1(Integer schoolId, Integer renewId);
	
	@Transactional
	@Modifying
	@Query("update StudentMasterModel a set a.studentRegNo=?1 where a.schoolMasterModel.schoolid=?2 and a.studentId=?3 ")
	Integer updateStudentRegNumber(String studentRegNumber, Integer schoolId, Integer studentId);

	//CertificateExcelSchoolWiseModel save(CertificateExcelSchoolWiseModel certificateExcelSchoolWiseModel);
}
