package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.StudentDynamicDetailsBean;
import com.ingenio.admission.model.StudentDynamicDetailsMasterModel;
@Repository
public interface DynamicStudentDetailsRepository extends JpaRepository<StudentDynamicDetailsMasterModel, Integer>{

	@Transactional
	@Modifying
	@Query(value = "delete from StudentDynamicDetailsMasterModel d WHERE d.studentMasterModel.studentId =?1 and d.dynamicTitle=?2 and d.yearMasterModel.yearId=?3 ")
	int deleteByStudentId(Integer studentId, String dynamicTitleArr, Integer yearId);
	
	@Query("select new com.ingenio.admission.bean.StudentDynamicDetailsBean(a.dynamicTitle,a.dynamicValue) from StudentDynamicDetailsMasterModel a "
			+ "where a.studentMasterModel.studentId=?1 and a.isDel='0' ")
	List<StudentDynamicDetailsBean> getDynamicDetails1ForEdit(Integer studentId);

	@Query(value="SELECT COALESCE(MAX(a.dynamicDetailsId),0)+1 from StudentDynamicDetailsMasterModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
