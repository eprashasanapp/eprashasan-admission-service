package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.StaffAttendanceModel;

public interface StaffAttendanceSaveRepository extends JpaRepository<StaffAttendanceModel, Integer>{ 

}
