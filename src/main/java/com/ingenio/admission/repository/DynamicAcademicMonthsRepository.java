package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.DynamicAcademicMonthsModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface DynamicAcademicMonthsRepository extends JpaRepository<DynamicAcademicMonthsModel, Integer>{
	
	List<DynamicAcademicMonthsModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

	@Query("select a.fromMonth from DynamicAcademicMonthsModel a where a.schoolMasterModel.schoolid=:schoolId and a.id=(select max(b.id) from DynamicAcademicMonthsModel b where b.schoolMasterModel.schoolid=:schoolId ) ")
	String getFromMonth(Integer schoolId);

}
