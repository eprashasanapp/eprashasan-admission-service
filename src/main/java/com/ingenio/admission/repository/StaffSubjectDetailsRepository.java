package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.StaffSubjectDetailsModel;

public interface StaffSubjectDetailsRepository extends JpaRepository<StaffSubjectDetailsModel, Integer> {
	
	@Query("select a from StaffSubjectDetailsModel a where a.staffBasicDetailsModel.staffId=?1 and a.isClassTeacher='1' group by a.standardMasterModel.standardId  ")
	List<StaffSubjectDetailsModel> findClassteacher(Integer staffId);

}
