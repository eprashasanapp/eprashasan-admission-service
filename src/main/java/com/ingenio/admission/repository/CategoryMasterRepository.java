package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.CategoryMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface CategoryMasterRepository  extends JpaRepository<CategoryMasterModel, Integer>{
	
	List<CategoryMasterModel> findBySchoolMasterModelOrderByCategoryId(SchoolMasterModel schoolMasterModel);
}
