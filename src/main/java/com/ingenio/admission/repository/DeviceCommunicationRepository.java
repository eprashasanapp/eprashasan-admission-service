package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.AttendanceRFIDPunchModel;

@Repository
public interface DeviceCommunicationRepository extends JpaRepository<AttendanceRFIDPunchModel, Integer>{

	@Query(value="SELECT COALESCE(MAX(a.attendanceRfid),0)+1 from AttendanceRFIDPunchModel a  ")
	String getMaxId();

	
	@Query(value="SELECT a.appUserRoleId from AppUserModel a where a.schoolid=:schoolId and a.roleName='ROLE_PRINCIPAL' ")
	Integer getprincipalUserId(Integer schoolId);

	@Query(value="SELECT a.staffAttendanceID from StaffAttendanceModel a where a.schoolMasterModel.schoolid=:schoolId and a.staffBasicDetailsModel.staffId=:staffId and a.date=:splitDate ")
	Integer getStaffId(Integer staffId, Integer schoolId, String splitDate);
	
	@Query(value="SELECT COALESCE(MAX(a.staffAttendanceID),0)+1 from StaffAttendanceModel a where a.schoolMasterModel.schoolid=:schoolId  ")
	Integer getStaffAttendanceId(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.attandenceClasswiseId),0)+1 from AttendanceClassWiseCatloagModel a where a.schoolMasterModel.schoolid=:schoolId  ")
	Long getattendanceMaxId(Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.schedularTimeId),0)+1 from MaintainTimeForRFIDSchedularModel a where a.schoolMasterModel.schoolid=:schoolId  ")
	Integer gettimeschedularId(Integer schoolId);


	/*
	 * @Query(
	 * value="SELECT a.SchoolMasterModel.schoolId from RFIDSchoolSettingSchedularModel a where a.deviceId=:deviceId and a.organizationId=:organizationId "
	 * ) int getSchoolId(int deviceId, int organizationId);
	 */





}
