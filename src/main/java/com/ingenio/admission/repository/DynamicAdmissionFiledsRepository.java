package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.DynamicAdmissionFiledsModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface DynamicAdmissionFiledsRepository extends JpaRepository<DynamicAdmissionFiledsModel, Integer> {

	 
	@Query(value = "select d from DynamicAdmissionFiledsModel d where d.fieldName is not null and d.fieldName!='' and d.schoolMasterModel.schoolid=?1 and d.isDel='0' ")
	List<DynamicAdmissionFiledsModel> getOldDynamicFields(Integer schoolId);

	@Query(value = "select d from DynamicAdmissionFiledsModel d where d.fieldName=?1 and d.schoolMasterModel.schoolid=?2 and d.isDel='0' ")
	List<DynamicAdmissionFiledsModel> getOldDynamicFieldValueId(String fieldName, Integer schoolId);

	@Query(value ="select * from dynamicadmissionfileds a where a.schoolid=?1 and a.fieldName=?2" ,nativeQuery = true)
	List<DynamicAdmissionFiledsModel> getValueToPopulateForDynamic2Field(Integer schoolId, String fieldName);

	List<DynamicAdmissionFiledsModel> findBySchoolMasterModelOrderByFieldId(SchoolMasterModel schoolMasterModel);

}
