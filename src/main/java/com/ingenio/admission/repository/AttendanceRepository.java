package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.AttendanceModel;

public interface AttendanceRepository extends JpaRepository<AttendanceModel, Integer>{

	@Query(value="select COALESCE(count(c.attendedDate),'0') from AttendanceModel c where c.presentStudentId LIKE %:renewId% and c.yearMasterModel.yearId=:yearId ")
	String getStudentPresentCount(String renewId, Integer yearId);
	
	@Query(value="select COALESCE(count(c.attendedDate),'0') from AttendanceModel c where c.absentStudentId LIKE %:renewId% and c.yearMasterModel.yearId=:yearId ")
	String getStudentAbsentCount(String renewId, Integer yearId);
	
}
