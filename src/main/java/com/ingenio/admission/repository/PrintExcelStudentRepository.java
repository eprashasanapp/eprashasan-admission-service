package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.CertificateExcelSchoolWiseModel;

public interface PrintExcelStudentRepository extends JpaRepository<CertificateExcelSchoolWiseModel, Integer> {

	@Query(value="SELECT COALESCE(MAX(a.cerExcelSchoolWiseId),0)+1 from CertificateExcelSchoolWiseModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
