package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.ElectiveGroupBean;
import com.ingenio.admission.bean.StudentDynamicDetailsBean;
import com.ingenio.admission.bean.SubGroupBean;
import com.ingenio.admission.bean.SubjectBean;
import com.ingenio.admission.model.AssignSubjectToStudentModel;

@Repository
public interface AssignSubjectToStudentRepository extends JpaRepository<AssignSubjectToStudentModel, Integer>{
	
	@Query("select new com.ingenio.admission.bean.StudentDynamicDetailsBean(a.subjectId) from AssignSubjectToStudentModel a "
			+ "where a.studentId=:studentId and a.isDel='0' ")
	List<StudentDynamicDetailsBean> getAssignedSubjects(@Param("studentId") String studentId);

	@Transactional
	@Modifying
	@Query(value = "delete from AssignSubjectToStudentModel d WHERE d.studentId =:studentId ")
	int deleteByStudentId(String studentId);

	@Query(value="SELECT COALESCE(MAX(a.assignSubjectId),0)+1 from AssignSubjectToStudentModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);
	
	@Query("select new com.ingenio.admission.bean.SubjectBean(a.subjectId,a.subjectName,a.courseCode,false) from SubjectModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.compulElectiveFlag='Compulsory' and a.standardMasterModel.standardId=:standardId "
			+ "and a.yearMasterModel.yearId=:yearId ")
	List<SubjectBean> getCumplosrySubjects(@Param("schoolId") Integer schoolId,@Param("yearId") Integer yearId,@Param("standardId") Integer standardId);
	
	@Query("select new com.ingenio.admission.bean.ElectiveGroupBean(a.groupId,a.groupName,a.selectionType,a.maxSubGroups,a.minSubGroups,a.totalSubjects) from ExamGroupModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.standardMasterModel.standardId=:standardId "
			+ "and a.yearMasterModel.yearId=:yearId ")
	List<ElectiveGroupBean> getGroups(@Param("schoolId") Integer schoolId,@Param("yearId") Integer yearId,@Param("standardId") Integer standardId);
	
	@Query("select new com.ingenio.admission.bean.SubGroupBean(a.subGroupId,a.subGroupName,a.selectionType,a.maxSubjects,a.minSubjects) from ExamSubGroupModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.examGroupModel.groupId=:groupId ")
	List<SubGroupBean> getSubGroups(@Param("schoolId") Integer schoolId,@Param("groupId") Integer groupId);
	
	@Query("select new com.ingenio.admission.bean.SubjectBean(a.subjectId,a.subjectName,a.courseCode,false) from SubjectModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.compulElectiveFlag='Elective' and a.examGroupModel.groupId=:groupId "
			+ "and a.examSubGroupModel.subGroupId=:subGroupId ")
	List<SubjectBean> getElectiveSubjects(@Param("schoolId") Integer schoolId,@Param("groupId") Integer groupId,@Param("subGroupId") Integer subGroupId);
	
	@Query("select a.totalSubjects from ExamStdSubCountModel a "
			+ "where a.schoolMasterModel.schoolid=:schoolId and a.standardMasterModel.standardId=:standardId and a.yearMasterModel.yearId=:yearId ") 
	Integer getTotalSubjectCount(@Param("schoolId") Integer schoolId,@Param("yearId") Integer yearId,@Param("standardId") Integer standardId);
}
