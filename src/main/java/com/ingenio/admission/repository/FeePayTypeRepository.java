package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeePayTypeModel;
import com.ingenio.admission.model.SchoolMasterModel;

public interface FeePayTypeRepository extends JpaRepository<FeePayTypeModel, Integer> {
	
	List<FeePayTypeModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

}
