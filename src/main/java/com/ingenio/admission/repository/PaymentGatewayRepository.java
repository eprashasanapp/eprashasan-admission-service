package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeePaymentGatewayModel;

public interface PaymentGatewayRepository extends JpaRepository<FeePaymentGatewayModel, Integer> {

}
