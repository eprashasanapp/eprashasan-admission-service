package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.RFIDSchoolSettingSchedularModel;

@Repository
public interface RFIDSchoolSettingSchedularRepository extends JpaRepository<RFIDSchoolSettingSchedularModel, Integer>{

//	@Query("SELECT r FROM RFIDSchoolSettingSchedularModel r WHERE r.schoolMasterModel.schoolid = :schoolId "
//			+ "AND r.organizationId = :orgId ")
//	public RFIDSchoolSettingSchedularModel getDeviceId(Integer schoolId, Integer orgId);

	@Query("SELECT r FROM RFIDSchoolSettingSchedularModel r WHERE r.schoolMasterModel.schoolid = :schoolId ")
	public RFIDSchoolSettingSchedularModel getDeviceId(Integer schoolId);
	
}
