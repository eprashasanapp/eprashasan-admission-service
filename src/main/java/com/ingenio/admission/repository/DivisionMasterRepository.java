package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.DivisionMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface DivisionMasterRepository  extends JpaRepository<DivisionMasterModel, Integer>{
	
	
	List<DivisionMasterModel> findBySchoolMasterModelOrderByDivisionId(SchoolMasterModel schoolMasterModel);


}
