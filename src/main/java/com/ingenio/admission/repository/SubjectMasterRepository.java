package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.model.SubjectModel;

@Repository
public interface SubjectMasterRepository extends JpaRepository<SubjectModel, Integer>{
	
	@Query("select new com.ingenio.admission.bean.AdmissionSettingBean(a.subjectCatId,a.subjectCatName,b.subjectId,b.subjectName) from SubjectCategoryModel a "
			  + "left join SubjectModel b on a.subjectCatId=b.subCatId and a.schoolMasterModel.schoolid = b.schoolMasterModel.schoolid "
			  + "where a.schoolMasterModel.schoolid=?3 and a.isDel='0' and b.yearMasterModel.yearId=?1 and b.standardMasterModel.standardId=?2 order by a.subjectCatId, b.subjectId ")
		List<AdmissionSettingBean> findAssignedSubjects(Integer yearId,Integer standardId,Integer schoolId);

}
