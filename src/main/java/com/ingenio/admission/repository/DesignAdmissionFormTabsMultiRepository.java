package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.model.DesignAdmiFormFieldsModel;
import com.ingenio.admission.model.DesignAdmiFormTabsMultiModel;
import com.ingenio.admission.model.SchoolMasterModel;


@Repository
public interface DesignAdmissionFormTabsMultiRepository extends JpaRepository<DesignAdmiFormTabsMultiModel, Integer> {
	
	@Query("select c from DesignAdmiFormTabsMultiModel c WHERE c.designAdmiFormTabsModel.tabsId =?1 and c.schoolMasterModel.schoolid=?2 and c.designAdmiFormLegendModel.legendId=?3 and c.isDel='0' ")
	List<DesignAdmiFormTabsMultiModel> getFieldTabsAndLegendWise(Integer tabId, Integer schoolId,Integer legendId);

	
	@Transactional
	@Modifying
	@Query(value = "update design_admi_form_tabs_multi set isDisplay=?1,isRequired=?2,isYearly=?3,priority=?4,validationFlag=?5,legendId=?6 WHERE fieldId =?7 and tabsId=?8  ",nativeQuery = true)
	Integer updateField(String string, String string2, String string3, int l,String validationFlag,int legendId, int m,int tabId);

	@Transactional
	@Modifying
	@Query(value = "update design_admi_form_tabs_multi set isDisplay=?1,isRequired=?2,isYearly=?3,priority=?4,validationFlag=?5 WHERE fieldId =?6 and tabsId=?7  ",nativeQuery = true)
	Integer updateField2(String string, String string2, String string3, int parseInt, String string4, int parseInt2,Integer currentTabName);

	@Query(value = "select c from DesignAdmiFormFieldsModel c WHERE c.designAdmiFormTabsModel.tabsId =?1 and c.schoolMasterModel.schoolid=?2 and isDel='0' ")
	List<DesignAdmiFormFieldsModel> getAllFeildsNames(Integer tabId, Integer schoolId);

	@Query(value = "select c from DesignAdmiFormTabsMultiModel c WHERE c.designAdmiFormTabsModel.tabsId =?1 and c.schoolMasterModel.schoolid=?2  and c.isStaticOrDynamic=2 and isDel='0' ")
	List<DesignAdmiFormTabsMultiModel> getOldDynamicFieldId(Integer tabId, Integer schoolId);

	@Query("select c from DesignAdmiFormTabsMultiModel c WHERE c.designAdmiFormTabsModel.tabsId =?1 and c.schoolMasterModel.schoolid=?2 and c.designAdmiFormLegendModel.legendId=?3 and c.fieldName=?4 and c.isDel='0' ")
	List<DesignAdmiFormTabsMultiModel> getFieldInformation(Integer currentTabName, Integer schoolId,Integer currentLegendIdFinal, String fieldName);

	List<DesignAdmiFormTabsMultiModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

	@Query("select new com.ingenio.admission.bean.AdmissionSettingBean(b.legendName,c.tabsName) from DesignAdmiFormTabsMultiModel a " 
			+ " left join DesignAdmiFormLegendModel b on b.legendId=a.designAdmiFormLegendModel.legendId and a.schoolMasterModel.schoolid = b.schoolMasterModel.schoolid " 
			+  " left join DesignAdmiFormTabsModel c on c.tabsId=a.designAdmiFormTabsModel.tabsId and a.schoolMasterModel.schoolid = c.schoolMasterModel.schoolid "  
			+ " where a.fieldName=?1 and a.isDisplay='1' and a.schoolMasterModel.schoolid=?2 and a.isDel='0' ")
	List<AdmissionSettingBean> checkStaticFieldIsDisplayOrNot(String staticFieldName, Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.fieldId),0)+1 from DesignAdmiFormTabsMultiModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	@Transactional
	@Modifying
	@Query(value = "update design_admi_form_tabs_multi set isYearly=?1 WHERE fieldId =?2 ",nativeQuery = true)
	void updateIsYearly(String flag, String fieldId);

	@Transactional
	@Modifying
	@Query(value = "update design_admi_form_tabs_multi set isRequired=?1 WHERE fieldId =?2 ",nativeQuery = true)
	void updateIsRequired(String string, String string2);

	@Transactional
	@Modifying
	@Query(value = "update design_admi_form_tabs_multi set isDisplay=?1 WHERE fieldId =?2 ",nativeQuery = true)
	void updateIsDisplay(String string, String string2);

}
