package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.bean.HomeScreenBean;
import com.ingenio.admission.bean.SchoolDetailBean;
import com.ingenio.admission.bean.StudentCountBean;
import com.ingenio.admission.model.AndroidHomeScreenMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;

public interface HomeScreenMasterRepository extends JpaRepository<AndroidHomeScreenMasterModel, Integer> {

	@Query(value="select new com.ingenio.admission.bean.HomeScreenBean(c.flag,c.title,c.completedTitle,c.remainingTitle,"
			+ "COALESCE(d.message,''),c.completedColor,c.remainingColor,c.screenType) "
			+ "from AndroidHomeScreenMasterModel c"
			+ " Left join AndroidHomeMessageCategoryModel d "
			+ "on d.messageCategoryId=c.androidHomeMessageCategoryModel.messageCategoryId and d.schoolMasterModel.schoolid = c.schoolMasterModel.schoolid  "
			+ "where c.role=:profileRole and c.schoolMasterModel.schoolid=:schoolId order by c.priority  ")
	List<HomeScreenBean> findByRole(String profileRole, Integer schoolId);
	
	@Query(value="select new com.ingenio.admission.bean.SchoolDetailBean(c.schoolid ,c.schoolName,d.groupOfSchoolName,b.username, "
			+"COALESCE(CONCAT(b.firstName,' ',b.lastName),'') as principalName, COALESCE(e.sregNo,''),COALESCE(c.address,''),COALESCE(c.schoolKey,'') )"
			+ "from SchoolMasterModel c Left join GroupOfSchoolModel d on d.groupOfSchoolId = c.groupOfSchoolModel.groupOfSchoolId "
			+ "Left join AppUserModel b on c.schoolid = b.schoolid "
			+ "left join StaffBasicDetailsModel e on e.staffId=b.staffId and b.roleName='ROLE_PRINCIPAL' "
			+"where c.schoolid=:schoolId and b.roleName='ROLE_PRINCIPAL' ")
	List<SchoolDetailBean> findSchoolDetails(Integer schoolId);

	@Query(value = "select new com.ingenio.admission.bean.StudentCountBean(std.standardName as standardName,std.standardId as standardId, Coalesce(div.divisionName,'NA') as divisionName, Coalesce(div.divisionId,0) as divisionId ,count(sm.studentId) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join StandardMasterModel std on (ssr.standardMasterModel.standardId=std.standardId and std.isDel='0' and sm.schoolMasterModel.schoolid = std.schoolMasterModel.schoolid ) "
			+ "left join DivisionMasterModel div on (ssr.divisionMasterModel.divisionId=div.divisionId and div.isDel='0' and sm.schoolMasterModel.schoolid = div.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and (ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='' ) "
			+ "and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 "
			+ "and ssr.yearMasterModel.yearId=:yearId and ssr.standardMasterModel.standardId=:standardId and ssr.divisionMasterModel.divisionId=:divisionId group by 1,2,3,4 ")
	StudentCountBean getDivisionStrengthwithId(Integer schoolId, Integer yearId, Integer standardId, Integer divisionId);
	
	
	

}
