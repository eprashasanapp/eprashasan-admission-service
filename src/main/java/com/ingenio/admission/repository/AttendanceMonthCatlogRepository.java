package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.admission.model.AttendanceMonthCatlogModel;

public interface AttendanceMonthCatlogRepository extends JpaRepository<AttendanceMonthCatlogModel, Integer>{

	@Query(value="select a from AttendanceMonthCatlogModel a where a.studentStandardRenewModel.renewStudentId=:renewStudentId and a.monthId IN(:monthlist)")
	List<AttendanceMonthCatlogModel> getNextMonthAvailableDetails(Integer renewStudentId, List<Integer> monthlist);

	@Query(value="SELECT COALESCE(MAX(a.attandenceMonthCatalogId),0)+1 from AttendanceMonthCatlogModel a where a.schoolMasterModel.schoolid=:schoolId")
	String getMaxId(Integer schoolId);

	@Query(value="select a from AttendanceMonthCatlogModel a where a.studentStandardRenewModel.renewStudentId=:studentRenewId and a.monthId=:monthId and a.isDel='0'")
	AttendanceMonthCatlogModel getStudentDetails(Integer studentRenewId, Integer monthId);

}
