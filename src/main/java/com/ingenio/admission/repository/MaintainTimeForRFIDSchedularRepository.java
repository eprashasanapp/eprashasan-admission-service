package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.MaintainTimeForRFIDSchedularModel;

public interface MaintainTimeForRFIDSchedularRepository  extends JpaRepository<MaintainTimeForRFIDSchedularModel, Integer> {

}
