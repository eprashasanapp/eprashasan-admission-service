package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.admission.model.FeeSettingModel;

public interface FeeSettingRepository extends JpaRepository<FeeSettingModel, Integer> {

}
