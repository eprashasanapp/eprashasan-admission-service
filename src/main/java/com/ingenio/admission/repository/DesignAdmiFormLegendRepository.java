package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.model.DesignAdmiFormLegendModel;

@Repository
public interface DesignAdmiFormLegendRepository extends JpaRepository<DesignAdmiFormLegendModel, Integer>{

	@Query("select new com.ingenio.admission.bean.AdmissionSettingBean(a.tabsId,a.tabsName,b.legendId,b.legendName,c.fieldId,c.fieldName,c.fieldTypeFlag as fieldTypeId,c.isRequired,c.isStaticOrDynamic,c.validationFlag) from "
			 + "DesignAdmiFormTabsModel a left join DesignAdmiFormLegendModel b on b.designAdmiFormTabsModel.tabsId=a.tabsId and b.isDel='0' "
			 + "left join DesignAdmiFormTabsMultiModel c on c.designAdmiFormLegendModel.legendId=b.legendId and c.isDel='0' and c.schoolMasterModel.schoolid = a.schoolMasterModel.schoolid "
			 + "where a.schoolMasterModel.schoolid=?1 and a.isDel='0' and a.isTabDisplay='1' and c.isDisplay='1' order by a.tabsId, c.designAdmiFormLegendModel.legendId,c.priority ")
	List<AdmissionSettingBean> getTabsAndLegendsSetting(@Param("schoolId") Integer schoolId);

	@Query(value = "select c from DesignAdmiFormLegendModel c WHERE c.designAdmiFormTabsModel.tabsId =?1 and c.schoolMasterModel.schoolid=?2 and isDel='0' ")
	List<DesignAdmiFormLegendModel> findBySchoolMasterModel(Integer tabId, Integer schoolId);

	@Transactional
	@Modifying
	@Query(value = "delete from DesignAdmiFormLegendModel d WHERE d.designAdmiFormTabsModel.tabsId =?1 and d.legendId=?2 and d.schoolMasterModel.schoolid=?3 and d.isDel='0' ")
	Integer deleteLegendName(Integer tabId, Integer legendId, Integer schoolId);
	
	@Transactional
	@Modifying
	@Query(value = "update DesignAdmiFormLegendModel d set d.legendName=?1 WHERE d.legendId =?2 ")
	Integer updateTabsName(String legendName, Integer legendId);
	
	
	/*@Query("select new com.ingenio.admission.bean.AdmissionSettingBean(a.tabsId,a.tabsName,b.legendId,b.legendName,c.fieldId,c.fieldName,c.fieldTypeFlag as fieldTypeId,c.isRequired,c.isStaticOrDynamic,c.validationFlag) from "
			 + "DesignAdmiFormTabsModel a left join DesignAdmiFormLegendModel b on b.designAdmiFormTabsModel.tabsId=a.tabsId and b.isDel='0' "
			 + "left join DesignAdmiFormTabsMultiModel c on c.designAdmiFormLegendModel.legendId=b.legendId and c.isDel='0' "
			 + "where a.schoolMasterModel.schoolid=?1 and a.isDel='0' and c.isDisplayForAndroid='1' order by c.designAdmiFormLegendModel.legendId,c.priority ")
	List<AdmissionSettingBean> getTabsAndLegendsSettingForAndroid(@Param("schoolId") Integer schoolId);
*/
	@Query(value="SELECT COALESCE(MAX(a.legendId),0)+1 from DesignAdmiFormLegendModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);


}
