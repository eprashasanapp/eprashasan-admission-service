package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.admission.bean.NotificationBean;
import com.ingenio.admission.model.AndroidFCMTokenMaster;
import com.ingenio.admission.model.AppUserModel;
import com.ingenio.admission.model.AppUserRoleModel;
import com.ingenio.admission.model.SchoolMasterModel;

public interface FCMPushNotificationRepository extends JpaRepository<AndroidFCMTokenMaster, Integer> {

	@Query("select a from AndroidFCMTokenMaster a where a.token=:token ")
	List<AndroidFCMTokenMaster> getUsersByToken(String token);
	
	@Query("select a from AndroidFCMTokenMaster a where a.mobileNo=:mobileNo ")
	List<AndroidFCMTokenMaster> getUsersByMobileNo(String mobileNo);
	
	@Query("select a from AppUserModel a where a.username=:mobileNo and (a.roleName!='ROLE_PARENT2' AND a.roleName!='ROLE_STUDENT') ")
	List<AppUserModel> getAppUsersByMobileNo(String mobileNo);
	
	@Query("select new com.ingenio.admission.model.AppUserModel(a.appUserRoleId,a.schoolid) from AppUserModel a where a.username=:mobileNo and (a.roleName!='ROLE_PARENT2' AND a.roleName!='ROLE_STUDENT') ")
	List<AppUserModel> getAppUsersByMobileNoNew(String mobileNo);
	
	@Transactional
	@Modifying
	@Query("delete from AndroidFCMTokenMaster a where a.token=:token ")
	void delete(String token);
	
	@Transactional
	@Modifying
	@Query("update AndroidFCMTokenMaster a set a.token=:token where a.mobileNo=:mobileNo ")
	List<AppUserRoleModel> update(String token,String mobileNo);

	@Transactional
	void deleteByMobileNoAndSchoolMasterModel(String mobileNo,SchoolMasterModel schoolMasterModel);

	@Transactional
	void deleteByToken(String token);
	
	@Transactional
	void deleteByMobileNo(String mobileNo);

	@Query(value = "SELECT f.fcmMasterTokenId FROM android_fcm_token_master f "
			+ "WHERE f.notificationUserId = ?2 AND f.schoolId = ?1 AND f.mobileNo = ?3 ", nativeQuery = true)
	List<Integer> checkIfExists(Integer schoolId, Integer userId, String mobileNo);

	@Transactional
	@Modifying
	@Query(value = "UPDATE android_fcm_token_master f SET f.token = ?4 "
			+ "WHERE f.notificationUserId = ?2 AND f.schoolId = ?1 AND f.mobileNo = ?3 ", nativeQuery = true)
	Integer updateToken(Integer schoolId, Integer userId, String mobileNo, String fcmToken);

	
	
	/*
	 * @Query(
	 * value="select new com.ingenio.admission.bean.NotificationBean(a.notificationType, COALESCE(a.title,'') as notificationTitle , COALESCE(a.message,'') as notificationmessage , "
	 * + "a.notificationDate , a.notificationTime )" +
	 * "from AndroidNotificationStatusModel a " +
	 * "left join StudentAssignHomeworkModel b on b.assignHomeworkId=a.joinId " +
	 * "left join StudentHomeworkAssignToModel c on c.studentAssignHomeworkModel.assignHomeworkId=b.assignHomeworkId "
	 * + "where c.studentStandardRenewModel.renewStudentId=:renewId ")
	 * List<NotificationBean> getAllNotifications(Integer renewId);
	 */
	
	

	
}
