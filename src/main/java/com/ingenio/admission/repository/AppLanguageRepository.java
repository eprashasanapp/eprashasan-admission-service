package com.ingenio.admission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.AppLanguageModel;


@Repository
public interface AppLanguageRepository extends JpaRepository<AppLanguageModel, Integer> {
	
}
