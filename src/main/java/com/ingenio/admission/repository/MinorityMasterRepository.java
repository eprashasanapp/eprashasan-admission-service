package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.MinorityMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface MinorityMasterRepository  extends JpaRepository<MinorityMasterModel, Integer>{
	
	
		List<MinorityMasterModel> findBySchoolIdOrderByMinorityId(SchoolMasterModel schoolMasterModel);
}
