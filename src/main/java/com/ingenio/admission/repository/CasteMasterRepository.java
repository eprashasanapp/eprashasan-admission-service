package com.ingenio.admission.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.admission.model.CasteMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;

@Repository
public interface CasteMasterRepository  extends JpaRepository<CasteMasterModel, Integer>{
	
	List<CasteMasterModel> findBySchoolMasterModelOrderByCasteId(SchoolMasterModel schoolMasterModel);
}
