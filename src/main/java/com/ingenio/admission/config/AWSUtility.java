package com.ingenio.admission.config;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;


public class AWSUtility {
	public static Regions clientRegion;
	public static AmazonS3 s3client;
	public  static String accessKey;
	public  static String secretKey;
	public static String endPoint = "";
	public static AWSCredentials credentials;

	static {
		try {
			 accessKey ="AKIAJSKKZCPGIUS5UXEA";	
			 secretKey ="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";	
			 endPoint = "s3.ap-south-1.amazonaws.com";
			 clientRegion = Regions.AP_SOUTHEAST_1;
			 //s3client = AmazonS3ClientBuilder.standard().withCredentials(new EnvironmentVariableCredentialsProvider()).withRegion(clientRegion).build();
			 credentials = new BasicAWSCredentials(accessKey, secretKey);
			 s3client = new AmazonS3Client(credentials).withRegion(Region.getRegion(clientRegion)).withEndpoint(endPoint);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static byte[] downloadFile(String path) throws IOException {
		try {
			if(path.contains("http")) {
				System.out.println("path "+path);
				if (extractIP(path) == null) {
					URI fileToBeDownloaded = new URI(path); 
					//System.out.println("uri path "+fileToBeDownloaded);
					AmazonS3URI s3URI = new AmazonS3URI(fileToBeDownloaded);
					//System.out.println("s3URI "+s3URI);
					S3Object object = s3client.getObject(new GetObjectRequest(s3URI.getBucket(),s3URI.getKey()));
					//System.out.println("object"+object);
					return IOUtils.toByteArray(object.getObjectContent());
				}else {
		             File f = new File(path);
					 return AWSUtility.getSelectedImage(f);
				}
			}else {
				 File f1 = new File(path);
				 return AWSUtility.getSelectedImage(f1);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getS3PreSignedUrl(String imagePath) {
		if (imagePath.contains(".com/")) {
			String[] arrayStr = imagePath.split(".com/");
			try {
				java.util.Date expiration = new java.util.Date();
				long expTimeMillis = expiration.getTime();
				expTimeMillis += 1000 * 60 * 10;
				expiration.setTime(expTimeMillis);
				String bucketName = "eprashasan";
				GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, arrayStr[1]).withMethod(HttpMethod.GET).withExpiration(expiration);
				URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
				return url.toString();
			} catch (Exception e) {
				System.out.println("imagePath--"+imagePath);
				e.printStackTrace();
				return null;
			}
		} else {
			return imagePath;
		}
		
	}

    public static boolean checkDimension(File file, Integer height, Integer width) {
    	Integer Imageheight = 0;
    	Integer Imagewidth = 0;
    	BufferedImage bimg;
		try {
			 bimg = ImageIO.read(file);
			 Imageheight = bimg.getHeight();
			 Imagewidth = bimg.getWidth();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	if((Imageheight > height || Imagewidth > width)) {
    		return true;
    	}else {
    		return false;
    	}
    }
    
	public static byte[] getSelectedImage(File filename) throws IOException{
		InputStream is;
		byte[] bytesImg = null;
		try {
			//is = new URL(filename.getAbsolutePath()).openStream();
			 is = new FileInputStream(filename);
			 bytesImg = IOUtils.toByteArray(is);
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return bytesImg;
	}
    
    public static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }
    
    public static String extractIP(String s) {
        java.util.regex.Matcher m = java.util.regex.Pattern.compile(
            "(?<!\\d|\\d\\.)" +
            "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])" +
            "(?!\\d|\\.\\d)").matcher(s);
        return m.find() ? m.group() : null;
    }
}
