package com.ingenio.admission.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.ingenio.admission.config.ConfigurationProperties;

@Component
//@PropertySource("file:C:/updatedwars/configuration/authentication.properties")
public class ConfigurationProperties {

	public static String uploadImageUrl;
	public static String studentPhotoUrl;
	public static String staffPhotoUrl;
	public static String sansthaOfficerPhotoUrl;
	public static String superOfficerPhotoUrl;
	public static String feeRestUrl;
	public static String schoolLogo;
	public static String sansthaLogo;
	public static String checkExistingUser;
	public static String addRoutingUserUrl;
	public static String postUrl;
	//public static String getExePathDB;
	
	

	
	public static  String hostingIP;
	
	public static  String paymentRatesUrl;
	
	public static  String paymentRatesCommonUrl;
	
	public static  String commissionDetailsUrl;
	
	public static String schoolNameSecretKey;
	
	public static String razorPayTransferAPI;
	
	public static String razorPayOrderAPI;
	
	public static String razorPayAuthorization;


	

	@Value("${hostingIP}")
	public  void setHostingIP(String hostingIp) {
		ConfigurationProperties.hostingIP = hostingIp;
	}

	@Value("${commissionDetailsUrl}")
	public  void setCommissionDetailsUrl(String commissionDetailsUrl) {
		ConfigurationProperties.commissionDetailsUrl = commissionDetailsUrl;
	}
	
	@Value("${schoolNameSecretKey}")
	public  void setSchoolNameSecretKey(String schoolNameSecretKey) {
		ConfigurationProperties.schoolNameSecretKey = schoolNameSecretKey;
	}

	@Value("${paymentRatesUrl}")
	public  void setPaymentRatesUrl(String paymentRatesUrl) {
		ConfigurationProperties.paymentRatesUrl = paymentRatesUrl;
	}
	
	@Value("${paymentRatesCommonUrl}")
	public void setPaymentRatesCommonUrl(String paymentRatesCommonUrl) {
		ConfigurationProperties.paymentRatesCommonUrl = paymentRatesCommonUrl;
	}

	@Value("${razorPayTransferAPI}")
	public  void setRazorPayTransferAPI(String razorPayTransferAPI) {
		ConfigurationProperties.razorPayTransferAPI = razorPayTransferAPI;
	}

	@Value("${razorPayOrderAPI}")
	public  void setRazorPayOrderAPI(String razorPayOrderAPI) {
		ConfigurationProperties.razorPayOrderAPI = razorPayOrderAPI;
	}

	@Value("${razorPayAuthorization}")
	public  void setRazorPayAuthorization(String razorPayAuthorization) {
		ConfigurationProperties.razorPayAuthorization = razorPayAuthorization;
	}

	public String getUploadImageUrl() {
		return uploadImageUrl;
	}

	@Value("${uploadImageUrl}")
	public void setUploadImageUrl(String uploadImageUrl) {
		ConfigurationProperties.uploadImageUrl = uploadImageUrl;
	}

	@Value("${studentPhotoUrl}")
	public void setStudentPhotoUrl(String studentPhotoUrl) {
		ConfigurationProperties.studentPhotoUrl = studentPhotoUrl;
	}

	@Value("${staffPhotoUrl}")
	public void setStaffPhotoUrl(String staffPhotoUrl) {
		ConfigurationProperties.staffPhotoUrl = staffPhotoUrl;
	}

	@Value("${sansthaOfficerPhotoUrl}")
	public void setSansthaOfficerPhotoUrl(String sansthaOfficerPhotoUrl) {
		ConfigurationProperties.sansthaOfficerPhotoUrl = sansthaOfficerPhotoUrl;
	}

	@Value("${superOfficerPhotoUrl}")
	public void setSuperOfficerPhotoUrl(String superOfficerPhotoUrl) {
		ConfigurationProperties.superOfficerPhotoUrl = superOfficerPhotoUrl;
	}

	@Value("${feeRestUrl}")
	public void setFeeRestUrl(String feeRestUrl) {
		ConfigurationProperties.feeRestUrl = feeRestUrl;
	}

	@Value("${schoolLogo}")
	public void setSchoolLogo(String schoolLogo) {
		ConfigurationProperties.schoolLogo = schoolLogo;
	}

	@Value("${sansthaLogo}")
	public void setSansthaLogo(String sansthaLogo) {
		ConfigurationProperties.sansthaLogo = sansthaLogo;
	}

	@Value("${checkExistingUser}")
	public void setCheckExistingUser(String checkExistingUser) {
		ConfigurationProperties.checkExistingUser = checkExistingUser;
	}
	
	@Value("${postUrl}")
	public void setPostUrl(String postUrl) {
		ConfigurationProperties.postUrl = postUrl;
	}

//	@Value("${addRoutingUserUrl}")
//	public void setAddRoutingUserUrl(String addRoutingUserUrl) {
//		ConfigurationProperties.addRoutingUserUrl = addRoutingUserUrl;
//	}
//	
//	@Value("${getExePathDB}")
//	public void setGetExePathDB(String getExePathDB) {
//		ConfigurationProperties.getExePathDB = getExePathDB;
//	}

}
