/*package com.ingenio.admission.config;

import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

   @Bean
   @Override
   public UserDetailsService userDetailsService () {
   UserDetails user = User.builder().username("user").password(passwordEncoder().encode("secret")).
   roles ("USER").build();
   UserDetails userAdmin = User.builder().username("admin").password(passwordEncoder().encode("secret")).
   roles ("ADMIN").build();
       return new InMemoryUserDetailsManager(user, userAdmin);
   }

   @Bean
   public PasswordEncoder passwordEncoder() {
       return new  BCryptPasswordEncoder();
   }
}
*/