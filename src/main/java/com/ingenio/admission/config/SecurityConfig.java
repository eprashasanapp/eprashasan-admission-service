package com.ingenio.admission.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.techsync.security.jwt.BaseJwtFilter;
import com.techsync.security.jwt.BaseSecurityConfig;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.techsync.security.jwt")
public class SecurityConfig extends BaseSecurityConfig{

	public SecurityConfig(BaseJwtFilter jwtFilter) {
		super(jwtFilter);
	}

	@Override
    protected List<String> getAdditionalExcludedUrls() {
		
		List<String> urls = new ArrayList<>(super.getAdditionalExcludedUrls());
//        return Arrays.asList("/validateOtp", "/checkExistingUser");
		urls.add("/generateOtp");
		urls.add("/generateOtpForLogIn");
		urls.add("/generateOtpForForgotPassword");
		urls.add("/validateOtp");
		urls.add("/loginLink");
		urls.add("/logout");
		urls.add("/saveAttendanceRFIData");
		return urls;
    }
	
//	@Override
//    protected List<String> getRemovedExcludedUrls() {
//        return Arrays.asList("");
//    }
}
