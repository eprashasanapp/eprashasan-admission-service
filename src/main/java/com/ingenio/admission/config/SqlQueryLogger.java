package com.ingenio.admission.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlQueryLogger {
    private static final Logger logger = LoggerFactory.getLogger("SqlQueryLogger");

    public static void logQuery(String query, Object... parameters) {
        logger.info("Executing SQL query: {}", query);
        for (Object parameter : parameters) {
            logger.info("Parameter value: {}", parameter);
        }
    }
}
