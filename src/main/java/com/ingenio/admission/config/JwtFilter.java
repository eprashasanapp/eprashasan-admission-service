package com.ingenio.admission.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.techsync.security.jwt.BaseJwtFilter;
import com.techsync.security.jwt.JwtUtil;

@Component
public class JwtFilter extends BaseJwtFilter{

	public JwtFilter(JwtUtil jwtUtil) {
		super(jwtUtil);
	}

	@Override
	protected List<String> getAdditionalExcludedUrls() {

		List<String> urls = new ArrayList<>(super.getAdditionalExcludedUrls());
		urls.add("/generateOtp");
		urls.add("/generateOtpForLogIn");
		urls.add("/generateOtpForForgotPassword");
		urls.add("/validateOtp");
		urls.add("/loginLink");
		urls.add("/logout");
		urls.add("/saveAttendanceRFIData");
		return urls;
	}
	
}
