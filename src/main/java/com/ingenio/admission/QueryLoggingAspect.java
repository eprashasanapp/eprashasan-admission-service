package com.ingenio.admission;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class QueryLoggingAspect {

////	@Pointcut("execution(* com.ingenio.admission.repository.*.*(..))")
//    @Pointcut("execution(* com.ingenio.admission.repository.*.*(..)) || execution(* com.ingenio.admission.repository.impl.*.*(..))")
//    public void repositoryMethods() {}
//
//	@AfterReturning(pointcut = "repositoryMethods()", returning = "result")
//    public void logQueryExecution(JoinPoint joinPoint, Object result) {
//        // Get the current HTTP request (if available)
//        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (attributes != null) {
//            HttpServletRequest request = attributes.getRequest();
//            // Log the HTTP method and the repository method name
//            String repositoryMethodName = joinPoint.getSignature().getName();
//            System.out.println("Query executed: HTTP " + request.getMethod() + ", Repository method: " + repositoryMethodName);
//            System.out.println("\n ");
//        }
//    }
	
	private Map<String, Integer> queryCountMap = new HashMap<>();

    @Pointcut("execution(* com.ingenio.admission.repository.*.*(..)) || execution(* com.ingenio.admission.repository.impl.*.*(..))")
    public void repositoryMethods() {}

    @AfterReturning(pointcut = "repositoryMethods()", returning = "result")
    public void logQueryExecution(JoinPoint joinPoint, Object result) {
        // Get the current HTTP request (if available)
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes != null) {
            HttpServletRequest request = attributes.getRequest();
            // Log the HTTP method and the repository method name
            String repositoryMethodName = joinPoint.getSignature().getName();
            int count = queryCountMap.getOrDefault(repositoryMethodName, 0);
            queryCountMap.put(repositoryMethodName, count + 1);
            System.out.println("\n ");
            System.out.println("Query executed: HTTP " + request.getMethod() + ", Repository method: " + repositoryMethodName + ", Count: " + queryCountMap.get(repositoryMethodName));
//            System.out.println("\n ");
        }
    }
}
