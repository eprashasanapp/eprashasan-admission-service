package com.ingenio.admission.service;

import java.util.List;

import com.ingenio.admission.bean.FCMUpdateBean;
import com.ingenio.admission.bean.NotificationBean;

public interface FCMPushNotificationService {

	String pushNotification(List<String> deviceTokenList, String message, String message2);

	Integer updateFCMToken(String token, String mobileNo,String deviceType,Integer schoolId);

	List<NotificationBean> getAllNotifications(Integer renewId, String role, Integer offset);

	Integer updateFCMToken1(FCMUpdateBean fcmUpdateBean);

}
