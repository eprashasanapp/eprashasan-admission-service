package com.ingenio.admission.service;

import java.util.List;

import com.ingenio.admission.bean.AppRoleBean;
import com.ingenio.admission.bean.StudentAdmissionBean;

public interface UserAuthenticationService {

	public int generateOTP(String key);
	
	public int getOtp(String key);
	
	public void clearOTP(String key);
	
	public void sendOtpMessage(String toMobile, String message,Integer schoolId, Integer languageId);

	public List<StudentAdmissionBean> getStudentDetails(String userName, String password, Integer schoolId);

	public AppRoleBean getAdminDetails(String userName, String password, int parseInt);

	public Integer forgotPassword(String userName, String password, Integer schoolId);

	public Integer changePassword(String oldpassword, String newPassword, Integer studentId, Integer schoolId, String userName);

	public List<StudentAdmissionBean> findByUser(String userName, int parseInt);

	public void sendOtpMessageForRFID(String mobileNo, String message, Integer valueOf, Integer languageId, String templateId);

	public void sendWhatsAppSMS(String username, String message, Integer schoolId, int messageType);

}
