package com.ingenio.admission.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.SearchTextBean;
import com.ingenio.admission.bean.StudentAdmissionBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.StudentDynamicDetailsBean;
import com.ingenio.admission.bean.SubjectSelectionBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.StudentMasterModel;

public interface StudentAdmissionService {

	StudentMasterModel saveStudentDetails(StudentAdmissionBean studentAdmissionBean, List<MultipartFile> file, List<String> filename, HttpSession session, Integer flag);

	List<AdmissionSettingBean> getTabsAndLegendsSetting(Integer schoolId);

	List<AdmissionSettingBean> getValueToPopulateForDynamicField(Integer schoolId, Integer fieldId);

	StudentAdmissionBean getStudentDetails(String username);

	String getSchoolName(Integer schoolId);

	List<AdmissionSettingBean> getValueToPopulateForDynamic2Field(Integer schoolId, String fieldName);

	List<StudentDynamicDetailsBean> getStudentAssignedSubjectsForEdit(String studentId);

	List<StudentDynamicDetailsBean> getAttachmentsForEdit(Integer studentId);

	List<StudentDynamicDetailsBean> getDynamicDetails2ForEdit(Integer studentId,Integer schoolId);

	List<StudentDynamicDetailsBean> getDynamicDetails1ForEdit(Integer studentId);	

	List<StudentAdmissionBean> getStudentDetailsForEdit(Integer studentId);

	String getFormNumber(Integer schoolId);

	List<StudentDynamicDetailsBean> getAttachmentsForEditWithImages(Integer studentId);

	String getAdmissionPrint(String studRegNo, Integer schoolId, String yearComboVal, String std, String admissionDateSpan, String schoolName, String standardText, HttpServletResponse response);

	StudentMasterModel saveNewRegistration(String newRegName, String yearId, String standardId, String passwordNewReg,String newRegUserName, String schoolId, String formNo, String missMr, String secondName, String lastName, HttpSession session);

	ByteArrayInputStream getreport(Integer schoolId);

	List<UserDetailsBean> getStudentInformation(String studRegNo, Integer schoolId, String yearComboVal);

	List<StudentAdmissionBean> getStudentAttachmentsforPrint(String studentId);

	SubjectSelectionBean getSubjectList(Integer schoolId, Integer yearId, Integer standardId, Integer studentId);

	boolean saveSubject(Integer schoolId, Integer yearId, Integer standardId, Integer studentId, String[] subjectArr);

	StudentMasterModel saveNewRegistrationForCalendar(String firstName, String yearId, String standardId,
			String passwordNewReg, String mobileNo, String schoolId, String formNo, String missMr, String secondName,
			HttpSession session, Integer divisionId, String fcm_id);
	
	Integer getrenewId(Integer studentId, Integer valueOf);


	String getCertificatePrint(String studRegNo, Integer schoolId, String yearComboVal, String std,
			String standardText, String schoolName, String globalCertificateId, HttpServletResponse response, HttpSession session);


	List<StudentDetailsBean> getStudentNameAutoComplete(Integer schoolId, String searchText);

	boolean savemonthlyCatalog(StudentDetailsBean studentDetailsBean);

//	boolean savemonthlyCatalog(List<StudentDetailsBean> studentDetailsBean);
	
	List<StudentDetailsBean> getStudentInformation(Integer schoolId, String searchFlag, String searchText);

	Integer updateStudentRegNumber(String studentRegNumber, Integer schoolId, Integer studentId);

	List<StudentDetailsBean> getStudentInformation(List<SearchTextBean> searchTextBean);


	/*String getDownloadPrint(String studRegNo, Integer schoolId, String yearComboVal, String std,
			String admissionDateSpan, String schoolName, HttpServletResponse response);*/

}
