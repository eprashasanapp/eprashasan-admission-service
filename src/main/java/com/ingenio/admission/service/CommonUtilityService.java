package com.ingenio.admission.service;

import java.util.List;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.CheckBalanceBean;
import com.ingenio.admission.bean.CommonUtilityBean;
import com.ingenio.admission.bean.FrontPageImagesBean;
import com.ingenio.admission.bean.SchoolMappingBean;
import com.ingenio.admission.bean.StandardYearBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.AttachmentMasterModel;
import com.ingenio.admission.model.BonafideTCImageModel;
import com.ingenio.admission.model.CasteMasterModel;
import com.ingenio.admission.model.CategoryMasterModel;
import com.ingenio.admission.model.DesignAdmiFormTabsModel;
import com.ingenio.admission.model.DivisionMasterModel;
import com.ingenio.admission.model.GRBookNameModel;
import com.ingenio.admission.model.MinorityMasterModel;
import com.ingenio.admission.model.ReligionMasterModel;
import com.ingenio.admission.model.StandardMasterModel;
import com.ingenio.admission.model.YearMasterModel;

public interface CommonUtilityService {

	List<YearMasterModel> getAcademicYearCombo(Integer schoolId);

	List<StandardMasterModel> getStandardCombo(Integer schoolId);
	
	List<CategoryMasterModel> getCategoryCombo(Integer schoolId);
	
	List<CasteMasterModel> getCasteCombo(Integer schoolId);
	
	List<MinorityMasterModel> getMinorityCombo(Integer schoolId);
	
	List<ReligionMasterModel> getReligionCombo(Integer schoolId);
	
	List<DivisionMasterModel> getDivisionCombo(Integer schoolId);

	List<GRBookNameModel> getGRCombo(Integer schoolId);

	List<AttachmentMasterModel> getStudentAttachments(Integer schoolId);

	List<DesignAdmiFormTabsModel> getTabsCombo(Integer schoolId);

	List<BonafideTCImageModel> getLetterHead(Integer schoolId);

	List<AdmissionSettingBean> getAssignedSubjects(Integer schoolId, Integer yearId, Integer standardId);

	 List<UserDetailsBean> getUserRoleAndSchoolDetails(String username, String language);

	List<CommonUtilityBean> getLetterHeadImg(Integer schoolId);

	List<UserDetailsBean> checkExistingUser(String username, Integer schoolId);

	String getAcademicYear(String schoolId);

	List<CommonUtilityBean> getStudentImageSignature(Integer schoolId, String regNo, Integer type);

	StandardYearBean getStandardYear(String studentId);

	List<CheckBalanceBean> getSmsBalance(Integer schoolId);

	FrontPageImagesBean getAllBannerList(Integer schoolId);

//	List<SchoolMappingBean> getAllSchoolMappingList(Integer schoolId);

	List<SchoolMappingBean> getAllSchoolMappingList(Integer schoolId, Integer onlineAdmissionFlag);
}
