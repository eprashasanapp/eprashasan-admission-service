package com.ingenio.admission.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingenio.admission.bean.DiscountBean;
import com.ingenio.admission.bean.EmailRequestBean;
import com.ingenio.admission.bean.FeeBean;
import com.ingenio.admission.bean.FeeIdBean;
import com.ingenio.admission.bean.FeePayTypeBean;
import com.ingenio.admission.bean.FeePayementGatewayBean;
import com.ingenio.admission.bean.FeeProspectBean;
import com.ingenio.admission.bean.HeadFeeBean;
import com.ingenio.admission.bean.OrderBean;
import com.ingenio.admission.bean.PaymentRequestBean;
import com.ingenio.admission.bean.PaymentResponseBean;
import com.ingenio.admission.bean.PaymentResponseList;
import com.ingenio.admission.bean.PaymentSettingBean;
import com.ingenio.admission.bean.SavePaidBean;
import com.ingenio.admission.bean.SchoolDetailBean;
import com.ingenio.admission.bean.SessionResponseBean;
//192.168.0.10/microservices/eprashasanadmission.git
import com.ingenio.admission.bean.SubHeadBean;
import com.ingenio.admission.bean.TransferBean;
import com.ingenio.admission.bean.TransferRequestBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.config.ConfigurationProperties;
import com.ingenio.admission.enums.CommissionTypes;
import com.ingenio.admission.enums.DateFormat;
import com.ingenio.admission.enums.DiscountLabels;
import com.ingenio.admission.enums.Numbers;
import com.ingenio.admission.enums.PayTypes;
import com.ingenio.admission.enums.RestUrlConfigs;
import com.ingenio.admission.model.AppUserRoleModel;
import com.ingenio.admission.model.DummyRegistrationModel;
import com.ingenio.admission.model.FeeDiscountLabelModel;
import com.ingenio.admission.model.FeeHeadMasterModel;
import com.ingenio.admission.model.FeePaidDueModel;
import com.ingenio.admission.model.FeePaidFeeModel;
import com.ingenio.admission.model.FeePayTypeModel;
import com.ingenio.admission.model.FeePaymentGatewayModel;
import com.ingenio.admission.model.FeeReceiptModel;
import com.ingenio.admission.model.FeeSettingModel;
import com.ingenio.admission.model.FeeSettingMultiModel;
import com.ingenio.admission.model.FeeStudentFeeModel;
import com.ingenio.admission.model.FeeSubheadMasterModel;
import com.ingenio.admission.model.FeeTypeModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StandardMasterModel;
import com.ingenio.admission.model.StudentMasterModel;
import com.ingenio.admission.model.StudentStandardRenewModel;
import com.ingenio.admission.model.YearMasterModel;
import com.ingenio.admission.repository.DiscountLabelRepository;
import com.ingenio.admission.repository.DummyRegistrationRepository;
import com.ingenio.admission.repository.FeePayTypeRepository;
import com.ingenio.admission.repository.FeePaymentGatewayRepository;
import com.ingenio.admission.repository.FeeRepository;
import com.ingenio.admission.repository.FeeSettingMultiRepository;
import com.ingenio.admission.repository.FeeSettingRepository;
import com.ingenio.admission.repository.FeeStudentFeeRepository;
import com.ingenio.admission.repository.PaidFeeRepository;
import com.ingenio.admission.repository.PaidFineRepository;
import com.ingenio.admission.repository.ReceiptRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.repository.StudentMasterRepository;
import com.ingenio.admission.repository.UserRepository;
import com.ingenio.admission.repository.impl.FeeReportsRepositoryImpl;
import com.ingenio.admission.service.PaymentGatewayService;
import com.ingenio.admission.util.CryptographyUtil;
import com.ingenio.admission.util.RestUtil;
import com.ingenio.admission.util.UTFConversionUtil;
import com.ingenio.admission.util.Utillity;

@Component

//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")

//  @PropertySource("classpath:messages_en.properties")
//  
//  @PropertySource("classpath:messages_mr.properties")
//  
//  @PropertySource("classpath:messages_hn.properties")
//  

public class PaymentGatewayServiceImpl implements PaymentGatewayService {

	@Autowired
	FeeRepository feeRepository;

	@Autowired
	FeeReportsRepositoryImpl feeReportsRepositoryImpl;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	StudentMasterRepository studentMasterRepository;

	@Autowired
	FeePaymentGatewayRepository feePaymentGatewayRepository;

	@Autowired
	FeePayTypeRepository feePayTypeRepository;

	@Autowired
	DiscountLabelRepository discountLabelRepository;

	@Autowired
	PaidFeeRepository paidFeeRepository;

	@Autowired
	PaidFineRepository paidFineRepository;

	@Autowired
	ReceiptRepository receiptRepository;

	@Autowired
	FeeSettingRepository feeSettingRepository;

	@Autowired
	FeeSettingMultiRepository feeSettingMultiRepository;

	@Autowired
	FeeStudentFeeRepository feeStudentFeeRepository;

	@Autowired
	UserAuthenticationServiceImpl userAuthenticationServiceImpl;

	CryptographyUtil cry = new CryptographyUtil();

	@Autowired
	private Environment env;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private DummyRegistrationRepository dummyRegistrationRepository;

	@Override
	public List<FeePayementGatewayBean> getPaymentGatewayReport(Integer renewStudentId, Integer schoolId,
			Integer yearId, String date) {

		List<FeePayementGatewayBean> overViewBeanList = new ArrayList<>();
		List<FeeBean> feeBeanList = feeReportsRepositoryImpl.getTotalFee(renewStudentId);
		HeadFeeBean headFeeBean = new HeadFeeBean();
		List<SubHeadBean> subheadList = new ArrayList<>();
		List<HeadFeeBean> headFeeBeanList = new ArrayList<>();
		FeePayementGatewayBean overViewBean = new FeePayementGatewayBean();
		Double headWiseFeeTotal = 0.0;
		Double headWisePaidFeeTotal = 0.0;
		Double headWiseRemainingFeeTotal = 0.0;
		Integer headId = 0;
		Double feeTotal = 0.0;
		Double paidFeeTotal = 0.0;
		Double remainingFeeTotal = 0.0;
		Double grandTotal = 0.0;
		Double paidGrandTotal = 0.0;
		Double remainingGrandTotal = 0.0;
		Double totalLateFeeCharges = 0.0;
		Double totalPaidLateFeeCharges = 0.0;
		Double totalRemainingLateFeeCharges = 0.0;
		Double chequeReturnCharges = 0.0;
		Double chequePaidCharges = 0.0;
		try {
			if (CollectionUtils.isNotEmpty(feeBeanList)) {
				List<Double> bouncechargesList = feeRepository.getBouncedAssignedCharges(renewStudentId);
				List<Double> chequeReturnChargesList = feeRepository.getBouncedPaidCharges(renewStudentId);
				if (CollectionUtils.isNotEmpty(bouncechargesList)) {
					for (int i = 0; i < bouncechargesList.size(); i++) {
						chequeReturnCharges += bouncechargesList.get(i);
					}
				}

				if (CollectionUtils.isNotEmpty(chequeReturnChargesList)) {
					for (int i = 0; i < chequeReturnChargesList.size(); i++) {
						chequePaidCharges += chequeReturnChargesList.get(i);
					}
				}
				List<DiscountBean> discountBeanList = calculateDiscount(schoolId, yearId, date, feeBeanList);
				for (FeeBean feeBean : feeBeanList) {
					if (!feeBean.getHeadId().equals(headId)) {
						if (headId != 0) {
							headFeeBean.setHeadWiseFeeTotal(headWiseFeeTotal);
							headFeeBean.setHeadWisePaidFeeTotal(headWisePaidFeeTotal);
							headFeeBean.setHeadWiseRemainingFeeTotal(headWiseRemainingFeeTotal);
							headFeeBean.setSubHeadList(subheadList);
							headFeeBeanList.add(headFeeBean);
							subheadList = new ArrayList<>();
							feeTotal += headWiseFeeTotal;
							paidFeeTotal += headWisePaidFeeTotal;
							remainingFeeTotal += headWiseRemainingFeeTotal;
							headWiseFeeTotal = 0.0;
							paidFeeTotal = 0.0;
							remainingFeeTotal = 0.0;

						}
						headFeeBean = new HeadFeeBean();
						headFeeBean.setHeadId(feeBean.getHeadId());
						headFeeBean.setHeadName(feeBean.getHeadName());
						subheadList = new ArrayList<>();
					}
					SubHeadBean subHeadBean = new SubHeadBean();
					subHeadBean.setSubheadId(feeBean.getSubheadId());
					subHeadBean.setSubHeadName(feeBean.getSubheadName());
					subHeadBean.setFeeSetMultiID(feeBean.getFeeSetMultiID());
					subHeadBean.setFee(feeBean.getAssignFee());
					subHeadBean.setPaidFee(feeBean.getPaidFee() - Double.parseDouble(feeBean.getDiscount()));
					subHeadBean.setRemainingFee(Double.parseDouble(feeBean.getRemainingFee()));
					subHeadBean.setRemainingFine(Double.parseDouble(feeBean.getRemainingFine()));
					subHeadBean.setPaidFine(Double.parseDouble(feeBean.getPaidFine()));
					subHeadBean.setStudFeeID(feeBean.getStudFeeID());
					subheadList.add(subHeadBean);
					headWiseFeeTotal += feeBean.getAssignFee();
					headWisePaidFeeTotal += subHeadBean.getPaidFee();
					headWiseRemainingFeeTotal += subHeadBean.getRemainingFee();
					headId = feeBean.getHeadId();
					totalLateFeeCharges += feeBean.getAssignedFine();
					totalPaidLateFeeCharges += Double.parseDouble(feeBean.getPaidFine());
					totalRemainingLateFeeCharges += Double.parseDouble(feeBean.getRemainingFine());

				}
				headFeeBean.setHeadWiseFeeTotal(headWiseFeeTotal);
				headFeeBean.setHeadWiseRemainingFeeTotal(headWiseRemainingFeeTotal);
				headFeeBean.setHeadWisePaidFeeTotal(headWisePaidFeeTotal);
				headFeeBean.setSubHeadList(subheadList);
				headFeeBeanList.add(headFeeBean);

				feeTotal += headWiseFeeTotal;
				paidFeeTotal += headWisePaidFeeTotal;
				remainingFeeTotal += headWiseRemainingFeeTotal;
				grandTotal = 0.0;
				paidGrandTotal = 0.0;
				remainingGrandTotal = 0.0;

				overViewBean.setAssignedLateFee(totalLateFeeCharges);
				overViewBean.setPaidLateFee(totalPaidLateFeeCharges);
				overViewBean.setRemainingLateFee(totalRemainingLateFeeCharges);
				overViewBean.setAssignedBounceCharges(chequeReturnCharges != null ? chequeReturnCharges : 0.0);
				overViewBean.setPaidBounceCharges(chequePaidCharges != null ? chequePaidCharges : 0.0);
				overViewBean.setRemainingBounceCharges(
						overViewBean.getAssignedBounceCharges() - overViewBean.getPaidBounceCharges());
				overViewBean.setDiscountBean(discountBeanList);

				grandTotal = feeTotal + chequeReturnCharges + totalLateFeeCharges;
				paidGrandTotal = paidFeeTotal + chequePaidCharges + totalPaidLateFeeCharges;
				remainingGrandTotal = remainingFeeTotal + (chequeReturnCharges - chequePaidCharges)
						+ totalRemainingLateFeeCharges;

				overViewBean.setTotalAssignedFee(feeTotal);
				overViewBean.setTotalPaidFee(paidFeeTotal);
				overViewBean.setTotalRemainingFee(remainingFeeTotal);
				overViewBean.setGrandAssignedTotal(grandTotal);
				overViewBean.setGrandPaidTotal(paidGrandTotal);
				overViewBean.setGrandRemainingTotal(remainingGrandTotal);
				overViewBean.setHeadFeeBeanList(headFeeBeanList);
				overViewBeanList.add(overViewBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return overViewBeanList;
	}

	@Override
	public List<FeeBean> saveTemporaryFees(PaymentRequestBean paymentRequestBean) {
		if (StringUtils.isEmpty("" + paymentRequestBean.getConvenienceFee())) {
			return new ArrayList<FeeBean>();
		}
		String processId = StringUtils.EMPTY;
		String orderId = StringUtils.EMPTY;
		try {
			Double totalAmount = paymentRequestBean.getTotalPaidAmount();
			Integer subheadSize = 0;
			Integer remainingDiscount = 0;
			Integer remainingConvinienceFee = 0;
			for (int i = 0; i < paymentRequestBean.getHeadFeeBeanList().size(); i++) {
				List<SubHeadBean> subHeadList = paymentRequestBean.getHeadFeeBeanList().get(i).getSubHeadList();
				for (int j = 0; j < subHeadList.size(); j++) {
					if (subHeadList.get(j).getRemainingFee() > 0 || subHeadList.get(j).getRemainingFine() > 0) {
						Double subheadAmount = 0.0;
						Double subheadFine = 0.0;
						if (subHeadList.get(j).getRemainingFee() > 0
								&& totalAmount >= subHeadList.get(j).getRemainingFee()) {
							subheadAmount = Double.parseDouble("" + subHeadList.get(j).getRemainingFee().intValue());
						} else if (subHeadList.get(j).getRemainingFee() > 0) {
							subheadAmount = Double.parseDouble("" + totalAmount.intValue());
						}

						totalAmount = totalAmount - subheadAmount;
						if (subHeadList.get(j).getRemainingFine() > 0
								&& totalAmount >= subHeadList.get(j).getRemainingFine()) {
							subheadFine = Double.parseDouble("" + subHeadList.get(j).getRemainingFine().intValue());
						} else if (subHeadList.get(j).getRemainingFine() > 0) {
							subheadFine = Double.parseDouble("" + totalAmount.intValue());
						}
						totalAmount = totalAmount - subheadFine;
						subHeadList.get(j).setPaidFee(subheadAmount);
						subHeadList.get(j).setPaidFine(subheadFine);
						if (subheadAmount > 0) {
							subheadSize++;
						}
					} else {
						subHeadList.get(j).setPaidFee(0.0);
						subHeadList.get(j).setPaidFine(0.0);
					}
				}

			}
			Integer discountAmount = 0;
			Integer convinienceFee = 0;
			if (subheadSize > 0) {
				discountAmount = (int) (paymentRequestBean.getTotalDiscount() / subheadSize);
				convinienceFee = (int) (paymentRequestBean.getConvenienceFee() / subheadSize);
			}
			if (discountAmount < paymentRequestBean.getTotalDiscount().intValue()) {
				remainingDiscount = paymentRequestBean.getTotalDiscount().intValue() - (discountAmount * subheadSize);
				remainingConvinienceFee = paymentRequestBean.getConvenienceFee().intValue()
						- (convinienceFee * subheadSize);
			}
			Integer firstDiscount = discountAmount + remainingDiscount;
			Integer firstConvinience = convinienceFee + remainingConvinienceFee;
			Double payGateHeadTotal = 0.0;
			List<FeeBean> feeBeanList = new ArrayList<>();
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(paymentRequestBean.getSchoolId());
			String schoolKey = schoolMasterRepository.findBySchoolid(paymentRequestBean.getSchoolId()).getSchoolKey();
			String globalDbSchoolKey = "1".concat("" + schoolKey);
			Integer principaluserId = userRepository.getprincipalUserId(paymentRequestBean.getSchoolId());
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(principaluserId);
			String processIncId = feePaymentGatewayRepository.getMaxProcessId(paymentRequestBean.getSchoolId());
			processId = Utillity.generatePrimaryKey(globalDbSchoolKey, processIncId);
			if (CollectionUtils.isNotEmpty(paymentRequestBean.getHeadFeeBeanList())) {
				for (int i = 0; i < paymentRequestBean.getHeadFeeBeanList().size(); i++) {
					payGateHeadTotal = 0.0;
					FeeHeadMasterModel feeHeadMaster = new FeeHeadMasterModel();
					feeHeadMaster.setHeadId(paymentRequestBean.getHeadFeeBeanList().get(i).getHeadId());
					String accountId = feePaymentGatewayRepository
							.findByFeeHeadMaster(paymentRequestBean.getHeadFeeBeanList().get(i).getHeadId());
					if (StringUtils.isEmpty(accountId) || ("0").equals(accountId)) {
						return new ArrayList<>();
					}
					List<SubHeadBean> subHeadList = paymentRequestBean.getHeadFeeBeanList().get(i).getSubHeadList();
					if (CollectionUtils.isNotEmpty(subHeadList)) {
						for (int j = 0; j < subHeadList.size(); j++) {
							if (subHeadList.get(j).getPaidFee() > 0 || subHeadList.get(j).getPaidFine() > 0) {
								String incrementedId = ""
										+ feeRepository.getMaxAssignToId(paymentRequestBean.getSchoolId());
								String paymentGatewayId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedId);
								FeePaymentGatewayModel feePaymentGatewayModel = new FeePaymentGatewayModel();
								FeeSettingMultiModel feeSettingMulti = new FeeSettingMultiModel();
								feeSettingMulti.setFeeSetMultiId(subHeadList.get(j).getFeeSetMultiID());
								feePaymentGatewayModel.setFeeSettingMultiModel(feeSettingMulti);
								FeeStudentFeeModel feeStudentFeeModel = new FeeStudentFeeModel();
								feeStudentFeeModel.setStudFeeId(subHeadList.get(j).getStudFeeID());
								feePaymentGatewayModel.setFeeStudentFeeModel(feeStudentFeeModel);
								feePaymentGatewayModel.setConvenienceFee(Double.parseDouble("" + convinienceFee));
								feePaymentGatewayModel
										.setPaymentGatewayRateId(paymentRequestBean.getPaymentGatewayRateId());
								feePaymentGatewayModel.setSchoolMasterModel(schoolMasterModel);
								feePaymentGatewayModel.setProcessId(Integer.parseInt(processId));
								feePaymentGatewayModel.setPayType(paymentRequestBean.getPayType());
								feePaymentGatewayModel.setPaidfeeDate(paymentRequestBean.getPaidDate());
								feePaymentGatewayModel.setPaymentStatusCode("0");
								feePaymentGatewayModel.setDeviceType(paymentRequestBean.getDeviceType());
								feePaymentGatewayModel.setDiscount(Double.parseDouble("" + discountAmount));
								if (i == 0 && j == 0) {
									feePaymentGatewayModel.setDiscount(Double.parseDouble("" + firstDiscount));
								}
								if (i == 0 && j == 0) {
									feePaymentGatewayModel.setConvenienceFee(Double.parseDouble("" + firstConvinience));
								}
								feePaymentGatewayModel.setPayFee(subHeadList.get(j).getPaidFee());
								feePaymentGatewayModel.setPaidFine(subHeadList.get(j).getPaidFine());
								feePaymentGatewayModel.setDeviceType(paymentRequestBean.getDeviceType());
								feePaymentGatewayModel.setPaymentStatus(paymentRequestBean.getPaymentStatus());
								feePaymentGatewayModel.setPaymentStatusMessage(paymentRequestBean.getPaymentMessage());
								feePaymentGatewayModel.setFeeHeadMasterModel(feeHeadMaster);
								feePaymentGatewayModel.setAppUserRoleModel(appUserRoleModel);
								feePaymentGatewayModel.setOrderId(orderId);
								FeeSubheadMasterModel feeSubheadMasterModel = new FeeSubheadMasterModel();
								feeSubheadMasterModel.setSubheadId(subHeadList.get(j).getSubheadId());
								feePaymentGatewayModel.setFeeSubheadMasterModel(feeSubheadMasterModel);
								feePaymentGatewayModel.setAppUserRoleModel(appUserRoleModel);
								feePaymentGatewayModel.setPaymentGatewayId(Integer.parseInt(paymentGatewayId));
								feePaymentGatewayRepository.saveAndFlush(feePaymentGatewayModel);
								payGateHeadTotal += ((subHeadList.get(j).getPaidFee() - discountAmount)
										+ subHeadList.get(j).getPaidFine());
							}

						}

					}

					if (payGateHeadTotal > 0) {
						FeeBean feeBean = new FeeBean();
						feeBean.setHeadId(paymentRequestBean.getHeadFeeBeanList().get(i).getHeadId());
						feeBean.setPaymentGatewayAmount(payGateHeadTotal);
						feeBean.setPaymentGatewayAccountId(accountId);
						feeBean.setOrderId(orderId);
						feeBeanList.add(feeBean);
					}

				}
			}

			String schoolNameSecretKey = ConfigurationProperties.schoolNameSecretKey;
			SchoolMasterModel scMasterModel = schoolMasterRepository.findBySchoolid(paymentRequestBean.getSchoolId());
			String hostingIP = ConfigurationProperties.hostingIP;
			String restUrl = ConfigurationProperties.commissionDetailsUrl;
			String payTypeUrl = restUrl + "schoolId=" + paymentRequestBean.getSchoolId() + "&hostingIp=" + hostingIP;
			PaymentResponseList paymentRatesList = (PaymentResponseList) RestUtil.getCommissionList(payTypeUrl);

			Double convinienceAmount = paymentRequestBean.getConvenienceFee() - paymentRequestBean.getEpCommission()
					- paymentRequestBean.getSchoolCommission() - paymentRequestBean.getTpCommission()
					- paymentRequestBean.getPayTypeRouteCharges();

			if (paymentRatesList != null && CollectionUtils.isNotEmpty(paymentRatesList.getPaymentList())) {
				paymentRatesList.getPaymentList().stream().forEach(paymentRatesBean -> {

					if (CommissionTypes.EPCOMMISSION.getCommissionTypes()
							.equals(paymentRatesBean.getCommissionType())) {
						FeeBean feeBean = new FeeBean();
						feeBean.setHeadId(0);
						feeBean.setPaymentGatewayAmount(paymentRequestBean.getEpCommission());
						feeBean.setPaymentGatewayAccountId(paymentRatesBean.getAccountId());
						feeBeanList.add(feeBean);
					} else if (CommissionTypes.SCHOOLCOMMISSION.getCommissionTypes()
							.equals(paymentRatesBean.getCommissionType())) {
						FeeBean feeBean = new FeeBean();
						feeBean.setHeadId(0);
						feeBean.setPaymentGatewayAmount(paymentRequestBean.getSchoolCommission());
						feeBean.setPaymentGatewayAccountId(paymentRatesBean.getAccountId());
						feeBeanList.add(feeBean);
					} else if (CommissionTypes.TPCOMMISSION.getCommissionTypes()
							.equals(paymentRatesBean.getCommissionType())) {
						FeeBean feeBean = new FeeBean();
						feeBean.setHeadId(0);
						feeBean.setPaymentGatewayAmount(paymentRequestBean.getTpCommission());
						feeBean.setPaymentGatewayAccountId(paymentRatesBean.getAccountId());
						feeBeanList.add(feeBean);
					}
				});
			}

			FeeBean feeBean = new FeeBean();
			feeBean.setHeadId(0);
			feeBean.setPaymentGatewayAmount(convinienceAmount);
			feeBean.setPaymentGatewayAccountId(Numbers.Zero.getNumber());
			feeBean.setOrderId(orderId);
			feeBeanList.add(feeBean);

			double paymentGatewayAmount = (paymentRequestBean.getTotalPaidAmount()
					- paymentRequestBean.getTotalDiscount()) + paymentRequestBean.getConvenienceFee();
			for (int i = 0; i < feeBeanList.size(); i++) {
				if (i == 0) {
					feeBeanList.get(i).setPaymentGatewayAmount(
							feeBeanList.get(i).getPaymentGatewayAmount() + paymentRequestBean.getPayTypeRouteCharges());
				}
				feeBeanList.get(i).setMerchantName(cry.decrypt(schoolNameSecretKey, scMasterModel.getSchoolName()));
				feeBeanList.get(i).setMerchantDescription("");
				feeBeanList.get(i).setProcessId(processId);
			}
			orderId = createOrderId(paymentGatewayAmount, feeBeanList);
			feePaymentGatewayRepository.updateOrderId(orderId, Integer.parseInt(processId));
			for (int i = 0; i < feeBeanList.size(); i++) {
				feeBeanList.get(i).setOrderId(orderId);
			}
			return feeBeanList;
		} catch (Exception e) {
			updatePaymentStatus(paymentRequestBean.getSchoolId(), processId, "500", "Failed",
					"Failed to generate order");
			e.printStackTrace();
		}

		return new ArrayList<>();
	}

	@Override
	public List<FeePayTypeBean> getPayTypeList(Integer schoolId, Double amount) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		List<FeePayTypeModel> payTypeList = feePayTypeRepository.findBySchoolMasterModel(schoolMasterModel);
		List<FeePayTypeBean> mobileList = new ArrayList<>();
		String hostingIP = ConfigurationProperties.hostingIP;
		String restUrl = ConfigurationProperties.paymentRatesCommonUrl;

		Double epRoutingCharge = 0.0;
		Double epCommission = 0.0;
		Double epGstCharge = 0.0;
		Double epCommRouteGst = 0.0;

		Double schCommission = 0.0;
		Double schGstCharge = 0.0;
		Double schRoutingCharge = 0.0;
		Double schCommRouteGst = 0.0;

		Double tpCommission = 0.0;
		Double tpGstCharge = 0.0;
		Double tpRoutingCharge = 0.0;
		Double tpCommRouteGst = 0.0;
		PaymentResponseBean routingRatesBean = new PaymentResponseBean();

		String payTypeUrl = restUrl + "schoolId=" + schoolId + "&hostingIp=" + hostingIP;

		PaymentResponseList paymentResponseList = (PaymentResponseList) RestUtil.makeCommonRestCall(payTypeUrl);
		if (paymentResponseList != null && CollectionUtils.isNotEmpty(paymentResponseList.getPaymentList())) {
			List<PaymentResponseBean> paymentResponseBeanList = paymentResponseList.getPaymentList();
			for (int i = 0; i < paymentResponseBeanList.size(); i++) {
				if (CommissionTypes.ROUTINGCHARGES.getCommissionTypes()
						.equalsIgnoreCase(paymentResponseBeanList.get(i).getPaymentType())) {
					routingRatesBean = paymentResponseBeanList.get(i);
					break;
				}
			}
			for (int i = 0; i < paymentResponseBeanList.size(); i++) {
				Double commission = 0.0;
				Double gstCharge = 0.0;
				Double routingCharge = 0.0;
				Double routingGST = 0.0;
				PaymentResponseBean paymentResponseBean = paymentResponseBeanList.get(i);
				if (("Less Than").equalsIgnoreCase(paymentResponseBean.getCompareType())
						&& Double.parseDouble(paymentResponseBean.getCharge()) <= amount) {
					continue;
				}
				if (("More Than").equalsIgnoreCase(paymentResponseBean.getCompareType())
						&& Double.parseDouble(paymentResponseBean.getCharge()) > amount) {
					continue;
				}
				if (("0").equals(paymentResponseBean.getAmountPercentFlag())) {
					commission = ((paymentResponseBean.getAmount() * amount) / 100);
				}
				if (("1").equals(paymentResponseBean.getAmountPercentFlag())) {
					commission = (double) paymentResponseBean.getAmount();
				}
				gstCharge = (paymentResponseBean.getGstPercentage() * commission) / 100;
				commission += gstCharge;
				if (routingRatesBean != null && ("0").equals(routingRatesBean.getAmountPercentFlag())) {
					routingCharge = ((routingRatesBean.getAmount() * epCommission) / 100);
				}
				if (routingRatesBean != null && ("1").equals(routingRatesBean.getAmountPercentFlag())) {
					routingCharge = (double) routingRatesBean.getAmount();
				}
				if (routingRatesBean != null && routingRatesBean.getGstPercentage() != null) {
					routingGST = (routingRatesBean.getGstPercentage() * routingCharge) / 100;
				}
				routingCharge += routingGST;
				commission += routingCharge;

				if (CommissionTypes.EPCOMMISSION.getCommissionTypes()
						.equalsIgnoreCase(paymentResponseBean.getPaymentType())) {
					epRoutingCharge = routingCharge;
					epCommission = commission;
					epGstCharge = 0.0;
					epCommRouteGst = 0.0;
				}
				if (CommissionTypes.SCHOOLCOMMISSION.getCommissionTypes()
						.equalsIgnoreCase(paymentResponseBean.getPaymentType())) {
					schRoutingCharge = routingCharge;
					schCommission = commission;
					schGstCharge = 0.0;
					schCommRouteGst = 0.0;
				}
				if (CommissionTypes.SCHOOLCOMMISSION.getCommissionTypes()
						.equalsIgnoreCase(paymentResponseBean.getPaymentType())) {
					tpRoutingCharge = routingCharge;
					tpCommission = commission;
					tpGstCharge = 0.0;
					tpCommRouteGst = 0.0;
				}
			}

			final double commissionTotal = epCommission + schCommission + tpCommission;
			for (int i = 0; i < paymentResponseBeanList.size(); i++) {
				PaymentResponseBean paymentResponseBean = paymentResponseBeanList.get(i);
				if ((((PayTypes.DEBITCARD.getPayTypes()).equalsIgnoreCase(paymentResponseBean.getPaymentType()))
						|| ((PayTypes.CREDITCARD.getPayTypes()).equalsIgnoreCase(paymentResponseBean.getPaymentType()))
						|| ((PayTypes.NETBANKING.getPayTypes()).equalsIgnoreCase(paymentResponseBean.getPaymentType()))
						|| ((PayTypes.UPI.getPayTypes()).equalsIgnoreCase(paymentResponseBean.getPaymentType()))
						|| ((PayTypes.WALLET.getPayTypes()).equalsIgnoreCase(paymentResponseBean.getPaymentType())))) {
					/*
					 * if(("Less Than").equalsIgnoreCase(paymentResponseBean.getCompareType()) &&
					 * Double.parseDouble(paymentResponseBean.getCharge())<=amount) { continue; }
					 * if(("More Than").equalsIgnoreCase(paymentResponseBean.getCompareType()) &&
					 * Double.parseDouble(paymentResponseBean.getCharge())>amount) { continue; }
					 */
					Double payTypeConvinience = 0.0;
					Double routingCharges = 0.0;
					Double payTypeRoutingGst = 0.0;
					FeePayTypeBean bean = new FeePayTypeBean();
					for (int j = 0; j < payTypeList.size(); j++) {
						if (paymentResponseBean.getPaymentType()
								.equalsIgnoreCase(payTypeList.get(j).getPayTypeName())) {
							bean.setPayTypeId(payTypeList.get(j).getPayTypeId());
							bean.setPayTypeName(payTypeList.get(j).getPayTypeName());
							break;
						}
					}

					Double payTypeAmount = commissionTotal + amount;
					Double payTypeGstCharge = 0.0;
					if (("0").equals(paymentResponseBean.getAmountPercentFlag())) {
						payTypeConvinience = ((paymentResponseBean.getAmount() * payTypeAmount) / 100);
					}
					if (("1").equals(paymentResponseBean.getAmountPercentFlag())) {
						payTypeConvinience = (double) paymentResponseBean.getAmount();
					}
					payTypeGstCharge = (paymentResponseBean.getGstPercentage() * payTypeConvinience) / 100;
					payTypeConvinience += payTypeGstCharge;
					bean.setPaymentGatewayRateId(paymentResponseBean.getPaymentGatewayRateId());
					if (routingRatesBean != null) {
						if (routingRatesBean != null && ("0").equals(routingRatesBean.getAmountPercentFlag())) {
							routingCharges = ((routingRatesBean.getAmount() * amount) / 100);
						}
						if (routingRatesBean != null && ("1").equals(routingRatesBean.getAmountPercentFlag())) {
							routingCharges = (double) routingRatesBean.getAmount();
						}
						if (routingRatesBean != null && routingRatesBean.getGstPercentage() != null) {
							payTypeRoutingGst = (routingRatesBean.getGstPercentage() * routingCharges) / 100;
						}

						routingCharges += payTypeRoutingGst;
					}
					Double convinienceFee = payTypeConvinience + commissionTotal + routingCharges;
					bean.setConvinienceFee("" + Math.round(convinienceFee * 100D) / 100D);
					bean.setPayTypeCharges(Math.round(payTypeConvinience * 100D) / 100D);
					bean.setPayTypeGst(Math.round(payTypeGstCharge * 100D) / 100D);
					bean.setPayTypeRouteCharges(Math.round(routingCharges * 100D) / 100D);
					bean.setPayTypeRouteGst(Math.round(payTypeRoutingGst * 100D) / 100D);
					bean.setEpCommission(Math.round(epCommission * 100D) / 100D);
					bean.setSchoolCommission(Math.round(schCommission * 100D) / 100D);
					bean.setTpCommission(Math.round(tpCommission * 100D) / 100D);
					bean.setEpCommRouteCharges(Math.round(epRoutingCharge * 100D) / 100D);
					bean.setTpCommRouteCharges(Math.round(tpRoutingCharge * 100D) / 100D);
					bean.setSchCommRouteCharges(Math.round(schRoutingCharge * 100D) / 100D);
					bean.setEpGst(Math.round(epGstCharge * 100D) / 100D);
					bean.setTpGst(Math.round(tpGstCharge * 100D) / 100D);
					bean.setSchGST(Math.round(schGstCharge * 100D) / 100D);
					bean.setEpCommRouteGst(Math.round(epCommRouteGst * 100D) / 100D);
					bean.setSchCommRouteGst(Math.round(schCommRouteGst * 100D) / 100D);
					bean.setTpCommRouteGst(Math.round(tpCommRouteGst * 100D) / 100D);
					mobileList.add(bean);
				}
			}

		}
		return mobileList;
	}

	@Override
	public List<PaymentRequestBean> getTransactions(Integer studentFeeId, String language) {
		List<PaymentRequestBean> beanList = new ArrayList<>();
		List<FeeBean> transactionList = feePaymentGatewayRepository.getTransactions(studentFeeId);
		PaymentRequestBean payTypeBean = new PaymentRequestBean();
		if (CollectionUtils.isNotEmpty(transactionList)) {
			for (int i = 0; i < transactionList.size(); i++) {
				payTypeBean = new PaymentRequestBean();
				payTypeBean.setTransactionId(transactionList.get(i).getTransactionId1());
				payTypeBean.setOrderId(transactionList.get(i).getOrderId());
				payTypeBean.setPaidDate("" + transactionList.get(i).getPaidFeeDate());
				payTypeBean.setTime(transactionList.get(i).getTime());
				String paymentStatus = env
						.getProperty(language + "." + transactionList.get(i).getPaymentStatus().replaceAll("\\s+", ""));
				payTypeBean.setPaymentStatus(paymentStatus);
				payTypeBean.setTotalPaidAmount(transactionList.get(i).getTransanctionAmount());
				payTypeBean.setProcessId(Integer.parseInt(transactionList.get(i).getProcessId()));
				payTypeBean.setOrderId(transactionList.get(i).getOrderId());
				payTypeBean.setReceiptId(transactionList.get(i).getReceiptId());
				payTypeBean.setReceiptNo(transactionList.get(i).getReceiptNo());
				beanList.add(payTypeBean);
			}
		}
		return beanList;
	}

	@Override
	public List<PaymentRequestBean> savePaidFees(SavePaidBean savePaidBean) {
		System.out.println("schoolId"+savePaidBean.getSchoolId());
		Integer paymentGatewayId = feePaymentGatewayRepository.checkAlreadyPayment(savePaidBean.getOrderId(),
				savePaidBean.getTransactionId());
		if (paymentGatewayId == null) {
			List<FeeBean> feeBeanList = savePaidBean.getFeeBeanList();
			// String transferId =
			// createTransfer(feeBeanList,""+savePaidBean.getTransactionId());
			String transferId = "1";
			feePaymentGatewayRepository.updateTransferId(transferId, savePaidBean.getOrderId());
			List<SavePaidBean> savePaidBeanList = feePaymentGatewayRepository.getSchoolId(savePaidBean.getOrderId());
			savePaidBean.setSchoolId(savePaidBeanList.get(0).getSchoolId());
			savePaidBean.setProcessId(savePaidBeanList.get(0).getProcessId());
			if (StringUtils.isNotEmpty(transferId) && transferId.equals("1")) {
				List<FeeBean> transactionList = feePaymentGatewayRepository.findByOrderId(savePaidBean.getOrderId());
				String schoolKey = schoolMasterRepository.findBySchoolid(savePaidBean.getSchoolId()).getSchoolKey();
				String globalDbSchoolKey = "1".concat("" + schoolKey);
				List<String> receiptIdList = feeRepository.getMaxReceiptNo(savePaidBean.getSchoolId());
				String receiptNo = "" + 0;
				if (receiptIdList.size() == 0) {
					receiptNo = "" + 1;
				} else {
					receiptNo = "" + (Integer.valueOf(receiptIdList.get(0)) + 1);
				}
				String incrementedReceiptId = "" + feeRepository.getMaxReceiptId(savePaidBean.getSchoolId());
				String receiptId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedReceiptId);
				FeeReceiptModel feeReceiptModel = new FeeReceiptModel();
				feeReceiptModel.setReceiptId(Integer.parseInt(receiptId));
				SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
				schoolMasterModel.setSchoolid(savePaidBean.getSchoolId());
				if (savePaidBean.getYearId() != "" || savePaidBean.getYearId() != null) {
					YearMasterModel yearMasterModel = new YearMasterModel();
					yearMasterModel.setYearId(Integer.parseInt(savePaidBean.getYearId()));
					feeReceiptModel.setYearMaster(yearMasterModel);
				}
				feeReceiptModel.setSchoolMasterModel(schoolMasterModel);
				feeReceiptModel.setReceiptNo(receiptNo);
				feeReceiptModel.setHeadId(0);
				feeReceiptModel.setDeviceType(savePaidBean.getDeviceType());
				feeReceiptModel.setIpAddress(savePaidBean.getIpAddress());
				feeReceiptModel.setIsconsolidation(0);
				receiptId = "" + receiptRepository.saveAndFlush(feeReceiptModel).getReceiptId();
				feePaymentGatewayRepository.updateTransactionId(savePaidBean.getOrderId(),
						savePaidBean.getTransactionId(), Integer.parseInt(receiptId));
				if (CollectionUtils.isNotEmpty(transactionList)) {
					for (int i = 0; i < transactionList.size(); i++) {
						String incrementedId = "" + feeRepository.getMaxPaidId(savePaidBean.getSchoolId());
						String paidId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedId);
						FeePaidFeeModel feeModel = new FeePaidFeeModel();
						feeModel.setPaidId(Integer.parseInt(paidId));
						feeReceiptModel.setReceiptId(Integer.parseInt(receiptId));
						feeModel.setFeeReceiptModel(feeReceiptModel);
						FeeSettingMultiModel feeSettingMultiModel = new FeeSettingMultiModel();
						feeSettingMultiModel.setFeeSetMultiId(transactionList.get(i).getFeeSetMultiID());
						feeModel.setFeeSettingMultiModel(feeSettingMultiModel);
						FeeStudentFeeModel feeStudentFeeModel = new FeeStudentFeeModel();
						feeStudentFeeModel.setStudFeeId(transactionList.get(i).getStudFeeID());
						feeModel.setFeeStudentFeeModel(feeStudentFeeModel);
						feeModel.setSchoolMasterModel(schoolMasterModel);
						FeeHeadMasterModel feeHeadMasterModel = new FeeHeadMasterModel();
						feeHeadMasterModel.setHeadId(transactionList.get(i).getHeadId());
						feeModel.setFeeHeadMasterModel(feeHeadMasterModel);
						FeeSubheadMasterModel feeSubheadMasterModel = new FeeSubheadMasterModel();
						feeSubheadMasterModel.setSubheadId(transactionList.get(i).getSubheadId());
						feeModel.setFeeSubheadMasterModel(feeSubheadMasterModel);
						feeModel.setPayFee(transactionList.get(i).getPaidFee());
						feeModel.setPaidfeeDate(transactionList.get(i).getPaidFeeDate());
						feeModel.setPayType(transactionList.get(i).getPayType());
						feeModel.setDiscount(Double.parseDouble(transactionList.get(i).getDiscount()));
						feeModel.setDeviceType(savePaidBean.getDeviceType());
						feeModel.setIpAddress(savePaidBean.getIpAddress());

						Integer principaluserId = userRepository.getprincipalUserId(savePaidBean.getSchoolId());
						AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
						appUserRoleModel.setAppUserRoleId(principaluserId);
						feeModel.setAppUserRoleModel(appUserRoleModel);
						paidFeeRepository.saveAndFlush(feeModel);

						if (transactionList.get(i).getDueFee() > 0) {
							String incrementedDueId = "" + feeRepository.getMaxPaidDueId(savePaidBean.getSchoolId());
							String paidDueId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedDueId);
							FeePaidDueModel feePaidDueModel = new FeePaidDueModel();
							feePaidDueModel.setPaidDueId(Integer.parseInt(paidDueId));
							feeReceiptModel.setReceiptId(Integer.parseInt(receiptId));
							feePaidDueModel.setFeeReceiptModel(feeReceiptModel);
							feePaidDueModel.setFeeSettingMultiModel(feeSettingMultiModel);
							feePaidDueModel.setFeeStudentFeeModel(feeStudentFeeModel);
							feePaidDueModel.setSchoolMasterModel(schoolMasterModel);
							feePaidDueModel.setFeeHeadMasterModel(feeHeadMasterModel);
							feePaidDueModel.setFeeSubheadMasterModel(feeSubheadMasterModel);
							feePaidDueModel.setDueFee(transactionList.get(i).getDueFee());
							feePaidDueModel.setAppUserRoleModel(appUserRoleModel);
							feePaidDueModel.setDeviceType(savePaidBean.getDeviceType());
							feePaidDueModel.setIpAddress(savePaidBean.getIpAddress());
							paidFineRepository.saveAndFlush(feePaidDueModel);
						}
					}

					List<PaymentRequestBean> paymentRequestBeans = new ArrayList<>();
					PaymentRequestBean paymentRequestBean = new PaymentRequestBean();
					paymentRequestBeans.add(paymentRequestBean);
					return paymentRequestBeans;
				}
			}
		}
		return new ArrayList<>();
	}

	@Override
	public List<OrderBean> getOrders(String orderId) {
		return new ArrayList<>();
	}

	private List<DiscountBean> calculateDiscount(Integer schoolId, Integer yearId, String date,
			List<FeeBean> feeBeanList) {
		List<DiscountBean> discountBeanList = new ArrayList<>();
		List<FeeDiscountLabelModel> discountList = discountLabelRepository.findDiscountLabels(schoolId,
				feeBeanList.get(0).getStandardId(), yearId, date);
		if (CollectionUtils.isNotEmpty(discountList) && discountList.size() > 1) {
			discountList.parallelStream().forEach(discount -> {
				DiscountBean discountBean = new DiscountBean();
				discountBean.setDiscountDate("" + discount.getDiscountDate());
				discountBean.setMinimumAmount(discount.getMinimumAmount());
				discountBean.setType(discount.getDiscountType());
				discountBean.setDiscountPercent(discount.getPercentAmount());
				discountBean.setPercentFlag(discount.getPercentFlag());
				StringBuilder discountString = new StringBuilder();
				discountString.append(DiscountLabels.PAY.getDiscountLabels());

				if ((Numbers.Zero.getNumber()).equals(discount.getDiscountType())) {
					discountString.append(" " + DiscountLabels.FULLAMOUNT.getDiscountLabels());
				} else if ((Numbers.One.getNumber()).equals(discount.getDiscountType())) {
					discountString.append(" " + DiscountLabels.HALFAMOUNT.getDiscountLabels());
				} else {
					discountString.append(" " + DiscountLabels.MINIMUMRS.getDiscountLabels() + StringUtils.EMPTY
							+ discount.getMinimumAmount());
				}
				if (StringUtils.isNotEmpty(" " + discount.getDiscountDate())) {
					SimpleDateFormat month_date = new SimpleDateFormat(DateFormat.ddmmmyyyy.getDateFormat(),
							Locale.ENGLISH);
					SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.ddmmyyyy.getDateFormat());
					Date actualDate = null;
					try {
						actualDate = sdf.parse(discount.getDiscountDate());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					String discountDate = month_date.format(actualDate);

					discountString
							.append(" " + DiscountLabels.BY.getDiscountLabels() + StringUtils.EMPTY + discountDate);
				}
				discountString.append(" " + DiscountLabels.TOAVAIL.getDiscountLabels());
				if ((Numbers.One.getNumber()).equals(discount.getPercentFlag())) {
					discountString.append(" " + DiscountLabels.RS.getDiscountLabels() + discount.getPercentAmount());
				}

				else if ((Numbers.Zero.getNumber()).equals(discount.getPercentFlag())) {
					discountString
							.append(" " + discount.getPercentAmount() + DiscountLabels.PERCENT.getDiscountLabels());
				}
				discountString.append(" " + DiscountLabels.DISCOUNT.getDiscountLabels());
				discountBean.setDiscountLabel(discountString.toString());
				discountBeanList.add(discountBean);
			});
		} else if (CollectionUtils.isNotEmpty(discountList) && discountList.size() == 1) {
			DiscountBean discountBean = new DiscountBean();
			discountBean.setDiscountDate(StringUtils.EMPTY);
			discountBean.setMinimumAmount(Numbers.Zero.getNumber());
			discountBean.setType(Numbers.Zero.getNumber());
			if (Numbers.Zero.getNumber().equals(discountList.get(0).getDiscountType())) {
				discountBean.setType(Numbers.Two.getNumber());
			}
			discountBean.setDiscountPercent(StringUtils.EMPTY);
			discountBean.setPercentFlag(Numbers.Zero.getNumber());
			discountBean.setDiscountLabel(DiscountLabels.NODISCOUNT.getDiscountLabels());
			discountBeanList.add(discountBean);
		} else {
			DiscountBean discountBean = new DiscountBean();
			discountBean.setDiscountDate(StringUtils.EMPTY);
			discountBean.setMinimumAmount(Numbers.Zero.getNumber());
			discountBean.setType(StringUtils.EMPTY);
			discountBean.setDiscountPercent(StringUtils.EMPTY);
			discountBean.setPercentFlag(Numbers.Zero.getNumber());
			discountBean.setDiscountLabel(DiscountLabels.NODISCOUNT.getDiscountLabels());
			discountBeanList.add(discountBean);

			discountBean = new DiscountBean();
			discountBean.setDiscountDate(StringUtils.EMPTY);
			discountBean.setMinimumAmount(Numbers.Zero.getNumber());
			discountBean.setType(StringUtils.EMPTY);
			discountBean.setDiscountPercent(StringUtils.EMPTY);
			discountBean.setPercentFlag(Numbers.Zero.getNumber());
			discountBean.setDiscountLabel(DiscountLabels.NODISCOUNT.getDiscountLabels());
			discountBeanList.add(discountBean);
		}
		discountBeanList.sort(Comparator.comparing(DiscountBean::getType));
		return discountBeanList;
	}

	private String createOrderId(Double amount, List<FeeBean> feeBeanList) {
		String orderId = StringUtils.EMPTY;
		URL obj;
		try {
			List<TransferBean> transferBeanList = new ArrayList<>();
			for (int i = 0; i < feeBeanList.size(); i++) {
				if (!(("0").equals(feeBeanList.get(i).getPaymentGatewayAccountId()))) {
					TransferBean transferBean = new TransferBean();
					transferBean.setAccount(feeBeanList.get(i).getPaymentGatewayAccountId());
					Double amount1 = (feeBeanList.get(i).getPaymentGatewayAmount() * 100);
					Double myDouble = amount1;
					transferBean.setAmount(Integer.valueOf(myDouble.intValue()));
					transferBean.setCurrency(DiscountLabels.INR.getDiscountLabels());
					transferBeanList.add(transferBean);
				}
			}
			OrderBean orderRequest = new OrderBean();
			orderRequest.setAmount(Long.valueOf((Double.valueOf(amount * 100).intValue())));
			orderRequest.setPayment_capture(1);
			orderRequest.setCurrency(DiscountLabels.INR.getDiscountLabels());
			orderRequest.setTransfers(transferBeanList);
			ObjectMapper Obj = new ObjectMapper();
			obj = new URL(ConfigurationProperties.razorPayOrderAPI);
			HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
			postConnection.setRequestMethod("POST");
			postConnection.setRequestProperty(RestUrlConfigs.AUTHORIZATION.getRestUrlConfigs(),
					ConfigurationProperties.razorPayAuthorization);
			postConnection.setRequestProperty(RestUrlConfigs.CONTENTTYPE.getRestUrlConfigs(),
					RestUrlConfigs.APPLICATIONJSON.getRestUrlConfigs());
			postConnection.setDoOutput(true);
			OutputStream os = postConnection.getOutputStream();
			String jsonStr = StringUtils.EMPTY;
			try {
				jsonStr = Obj.writeValueAsString(orderRequest);

			} catch (Exception e1) {
				e1.printStackTrace();
			}
			os.write(jsonStr.getBytes());
			os.flush();
			os.close();
			int responseCode = postConnection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
				String inputLine;
				StringBuilder response = new StringBuilder();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				JSONObject jsonObj = new JSONObject(response.toString());
				orderId = jsonObj.getString("id");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return orderId;
	}

	private String createTransfer(List<FeeBean> feeBeanList, String transanctionId) {
		URL obj;
		String razorPayTransferApi = ConfigurationProperties.razorPayTransferAPI;
		razorPayTransferApi = razorPayTransferApi.replace("{transanctionId}", transanctionId);
		try {
			List<TransferBean> transferBeanList = new ArrayList<>();
			for (int i = 0; i < feeBeanList.size(); i++) {
				if (!(("0").equals(feeBeanList.get(i).getPaymentGatewayAccountId()))) {
					TransferBean transferBean = new TransferBean();
					transferBean.setAccount("" + feeBeanList.get(i).getPaymentGatewayAccountId());
					Double amount = (feeBeanList.get(i).getPaymentGatewayAmount() * 100);
					Double myDouble = amount;
					transferBean.setAmount(Integer.valueOf(myDouble.intValue()));
					transferBean.setCurrency(DiscountLabels.INR.getDiscountLabels());
					transferBeanList.add(transferBean);
				}
			}
			TransferRequestBean transferRequestBean = new TransferRequestBean();
			transferRequestBean.setTransfers(transferBeanList);
			ObjectMapper Obj = new ObjectMapper();
			obj = new URL(razorPayTransferApi);
			HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
			postConnection.setRequestMethod(RestUrlConfigs.POST.getRestUrlConfigs());
			postConnection.setRequestProperty(RestUrlConfigs.AUTHORIZATION.getRestUrlConfigs(),
					ConfigurationProperties.razorPayAuthorization);
			postConnection.setRequestProperty(RestUrlConfigs.CONTENTTYPE.getRestUrlConfigs(),
					RestUrlConfigs.APPLICATIONJSON.getRestUrlConfigs());
			postConnection.setDoOutput(true);
			OutputStream os = postConnection.getOutputStream();
			String jsonStr = StringUtils.EMPTY;
			try {
				jsonStr = Obj.writeValueAsString(transferRequestBean);
				System.out.println("eprashashan" + jsonStr);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			os.write(jsonStr.getBytes());
			os.flush();
			os.close();
			int responseCode = postConnection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				/*
				 * BufferedReader in = new BufferedReader(new
				 * InputStreamReader(postConnection.getInputStream())); String inputLine;
				 * StringBuilder response = new StringBuilder(); while ((inputLine = in
				 * .readLine()) != null) { response.append(inputLine); } in .close(); JSONObject
				 * jsonObj = new JSONObject(response.toString()); JSONObject itemObj = new
				 * JSONObject(jsonObj.getJSONArray("items").toString());
				 * transferId=itemObj.getString("id");
				 */
				return Numbers.One.getNumber();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Numbers.Zero.getNumber();
	}

	@Override
	public Integer updatePaymentStatus(Integer schoolId, String processId, String statusCode, String status,
			String statusMessage) {
		feePaymentGatewayRepository.updateStatus(schoolId, processId, statusCode, status, statusMessage);
		return 1;
	}

	@Override
	public List<FeeIdBean> getStudentFeeId(Integer renewStudentId) {
		return feePaymentGatewayRepository.getStudentFeeId(renewStudentId);
	}

	@Override
	public Double getFeesForYearAndStandard(Integer schoolId, String standardId, String yearId) {
		Integer flag = feePaymentGatewayRepository.getOnlineAdmissionSetting(schoolId);
		if (flag == 1) {
			return feePaymentGatewayRepository.getFeesForYearAndStandard(schoolId, Integer.parseInt(standardId),
					Integer.parseInt(yearId));
		} else {
			return 0.0;
		}

	}

	@Override
	public void setStudentFee(String yearId, String standardId, String schoolId, Integer studentId, Integer userId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(Integer.parseInt(schoolId));

		AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
		appUserRoleModel.setAppUserRoleId(userId);

		YearMasterModel yearMasterModel = new YearMasterModel();
		yearMasterModel.setYearId(Integer.parseInt(yearId));

		StandardMasterModel standardMasterModel = new StandardMasterModel();
		standardMasterModel.setStandardId(Integer.parseInt(standardId));
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(Integer.valueOf(schoolId));
		String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

		String incrementedId = "" + feePaymentGatewayRepository.getMaxFeeSettingId(Integer.parseInt(schoolId));
		String maxFeeSettingId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);

		Double totalFee = feePaymentGatewayRepository.getTotalAssignedFeeForYearAndStandard(Integer.parseInt(schoolId),
				Integer.parseInt(standardId), Integer.parseInt(yearId));
		Integer feeTypeId = feePaymentGatewayRepository.getFeeTypeId(Integer.parseInt(schoolId));
		FeeTypeModel feeTypeModel = new FeeTypeModel();
		feeTypeModel.setFeeTypeId(feeTypeId);
		FeeSettingModel feeSettingModel = new FeeSettingModel();
		feeSettingModel.setFeeTypeModel(feeTypeModel);
		feeSettingModel.setSchoolMasterModel(schoolMasterModel);
		feeSettingModel.setAppUserRoleModel(appUserRoleModel);
		feeSettingModel.setTotalAssignFee(totalFee);
		feeSettingModel.setFeeSettingId(Integer.parseInt(maxFeeSettingId));
		feeSettingModel.setYearMasterModel(yearMasterModel);
		standardMasterModel.setStandardId(Integer.parseInt(standardId));
		feeSettingModel.setStandardMasterModel(standardMasterModel);
		feeSettingRepository.saveAndFlush(feeSettingModel);

		FeeStudentFeeModel feeStudentFee = new FeeStudentFeeModel();
		incrementedId = "" + feePaymentGatewayRepository.getMaxStudentFeeId(Integer.parseInt(schoolId));
		String studFeeId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
		feeStudentFee.setStudFeeId(Integer.parseInt(studFeeId));
		feeStudentFee.setFeeSettingModel(feeSettingModel);
		feeStudentFee.setSchoolMasterModel(schoolMasterModel);
		feeStudentFee.setAppUserRoleModel(appUserRoleModel);
		StudentStandardRenewModel studentStandardRenewModel = new StudentStandardRenewModel();
		StudentMasterModel studentMasterModel = new StudentMasterModel();
		studentMasterModel.setStudentId(studentId);
		studentStandardRenewModel.setStudentMasterModel(studentMasterModel);
		studentStandardRenewModel.setRenewStudentId(studentId);
		feeStudentFee.setStudentStandardRenewModel(studentStandardRenewModel);
		feeStudentFeeRepository.saveAndFlush(feeStudentFee);

		List<FeeIdBean> feeBeanList = feePaymentGatewayRepository.getFeeStructureForYearAndStandard(
				Integer.parseInt(schoolId), Integer.parseInt(standardId), Integer.parseInt(yearId));
		if (CollectionUtils.isNotEmpty(feeBeanList)) {
			for (int i = 0; i < feeBeanList.size(); i++) {
				incrementedId = "" + feePaymentGatewayRepository.getMaxMultiId(Integer.parseInt(schoolId));
				String multiId = UTFConversionUtil.generatePrimaryKey1(globalDbSchoolKey, incrementedId);
				FeeSettingMultiModel feeSettingMultiModel = new FeeSettingMultiModel();
				FeeHeadMasterModel feeHeadMasterModel = new FeeHeadMasterModel();
				feeHeadMasterModel.setHeadId(feeBeanList.get(i).getHeadId());
				FeeSubheadMasterModel feeSubheadMasterModel = new FeeSubheadMasterModel();
				feeSubheadMasterModel.setSubheadId(feeBeanList.get(i).getSubheadId());
				feeSettingMultiModel.setFeeHeadMasterModel(feeHeadMasterModel);
				feeSettingMultiModel.setFeeSubheadMasterModel(feeSubheadMasterModel);
				feeSettingMultiModel.setFeeSettingModel(feeSettingModel);
				feeSettingMultiModel.setAssignFee(feeBeanList.get(i).getAssignFee());
				if (feeBeanList.get(i).getFine() != 0) {
					feeSettingMultiModel.setFine(feeBeanList.get(i).getFine());
				}
				feeSettingMultiModel.setDueDate(feeBeanList.get(i).getDueDate());
				feeSettingMultiModel.setFineIncreament(feeBeanList.get(i).getFineIncrement());

				feeSettingMultiModel.setSchoolMasterModel(schoolMasterModel);
				feeSettingMultiModel.setAppUserRoleModel(appUserRoleModel);
				feeSettingMultiModel.setFeeSetMultiId(new BigInteger(multiId));
				feeSettingMultiRepository.saveAndFlush(feeSettingMultiModel);
			}
		}
	}

	public List<SchoolDetailBean> getSchoolDetails(String schoolId) {
		List<SchoolDetailBean> schoolBeanList = feePaymentGatewayRepository.getSchoolDetails(Integer.valueOf(schoolId));
		schoolBeanList.forEach(schoolBean -> {
			try {
				schoolBean.setSchoolName(cry.decrypt("qwertyschoolname",
						StringEscapeUtils.escapeJava(schoolBean.getSchoolName().replaceAll("[\\r\\n]+", ""))));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return schoolBeanList;
	}

	@Override
	public UserDetailsBean getStudentDetails(String studentID) {
		Date date = new Date();
		String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
		return studentMasterRepository.findStudentDetails1(Integer.valueOf(studentID), currentDate);
	}

	@Override
	public List<SessionResponseBean> getSessionList(String razorpay_payment_id, String razorpay_order_id) {
		List<SessionResponseBean> sessionList = feePaymentGatewayRepository.getSessionList(razorpay_payment_id,
				razorpay_order_id);
		return sessionList;
	}

	@Override
	public void sendSuccessMessage(String renewId, String mobileNumber, String schoolkey, String schoolName,
			String studentName) {
		List<FeeIdBean> studentFeeIdList = feePaymentGatewayRepository.getStudentFeeId(Integer.valueOf(renewId));
		Integer studFeeId = studentFeeIdList.get(0).getFeeId();
		List<FeeBean> transactionList = feePaymentGatewayRepository.getTransactions(studFeeId);
		String message = "Dear " + studentName + ", thanks for paying fees of Rs. "
				+ transactionList.get(0).getTransanctionAmount() + "  receipt no."
				+ transactionList.get(0).getReceiptNo() + " dated  " + transactionList.get(0).getPaidFeeDate()
				+ ". We will review payment and approve your admission . - " + schoolName;
		userAuthenticationServiceImpl.sendOtpMessage(mobileNumber, message, Integer.valueOf(schoolkey), 1);

	}

	@Override
	public void sendFailureMessage(String renewId, String mobileNumber, String schoolkey, String schoolName,
			String studentName) {
		List<FeeIdBean> studentFeeIdList = feePaymentGatewayRepository.getStudentFeeId(Integer.valueOf(renewId));
		Integer studFeeId = studentFeeIdList.get(0).getFeeId();
		List<FeeBean> transactionList = feePaymentGatewayRepository.getTransactions(studFeeId);
		String message = "Dear " + studentName + ", Your payment of Rs.  "
				+ transactionList.get(0).getTransanctionAmount()
				+ " was failed due to some technical error kindly pay again to complete transaction. - " + schoolName;
		userAuthenticationServiceImpl.sendOtpMessage(mobileNumber, message, Integer.valueOf(schoolkey), 1);
	}

	@Override
	public void updateStatus(String renewId) {
		List<FeeIdBean> studentFeeIdList = feePaymentGatewayRepository.getStudentFeeId(Integer.valueOf(renewId));
		Integer studFeeId = studentFeeIdList.get(0).getFeeId();
		List<FeeBean> transactionList = feePaymentGatewayRepository.getTransactions(studFeeId);
		String successStatus = "Failed ";
		feePaymentGatewayRepository.updateFailureStatus(successStatus, transactionList.get(0).getOrderId());
	}

	@Override
	public Boolean getFeesSettingFlag(int schoolId, int studentId) {
		Integer flag = feePaymentGatewayRepository.getOnlineAdmissionSetting(schoolId);
		if (flag == 1) {
			return true;
		} else if (flag == 2) {
			String approvalFlag = feePaymentGatewayRepository.getStudentApprovalFlag(studentId);
			if (approvalFlag.equals("0")) {
				return true;
			}
		}

		return false;
	}

	@Override
	public List<PaymentSettingBean> getPaymentSetting(String schoolId, String yearId, String standardId) {
		return feePaymentGatewayRepository.getPaymentSetting(Integer.valueOf(schoolId), Integer.valueOf(yearId),
				Integer.valueOf(standardId));
	}

	@Override
	public void sendSuccessmail(String razorpay_payment_id, String renewId, UserDetailsBean userDetailsBean,
			String schoolkey, String schoolName) {

		StringBuilder subject = new StringBuilder();

		List<EmailRequestBean> emailRequestList = feeReportsRepositoryImpl.getEmailDetails(schoolkey,
				razorpay_payment_id);

		subject.append("Online Payment Rs. " + emailRequestList.get(0).getAmount() + "from "
				+ userDetailsBean.getMobileNumber() + "-" + userDetailsBean.getFirstName());

		try {
			sendEmail(subject.toString(), userDetailsBean, schoolName, emailRequestList.get(0).getEmailId());
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	public void sendEmail(String subject, UserDetailsBean userDetailsBean, String schoolName, String emailId)
			throws AddressException, MessagingException {

////		String host = "smtp.gmail.com";
////		String port = "587";
////		String mailFrom = "onlinefees.eprashasan@gmail.com";
////		String password = "Ingenio!123";
//		String host = "smtp.gmail.com";
//		String port = "587";
//		String mailFrom = "feedback.ingenio@gmail.com";
//		String password = "eprashasan";
//		String recipients = "onlinefees.eprashasan@gmail.com";

		String host = "smtp.gmail.com";
		String port = "587";
		String mailFrom = "feedback.ingenio@gmail.com";
		String password = "eprashasan";
		String[] recipients = { "onlinefees.eprashasan@gmail.com" };

		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.user", mailFrom);
		properties.put("mail.password", password);
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailFrom, password);
			}
		};

		Session session = Session.getInstance(properties, auth);

		// creates a new e-mail message
		MimeMessage msg = new MimeMessage(session);

		msg.setFrom(new InternetAddress(mailFrom));
		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}

		msg.setRecipients(Message.RecipientType.TO, addressTo);
		msg.setSubject(subject, "UTF-8");
		msg.setSentDate(new Date());

		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();

		messageBodyPart.setText("hi test mail");

		multipart.addBodyPart(messageBodyPart);

		msg.setContent(multipart);

		Transport t = session.getTransport("smtp");
		t.connect(mailFrom, password);
		t.sendMessage(msg, msg.getAllRecipients());
		t.close();
	}

	@Override
	public FeeProspectBean getProspectFee(Integer schoolId, Integer yearId, Integer standardId) {
		FeeProspectBean feeProspectBean = new FeeProspectBean();
		List<FeeProspectBean> prospectFeeList = feePaymentGatewayRepository.getProspectFee(schoolId, yearId,
				standardId);
		prospectFeeList.forEach(prospect -> {
			prospect.setTotalFee(prospect.getCgst() + prospect.getSgst() + prospect.getProspectFee());
		});
		Double alltotal = 0.0;
		for (int i = 0; i < prospectFeeList.size(); i++) {
			alltotal = alltotal + prospectFeeList.get(i).getTotalFee();
		}
		feeProspectBean.setAllTotalFee(alltotal);
		feeProspectBean.setProspectfeeList(prospectFeeList);
		return feeProspectBean;
	}

	@Override
	public List<FeeBean> saveTempForProspectFee(PaymentRequestBean paymentRequestBean)   {
		String orderId = StringUtils.EMPTY;
		List<FeeBean> feeBeanList = new ArrayList<>();
		double paymentGatewayAmount = (paymentRequestBean.getTotalPaidAmount() - paymentRequestBean.getTotalDiscount())
				+ paymentRequestBean.getConvenienceFee();
		List<TransferBean> transferBeanList = new ArrayList<>();
		TransferBean transferBean = new TransferBean();
		transferBean.setAccount("acc_DizjGJRob2kdYM");
		Double amount1 = (paymentGatewayAmount * 100);
		Double myDouble = amount1;
		transferBean.setAmount(Integer.valueOf(myDouble.intValue()));
		transferBean.setCurrency(DiscountLabels.INR.getDiscountLabels());
		transferBeanList.add(transferBean);
		URL obj;
		OrderBean orderRequest = new OrderBean();
		orderRequest.setAmount(Long.valueOf((Double.valueOf(paymentGatewayAmount * 100).intValue())));
		orderRequest.setPayment_capture(1);
		orderRequest.setCurrency(DiscountLabels.INR.getDiscountLabels());
		orderRequest.setTransfers(transferBeanList);
		ObjectMapper Obj = new ObjectMapper();
		try {
			obj = new URL(ConfigurationProperties.razorPayOrderAPI);
		HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
		postConnection.setRequestMethod("POST");
		postConnection.setRequestProperty(RestUrlConfigs.AUTHORIZATION.getRestUrlConfigs(),
				ConfigurationProperties.razorPayAuthorization);
		postConnection.setRequestProperty(RestUrlConfigs.CONTENTTYPE.getRestUrlConfigs(),
				RestUrlConfigs.APPLICATIONJSON.getRestUrlConfigs());
		postConnection.setDoOutput(true);
		OutputStream os = postConnection.getOutputStream();
		String jsonStr = StringUtils.EMPTY;
		try {
			jsonStr = Obj.writeValueAsString(orderRequest);

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		os.write(jsonStr.getBytes());
		os.flush();
		os.close();
		int responseCode = postConnection.getResponseCode();
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
			String inputLine;
			StringBuilder response = new StringBuilder();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			JSONObject jsonObj = new JSONObject(response.toString());
			orderId = jsonObj.getString("id");
			
			DummyRegistrationModel dummyRegistrationModel = dummyRegistrationRepository.findByMobileNo(paymentRequestBean.getMobileNo());
			dummyRegistrationModel.setOrder_id(orderId);
			dummyRegistrationRepository.save(dummyRegistrationModel);
		}

		FeeBean feeBean = new FeeBean();
		feeBean.setOrderId(orderId);
		feeBeanList.add(feeBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feeBeanList;

	}

	@Override
	public FeeProspectBean getRegistrationSetting(Integer schoolId) {
		List<FeeProspectBean> prospectFeeList = feePaymentGatewayRepository.getRegistrationSetting(schoolId);
		if(prospectFeeList.size()==0) {
			FeeProspectBean feeProspectBean =new FeeProspectBean();
			feeProspectBean.setRegistrationFlag(0);
			prospectFeeList.add(feeProspectBean);
		}
		return prospectFeeList.get(0);
	}

}
