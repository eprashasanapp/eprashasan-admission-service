package com.ingenio.admission.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.ingenio.admission.bean.AndroidMenuBean;
import com.ingenio.admission.bean.AppVersionForReactBean;
import com.ingenio.admission.bean.StandardDivisionBean;
import com.ingenio.admission.bean.StudentCountBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.StudentImageBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.config.ConfigurationProperties;
import com.ingenio.admission.model.DynamicAcademicMonthsModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StaffSubjectDetailsModel;
import com.ingenio.admission.model.YearMasterModel;
import com.ingenio.admission.repository.AndroidMenuNotForSchoolRepository;
import com.ingenio.admission.repository.AndroidMenuRepository;
import com.ingenio.admission.repository.AppUserRoleRepository;
import com.ingenio.admission.repository.AppVersionMasterRepository;
import com.ingenio.admission.repository.AppVersionToRoleRepository;
import com.ingenio.admission.repository.AppVersionToSchoolRepository;
import com.ingenio.admission.repository.DynamicAcademicMonthsRepository;
import com.ingenio.admission.repository.IcardMasterRepository;
import com.ingenio.admission.repository.SansthaRegistrationRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.repository.StaffBasicDetailsRepository;
import com.ingenio.admission.repository.StaffSubjectDetailsRepository;
import com.ingenio.admission.repository.StudentDetailsRepository;
import com.ingenio.admission.repository.StudentMasterRepository;
import com.ingenio.admission.repository.StudentStandardRepository;
import com.ingenio.admission.repository.UserRepository;
import com.ingenio.admission.repository.YearMasterRepository;
import com.ingenio.admission.repository.impl.FCMPushNotificationRepositoryImpl;
import com.ingenio.admission.service.UserRoleService;
import com.ingenio.admission.util.HttpClientUtil;
import com.ingenio.admission.util.Utillity;

@Service

//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")

//@PropertySource("classpath:messages_en.properties")
//
//@PropertySource("classpath:messages_mr.properties")
//
//@PropertySource("classpath:messages_hn.properties")

public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	StudentMasterRepository studentMasterRepository;

	@Autowired
	StaffBasicDetailsRepository staffBasicDetailsRepository;

	@Autowired
	StaffSubjectDetailsRepository staffSubjectDetailsRepository;

	@Autowired
	AndroidMenuRepository androidMenuRepository;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Autowired
	SansthaRegistrationRepository sansthaRegistrationRepository;

	@Autowired
	YearMasterRepository yearMasterRepository;

	@Autowired
	DynamicAcademicMonthsRepository dynamicAcademicMonthsRepository;

	@Autowired
	private Environment env;

	@Autowired
	StudentDetailsRepository studentDetailsRepository;

	@Autowired
	AppUserRoleRepository appUserRoleRepository;

	@Autowired
	AppVersionMasterRepository appVersionMasterRepository;

	@Autowired
	AppVersionToSchoolRepository appVersionToSchoolRepository;

	@Autowired
	AppVersionToRoleRepository appVersionToRoleRepository;

	@Autowired
	FCMPushNotificationRepositoryImpl fCMPushNotificationServiceImpl;

	@Autowired
	IcardMasterRepository icardMasterRepository;

	@Autowired
	Utillity utility;

	@Autowired
	AndroidMenuNotForSchoolRepository androidMenuNotForSchoolRepository;

	@Autowired
	StudentStandardRepository studentStandardRepository;

	@Override
	public UserDetailsBean getStudentDetails(Integer userId, String deviceId, String IMEICode, String language,
			String mobNum, String deviceType) {
		UserDetailsBean userDetailsBean = new UserDetailsBean();
		UserDetailsBean appUserModel = null;
//		if(mobNum!=null || StringUtils.isNotEmpty(mobNum)) {
//			String url = ConfigurationProperties.checkExistingUser+"="+mobNum+"&deviceType="+deviceType+"&IMEICode="+IMEICode;
//			ResponseBodyBean responseBodyBean = (ResponseBodyBean) RestUtil.makeRestCallForExistingUser(url);
//			if(responseBodyBean!=null) {
//			Integer isUserExit = (Integer) responseBodyBean.getResponseData();
//			if(isUserExit==1) {
//				appUserModel = userRepository.findByAppUserRoleId(userId);
//				}
//			else {
//				appUserModel=null;
//			}
//		}
//		} 
		appUserModel = userRepository.findByAppUserRoleId(userId);
		if (appUserModel != null) {
			String roleName = appUserModel.getRoleName();
			Integer schoolId = appUserModel.getSchoolId();
			Integer studentId = appUserModel.getStaffId();
			Date date = new Date();
			if (roleName.equalsIgnoreCase("ROLE_PARENT1") || roleName.equalsIgnoreCase("ROLE_PARENT2")
					|| roleName.equalsIgnoreCase("ROLE_STUDENT")) {
				String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
				userDetailsBean = studentMasterRepository.findStudentDetails(studentId, currentDate);
				if (userDetailsBean != null) {
					String photoString = setStudentPhotoUrl(schoolId, userDetailsBean);
					if (!photoString.equalsIgnoreCase(null)) {
						userDetailsBean.setStudentPhotoUrl(photoString);
					} else {
						userDetailsBean.setStudentPhotoUrl("");
					}

					Integer currentYear = getCurrentYear(schoolId);
					userDetailsBean.setCurrentYear(currentYear);
					userDetailsBean.setRollColor("#FFC75A");
					YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
					if (yearMasterModel != null) {
						userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
					}
					List<AndroidMenuBean> menuList = new ArrayList<AndroidMenuBean>();
//				System.out.println(userDetailsBean.getIsOnlineAdmission());
					if (!userDetailsBean.getIsOnlineAdmission().equalsIgnoreCase("0")) {
						menuList = androidMenuRepository.findBySchoolMasterModel(schoolId, getReverseRoles(roleName));
					} else {
						menuList = androidMenuRepository.findBySchoolMasterModel(schoolId, 19,
								getReverseRoles(roleName));
					}

					userDetailsBean.setAndroidMenuList(menuList);
				}
			}

			else if (roleName.equalsIgnoreCase("ROLE_TEACHINGSTAFF")
					|| roleName.equalsIgnoreCase("ROLE_NONTEACHINGSTAFF")
					|| roleName.equalsIgnoreCase("ROLE_PRINCIPAL")) {
				String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
				userDetailsBean = staffBasicDetailsRepository.findStaffDetails(studentId);
				userDetailsBean.setRollColor("#8AD271");
				if (userDetailsBean != null) {
					userDetailsBean.setStudentPhotoUrl(setStaffPhotoUrl(schoolId, userDetailsBean));
					if (!roleName.equalsIgnoreCase("ROLE_PRINCIPAL")) {
						List<AndroidMenuBean> menuList = androidMenuRepository.findBySchoolMasterModel(schoolId,
								getReverseRoles(roleName));
						List<AndroidMenuBean> updatedMenuList = new ArrayList<>();
						List<StandardDivisionBean> standardDivisionList = new ArrayList<>();
						List<StaffSubjectDetailsModel> staffSubjectDetailsList = staffSubjectDetailsRepository
								.findClassteacher(studentId);
						userDetailsBean.setStandardName("");
						userDetailsBean.setDivisionName("");
						if (CollectionUtils.isNotEmpty(staffSubjectDetailsList)) {
							userDetailsBean.setStandardName(
									staffSubjectDetailsList.get(0).getStandardMasterModel().getStandardName());
							userDetailsBean.setDivisionName(
									staffSubjectDetailsList.get(0).getDivisionMasterModel().getDivisionName());
							staffSubjectDetailsList.stream().forEach(StaffSubjectDetailsModel -> {
								StandardDivisionBean standardDivisionBean = new StandardDivisionBean();
								standardDivisionBean.setDivisionId(
										StaffSubjectDetailsModel.getDivisionMasterModel().getDivisionId());
								standardDivisionBean.setDivisionName(
										StaffSubjectDetailsModel.getDivisionMasterModel().getDivisionName());
								standardDivisionBean.setStandardId(
										StaffSubjectDetailsModel.getStandardMasterModel().getStandardId());
								standardDivisionBean.setStandardName(
										StaffSubjectDetailsModel.getStandardMasterModel().getStandardName());
								standardDivisionList.add(standardDivisionBean);
							});
						}
						userDetailsBean.setStandardDivisionList(standardDivisionList);
						userDetailsBean.setRollColor("#009FD5");

						if (CollectionUtils.isEmpty(staffSubjectDetailsList) && CollectionUtils.isNotEmpty(menuList)) {
							menuList.stream().forEach(menu -> {
								if (menu.getMenuId() != 3) {
									updatedMenuList.add(menu);
								}
							});

							userDetailsBean.setAndroidMenuList(updatedMenuList);
						} else {
							userDetailsBean.setAndroidMenuList(menuList);
						}

					}
					Integer currentYear = getCurrentYear(schoolId);
					userDetailsBean.setCurrentYear(currentYear);
					YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
					if (yearMasterModel != null) {
						userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
					}
				}

			} else if (roleName.equalsIgnoreCase("ROLE_SANSTHAOFFICER")
					|| roleName.equalsIgnoreCase("ROLE_SUPEROFFICER")) {

				if (roleName.equalsIgnoreCase("ROLE_SANSTHAOFFICER")) {
					userDetailsBean = staffBasicDetailsRepository.findSansthaOfficerDetails(userId);
					userDetailsBean.setRollColor("#FF706F");
					if (userDetailsBean != null) {
						userDetailsBean.setStudentPhotoUrl(setSansthaOfficerPhotoUrl(schoolId, userDetailsBean));
					}
				}
				if (roleName.equalsIgnoreCase("ROLE_SUPEROFFICER")) {
					userDetailsBean = staffBasicDetailsRepository.findSuperOfficerDetails(userId);
					userDetailsBean.setRollColor("#FF706F");
					if (userDetailsBean != null) {
						userDetailsBean.setStudentPhotoUrl(setSuperOfficerPhotoUrl(schoolId, userDetailsBean));
					}
				}
			}

			if (userDetailsBean != null) {
				if (CollectionUtils.isNotEmpty(userDetailsBean.getAndroidMenuList())
						&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
					if (userDetailsBean != null && CollectionUtils.isNotEmpty(userDetailsBean.getAndroidMenuList())) {
						userDetailsBean.getAndroidMenuList().stream().forEach(menu -> {
							String menuName = env
									.getProperty(language + "." + menu.getMenuName().replaceAll("\\s+", ""));
							if (StringUtils.isNotEmpty(menuName)) {
								menu.setMenuName(menuName);
							}
						});
					}

				}
			}
		}

		return userDetailsBean;

	}

	private String setStudentPhotoUrl(Integer schoolId, UserDetailsBean userDetailsBean) {
		List<StudentImageBean> studentAttachmentList = icardMasterRepository
				.getStudentPhotographForPrint(userDetailsBean.getStudentId(), userDetailsBean.getRenewId(), schoolId);
		String imagePath = null;
		if (studentAttachmentList.size() > 0) {
			imagePath = utility.getS3PreSignedUrl(studentAttachmentList.get(0).getFilePath());
		}
		return imagePath;
	}

	private String setStaffPhotoUrl(Integer schoolId, UserDetailsBean userDetailsBean) {
		String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = "" + ConfigurationProperties.staffPhotoUrl + "/" + schoolKey;
		String extension = "jpg";
		return directoryPath + "/" + userDetailsBean.getRegistartionNo() + "." + extension;
	}

	private String setSansthaOfficerPhotoUrl(Integer sansthaId, UserDetailsBean userDetailsBean) {
		String schoolKey = sansthaRegistrationRepository.findBySansthaId(sansthaId).getSansthaKey();
		String directoryPath = "" + ConfigurationProperties.sansthaOfficerPhotoUrl + "/" + schoolKey;
		String extension = "jpg";
		return directoryPath + "/" + userDetailsBean.getRegistartionNo() + "." + extension;
	}

	private String setSuperOfficerPhotoUrl(Integer sansthaId, UserDetailsBean userDetailsBean) {
		String schoolKey = sansthaRegistrationRepository.findBySansthaId(sansthaId).getSansthaKey();
		String directoryPath = "" + ConfigurationProperties.superOfficerPhotoUrl + "/" + schoolKey;
		String extension = "jpg";
		return directoryPath + "/" + userDetailsBean.getRegistartionNo() + "." + extension;
	}

	private String getReverseRoles(String userName) {
		switch (userName) {
		case "Principal":
			return "ROLE_PRINCIPAL";
		case "Vice Principal":
			return "ROLE_VICEPRINCIPAL";
		case "Admin":
			return "ROLE_ADMIN";
		case "Non Teaching Staff":
			return "ROLE_NONTEACHINGSTAFF";
		case "Teacher":
			return "ROLE_TEACHINGSTAFF";
		case "Father":
			return "ROLE_PARENT1";
		case "Mother":
			return "ROLE_PARENT2";
		case "Sanstha Officer":
			return "ROLE_SANSTHAOFFICER";
		case "Student":
			return "ROLE_STUDENT";
		case "Super Admin":
			return "ROLE_SUPERADMIN";
		case "User":
			return "ROLE_USER";
		default:
			return userName;
		}
	}

	@Override
	public String uploadProfilePhoto(MultipartFile file, String fileName, Integer schoolId, String regNo,
			Integer studentId, String role) {
		String folderName = "";
		String url = "";
		String schoolKey = "";
		try {
			if (role.equals("Student") || role.equals("Father") || role.equals("Mother")) {
				schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
				String grbookNameId = studentMasterRepository.findGrName(studentId);
				folderName = File.separator + "IcardImages" + File.separator + schoolKey + File.separator
						+ grbookNameId;
				fileName = regNo.concat(".").concat("jpg");
			}
			if (role.equals("Teacher") || role.equals("Principal")) {
				schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
				folderName = "cms" + File.separator + "StaffProfileImage" + File.separator + schoolKey;
				fileName = regNo.concat(".").concat("jpg");
			}
			if (role.equals("Sanstha Officer")) {
				schoolKey = sansthaRegistrationRepository.findBySansthaId(schoolId).getSansthaKey();
				folderName = "cms" + File.separator + "SansthaProfileImage" + File.separator + schoolKey;
				fileName = regNo.concat(".").concat("jpg");
			}
			if (role.equals("Super Officer")) {
				schoolKey = sansthaRegistrationRepository.findBySansthaId(schoolId).getSansthaKey();
				folderName = "cms" + File.separator + "SuperOfficerProfileImage" + File.separator + schoolKey;
				fileName = regNo.concat(".").concat("jpg");
			}
			if (!file.isEmpty() && StringUtils.isNotEmpty(fileName)) {
				url = HttpClientUtil.uploadImage(file, fileName, folderName, schoolKey);
			}
			return url;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}

	int getCurrentYear(Integer schoolId) {
		int globleYearId = 0;
		try {

			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DynamicAcademicMonthsModel> dynamicAcadYearBean = dynamicAcademicMonthsRepository
					.findBySchoolMasterModel(schoolMasterModel);
			if (CollectionUtils.isNotEmpty(dynamicAcadYearBean)) {
				String localDynamicAcadSdate = dynamicAcadYearBean.get(0).getFromMonth();
				String globalDynamicAcadSdate = localDynamicAcadSdate.split("-")[1];

				LocalDateTime now = LocalDateTime.now();
				int year = now.getYear();
				int month = (now.getMonthValue());

				String currentAcYr = "";
				if (month >= Integer.parseInt(globalDynamicAcadSdate) && month <= 12) {
					year++;
					currentAcYr = now.getYear() + "-" + year;
				} else {
					year--;
					currentAcYr = year + "-" + now.getYear();
				}

				List<YearMasterModel> list = yearMasterRepository.findBySchoolMasterModelOrderByYear(schoolMasterModel);
				for (int i = 0; i < list.size(); i++) {
					if (Utillity.convertOtherToEnglish(list.get(i).getYear()).equals(currentAcYr)) {
						globleYearId = list.get(i).getYearId();
					}
				}
			}

		} catch (NumberFormatException e) {

			e.printStackTrace();
		}

		return globleYearId;
	}

	public String getProperty(String pPropertyKey) {
		return env.getProperty(pPropertyKey);
	}

	@Override
	public UserDetailsBean getUserDetails(Integer studentId, Integer schoolId) {
		UserDetailsBean userDetailsBean = new UserDetailsBean();
		userDetailsBean = studentMasterRepository.findStudentDetailsFromRenewId(studentId);
		if (userDetailsBean != null) {
			userDetailsBean.setStudentPhotoUrl(setStudentPhotoUrl(schoolId, userDetailsBean));
		}
		return userDetailsBean;
	}

	@Override
	public UserDetailsBean getStaffDetails(Integer staffId, Integer schoolId) {
		UserDetailsBean userDetailsBean = new UserDetailsBean();
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		userDetailsBean = staffBasicDetailsRepository.findStaffDetails(staffId);
		if (userDetailsBean != null) {
			userDetailsBean.setStudentPhotoUrl(setStaffPhotoUrl(schoolId, userDetailsBean));
		}
		return userDetailsBean;
	}

	@Override
	public List<StudentCountBean> getStudentStrengthForTeacher(Integer staffId, Integer schoolId, String profileRole,
			Integer yearId, String renewDate, String language) {
		List<StudentCountBean> studentStandardwiseList = new ArrayList<StudentCountBean>();
		studentStandardwiseList = studentMasterRepository.getStudentStrengthForTeacher(schoolId, yearId, renewDate,
				staffId);
		for (int i = 0; i < studentStandardwiseList.size(); i++) {
			StudentCountBean studentCountBean = studentStandardwiseList.get(i);
			studentCountBean.setStandardDivisionName(
					studentCountBean.getStandardName() + "-" + studentCountBean.getDivisionName());
		}
		return studentStandardwiseList;
	}

	@Override
	public List<StudentDetailsBean> getclassAndDivisionWiseStudentDetails(Integer standardId, Integer divisionId,
			Integer schoolId, String profileRole, Integer yearId, String renewDate) {
		List<StudentDetailsBean> studentList = new ArrayList<>();
		studentList = studentDetailsRepository.getDivisionwiseStudentDetails(schoolId, yearId, standardId, divisionId,
				renewDate);
		List<StudentDetailsBean> studentRecordwithPhotoList = new ArrayList<>();
		for (int i = 0; i < studentList.size(); i++) {
			StudentDetailsBean studentDetailsBean = studentList.get(i);
			studentDetailsBean = setStudentPhotoUrl(schoolId, studentDetailsBean);
			studentRecordwithPhotoList.add(studentDetailsBean);
		}
		return studentRecordwithPhotoList;
	}

	private StudentDetailsBean setStudentPhotoUrl(Integer schoolId, StudentDetailsBean userDetailsBean) {
		String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = "" + ConfigurationProperties.studentPhotoUrl + "" + schoolKey + "/"
				+ userDetailsBean.getStudentgrBookId();
		String extension = "jpg";
		userDetailsBean.setStudentPhoto(directoryPath + "/" + userDetailsBean.getStudentRegNo() + "." + extension);
		return userDetailsBean;
	}

	@Override
	public Integer setShowHideAddButtton(Integer staffId, Integer schoolId) {
		UserDetailsBean userDetailsBean = new UserDetailsBean();
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		userDetailsBean = staffBasicDetailsRepository.findStaffDetailsForFlag(staffId, currentDate);
		if (userDetailsBean != null) {
			return 1;
		}
		return 0;
	}

	@Override
	public Integer saveDeviceDetails(String userName, String deviceId, String IMEICode) {
		userRepository.updateAppUser(userName, IMEICode, deviceId);
		return 1;
	}

	@Override
	public UserDetailsBean getStudentDetails1(Integer userId, String deviceId, String iMEICode, String language,
			String mobNum, String deviceType, Integer isReact, Integer schoolId1, Integer priorityType, 
			Integer typeOfManagementId, Integer menuPolicy, Integer specificMenuCategory, Integer sansthaKey) {
		UserDetailsBean userDetailsBean = new UserDetailsBean();
		UserDetailsBean appUserModel = null;
//		if(mobNum!=null || StringUtils.isNotEmpty(mobNum)) {
//		String url = ConfigurationProperties.checkExistingUser+"="+mobNum+"&deviceType="+deviceType+"&IMEICode="+IMEICode;
//		ResponseBodyBean responseBodyBean = (ResponseBodyBean) RestUtil.makeRestCallForExistingUser(url);
//		if(responseBodyBean!=null) {
//		Integer isUserExit = (Integer) responseBodyBean.getResponseData();
//		if(isUserExit==1) {
//			appUserModel = userRepository.findByAppUserRoleId(userId);
//			}
//		else {
//			appUserModel=null;
//		}
//	}
//	}
		try {		
		if (schoolId1 != null) {
			appUserModel = userRepository.findByAppUserRoleId(userId, schoolId1);
		} else {
			appUserModel = userRepository.findByAppUserRoleId(userId);
		}

		if(menuPolicy == 0) {
		if (appUserModel != null) {
			String roleName = appUserModel.getRoleName();
			Integer schoolId = appUserModel.getSchoolId();
			Integer studentId = appUserModel.getStaffId();
			
//			List<AndroidMenuBean> groupList = androidMenuRepository.getGroups();
			List<AndroidMenuBean> groupList = androidMenuRepository.getGroups(priorityType);
			String isReactFlag;
			if (isReact == 1) {
				isReactFlag = "'1','2'";
			} else {
				isReactFlag = "'0','2'";
			}
			Date date = new Date();
			
			if (roleName.equalsIgnoreCase("ROLE_PARENT1") || roleName.equalsIgnoreCase("ROLE_PARENT2")
					|| roleName.equalsIgnoreCase("ROLE_STUDENT")) {
				String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
				userDetailsBean = studentMasterRepository.findStudentDetails(studentId, currentDate);
				userDetailsBean.setUnseenNotificationCount(fCMPushNotificationServiceImpl.getUnseenNotificationCount(userDetailsBean.getRenewId(),"1"));
				userDetailsBean.setMenuWiseUnseenNotificationList(androidMenuRepository.getMenuWiseUnseenNotificationCount(userDetailsBean.getRenewId(),"1"));
				userDetailsBean.setIsOrganization(appUserModel.getIsOrganization());
				if (userDetailsBean != null) {
					String photoString = setStudentPhotoUrl(schoolId, userDetailsBean);
					if (photoString != null) {
						userDetailsBean.setStudentPhotoUrl(photoString);
					} else {
						userDetailsBean.setStudentPhotoUrl("");
					}
					// Integer currentYear = getCurrentYear(schoolId);
					Integer currentYear = studentStandardRepository.getCurentYear(studentId);
					userDetailsBean.setCurrentYear(currentYear);
					userDetailsBean.setRollColor(" #FFC75A");
					YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
					if (yearMasterModel != null) {
						userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
					}
					List<AndroidMenuBean> groupWiseMenuList = new ArrayList<AndroidMenuBean>();
					if (userDetailsBean.getIsOnlineAdmission().equalsIgnoreCase("1")
							&& userDetailsBean.getOnlineAdmissionApprovalFlag().equalsIgnoreCase("0")) {
						AndroidMenuBean androidMenuBean = new AndroidMenuBean();
//				List<AndroidMenuBean> menuList = androidMenuRepository.findBySchoolMasterModel(schoolId,
//						getReverseRoles(roleName),1,isReactFlag);	
//				if(isReactFlag.equals("'0','2'")) {
						List<AndroidMenuBean> menuList = fCMPushNotificationServiceImpl
								.findBySchoolMasterModel(schoolId, getReverseRoles(roleName), 1, isReactFlag);

						String globalRole = "0";
						List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuList(globalRole,
								getReverseRoles(roleName), 1, isReactFlag);

						List<Integer> notApplicable = androidMenuNotForSchoolRepository
								.checkNotApplicableMenus(schoolId);

						for (AndroidMenuBean globalmenu : GlobalmenuList) {

							if (notApplicable.contains(globalmenu.getMenuId())) {
								continue;
							} else {
								menuList.add(globalmenu);
							}
						}

						androidMenuBean.setMenuList(menuList);
						androidMenuBean.setGroupdId(1);
						androidMenuBean.setGroupName("Administration");
						groupWiseMenuList.add(androidMenuBean);
//				}else if(isReactFlag.equals("'1','2'")) {
//					List<AndroidMenuBean> menuList =fCMPushNotificationServiceImpl.findBySchoolMasterModelForReact(schoolId,getReverseRoles(roleName),1,isReactFlag);
//					androidMenuBean.setMenuList(menuList);
//					androidMenuBean.setGroupdId(1);
//					androidMenuBean.setGroupName("Administration");
//					groupWiseMenuList.add(androidMenuBean);
//				}

					} else {
						if (CollectionUtils.isNotEmpty(groupList)) {
							groupList.stream().forEach(group -> {
								AndroidMenuBean androidMenuBean = new AndroidMenuBean();
								AndroidMenuBean GlobalandroidMenuBean = new AndroidMenuBean();
//				List<AndroidMenuBean> menuList = androidMenuRepository.findBySchoolMasterModel(schoolId,getReverseRoles(roleName),group.getGroupdId(),isReactFlag);	
								// if(isReactFlag.equals("'0','2'")) {
								List<AndroidMenuBean> menuList = fCMPushNotificationServiceImpl.findBySchoolMasterModel(
										schoolId, getReverseRoles(roleName), group.getGroupdId(), isReactFlag);

								String globalRole = "'0','2'";
								List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuList(
										globalRole, getReverseRoles(roleName), group.getGroupdId(), isReactFlag);

								List<Integer> notApplicable = androidMenuNotForSchoolRepository
										.checkNotApplicableMenus(schoolId);

								for (AndroidMenuBean globalmenu : GlobalmenuList) {

									if (notApplicable.contains(globalmenu.getMenuId())) {
										continue;
									} else {
										menuList.add(globalmenu);
									}
								}

								if (menuList.size() > 0) {
									androidMenuBean.setMenuList(menuList);
									androidMenuBean.setGroupdId(group.getGroupdId());
									androidMenuBean.setGroupName(group.getGroupName());
									groupWiseMenuList.add(androidMenuBean);
								}

//				}else if(isReactFlag.equals("'1','2'")) {
//					List<AndroidMenuBean> menuList =fCMPushNotificationServiceImpl.findBySchoolMasterModelForReact(schoolId,getReverseRoles(roleName),group.getGroupdId(),isReactFlag);	
//					if(menuList.size()>0) {
//						androidMenuBean.setMenuList(menuList);
//						androidMenuBean.setGroupdId(group.getGroupdId());
//						androidMenuBean.setGroupName(group.getGroupName());
//						groupWiseMenuList.add(androidMenuBean);
//						}
//				}

							});
						}
					}
					userDetailsBean.setAndroidMenuList(groupWiseMenuList);
				}
			}

			else if (roleName.equalsIgnoreCase("ROLE_TEACHINGSTAFF")
					|| roleName.equalsIgnoreCase("ROLE_NONTEACHINGSTAFF")
					|| roleName.equalsIgnoreCase("ROLE_PRINCIPAL")
					|| roleName.equalsIgnoreCase("ROLE_VICEPRINCIPAL")) {
				String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
				userDetailsBean = staffBasicDetailsRepository.findStaffDetails(studentId);
				
				
				userDetailsBean.setUnseenNotificationCount(fCMPushNotificationServiceImpl.getUnseenNotificationCount(studentId,"2"));
				
				userDetailsBean.setMenuWiseUnseenNotificationList(androidMenuRepository.getMenuWiseUnseenNotificationCount(userDetailsBean.getRenewId(),"2"));
				userDetailsBean.setIsOrganization(appUserModel.getIsOrganization());
				userDetailsBean.setRollColor("#8AD271");
				if (userDetailsBean != null) {
					userDetailsBean.setStudentPhotoUrl(setStaffPhotoUrl(schoolId, userDetailsBean));
					Integer currentYear = getCurrentYear(schoolId);
					userDetailsBean.setCurrentYear(currentYear);
					YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
					if (yearMasterModel != null) {
						userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
					}
//					if (!roleName.equalsIgnoreCase("ROLE_PRINCIPAL") || !roleName.equalsIgnoreCase("ROLE_VICEPRINCIPAL")) {
					if (!roleName.equalsIgnoreCase("ROLE_PRINCIPAL") && !roleName.equalsIgnoreCase("ROLE_VICEPRINCIPAL")) {
						
						List<AndroidMenuBean> groupWiseMenuList = new ArrayList<AndroidMenuBean>();
						if (CollectionUtils.isNotEmpty(groupList)) {
							groupList.stream().forEach(group -> {
								System.out.println(group.getGroupName() + "\n");
								AndroidMenuBean androidMenuBean = new AndroidMenuBean();
//					List<AndroidMenuBean> menuList = androidMenuRepository.findBySchoolMasterModel(schoolId,getReverseRoles(roleName),group.getGroupdId(),isReactFlag);	
								// if(isReactFlag.equals("'0','2'")) {
								List<AndroidMenuBean> menuList = fCMPushNotificationServiceImpl.findBySchoolMasterModel(
										schoolId, getReverseRoles(roleName), group.getGroupdId(), isReactFlag);
								
//								String globalRole = "'1','2'";
//								List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuList(
//										globalRole, getReverseRoles(roleName), group.getGroupdId(), isReactFlag);
								
//								new 1
								List<AndroidMenuBean> GlobalmenuList  = new ArrayList<AndroidMenuBean>();
								if (roleName.equalsIgnoreCase("ROLE_TEACHINGSTAFF")) {
																	
								String globalRole = "'1','2'";
//								List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuList(
								GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuList(
										globalRole, getReverseRoles(roleName), group.getGroupdId(), isReactFlag);
								}
//								else {
////									List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.PrincipalMenuList(
//									GlobalmenuList = fCMPushNotificationServiceImpl.PrincipalMenuList(
//											getReverseRoles(roleName), group.getGroupdId(), isReactFlag);
//								}
//								new 1
								List<Integer> notApplicable = androidMenuNotForSchoolRepository
										.checkNotApplicableMenus(schoolId);

								for (AndroidMenuBean globalmenu : GlobalmenuList) {

									if (notApplicable.contains(globalmenu.getMenuId())) {
										continue;
									} else {
										menuList.add(globalmenu);
									}
								}

								if (menuList.size() > 0) {
									androidMenuBean.setMenuList(menuList);
									androidMenuBean.setGroupdId(group.getGroupdId());
									androidMenuBean.setGroupName(group.getGroupName());
									groupWiseMenuList.add(androidMenuBean);

								}
//				}else if(isReactFlag.equals("'1','2'")) {
//					List<AndroidMenuBean> menuList =fCMPushNotificationServiceImpl.findBySchoolMasterModelForReact(schoolId,getReverseRoles(roleName),group.getGroupdId(),isReactFlag);
//					if(menuList.size()>0) {
//					androidMenuBean.setMenuList(menuList);
//					androidMenuBean.setGroupdId(group.getGroupdId());
//					androidMenuBean.setGroupName(group.getGroupName());
//					groupWiseMenuList.add(androidMenuBean);
//
//				}
//				}

							});
//							here
							AndroidMenuBean androidMenuBean = new AndroidMenuBean();
							
							List<AndroidMenuBean> menuList = fCMPushNotificationServiceImpl.findBySchoolMasterModel(
									schoolId, getReverseRoles(roleName), 27, isReactFlag);
							
							String globalRole = "'1','2'";
							List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuList1(
									globalRole, getReverseRoles(roleName), studentId, isReactFlag);
							
							for (AndroidMenuBean globalmenu : GlobalmenuList) {
									menuList.add(globalmenu);
							}

							if (menuList.size() > 0) {
								androidMenuBean.setMenuList(menuList);
								androidMenuBean.setGroupdId(27);
//								androidMenuBean.setGroupName(GlobalmenuList.get(0).getGroupName());
								androidMenuBean.setGroupName("Admin Modules");
								groupWiseMenuList.add(androidMenuBean);

							}
						}
						List<StandardDivisionBean> standardDivisionList = new ArrayList<>();
						List<StaffSubjectDetailsModel> staffSubjectDetailsList = staffSubjectDetailsRepository
								.findClassteacher(studentId);
						userDetailsBean.setStandardName("");
						userDetailsBean.setDivisionName("");
						if (CollectionUtils.isNotEmpty(staffSubjectDetailsList)) {
							userDetailsBean.setStandardName(
									staffSubjectDetailsList.get(0).getStandardMasterModel().getStandardName());
							userDetailsBean.setDivisionName(
									staffSubjectDetailsList.get(0).getDivisionMasterModel().getDivisionName());
							staffSubjectDetailsList.stream().forEach(StaffSubjectDetailsModel -> {
								StandardDivisionBean standardDivisionBean = new StandardDivisionBean();
								standardDivisionBean.setDivisionId(
										StaffSubjectDetailsModel.getDivisionMasterModel().getDivisionId());
								standardDivisionBean.setDivisionName(
										StaffSubjectDetailsModel.getDivisionMasterModel().getDivisionName());
								standardDivisionBean.setStandardId(
										StaffSubjectDetailsModel.getStandardMasterModel().getStandardId());
								standardDivisionBean.setStandardName(
										StaffSubjectDetailsModel.getStandardMasterModel().getStandardName());
								standardDivisionList.add(standardDivisionBean);
							});
						}
						userDetailsBean.setStandardDivisionList(standardDivisionList);
						userDetailsBean.setRollColor("#009FD5");
						if (CollectionUtils.isEmpty(staffSubjectDetailsList)
								&& CollectionUtils.isNotEmpty(groupWiseMenuList)) {
							for (int i = 0; i < groupWiseMenuList.size(); i++) {
								AndroidMenuBean androidMenuBean = groupWiseMenuList.get(i);
								List<AndroidMenuBean> menuList = androidMenuBean.getMenuList();
								List<AndroidMenuBean> updatedMenuList = new ArrayList<>();
								if (CollectionUtils.isEmpty(staffSubjectDetailsList)
										&& CollectionUtils.isNotEmpty(menuList)) {
									menuList.stream().forEach(menu -> {
										if (menu.getMenuId() != 3) {
											updatedMenuList.add(menu);
										}
									});
									androidMenuBean.setMenuList(updatedMenuList);
								} else {
									androidMenuBean.setMenuList(menuList);
								}
								groupWiseMenuList.set(i, androidMenuBean);
							}
						}
						userDetailsBean.setAndroidMenuList(groupWiseMenuList);

					}

				}
			} else if (roleName.equalsIgnoreCase("ROLE_SANSTHAOFFICER")
					|| roleName.equalsIgnoreCase("ROLE_SUPEROFFICER")) {
				userDetailsBean.setUnseenNotificationCount(fCMPushNotificationServiceImpl.getUnseenNotificationCount(studentId,"2"));
				userDetailsBean.setMenuWiseUnseenNotificationList(androidMenuRepository.getMenuWiseUnseenNotificationCount(userDetailsBean.getRenewId(),"2"));
				userDetailsBean.setIsOrganization(appUserModel.getIsOrganization());
				if (roleName.equalsIgnoreCase("ROLE_SANSTHAOFFICER")) {
					userDetailsBean = staffBasicDetailsRepository.findSansthaOfficerDetails(userId);
					userDetailsBean.setRollColor("#FF706F");
					if (userDetailsBean != null) {
						userDetailsBean.setStudentPhotoUrl(setSansthaOfficerPhotoUrl(schoolId, userDetailsBean));
					}
					Integer currentYear = getCurrentYear(schoolId);
					userDetailsBean.setCurrentYear(currentYear);
					YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
					if (yearMasterModel != null) {
						userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
					}
				}
				if (roleName.equalsIgnoreCase("ROLE_SUPEROFFICER")) {
					userDetailsBean = staffBasicDetailsRepository.findSuperOfficerDetails(userId);
					userDetailsBean.setRollColor("#FF706F");
					if (userDetailsBean != null) {
						userDetailsBean.setStudentPhotoUrl(setSuperOfficerPhotoUrl(schoolId, userDetailsBean));
					}
					Integer currentYear = getCurrentYear(schoolId);
					userDetailsBean.setCurrentYear(currentYear);
					YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
					if (yearMasterModel != null) {
						userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
					} else {
//						userDetailsBean.setCurrentYearText("2022-2023");
//						userDetailsBean.setCurrentYearText("2023-2024");
						userDetailsBean.setCurrentYearText("2024-2025");
					}

				}
			}

			if (userDetailsBean != null) {
				if (CollectionUtils.isNotEmpty(userDetailsBean.getAndroidMenuList())
						&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
					if (userDetailsBean != null && CollectionUtils.isNotEmpty(userDetailsBean.getAndroidMenuList())) {
						userDetailsBean.getAndroidMenuList().stream().forEach(menu -> {
							String groupName = env
									.getProperty(language + "." + menu.getGroupName().replaceAll("\\s+", ""));
							if (StringUtils.isNotEmpty(groupName)) {
								menu.setGroupName(groupName);
							}
							List<AndroidMenuBean> menuList5 = menu.getMenuList();
							menuList5.stream().forEach(menutype -> {
								String menuName = env
										.getProperty(language + "." + menutype.getMenuName().replaceAll("\\s+", ""));
								if (StringUtils.isNotEmpty(menuName)) {
									menutype.setMenuName(menuName);
								}
							});
						});
					}
				}
			}
		}
		}
		else if(menuPolicy != 0) {
			
			if (appUserModel != null) {
				String roleName = appUserModel.getRoleName();
				Integer schoolId = appUserModel.getSchoolId();
				Integer studentId = appUserModel.getStaffId();
				
//				List<AndroidMenuBean> groupList = androidMenuRepository.getGroups();
				List<AndroidMenuBean> groupList = androidMenuRepository.getGroups(priorityType);
				String isReactFlag;
				if (isReact == 1) {
					isReactFlag = "'1','2'";
				} else {
					isReactFlag = "'0','2'";
				}
				Date date = new Date();
				
				 List<Integer> categoryMasterMenuIdsList = fCMPushNotificationServiceImpl.getMenuMasterCategory(typeOfManagementId, menuPolicy, specificMenuCategory, sansthaKey);
				 Integer categoryMasterMenuId = categoryMasterMenuIdsList.get(0);				
				
				if (roleName.equalsIgnoreCase("ROLE_PARENT1") || roleName.equalsIgnoreCase("ROLE_PARENT2")
						|| roleName.equalsIgnoreCase("ROLE_STUDENT")) {
					
					String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
					userDetailsBean = studentMasterRepository.findStudentDetails(studentId, currentDate);
					userDetailsBean.setUnseenNotificationCount(fCMPushNotificationServiceImpl.getUnseenNotificationCount(userDetailsBean.getRenewId(),"1"));
					userDetailsBean.setMenuWiseUnseenNotificationList(androidMenuRepository.getMenuWiseUnseenNotificationCount(userDetailsBean.getRenewId(),"1"));
					userDetailsBean.setIsOrganization(appUserModel.getIsOrganization());
					
					if (userDetailsBean != null) {
						
						String photoString = setStudentPhotoUrl(schoolId, userDetailsBean);
						
						if (photoString != null) {
							userDetailsBean.setStudentPhotoUrl(photoString);
						} else {
							userDetailsBean.setStudentPhotoUrl("");
						}
						
						// Integer currentYear = getCurrentYear(schoolId);
						Integer currentYear = studentStandardRepository.getCurentYear(studentId);
						userDetailsBean.setCurrentYear(currentYear);
						userDetailsBean.setRollColor(" #FFC75A");
						YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
						if (yearMasterModel != null) {
							userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
						}
						List<AndroidMenuBean> groupWiseMenuList = new ArrayList<AndroidMenuBean>();
						
						if (userDetailsBean.getIsOnlineAdmission().equalsIgnoreCase("1")
								&& userDetailsBean.getOnlineAdmissionApprovalFlag().equalsIgnoreCase("0")) {
							AndroidMenuBean androidMenuBean = new AndroidMenuBean();

							List<AndroidMenuBean> menuList = fCMPushNotificationServiceImpl
									.findBySchoolMasterModel(schoolId, getReverseRoles(roleName), 1, isReactFlag);

							String globalRole = "0";
							List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuListCategory(globalRole,
									getReverseRoles(roleName), categoryMasterMenuId);

//							here
							Map<Integer, List<AndroidMenuBean>> groupedMenus = GlobalmenuList.stream()
					                .collect(Collectors.groupingBy(AndroidMenuBean::getGroupdId));
							
							List<AndroidMenuBean> groupedResponse = groupedMenus.entrySet().stream().map(entry -> {

					            List<AndroidMenuBean> menus = entry.getValue();

					            AndroidMenuBean groupBean = new AndroidMenuBean();
					            groupBean.setGroupdId(entry.getKey());
					            groupBean.setGroupName(menus.get(0).getGroupName()); 
					            groupBean.setMenuList(menus); 

					            return groupBean;
					        }).collect(Collectors.toList());
							
							userDetailsBean.setAndroidMenuList(groupedResponse);
//					        return groupedResponse;
//							List<Integer> notApplicable = androidMenuNotForSchoolRepository
//									.checkNotApplicableMenus(schoolId);

//							for (AndroidMenuBean globalmenu : GlobalmenuList) {
//
//								if (notApplicable.contains(globalmenu.getMenuId())) {
//									continue;
//								} else {
//									menuList.add(globalmenu);
//								}
//							}

							androidMenuBean.setMenuList(menuList);
							androidMenuBean.setGroupdId(1);
							androidMenuBean.setGroupName("Administration");
							groupWiseMenuList.add(androidMenuBean);
//					}else if(isReactFlag.equals("'1','2'")) {
//						List<AndroidMenuBean> menuList =fCMPushNotificationServiceImpl.findBySchoolMasterModelForReact(schoolId,getReverseRoles(roleName),1,isReactFlag);
//						androidMenuBean.setMenuList(menuList);
//						androidMenuBean.setGroupdId(1);
//						androidMenuBean.setGroupName("Administration");
//						groupWiseMenuList.add(androidMenuBean);
//					}

						} 
//						else {
//							if (CollectionUtils.isNotEmpty(groupList)) {
//								groupList.stream().forEach(group -> {
//									AndroidMenuBean androidMenuBean = new AndroidMenuBean();
//									AndroidMenuBean GlobalandroidMenuBean = new AndroidMenuBean();
////					List<AndroidMenuBean> menuList = androidMenuRepository.findBySchoolMasterModel(schoolId,getReverseRoles(roleName),group.getGroupdId(),isReactFlag);	
//									// if(isReactFlag.equals("'0','2'")) {
//									List<AndroidMenuBean> menuList = fCMPushNotificationServiceImpl.findBySchoolMasterModel(
//											schoolId, getReverseRoles(roleName), group.getGroupdId(), isReactFlag);
//
//									String globalRole = "'0','2'";
//									List<AndroidMenuBean> GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuList(
//											globalRole, getReverseRoles(roleName), group.getGroupdId(), isReactFlag);
//
//									List<Integer> notApplicable = androidMenuNotForSchoolRepository
//											.checkNotApplicableMenus(schoolId);
//
//									for (AndroidMenuBean globalmenu : GlobalmenuList) {
//
//										if (notApplicable.contains(globalmenu.getMenuId())) {
//											continue;
//										} else {
//											menuList.add(globalmenu);
//										}
//									}
//
//									if (menuList.size() > 0) {
//										androidMenuBean.setMenuList(menuList);
//										androidMenuBean.setGroupdId(group.getGroupdId());
//										androidMenuBean.setGroupName(group.getGroupName());
//										groupWiseMenuList.add(androidMenuBean);
//									}
//
////					}else if(isReactFlag.equals("'1','2'")) {
////						List<AndroidMenuBean> menuList =fCMPushNotificationServiceImpl.findBySchoolMasterModelForReact(schoolId,getReverseRoles(roleName),group.getGroupdId(),isReactFlag);	
////						if(menuList.size()>0) {
////							androidMenuBean.setMenuList(menuList);
////							androidMenuBean.setGroupdId(group.getGroupdId());
////							androidMenuBean.setGroupName(group.getGroupName());
////							groupWiseMenuList.add(androidMenuBean);
////							}
////					}
//
//								});
//							}
//						}
//						userDetailsBean.setAndroidMenuList(groupWiseMenuList);
					}
				}

				else if (roleName.equalsIgnoreCase("ROLE_TEACHINGSTAFF")
						|| roleName.equalsIgnoreCase("ROLE_NONTEACHINGSTAFF")
						|| roleName.equalsIgnoreCase("ROLE_PRINCIPAL")
						|| roleName.equalsIgnoreCase("ROLE_VICEPRINCIPAL")) {
					
					String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);					
					userDetailsBean = staffBasicDetailsRepository.findStaffDetails(studentId);
					
					
					userDetailsBean.setUnseenNotificationCount(fCMPushNotificationServiceImpl.getUnseenNotificationCount(studentId,"2"));
					
					userDetailsBean.setMenuWiseUnseenNotificationList(androidMenuRepository.getMenuWiseUnseenNotificationCount(userDetailsBean.getRenewId(),"2"));
					userDetailsBean.setIsOrganization(appUserModel.getIsOrganization());
					userDetailsBean.setRollColor("#8AD271");
					
					if (userDetailsBean != null) {
						userDetailsBean.setStudentPhotoUrl(setStaffPhotoUrl(schoolId, userDetailsBean));
						Integer currentYear = getCurrentYear(schoolId);
						userDetailsBean.setCurrentYear(currentYear);
						YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
						if (yearMasterModel != null) {
							userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
						}

						if (!roleName.equalsIgnoreCase("ROLE_PRINCIPAL") && !roleName.equalsIgnoreCase("ROLE_VICEPRINCIPAL")) {
							
//							List<AndroidMenuBean> groupWiseMenuList = new ArrayList<AndroidMenuBean>();
//							if (CollectionUtils.isNotEmpty(groupList)) {
//									AndroidMenuBean androidMenuBean = new AndroidMenuBean();
//																		
//									List<AndroidMenuBean> GlobalmenuList  = new ArrayList<AndroidMenuBean>();
//									List<AndroidMenuBean> groupedResponse = null;
//									if (roleName.equalsIgnoreCase("ROLE_TEACHINGSTAFF")) {
//																		
//									String globalRole = "'1','2'";
//									
//									GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuListCategory(
//											globalRole, getReverseRoles(roleName), categoryMasterMenuId);
//
//									Map<Integer, List<AndroidMenuBean>> groupedMenus = GlobalmenuList.stream()
//							                .collect(Collectors.groupingBy(AndroidMenuBean::getGroupdId));
//									
//									groupedResponse = groupedMenus.entrySet().stream().map(entry -> {
//
//							            List<AndroidMenuBean> menus = entry.getValue();
//
//							            AndroidMenuBean groupBean = new AndroidMenuBean();
//							            groupBean.setGroupdId(entry.getKey());
//							            groupBean.setGroupName(menus.get(0).getGroupName()); 
//							            groupBean.setMenuList(menus); 
//
//							            return groupBean;
//							        }).collect(Collectors.toList());
//									
//									androidMenuBean.setMenuList(groupedResponse);
//									}									
//						groupWiseMenuList.add(androidMenuBean);
//							}

							List<AndroidMenuBean> groupWiseMenuList = new ArrayList<>();

							if (CollectionUtils.isNotEmpty(groupList)) {
							    List<AndroidMenuBean> GlobalmenuList = new ArrayList<>();
							    if (roleName.equalsIgnoreCase("ROLE_TEACHINGSTAFF")) {
							        String globalRole = "'1','2'";

							        GlobalmenuList = fCMPushNotificationServiceImpl.GlobalMenuListCategory(
							                globalRole, getReverseRoles(roleName), categoryMasterMenuId);

							        Map<Integer, List<AndroidMenuBean>> groupedMenus = GlobalmenuList.stream()
							                .collect(Collectors.groupingBy(AndroidMenuBean::getGroupdId));

							        List<AndroidMenuBean> groupedResponse = groupedMenus.entrySet().stream()
							                .map(entry -> {
							                    List<AndroidMenuBean> menus = entry.getValue();

							                    AndroidMenuBean groupBean = new AndroidMenuBean();
							                    groupBean.setGroupdId(entry.getKey());
							                    groupBean.setGroupName(menus.get(0).getGroupName());
							                    groupBean.setMenuList(menus);

							                    return groupBean;
							                })
							                .collect(Collectors.toList());

							        groupWiseMenuList.addAll(groupedResponse);
							    }
							}

							
							List<StandardDivisionBean> standardDivisionList = new ArrayList<>();
							
							List<StaffSubjectDetailsModel> staffSubjectDetailsList = staffSubjectDetailsRepository.findClassteacher(studentId);
							userDetailsBean.setStandardName("");
							userDetailsBean.setDivisionName("");
							
							if (CollectionUtils.isNotEmpty(staffSubjectDetailsList)) {
								userDetailsBean.setStandardName(
										staffSubjectDetailsList.get(0).getStandardMasterModel().getStandardName());
								userDetailsBean.setDivisionName(
										staffSubjectDetailsList.get(0).getDivisionMasterModel().getDivisionName());
								staffSubjectDetailsList.stream().forEach(StaffSubjectDetailsModel -> {
									StandardDivisionBean standardDivisionBean = new StandardDivisionBean();
									standardDivisionBean.setDivisionId(
											StaffSubjectDetailsModel.getDivisionMasterModel().getDivisionId());
									standardDivisionBean.setDivisionName(
											StaffSubjectDetailsModel.getDivisionMasterModel().getDivisionName());
									standardDivisionBean.setStandardId(
											StaffSubjectDetailsModel.getStandardMasterModel().getStandardId());
									standardDivisionBean.setStandardName(
											StaffSubjectDetailsModel.getStandardMasterModel().getStandardName());
									standardDivisionList.add(standardDivisionBean);
								});
							}
							userDetailsBean.setStandardDivisionList(standardDivisionList);
							userDetailsBean.setRollColor("#009FD5");
//							if (CollectionUtils.isEmpty(staffSubjectDetailsList)
//									&& CollectionUtils.isNotEmpty(groupWiseMenuList)) {
//								for (int i = 0; i < groupWiseMenuList.size(); i++) {
//									AndroidMenuBean androidMenuBean = groupWiseMenuList.get(i);
//									List<AndroidMenuBean> menuList = androidMenuBean.getMenuList();
//									List<AndroidMenuBean> updatedMenuList = new ArrayList<>();
//									if (CollectionUtils.isEmpty(staffSubjectDetailsList)
//											&& CollectionUtils.isNotEmpty(menuList)) {
//										menuList.stream().forEach(menu -> {
//											if (menu.getMenuId() != 3) {
//												updatedMenuList.add(menu);
//											}
//										});
//										androidMenuBean.setMenuList(updatedMenuList);
//									} else {
//										androidMenuBean.setMenuList(menuList);
//									}
//									groupWiseMenuList.set(i, androidMenuBean);
//								}
//							}
							userDetailsBean.setAndroidMenuList(groupWiseMenuList);

						}

					}
				} else if (roleName.equalsIgnoreCase("ROLE_SANSTHAOFFICER")
						|| roleName.equalsIgnoreCase("ROLE_SUPEROFFICER")) {
					userDetailsBean.setUnseenNotificationCount(fCMPushNotificationServiceImpl.getUnseenNotificationCount(studentId,"2"));
					userDetailsBean.setMenuWiseUnseenNotificationList(androidMenuRepository.getMenuWiseUnseenNotificationCount(userDetailsBean.getRenewId(),"2"));
					userDetailsBean.setIsOrganization(appUserModel.getIsOrganization());
					if (roleName.equalsIgnoreCase("ROLE_SANSTHAOFFICER")) {
						userDetailsBean = staffBasicDetailsRepository.findSansthaOfficerDetails(userId);
						userDetailsBean.setRollColor("#FF706F");
						if (userDetailsBean != null) {
							userDetailsBean.setStudentPhotoUrl(setSansthaOfficerPhotoUrl(schoolId, userDetailsBean));
						}
						Integer currentYear = getCurrentYear(schoolId);
						userDetailsBean.setCurrentYear(currentYear);
						YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
						if (yearMasterModel != null) {
							userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
						}
					}
					if (roleName.equalsIgnoreCase("ROLE_SUPEROFFICER")) {
						userDetailsBean = staffBasicDetailsRepository.findSuperOfficerDetails(userId);
						userDetailsBean.setRollColor("#FF706F");
						if (userDetailsBean != null) {
							userDetailsBean.setStudentPhotoUrl(setSuperOfficerPhotoUrl(schoolId, userDetailsBean));
						}
						Integer currentYear = getCurrentYear(schoolId);
						userDetailsBean.setCurrentYear(currentYear);
						YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
						if (yearMasterModel != null) {
							userDetailsBean.setCurrentYearText(yearMasterModel.getYear());
						} else {
//							userDetailsBean.setCurrentYearText("2022-2023");
//							userDetailsBean.setCurrentYearText("2023-2024");
							userDetailsBean.setCurrentYearText("2024-2025");
						}

					}
				}

				if (userDetailsBean != null) {
					if (CollectionUtils.isNotEmpty(userDetailsBean.getAndroidMenuList())
							&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
						if (userDetailsBean != null && CollectionUtils.isNotEmpty(userDetailsBean.getAndroidMenuList())) {
							userDetailsBean.getAndroidMenuList().stream().forEach(menu -> {
								String groupName = env
										.getProperty(language + "." + menu.getGroupName().replaceAll("\\s+", ""));
								if (StringUtils.isNotEmpty(groupName)) {
									menu.setGroupName(groupName);
								}
								List<AndroidMenuBean> menuList5 = menu.getMenuList();
								menuList5.stream().forEach(menutype -> {
									String menuName = env
											.getProperty(language + "." + menutype.getMenuName().replaceAll("\\s+", ""));
									if (StringUtils.isNotEmpty(menuName)) {
										menutype.setMenuName(menuName);
									}
								});
							});
						}
					}
				}
			}
			
		}
		if (appUserModel.getUserActiveOrInActiveStatus().equals("0")) {
			userDetailsBean.setUserActiveInActiceStatusName("Active");
			userDetailsBean.setUserActiveOrInActiveStatus("0");
		} else if (appUserModel.getUserActiveOrInActiveStatus().equals("1")) {
			userDetailsBean.setUserActiveInActiceStatusName("InActive");
			userDetailsBean.setUserActiveOrInActiveStatus("0");
		}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetailsBean;

	}

	@Override
	public String getCurrentApkVersion() {
		return userRepository.getCurrentApkVersion();
	}

	@Override
	public String getCurrentApkVersionForCalendar() {
		return userRepository.getCurrentApkVersionForCalendar();
	}

	@Override
	public List<AppVersionForReactBean> getCurrentApkVersionDetailsForReact(Integer schoolId, Integer userId,
			String isReact,String appType) {
		String react = isReact;

		System.out.println("react----------------" + react);
		if (react == null || react.equals("0")) {
			react = "0";
		} else {
			react = "1";
		}
		List<AppVersionForReactBean> appVersionForReactBean = appVersionMasterRepository.getRecentVersion(react,appType);

		if (appVersionForReactBean.get(0).getForSchool().equals("1")) {

			Integer schoolid = appVersionToSchoolRepository.findSchool(appVersionForReactBean.get(0).getVersionID(),
					schoolId);
			if (schoolid == null) {
				appVersionForReactBean.get(0).setNoaction("0");
				appVersionForReactBean.get(0).setIsmandatory("0");
			} else if (appVersionForReactBean.get(0).getForRole().equals("1")) {
				String userRole = appUserRoleRepository.getUserRole(userId);
				String roleName = appVersionToRoleRepository
						.findUserPresent(appVersionForReactBean.get(0).getVersionID(), userRole);
				if (userRole.equals(roleName)) {
					appVersionForReactBean.get(0).setNoaction("1");
					if (appVersionForReactBean.get(0).getIsMandatoryToSchool().equals("1")
							|| appVersionForReactBean.get(0).getIsMandatoryToRole().equals("1"))
						appVersionForReactBean.get(0).setIsmandatory("1");
					else
						appVersionForReactBean.get(0).setIsmandatory("0");
				} else {
					appVersionForReactBean.get(0).setNoaction("0");

					if (appVersionForReactBean.get(0).getIsMandatoryToSchool().equals("1")
							|| appVersionForReactBean.get(0).getIsMandatoryToRole().equals("1"))
						appVersionForReactBean.get(0).setIsmandatory("1");
					else
						appVersionForReactBean.get(0).setIsmandatory("0");
				}

			} else if (appVersionForReactBean.get(0).getForRole().equals("0")) {
				appVersionForReactBean.get(0).setNoaction("1");

				if (appVersionForReactBean.get(0).getIsMandatoryToSchool().equals("1")
						|| appVersionForReactBean.get(0).getIsMandatoryToRole().equals("1"))
					appVersionForReactBean.get(0).setIsmandatory("1");
				else
					appVersionForReactBean.get(0).setIsmandatory("0");
			}
		} else if (appVersionForReactBean.get(0).getForSchool().equals("0")) {

			if (appVersionForReactBean.get(0).getForRole().equals("1")) {
				String userRole = appUserRoleRepository.getUserRole(userId);
				String roleName = appVersionToRoleRepository
						.findUserPresent(appVersionForReactBean.get(0).getVersionID(), userRole);
				if (userRole.equals(roleName)) {
					appVersionForReactBean.get(0).setNoaction("1");
					if (appVersionForReactBean.get(0).getIsMandatoryToSchool().equals("1")
							|| appVersionForReactBean.get(0).getIsMandatoryToRole().equals("1"))
						appVersionForReactBean.get(0).setIsmandatory("1");
					else
						appVersionForReactBean.get(0).setIsmandatory("0");

				} else {
					appVersionForReactBean.get(0).setNoaction("0");
					appVersionForReactBean.get(0).setIsmandatory("0");
				}
			} else {

				if (appVersionForReactBean.get(0).getIsMandatoryToSchool().equals("1")
						|| appVersionForReactBean.get(0).getIsMandatoryToRole().equals("1")) {
					appVersionForReactBean.get(0).setIsmandatory("1");
					appVersionForReactBean.get(0).setNoaction("1");
				} else {
					appVersionForReactBean.get(0).setIsmandatory("0");
					appVersionForReactBean.get(0).setNoaction("1");
				}
			}
		}

		return appVersionForReactBean;
	}

}
