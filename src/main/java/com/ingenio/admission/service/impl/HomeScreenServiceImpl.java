package com.ingenio.admission.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.ingenio.admission.bean.AnnouncementResponseBean;
import com.ingenio.admission.bean.FeeBean;
import com.ingenio.admission.bean.HomeScreenBean;
import com.ingenio.admission.bean.SchoolDetailBean;
import com.ingenio.admission.bean.StudentCountBean;
import com.ingenio.admission.config.ConfigurationProperties;
import com.ingenio.admission.model.StaffSubjectDetailsModel;
import com.ingenio.admission.repository.AnnouncementRepository;
import com.ingenio.admission.repository.AttendanceRepository;
import com.ingenio.admission.repository.HomeScreenMasterRepository;
import com.ingenio.admission.repository.SansthaLogoRepository;
import com.ingenio.admission.repository.SchoolLogoRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.repository.StaffSubjectDetailsRepository;
import com.ingenio.admission.repository.StudentMasterRepository;
import com.ingenio.admission.service.HomeScreenService;
import com.ingenio.admission.util.CryptographyUtil;
import com.ingenio.admission.util.RestUtil;


@Component
//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")


//  @PropertySource("classpath:messages_en.properties")
//  
//  @PropertySource("classpath:messages_mr.properties")
//  
//  @PropertySource("classpath:messages_hn.properties")
// 

 
public class HomeScreenServiceImpl implements HomeScreenService {
	
	@Autowired
	HomeScreenMasterRepository homeScreenRepository;
	
	@Autowired
	AnnouncementRepository announcementRepository;
	
	@Autowired
	AttendanceRepository attendanceRepository;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	StudentMasterRepository studentMasterRepository;
	
	@Autowired 
	StaffSubjectDetailsRepository staffSubjectDetailsRepository;
	
    @Autowired
    private Environment env;
    
    @Autowired
    SansthaLogoRepository sansthaLogoRepository;
    
    @Autowired
    SchoolLogoRepository schoolLogoRepository;
    
    CryptographyUtil cry= new CryptographyUtil();
	

	@Override
	public List<HomeScreenBean> getHomeScreenDetails(Integer staffStudentId, String profileRole,Integer yearId,Integer schoolId,String language) {
		List<HomeScreenBean> updatedHomeScreenList = new ArrayList<>();
		List<HomeScreenBean> homeScreenList = homeScreenRepository.findByRole(profileRole,schoolId);
		homeScreenList.stream().forEach( homeScreenBean -> {
			if(homeScreenBean.getType().equals("SchoolDetails")) {	
				try {
				List<SchoolDetailBean>schoolDetailBeanList= homeScreenRepository.findSchoolDetails(schoolId);
			if(CollectionUtils.isNotEmpty(schoolDetailBeanList)) {
				SchoolDetailBean schoolDetailBean=schoolDetailBeanList.get(0);
				homeScreenBean.setTitle(cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(schoolDetailBean.getSchoolName().replaceAll("[\\r\\n]+", ""))));
				homeScreenBean.setType("School");
				homeScreenBean.setDate(""+new Date());
				homeScreenBean.setMessage1(StringUtils.EMPTY);
				homeScreenBean.setDescription(cry.decrypt("qwertyschooladdress", StringEscapeUtils.escapeJava(schoolDetailBean.getSchoolAddress().replaceAll("[\\r\\n]+", ""))));	
				List<String> logoList = new ArrayList<>();
				logoList.add(ConfigurationProperties.schoolLogo+schoolDetailBean.getSchoolKey()+"/"+schoolId+".jpg");
				
//				logoList.add(schoolLogoRepository.getSchoolLogoPath(schoolId));
				
				homeScreenBean.setIconUrlList(logoList);
				homeScreenBean.setPostedBy("Principal : "+schoolDetailBean.getPrincipalName());	
				homeScreenBean.setPostedbyRole(getRoles("ROLE_PRINCIPAL"));
				homeScreenBean.setPostedByImgUrl(setStaffPhotoUrl(schoolId,schoolDetailBean.getRegNo()));
				updatedHomeScreenList.add(homeScreenBean);
				}
			}catch (Exception e) {
					homeScreenBean.setTitle("School");				
				}
				
			}
			if(homeScreenBean.getType().equals("Announcement")) {
				List<AnnouncementResponseBean> announcementList = announcementRepository.getStudentAnnouncement(staffStudentId,profileRole,yearId);
				if(CollectionUtils.isNotEmpty(announcementList)) {
					AnnouncementResponseBean announcement = announcementList.get(0);
					homeScreenBean.setDescription(announcement.getAnnouncement());
					homeScreenBean.setTitle(announcement.getAnnouncementTitle());
					homeScreenBean.setDate(""+announcement.getStartDate());
					homeScreenBean.setPostedBy(announcement.getAnnouncedBy());
					homeScreenBean.setPostedbyRole(getRoles(announcement.getAnnouncedByRole()));
					homeScreenBean.setMessage1(StringUtils.EMPTY);
					String announcedByUrl = setStaffPhotoUrl(announcement.getSchoolId(),announcement.getRegistrationNo());
					homeScreenBean.setPostedByImgUrl(announcedByUrl );
					List<String> attachmentList = announcementRepository.getAttachments(announcement.getAnnouncementId());
					homeScreenBean.setIconUrlList(attachmentList);
					updatedHomeScreenList.add(homeScreenBean);
				}
			}
			if(homeScreenBean.getType().equals("Attendance")) {
				homeScreenBean.setCompletedCount(attendanceRepository.getStudentPresentCount(""+staffStudentId, yearId));
				homeScreenBean.setRemainingCount(attendanceRepository.getStudentAbsentCount(""+staffStudentId, yearId));
				homeScreenBean.setMessage1(StringUtils.EMPTY);
				updatedHomeScreenList.add(homeScreenBean);
			}
			
			if(homeScreenBean.getType().equals("Fees")) {
				String url = ConfigurationProperties.feeRestUrl+"="+staffStudentId;
				FeeBean feeOverViewBean = (FeeBean) RestUtil.makeRestCall(url);
				homeScreenBean.setCompletedCount("0");
				homeScreenBean.setRemainingCount("0");
				if(feeOverViewBean != null) {
					homeScreenBean.setCompletedCount(""+feeOverViewBean.getPaidFee());
					homeScreenBean.setRemainingCount(feeOverViewBean.getRemainingFee());
				}
				homeScreenBean.setMessage1(StringUtils.EMPTY);
				updatedHomeScreenList.add(homeScreenBean);
			}	

			if(homeScreenBean.getType().equals("Strength")) {				
			List<StaffSubjectDetailsModel> staffSubjectDetailsList = staffSubjectDetailsRepository.findClassteacher(staffStudentId);		
			if(CollectionUtils.isNotEmpty(staffSubjectDetailsList)) {
			for(int i=0;i<staffSubjectDetailsList.size();i++) {
				Integer divisionId=staffSubjectDetailsList.get(i).getDivisionMasterModel().getDivisionId();
				Integer standardId=staffSubjectDetailsList.get(i).getStandardMasterModel().getStandardId();
				StudentCountBean studentCountBean = new StudentCountBean();
				studentCountBean =  homeScreenRepository.getDivisionStrengthwithId(schoolId, yearId, standardId,divisionId);				
				if(studentCountBean!=null) {
				homeScreenBean.setFlag("1");
				homeScreenBean.setType("Strength");				
				homeScreenBean.setTitle(""+studentCountBean.getStandardName()+" "+studentCountBean.getDivisionName());
				homeScreenBean.setCompletedCount(""+studentCountBean.getGirlsCount());
				homeScreenBean.setRemainingCount(""+studentCountBean.getBoysCount());	
				homeScreenBean.setMessage("Total");
				homeScreenBean.setMessage1(""+studentCountBean.getTotalCount());				
				updatedHomeScreenList.add(homeScreenBean);
				}
			}
		}
	}
		});
		
		if(CollectionUtils.isNotEmpty(updatedHomeScreenList) && (language.equalsIgnoreCase("En") || language.equalsIgnoreCase(" "))) {
			updatedHomeScreenList.stream().forEach( menu -> { 
				menu.setMessage(menu.getMessage()+" "+menu.getMessage1());
				});
			}
		
		if(CollectionUtils.isNotEmpty(updatedHomeScreenList) && (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			updatedHomeScreenList.stream().forEach( menu -> { 
					String title = env.getProperty(language+"."+menu.getTitle().replaceAll("\\s+",""));
					if(StringUtils.isNotEmpty(title)) {
						menu.setTitle(title);
					}
					String completedTitle = env.getProperty(language+"."+menu.getCompletedTitle().replaceAll("\\s+",""));
					if(StringUtils.isNotEmpty(completedTitle)) {
						menu.setCompletedTitle(completedTitle);
					}
					String remainingTitle = env.getProperty(language+"."+menu.getRemainingTitle().replaceAll("\\s+",""));
					if(StringUtils.isNotEmpty(remainingTitle)) {
						menu.setRemainingTitle(remainingTitle);
					}
					
					if(StringUtils.isNotEmpty(menu.getMessage())) {
						String message = env.getProperty(language+"."+menu.getMessage().replaceAll("\\s+",""));
						if(StringUtils.isNotEmpty(message)) {
							menu.setMessage(message);
						}
					}
					menu.setMessage(menu.getMessage()+" "+menu.getMessage1());
			});
		}
		
		
		return updatedHomeScreenList;
	}

	private String setStaffPhotoUrl(Integer schoolId,String registartionNo) {
		String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		String directoryPath = ""+ConfigurationProperties.staffPhotoUrl+"/"+schoolKey;
		String extension="jpg";
		return directoryPath+"/"+registartionNo+"."+extension;
	}
	
	private String getRoles(String userName) {
		switch (userName) {
		case "ROLE_PRINCIPAL":
			return "Principal";
		case "ROLE_ADMIN":
			return "Admin";
		case "ROLE_NONTEACHINGSTAFF":
			return "Non Teaching Staff";
		case "ROLE_TEACHINGSTAFF":
			return "Teacher";
		case "ROLE_PARENT1":
			return "Father";
		case "ROLE_PARENT2":
			return "Mother";
		case "ROLE_SANSTHAOFFICER":
			return "Sanstha Officer";
		case "ROLE_STUDENT":
			return "Student";
		case "ROLE_SUPERADMIN":
			return "Super Admin";
		case "ROLE_USER":
			return "User";
		default:
			return userName;
		}

	}
	
}
