package com.ingenio.admission.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ingenio.admission.bean.FCMUpdateBean;
import com.ingenio.admission.bean.FCMUpdateInnerBean;
import com.ingenio.admission.bean.NotificationBean;
import com.ingenio.admission.bean.StandardDivisionBean;
import com.ingenio.admission.model.AndroidFCMTokenMaster;
import com.ingenio.admission.model.AppUserModel;
import com.ingenio.admission.model.AppUserRoleModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.repository.FCMPushNotificationRepository;
import com.ingenio.admission.repository.impl.FCMPushNotificationRepositoryImpl;
import com.ingenio.admission.service.FCMPushNotificationService;

@Component
public class FCMPushNotificationServiceImpl implements FCMPushNotificationService {

	@Autowired
	FCMPushNotificationRepository fcmPushNotificationRepository;

	@Autowired
	FCMPushNotificationRepositoryImpl fCMPushNotificationRepositoryImpl;

	@SuppressWarnings("unchecked")
	public String pushNotification(List<String> deviceTokenList, String title, String message1) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization",
				"key=AAAAwvdf7Qo:APA91bGSz4zXr2-CfrxPpPZtQLxyf1qU5vjLn9f_pCZex5_sD9q7xJjow8VdWK_O64TGyJ_8gmzK6Il8rPq0lv_GdENCng2acls-6fojq1BUlbI3EcIADU_9Rwfgl9BI0khV1uA8rcdA");
		JSONObject message = new JSONObject();

		message.put("registration_ids", deviceTokenList);
		message.put("priority", "high");
		/*
		 * JSONObject notification = new JSONObject(); notification.put("title", title);
		 * notification.put("body", message1); message.put("notification",
		 * notification);
		 */

		JSONObject data = new JSONObject();
		data.put("title", title);
		data.put("body", message1);
		message.put("data", data);
		post.setEntity(new StringEntity(message.toString(), "UTF-8"));
		try {
			client.execute(post);
		} catch (Exception e) {
			e.printStackTrace();
			return "500";
		}

		return "200";

	}

	@Override
	public  Integer updateFCMToken(String token, String mobileNo,String deviceType,Integer schoolId) {
		Integer saveId = 1;
//		try {
//			SchoolMasterModel schoolMaster = new SchoolMasterModel();
//			schoolMaster.setSchoolid(schoolId);
//			if (StringUtils.isEmpty(token) || token.equals("-")) {
//				
//				if(schoolId!=0) {
//				fcmPushNotificationRepository.deleteByMobileNoAndSchoolMasterModel(mobileNo,schoolMaster);
//				}else {
//					fcmPushNotificationRepository.deleteByMobileNo(mobileNo);
//				}
//				
//				} else {
//				List<AndroidFCMTokenMaster> fcmTokenMaster = fcmPushNotificationRepository.getUsersByToken(token);
//				List<AndroidFCMTokenMaster> fcmTokenMaster1 = fcmPushNotificationRepository.getUsersByMobileNo(mobileNo);
//				if (CollectionUtils.isNotEmpty(fcmTokenMaster)) {
//					fcmPushNotificationRepository.deleteByToken(token);
//					List<AppUserModel> appUserModelList = fcmPushNotificationRepository.getAppUsersByMobileNo(mobileNo);
////					List<AppUserModel> appUserModelList = fcmPushNotificationRepository.getAppUsersByMobileNoNew(mobileNo);
//					System.out.println(""+appUserModelList.size());
//					for (AppUserModel appUserModel : appUserModelList) {
//						AndroidFCMTokenMaster androidFCMTokenMaster = new AndroidFCMTokenMaster();
//						androidFCMTokenMaster.setToken(token);
//						androidFCMTokenMaster.setMobileNo(mobileNo);
//						androidFCMTokenMaster.setNotificationUserId(appUserModel.getAppUserRoleId());
//						androidFCMTokenMaster.setDeviceType(deviceType);
//					    if(schoolId==0) {
//					    SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
//						schoolMasterModel.setSchoolid(appUserModel.getSchoolid());
//						androidFCMTokenMaster.setSchoolMasterModel(schoolMasterModel);
//					    }else {
//					    	androidFCMTokenMaster.setSchoolMasterModel(schoolMaster);
//					    }
//						fcmPushNotificationRepository.saveAndFlush(androidFCMTokenMaster);
//					}
//				} else if (CollectionUtils.isNotEmpty(fcmTokenMaster1)) {
//					if(schoolId!=0) {
//						fcmPushNotificationRepository.deleteByMobileNoAndSchoolMasterModel(mobileNo,schoolMaster);
//					}else {
//						fcmPushNotificationRepository.deleteByMobileNo(mobileNo);
//					}
//					List<AppUserModel> appUserModelList = fcmPushNotificationRepository.getAppUsersByMobileNo(mobileNo);
//					System.out.println(""+appUserModelList.size());
//					for (AppUserModel appUserModel : appUserModelList) {
//						AndroidFCMTokenMaster androidFCMTokenMaster = new AndroidFCMTokenMaster();
//						androidFCMTokenMaster.setToken(token);
//						androidFCMTokenMaster.setMobileNo(mobileNo);
//						androidFCMTokenMaster.setNotificationUserId(appUserModel.getAppUserRoleId());
//
////						SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
////						schoolMasterModel.setSchoolid(appUserModel.getSchoolid());
////						androidFCMTokenMaster.setSchoolMasterModel(schoolMasterModel);
//
//						  if(schoolId==0) {
//							    SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
//								schoolMasterModel.setSchoolid(appUserModel.getSchoolid());
//								androidFCMTokenMaster.setSchoolMasterModel(schoolMasterModel);
//							    }else {
//							    	androidFCMTokenMaster.setSchoolMasterModel(schoolMaster);
//							    }
//						fcmPushNotificationRepository.saveAndFlush(androidFCMTokenMaster);
//					}
//				} else {
//					List<AppUserModel> appUserModelList = fcmPushNotificationRepository.getAppUsersByMobileNoNew(mobileNo);
//					System.out.println(""+appUserModelList.size());
//					for (AppUserModel appUserModel : appUserModelList) {
//						AndroidFCMTokenMaster androidFCMTokenMaster = new AndroidFCMTokenMaster();
//						androidFCMTokenMaster.setToken(token);
//						androidFCMTokenMaster.setMobileNo(mobileNo);
//						androidFCMTokenMaster.setNotificationUserId(appUserModel.getAppUserRoleId());
//					  
////						SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
////						schoolMasterModel.setSchoolid(appUserModel.getSchoolid());
////						androidFCMTokenMaster.setSchoolMasterModel(schoolMasterModel);
//						
//						
//						  if(schoolId==0) {
//							    SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
//								schoolMasterModel.setSchoolid(appUserModel.getSchoolid());
//								androidFCMTokenMaster.setSchoolMasterModel(schoolMasterModel);
//							    }else {
//							    	androidFCMTokenMaster.setSchoolMasterModel(schoolMaster);
//							    }
//						fcmPushNotificationRepository.saveAndFlush(androidFCMTokenMaster).getFcmMasterTokenId();
//					}
//				}
//		}
//			
//		//	fCMPushNotificationRepositoryImpl.updateTokenNoInAppUser(token,mobileNo,schoolId);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//
		return saveId;
	}

	@Override
	public List<NotificationBean> getAllNotifications(Integer renewId, String role,Integer offset) {
		List<NotificationBean> notificationList = new ArrayList<NotificationBean>();
		if (role.equalsIgnoreCase("Parent") || role.equalsIgnoreCase("father")) {
			notificationList = fCMPushNotificationRepositoryImpl.getAllNotificationsForStudents(renewId, role,offset);
		} else if (role.equalsIgnoreCase("Teacher")) {
			List<StandardDivisionBean> standardDivisionList = fCMPushNotificationRepositoryImpl.getStandardDivisionList(renewId);
			if (CollectionUtils.isNotEmpty(standardDivisionList)) {
			Integer standardId = standardDivisionList.get(0).getStandardId();
			Integer divisionId = standardDivisionList.get(0).getDivisionId();
			String joinId = "".concat("" + standardId).concat("/" + divisionId).concat("/");
			notificationList = fCMPushNotificationRepositoryImpl.getAllNotificationsForTeachers(renewId, role, joinId,offset);
			}
		}
		List<NotificationBean> notificationList1 = new ArrayList<NotificationBean>();
		List<NotificationBean> todaysList = new ArrayList<NotificationBean>();
		List<NotificationBean> yesterdayList = new ArrayList<NotificationBean>();
		List<NotificationBean> earlierDaysList = new ArrayList<NotificationBean>();
		NotificationBean notificationBean = new NotificationBean();
		if (CollectionUtils.isNotEmpty(notificationList)) {
			notificationList.stream().forEach(notifi -> {
				if (notifi.getDateDiffrenceValue().equalsIgnoreCase("0")) {
					todaysList.add(notifi);
				} else if (notifi.getDateDiffrenceValue().equalsIgnoreCase("1")) {
					yesterdayList.add(notifi);
				} else {
					earlierDaysList.add(notifi);
				}
			});
			notificationBean.setEarlierDaysList(earlierDaysList);
			notificationBean.setYesterDaysList(yesterdayList);
			notificationBean.setTodaysList(todaysList);
			notificationList1.add(notificationBean);
			return notificationList1;
		}
		
		return new ArrayList<NotificationBean>();
	}

	@Override
	public Integer updateFCMToken1(FCMUpdateBean fcmUpdateBean) {
		
		List<FCMUpdateInnerBean> inner = fcmUpdateBean.getInnerDetails();
		
		for (FCMUpdateInnerBean bean : inner) {
			
			List<Integer> exists = fcmPushNotificationRepository.checkIfExists(bean.getSchoolId(), bean.getUserId(), fcmUpdateBean.getMobileNo());
			
			if(exists != null && !exists.isEmpty()) {
				fcmPushNotificationRepository.updateToken(bean.getSchoolId(), bean.getUserId(), fcmUpdateBean.getMobileNo(), fcmUpdateBean.getFcmToken());
			}
			else {
				AndroidFCMTokenMaster tokenMaster = new AndroidFCMTokenMaster();
				SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
				schoolMasterModel.setSchoolid(bean.getSchoolId());
				tokenMaster.setSchoolMasterModel(schoolMasterModel);
				
				tokenMaster.setNotificationUserId(bean.getUserId());
				
				tokenMaster.setMobileNo(fcmUpdateBean.getMobileNo());
				tokenMaster.setToken(fcmUpdateBean.getFcmToken());
				fcmPushNotificationRepository.save(tokenMaster);
			}
		}		
		return 1;
	}

}
