package com.ingenio.admission.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.admission.bean.DeviceCommunicationBean;
import com.ingenio.admission.model.AppUserRoleModel;
import com.ingenio.admission.model.AttendanceClassWiseCatloagModel;
import com.ingenio.admission.model.AttendanceNumOFTimeModel;
import com.ingenio.admission.model.AttendanceRFIDPunchModel;
import com.ingenio.admission.model.DivisionMasterModel;
import com.ingenio.admission.model.MaintainTimeForRFIDSchedularModel;
import com.ingenio.admission.model.PresentAbsentMasterModel;
import com.ingenio.admission.model.RFIDSchoolSettingSchedularModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StaffAttendanceModel;
import com.ingenio.admission.model.StaffBasicDetailsModel;
import com.ingenio.admission.model.StandardMasterModel;
import com.ingenio.admission.model.StudentMasterModel;
import com.ingenio.admission.model.StudentStandardRenewModel;
import com.ingenio.admission.model.YearMasterModel;
import com.ingenio.admission.repository.AttendanceClassWiseCatlogRepository;
import com.ingenio.admission.repository.AttendanceRFIDPunchRepository;
import com.ingenio.admission.repository.DeviceCommunicationRepository;
import com.ingenio.admission.repository.MaintainTimeForRFIDSchedularRepository;
import com.ingenio.admission.repository.RFIDSchoolSettingSchedularRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.repository.StaffAttendanceSaveRepository;
import com.ingenio.admission.repository.impl.DeviceCommunicationRepositoryImpl;
import com.ingenio.admission.service.DeviceCommunicationService;
import com.ingenio.admission.service.UserAuthenticationService;
import com.ingenio.admission.util.CryptographyUtil;


@Component
public class DeviceCommunicationServiceImpl implements DeviceCommunicationService {

	@Autowired
	DeviceCommunicationRepository deviceCommunicationRepository;
	
	@Autowired
	StaffAttendanceSaveRepository staffAttendanceSaveRepository;
	
	@Autowired
	MaintainTimeForRFIDSchedularRepository  maintainTimeForRFIDSchedularRepository;
	
	@Autowired
	DeviceCommunicationRepositoryImpl deviceCommunicationRepositoryImpl;
	@Autowired
	AttendanceClassWiseCatlogRepository attendanceClassWiseCatlogRepository;
	
	@Autowired
	UserAuthenticationService userAuthenticationService;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	private AttendanceRFIDPunchRepository attendanceRFIDPunchRepository;

	@Autowired
	private RFIDSchoolSettingSchedularRepository rfidSchoolSettingSchedularRepository;
	
	CryptographyUtil cry= new CryptographyUtil();
	
	/*
	 * @Autowired StudentMappToRFIDRepository studentMappToRFIDRepository;
	 */
	
//	@Override
//	public DeviceCommunicationBean saveAttendanceRFIData(String rfidString) {
//		try {
//			String rfid="";
//			int deviceId=0;
//			int schoolId=0; 
//			String dot="";
//			String dateOfTransection="";
//			String atterfids="";
//			String rfidString1=""+rfidString.replace("$","");
//			String rfidString2=""+rfidString1.replace("*", "");
//			String rfiString[]=rfidString2.split(",");
//			DeviceCommunicationBean list=new DeviceCommunicationBean();
//			for(int i=0;i<rfiString.length;i++) {
//				AttendanceRFIDPunchModel attendanceRFIDPunchModel=new AttendanceRFIDPunchModel();
//				String idString[]=rfiString[i].split("&");
//				for(int y=0;y<idString.length;y++) {
//					//SchoolMasterModel schoolMasterModel=new SchoolMasterModel();
//					if(i==0) {
//						if(y==0) {
//							//schoolMasterModel.setSchoolid(Integer.parseInt(idString[y]));
//							attendanceRFIDPunchModel.setSchoolId(Integer.parseInt(idString[y]));
//							schoolId=Integer.parseInt(idString[y]);
//						}else if(y==1) {
//							attendanceRFIDPunchModel.setDeviceId(Integer.parseInt(idString[y]));
//							deviceId=Integer.parseInt(idString[y]);
//						}else if(y==2) {
//							attendanceRFIDPunchModel.setRfid(idString[y]);
//							rfid+=idString[y]+",";
//						}else if(y==3) {
//							String sDate1=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+" "+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
//							String sDate2=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+"@"+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
//							//Date date1;
//						    dot+=sDate2+",";
//						    dateOfTransection+=sDate1+",";
//						    
//							SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); 
//							Date date1 =new Date();
//							
//							date1 = df.parse(sDate1);
//							attendanceRFIDPunchModel.setDateOfTransection(date1);
//						}						
//					}else {
//						if(y==0) {
//							attendanceRFIDPunchModel.setRfid(idString[y]);
//							rfid+=idString[y]+",";
//						}else if(y==1) {
//							String sDate1=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+" "+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
//							String sDate2=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+"@"+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
//							Date date1;
//						    dot+=sDate2+",";
//						    dateOfTransection+=sDate1+",";
//							date1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(sDate1);
//							attendanceRFIDPunchModel.setDateOfTransection(date1);
//						}	
//						attendanceRFIDPunchModel.setSchoolId(schoolId);
//						attendanceRFIDPunchModel.setDeviceId(deviceId);
//					}
//				}
//				
////				new 1
////				if()
////				
//				
//				String attendanceRfid = ""+deviceCommunicationRepository.getMaxId();
//				atterfids+=attendanceRfid+",";
//				attendanceRFIDPunchModel.setAttendanceRfid(Integer.parseInt(attendanceRfid));
//				deviceCommunicationRepository.save(attendanceRFIDPunchModel);
//			}
//			//String rfidDot=""+rfid+"#"+dot;
//			list.setRfids(rfid);
//			list.setDots(dot);
//			list.setAttRfids(atterfids);
//			list.setDateOfTransection(dateOfTransection);
//			list.setDeviceId(deviceId);
//			list.setOrganizationId(schoolId);
//			return list;
//			
//		} catch (ParseException e) {
//			e.printStackTrace();
//			return null;
//		}  
//	}
	
//	currently in use
	
	@Override
	public DeviceCommunicationBean saveAttendanceRFIData(String rfidString, String latitude, String longitude, String ipAddress, 
			String macAddress, String randomCode, String imei) {
		try {
			String rfid="";
			int deviceId=0;
			int schoolId=0; 
			String dot="";
			String dateOfTransection="";
			String atterfids="";
			String rfidString1=""+rfidString.replace("$","");
			String rfidString2=""+rfidString1.replace("*", "");
			String rfiString[]=rfidString2.split(",");
			DeviceCommunicationBean list=new DeviceCommunicationBean();
			for(int i=0;i<rfiString.length;i++) {
				AttendanceRFIDPunchModel attendanceRFIDPunchModel=new AttendanceRFIDPunchModel();
				String idString[]=rfiString[i].split("&");
				for(int y=0;y<idString.length;y++) {
					//SchoolMasterModel schoolMasterModel=new SchoolMasterModel();
					if(i==0) {
						if(y==0) {
							//schoolMasterModel.setSchoolid(Integer.parseInt(idString[y]));
							attendanceRFIDPunchModel.setSchoolId(Integer.parseInt(idString[y]));
							schoolId=Integer.parseInt(idString[y]);
						}else if(y==1) {
							attendanceRFIDPunchModel.setDeviceId(Integer.parseInt(idString[y]));
							deviceId=Integer.parseInt(idString[y]);
						}else if(y==2) {
							attendanceRFIDPunchModel.setRfid(idString[y]);
							rfid+=idString[y]+",";
						}else if(y==3) {
							String sDate1=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+" "+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
							String sDate2=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+"@"+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
							//Date date1;
						    dot+=sDate2+",";
						    dateOfTransection+=sDate1+",";
						    
							SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); 
							Date date1 =new Date();
							
							date1 = df.parse(sDate1);
							attendanceRFIDPunchModel.setDateOfTransection(date1);
						}						
					}else {
						if(y==0) {
							attendanceRFIDPunchModel.setRfid(idString[y]);
							rfid+=idString[y]+",";
						}else if(y==1) {
							String sDate1=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+" "+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
							String sDate2=""+idString[y].charAt(0)+""+idString[y].charAt(1)+"-"+idString[y].charAt(2)+""+idString[y].charAt(3)+"-"+idString[y].charAt(4)+""+idString[y].charAt(5)+""+idString[y].charAt(6)+""+idString[y].charAt(7)+"@"+idString[y].charAt(8)+""+idString[y].charAt(9)+":"+idString[y].charAt(10)+""+idString[y].charAt(11)+":"+idString[y].charAt(12)+""+idString[y].charAt(13);
							Date date1;
						    dot+=sDate2+",";
						    dateOfTransection+=sDate1+",";
							date1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(sDate1);
							attendanceRFIDPunchModel.setDateOfTransection(date1);
						}	
						attendanceRFIDPunchModel.setSchoolId(schoolId);
						attendanceRFIDPunchModel.setDeviceId(deviceId);
					}
				}
				
//				new 1
				if((latitude != null) && !(latitude.equalsIgnoreCase("0")))
					attendanceRFIDPunchModel.setLatitude(latitude);
				if((longitude != null) && !(longitude.equalsIgnoreCase("0")))
					attendanceRFIDPunchModel.setLongitude(longitude);
				if((ipAddress != null) && !(ipAddress.equalsIgnoreCase("0")))
					attendanceRFIDPunchModel.setIpAddress(ipAddress);
				if((imei != null) && !(imei.equalsIgnoreCase("0")))
					attendanceRFIDPunchModel.setImei(imei);
				if((macAddress != null) && !(macAddress.equalsIgnoreCase("0")))
					attendanceRFIDPunchModel.setMacAddress(macAddress);
				if((randomCode != null) && !(randomCode.equalsIgnoreCase("0")))
					attendanceRFIDPunchModel.setRandomCode(randomCode);
//				
				
//				String attendanceRfid = ""+deviceCommunicationRepository.getMaxId();
//				atterfids+=attendanceRfid+",";
//				attendanceRFIDPunchModel.setAttendanceRfid(Integer.parseInt(attendanceRfid));
				AttendanceRFIDPunchModel attendanceRfid = deviceCommunicationRepository.save(attendanceRFIDPunchModel);
				// new
				atterfids+=attendanceRfid.getAttendanceRfid()+",";
			}
			//String rfidDot=""+rfid+"#"+dot;
			System.out.println("rfid - " + rfid);
			list.setRfids(rfid);
			
			System.out.println("dot - " + dot);
			list.setDots(dot);
			
			System.out.println("atterfids - " + atterfids);
			list.setAttRfids(atterfids);
			
			System.out.println("dateOfTransection - " + dateOfTransection);
			list.setDateOfTransection(dateOfTransection);
			
//			System.out.println();
			list.setDeviceId(deviceId);
			
//			System.out.println();
			list.setOrganizationId(schoolId);
			
			return list;
			
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}  
	}
	
	@Override
	public boolean saveStaffAttendance(DeviceCommunicationBean deviceCommunicationBean) {
		try {
			// staff
		String rfid1[]=deviceCommunicationBean.getRfids().split(",");
		String dot1[]=deviceCommunicationBean.getDots().split(",");
		String attRfid[]=deviceCommunicationBean.getAttRfids().split(",");
		String dateOfTran[]=deviceCommunicationBean.getDateOfTransection().split(",");
		int deviceId=deviceCommunicationBean.getDeviceId();
		int organizationId=deviceCommunicationBean.getOrganizationId();
		List<DeviceCommunicationBean>  schoolList=deviceCommunicationRepositoryImpl.getSchoolId(deviceId,organizationId);
		for(int t=0;t<rfid1.length;t++) {
			for(int f=0;f<schoolList.size();f++) {
				List<DeviceCommunicationBean> list= deviceCommunicationRepositoryImpl.getStaffIdFromStaffRfid(rfid1[t],schoolList.get(f).getSchoolId(),dateOfTran[t],deviceId,organizationId);
				if(CollectionUtils.isNotEmpty(list)) {	
					for(int r=0;r<list.size();r++) {
						SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
						schoolMasterModel.setSchoolid(list.get(r).getSchoolId());
						Integer  principaluserId = deviceCommunicationRepository.getprincipalUserId(list.get(r).getSchoolId());
						AppUserRoleModel appUserRoleModel =new AppUserRoleModel();
						appUserRoleModel.setAppUserRoleId(principaluserId);
						if(list.get(r).getStaffOrStudentFlag().equals("1")) {
							String splitDate[]=dot1[t].split("@");
//							make changes here to get the inTime and then compare it with the outTime to be posted. If equal, do not update/post.
							Integer staffId=deviceCommunicationRepository.getStaffId(list.get(r).getStaffId(),list.get(r).getSchoolId(),splitDate[0]);
//							StaffAttendanceModel staffAttDetails =deviceCommunicationRepository.getStaffId(list.get(r).getStaffId(),list.get(r).getSchoolId(),splitDate[0]);							
//							Integer staffId = staffAttDetails.getStaffBasicDetailsModel().getstaffId();
//
							StaffAttendanceModel staffAttendanceModel=new StaffAttendanceModel();
							StaffBasicDetailsModel staffBasicDetailsModel=new StaffBasicDetailsModel();
							staffAttendanceModel.setDate(splitDate[0]);
							Integer getStaffAttendanceId=deviceCommunicationRepository.getStaffAttendanceId(list.get(r).getSchoolId());
							staffAttendanceModel.setUserId(appUserRoleModel);
							staffAttendanceModel.setSchoolMasterModel(schoolMasterModel);
							String splitTime[]=splitDate[1].split(":");
							String _24HourTime = ""+splitTime[0]+":"+splitTime[1];
				           SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
				           SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
				           	Date _24HourDt = _24HourSDF.parse(_24HourTime);
				           	List<DeviceCommunicationBean> list1=deviceCommunicationRepositoryImpl.getAllStaffIdData(staffId);
							if(staffId!=null) {
								staffAttendanceModel.setStaffAttendanceID(staffId);
								staffAttendanceModel.setIsEdit("1");
//								staffAttendanceModel.setOutTime(_12HourSDF.format(_24HourDt));
								staffBasicDetailsModel.setstaffId(list.get(r).getStaffId());
								staffAttendanceModel.setCreatedDate(list1.get(0).getCreateDate());
								staffAttendanceModel.setInTime(list1.get(0).getInTime());
								if(!_12HourSDF.format(_24HourDt).equalsIgnoreCase(list1.get(0).getInTime()))
									staffAttendanceModel.setOutTime(_12HourSDF.format(_24HourDt));
								staffAttendanceModel.setPresentFlag(list1.get(0).getPresentFlag());
								staffAttendanceModel.setInTimeAttendanceRfid(list1.get(0).getInRfId());
								staffAttendanceModel.setOutTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t],dateOfTran[t],0);
							}else {
								staffBasicDetailsModel.setstaffId(list.get(r).getStaffId());
								staffAttendanceModel.setStaffAttendanceID(getStaffAttendanceId);
								staffAttendanceModel.setInTime(_12HourSDF.format(_24HourDt));
								staffAttendanceModel.setCreatedDate(splitDate[0]+" "+splitDate[1]);
								staffAttendanceModel.setPresentFlag(0);
								staffAttendanceModel.setInTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t],dateOfTran[t],1);
							}
							staffAttendanceModel.setStaffBasicDetailsModel(staffBasicDetailsModel);
							staffAttendanceModel.setStatus(1);
							staffAttendanceSaveRepository.save(staffAttendanceModel);
						}else {
							AttendanceClassWiseCatloagModel attendanceClassWiseCatloagModel=new AttendanceClassWiseCatloagModel();
							String splitDate[]=dot1[t].split("@");
							List<DeviceCommunicationBean> list2=deviceCommunicationRepositoryImpl.getAttendanceClassId(list.get(r).getStudentId(),list.get(r).getSchoolId(),splitDate[0],list.get(r).getStudRenewId());
							DivisionMasterModel divisionMasterModel=new DivisionMasterModel();
							YearMasterModel yearMasterModel=new YearMasterModel();
							StandardMasterModel standardMasterModel=new StandardMasterModel();
							StudentMasterModel studentMasterModel=new StudentMasterModel();
							StudentStandardRenewModel studentStandardRenewModel=new StudentStandardRenewModel();
							PresentAbsentMasterModel presentAbsentMasterModel=new PresentAbsentMasterModel();
							presentAbsentMasterModel.setPresentAbsentiD(null);
							AttendanceNumOFTimeModel attendanceNumOFTimeModel=new AttendanceNumOFTimeModel();
							attendanceNumOFTimeModel.setAttendanceNumId(list.get(r).getSchoolId());
							String splitDatemonth[]=splitDate[0].split("-");
							int month=0;
							if(splitDatemonth[1].charAt(0)==0) {
								month=splitDatemonth[1].charAt(1);
								
							}else {
								month=Integer.parseInt(splitDatemonth[1]);
							}
							String splitTime[]=splitDate[1].split(":");
							String _24HourTime = ""+splitTime[0]+":"+splitTime[1];
							if(CollectionUtils.isNotEmpty(list2) && !list2.get(0).getAttandenceClasswiseId().equals(null)) {
								divisionMasterModel.setDivisionId(list2.get(0).getDivisionId());
								yearMasterModel.setYearId(list2.get(0).getYearId());
								standardMasterModel.setStandardId(list2.get(0).getStandardId());
								studentMasterModel.setStudentId(list2.get(0).getStudentId());
								studentStandardRenewModel.setRenewStudentId(list2.get(0).getStudRenewId());
								attendanceClassWiseCatloagModel.setAttandenceClasswiseId(Long.parseLong(list2.get(0).getAttandenceClasswiseId()));
								attendanceClassWiseCatloagModel.setSchoolMasterModel(schoolMasterModel);
								attendanceClassWiseCatloagModel.setStudentMasterModel(studentMasterModel);
								attendanceClassWiseCatloagModel.setStudentStandardRenewModel(studentStandardRenewModel);
								attendanceClassWiseCatloagModel.setMonthId(list2.get(0).getMonthId());
								attendanceClassWiseCatloagModel.setRollNo(Integer.parseInt(list2.get(0).getRollNo()));
								attendanceClassWiseCatloagModel.setAttendanceDate(list2.get(0).getAttendanceDate());
								attendanceClassWiseCatloagModel.setInTime(list2.get(0).getInTime());
								attendanceClassWiseCatloagModel.setOutTime(_24HourTime);
								attendanceClassWiseCatloagModel.setP_AFlag(list2.get(0).getpFlag());
								//attendanceClassWiseCatloagModel.setPresentAbsentMasterModel(presentAbsentMasterModel);
								attendanceClassWiseCatloagModel.setNoOfTimeAttendance(attendanceNumOFTimeModel);
								attendanceClassWiseCatloagModel.setRemark(list2.get(0).getRemark());
								attendanceClassWiseCatloagModel.setHardwareType(list2.get(0).getHardwareType());
								attendanceClassWiseCatloagModel.setDivisionMasterModel(divisionMasterModel);
								attendanceClassWiseCatloagModel.setStandardMasterModel(standardMasterModel);
								attendanceClassWiseCatloagModel.setYearMasterModel(yearMasterModel);
								attendanceClassWiseCatloagModel.setUserId(appUserRoleModel);
								attendanceClassWiseCatloagModel.setInTimeAttendanceRfid(list2.get(0).getInRfId());
								attendanceClassWiseCatloagModel.setOutTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								//deviceCommunicationRepository.updateOutTimeAttRfid(rfid1[t],dateOfTran[t]);
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t],dateOfTran[t],0);
							}else {
								divisionMasterModel.setDivisionId(list.get(r).getDivisionId());
								yearMasterModel.setYearId(list.get(r).getYearId());
								standardMasterModel.setStandardId(list.get(r).getStandardId());
								studentMasterModel.setStudentId(list.get(r).getStudentId());
								studentStandardRenewModel.setRenewStudentId(list.get(r).getStudRenewId());
								Long getattendanceMaxId=deviceCommunicationRepository.getattendanceMaxId(list.get(r).getSchoolId());
								attendanceClassWiseCatloagModel.setAttandenceClasswiseId(getattendanceMaxId);
								attendanceClassWiseCatloagModel.setSchoolMasterModel(schoolMasterModel);
								attendanceClassWiseCatloagModel.setStudentMasterModel(studentMasterModel);
								attendanceClassWiseCatloagModel.setStudentStandardRenewModel(studentStandardRenewModel);
								attendanceClassWiseCatloagModel.setMonthId(month);
								if(!list.get(r).getRollNo().equals("")) {
									attendanceClassWiseCatloagModel.setRollNo(Integer.parseInt(list.get(r).getRollNo()));
								}
								
								attendanceClassWiseCatloagModel.setAttendanceDate(splitDate[0]);
								attendanceClassWiseCatloagModel.setInTime(_24HourTime);
								attendanceClassWiseCatloagModel.setP_AFlag("P");
								attendanceClassWiseCatloagModel.setNoOfTimeAttendance(attendanceNumOFTimeModel);
								attendanceClassWiseCatloagModel.setHardwareType(rfid1[t]);
								attendanceClassWiseCatloagModel.setDivisionMasterModel(divisionMasterModel);
								attendanceClassWiseCatloagModel.setStandardMasterModel(standardMasterModel);
								attendanceClassWiseCatloagModel.setYearMasterModel(yearMasterModel);
								attendanceClassWiseCatloagModel.setUserId(appUserRoleModel);
								attendanceClassWiseCatloagModel.setInTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t],dateOfTran[t],1);
								//deviceCommunicationRepository.updateInTimeAttRfid(rfid1[t],dateOfTran[t]);
							}
							attendanceClassWiseCatlogRepository.save(attendanceClassWiseCatloagModel);
						}
					}
			    }
			}
		}
		return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}  
	}

	@Override
	public DeviceCommunicationBean sendSMSForAbsentStudent() throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, IOException {
		try {
			List<DeviceCommunicationBean> list=deviceCommunicationRepositoryImpl.getAllSchoolDataForAbsentList();
			List<DeviceCommunicationBean> schoolList=deviceCommunicationRepositoryImpl.getPerticularSchoolList();
			List<Integer> intSchoolList=schoolList.stream().map(n->n.getSchoolId()).collect(Collectors.toList());
			for(int i=0;i<list.size();i++) {
				 List<DeviceCommunicationBean> list1=deviceCommunicationRepositoryImpl.getAbsentStudentList(list.get(i).getSchoolId());
				 List<DeviceCommunicationBean> singleList=deviceCommunicationRepositoryImpl.getAllSinglePunchRecord(list.get(i).getSchoolId());
				 String date1[]=list1.get(0).getCurrDate().split("-");
				 String mainDate="";
				 String month="";
				 if(date1[0].charAt(0)=='0') {
					 mainDate=""+date1[0].charAt(1);
				 }else {
					 mainDate=""+date1[0];
				 }
				 
				 if(date1[1].charAt(0)=='0') {
						month=""+date1[1].charAt(1);
						
					}else {
						month=""+date1[1];
					}
				 
				 int holidayList=deviceCommunicationRepositoryImpl.getHolidayList(list.get(i).getSchoolId(),mainDate,month,date1[2]);
				 
				 SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
				 schoolMasterModel.setSchoolid(list.get(i).getSchoolId());
				 LocalDate datetime = LocalDate.now();
				 int gettimeId=deviceCommunicationRepositoryImpl.gettimeId(list.get(i).getSchoolId(),datetime);
				 if(holidayList==0) {
				 if(gettimeId==0) {
				if(list1.size()>0) {
					for(int r=0;r<list1.size();r++) 
					  { 
						  AttendanceClassWiseCatloagModel attendanceClassWiseCatloagModel=new AttendanceClassWiseCatloagModel();
						  DivisionMasterModel divisionMasterModel=new DivisionMasterModel();
						  YearMasterModel yearMasterModel=new YearMasterModel(); StandardMasterModel
						  standardMasterModel=new StandardMasterModel(); StudentMasterModel
						  studentMasterModel=new StudentMasterModel(); StudentStandardRenewModel
						  studentStandardRenewModel=new StudentStandardRenewModel();
						  PresentAbsentMasterModel presentAbsentMasterModel=new
						  PresentAbsentMasterModel();
						  presentAbsentMasterModel.setPresentAbsentiD(null); AttendanceNumOFTimeModel
						  attendanceNumOFTimeModel=new AttendanceNumOFTimeModel();
						  attendanceNumOFTimeModel.setAttendanceNumId(list.get(i).getSchoolId());
						  
						  divisionMasterModel.setDivisionId(list1.get(r).getDivisionId());
						  yearMasterModel.setYearId(list1.get(r).getYearId());
						  standardMasterModel.setStandardId(list1.get(r).getStandardId());
						  studentMasterModel.setStudentId(list1.get(r).getStudentId());
						  studentStandardRenewModel.setRenewStudentId(list1.get(r).getStudRenewId());
						  SchoolMasterModel schoolModel =
						  schoolMasterRepository.findBySchoolid(Integer.valueOf(list1.get(r).getSchoolId())); 
						  Integer languageId =schoolModel.getLanguage().getLanguageId();
					  
						  Long getattendanceMaxId=deviceCommunicationRepository.getattendanceMaxId(list1.get(r).getSchoolId());
						  attendanceClassWiseCatloagModel.setAttandenceClasswiseId(getattendanceMaxId);
						  attendanceClassWiseCatloagModel.setSchoolMasterModel(schoolMasterModel);
						  attendanceClassWiseCatloagModel.setStudentMasterModel(studentMasterModel);
						  attendanceClassWiseCatloagModel.setStudentStandardRenewModel(
						  studentStandardRenewModel); 
						  String date[]=list1.get(r).getCurrDate().split("-");
						  attendanceClassWiseCatloagModel.setMonthId(Integer.parseInt(date[1]));
						  if(!list1.get(r).getRollNo().equals("")) {
							  attendanceClassWiseCatloagModel.setRollNo(
							 Integer.parseInt(list1.get(r). getRollNo())); 
							}
					  
						  attendanceClassWiseCatloagModel.setAttendanceDate(list1.get(r).getCurrDate()); 
						  attendanceClassWiseCatloagModel.setInTime(null);
						  attendanceClassWiseCatloagModel.setP_AFlag("A");
						  attendanceClassWiseCatloagModel.setNoOfTimeAttendance(
						  attendanceNumOFTimeModel);
						  attendanceClassWiseCatloagModel.setHardwareType(list1.get(r).getRfids());
						  attendanceClassWiseCatloagModel.setDivisionMasterModel(divisionMasterModel);
						  attendanceClassWiseCatloagModel.setStandardMasterModel(standardMasterModel);
						  attendanceClassWiseCatloagModel.setYearMasterModel(yearMasterModel);
						  //attendanceClassWiseCatloagModel.setUserId(null);
						  //attendanceClassWiseCatloagModel.setInTimeAttendanceRfid(nu);
						  attendanceClassWiseCatlogRepository.save(attendanceClassWiseCatloagModel);
						  String schoolName = schoolModel.getSchoolAlias(); String message=""; //
						  message ="Dear "+list1.get(r).getStudName()+" No RFID Record Detected on "+list1.get(r).getCurrDate()+". Your attendance Marked as \"Absent\". "+schoolName+" INGENIO Technologies Pvt Ltd"; 
						 // message ="Dear "+list1.get(r).getStudName()+" No RFID Record Detected on "+list1.get(r).getCurrDate()+". Your attendance Marked as \"Absent\". "+schoolName+" INGENIO Technologies Pvt Ltd"; 
						  System.out.println(message);
						  String templateId="1207164067507421248";
						  if(intSchoolList.contains(list1.get(r).getSchoolId())) {
							  userAuthenticationService.sendOtpMessageForRFID(list1.get(r).getMobileNo(),message,Integer.valueOf(list1.get(r).getSchoolId()),languageId,templateId);
						  }
					  }
				}
				  
				 // for single punch data
					if(singleList.size()>0) {
						  for(int j=0;j<singleList.size();j++) {
							  SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(Integer.valueOf(list1.get(0).getSchoolId()));
							  String schoolName = schoolModel.getSchoolAlias();
							  Integer languageId = schoolModel.getLanguage().getLanguageId();
								 String message="";
								 message = "Dear "+singleList.get(j).getStudName()+" RFID Record Detected on "+singleList.get(j).getCurrDate()+".In Time="+singleList.get(j).getInTime()+",Out Time="+singleList.get(j).getOutTime()+",but Not Completed Presenty Hours.Your attendance Marked as \"Absent\". "+schoolName+" INGENIO Technologies Pvt Ltd";
								// message = "Dear "+singleList.get(j).getStudName()+" RFID Record Detected on "+list1.get(0).getCurrDate()+".Your attendance Marked as \"Absent\". "+schoolName+" INGENIO Technologies Pvt Ltd";
								 System.out.println(message);
								 String templateId="1207164067517472058";
								 userAuthenticationService.sendOtpMessageForRFID(singleList.get(j).getMobileNo(), message,Integer.valueOf(list1.get(0).getSchoolId()),languageId,templateId);
						  }
					}
					
					Integer gettimeschedularId=deviceCommunicationRepository.gettimeschedularId(list.get(i).getSchoolId()) ;
					MaintainTimeForRFIDSchedularModel maintainTimeForRFIDSchedularModel=new MaintainTimeForRFIDSchedularModel();
					maintainTimeForRFIDSchedularModel.setSchedularTimeId(gettimeschedularId);
					maintainTimeForRFIDSchedularModel.setSchoolMasterModel(schoolMasterModel);
					maintainTimeForRFIDSchedularRepository.save(maintainTimeForRFIDSchedularModel);
				 }
				}
			}
			
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} 
		 return null;
	}

	@Override
	public String rfidPunchToStaffAttendance(Integer schoolId, String fromDate, String toDate) {
//	public String rfidPunchToStaffAttendance(Integer schoolId, String date) {
		try {					
			
//			if(schoolId == 1144000001)
//			{
//				
//			}
			
			String schoolId1 = String.valueOf(schoolId);
//	        Integer orgId = Integer.parseInt(schoolId1.substring(0, 5));
			
//	        RFIDSchoolSettingSchedularModel device = rfidSchoolSettingSchedularRepository.getDeviceId(schoolId, orgId);
	        RFIDSchoolSettingSchedularModel device = rfidSchoolSettingSchedularRepository.getDeviceId(schoolId);
	        
	        Integer orgId = device.getOrganizationId();
	        
//	        List<DeviceCommunicationBean> fromPunch = attendanceRFIDPunchRepository.getAttendanceRfidDataBySchoolId(orgId, date);
	        List<DeviceCommunicationBean> fromPunch = attendanceRFIDPunchRepository.getAttendanceRfidDataBySchoolId(orgId, fromDate, toDate);
	        
	        StringBuilder rfids = new StringBuilder();
	        StringBuilder dots = new StringBuilder();
	        StringBuilder attRfids = new StringBuilder();
	        StringBuilder dateOfTransections = new StringBuilder();

	        String inputFormat = "yyyy-MM-dd HH:mm:ss";
	        String outputFormat = "dd-MM-yyyy HH:mm:ss";
	        SimpleDateFormat inputFormatter = new SimpleDateFormat(inputFormat);
	        SimpleDateFormat outputFormatter = new SimpleDateFormat(outputFormat);
	        
	        for (DeviceCommunicationBean bean : fromPunch) {
	        	
	            rfids.append(bean.getRfids()).append(",");
	            
	            String dot3 = bean.getcDate().toString();
	            Date dot4 = inputFormatter.parse(dot3);
	            String dot1 = outputFormatter.format(dot4);
	            String dot2 = dot1.replace(' ', '@');
	            dots.append(dot2).append(",");
	            
	            attRfids.append(bean.getStudentId()).append(",");
	            
	            String dateOfTransection3 = bean.getcDate().toString();
	            Date dateOfTransection4 = inputFormatter.parse(dateOfTransection3);
	            String dateOfTransection1 = outputFormatter.format(dateOfTransection4);
	            dateOfTransections.append(dateOfTransection1).append(",");
	        }

	        System.out.println("RFIDs: " + rfids.toString());
	        System.out.println("Dots: " + dots.toString());
	        System.out.println("AttRfIDs: " + attRfids.toString());
	        System.out.println("DateOfTransections: " + dateOfTransections.toString());	        	        
		
	    String rfids2 = rfids.toString();	        
		String rfid1[] = rfids2.split(",");
		
		String dot2 = dots.toString();
		String dot1[] = dot2.split(",");
		
		String attRfid1 = attRfids.toString();
		String attRfid[] = attRfid1.split(",");
				
		String dateOfTran1 = dateOfTransections.toString();
		String dateOfTran[] = dateOfTran1.split(",");
		
//		int deviceId = 99;
		int deviceId = device.getDeviceId();
		int organizationId = orgId;
				
		List<DeviceCommunicationBean>  schoolList=deviceCommunicationRepositoryImpl.getSchoolId(deviceId,organizationId);
		for(int t=0;t<rfid1.length;t++) {
//			for (int t = 0; t < rfid1.length; t++) {
			for(int f=0;f<schoolList.size();f++) {
//				List<DeviceCommunicationBean> list = deviceCommunicationRepositoryImpl.getStaffIdFromStaffRfid(rfid1[t],
//						schoolId, dateOfTran[t], deviceId, organizationId);
				List<DeviceCommunicationBean> list= deviceCommunicationRepositoryImpl.getStaffIdFromStaffRfid(rfid1[t],schoolList.get(f).getSchoolId(),
						dateOfTran[t],deviceId,organizationId);
				if (CollectionUtils.isNotEmpty(list)) {
					for (int r = 0; r < list.size(); r++) {
						SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
						schoolMasterModel.setSchoolid(list.get(r).getSchoolId());
						Integer principaluserId = deviceCommunicationRepository
								.getprincipalUserId(list.get(r).getSchoolId());
						AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
						appUserRoleModel.setAppUserRoleId(principaluserId);
						if (list.get(r).getStaffOrStudentFlag().equals("1")) {
							String splitDate[] = dot1[t].split("@");
							Integer staffId = deviceCommunicationRepository.getStaffId(list.get(r).getStaffId(),
									list.get(r).getSchoolId(), splitDate[0]);
							StaffAttendanceModel staffAttendanceModel = new StaffAttendanceModel();
							StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
							staffAttendanceModel.setDate(splitDate[0]);
							Integer getStaffAttendanceId = deviceCommunicationRepository
									.getStaffAttendanceId(list.get(r).getSchoolId());
							
//	Change 29-11-2024 start
							if(getStaffAttendanceId == 1)
								getStaffAttendanceId = list.get(r).getSchoolId();
//	Change 29-11-2024 end
							staffAttendanceModel.setUserId(appUserRoleModel);
							staffAttendanceModel.setSchoolMasterModel(schoolMasterModel);
							String splitTime[] = splitDate[1].split(":");
							String _24HourTime = "" + splitTime[0] + ":" + splitTime[1];
							SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
							SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
							Date _24HourDt = _24HourSDF.parse(_24HourTime);
							List<DeviceCommunicationBean> list1 = deviceCommunicationRepositoryImpl
									.getAllStaffIdData(staffId);
							if (staffId != null) {
								staffAttendanceModel.setStaffAttendanceID(staffId);
								staffAttendanceModel.setIsEdit("1");
//								staffAttendanceModel.setOutTime(_12HourSDF.format(_24HourDt));
								staffBasicDetailsModel.setstaffId(list.get(r).getStaffId());
								staffAttendanceModel.setCreatedDate(list1.get(0).getCreateDate());
								staffAttendanceModel.setInTime(list1.get(0).getInTime());
								if(!_12HourSDF.format(_24HourDt).equalsIgnoreCase(list1.get(0).getInTime()))
									staffAttendanceModel.setOutTime(_12HourSDF.format(_24HourDt));
								staffAttendanceModel.setPresentFlag(list1.get(0).getPresentFlag());
								staffAttendanceModel.setInTimeAttendanceRfid(list1.get(0).getInRfId());
								staffAttendanceModel.setOutTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t],dateOfTran[t],0);
//								if (!attRfid[t].equalsIgnoreCase(list1.get(0).getInTime()))
//								if (!list1.get(0).getInTime().equalsIgnoreCase(list1.get(0).getInTime()))
//									deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t], dateOfTran[t], 0);
							} else {
								staffBasicDetailsModel.setstaffId(list.get(r).getStaffId());
								staffAttendanceModel.setStaffAttendanceID(getStaffAttendanceId);
								staffAttendanceModel.setInTime(_12HourSDF.format(_24HourDt));
								staffAttendanceModel.setCreatedDate(splitDate[0] + " " + splitDate[1]);
								staffAttendanceModel.setPresentFlag(0);
								staffAttendanceModel.setInTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t], dateOfTran[t], 1);
							}
							staffAttendanceModel.setStaffBasicDetailsModel(staffBasicDetailsModel);
							staffAttendanceModel.setStatus(1);
							staffAttendanceSaveRepository.save(staffAttendanceModel);
						} else {
							AttendanceClassWiseCatloagModel attendanceClassWiseCatloagModel = new AttendanceClassWiseCatloagModel();
							String splitDate[] = dot1[t].split("@");
							List<DeviceCommunicationBean> list2 = deviceCommunicationRepositoryImpl
									.getAttendanceClassId(list.get(r).getStudentId(), list.get(r).getSchoolId(),
											splitDate[0], list.get(r).getStudRenewId());
							DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
							YearMasterModel yearMasterModel = new YearMasterModel();
							StandardMasterModel standardMasterModel = new StandardMasterModel();
							StudentMasterModel studentMasterModel = new StudentMasterModel();
							StudentStandardRenewModel studentStandardRenewModel = new StudentStandardRenewModel();
							PresentAbsentMasterModel presentAbsentMasterModel = new PresentAbsentMasterModel();
							presentAbsentMasterModel.setPresentAbsentiD(null);
							AttendanceNumOFTimeModel attendanceNumOFTimeModel = new AttendanceNumOFTimeModel();
							attendanceNumOFTimeModel.setAttendanceNumId(list.get(r).getSchoolId());
							String splitDatemonth[] = splitDate[0].split("-");
							int month = 0;
							if (splitDatemonth[1].charAt(0) == 0) {
								month = splitDatemonth[1].charAt(1);

							} else {
								month = Integer.parseInt(splitDatemonth[1]);
							}
							String splitTime[] = splitDate[1].split(":");
							String _24HourTime = "" + splitTime[0] + ":" + splitTime[1];
							if (CollectionUtils.isNotEmpty(list2)
									&& !list2.get(0).getAttandenceClasswiseId().equals(null)) {
								divisionMasterModel.setDivisionId(list2.get(0).getDivisionId());
								yearMasterModel.setYearId(list2.get(0).getYearId());
								standardMasterModel.setStandardId(list2.get(0).getStandardId());
								studentMasterModel.setStudentId(list2.get(0).getStudentId());
								studentStandardRenewModel.setRenewStudentId(list2.get(0).getStudRenewId());
								attendanceClassWiseCatloagModel.setAttandenceClasswiseId(
										Long.parseLong(list2.get(0).getAttandenceClasswiseId()));
								attendanceClassWiseCatloagModel.setSchoolMasterModel(schoolMasterModel);
								attendanceClassWiseCatloagModel.setStudentMasterModel(studentMasterModel);
								attendanceClassWiseCatloagModel.setStudentStandardRenewModel(studentStandardRenewModel);
								attendanceClassWiseCatloagModel.setMonthId(list2.get(0).getMonthId());
								attendanceClassWiseCatloagModel.setRollNo(Integer.parseInt(list2.get(0).getRollNo()));
								attendanceClassWiseCatloagModel.setAttendanceDate(list2.get(0).getAttendanceDate());
								attendanceClassWiseCatloagModel.setInTime(list2.get(0).getInTime());
								attendanceClassWiseCatloagModel.setOutTime(_24HourTime);
								attendanceClassWiseCatloagModel.setP_AFlag(list2.get(0).getpFlag());
								attendanceClassWiseCatloagModel.setNoOfTimeAttendance(attendanceNumOFTimeModel);
								attendanceClassWiseCatloagModel.setRemark(list2.get(0).getRemark());
								attendanceClassWiseCatloagModel.setHardwareType(list2.get(0).getHardwareType());
								attendanceClassWiseCatloagModel.setDivisionMasterModel(divisionMasterModel);
								attendanceClassWiseCatloagModel.setStandardMasterModel(standardMasterModel);
								attendanceClassWiseCatloagModel.setYearMasterModel(yearMasterModel);
								attendanceClassWiseCatloagModel.setUserId(appUserRoleModel);
								attendanceClassWiseCatloagModel.setInTimeAttendanceRfid(list2.get(0).getInRfId());
								attendanceClassWiseCatloagModel.setOutTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								//deviceCommunicationRepository.updateOutTimeAttRfid(rfid1[t],dateOfTran[t]);
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t], dateOfTran[t], 0);
							} else {
								divisionMasterModel.setDivisionId(list.get(r).getDivisionId());
								yearMasterModel.setYearId(list.get(r).getYearId());
								standardMasterModel.setStandardId(list.get(r).getStandardId());
								studentMasterModel.setStudentId(list.get(r).getStudentId());
								studentStandardRenewModel.setRenewStudentId(list.get(r).getStudRenewId());
								Long getattendanceMaxId = deviceCommunicationRepository
										.getattendanceMaxId(list.get(r).getSchoolId());
								attendanceClassWiseCatloagModel.setAttandenceClasswiseId(getattendanceMaxId);
								attendanceClassWiseCatloagModel.setSchoolMasterModel(schoolMasterModel);
								attendanceClassWiseCatloagModel.setStudentMasterModel(studentMasterModel);
								attendanceClassWiseCatloagModel.setStudentStandardRenewModel(studentStandardRenewModel);
								attendanceClassWiseCatloagModel.setMonthId(month);
								if (!list.get(r).getRollNo().equals("")) {
									attendanceClassWiseCatloagModel
											.setRollNo(Integer.parseInt(list.get(r).getRollNo()));
								}

								attendanceClassWiseCatloagModel.setAttendanceDate(splitDate[0]);
								attendanceClassWiseCatloagModel.setInTime(_24HourTime);
								attendanceClassWiseCatloagModel.setP_AFlag("P");
								attendanceClassWiseCatloagModel.setNoOfTimeAttendance(attendanceNumOFTimeModel);
								attendanceClassWiseCatloagModel.setHardwareType(rfid1[t]);
								attendanceClassWiseCatloagModel.setDivisionMasterModel(divisionMasterModel);
								attendanceClassWiseCatloagModel.setStandardMasterModel(standardMasterModel);
								attendanceClassWiseCatloagModel.setYearMasterModel(yearMasterModel);
								attendanceClassWiseCatloagModel.setUserId(appUserRoleModel);
								attendanceClassWiseCatloagModel.setInTimeAttendanceRfid(Integer.parseInt(attRfid[t]));
								deviceCommunicationRepositoryImpl.updateOutTimeAttRfid(rfid1[t], dateOfTran[t], 1);
								//deviceCommunicationRepository.updateInTimeAttRfid(rfid1[t],dateOfTran[t]);
							}
							attendanceClassWiseCatlogRepository.save(attendanceClassWiseCatloagModel);
						}
						//					}
					}
				}
			} 
		}
		return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}  
	}
	


}
