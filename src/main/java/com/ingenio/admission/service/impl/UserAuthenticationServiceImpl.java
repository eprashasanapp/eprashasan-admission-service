package com.ingenio.admission.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.ingenio.admission.bean.AppRoleBean;
import com.ingenio.admission.bean.StudentAdmissionBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.WhatsAppMessageBean;
import com.ingenio.admission.model.FeeSendSmsNumbersMstrModel;
import com.ingenio.admission.model.GlobalWhatsAppSMSModel;
import com.ingenio.admission.model.SMSAPiMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.SchoolWiseWhatsAppSMSModel;
import com.ingenio.admission.repository.AppUserRepository;
import com.ingenio.admission.repository.GlobalWhatsAppSMSRepository;
import com.ingenio.admission.repository.SaveSendWhatsAppSMSRepository;
import com.ingenio.admission.repository.SmsApiMasterRepository;
import com.ingenio.admission.repository.StudentMasterRepository;
import com.ingenio.admission.repository.UserAuthenticationRepository;
import com.ingenio.admission.service.UserAuthenticationService;
import com.ingenio.admission.util.CryptographyUtil;

@Service
public class UserAuthenticationServiceImpl implements UserAuthenticationService{
	
	@Autowired
	UserAuthenticationRepository userAuthenticationRepository;
	
	@Autowired
	StudentMasterRepository studentMasterRepository;
	
	@Autowired
	SmsApiMasterRepository smsApiMasterRepository;
	
	@Autowired
	AppUserRepository appUserRepository;
	
	CryptographyUtil cryptoUtil = new CryptographyUtil();
	
	@Autowired
	GlobalWhatsAppSMSRepository globalWhatsAppSMSRepository;
	
	@Autowired
	SaveSendWhatsAppSMSRepository saveSendWhatsAppSMSRepository;
	
	 //cache based on username and OPT MAX 8 
	 private static final Integer EXPIRE_MINS = 5;
	 private LoadingCache<String, Integer> otpCache;
	
	 public UserAuthenticationServiceImpl(){
		 super();
		 otpCache = CacheBuilder.newBuilder().expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES).build(new CacheLoader<String, Integer>() {
					 public Integer load(String key) {
						 return 0;
					 }
				 });
	 }
	 
	 //This method is used to push the opt number against Key. Rewrite the OTP if it exists using user id  as key
	@Override
	public int generateOTP(String key){
		Random random = new Random();
		int otp = 100000 + random.nextInt(900000);
		otpCache.put(key, otp);
		return otp;
	 }
	 
	//This method is used to return the OPT number against Key->Key values is username
	@Override
	public int getOtp(String key){ 
		try{
			return otpCache.get(key); 
		}catch (Exception e){
			return 0; 
		}
	 }
	
	//This method is used to clear the OTP catched already
	@Override
	public void clearOTP(String key){ 
		otpCache.invalidate(key);
	}
	
	@Override
	public void sendOtpMessage(String toMobile,String message,Integer schoolId ,Integer languageId) {

	
	  String apikey = null; 
	  String senderId = null; 
	  String type=null;
	  String mainUrl=null;
	  String route="";
	  String country="";
	  String templateID="";
	  String statusUrl="";
	  String response = null;
	  String user=null;
	  if(languageId == 2) {
		  type  = "uni" ;
	  }
	  SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
	  try{
		  if(schoolId==0) {
			  //schoolMasterModel.setSchoolid(1210000001); 
			  List<SMSAPiMasterModel> list1 = smsApiMasterRepository.findByMobNoId(1);
			  for(int i=0;i<list1.size();i++)	{
					if("1".equals(""+list1.get(i).getAdminPermission())){
						apikey = cryptoUtil.decrypt("azxopre", list1.get(i).getApikey());
						user = cryptoUtil.decrypt("mqpghdrz",list1.get(i).getUserName());
						//aPiName = list1.get(i).getApiname();
						//password = cry.decrypt("opqzakgj",list1.get(i).getPassword());
						//messageType= list1.get(i).getMsgType();
						//mobileId = list1.get(i).getMobNoId();
						mainUrl = cryptoUtil.decrypt("yrewpzgl",list1.get(i).getWebsiteUrl());
						statusUrl = cryptoUtil.decrypt("qbxozgl", list1.get(i).getStatusUrl());
						route = list1.get(i).getRoute();
						country = list1.get(i).getCountry(); 
						senderId = list1.get(i).getSenderId();
						templateID="1207162072752537804";
					}
				}
		  }
		  else {
			  schoolMasterModel.setSchoolid(schoolId);
			  List<FeeSendSmsNumbersMstrModel> list1 = userAuthenticationRepository.findBySchoolMasterModel(schoolMasterModel);
				
			  for(int i=0;i<list1.size();i++)	{
					if("1".equals(""+list1.get(i).getAdminPermission())){
						apikey = cryptoUtil.decrypt("azxopre", list1.get(i).getApikey());
						user = cryptoUtil.decrypt("mqpghdrz",list1.get(i).getUserName());
						//aPiName = list1.get(i).getApiname();
						//password = cry.decrypt("opqzakgj",list1.get(i).getPassword());
						//messageType= list1.get(i).getMsgType();
						//mobileId = list1.get(i).getMobNoId();
						mainUrl = cryptoUtil.decrypt("yrewpzgl",list1.get(i).getWebsiteUrl());
						statusUrl = cryptoUtil.decrypt("qbxozgl", list1.get(i).getStatusUrl());
						route = list1.get(i).getRoute();
						country = list1.get(i).getCountry(); 
						senderId = list1.get(i).getSenderId();
						templateID="1207162072752537804";
					}
				}
		  }
		
			URLConnection myURLConnection=null;
			URL myURL=null;
			BufferedReader reader=null;
			
			String encoded_message = URLEncoder.encode(message,"UTF-8");
			StringBuilder sbPostData= new StringBuilder(mainUrl);
	
			 if(country.equalsIgnoreCase("0")) {
				  sbPostData.append("authkey="+apikey); 
				  sbPostData.append("&mobiles="+toMobile);
				  sbPostData.append("&message="+encoded_message);
				  sbPostData.append("&route="+route);
				  sbPostData.append("&sender="+senderId);
				  sbPostData.append("&country="+country);
				  sbPostData.append("&unicode="+1);
				  sbPostData.append("&tid="+templateID);
				  mainUrl = sbPostData.toString();
				  myURL = new URL(mainUrl);
				  myURLConnection = myURL.openConnection();
				  myURLConnection.connect();
				  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
				  reader.close();
				  type ="4";
			
			  
				  sbPostData = new StringBuilder(statusUrl);
				  sbPostData.append("authkey="+apikey); 
				  sbPostData.append("&type=" + type);
				  mainUrl = sbPostData.toString();
				  myURL = new URL(mainUrl);
				  myURLConnection = myURL.openConnection();
				  myURLConnection.connect();
				  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
				  reader.close();
			  }
			  else{		
				  if(statusUrl.equalsIgnoreCase("")) {
					  URL url = new URL(""+mainUrl+"user="+user+"&pwd="+apikey+"&senderid="+senderId+"&mobileno="+toMobile+"&msgtext="+encoded_message+"&tid="+templateID+"");
					  HttpURLConnection conn = (HttpURLConnection)url.openConnection();
					  conn.setRequestMethod("GET");
					  conn.setDoOutput(true);
					  conn.setDoInput(true);
					  conn.setUseCaches(false);
					  conn.connect();
					  BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					  String line;
					  StringBuffer buffer = new StringBuffer();
					  while ((line = rd.readLine()) != null)
					  {
						  buffer.append(line).append("\n");
					  }
					  
					  rd.close();
					  conn.disconnect();
				  }
				  else{
					  StringBuilder sbPostData1 = new StringBuilder(mainUrl);
					  sbPostData1.append("user=" +  user);
					  sbPostData1.append("&apikey=" + apikey);
					  sbPostData1.append("&message=" + encoded_message);
					  sbPostData1.append("&mobile=" + toMobile);
					  sbPostData1.append("&senderid=" + senderId);
					  sbPostData1.append("&type=" + type);
					  sbPostData1.append("&tid=" + templateID);
					  String msgid = "";
					  mainUrl = sbPostData1.toString();
					  myURL = new URL(mainUrl);
					  myURLConnection = myURL.openConnection();
					  myURLConnection.connect();
					  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
						  while ((response = reader.readLine()) != null) {
							  msgid = response.trim();
						  }
						  reader.close();
                       System.out.println("msgid"+msgid);
					  sbPostData = new StringBuilder(statusUrl);
					  sbPostData.append("user=" +  user);
					  sbPostData.append("&apikey=" +  apikey);
					  sbPostData.append("&msgid=" + msgid);
					  mainUrl = sbPostData.toString();
						  myURL = new URL(mainUrl);
						  myURLConnection = myURL.openConnection();
						  myURLConnection.connect();
						  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
						  while ((response = reader.readLine()) != null) {
							  
						  }reader.close();

					  String balncurl="";
					  balncurl=statusUrl.replace("status", "balance");
					  sbPostData = new StringBuilder(balncurl);
					  sbPostData.append("user=" + user);
					  sbPostData.append("&apikey=" + apikey);
					  mainUrl = sbPostData.toString();
					  myURL = new URL(mainUrl);
					  myURLConnection = myURL.openConnection();
					  myURLConnection.connect();
					  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					  reader.close();
					 }			 
				 }
			}
			catch(Exception e) {
				e.printStackTrace();
		}
	}

	@Override
	public AppRoleBean getAdminDetails(String userName, String password, int schoolId) {
		AppRoleBean appRoleBean = null;
		try {
			password = md5(password);
			return studentMasterRepository.getAdminDetails(userName, password, schoolId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return appRoleBean;
	}
	
	@Override
	public List<StudentAdmissionBean> getStudentDetails(String userName, String password,Integer schoolId) {
	try {
			password=md5(password);
			return studentMasterRepository.getStudentDetails(userName,password,schoolId);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return new ArrayList<>();
	}
	
	@Override
	public Integer changePassword(String oldpassword ,String newPassword, Integer studentId, Integer schoolId,String userName) {
		AppRoleBean appRoleBean = null;
		Integer result=0;
		try {
			String EncryOldpassword=md5(oldpassword);
			String EncryNewpassword = md5(newPassword);
			return appUserRepository.changePassword(EncryNewpassword, userName, schoolId,EncryOldpassword);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
}

	@Override
	public Integer forgotPassword(String userName, String password, Integer schoolId) {
		AppRoleBean appRoleBean = null;
		try {
			String EncryNewpassword = md5(password);
			return appUserRepository.forgotPassword(userName, EncryNewpassword, schoolId);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static String md5(String input) {		
		String md5 = null;		
		if (null == input)
			return null;
		
		try {
			// Create MessageDigest object for MD5
			MessageDigest digest = MessageDigest.getInstance("MD5");			
			// Update input string in message digest
			digest.update(input.getBytes(), 0, input.length());			
			// Converts message digest value in base 16 (hex)
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return md5;
	}

	@Override
	public List<StudentAdmissionBean> findByUser(String userName, int schoolId) {
		return studentMasterRepository.findByUser(userName,schoolId);
	}

	@Override
	public void sendOtpMessageForRFID(String toMobile, String message, Integer schoolId, Integer languageId,
			String templateId) {
		  String apikey = null; 
		  String senderId = null; 
		  String type=null;
		  String mainUrl=null;
		  String route="";
		  String country="";
		  String templateID="";
		  String statusUrl="";
		  String response = null;
		  String user=null;
		  if(languageId == 2) {
			  type  = "uni" ;
		  }
		  SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		  try{
				/*
				 * if(schoolId==0) { //schoolMasterModel.setSchoolid(1210000001);
				 * List<SMSAPiMasterModel> list1 = smsApiMasterRepository.findByMobNoId(1);
				 * for(int i=0;i<list1.size();i++) {
				 * if("1".equals(""+list1.get(i).getAdminPermission())){ apikey =
				 * cryptoUtil.decrypt("azxopre", list1.get(i).getApikey()); user =
				 * cryptoUtil.decrypt("mqpghdrz",list1.get(i).getUserName()); //aPiName =
				 * list1.get(i).getApiname(); //password =
				 * cry.decrypt("opqzakgj",list1.get(i).getPassword()); //messageType=
				 * list1.get(i).getMsgType(); //mobileId = list1.get(i).getMobNoId(); mainUrl =
				 * cryptoUtil.decrypt("yrewpzgl",list1.get(i).getWebsiteUrl()); statusUrl =
				 * cryptoUtil.decrypt("qbxozgl", list1.get(i).getStatusUrl()); route =
				 * list1.get(i).getRoute(); country = list1.get(i).getCountry(); senderId =
				 * list1.get(i).getSenderId(); templateID=templateId; } } } else {
				 */
				  schoolMasterModel.setSchoolid(schoolId);
				  List<FeeSendSmsNumbersMstrModel> list1 = userAuthenticationRepository.findBySchoolMasterModel(schoolMasterModel);
					
				  for(int i=0;i<list1.size();i++)	{
						if("1".equals(""+list1.get(i).getAdminPermission())){
							apikey = cryptoUtil.decrypt("azxopre", list1.get(i).getApikey());
							user = cryptoUtil.decrypt("mqpghdrz",list1.get(i).getUserName());
							//aPiName = list1.get(i).getApiname();
							//password = cry.decrypt("opqzakgj",list1.get(i).getPassword());
							//messageType= list1.get(i).getMsgType();
							//mobileId = list1.get(i).getMobNoId();
							mainUrl = cryptoUtil.decrypt("yrewpzgl",list1.get(i).getWebsiteUrl());
							statusUrl = cryptoUtil.decrypt("qbxozgl", list1.get(i).getStatusUrl());
							route = list1.get(i).getRoute();
							country = list1.get(i).getCountry(); 
							senderId = list1.get(i).getSenderId();
							templateID=templateId;
						}
					}
			 // }
			
				URLConnection myURLConnection=null;
				URL myURL=null;
				BufferedReader reader=null;
				
				String encoded_message = URLEncoder.encode(message,"UTF-8");
				StringBuilder sbPostData= new StringBuilder(mainUrl);
		
				 if(country.equalsIgnoreCase("0")) {
					  sbPostData.append("authkey="+apikey); 
					  sbPostData.append("&mobiles="+toMobile);
					  sbPostData.append("&message="+encoded_message);
					  sbPostData.append("&route="+route);
					  sbPostData.append("&sender="+senderId);
					  sbPostData.append("&country="+country);
					  sbPostData.append("&unicode="+1);
					  sbPostData.append("&tid="+templateID);
					  mainUrl = sbPostData.toString();
					  myURL = new URL(mainUrl);
					  myURLConnection = myURL.openConnection();
					  myURLConnection.connect();
					  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					  reader.close();
					  type ="4";
				
				  
					  sbPostData = new StringBuilder(statusUrl);
					  sbPostData.append("authkey="+apikey); 
					  sbPostData.append("&type=" + type);
					  mainUrl = sbPostData.toString();
					  myURL = new URL(mainUrl);
					  myURLConnection = myURL.openConnection();
					  myURLConnection.connect();
					  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					  reader.close();
				  }
				  else{		
					  if(statusUrl.equalsIgnoreCase("")) {
						  URL url = new URL(""+mainUrl+"user="+user+"&pwd="+apikey+"&senderid="+senderId+"&mobileno="+toMobile+"&msgtext="+encoded_message+"&tid="+templateID+"");
						  HttpURLConnection conn = (HttpURLConnection)url.openConnection();
						  conn.setRequestMethod("GET");
						  conn.setDoOutput(true);
						  conn.setDoInput(true);
						  conn.setUseCaches(false);
						  conn.connect();
						  BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						  String line;
						  StringBuffer buffer = new StringBuffer();
						  while ((line = rd.readLine()) != null)
						  {
							  buffer.append(line).append("\n");
						  }
						  
						  rd.close();
						  conn.disconnect();
					  }
					  else{
						  StringBuilder sbPostData1 = new StringBuilder(mainUrl);
						  sbPostData1.append("user=" +  user);
						  sbPostData1.append("&apikey=" + apikey);
						  sbPostData1.append("&message=" + encoded_message);
						  sbPostData1.append("&mobile=" + toMobile);
						  sbPostData1.append("&senderid=" + senderId);
						  sbPostData1.append("&type=" + type);
						  sbPostData1.append("&tid=" + templateID);
						  String msgid = "";
						  mainUrl = sbPostData1.toString();
						  myURL = new URL(mainUrl);
						  myURLConnection = myURL.openConnection();
						  myURLConnection.connect();
						  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
							  while ((response = reader.readLine()) != null) {
								  msgid = response.trim();
							  }
							  reader.close();
	                       System.out.println("msgid"+msgid);
						  sbPostData = new StringBuilder(statusUrl);
						  sbPostData.append("user=" +  user);
						  sbPostData.append("&apikey=" +  apikey);
						  sbPostData.append("&msgid=" + msgid);
						  mainUrl = sbPostData.toString();
							  myURL = new URL(mainUrl);
							  myURLConnection = myURL.openConnection();
							  myURLConnection.connect();
							  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
							  while ((response = reader.readLine()) != null) {
								  
							  }reader.close();

						  String balncurl="";
						  balncurl=statusUrl.replace("status", "balance");
						  sbPostData = new StringBuilder(balncurl);
						  sbPostData.append("user=" + user);
						  sbPostData.append("&apikey=" + apikey);
						  mainUrl = sbPostData.toString();
						  myURL = new URL(mainUrl);
						  myURLConnection = myURL.openConnection();
						  myURLConnection.connect();
						  reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
						  reader.close();
						 }			 
					 }
				}
				catch(Exception e) {
					e.printStackTrace();
			}
		}

	@Override
	public void sendWhatsAppSMS(String username, String message, Integer schoolId,int messagetype) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		String instanceId,accessToken;
		List<SchoolWiseWhatsAppSMSModel> schoolWiseList=globalWhatsAppSMSRepository.getSchoolWiseSetting(schoolId);
		if(schoolWiseList.size()>0) {
			instanceId=schoolWiseList.get(0).getInstanceId();
			accessToken=schoolWiseList.get(0).getAccessToken();
		}else {
			GlobalWhatsAppSMSModel settingList=globalWhatsAppSMSRepository.getOne(1);
			instanceId=settingList.getInstanceId();
			accessToken=settingList.getAccessToken();
		}
		String newMsg[]=message.split(" ");
		String NewMessage="+";
		for (String str : newMsg) {
			NewMessage=NewMessage.concat("+"+str);
		}
		WhatsAppMessageBean whatsAppMessageBean = new WhatsAppMessageBean();
		whatsAppMessageBean.setAccessToken(accessToken);
		whatsAppMessageBean.setInstanceId(instanceId);
		whatsAppMessageBean.setMessage(message);
		whatsAppMessageBean.setEncryptedmessage(NewMessage);
		whatsAppMessageBean.setIsMediaType(0);
		whatsAppMessageBean.setTypeFlag("0");
		whatsAppMessageBean.setMessageType(String.valueOf(messagetype));
		whatsAppMessageBean.setRenewId(0);
		whatsAppMessageBean.setSchoolId(schoolId);
		whatsAppMessageBean.setMobileNo(username);

		try {
			ObjectMapper Obj = new ObjectMapper();
			String serverUrl="13.233.210.164:8073";  
//			String serverUrl="localhost:8073";
			String generateMastersUrl="http://"+serverUrl+"/sendWhatsAppMessageNotification?isSendWhatsAppMsg=1";
			URL url = new URL(generateMastersUrl);
//			http://localhost:8083/sendWhatsAppMessageNotification
			HttpURLConnection postConnection = (HttpURLConnection) url.openConnection();
			postConnection.setRequestMethod("POST");
			postConnection.setRequestProperty("Content-Type", "application/json");
			postConnection.setDoOutput(true);
			postConnection.connect();
			OutputStream os = postConnection.getOutputStream();
			String jsonStr = StringUtils.EMPTY;
			try {
				jsonStr = Obj.writeValueAsString(whatsAppMessageBean);
//				System.out.println(jsonStr);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			os.write(jsonStr.getBytes());
			os.flush();
			os.close();
			int responseCode = postConnection.getResponseCode();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

	}



	
	
	
	
}
