package com.ingenio.admission.service.impl;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.BannerImages;
import com.ingenio.admission.bean.CheckBalanceBean;
import com.ingenio.admission.bean.CommonUtilityBean;
import com.ingenio.admission.bean.FrontPageImagesBean;
import com.ingenio.admission.bean.SchoolMappingBean;
import com.ingenio.admission.bean.StandardYearBean;
import com.ingenio.admission.bean.UploadFileBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.AttachmentMasterModel;
import com.ingenio.admission.model.BonafideTCImageModel;
import com.ingenio.admission.model.CasteMasterModel;
import com.ingenio.admission.model.CategoryMasterModel;
import com.ingenio.admission.model.DesignAdmiFormTabsModel;
import com.ingenio.admission.model.DivisionMasterModel;
import com.ingenio.admission.model.DynamicAcademicMonthsModel;
import com.ingenio.admission.model.FeeSendSmsNumbersMstrModel;
import com.ingenio.admission.model.GRBookNameModel;
import com.ingenio.admission.model.MinorityMasterModel;
import com.ingenio.admission.model.ReligionMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StandardMasterModel;
import com.ingenio.admission.model.YearMasterModel;
import com.ingenio.admission.repository.AttachmentMasterRepository;
import com.ingenio.admission.repository.BonafideTCImgRepository;
import com.ingenio.admission.repository.CasteMasterRepository;
import com.ingenio.admission.repository.CategoryMasterRepository;
import com.ingenio.admission.repository.DesignAdmiFormTabsRepository;
import com.ingenio.admission.repository.DivisionMasterRepository;
import com.ingenio.admission.repository.DynamicAcademicMonthsRepository;
import com.ingenio.admission.repository.GrBookRepository;
import com.ingenio.admission.repository.ImageRepository;
import com.ingenio.admission.repository.MinorityMasterRepository;
import com.ingenio.admission.repository.ReligionMasterRepository;
import com.ingenio.admission.repository.SansthaLogoRepository;
import com.ingenio.admission.repository.SchoolLogoRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.repository.SmsApiMasterRepository;
import com.ingenio.admission.repository.StandardMasterRepository;
import com.ingenio.admission.repository.StudentMasterRepository;
import com.ingenio.admission.repository.SubjectMasterRepository;
import com.ingenio.admission.repository.UserAuthenticationRepository;
import com.ingenio.admission.repository.UserRepository;
import com.ingenio.admission.repository.YearMasterRepository;
import com.ingenio.admission.service.CommonUtilityService;
import com.ingenio.admission.util.CryptographyUtil;
import com.ingenio.admission.util.Utillity;

@Component

//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")


//  @PropertySource("classpath:messages_en.properties")
//  
//  @PropertySource("classpath:messages_mr.properties")
//  
//  @PropertySource("classpath:messages_hn.properties")
 
public class CommonUtilittyServiceImpl implements CommonUtilityService {

	@Autowired 
	private YearMasterRepository yearMasterRepository;
	
	@Autowired 
	private StandardMasterRepository standardMasterRepository;
		
	@Autowired 
	private CategoryMasterRepository categoryMasterRepository;
	
	@Autowired 
	private CasteMasterRepository casteMasterRepository;
	
	@Autowired 
	private MinorityMasterRepository minorityMasterRepository;
	
	@Autowired 
	private ReligionMasterRepository religionMasterRepository;
	
	@Autowired 
	private DivisionMasterRepository divisionMasterRepository;
	
	@Autowired 
	private GrBookRepository grBookRepository;
	
	@Autowired 
	private AttachmentMasterRepository attachmentMasterRepository;
	
	@Autowired 
	private BonafideTCImgRepository bonafideTCImgRepository;
	
	@Autowired
	private DesignAdmiFormTabsRepository designAdmiFormTabsRepository;
	
	@Autowired
	private SubjectMasterRepository subjectMasterRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private DynamicAcademicMonthsRepository dynamicAcademicMonthsRepository;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	UserAuthenticationRepository userAuthenticationRepository;
	
	@Autowired
	StudentMasterRepository studentMasterRepository;
	
	@Autowired
	SmsApiMasterRepository smsApiMasterRepository;
	
    @Autowired
    private Environment env;
    
    @Autowired
    ImageRepository imageRepository;
    
    @Autowired 
	private RestTemplate restTemplate;
    
    @Autowired
    SansthaLogoRepository sansthaLogoRepository;
    
    @Autowired
    SchoolLogoRepository schoolLogoRepository;
    
    @Autowired
    Utillity utillity;
    
    private static final String TYPE_2 = "4";

	private static final String MAIN_URL = "yrewpzgl";

	private static final String DEFAULT_COUNTRY = "0";

	private static final String MSGTEXT = "&msgtext=";

	private static final String UNICODE = "&unicode=";

	private static final String COUNTRY2 = "&country=";

	private static final String SENDER = "&sender=";

	private static final String ROUTE2 = "&route=";

	private static final String MOBILES = "&mobiles=";

	private static final String AUTHKEY = "authkey=";

	private static final String MOBILENO = "&mobileno=";

	private static final String SMSPW = "&pwd=";

	private static final String TYPE2 = "&type=";

	private static final String SENDERID2 = "&senderid=";

	private static final String MOBILE = "&mobile=";

	private static final String MESSAGE2 = "&message=";

	private static final String MSGID2 = "&msgid=";

	private static final String DOWNLOAD_MESSAGE = "Please Download Mobile Application and Stay updated Daily";

	private static final String ADMIN_PERMISSION = "1";

	private static final String APIKEY2 = "&apikey=";

	private static final String USERKEY = "user=";

	private static final String BALANCE = "balance";

	private static final String STATUS = "status";

	private static final String API_KEY = "azxopre";

	private static final String USER = "mqpghdrz";

	private static final String STATUS_URL = "qbxozgl";
	
	CryptographyUtil cry= new CryptographyUtil();
	
	@Override
	public List<YearMasterModel> getAcademicYearCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return yearMasterRepository.findBySchoolMasterModelOrderByYear(schoolMasterModel);
		
	}

	@Override
	public List<StandardMasterModel> getStandardCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return standardMasterRepository.findBySchoolMasterModelOrderByStandardPriority(schoolMasterModel);
	}

	@Override
	public List<CategoryMasterModel> getCategoryCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return categoryMasterRepository.findBySchoolMasterModelOrderByCategoryId(schoolMasterModel);
	
	}
	
	
	@Override
	public List<CasteMasterModel> getCasteCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return casteMasterRepository.findBySchoolMasterModelOrderByCasteId(schoolMasterModel);
	
	}


	@Override
	public List<MinorityMasterModel> getMinorityCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return minorityMasterRepository.findBySchoolIdOrderByMinorityId(schoolMasterModel);
	
	}
	
	
	@Override
	public List<ReligionMasterModel> getReligionCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return religionMasterRepository.findBySchoolMasterModelOrderByReligionId(schoolMasterModel);
		} 
	
	
	@Override
	public List<DivisionMasterModel> getDivisionCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return divisionMasterRepository.findBySchoolMasterModelOrderByDivisionId(schoolMasterModel);
	
	}
	
	@Override
	public List<GRBookNameModel> getGRCombo(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return grBookRepository.findBySchoolMasterModelOrderByGrBookId(schoolMasterModel);
	
	}

	@Override
	public List<AttachmentMasterModel> getStudentAttachments(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return attachmentMasterRepository.findBySchoolMasterModelOrderByAttachmentMasterId(schoolMasterModel);
	}
	
	@Override
	public List<DesignAdmiFormTabsModel> getTabsCombo(Integer schoolId) {
		return designAdmiFormTabsRepository.getTabsCombo(schoolId);
	}

	@Override
	public List<BonafideTCImageModel> getLetterHead(Integer schoolId) {
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		return bonafideTCImgRepository.findLetterHeadImage(schoolId);
	}

	@Override
	public List<AdmissionSettingBean> getAssignedSubjects(Integer schoolId, Integer yearId, Integer standardId) {
		return subjectMasterRepository.findAssignedSubjects(yearId,standardId,schoolId);
	}

	@Override
	public List<UserDetailsBean> getUserRoleAndSchoolDetails(String username,String language) {
		List<UserDetailsBean> userBean =  userRepository.getUserRoleAndSchoolDetails(username);
		try {
			List<Integer> userList = new ArrayList<>();
			if(CollectionUtils.isNotEmpty(userBean)) {
				for(int i=0;i<userBean.size();i++) {
						Integer currentYear = getCurrentYear(userBean.get(i).getSchoolId());
						userBean.get(i).setCurrentYear(currentYear);
						YearMasterModel yearMasterModel = yearMasterRepository.findByYearId(currentYear);
						if(yearMasterModel !=null) {
							userBean.get(i).setCurrentYearText(yearMasterModel.getYear());
						}
						//userBean.get(i).setSchoolLogoUrl(ConfigurationProperties.schoolLogo+userBean.get(i).getSchoolId()+".jpg");
						
						userBean.get(i).setSchoolLogoUrl(schoolLogoRepository.getSchoolLogoPath(userBean.get(i).getSchoolId()));

						if(userBean.get(i).getRoleName().equals("ROLE_SANSTHAOFFICER") ) {
							List<UserDetailsBean> userBeanSanstaOfficer =  userRepository.getUserRoleAndSansthaOfficerDetails(username,userBean.get(i).getUserId());
							userBean.set(i,userBeanSanstaOfficer.get(0));
							userBean.get(i).setSchoolName(cry.decrypt("qwertysansthaname", StringEscapeUtils.escapeJava(userBean.get(i).getSchoolName().replaceAll("[\\r\\n]+", ""))));
							userBean.get(i).setRoleName(getRoles(userBean.get(i).getRoleName()));
							userBean.get(i).setRoleNameForDisplay(getRoles(userBean.get(i).getRoleName()));
							//userBean.get(i).setSchoolLogoUrl(ConfigurationProperties.sansthaLogo+userBean.get(i).getSchoolId()+".jpg");
							userBean.get(i).setSchoolLogoUrl(sansthaLogoRepository.getSansthaLogoPath(userBeanSanstaOfficer.get(i).getSchoolSansthaKey()));
							
							userBean.get(i).setCurrentYearText("2019-2020");
							userBean.get(i).setRollColor("#EC5551");
							userBean.get(i).setCurrentYear(0);
						}
						else if(userBean.get(i).getRoleName().equals("ROLE_SUPEROFFICER")) {
							List<UserDetailsBean>  userBeanSuperOfficer =  userRepository.getUserRoleAndSuperOfficerDetails(username,userBean.get(i).getUserId());
							userBean.set(i,userBeanSuperOfficer.get(0));
							userBean.get(i).setSchoolName(cry.decrypt("qwertysansthaname", StringEscapeUtils.escapeJava(userBean.get(i).getSchoolName().replaceAll("[\\r\\n]+", ""))));
							userBean.get(i).setRoleName(getRoles(userBean.get(i).getRoleName()));
							userBean.get(i).setRoleNameForDisplay(getRoles(userBean.get(i).getRoleName()));
							//userBean.get(i).setSchoolLogoUrl(ConfigurationProperties.sansthaLogo+userBean.get(i).getSchoolId()+".jpg");
							userBean.get(i).setSchoolLogoUrl(sansthaLogoRepository.getSansthaLogoPath(userBeanSuperOfficer.get(i).getSchoolSansthaKey()));
							userBean.get(i).setCurrentYearText("2019-2020");
							userBean.get(i).setCurrentYear(0);
							userBean.get(i).setRollColor("#EC5551");
							userList.add(userBean.get(i).getUserId());
						}
						else if(userBean.get(i).getRoleName().equalsIgnoreCase("ROLE_PARENT1") || userBean.get(i).getRoleName().equalsIgnoreCase("ROLE_PARENT2") || userBean.get(i).getRoleName().equalsIgnoreCase("ROLE_STUDENT")) {
							userBean.get(i).setRollColor("#f5a623");
						}
						else if(userBean.get(i).getRoleName().equalsIgnoreCase("ROLE_TEACHINGSTAFF") || userBean.get(i).getRoleName().equalsIgnoreCase("ROLE_NONTEACHINGSTAFF")) {
							userBean.get(i).setRollColor("#1478f6") ; 
						}
						else if(userBean.get(i).getRoleName().equalsIgnoreCase("ROLE_PRINCIPAL")) {
							userBean.get(i).setRollColor("#50B051");
						}
						else {
							try {
								userBean.get(i).setSchoolName(cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(userBean.get(i).getSchoolName().replaceAll("[\\r\\n]+", ""))));
								userBean.get(i).setRoleName(getRoles(userBean.get(i).getRoleName()));
								userBean.get(i).setRoleNameForDisplay(getRoles(userBean.get(i).getRoleName()));
								userList.add(userBean.get(i).getUserId());
							
							} catch (Exception e) {
								userBean.get(i).setSchoolName("School");
								userBean.get(i).setRoleName(getRoles(userBean.get(i).getRoleName()));
								userBean.get(i).setRoleNameForDisplay(getRoles(userBean.get(i).getRoleName()));
							}
						}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(CollectionUtils.isEmpty(userBean) && (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			userBean.stream().forEach( role -> { 
					String roleName = env.getProperty(language+"."+role.getRoleNameForDisplay().replaceAll("\\s+",""));
					if(StringUtils.isNotEmpty(roleName)) {
						role.setRoleNameForDisplay(roleName);
					}
			});
		}
		return userBean;
	}

	private String getRoles(String userName) {
		switch(userName) {
			case "ROLE_PRINCIPAL" :
				return "Principal";
			case "ROLE_ADMIN" :
				return "Admin";
			case "ROLE_NONTEACHINGSTAFF" :
				return "Non Teaching Staff";
			case "ROLE_TEACHINGSTAFF" :
				return "Teacher";
			case "ROLE_PARENT1" :
				return "Father";
			case "ROLE_PARENT2" :
				return "Mother";
			case "ROLE_SANSTHAOFFICER" :
				return "Sanstha Officer";
			case "ROLE_SUPEROFFICER" :
				return "Super Officer";
			case "ROLE_STUDENT" :
				return "Student";
			case "ROLE_SUPERADMIN" :
				return "Super Admin";
			case "ROLE_USER" :
				return "User";
			default: 
				return userName;
		}
		
	}


	public int getCurrentYear(Integer schoolId) {
		int globleYearId = 0;
		try {
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DynamicAcademicMonthsModel> dynamicAcadYearBean = dynamicAcademicMonthsRepository.findBySchoolMasterModel(schoolMasterModel);
			if(CollectionUtils.isNotEmpty(dynamicAcadYearBean)) {
				String localDynamicAcadSdate=dynamicAcadYearBean.get(0).getFromMonth();
				String globalDynamicAcadSdate=localDynamicAcadSdate.split("-")[1];
				
				LocalDateTime now = LocalDateTime.now();
				int year = now.getYear();
				int month =(now.getMonthValue()); 
				
				
				String currentAcYr="";
				if (month >= Integer.parseInt(globalDynamicAcadSdate) && month <= 12) {
					year++;
					currentAcYr = now.getYear() + "-" + year;
				} else {
					year--;
					currentAcYr = year + "-" + now.getYear();
				}
				
				List<YearMasterModel> list = yearMasterRepository.findBySchoolMasterModelOrderByYear(schoolMasterModel);
				for(int i=0;i<list.size();i++) {
					if (list.get(i).getYear().equals(currentAcYr)) {
						globleYearId = list.get(i).getYearId();
					}
				}
			}
			
		} catch (NumberFormatException e) {
			
			e.printStackTrace();
		}
		
		return globleYearId;
	}

	@Override
	public List<CommonUtilityBean> getLetterHeadImg(Integer schoolId) {
	/*
	byte[] newbytes1 = null;
	String base64Image1 = "";
	String imageName1 = "Bonafied_Image";
	String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
	System.out.println("C:/ePrashasan/MainDB/cms/Certificates/Student_Bonafied_Image/" + schoolKey);
	File directory = new File("C:/ePrashasan/MainDB/cms/Certificates/Student_Bonafied_Image/" + schoolKey);
	List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
	File[] filenamelist = directory.listFiles();
	if (filenamelist != null) {
		for (File f : filenamelist ) {
			String extension = FilenameUtils.getExtension("" + f);
			String fileNameWithOutExtension = FilenameUtils.removeExtension(f.getName());
			if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png")||extension.equalsIgnoreCase("jpeg")) {
				if (fileNameWithOutExtension.equalsIgnoreCase(imageName1)) {
					newbytes1 = null;
					try {
						InputStream inputStream = new FileInputStream(f);
						newbytes1 = IOUtils.toByteArray(inputStream);
						base64Image1 = Base64.getEncoder().encodeToString(newbytes1);
						CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
						commonUtilityBean.setLetterHeadImg(base64Image1);
						commonUtilityBean.setFlag("1");
						commonUtilityBeanList.add(commonUtilityBean);
					} catch (Exception e) {
						e.printStackTrace();
						return new ArrayList<>();
					}
				}
			}
		}
	}
	*/
	List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
	String filePath = imageRepository.getFilePath(schoolId);
	if(StringUtils.isNotEmpty(filePath)) {
		try {
		 	String getUri= env.getProperty("awsDownloadUrl");
			URI uri = new URI(getUri);
			UploadFileBean bean = new UploadFileBean();
			bean.setFilePath(filePath);
			UploadFileBean image = restTemplate.postForObject(uri, bean,UploadFileBean.class);
	    	byte[] target =  image.getFileArr();
	    	String base64Image1 = "";
	    	base64Image1 = Base64.getEncoder().encodeToString(target);
	    	CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
			commonUtilityBean.setLetterHeadImg(base64Image1);
			commonUtilityBean.setFlag("1");
			commonUtilityBeanList.add(commonUtilityBean);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
	return commonUtilityBeanList;
}

	@Override
	public List<UserDetailsBean> checkExistingUser(String username, Integer schoolId) {
		List<UserDetailsBean> userBean =  userRepository.checkExistingUser(username,schoolId);
		return userBean;
	}

	@Override
	public String getAcademicYear(String schoolId) {
		String currentYear="0";
		try {
			 currentYear = String.valueOf(getCurrentYear(Integer.valueOf(schoolId)));
		}catch (Exception e) {
			e.printStackTrace();
			return currentYear;
		}
		return currentYear;
	}

	@Override
	public List<CommonUtilityBean> getStudentImageSignature(Integer schoolId, String regNo,Integer type) {
		//String exePath = HttpClientUtil.getExePathDB();
		String exePath = "C:/ePrashasan/MainDB/";
		byte[] newbytes1 = null;
		String base64Image1 = "";
		if(type==2) {
			regNo="sign_"+regNo;
		}
		/*
		String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
		System.out.println(exePath +"/"+schoolKey +"/"+ schoolId);
		File directory = new File(exePath+"/cms/IcardImages/"+schoolKey+"/"+schoolId);
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		File[] filenamelist = directory.listFiles();
		if (filenamelist != null) {
			for (File f : filenamelist ) {
				String extension = FilenameUtils.getExtension("" + f);
				String fileNameWithOutExtension = FilenameUtils.removeExtension(f.getName());
				if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png")||extension.equalsIgnoreCase("jpeg")) {
					if (fileNameWithOutExtension.equalsIgnoreCase(regNo.toString())) {
						newbytes1 = null;
						try {
							InputStream inputStream = new FileInputStream(f);
							newbytes1 = IOUtils.toByteArray(inputStream);
							base64Image1 = Base64.getEncoder().encodeToString(newbytes1);
							CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
							commonUtilityBean.setLetterHeadImg(base64Image1);
							commonUtilityBean.setFlag("1");
							commonUtilityBeanList.add(commonUtilityBean);
						} catch (Exception e) {
							e.printStackTrace();
							return new ArrayList<>();
						}
					}
				}
			}
		}
		*/
		
		//
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		String filePath = imageRepository.getStudentImg(schoolId,regNo);
		if(!filePath.equals("")) {
			try {
			 	String getUri= env.getProperty("awsDownloadUrl");
				URI uri = new URI(getUri);
				UploadFileBean bean = new UploadFileBean();
				bean.setFilePath(filePath);
				UploadFileBean image = restTemplate.postForObject(uri, bean,UploadFileBean.class);
		    	byte[] target =  image.getFileArr();
		    	String base64Image = "";
		    	base64Image = Base64.getEncoder().encodeToString(target);
		    	CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
				commonUtilityBean.setLetterHeadImg(base64Image);
				commonUtilityBean.setFlag("1");
				commonUtilityBeanList.add(commonUtilityBean);
			}
			catch (Exception e) {
				e.printStackTrace();
				return new ArrayList<>();
			}
		}
			return commonUtilityBeanList;
	}

	@Override
	public StandardYearBean getStandardYear(String studentId) {
		List<StandardYearBean> userBean =  userRepository.getStandardYear(Integer.parseInt(studentId));
		return userBean.get(0);
	}


	@Override
	public List<CheckBalanceBean> getSmsBalance(Integer schoolId) {
		CheckBalanceBean checkBalanceBean = new CheckBalanceBean();
		List<CheckBalanceBean> smsBalanceList = new ArrayList<CheckBalanceBean>();		
		checkBalanceBean.setSendingFlag(1);
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		try {
			schoolMasterModel.setSchoolid(schoolId);
			String apikey = null;
			String statusUrl = "";
			String response = null;
			String user = null;
			String balance = DEFAULT_COUNTRY;
			  List<FeeSendSmsNumbersMstrModel> list1 = userAuthenticationRepository.findBySchoolMasterModel(schoolMasterModel);
			for (int i = 0; i < list1.size(); i++) {
				if (ADMIN_PERMISSION.equals("" + list1.get(i).getAdminPermission())) {
					apikey = cry.decrypt(API_KEY, list1.get(i).getApikey());
					user = cry.decrypt(USER, list1.get(i).getUserName());
					statusUrl = cry.decrypt(STATUS_URL, list1.get(i).getStatusUrl());
				}
			}
			URLConnection myURLConnection = null;
			URL myURL = null;
			BufferedReader reader = null;
			String balncurl = statusUrl.replace(STATUS, BALANCE);
			StringBuilder sbPostData = new StringBuilder(balncurl);
			sbPostData.append(USERKEY + user);
			sbPostData.append(APIKEY2 + apikey);
			String mainUrl = sbPostData.toString();
			myURL = new URL(mainUrl);
			myURLConnection = myURL.openConnection();
			myURLConnection.connect();
			reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
			while ((response = reader.readLine()) != null) {
				balance = response;
			}
			reader.close();
			String smsarray[] = balance.split(":");
			checkBalanceBean.setBalance(smsarray[1]);
			smsBalanceList.add(checkBalanceBean);
			return smsBalanceList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return smsBalanceList;
	}

	
	@Override
	public FrontPageImagesBean getAllBannerList(Integer schoolId) {
		// TODO Auto-generated method stub
		List<BannerImages> schoolwiseBannerList =  userRepository.getAllBannerList(schoolId);
		schoolwiseBannerList.parallelStream().forEach( s-> {
			 s.setImagePath(utillity.getS3PreSignedUrl(s.getImagePath()));
		 });
		List<BannerImages> commonImagesList =  userRepository.getAllCommonImagesList();
		commonImagesList.parallelStream().forEach( s-> {
			 s.setImagePath(utillity.getS3PreSignedUrl(s.getImagePath()));
		 });
		FrontPageImagesBean frontPageImagesBean =new FrontPageImagesBean();
		frontPageImagesBean.setSchoolWiseImagesList(schoolwiseBannerList);
		frontPageImagesBean.setCommonImagesList(commonImagesList);
		return frontPageImagesBean;
	}

//	@Override
//	public List<SchoolMappingBean> getAllSchoolMappingList(Integer schoolId) {
//		List<SchoolMappingBean> schoolMappingList =  userRepository.getAllSchoolMappingList(schoolId);
//		schoolMappingList.stream().forEach(school ->{
//			try {
//				String schoolName  = cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(school.getSchoolName()).replaceAll("[\\r\\n]+", ""));
//				school.setSchoolName(schoolName);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
//		});
//		return schoolMappingList;
//	}
	
	@Override
	public List<SchoolMappingBean> getAllSchoolMappingList(Integer schoolId, Integer onlineAdmissionFlag) {
//		List<SchoolMappingBean> schoolMappingList =  userRepository.getAllSchoolMappingList(schoolId);
		
		List<SchoolMappingBean> schoolMappingList = null;
		
//		if((onlineAdmissionFlag == 0) || (onlineAdmissionFlag == null))
		if((onlineAdmissionFlag == null) || (onlineAdmissionFlag == 0))
		{
			schoolMappingList =  userRepository.getAllSchoolMappingList(schoolId, 0);
		}
		else
		{
			schoolMappingList =  userRepository.getAllSchoolMappingList(schoolId, 1);
		}
		
		schoolMappingList.stream().forEach(school ->{
			try {
				String schoolName  = cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(school.getSchoolName()).replaceAll("[\\r\\n]+", ""));
				school.setSchoolName(schoolName);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		});
		return schoolMappingList;
	}
	
}


			





//try {
//List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
//String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
//File file = new File("C:/ePrashasan/MainDB/cms/Certificates/Student_Bonafied_Image/" + schoolKey + ".");
//
////File file = new File("C:\\ePrashasan\\MainDB\\cms\\Certificates\\Student_Bonafied_Image\\"+schoolId+".png");
//if(file.exists()) { 
//    FileInputStream fis = new FileInputStream(file);
//    ByteArrayOutputStream bos = new ByteArrayOutputStream();
//    byte[] buf = new byte[1024];
//    try {
//        for (int readNum; (readNum = fis.read(buf)) != -1;) {
//            //Writes to this byte array output stream
//            bos.write(buf, 0, readNum); 
//        }
//    } catch (IOException ex) {
//    	ex.printStackTrace();
//    }
//    byte[] bytes = bos.toByteArray();
//    byte[] encodeBase64 = Base64.encodeBase64(bytes);
//    String base64Encoded = new String(encodeBase64, "UTF-8");
//	CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
//	commonUtilityBean.setLetterHeadImg(base64Encoded);
//	commonUtilityBean.setFlag("1");
//	commonUtilityBeanList.add(commonUtilityBean);
//	return commonUtilityBeanList;
//}else {
//	return null;
//}
//}catch(Exception e) {
//e.printStackTrace();
//return new ArrayList<>();
//}
//}
