package com.ingenio.admission.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.model.AppUserRoleModel;
import com.ingenio.admission.model.DesignAdmiDynamicFieldValueModel;
import com.ingenio.admission.model.DesignAdmiFormFieldsModel;
import com.ingenio.admission.model.DesignAdmiFormLegendModel;
import com.ingenio.admission.model.DesignAdmiFormTabsModel;
import com.ingenio.admission.model.DesignAdmiFormTabsMultiModel;
import com.ingenio.admission.model.DynamicAdmissionFiledsModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.repository.AppUserRepository;
import com.ingenio.admission.repository.DesignAdmiFormDynamicFieldValueRepository;
import com.ingenio.admission.repository.DesignAdmiFormFieldsRepository;
import com.ingenio.admission.repository.DesignAdmiFormLegendRepository;
import com.ingenio.admission.repository.DesignAdmiFormTabsRepository;
import com.ingenio.admission.repository.DesignAdmissionFormTabsMultiRepository;
import com.ingenio.admission.repository.DynamicAdmissionFiledsRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.repository.impl.DynamicAdmissionFiledsRepositoryImpl;
import com.ingenio.admission.service.AdmissionSettingService;
import com.ingenio.admission.util.UTFConversionUtil;

@Component
public class AdmissionSettingServiceImpl implements AdmissionSettingService {

	@Autowired
	private DesignAdmissionFormTabsMultiRepository designAdmissionFormTabsMultiRepository;
	
	@Autowired
	private DesignAdmiFormDynamicFieldValueRepository designAdmiFormDynamicFieldValueRepository;
	
	@Autowired
	private DesignAdmiFormTabsRepository designAdmiFormTabsRepository;
	
	@Autowired
	private DesignAdmiFormLegendRepository designAdmiFormLegendRepository;
	
	@Autowired
	private DynamicAdmissionFiledsRepository dynamicAdmissionFiledsRepository;
	
	@Autowired
	DynamicAdmissionFiledsRepositoryImpl dynamicAdmissionFiledsRepositoryImpl;
	
	@Autowired
	DesignAdmiFormFieldsRepository designAdmiFormFieldsRepository;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	AppUserRepository appUserRepository;
	
	@Override
	public List<AdmissionSettingBean> getTabsName(Integer schoolId) {
		List<AdmissionSettingBean> AdmissionSettingBeanList = new ArrayList<AdmissionSettingBean>();
		try {
			SchoolMasterModel schoolMasterModel=new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DesignAdmiFormTabsModel> tabsList=designAdmiFormTabsRepository.findBySchoolMasterModel(schoolMasterModel);
			if(CollectionUtils.isNotEmpty(tabsList)) {
				for (int i = 0; i < tabsList.size(); i++) {
					AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean(); 
					admissionSettingBean.setTabsId(tabsList.get(i).getTabsId());
					admissionSettingBean.setTabsName(tabsList.get(i).getTabsName());
					admissionSettingBean.setIsTabDisplayOrNot(tabsList.get(i).getIsTabDisplay());
					AdmissionSettingBeanList.add(admissionSettingBean);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return AdmissionSettingBeanList;
	}

	@Override
	public Integer updateTabsName(String tabName,Integer schoolId,Integer tabId) {
		try {
			return designAdmiFormTabsRepository.updateTabsName(tabName,tabId,schoolId);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	@Transactional 
	public List<AdmissionSettingBean> getAllFeilds(Integer tabId, Integer schoolId,Integer legendId) {
		List<AdmissionSettingBean> AdmissionSettingBeanList = new ArrayList<AdmissionSettingBean>();
		try {
			List<DesignAdmiFormFieldsModel> staticFieldList=designAdmiFormFieldsRepository.getStaticTabsForAdmissionForm(schoolId);
			List<DesignAdmiFormTabsMultiModel> tabAndLegendWiseList=designAdmissionFormTabsMultiRepository.getFieldTabsAndLegendWise(tabId,schoolId,legendId);
			if(CollectionUtils.isNotEmpty(staticFieldList)) {
				for (int i = 0; i < staticFieldList.size(); i++) {
					AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean();
					admissionSettingBean.setFieldTypeId(staticFieldList.get(i).getFieldTypeFlag());
					admissionSettingBean.setFieldName(staticFieldList.get(i).getFieldName());
					admissionSettingBean.setIsRequired(staticFieldList.get(i).getIsRequired());
					admissionSettingBean.setIsDisplay(staticFieldList.get(i).getIsDisplay());
					admissionSettingBean.setIsyearly(staticFieldList.get(i).getIsyearly());
					admissionSettingBean.setIsStaticOrDynamic(String.valueOf(staticFieldList.get(i).getIsStaticOrDynamic()));
					admissionSettingBean.setPriority(staticFieldList.get(i).getPriority());
					admissionSettingBean.setFieldId(staticFieldList.get(i).getFieldId());
					admissionSettingBean.setDynamicvalidationFlag(staticFieldList.get(i).getValidationFlag());
					admissionSettingBean.setIsPreviousOrNot(0);
					if(CollectionUtils.isNotEmpty(tabAndLegendWiseList)) {
						for(int j=0;j<tabAndLegendWiseList.size();j++) { 
							if(staticFieldList.get(i).getFieldName().equals(tabAndLegendWiseList.get(j).getFieldName())) {
								admissionSettingBean.setFieldId(tabAndLegendWiseList.get(j).getFieldId());
								admissionSettingBean.setFieldTypeId(tabAndLegendWiseList.get(j).getFieldTypeFlag());
								admissionSettingBean.setFieldName(tabAndLegendWiseList.get(j).getFieldName());
								admissionSettingBean.setIsRequired(tabAndLegendWiseList.get(j).getIsRequired());
								admissionSettingBean.setIsDisplay(tabAndLegendWiseList.get(j).getIsDisplay());
								admissionSettingBean.setIsyearly(tabAndLegendWiseList.get(j).getIsyearly());
								admissionSettingBean.setIsStaticOrDynamic(String.valueOf(tabAndLegendWiseList.get(j).getIsStaticOrDynamic()));
								admissionSettingBean.setPriority(tabAndLegendWiseList.get(j).getPriority());
								admissionSettingBean.setDynamicvalidationFlag(tabAndLegendWiseList.get(j).getValidationFlag());
								admissionSettingBean.setIsPreviousOrNot(1);
								break;
							}
						}
					}
					AdmissionSettingBeanList.add(admissionSettingBean);
				}
			}
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DesignAdmiFormTabsMultiModel> dynamicFieldList=designAdmissionFormTabsMultiRepository.getFieldTabsAndLegendWise(tabId,schoolId,legendId);
			if(CollectionUtils.isNotEmpty(dynamicFieldList)) {
				for(int j=0;j<dynamicFieldList.size();j++) { 
					if(dynamicFieldList.get(j).getIsStaticOrDynamic().equals("1")) {
						AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean();
						admissionSettingBean.setFieldId(dynamicFieldList.get(j).getFieldId());
						admissionSettingBean.setFieldTypeId(dynamicFieldList.get(j).getFieldTypeFlag());
						admissionSettingBean.setFieldName(dynamicFieldList.get(j).getFieldName());
						admissionSettingBean.setIsRequired(dynamicFieldList.get(j).getIsRequired());
						admissionSettingBean.setIsDisplay(dynamicFieldList.get(j).getIsDisplay());
						admissionSettingBean.setIsyearly(dynamicFieldList.get(j).getIsyearly());
						admissionSettingBean.setIsStaticOrDynamic(String.valueOf(dynamicFieldList.get(j).getIsStaticOrDynamic()));
						admissionSettingBean.setPriority(dynamicFieldList.get(j).getPriority());
						admissionSettingBean.setDynamicvalidationFlag(dynamicFieldList.get(j).getValidationFlag());
						AdmissionSettingBeanList.add(admissionSettingBean);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AdmissionSettingBeanList;
	}

	@Override
	@Transactional
	public Integer saveLegendName(String legendName, Integer schoolId, Integer tabId) {
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
			DesignAdmiFormLegendModel designAdmiFormLegendModel=new DesignAdmiFormLegendModel();
			SchoolMasterModel schoolMasterModel=new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			DesignAdmiFormTabsModel designAdmiFormTabsModel=new DesignAdmiFormTabsModel();
			designAdmiFormTabsModel.setTabsId(tabId);
			designAdmiFormLegendModel.setLegendName(legendName);
			designAdmiFormLegendModel.setSchoolMasterModel(schoolMasterModel);
			designAdmiFormLegendModel.setDesignAdmiFormTabsModel(designAdmiFormTabsModel);
			String incrementedId = ""+designAdmiFormLegendRepository.getMaxId(schoolId);
			String legendId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			designAdmiFormLegendModel.setLegendId(Integer.parseInt(legendId));
			DesignAdmiFormLegendModel designAdmiFormLegendModel1=designAdmiFormLegendRepository.save(designAdmiFormLegendModel);
			return designAdmiFormLegendModel1.getLegendId();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public List<AdmissionSettingBean> getlegendName(Integer tabId, Integer schoolId) {
		List<AdmissionSettingBean> AdmissionSettingBeanList = new ArrayList<AdmissionSettingBean>();
		try {
			List<DesignAdmiFormLegendModel> legendList=designAdmiFormLegendRepository.findBySchoolMasterModel(tabId,schoolId);
			if(CollectionUtils.isNotEmpty(legendList)) {
				for (int i = 0; i < legendList.size(); i++) {
					AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean();
					admissionSettingBean.setLegendId(legendList.get(i).getLegendId());
					admissionSettingBean.setLegendName(legendList.get(i).getLegendName());
					AdmissionSettingBeanList.add(admissionSettingBean);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return AdmissionSettingBeanList;
	}

	@Override
	public Integer deleteLegendName(Integer tabId, Integer legendId, Integer schoolId) {
		try {
			 return designAdmiFormLegendRepository.deleteLegendName(tabId,legendId,schoolId);
		}
		catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	@Transactional
	public Integer updateField(AdmissionSettingBean admissionSettingBean) {
		int count =0;
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(admissionSettingBean.getSchoolId());
			String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
			SchoolMasterModel schoolMasterModel=new SchoolMasterModel();
			schoolMasterModel.setSchoolid(admissionSettingBean.getSchoolId());
			
			AppUserRoleModel userIdModel=new AppUserRoleModel();
			String userId = appUserRepository.getUserId(admissionSettingBean.getSchoolId()).toString();
			userIdModel.setAppUserRoleId(Integer.parseInt(userId));
			
			for(int i=0;i<admissionSettingBean.getIsDisplayArray2().size();i++) {
				if(admissionSettingBean.getFieldTypeIdArray2().get(i).equals("2") || admissionSettingBean.getFieldTypeIdArray2().get(i).equals("4")) {
					if(! admissionSettingBean.getDynamicFieldValuePOpUpArrayList2().get(i).equals("-")){
						designAdmiFormDynamicFieldValueRepository.deleteDynamicFieldValue(Integer.parseInt(admissionSettingBean.getFieldIdArray().get(i)),admissionSettingBean.getSchoolId());
						String dataArr2 = admissionSettingBean.getDynamicFieldValuePOpUpArrayList2().get(i); 
						String dateArr[] = dataArr2.split("\\$"); 
						for(int m=0; m<dateArr.length; m++) {
							DesignAdmiDynamicFieldValueModel designAdmiDyanmicFieldValue=new DesignAdmiDynamicFieldValueModel();
							
							designAdmiDyanmicFieldValue.setSchoolMasterModel(schoolMasterModel);
							designAdmiDyanmicFieldValue.setUserId(userIdModel);
							DesignAdmiFormLegendModel designAdmiFormLegendModel=new DesignAdmiFormLegendModel();
							designAdmiFormLegendModel.setLegendId(admissionSettingBean.getCurrentLegendIdFinal());
							DesignAdmiFormTabsModel designAdmiFormTabsModel=new DesignAdmiFormTabsModel();
							designAdmiFormTabsModel.setTabsId( admissionSettingBean.getCurrentTabName());
							DesignAdmiFormTabsMultiModel designAdmiFormTabsModel2=new DesignAdmiFormTabsMultiModel();
							designAdmiFormTabsModel2.setFieldId(Integer.parseInt(admissionSettingBean.getFieldIdArray().get(i)));
							designAdmiDyanmicFieldValue.setDesignAdmiFormTabsMultiModel(designAdmiFormTabsModel2);
							designAdmiDyanmicFieldValue.setFieldValue(dateArr[m]);
							designAdmiDyanmicFieldValue.setDesignAdmiFormLegendModel(designAdmiFormLegendModel);;
							designAdmiDyanmicFieldValue.setDesignAdmiFormTabsModel(designAdmiFormTabsModel);
							designAdmiDyanmicFieldValue.setFieldTypeFlag(String.valueOf(admissionSettingBean.getFieldTypeIdArray2().get(i)));
							String incrementedId = ""+designAdmiFormDynamicFieldValueRepository.getMaxId(admissionSettingBean.getSchoolId());
							String fieldId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
							designAdmiDyanmicFieldValue.setDynamicFieldId(Integer.parseInt(fieldId));
							designAdmiFormDynamicFieldValueRepository.saveAndFlush(designAdmiDyanmicFieldValue);
						}
					}
				}
				
				
				 List<DesignAdmiFormTabsMultiModel> tabAndLegendWiseList=designAdmissionFormTabsMultiRepository.getFieldInformation(admissionSettingBean.getCurrentTabName(),admissionSettingBean.getSchoolId(),
						 admissionSettingBean.getCurrentLegendIdFinal(),admissionSettingBean.getFieldNameArray().get(i));
				 if(CollectionUtils.isNotEmpty(tabAndLegendWiseList)) {
					 DesignAdmiFormTabsMultiModel designAdmiFormTabsMultiModel=new DesignAdmiFormTabsMultiModel();
					 designAdmiFormTabsMultiModel.setIsDisplay(admissionSettingBean.getIsDisplayArray2().get(i));
					 designAdmiFormTabsMultiModel.setIsRequired(admissionSettingBean.getIsRequiredArray().get(i));
					 if(! admissionSettingBean.getIsStaticOrDynamicArray().get(i).equals("1")) {
						 designAdmiFormTabsMultiModel.setIsyearly(admissionSettingBean.getIsYearlyArray().get(i));
					 }
					 if(StringUtils.isNotEmpty(admissionSettingBean.getNoOfPriorityArray().get(i))) {
						 designAdmiFormTabsMultiModel.setPriority(Integer.parseInt(admissionSettingBean.getNoOfPriorityArray().get(i)));
					 }
					
					 DesignAdmiFormLegendModel designAdmiFormLegendModel = new DesignAdmiFormLegendModel();
					 designAdmiFormLegendModel.setLegendId(admissionSettingBean.getCurrentLegendIdFinal());
					 designAdmiFormTabsMultiModel.setDesignAdmiFormLegendModel(designAdmiFormLegendModel );
					 designAdmiFormTabsMultiModel.setValidationFlag(admissionSettingBean.getValidationFlagArray().get(i));
					 DesignAdmiFormTabsModel designAdmiFormTabsModel = new DesignAdmiFormTabsModel();
					 designAdmiFormTabsModel.setTabsId(admissionSettingBean.getCurrentTabName());
					 designAdmiFormTabsMultiModel.setDesignAdmiFormTabsModel(designAdmiFormTabsModel);
					 designAdmiFormTabsMultiModel.setFieldName(admissionSettingBean.getFieldNameArray().get(i));
					 designAdmiFormTabsMultiModel.setIsStaticOrDynamic(admissionSettingBean.getIsStaticOrDynamicArray().get(i));
					 designAdmiFormTabsMultiModel.setFieldTypeFlag(admissionSettingBean.getFieldTypeIdArray2().get(i));
					 designAdmiFormTabsMultiModel.setSchoolMasterModel(schoolMasterModel);
					 designAdmiFormTabsMultiModel.setFieldId(tabAndLegendWiseList.get(0).getFieldId());
					 designAdmissionFormTabsMultiRepository.saveAndFlush(designAdmiFormTabsMultiModel);
					 count++;
				 }
				 else if(admissionSettingBean.getIsDisplayArray2().get(i).equals("1")){
					 DesignAdmiFormTabsMultiModel designAdmiFormTabsMultiModel=new DesignAdmiFormTabsMultiModel();
					 designAdmiFormTabsMultiModel.setIsDisplay(admissionSettingBean.getIsDisplayArray2().get(i));
					 designAdmiFormTabsMultiModel.setIsRequired(admissionSettingBean.getIsRequiredArray().get(i));
					 designAdmiFormTabsMultiModel.setIsyearly(admissionSettingBean.getIsYearlyArray().get(i));
					 
					 if(CollectionUtils.isNotEmpty(admissionSettingBean.getNoOfPriorityArray()) && StringUtils.isNotEmpty(admissionSettingBean.getNoOfPriorityArray().get(i))) {
						 designAdmiFormTabsMultiModel.setPriority(Integer.parseInt(admissionSettingBean.getNoOfPriorityArray().get(i)));
					 }
					
					 DesignAdmiFormLegendModel designAdmiFormLegendModel = new DesignAdmiFormLegendModel();
					 designAdmiFormLegendModel.setLegendId(admissionSettingBean.getCurrentLegendIdFinal());
					 designAdmiFormTabsMultiModel.setDesignAdmiFormLegendModel(designAdmiFormLegendModel );
					 designAdmiFormTabsMultiModel.setValidationFlag(admissionSettingBean.getValidationFlagArray().get(i));
					 DesignAdmiFormTabsModel designAdmiFormTabsModel = new DesignAdmiFormTabsModel();
					 designAdmiFormTabsModel.setTabsId(admissionSettingBean.getCurrentTabName());
					 designAdmiFormTabsMultiModel.setDesignAdmiFormTabsModel(designAdmiFormTabsModel);
					 designAdmiFormTabsMultiModel.setFieldName(admissionSettingBean.getFieldNameArray().get(i));
					 designAdmiFormTabsMultiModel.setFieldTypeFlag(admissionSettingBean.getFieldTypeIdArray2().get(i));
					 designAdmiFormTabsMultiModel.setSchoolMasterModel(schoolMasterModel);
					 designAdmiFormTabsMultiModel.setIsStaticOrDynamic(admissionSettingBean.getIsStaticOrDynamicArray().get(i));
					 String incrementedId = ""+designAdmissionFormTabsMultiRepository.getMaxId(admissionSettingBean.getSchoolId());
					 String fieldId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					 designAdmiFormTabsMultiModel.setFieldId(Integer.parseInt(fieldId));
					 designAdmissionFormTabsMultiRepository.saveAndFlush(designAdmiFormTabsMultiModel);
					 count++;
				 }
				
			}

			if(CollectionUtils.isNotEmpty(admissionSettingBean.getDynamicIsDisplayArray2())) {
				if(!admissionSettingBean.getDynamicIsDisplayArray2().isEmpty() && admissionSettingBean.getDynamicIsDisplayArray2()!=null) {
					for(int i=0;i<admissionSettingBean.getDynamicIsDisplayArray2().size();i++) {
						if(admissionSettingBean.getDynamicIsDisplayArray2().get(i).equals("1")) {
							DesignAdmiFormTabsMultiModel designAdmiFormTabsModel2=new DesignAdmiFormTabsMultiModel();
							designAdmiFormTabsModel2.setSchoolMasterModel(schoolMasterModel);
							designAdmiFormTabsModel2.setUserId(userIdModel);
							DesignAdmiFormLegendModel designAdmiFormLegendModel=new DesignAdmiFormLegendModel();
							designAdmiFormLegendModel.setLegendId(admissionSettingBean.getCurrentLegendIdFinal());
							DesignAdmiFormTabsModel designAdmiFormTabsModel=new DesignAdmiFormTabsModel();
							designAdmiFormTabsModel.setTabsId( admissionSettingBean.getCurrentTabName());
							designAdmiFormTabsModel2.setSchoolMasterModel(schoolMasterModel);
							designAdmiFormTabsModel2.setUserId(userIdModel);
							designAdmiFormTabsModel2.setDesignAdmiFormLegendModel(designAdmiFormLegendModel);;
							designAdmiFormTabsModel2.setDesignAdmiFormTabsModel(designAdmiFormTabsModel);
							designAdmiFormTabsModel2.setIsDisplay("1");
							designAdmiFormTabsModel2.setFieldName(admissionSettingBean.getDynamicFieldNameArray().get(i));
							designAdmiFormTabsModel2.setFieldTypeFlag(String.valueOf(admissionSettingBean.getDynamicFieldTypeArray().get(i)));
							designAdmiFormTabsModel2.setIsRequired(admissionSettingBean.getDynamicIsRequiredArray().get(i));
							designAdmiFormTabsModel2.setIsyearly(admissionSettingBean.getDynamicIsYearlyArray().get(i));
							designAdmiFormTabsModel2.setPriority(admissionSettingBean.getDynamicPriorityArray().get(i));
							designAdmiFormTabsModel2.setIsStaticOrDynamic("1");
							designAdmiFormTabsModel2.setValidationFlag(admissionSettingBean.getDynamicvalidationFlagArray().get(i));
							String incrementedId = ""+designAdmissionFormTabsMultiRepository.getMaxId(admissionSettingBean.getSchoolId());
							String fieldId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
							designAdmiFormTabsModel2.setFieldId(Integer.parseInt(fieldId));
							designAdmissionFormTabsMultiRepository.save(designAdmiFormTabsModel2);
							if(admissionSettingBean.getDynamicFieldTypeArray().get(i)==2 || admissionSettingBean.getDynamicFieldTypeArray().get(i)==4) {
								String dataArr2 = admissionSettingBean.getDynamicFieldValuePOpUpArrayList().get(i); 
								String dateArr[] = dataArr2.split("\\$"); 
								Set<DesignAdmiDynamicFieldValueModel> designAdmiDyanmicFieldValue2 = new HashSet<DesignAdmiDynamicFieldValueModel>();
								for(int m=0; m<dateArr.length; m++) {
									DesignAdmiDynamicFieldValueModel designAdmiDyanmicFieldValue=new DesignAdmiDynamicFieldValueModel();
									designAdmiDyanmicFieldValue.setFieldValue(dateArr[m]);
									designAdmiDyanmicFieldValue.setSchoolMasterModel(schoolMasterModel);
									designAdmiDyanmicFieldValue.setUserId(userIdModel);
									designAdmiDyanmicFieldValue.setDesignAdmiFormLegendModel(designAdmiFormLegendModel);;
									designAdmiDyanmicFieldValue.setDesignAdmiFormTabsModel(designAdmiFormTabsModel);
									designAdmiDyanmicFieldValue.setFieldTypeFlag(String.valueOf(admissionSettingBean.getDynamicFieldTypeArray().get(i)));
									designAdmiDyanmicFieldValue.setDesignAdmiFormTabsMultiModel(designAdmiFormTabsModel2);
									designAdmiDyanmicFieldValue2.add(designAdmiDyanmicFieldValue);
									incrementedId = ""+designAdmiFormDynamicFieldValueRepository.getMaxId(admissionSettingBean.getSchoolId());
									fieldId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
									designAdmiDyanmicFieldValue.setDynamicFieldId(Integer.parseInt(fieldId));
									designAdmiFormDynamicFieldValueRepository.saveAndFlush(designAdmiDyanmicFieldValue);
								}
							}
							
						}
					}
				}
			}
			
			for(int i=0;i<admissionSettingBean.getUpdateisYearlyVal().length;i++) {
				String dateArr[] = admissionSettingBean.getUpdateisYearlyVal()[i].split("\\@"); 
				designAdmissionFormTabsMultiRepository.updateIsYearly(dateArr[1],dateArr[0]);
			}
			
			for(int i=0;i<admissionSettingBean.getUpdateisRequiredVal().length;i++) {
				String dateArr[] = admissionSettingBean.getUpdateisRequiredVal()[i].split("\\@"); 
				designAdmissionFormTabsMultiRepository.updateIsRequired(dateArr[1],dateArr[0]);
			}
			
			for(int i=0;i<admissionSettingBean.getUpdateisDisplayVal().length;i++) {
				String dateArr[] = admissionSettingBean.getUpdateisDisplayVal()[i].split("\\@"); 
				designAdmissionFormTabsMultiRepository.updateIsDisplay(dateArr[1],dateArr[0]);
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public void updateisYearlyField(String[] isYearlyDynamicFieldVal, Integer schoolId) {
		try {
			for(int i=0;i<isYearlyDynamicFieldVal.length;i++) {
				String dateArr[] = isYearlyDynamicFieldVal[i].split("\\@"); 
				designAdmissionFormTabsMultiRepository.updateIsYearly(dateArr[1],dateArr[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();		
		}		
	}
	
	@Override
	public List<AdmissionSettingBean> getOldDynamicFields(Integer schoolId) {
		List<AdmissionSettingBean> AdmissionSettingBeanList = new ArrayList<AdmissionSettingBean>();
		try {
			List<DynamicAdmissionFiledsModel> list=dynamicAdmissionFiledsRepository.getOldDynamicFields(schoolId);
			for (int i = 0; i < list.size(); i++) {
				AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean();
				admissionSettingBean.setFieldId(list.get(i).getFieldId());
				admissionSettingBean.setFieldTypeId(String.valueOf(list.get(i).getFieldTypeFlag()));
				admissionSettingBean.setFieldName(list.get(i).getFieldName());
				admissionSettingBean.setIsDisplay(list.get(i).getFlag().toString());
				admissionSettingBean.setIsStaticOrDynamic("2");
				AdmissionSettingBeanList.add(admissionSettingBean);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return AdmissionSettingBeanList;
	}
		
	@Override
	public Integer updateLegendName(String legendName, Integer schoolId, Integer legendId) {
		try {
			 return designAdmiFormLegendRepository.updateTabsName(legendName,legendId);
		}
		catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Integer saveOldFieldValueName(Integer tabId, Integer schoolId, Integer oldFieldType,String oldfieldName) {
		int count=0;
		try {
		/* int fcount=0;
		List<DesignAdmiFormFieldsModel> onlyOldDynamicFeildsNamesFromStudAdmiFieldsList=designAdmiFormFieldsRepository.getAllOnlyOldDynamicFeildsNames(schoolId);
		List<DynamicAdmissionFiledsModel> oldDynamicFieldList=dynamicAdmissionFiledsRepository.getOldDynamicFields(schoolId);
		for (int i = 0; i < onlyOldDynamicFeildsNamesFromStudAdmiFieldsList.size(); i++) {
			for (int j = 0; j < oldDynamicFieldList.size(); j++) {
				if(oldDynamicFieldList.get(j).getFieldName().equals(onlyOldDynamicFeildsNamesFromStudAdmiFieldsList.get(i).getFieldName())){
					fcount=fcount+1;
					// delete Field
				}
			}
		}*/
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
		String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
		tabId = Integer.parseInt(UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, ""+tabId));
		List<DesignAdmiFormFieldsModel> dynamicFieldsList=designAdmissionFormTabsMultiRepository.getAllFeildsNames(tabId,schoolId);
		for (int i = 0; i < dynamicFieldsList.size(); i++) {
			if(oldfieldName.equals(dynamicFieldsList.get(i).getFieldName())){
				count=count+1;
			}
		}
			if(count==0){
					DesignAdmiFormTabsModel designAdmiFormTabsModel=new DesignAdmiFormTabsModel();
					DesignAdmiFormLegendModel designAdmiFormLegendModel=new DesignAdmiFormLegendModel();
					DesignAdmiFormFieldsModel designAdmiFormFieldsModel=new DesignAdmiFormFieldsModel();
					SchoolMasterModel schoolMasterModel=new SchoolMasterModel();
					schoolMasterModel.setSchoolid(schoolId);
					AppUserRoleModel userIdModel=new AppUserRoleModel();
					String userId = appUserRepository.getUserId(schoolId).toString();
					userIdModel.setAppUserRoleId(Integer.parseInt(userId));
					designAdmiFormTabsModel.setUserId(userIdModel);
					designAdmiFormFieldsModel.setSchoolMasterModel(schoolMasterModel);
					designAdmiFormFieldsModel.setFieldName(oldfieldName);
					designAdmiFormFieldsModel.setIsDisplay("0");
					designAdmiFormFieldsModel.setIsRequired("0");
					designAdmiFormFieldsModel.setIsyearly("0");
					designAdmiFormFieldsModel.setIsStaticOrDynamic("2");
					String legendId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey,"1");
					designAdmiFormLegendModel.setLegendId(Integer.parseInt(legendId));
					designAdmiFormFieldsModel.setDesignAdmiFormLegendModel(designAdmiFormLegendModel);
					tabId = Integer.parseInt(UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey,""+tabId));
					designAdmiFormTabsModel.setTabsId(tabId);
					designAdmiFormFieldsModel.setDesignAdmiFormTabsModel(designAdmiFormTabsModel);
					designAdmiFormFieldsModel.setFieldTypeFlag(String.valueOf(oldFieldType));
					String incrementedId = ""+designAdmiFormFieldsRepository.getMaxId(schoolId);
					String fieldId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					designAdmiFormFieldsModel.setFieldId(Integer.parseInt(fieldId));
					DesignAdmiFormFieldsModel designAdmiFormTabsModel1=designAdmiFormFieldsRepository.save(designAdmiFormFieldsModel);
					return designAdmiFormTabsModel1.getFieldId();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<AdmissionSettingBean> getDynamicFieldComboValue(Integer dynamicField,Integer schoolId) {
		List<AdmissionSettingBean> admissionSettingBeanList = new ArrayList<AdmissionSettingBean>();
		try {
			List<DesignAdmiDynamicFieldValueModel> list=designAdmiFormDynamicFieldValueRepository.getFieldComboValue(dynamicField,schoolId);
			for (int i = 0; i < list.size(); i++) {
				AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean();
				admissionSettingBean.setDynamicFieldComboValue(list.get(i).getFieldValue());
				admissionSettingBeanList.add(admissionSettingBean);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return admissionSettingBeanList;
	}

	@Override
	public List<AdmissionSettingBean> getOldDynamicFieldId(Integer tabId, Integer schoolId) {
		List<AdmissionSettingBean> AdmissionSettingBeanList = new ArrayList<AdmissionSettingBean>();
		try {
			List<DesignAdmiFormTabsMultiModel> list=designAdmissionFormTabsMultiRepository.getOldDynamicFieldId(tabId,schoolId);
			for (int i = 0; i < list.size(); i++) {
				AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean();
				admissionSettingBean.setFieldId(list.get(i).getFieldId());
				AdmissionSettingBeanList.add(admissionSettingBean);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return AdmissionSettingBeanList;
	}

	@Override
	public List<AdmissionSettingBean> setOldDynamicFieldValue(Integer oldDynamicFieldId, Integer schoolId,String fieldTypeId) {
		List<AdmissionSettingBean> admissionSettingBean=new ArrayList<AdmissionSettingBean>();
		try {
			admissionSettingBean=dynamicAdmissionFiledsRepositoryImpl.getComboValues(Integer.valueOf(schoolId),oldDynamicFieldId,String.valueOf(fieldTypeId));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return admissionSettingBean;
	}

	@Override
	public List<AdmissionSettingBean>  getOldDynamicFieldValueId(String fieldName, Integer schoolId) {
		List<AdmissionSettingBean> AdmissionSettingBeanList = new ArrayList<AdmissionSettingBean>();
		List<DynamicAdmissionFiledsModel> list=dynamicAdmissionFiledsRepository.getOldDynamicFieldValueId(fieldName,schoolId);
		try {
			for (int i = 0; i < list.size(); i++) {
				AdmissionSettingBean admissionSettingBean = new AdmissionSettingBean();
				admissionSettingBean.setFieldId(list.get(i).getFieldId());
				admissionSettingBean.setFieldName(String.valueOf(list.get(i).getFieldtype()));
				AdmissionSettingBeanList.add(admissionSettingBean);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return AdmissionSettingBeanList;
	}

	@Override
	public Integer updateTabsDisplayOrNotFlag(Integer schoolId, Integer tabId,String isTabDisplayOrNot) {
		try {
			return designAdmiFormTabsRepository.updateTabsDisplayOrNotFlag(schoolId,tabId,isTabDisplayOrNot);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public List<AdmissionSettingBean> checkStaticFieldIsDisplayOrNot(String staticFieldName, Integer schoolId) {
		try {
			return designAdmissionFormTabsMultiRepository.checkStaticFieldIsDisplayOrNot(staticFieldName,schoolId);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ArrayList<>();
	}

	
	
}
