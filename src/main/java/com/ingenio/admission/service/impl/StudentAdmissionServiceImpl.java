package com.ingenio.admission.service.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.common.io.ByteStreams;
import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.AppUserRoleBean;
import com.ingenio.admission.bean.ElectiveGroupBean;
import com.ingenio.admission.bean.SearchTextBean;
import com.ingenio.admission.bean.StudentAdmissionBean;
import com.ingenio.admission.bean.StudentCountBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.StudentDynamicDetailsBean;
import com.ingenio.admission.bean.SubGroupBean;
import com.ingenio.admission.bean.SubjectBean;
import com.ingenio.admission.bean.SubjectSelectionBean;
import com.ingenio.admission.bean.UploadFileBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.config.AWSUtility;
import com.ingenio.admission.model.AppUserModel;
import com.ingenio.admission.model.AppUserRoleModel;
import com.ingenio.admission.model.AssignSubjectToStudentModel;
import com.ingenio.admission.model.AttachmentMasterModel;
import com.ingenio.admission.model.AttendanceMonthCatlogModel;
import com.ingenio.admission.model.CasteMasterModel;
import com.ingenio.admission.model.CategoryMasterModel;
import com.ingenio.admission.model.DivisionMasterModel;
import com.ingenio.admission.model.DynamicAdmissionFiledsModel;
import com.ingenio.admission.model.DynamicCombobox1;
import com.ingenio.admission.model.DynamicCombobox2;
import com.ingenio.admission.model.DynamicCombobox3;
import com.ingenio.admission.model.DynamicCombobox4;
import com.ingenio.admission.model.DynamicCombobox5;
import com.ingenio.admission.model.DynamicFieldStudentInformationModel;
import com.ingenio.admission.model.GRBookNameModel;
import com.ingenio.admission.model.IcardMaster;
import com.ingenio.admission.model.ReligionMasterModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StandardMasterModel;
import com.ingenio.admission.model.StudentAttachmentModel;
import com.ingenio.admission.model.StudentDynamicDetailsMasterModel;
import com.ingenio.admission.model.StudentMasterModel;
import com.ingenio.admission.model.StudentStandardRenewModel;
import com.ingenio.admission.model.YearMasterModel;
import com.ingenio.admission.repository.AppUserRoleRepository;
import com.ingenio.admission.repository.AssignSubjectToStudentRepository;
import com.ingenio.admission.repository.AttendanceMonthCatlogRepository;
import com.ingenio.admission.repository.DesignAdmiFormDynamicFieldValueRepository;
import com.ingenio.admission.repository.DesignAdmiFormLegendRepository;
import com.ingenio.admission.repository.DesignAdmissionFormTabsMultiRepository;
import com.ingenio.admission.repository.DynamicAcademicMonthsRepository;
import com.ingenio.admission.repository.DynamicAdmissionFiledsRepository;
import com.ingenio.admission.repository.DynamicFieldStudentInfoRepository;
import com.ingenio.admission.repository.DynamicStudentDetailsRepository;
import com.ingenio.admission.repository.IcardMasterRepository;
import com.ingenio.admission.repository.ImageRepository;
import com.ingenio.admission.repository.PrintExcelStudentRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.repository.StudentAttachmentRepository;
import com.ingenio.admission.repository.StudentMasterRepository;
import com.ingenio.admission.repository.StudentStandardRepository;
import com.ingenio.admission.repository.UserRepository;
import com.ingenio.admission.repository.YearMasterRepository;
import com.ingenio.admission.repository.impl.DynamicAdmissionFiledsRepositoryImpl;
import com.ingenio.admission.repository.impl.PrintExcelStudentRepositoryImpl;
import com.ingenio.admission.service.StudentAdmissionService;
import com.ingenio.admission.util.CryptographyUtil;
import com.ingenio.admission.util.DateToWords;
import com.ingenio.admission.util.HttpClientUtil;
import com.ingenio.admission.util.UTFConversionUtil;
import com.ingenio.admission.util.Utillity;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

@Component


//  @PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//  
//  @PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//  
//  @PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")


//  @PropertySource("classpath:messages_en.properties")
//  
//  @PropertySource("classpath:messages_mr.properties")
//  
//  @PropertySource("classpath:messages_hn.properties")
  
 
public class StudentAdmissionServiceImpl implements StudentAdmissionService{

	@Autowired
	DesignAdmissionFormTabsMultiRepository designAdmissionFormTabsMultiRepository;
	
	@Autowired
	DesignAdmiFormLegendRepository designAdmiFormLegendRepository;

	@Autowired
	StudentMasterRepository studentMasterRepository;

	@Autowired
	DesignAdmiFormDynamicFieldValueRepository designAdmiFormDynamicFieldValueRepository;
	
	@Autowired
	DynamicAdmissionFiledsRepository dynamicAdmissionFieldRepository;
	
	@Autowired
	DynamicAdmissionFiledsRepositoryImpl dynamicAdmissionFieldRepositoryImpl;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AppUserRoleRepository appRoleRepository;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	AssignSubjectToStudentRepository assignSubjectToStudentRepository;
	
	@Autowired
	StudentAttachmentRepository studentAttachmentRepository;
	
	@Autowired
	DynamicStudentDetailsRepository dynamicStudentDetailsRepository;
	
	@Autowired
	DynamicFieldStudentInfoRepository dynamicFieldStudentInfoRepository;
	
	@Autowired
	DynamicAdmissionFiledsRepository dynamicAdmissionFiledsRepository;
	
	@Autowired
	PrintExcelStudentRepositoryImpl printExcelStudentRepositoryImpl;
	
	@Autowired
	StudentStandardRepository studentStandardRepository;
	
	@Autowired
	PrintExcelStudentRepository printExcelStudentRepository;
	
	@Autowired
	DynamicAcademicMonthsRepository dynamicAcademicMonthsRepository ;
	
	CryptographyUtil cry= new CryptographyUtil();
	
	
	 @Autowired
	 private Environment env;
	
	@Autowired
	UserAuthenticationServiceImpl userAuthenticationServiceImpl;
	
	@Autowired
    ImageRepository imageRepository;
    
    @Autowired 
	private RestTemplate restTemplate;
	
    @Autowired
	IcardMasterRepository icardMasterRepository;
    
    @Autowired
	AmazonS3 amazonS3;
    
    @Autowired
    Utillity utillity;
    
    @Autowired
    YearMasterRepository yearMasterRepository;
    
    @Autowired
    AttendanceMonthCatlogRepository attendanceMonthCatlogRepository;
    
	@Override
	@Transactional
	public StudentMasterModel saveNewRegistration(String newRegName, String yearId, String standardId,String passwordNewReg, String newRegUserName, String schoolId,String formNo,String missMr, String secondName, String lastName,HttpSession session) {
		StudentMasterModel studentMasterModel = new StudentMasterModel();
	try {
//		String newRegName = ;
//		String secondName = Utillity.convertInUTFFormat(secondName1);
//		String lastName = Utillity.convertInUTFFormat(lastName1);
		Integer  principaluserId = userRepository.getprincipalUserId(Integer.valueOf(schoolId));
		AppUserRoleModel appUserRoleModel =new AppUserRoleModel();
		appUserRoleModel.setAppUserRoleId(principaluserId);
		session.setAttribute("principalUserId", principaluserId);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate localDate = LocalDate.now();
		String date=(dtf.format(localDate)); //2016/11/16
		
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(Integer.valueOf(schoolId));
		String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
		String incrementedId = ""+studentMasterRepository.getMaxId(Integer.valueOf(schoolId));
		String studentId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
		studentMasterModel.setStudentId(Integer.parseInt(studentId));
		String provisionalNo = studentMasterRepository.getMaxProvisionalNo(Integer.valueOf(schoolId));
		studentMasterModel.setStudentRegNo("P"+provisionalNo);
		studentMasterModel.setProvisionalRegNo("P"+provisionalNo);
		studentMasterModel.setStudentFormNo(formNo);
		studentMasterModel.setAdmissionDate(date);
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(Integer.valueOf(schoolId));
		studentMasterModel.setSchoolMasterModel(schoolMasterModel);
		String getMaxGrBookId=studentMasterRepository.getMaxGrBookId(Integer.valueOf(schoolId));
		GRBookNameModel gRBookNameModel=new GRBookNameModel();
		gRBookNameModel.setGrBookId(Integer.valueOf(getMaxGrBookId));
		studentMasterModel.setGrBookName(gRBookNameModel);
		studentMasterModel.setMmobile(newRegUserName);
		studentMasterModel.setStudInitialName(missMr);
		studentMasterModel.setStudFName(newRegName);
		studentMasterModel.setStudLName(lastName);
		studentMasterModel.setStudMName(secondName);
		studentMasterModel.setIsOnlineAdm("1");
		studentMasterModel.setIsQuickAdmission(0);
		studentMasterModel.setIsApproval("1");
		studentMasterModel.setPreviousState(yearId);
		studentMasterModel.setAdmissionDate(date);
		studentMasterModel.setMedium("Select Medium");
		studentMasterModel.setUserId(appUserRoleModel);
		
		Set<StudentStandardRenewModel> studentStandardRenewMap = new HashSet<>();
		
		YearMasterModel yearMasterModel = new YearMasterModel();
		yearMasterModel.setYearId(Integer.parseInt(yearId));
		StandardMasterModel standardMasterModel = new StandardMasterModel();
		standardMasterModel.setStandardId(Integer.parseInt(standardId));
			StudentStandardRenewModel studentStandardRenewModel = new StudentStandardRenewModel();
			//studentStandardRenewModel.setRenewstudentId(studentAdmissionBean.getStudentRenewId());
			studentStandardRenewModel.setStudentMasterModel(studentMasterModel);
			studentStandardRenewModel.setYearMasterModel(yearMasterModel );
			studentStandardRenewModel.setStandardMasterModel(standardMasterModel );
			studentStandardRenewModel.setAlreadyRenew((short) 1);
			studentStandardRenewModel.setRenewAdmissionDate(date);
			studentStandardRenewModel.setIsOnlineAdmission("1");
			studentStandardRenewModel.setStudFailDate(date);
			studentStandardRenewModel.setSchoolMasterModel(schoolMasterModel);
			String incrementedId1 = ""+studentMasterRepository.getMaxRenewId(Integer.valueOf(schoolId));
			String renewId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
			studentStandardRenewModel.setRenewStudentId(Integer.parseInt(renewId));
			studentStandardRenewModel.setSchoolMasterModel(schoolMasterModel);
			studentStandardRenewMap.add(studentStandardRenewModel);
		
			studentMasterModel.setStudentStandardRenews(studentStandardRenewMap);
			studentMasterModel = studentMasterRepository.save(studentMasterModel);
		
			for(int i=0;i<3;i++) {
					String incrementedId11 = ""+userRepository.getMaxUserId(Integer.valueOf(schoolId));
					String maxUserId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId11);
					AppUserModel user  = new AppUserModel();
					user.setAppUserRoleId(Integer.parseInt(maxUserId));
					if(i==0) {
						user.setRoleName("ROLE_STUDENT");
						
					}
					if(i==1) {
						user.setRoleName("ROLE_PARENT1");
					}
					if(i==2) {
						user.setRoleName("ROLE_PARENT2");
					}
				
					user.setUsername(newRegUserName);
					user.setStaffId(studentMasterModel.getStudentId());
					user.setPassword(md5(passwordNewReg));
					user.setFirstName(newRegName);
					if(lastName.length()>0) {
						user.setLastName(lastName);
					}else {
						user.setLastName(newRegName);
					}
					user.setSchoolid(schoolMasterModel.getSchoolid());
					user.setSettingString("fjEHuisqm3ASr9F8oUudICFblqLrBExRMl4ywbLz8A/FEd9Uod5PQopFWu5+9C4oUlvghDncYonqPqEtO4SF/Guo+lq6LrpGXqMtRv4Pu9ZffpvBUFEUrUAloT62u5CLBW5LygVsBWEBzfrAlsn7qDncN0612klKx0/tKJ/3ulPvsmgdrt65tJJpFvntlKeo");
					userRepository.saveAndFlush(user);
					
					AppUserRoleModel appUserRole = new AppUserRoleModel();
					appUserRole.setAppUserRoleId(Integer.parseInt(maxUserId));
					appUserRole.setRoleName("ROLE_ADMIN");
					appUserRole.setUsername(newRegUserName);
					appUserRole.setSchoolid(schoolMasterModel.getSchoolid());
					appRoleRepository.saveAndFlush(appUserRole);
					if(i==0) {
						// for fee setting donot remove
						studentMasterModel.setUserId(appUserRole);
					}
					
			}
			StudentAdmissionBean studentAdmissionBean=new StudentAdmissionBean();
			studentAdmissionBean.setStudId(studentId);
			studentAdmissionBean.setStudLName(lastName);
			studentAdmissionBean.setStudFName(newRegName);
			studentAdmissionBean.setUserName(newRegUserName);
			String schoolName ="";
			
		  schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));
			
//			String url = ConfigurationProperties.addRoutingUserUrl;
			//Integer studentAdmissionBean2 = (Integer) RestUtil.makeRestCallForRoutingTable(url,studentAdmissionBean);
			Integer languageId = schoolModel.getLanguage().getLanguageId();
			String message ="";
			if(languageId == 2) {
				 message = studentMasterModel.getStudFName()+ ","+env.getProperty("mr.pravesh") + " " +studentMasterModel.getStudentFormNo() 
				+" "+env.getProperty("mr.date")  + studentMasterModel.getAdmissionDate() + " " + env.getProperty("mr.admission") + " - " + schoolName ;
				System.out.println(message);	
			}else {
				 message = "Dear"+studentMasterModel.getStudFName()+" "+studentMasterModel.getStudMName()+" "+studentMasterModel.getStudLName()+
						  ","+ "Your application for online admission has been received on "+ studentMasterModel.getAdmissionDate() + " " +
				          "Through Form No." + " " +studentMasterModel.getStudentFormNo() +" "+ "and Registration No."+" "+ studentMasterModel.getStudentRegNo() +
				          " " + ". To get Approval kindly login to fill other information . " + " - " + schoolName ;
				System.out.println(message);	
			}
			userAuthenticationServiceImpl.sendOtpMessage(studentMasterModel.getMmobile(), message, Integer.valueOf(schoolId),languageId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return studentMasterModel;
	}
	
	private String getSchoolName(String schoolId) {
		SchoolMasterModel schoolMasterModel =  schoolMasterRepository.findBySchoolid(Integer.parseInt(schoolId));
		if(schoolMasterModel != null) {
			return schoolMasterModel.getSchoolName();
		}
		
		return "";
	}

	@Override
	@Transactional
	public StudentMasterModel saveStudentDetails(StudentAdmissionBean studentAdmissionBean,List<MultipartFile> file, 
			List<String> filename,HttpSession session,Integer flag) {
		List<StudentAdmissionBean> renewList = new ArrayList<>();
		List<StudentAdmissionBean> studentMasterList = studentMasterRepository.findByUser(studentAdmissionBean.getUserName(),studentAdmissionBean.getSchoolId());
//		List<StudentAdmissionBean> studentMasterList = studentMasterRepository.getByStudentId(studentAdmissionBean.getStudentId(),studentAdmissionBean.getSchoolId());

		
		StudentMasterModel studentMasterModel = new StudentMasterModel();
		String provisionalNo ="";
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentAdmissionBean.getSchoolId());
		studentAdmissionBean.setSchoolKey(schoolModel.getSchoolKey());
		String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
		try {
			if(CollectionUtils.isNotEmpty(studentMasterList)) {
				session.setAttribute("newStudentString", "0");
				studentAdmissionBean.setStudentId(studentMasterList.get(0).getStudentId());
				studentAdmissionBean.setAppUserRoleId(studentMasterList.get(0).getAppUserRoleId());
				renewList = studentMasterRepository.findRenewId(studentAdmissionBean.getStudentId(),studentAdmissionBean.getAcademicYear(),studentAdmissionBean.getSchoolId());
				if(CollectionUtils.isNotEmpty(renewList)) {
					studentAdmissionBean.setStudentRenewId(renewList.get(0).getStudentRenewId());
				}
				
				List<StudentMasterModel> listModel =  studentMasterRepository.findByStudentId(studentMasterList.get(0).getStudentId());
				if(CollectionUtils.isNotEmpty(listModel)) {
					studentMasterModel = listModel.get(0);
				}
				provisionalNo = studentMasterList.get(0).getRegistrationNumber();
				studentMasterModel.setStudentRegNo(provisionalNo);
				studentMasterModel.setProvisionalRegNo(provisionalNo);
				studentAdmissionBean.setRegistrationNumber(provisionalNo);
				
			}
			else {
				session.setAttribute("newStudentString", "1");
				provisionalNo = studentMasterRepository.getMaxProvisionalNo(studentAdmissionBean.getSchoolId());
				studentMasterModel.setStudentRegNo("P"+provisionalNo);
				studentMasterModel.setProvisionalRegNo("P"+provisionalNo);
				studentAdmissionBean.setRegistrationNumber("P"+provisionalNo);
				String incrementedId = ""+studentMasterRepository.getMaxId(studentAdmissionBean.getSchoolId());
				String studentId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
				studentAdmissionBean.setStudentId(Integer.parseInt(studentId));
				studentMasterModel.setStudentId(Integer.parseInt(studentId));
			}
		
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentAdmissionBean.getSchoolId());
			studentMasterModel.setSchoolMasterModel(schoolMasterModel);
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getRegistrationNumber())) {
				studentMasterModel.setStudentRegNo(studentAdmissionBean.getRegistrationNumber());
			}

			GRBookNameModel grBookName = new GRBookNameModel();
			String grBookId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, "1");
			grBookName.setGrBookId(Integer.parseInt(grBookId));
			studentMasterModel.setGrBookName(grBookName);
			if(studentAdmissionBean.getGRBookName() !=null && StringUtils.isNotEmpty(""+studentAdmissionBean.getGRBookName())) {
				grBookName.setGrBookId(studentAdmissionBean.getGRBookName());
				studentMasterModel.setGrBookName(grBookName);
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getDefaultFormNo())) {
				studentMasterModel.setStudentFormNo(studentAdmissionBean.getDefaultFormNo());
			}
			
//			studentMasterModel.setAdmissionDate(studentAdmissionBean.getAdmissionDate());
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getMedium())) {
				studentMasterModel.setMedium(studentAdmissionBean.getMedium());
			}
			else if(StringUtils.isEmpty(studentMasterModel.getMedium())) {
				studentMasterModel.setMedium("Select Medium");
			}
			
			if(studentAdmissionBean.getCaste()!=null &&  StringUtils.isNotEmpty(""+studentAdmissionBean.getCaste())) {
				CasteMasterModel castMasterModel = new CasteMasterModel();
				castMasterModel.setCasteId(studentAdmissionBean.getCaste());
				studentMasterModel.setCastemaster(castMasterModel);
			}
			
			if(studentAdmissionBean.getCategory()!=null && StringUtils.isNotEmpty(""+studentAdmissionBean.getCategory())) {
				CategoryMasterModel category = new CategoryMasterModel();
				category.setCategoryId(studentAdmissionBean.getCategory());
				studentMasterModel.setCategory(category );
			}
			
			
			if(studentAdmissionBean.getReligion()!=null && StringUtils.isNotEmpty(""+studentAdmissionBean.getReligion())) {
				ReligionMasterModel religion = new ReligionMasterModel();
				religion.setReligionId(studentAdmissionBean.getReligion());
				studentMasterModel.setReligion(religion);
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getFirstName())) {
				studentMasterModel.setStudFName(Utillity.convertEachWordInUppercase(studentAdmissionBean.getFirstName()));
			}else if(StringUtils.isEmpty(studentMasterModel.getStudFName())) {
				studentMasterModel.setStudFName("");
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getMissMr())) {
				studentMasterModel.setStudInitialName(studentAdmissionBean.getMissMr());
			}
			if(StringUtils.isNotEmpty(studentAdmissionBean.getLastName())) {
				studentMasterModel.setStudLName(Utillity.convertEachWordInUppercase(studentAdmissionBean.getLastName()));
			}else if(StringUtils.isEmpty(studentMasterModel.getStudLName())) {
				studentMasterModel.setStudLName("");
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getMiddleName())) {
				studentMasterModel.setStudMName(Utillity.convertEachWordInUppercase(studentAdmissionBean.getMiddleName()));
			}else if(StringUtils.isEmpty(studentMasterModel.getStudMName())) {
				studentMasterModel.setStudMName("");
			}
			String birthDate = "";
			if(StringUtils.isNotEmpty(studentAdmissionBean.getDdBirthDate())) {
				birthDate = studentAdmissionBean.getDdBirthDate().concat("-").concat(studentAdmissionBean.getMmBirthDate()).concat("-").concat(studentAdmissionBean.getYyBirthDate());
			}
			
			if(StringUtils.isNotEmpty(birthDate)) {
				studentMasterModel.setBirthDate(birthDate);
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getBirthPlace())) {
				studentMasterModel.setBirthPlace(studentAdmissionBean.getBirthPlace());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getGender())) {
				studentMasterModel.setStudGender(studentAdmissionBean.getGender());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getMothertongue())) {
				studentMasterModel.setMothertonge(studentAdmissionBean.getMothertongue());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getNationality())) {
				studentMasterModel.setNationlity(studentAdmissionBean.getNationality());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getLocalAddress())) {
				studentMasterModel.setLaddress(studentAdmissionBean.getLocalAddress());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getLocalState())) {
				studentMasterModel.setLstate(studentAdmissionBean.getLocalState());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getLocalCountry())) {
				studentMasterModel.setLcountry(studentAdmissionBean.getLocalCountry());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getLocalDistrict())) {
				studentMasterModel.setLcity(studentAdmissionBean.getLocalDistrict());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getLocalPinCode())) {
				studentMasterModel.setLpincode(studentAdmissionBean.getLocalPinCode());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPermanentAddress())) {
				studentMasterModel.setPaddress(studentAdmissionBean.getPermanentAddress());
			}
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPermanentState())) {
				studentMasterModel.setPstate(studentAdmissionBean.getPermanentState());
			}
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPermanentCountry())) {
				studentMasterModel.setPcountry(studentAdmissionBean.getPermanentCountry());
			}
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPermanentDistrict())) {
				studentMasterModel.setPcity(studentAdmissionBean.getPermanentDistrict());
			}
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPermanentPinCode())) {
				studentMasterModel.setPpincode(studentAdmissionBean.getPermanentPinCode());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getFatherName())) {
				studentMasterModel.setFatherName(studentAdmissionBean.getFatherName());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getMotherName())) {
				studentMasterModel.setMotherName(studentAdmissionBean.getMotherName());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getEducation())) {
				studentMasterModel.setFeducation(studentAdmissionBean.getEducation());
			}
			if(StringUtils.isNotEmpty(studentAdmissionBean.getOccupation())) {
				studentMasterModel.setFoccupation(studentAdmissionBean.getOccupation());
			}
			if(studentAdmissionBean.getIncome() !=null) {
				studentMasterModel.setFincome(""+studentAdmissionBean.getIncome());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getAge())) {
				studentMasterModel.setFage(""+studentAdmissionBean.getAge());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getParentEmailID())) {
				studentMasterModel.setFemail(studentAdmissionBean.getParentEmailID());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getContactNumber1ForSMS())) {
				studentMasterModel.setMmobile(studentAdmissionBean.getContactNumber1ForSMS());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getContactNumber2())) {
				studentMasterModel.setFmobile(studentAdmissionBean.getContactNumber2());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getEmailID())) {
				studentMasterModel.setMemail(studentAdmissionBean.getEmailID());
			}

			if(StringUtils.isNotEmpty(studentAdmissionBean.getBankName())) {
				studentMasterModel.setMeducation(studentAdmissionBean.getBankName());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getBranchName())) {
				studentMasterModel.setMoccupation(studentAdmissionBean.getBranchName());
			}

			if(StringUtils.isNotEmpty(studentAdmissionBean.getBankIFSCNo())) {
				studentMasterModel.setMage(studentAdmissionBean.getBankIFSCNo());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getAccountNo())) {
				studentMasterModel.setMincome(studentAdmissionBean.getAccountNo());	
			}

			if(StringUtils.isNotEmpty(studentAdmissionBean.getAadhaarCardNo())) {
				studentMasterModel.setMtelephone(studentAdmissionBean.getAadhaarCardNo());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPreviousYear())) {
				studentMasterModel.setPreviousState(studentAdmissionBean.getPreviousYear());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPreviousSchoolName())) {
				studentMasterModel.setPreviousSchool(studentAdmissionBean.getPreviousSchoolName());
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getPreviousClass())) {
				studentMasterModel.setPreviousStandard(studentAdmissionBean.getPreviousClass());
			}
			
			if(studentAdmissionBean.getBirthDate() != null && StringUtils.isNotEmpty(""+studentAdmissionBean.getBirthDate())) {
				DateToWords num= new DateToWords();		
				studentMasterModel.setFtelephone(num.converttowordfull(studentAdmissionBean.getBirthDate()));
				
			}
			
			if(StringUtils.isNotEmpty(studentAdmissionBean.getReasonforLeavingPreviousSchool())) {
				studentMasterModel.setPreviousReason(studentAdmissionBean.getReasonforLeavingPreviousSchool());
			}
			
		if(flag==1) {
			Set<StudentStandardRenewModel> studentStandardRenewMap = new HashSet<>();
			YearMasterModel yearMasterModel = new YearMasterModel();
			yearMasterModel.setYearId(studentAdmissionBean.getAcademicYear());
			StandardMasterModel standardMasterModel = new StandardMasterModel();
			standardMasterModel.setStandardId(studentAdmissionBean.getStandard());
			if(CollectionUtils.isEmpty(renewList)) {
				StudentStandardRenewModel studentStandardRenewModel = new StudentStandardRenewModel();
				//studentStandardRenewModel.setRenewstudentId(studentAdmissionBean.getStudentRenewId());
				studentStandardRenewModel.setStudentMasterModel(studentMasterModel);
				studentStandardRenewModel.setYearMasterModel(yearMasterModel );
				studentStandardRenewModel.setStandardMasterModel(standardMasterModel );
				studentStandardRenewModel.setAlreadyRenew((short) 1);
				studentStandardRenewModel.setStudFailDate(studentAdmissionBean.getAdmissionDate());
				studentStandardRenewModel.setRenewAdmissionDate(studentAdmissionBean.getAdmissionDate());
				studentStandardRenewModel.setSchoolMasterModel(schoolMasterModel);
				String incrementedId = ""+studentMasterRepository.getMaxRenewId(studentAdmissionBean.getSchoolId());
				String renewId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
				studentStandardRenewModel.setRenewStudentId(Integer.parseInt(renewId));
				studentStandardRenewMap.add(studentStandardRenewModel);
			}
			
			studentMasterModel.setStudentStandardRenews(studentStandardRenewMap);
			studentMasterModel.setStudentId(studentAdmissionBean.getStudentId());
			studentMasterModel.setIsOnlineAdm("1");
			studentMasterModel.setIsQuickAdmission(0);
			studentMasterModel.setIsApproval("1");
			studentMasterModel = studentMasterRepository.save(studentMasterModel);
			
			
			for(int i=0;i<3;i++) {
				if(studentAdmissionBean.getAppUserRoleId() == null) {
					String incrementedId = ""+userRepository.getMaxUserId(studentAdmissionBean.getSchoolId());
					String maxUserId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					studentAdmissionBean.setAppUserRoleId(Integer.parseInt(maxUserId));
					AppUserModel user  = new AppUserModel();
					user.setAppUserRoleId(studentAdmissionBean.getAppUserRoleId());
					if(i==0) {
						user.setRoleName("ROLE_STUDENT");
					}
					if(i==1) {
						user.setRoleName("ROLE_PARENT1");
					}
					if(i==2) {
						user.setRoleName("ROLE_PARENT2");
					}
				
					user.setUsername(studentAdmissionBean.getUserName());
					user.setStaffId(studentMasterModel.getStudentId());
					user.setPassword(md5("default123"));
					user.setFirstName(studentAdmissionBean.getFirstName());
					user.setLastName(studentAdmissionBean.getLastName());
					user.setSchoolid(schoolMasterModel.getSchoolid());
					userRepository.saveAndFlush(user);
					
					AppUserRoleModel appUserRole = new AppUserRoleModel();
					appUserRole.setAppUserRoleId(studentAdmissionBean.getAppUserRoleId());
					appUserRole.setRoleName("ROLE_ADMIN");
					appUserRole.setUsername(studentAdmissionBean.getUserName());
					appUserRole.setSchoolid(schoolMasterModel.getSchoolid());
					appRoleRepository.saveAndFlush(appUserRole);
				}
			}
			
			
			AppUserRoleModel appUserRole = new AppUserRoleModel();
			appUserRole.setAppUserRoleId(studentAdmissionBean.getAppUserRoleId());
			if(studentAdmissionBean.getSelectedSubjectArr() != null && studentAdmissionBean.getSelectedSubjectArr().length>0) {
				assignSubjectToStudentRepository.deleteByStudentId(""+studentAdmissionBean.getStudentId());
				for (int i=0; i <studentAdmissionBean.getSelectedSubjectArr().length;i++ ) {
					AssignSubjectToStudentModel assignSubjectToStudent = new AssignSubjectToStudentModel();
					assignSubjectToStudent.setStudentId(""+studentMasterModel.getStudentId());
					assignSubjectToStudent.setYearId(studentAdmissionBean.getAcademicYear());
					assignSubjectToStudent.setSubjectId(studentAdmissionBean.getSelectedSubjectArr()[i]);
					assignSubjectToStudent.setSchoolMasterModel(schoolMasterModel);
					assignSubjectToStudent.setUserId(appUserRole);
					String incrementedId = ""+assignSubjectToStudentRepository.getMaxId(studentAdmissionBean.getSchoolId());
					String assignSubjectId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					assignSubjectToStudent.setAssignSubjectId(Integer.parseInt(assignSubjectId));
					assignSubjectToStudentRepository.saveAndFlush(assignSubjectToStudent);
				}
			}
			
			String[] dynamicTitleArr = studentAdmissionBean.getDynamicTitleArr();
			String[] dynamicValueArr = studentAdmissionBean.getDynamicValueArr();
			if(dynamicValueArr.length>0) {
				for(int i=0;i<dynamicValueArr.length;i++) {
					dynamicStudentDetailsRepository.deleteByStudentId(studentAdmissionBean.getStudentId(),dynamicTitleArr[i],studentAdmissionBean.getAcademicYear());
					if(StringUtils.isNotEmpty(dynamicValueArr[i])) {
						StudentDynamicDetailsMasterModel dynamicModel = new StudentDynamicDetailsMasterModel();
						dynamicModel.setDynamicTitle(dynamicTitleArr[i]);
						dynamicModel.setDynamicValue(dynamicValueArr[i]);
						dynamicModel.setIsYearly("0");
						dynamicModel.setYearMasterModel(yearMasterModel);
						dynamicModel.setStandardMasterModel(standardMasterModel);
						dynamicModel.setUserId(appUserRole);
						dynamicModel.setStudentMasterModel(studentMasterModel);
						dynamicModel.setSchoolMasterModel(schoolMasterModel);
						String incrementedId = ""+dynamicStudentDetailsRepository.getMaxId(studentAdmissionBean.getSchoolId());
						String dynamicId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
						dynamicModel.setDynamicDetailsId(Integer.parseInt(dynamicId));
						dynamicStudentDetailsRepository.saveAndFlush(dynamicModel);
					}
				}
			}
			
			String dynamicCheckValueArr[] = studentAdmissionBean.getDynamicCheckValueArr();
			String dynamicCheckTitleArr[] = studentAdmissionBean.getDynamicCheckTitleArr();
			if(dynamicCheckValueArr.length>0) {
				String preTitle="";
				String preVal="";
				for(int i=0;i<dynamicCheckTitleArr.length;i++) {
					/*if(i!=0) {
						if(preTitle.equals(dynamicCheckTitleArr[i]) && preTitle.equals(dynamicCheckValueArr[i])) {
							 
						//dynamicStudentDetailsRepository.deleteByStudentId(studentAdmissionBean.getStudentId(),dynamicCheckTitleArr[i],studentAdmissionBean.getAcademicYear());
						if(StringUtils.isNotEmpty(dynamicCheckValueArr[i])) {
							String[] checkedValues = dynamicCheckValueArr[i].split("-");
							String[] checkedTitles = dynamicCheckTitleArr[i].split("-");
							dynamicStudentDetailsRepository.deleteByStudentId(studentAdmissionBean.getStudentId(),checkedTitles[i],studentAdmissionBean.getAcademicYear());
							if(checkedValues.length>0) {
								for(int j=0;j<checkedValues.length;j++) {
									StudentDynamicDetailsMasterModel dynamicModel = new StudentDynamicDetailsMasterModel();
									dynamicModel.setDynamicTitle(checkedTitles[i]);
									dynamicModel.setDynamicValue(checkedValues[j]);
									dynamicModel.setIsYearly("0");
									dynamicModel.setUserId(appUserRole);
									dynamicModel.setYearMasterModel(yearMasterModel);
									dynamicModel.setStandardMasterModel(standardMasterModel);
									dynamicModel.setStudentMasterModel(studentMasterModel);
									dynamicModel.setSchoolMasterModel(schoolMasterModel);
									String incrementedId = ""+dynamicStudentDetailsRepository.getMaxId(studentAdmissionBean.getSchoolId());
									String dynamicId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
									dynamicModel.setDynamicDetailsId(Integer.parseInt(dynamicId));
									dynamicStudentDetailsRepository.saveAndFlush(dynamicModel);
								}
							}
						}
						}
						preTitle=dynamicCheckTitleArr[i];
						preVal=dynamicCheckValueArr[i];
					}else {*/
						//dynamicStudentDetailsRepository.deleteByStudentId(studentAdmissionBean.getStudentId(),dynamicCheckTitleArr[i],studentAdmissionBean.getAcademicYear());
						if(StringUtils.isNotEmpty(dynamicCheckValueArr[i])) {
							if(! preTitle.equals(dynamicCheckTitleArr[i]) && ! preVal.equals(dynamicCheckValueArr[i])) {
								//String[] dynamicCheckValueArrVal=dynamicCheckValueArr[i].split(",");
								//String[] dynamicCheckValueArrVal2=dynamicCheckTitleArr[i].split(",");
								String[] checkedValues = dynamicCheckValueArr[i].split("-");
								String[] checkedTitles = dynamicCheckTitleArr[i].split("-");
								for(int j=0;j<checkedValues.length;j++) {
									dynamicStudentDetailsRepository.deleteByStudentId(studentAdmissionBean.getStudentId(),checkedTitles[j],studentAdmissionBean.getAcademicYear());
								}
								if(checkedValues.length>0) {
									for(int j=0;j<checkedValues.length;j++) {
										//dynamicStudentDetailsRepository.deleteByStudentId(studentAdmissionBean.getStudentId(),checkedTitles[j],studentAdmissionBean.getAcademicYear());
										StudentDynamicDetailsMasterModel dynamicModel = new StudentDynamicDetailsMasterModel();
										dynamicModel.setDynamicTitle(checkedTitles[j]);
										dynamicModel.setDynamicValue(checkedValues[j]);
										dynamicModel.setIsYearly("0");
										dynamicModel.setUserId(appUserRole);
										dynamicModel.setYearMasterModel(yearMasterModel);
										dynamicModel.setStandardMasterModel(standardMasterModel);
										dynamicModel.setStudentMasterModel(studentMasterModel);
										dynamicModel.setSchoolMasterModel(schoolMasterModel);
										String incrementedId = ""+dynamicStudentDetailsRepository.getMaxId(studentAdmissionBean.getSchoolId());
										String dynamicId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
										dynamicModel.setDynamicDetailsId(Integer.parseInt(dynamicId));
										dynamicStudentDetailsRepository.saveAndFlush(dynamicModel);
									}
								}
							}
						}
						preTitle=dynamicCheckTitleArr[i];
						preVal=dynamicCheckValueArr[i];
					//}
					
				}
			}
			
			String[] dynamicTitleArr2 = studentAdmissionBean.getDynamicTitleArr2();
			String[] dynamicValueArr2 = studentAdmissionBean.getDynamicValueArr2();
			/*if(dynamicValueArr2.length>0) {
				List<DynamicFieldStudentInformationModel>  dynamicStudentList = new ArrayList<>();
				if(CollectionUtils.isNotEmpty(studentMasterList)) {
					 dynamicStudentList = dynamicFieldStudentInfoRepository.findByStudentMasterModel(studentMasterModel);
				}
				DynamicFieldStudentInformationModel dynamicModel = new DynamicFieldStudentInformationModel();
						
				if(CollectionUtils.isNotEmpty(dynamicStudentList)) {
					dynamicModel=dynamicStudentList.get(0);
				}
				else {
					String incrementedId = ""+dynamicFieldStudentInfoRepository.getMaxId(studentAdmissionBean.getSchoolId());
					String dynamicId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					dynamicModel.setInfoId(Integer.parseInt(dynamicId));
				}
				
				for(int i=0;i<dynamicValueArr2.length;i++) {
					if(StringUtils.isNotEmpty(dynamicValueArr2[i])) {
						List<DynamicAdmissionFiledsModel> dynamicList= dynamicAdmissionFieldRepository.getValueToPopulateForDynamic2Field(studentAdmissionBean.getSchoolId(),dynamicTitleArr2[i]);
						saveDynamicFields(dynamicValueArr2, i, dynamicModel, dynamicList);
					}
				}
				dynamicModel.setStudentMasterModel(studentMasterModel);
				dynamicModel.setSchoolMasterModel(schoolMasterModel);
				dynamicModel.setUserId(appUserRole);
				
				dynamicFieldStudentInfoRepository.saveAndFlush(dynamicModel);
			}*/
			
			
			addAttachment(studentAdmissionBean, file, filename, studentMasterModel,globalDbSchoolKey,appUserRole);
		}else {
			addStudentImg(studentAdmissionBean, file, filename, studentMasterModel,globalDbSchoolKey);
		}
			return studentMasterModel;
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return new StudentMasterModel();
	}

	private void saveDynamicFields(String[] dynamicValueArr2, int i, DynamicFieldStudentInformationModel dynamicModel,
			List<DynamicAdmissionFiledsModel> dynamicList) {
		if(dynamicList.get(0).getFieldtype().equals("textbox1")) {
			dynamicModel.setTextbox1(dynamicValueArr2[i]);
		}
		if(dynamicList.get(0).getFieldtype().equals("textbox2")) {
			dynamicModel.setTextbox2(dynamicValueArr2[i]);
		}
		if(dynamicList.get(0).getFieldtype().equals("textbox3")) {
			dynamicModel.setTextbox3(dynamicValueArr2[i]);
		}
		if(dynamicList.get(0).getFieldtype().equals("textbox4")) {
			dynamicModel.setTextbox4(dynamicValueArr2[i]);
		}
		if(dynamicList.get(0).getFieldtype().equals("textbox5")) {
			dynamicModel.setTextbox5(dynamicValueArr2[i]);
		}
		if(dynamicList.get(0).getFieldtype().equals("combobox1")) {
			DynamicCombobox1 Dynamiccombobox1= new DynamicCombobox1();
			Dynamiccombobox1.setComboId1(Integer.parseInt(dynamicValueArr2[i]));
			dynamicModel.setDynamiccombobox1(Dynamiccombobox1);
		}
		if(dynamicList.get(0).getFieldtype().equals("combobox2")) {
			DynamicCombobox2 Dynamiccombobox2= new DynamicCombobox2();
			Dynamiccombobox2.setComboId2(Integer.parseInt(dynamicValueArr2[i]));
			dynamicModel.setDynamiccombobox2(Dynamiccombobox2);
		}
		if(dynamicList.get(0).getFieldtype().equals("combobox3")) {
			DynamicCombobox3 Dynamiccombobox3= new DynamicCombobox3();
			Dynamiccombobox3.setComboId3(Integer.parseInt(dynamicValueArr2[i]));
			dynamicModel.setDynamiccombobox3(Dynamiccombobox3);
		}
		if(dynamicList.get(0).getFieldtype().equals("combobox4")) {
			DynamicCombobox4 Dynamiccombobox4= new DynamicCombobox4();
			Dynamiccombobox4.setComboId4(Integer.parseInt(dynamicValueArr2[i]));
			dynamicModel.setDynamiccombobox4(Dynamiccombobox4);
		}
		if(dynamicList.get(0).getFieldtype().equals("combobox5")) {
			DynamicCombobox5 Dynamiccombobox5= new DynamicCombobox5();
			Dynamiccombobox5.setComboId5(Integer.parseInt(dynamicValueArr2[i]));
			dynamicModel.setDynamiccombobox5(Dynamiccombobox5);
		}
		
	}
	
	private void addAttachment(StudentAdmissionBean studentMasterBean, List<MultipartFile> file, List<String> filename,
			StudentMasterModel studentMasterModel,String globalDbSchoolKey,AppUserRoleModel appUserRole) 
	{
		if(!filename.get(0).equals("NoFile")) {
			studentAttachmentRepository.deleteBy(studentMasterBean.getStudentId());
		} /*
			 * else if(filename.get(0).equals("studentPhoto")) {
			 * addStudentImg(studentMasterBean, file, filename,
			 * studentMasterModel,globalDbSchoolKey); }
			 */
		
		if(studentMasterBean.getAttachmentIdArr() != null && studentMasterBean.getAttachmentIdArr().length>0) {
			for(int i=0;i<studentMasterBean.getAttachmentIdArr().length;i++) {
				if(!(studentMasterBean.getAttachmentIdArr()[i].equals("default"))) {
					StudentAttachmentModel studentAttachmentModel = new StudentAttachmentModel();
					AttachmentMasterModel attachmentMasterModel = new AttachmentMasterModel();
					attachmentMasterModel.setAttachmentMasterId(Integer.parseInt(studentMasterBean.getAttachmentIdArr()[i]));
					studentAttachmentModel.setAttachmentMasterModel(attachmentMasterModel);
					studentAttachmentModel.setUserId(appUserRole);
					studentAttachmentModel.setStudentMasterModel(studentMasterModel);
					SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
					schoolMasterModel.setSchoolid(studentMasterBean.getSchoolId());
					studentAttachmentModel.setSchoolMasterModel(schoolMasterModel );
					studentAttachmentModel.setFileName("");
					studentAttachmentModel.setFilePath("");
					String incrementedId = ""+studentAttachmentRepository.getMaxId(studentMasterBean.getSchoolId());
					String attachmentId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					studentAttachmentModel.setStudentAttachmentId(Integer.parseInt(attachmentId));
					StringBuffer attachmentFileName = new StringBuffer();
					String folderName = null;
					if (!file.get(i).isEmpty() && StringUtils.isNotEmpty(filename.get(i))) {
						String[] splitFileName = filename.get(i).toString().split("\\.");
						attachmentFileName.append(studentMasterBean.getAcademicYear()).append("_").append(studentMasterBean.getStandard()).append("_").append(studentMasterBean.getAttachmentIdNameArr()[i]).append("_").append(studentMasterBean.getRegistrationNumber()).append(".").append(splitFileName[1]);
						folderName = "StudentAttachment"+File.separator+studentMasterBean.getSchoolKey()+File.separator+studentMasterBean.getAcademicYear()+File.separator+studentMasterBean.getStandard();
						String target = HttpClientUtil.uploadImage(file.get(i),filename.get(i),folderName,studentMasterBean.getSchoolKey());
						String path = null;
						//Upload attachment on s3
						try {
							String postUri= env.getProperty("awsUploadUrl");
							URI uri = new URI(postUri);
							File convFile = new File(attachmentFileName.toString());
						    convFile.createNewFile();
						    FileOutputStream fos = new FileOutputStream(convFile);
						    fos.write(file.get(i).getBytes());
						    fos.close();
							UploadFileBean bean = new UploadFileBean();
							bean.setFileName(attachmentFileName.toString());
							bean.setFile(convFile);
							bean.setFilePath("maindb"+"/"+folderName);
							path=uploadSingleFile(bean);
//							path = restTemplate.postForObject(uri, bean, String.class);
						 	convFile.delete();
						} catch (NumberFormatException | URISyntaxException | IOException e) {
							e.printStackTrace();
						} 
						//Upload attachment on s3
						if(!path.isEmpty())
						{	
							studentAttachmentModel.setFileName(attachmentFileName.toString());
							studentAttachmentModel.setFilePath(path);
						}else {
							studentAttachmentModel.setFileName(attachmentFileName.toString());
							studentAttachmentModel.setFilePath(target);
						}
					}
					 else if(StringUtils.isNotEmpty(filename.get(i)))
					 {
							 studentAttachmentModel.setFileName(filename.get(i));
							 studentAttachmentModel.setFilePath(studentMasterBean.getFilePath()[i]);
					 }
					studentAttachmentRepository.saveAndFlush(studentAttachmentModel);
				}
			}
		}
	}
	
	
	public String uploadSingleFile (UploadFileBean fileBean) {
		try {
//			String bucketName = "eprashasan1";
			String bucketName = "eprashasan2";
			String key = fileBean.getFilePath();
			key = key.replace("\\", "/");
			String returnPath = UploadFile(bucketName, key, fileBean.getFile());
			return  returnPath;
		}catch(Exception e) {
			return null;
		}
	}
	
	public String UploadFile(String bucketName,String keyName, File file){
		try{
			keyName = keyName+"/"+file.getName();
			keyName = keyName.replace(" ", "_");
			amazonS3.putObject(new PutObjectRequest(bucketName, keyName, file).withCannedAcl(CannedAccessControlList.PublicRead));
			return ((AmazonS3Client) amazonS3).getResourceUrl(bucketName, keyName);
		}catch (AmazonServiceException e){
			e.printStackTrace();
		}
		return "";
	}
	
	@Transactional
	private void addStudentImg(StudentAdmissionBean studentMasterBean, List<MultipartFile> file, List<String> filename,StudentMasterModel studentMasterModel,String globalDbSchoolKey) 
	{
			if(filename.size()>0) {
				StringBuffer attachmentFileName = new StringBuffer();
				if (!file.get(1).isEmpty() && StringUtils.isNotEmpty(filename.get(1))) {
					String[] splitFileName = filename.get(1).toString().split("\\.");
					attachmentFileName.append(studentMasterBean.getStudRegNo()).append(".").append(splitFileName[1]);
					String folderName = "cms\\IcardImages"+File.separator+studentMasterBean.getSchoolKey()+File.separator+studentMasterBean.getSchoolId();
					String target = HttpClientUtil.uploadImage(file.get(1),attachmentFileName.toString(),folderName,studentMasterBean.getSchoolKey());

//					Upload attachment on s3
					try {
						String postUri= env.getProperty("awsUploadUrl");
						URI uri = new URI(postUri);
						File convFile = new File(attachmentFileName.toString());
					    convFile.createNewFile();
					    FileOutputStream fos = new FileOutputStream(convFile);
					    fos.write(file.get(1).getBytes());
					    fos.close();
						UploadFileBean bean = new UploadFileBean();
						bean.setFileName(attachmentFileName.toString());
						bean.setFile(convFile);
						bean.setFilePath("maindb"+"/"+folderName);
						String path = restTemplate.postForObject(uri, bean, String.class);
					 	convFile.delete();
					 	
					 	//String regNo = studentMasterBean.getRegistrationNumber();
				 		IcardMaster icardMaster = icardMasterRepository.getIdcardMaster(studentMasterBean.getStudentId(),studentMasterBean.getStudRegNo());
				 		SchoolMasterModel schoolMaster = new SchoolMasterModel();
						schoolMaster.setSchoolid(studentMasterBean.getSchoolId());
						AppUserRoleModel appUserRole = new AppUserRoleModel();
						appUserRole.setAppUserRoleId(1000100046);
				 		if(icardMaster==null) {
							icardMaster = new IcardMaster();
							String maxId = icardMasterRepository.getMaxId(studentMasterBean.getSchoolId());
					 		icardMaster.setIcardId(Integer.parseInt(maxId));
							icardMaster.setSchoolId(schoolMaster);
							icardMaster.setStudId(studentMasterBean.getStudentId());
							icardMaster.setRenewId(studentMasterBean.getStudentRenewId());
							icardMaster.setRegno(studentMasterBean.getStudRegNo());
							if(!path.equals("") && !path.equals(null)) {
								icardMaster.setImagePath(path);
							}else {
								icardMaster.setImagePath(target);
							}
							icardMaster.setUserId(appUserRole);
							icardMaster.setOriginalName(attachmentFileName.toString());
							icardMaster.setcDate(new Date());
							//icardMasterRepository.saveICardMaster(icardMaster);
						}else {
							if(!path.equals("") && !path.equals(null)) {
								icardMaster.setImagePath(path);
							}else {
								icardMaster.setImagePath(target);
							}
							icardMaster.setEditDate(new Date());
							icardMaster.setEditBy(appUserRole);
						}
				 		
						icardMasterRepository.save(icardMaster);
					 	
					} catch (NumberFormatException | URISyntaxException | IOException e) {
						e.printStackTrace();
					} 
					//Upload attachment on s3
				}
			}
			
			// Remove comment to upload signature image
			/*
			if(filename.size()>0) {
				StringBuffer attachmentFileName = new StringBuffer();
				if (!file.get(2).isEmpty() && StringUtils.isNotEmpty(filename.get(2))) {
					String[] splitFileName = filename.get(2).toString().split("\\.");
					attachmentFileName.append("sign_"+studentMasterBean.getStudRegNo()).append(".").append(splitFileName[1]);
					String folderName = "cms\\IcardImages"+File.separator+studentMasterBean.getSchoolKey()+File.separator+studentMasterBean.getSchoolId();
					String target = HttpClientUtil.uploadImage(file.get(2),attachmentFileName.toString(),folderName,studentMasterBean.getSchoolKey());
				}
			}
			*/
	}
	
	@Override
	public List<AdmissionSettingBean> getTabsAndLegendsSetting(Integer schoolId) {
		try {
			return designAdmiFormLegendRepository.getTabsAndLegendsSetting(schoolId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>(); 
	}

	@Override
	public List<AdmissionSettingBean> getValueToPopulateForDynamicField(Integer schoolId, Integer fieldId) {
		try {
			return designAdmiFormDynamicFieldValueRepository.getValueToPopulateForDynamicField(schoolId,fieldId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>(); 
	}

	@Override
	public StudentAdmissionBean getStudentDetails(String username) {
		//List<StudentAdmissionBean> list=  studentMasterRepository.findDetailsByUser(username);
		List<StudentAdmissionBean> list= new ArrayList<>();
		if(CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		else return new StudentAdmissionBean();
	}

	@Override
	public String getSchoolName(Integer schoolId) {
		
		SchoolMasterModel schoolMasterModel =  schoolMasterRepository.findBySchoolid(schoolId);
		if(schoolMasterModel != null) {
			return schoolMasterModel.getSchoolName();
		}
		
		return "";
		
	}
	

	public String getGlobalSchoolName(Integer schoolId) {
		
		SchoolMasterModel schoolMasterModel =  schoolMasterRepository.findBySchoolid(schoolId);
		if(schoolMasterModel != null) {
			return schoolMasterModel.getSchoolNameGL();
		}
		
		return "";
		
	}

	@Override
	public List<AdmissionSettingBean> getValueToPopulateForDynamic2Field(Integer schoolId, String fieldName) {
		try {
			List<DynamicAdmissionFiledsModel> dynamicList= dynamicAdmissionFieldRepository.getValueToPopulateForDynamic2Field(schoolId,fieldName);
			if(CollectionUtils.isNotEmpty(dynamicList) && dynamicList.get(0).getFieldTypeFlag()!=1) {
				return dynamicAdmissionFieldRepositoryImpl.getComboValues(schoolId,dynamicList.get(0).getFieldId(),dynamicList.get(0).getFieldtype());	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>(); 
	}

	@Override
	public List<StudentDynamicDetailsBean> getStudentAssignedSubjectsForEdit(String studentId) {
		return assignSubjectToStudentRepository.getAssignedSubjects(studentId);
		
	}
	
	@Override
	public List<StudentDynamicDetailsBean> getAttachmentsForEdit(Integer studentId) {
		 List<StudentDynamicDetailsBean>studentAttachmentList=studentAttachmentRepository.getAttachmentDetails(studentId);
		 studentAttachmentList.parallelStream().forEach( s-> {
			 s.setFilePath(utillity.getS3PreSignedUrl(s.getFilePath()));
		 });
		 return  studentAttachmentList;
	}
	
	
	@Override
	public List<StudentDynamicDetailsBean> getAttachmentsForEditWithImages(Integer studentId) {
		List<StudentDynamicDetailsBean>studentAttachmentList=studentAttachmentRepository.getAttachmentDetails(studentId);
		 studentAttachmentList.parallelStream().forEach( s-> {
			 s.setFilePath(utillity.getS3PreSignedUrl(s.getFilePath()));
		 });
	 	 String base64Image1 = "";
	 	 String imageName1 = "";
		 for(int i=0;i<studentAttachmentList.size();i++)
		 {
			 StudentDynamicDetailsBean studentDynamicDetailsBean = studentAttachmentList.get(i);
			 String fileFullPath = studentAttachmentList.get(i).getFilePath().replace('\\', '/');;
			 fileFullPath = fileFullPath.replace(" ", "%20");
			 byte[] newbytes1 = null;
				try {
			 InputStream inputStream = new BufferedInputStream(new URL(fileFullPath).openStream());
		    	byte[] target = ByteStreams.toByteArray(inputStream);
		    	base64Image1 = Base64.getEncoder().encodeToString(target);
	    		inputStream.close();
	    		studentDynamicDetailsBean.setStudentDocImageString(base64Image1);
	    	
				}
				catch (Exception e) {
					e.printStackTrace();
					return new ArrayList<>();
				}
			
			studentAttachmentList.set(i, studentDynamicDetailsBean) ;
		 
		 }
		return  studentAttachmentList;
	}
	
	@Override
	public List<StudentDynamicDetailsBean> getDynamicDetails2ForEdit(Integer studentId,Integer schoolId) {
		List<StudentDynamicDetailsBean> beanList = new ArrayList<>();
		StudentMasterModel studentMasterModel = new StudentMasterModel();
		studentMasterModel.setStudentId(studentId);
		
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		List<DynamicAdmissionFiledsModel> list1 =  dynamicAdmissionFiledsRepository.findBySchoolMasterModelOrderByFieldId(schoolMasterModel);
		List<DynamicFieldStudentInformationModel> list2 =  dynamicFieldStudentInfoRepository.findByStudentMasterModel(studentMasterModel);

		if(CollectionUtils.isNotEmpty(list2)) {
			if(StringUtils.isNotEmpty(list2.get(0).getTextbox1())) {
				StudentDynamicDetailsBean bean = new StudentDynamicDetailsBean();
				bean.setDynamicTitle(list1.get(0).getFieldName());
				bean.setDynamicValue(list2.get(0).getTextbox1());
				beanList.add(bean);
			}
		
			if(StringUtils.isNotEmpty(list2.get(0).getTextbox2())) {
				StudentDynamicDetailsBean bean1 = new StudentDynamicDetailsBean();
				bean1.setDynamicTitle(list1.get(1).getFieldName());
				bean1.setDynamicValue(list2.get(0).getTextbox2());
				beanList.add(bean1);
			}
			
			if(StringUtils.isNotEmpty(list2.get(0).getTextbox3())) {
				StudentDynamicDetailsBean bean2 = new StudentDynamicDetailsBean();
				bean2.setDynamicTitle(list1.get(2).getFieldName());
				bean2.setDynamicValue(list2.get(0).getTextbox3());
				beanList.add(bean2);
			}
			
			if(StringUtils.isNotEmpty(list2.get(0).getTextbox4())) {
				StudentDynamicDetailsBean bean3 = new StudentDynamicDetailsBean();
				bean3.setDynamicTitle(list1.get(3).getFieldName());
				bean3.setDynamicValue(list2.get(0).getTextbox4());
				beanList.add(bean3);
			}
			
			if(StringUtils.isNotEmpty(list2.get(0).getTextbox5())) {
				StudentDynamicDetailsBean bean4 = new StudentDynamicDetailsBean();
				bean4.setDynamicTitle(list1.get(4).getFieldName());
				bean4.setDynamicValue(list2.get(0).getTextbox5());
				beanList.add(bean4);
			}
			
			if(list2.get(0).getDynamiccombobox1() !=null && list2.get(0).getDynamiccombobox1().getComboId1() !=null) {
				StudentDynamicDetailsBean bean5 = new StudentDynamicDetailsBean();
				bean5.setDynamicTitle(list1.get(5).getFieldName());
				bean5.setDynamicValue(""+list2.get(0).getDynamiccombobox1().getComboId1());
				beanList.add(bean5);
			}
			
			if(list2.get(0).getDynamiccombobox2() !=null && list2.get(0).getDynamiccombobox2().getComboId2() !=null) {
				StudentDynamicDetailsBean bean6 = new StudentDynamicDetailsBean();
				bean6.setDynamicTitle(list1.get(6).getFieldName());
				bean6.setDynamicValue(""+list2.get(0).getDynamiccombobox2().getComboId2());
				beanList.add(bean6);
			}
			
			if(list2.get(0).getDynamiccombobox3() !=null && list2.get(0).getDynamiccombobox3().getComboId3() !=null) {
				StudentDynamicDetailsBean bean7 = new StudentDynamicDetailsBean();
				bean7.setDynamicTitle(list1.get(7).getFieldName());
				bean7.setDynamicValue(""+list2.get(0).getDynamiccombobox3().getComboId3());
				beanList.add(bean7);
			}
			
			if(list2.get(0).getDynamiccombobox4() !=null && list2.get(0).getDynamiccombobox4().getComboId4() !=null) {
				StudentDynamicDetailsBean bean8 = new StudentDynamicDetailsBean();
				bean8.setDynamicTitle(list1.get(8).getFieldName());
				bean8.setDynamicValue(""+list2.get(0).getDynamiccombobox4().getComboId4());
				beanList.add(bean8);
			}
			
			if(list2.get(0).getDynamiccombobox5() !=null && list2.get(0).getDynamiccombobox5().getComboId5() !=null) {
				StudentDynamicDetailsBean bean9 = new StudentDynamicDetailsBean();
				bean9.setDynamicTitle(list1.get(9).getFieldName());
				bean9.setDynamicValue(""+list2.get(0).getDynamiccombobox5().getComboId5());
				beanList.add(bean9);
			}
		}
		return beanList;
	}
	
	@Override
	public List<StudentDynamicDetailsBean> getDynamicDetails1ForEdit(Integer studentId) {
		return  dynamicStudentDetailsRepository.getDynamicDetails1ForEdit(studentId);
	}

	@Override
	public List<StudentAdmissionBean> getStudentDetailsForEdit(Integer studentId) {
		return studentMasterRepository.findDetailsByStudentId(studentId);
	}

	@Override
	public String getFormNumber(Integer schoolId) {
		return studentMasterRepository.findMaxFormNumber(schoolId);
	}
	
	public static String md5(String input) {		
		String md5 = null;		
		if (null == input)
			return null;
		
		try {
			// Create MessageDigest object for MD5
			MessageDigest digest = MessageDigest.getInstance("MD5");			
			// Update input string in message digest
			digest.update(input.getBytes(), 0, input.length());			
			// Converts message digest value in base 16 (hex)
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return md5;
	}
	
	@SuppressWarnings("null")
	@Override
	public String getAdmissionPrint(String studRegNo,Integer schoolId,String yearComboVal, String std, String admissionDateSpan,String schoolName,String standardText,HttpServletResponse response) {
		//String listSize="0";
		try{	
			List<StudentAdmissionBean> exePath=printExcelStudentRepositoryImpl.getExeFolderPath();
//			System.out.println("EXEPATH : "+exePath.get(0).getFirstName());
			List<StudentAdmissionBean> studentDatalist=printExcelStudentRepositoryImpl.getstudValues(studRegNo,schoolId);
			List<StudentAdmissionBean> newDynamicFieldsWithValue=printExcelStudentRepositoryImpl.getNewDynamicFieldsValues(studRegNo,schoolId, studentDatalist.get(0).getStudId());
			List<StudentAdmissionBean> getAssignedSubjectToStudent=printExcelStudentRepositoryImpl.getAssignedSubjectToStudent(studRegNo,schoolId,yearComboVal,std);
			
			String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
			
			YearMasterModel yearMasterModel=yearMasterRepository.getYearId(studentDatalist.get(0).getAcademicYearText(),schoolId);
			String roasterFlag="0";
			List list=printExcelStudentRepositoryImpl.getAdmissionExcelFilePath(yearMasterModel.getYearId(),Integer.parseInt(std),schoolId,roasterFlag);
			
//			String absoluteExcelFilesPath = null;
			String fileName = null;
			
			String studentId=studentDatalist.get(0).getStudId();
			
//		    absoluteExcelFilesPath= exePath.get(0).getFirstName();
		    fileName="AdmissionExcel-"+studentId+"-"+studRegNo+"-"+yearComboVal+".xlsx";
		    
//		  //open source excel
//		    InputStream template = new FileInputStream(absoluteExcelFilesPath+"/cms/ExcelAdmissionFiles/"+schoolKey+"/"+yearComboVal+"/"+standardText+"/AdmissionForm.xlsx");
//		    Workbook wb = WorkbookFactory.create(template);
//
//		    //Saving excel to a different location or filename. 
//		    FileOutputStream out2 = new FileOutputStream(absoluteExcelFilesPath+"/cms/ExcelAdmissionFiles/"+schoolKey+"/"+yearComboVal+"/"+standardText+"/"+fileName);
//		    wb.write(out2);
//		    //wb.close();
//		    out2.close();
//		    template.close();
			
		    String awsFilePath = null;
			if(list.size()>0) {
				awsFilePath=list.get(0).toString();
			}
			String absoluteExcelFilesPath= exePath.get(0).getFirstName()+"/cms/ExcelAdmissionFiles/"+schoolKey+"/"+yearComboVal+"/"+standardText+"/";
			File f=new File(absoluteExcelFilesPath);
			if(!f.exists()){
				f.mkdirs();
			}

			byte[] newbytes = AWSUtility.downloadFile(awsFilePath);
			File f1  =  new File(absoluteExcelFilesPath,fileName);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f1));
			stream.write(newbytes);
			stream.close();
			
			FileInputStream in = new FileInputStream(absoluteExcelFilesPath+"/"+fileName);
			
            XSSFWorkbook workbook = new XSSFWorkbook(in); 
            XSSFSheet sheet = workbook.getSheetAt(0);
			
			
			//XSSFWorkbook workbook = new XSSFWorkbook();
			//XSSFSheet sheet = workbook.createSheet("Student Details");
			//XSSFSheet sheet = workbook.getSheetAt(0);
			
			int noOfRow=0;
			int noOfCell=0;
			
			int noOfRow2=4;
			int noOfCell2=3;
			
			XSSFRow row = sheet.createRow(noOfRow++); //create 1 Row
			row.createCell(noOfCell++).setCellValue("ePrashasan");
			row.createCell(noOfCell++).setCellValue("");
			row.createCell(noOfCell++).setCellValue(schoolName);
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 2 Row
			row.createCell(noOfCell++).setCellValue("Year : ");
			row.createCell(noOfCell++).setCellValue(yearComboVal);
			row.createCell(noOfCell++).setCellValue("");
			row.createCell(noOfCell++).setCellValue("Standard : ");
			row.createCell(noOfCell++).setCellValue(standardText);
			row.createCell(noOfCell++).setCellValue("");
			row.createCell(noOfCell++).setCellValue("Date : ");
			row.createCell(noOfCell++).setCellValue(admissionDateSpan);
			
		//  ----------   For Student Basic Details AND OLD Dynamic Fields OF Student  ---------------	
			
		//if(studentDatalist.size()>0 && ! StringUtils.isEmpty(studentDatalist)) {
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			row.createCell(noOfCell++).setCellValue("Roll No");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getRollNo());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Reg No");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getRegistrationNumber());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Student Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getFirstName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			row.createCell(noOfCell++).setCellValue("Religion Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getReligionName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Category Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCategoryName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			row.createCell(noOfCell++).setCellValue("Caste Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCasteName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Gender");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getGender());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			row.createCell(noOfCell++).setCellValue("Mother Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getMotherName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Father Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getFatherName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			row.createCell(noOfCell++).setCellValue("Birth Date");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBirthDate());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Accademic Year");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getAcademicYearText());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			row.createCell(noOfCell++).setCellValue("Standard Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStandardName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Division Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getDivName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 3 Row
			row.createCell(noOfCell++).setCellValue("Mobile No");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getmMobile());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Telephone No");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getfMobile());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Occupation ");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getOccupation());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Student Renew Id");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStudentRenewId());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("TextBox1");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox1());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("TextBox2");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox2());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("TextBox3");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox3());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("TextBox4");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox4());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("TextBox5");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox5());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("ComboBox1");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox1());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("ComboBox2");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox2());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("ComboBox3");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox3());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("ComboBox4");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox4());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("ComboBox5");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox5());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("SurName");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStudLName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("FirstName");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStudFName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Mother Tongue");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getMothertongue());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Birth Place");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBirthPlace());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Permenant Address");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getPermanentAddress());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Permanent State");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getPermanentState());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Permanent PinCode");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getPermanentPinCode());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Local Address");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getLocalAddress());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Local State");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getLocalState());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Local PinCode");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getLocalPinCode());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Parent Email-ID");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getParentEmailID());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Student Email-ID");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getEmailID());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("AadhaarCard No.");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getAadhaarCardNo());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Bank Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBankName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Branch Name");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBranchName());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Bank IFSC No.");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBankIFSCNo());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Bank Account No.");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getAccountNo());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Form No.");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getFormNumber());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Nationality");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getNationality());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Income");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getIncome());
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			row.createCell(noOfCell++).setCellValue("Qualification");
			row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getEducation());
			
		//}	
		//  ----------   For Student Basic Details AND OLD Dynamic Fields OF Student  END---------------		
			
			
		//  ----------   For Assign Subjects to Student  ---------------	
			if(getAssignedSubjectToStudent.size()>0) {
				for(int i=0;i<getAssignedSubjectToStudent.size();i++) {
					noOfCell2=3;
					row = sheet.getRow(noOfRow2+i); //create 4 Row
					row.createCell(noOfCell2).setCellValue(getAssignedSubjectToStudent.get(i).getFirstName());
				}
			}
			
		//  ----------   For Assign Subjects to Student END ---------------	
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			
			noOfCell=0;
			row = sheet.createRow(noOfRow++); //create 4 Row
			
		//  ----------   For Dynamic Fields OF Student  ---------------	
			
			if(newDynamicFieldsWithValue.size()>0 &&  ! org.springframework.util.StringUtils.isEmpty(newDynamicFieldsWithValue)) {
				for(int i=0;i<newDynamicFieldsWithValue.size();i++) {
					noOfCell=0;
					row = sheet.createRow(noOfRow++); //create 4 Row
					row.createCell(noOfCell++).setCellValue(newDynamicFieldsWithValue.get(i).getFirstName());
					row.createCell(noOfCell++).setCellValue(newDynamicFieldsWithValue.get(i).getGender());
				}
			}
		//  ----------   For Dynamic Fields OF Student END  ---------------	
			
			
			
			
			
			// Start Set Latter Head Images
			int col1[]=new int[11];
			int col2[]=new int[11];
			int row1[]=new int[11];
			int row2[]=new int[11];
						
			String dollerName[]=new String[11];	
			String ifUse[]=new String[11];
			String from[]=new String[11];
			String imageName[]=new String[11];
			int sheetNo[]=new int[11];
			String repited[]=new String[11];
			int totalCol[]=new int[11];
			int totalRow[]=new int[11];
			
			int rowCount=0;
			int collCount=0;
			// Start Read Excel Setting
            // Iterate through each rows one by one 
			sheet = workbook.getSheetAt(3); //Sheet Number 4
            Iterator<Row> rowIterator = sheet.iterator(); 
            while (rowIterator.hasNext()) { 
            	collCount=0;
            	Row rows = rowIterator.next();
            	if(rowCount==0){
            		rows = rowIterator.next();
            	}
                // For each row, iterate through all the columns 
                Iterator<Cell> cellIterator = rows.cellIterator();   
                while (cellIterator.hasNext()) { 
                    Cell cell1 = cellIterator.next();                     
                    collCount++;
                    // Check the cell type and format accordingly 
                    switch (collCount) {
                    	case 1: //Doller Name
                    		dollerName[rowCount]=""+cell1; 
	                        break; 
	                    case 2: //If Use
	                    	ifUse[rowCount]=""+cell1.toString();
	                    	break;
	                    case 3: //From
	                    	from[rowCount]=""+cell1.toString();
	                    	break;
	                    case 4: //Image Name
	                    	imageName[rowCount]=""+cell1.toString();
	                    	break;
	                    case 5: //Sheet No
	                    	if(cell1.toString().contains(".")){
                    			sheetNo[rowCount]=Integer.parseInt(""+cell1.toString().substring(0,cell1.toString().lastIndexOf(".")));
                    		}else {
                    			sheetNo[rowCount]=Integer.parseInt(""+cell1.toString());
							}
	                    	break; 
	                    case 6: //Repited
	                    	repited[rowCount]=""+cell1.toString();
	                    	break; 
	                    case 7: //col1
	                    	if(cell1.toString().contains(".")){
                    			col1[rowCount]=Integer.parseInt(""+cell1.toString().substring(0,cell1.toString().lastIndexOf(".")));
                    		}else {
                    			col1[rowCount]=Integer.parseInt(""+cell1.toString());
							}
	                    	break; 
	                    case 8: //col2
	                    	if(cell1.toString().contains(".")){
                    			col2[rowCount]=Integer.parseInt(""+cell1.toString().substring(0,cell1.toString().lastIndexOf(".")));
                    		}else {
                    			col2[rowCount]=Integer.parseInt(""+cell1.toString());
							}
	                        break; 
	                    case 9: //row1
	                    	if(cell1.toString().contains(".")){
                    			row1[rowCount]=Integer.parseInt(""+cell1.toString().substring(0,cell1.toString().lastIndexOf(".")));
                    		}else {
                    			row1[rowCount]=Integer.parseInt(""+cell1.toString());
							}
	                    	break; 
	                    case 10: //row2
	                    	if(cell1.toString().contains(".")){
                    			row2[rowCount]=Integer.parseInt(""+cell1.toString().substring(0,cell1.toString().lastIndexOf(".")));
                    		}else {
                    			row2[rowCount]=Integer.parseInt(""+cell1.toString());
							}
	                        break; 
	                    case 11: //Total Cols
	                    	if(cell1.toString().contains(".")){
                    			totalCol[rowCount]=Integer.parseInt(""+cell1.toString().substring(0,cell1.toString().lastIndexOf(".")));
                    		}else {
                    			totalCol[rowCount]=Integer.parseInt(""+cell1.toString());
							}
	                    	break; 
	                    case 12: //Total Rows
	                    	if(cell1.toString().contains(".")){
                    			totalRow[rowCount]=Integer.parseInt(""+cell1.toString().substring(0,cell1.toString().lastIndexOf(".")));
                    		}else {
                    			totalRow[rowCount]=Integer.parseInt(""+cell1.toString());
							}
	                        break; 
                    }
                } 
                rowCount++;
            }
            // End Read Excel Setting
            
            String folderName = ""+absoluteExcelFilesPath+"cms\\IcardImages"+File.separator+schoolKey+File.separator+schoolId;
            
            try { //Get Images From Folder
				for(int dd=0;dd<10;dd++)	{
					if(from[dd].equalsIgnoreCase("Folder") && ifUse[dd].equalsIgnoreCase("Yes")){
						if(totalRow[dd]!=0){
							sheet = workbook.getSheetAt((sheetNo[dd]-1));
							if(studentDatalist!=null && !studentDatalist.isEmpty()){
								for(int k=0;k<studentDatalist.size();k++)	{
									//Object [] obj1 = (Object[]) studentDatalist.get(k);
									File imgfile=null;
									if(dollerName[dd].equalsIgnoreCase("$Bonafide")){
										imgfile = new File(absoluteExcelFilesPath+"cms\\Certificates\\Student_Bonafied_Image"+File.separator+schoolKey+"/Bonafied_Image.jpg"); //BonafiedImg
									}
									if(dollerName[dd].equalsIgnoreCase("$TC")){
										imgfile = new File(absoluteExcelFilesPath+"cms\\Certificates\\Student_TC_Image"+File.separator+schoolKey+"/TC_Image.jpg"); //TCImg
									}
									if(dollerName[dd].equalsIgnoreCase("$PrincipalSignature")){
										imgfile = new File(absoluteExcelFilesPath+"cms\\Principal_Sign_Image"+File.separator+schoolKey+"/Principal_Sign.jpg"); //PrincipalImg
									}
									if(dollerName[dd].equalsIgnoreCase("$IdCardBackgroundImg")){
										imgfile = new File(absoluteExcelFilesPath+"cms\\ID_Card_Background_Image"+File.separator+schoolKey+"/ID_Card_Background.jpg"); //IdCardBackgroundImg
									}
									if(dollerName[dd].equalsIgnoreCase("$StudPhoto")){
										imgfile = new File(folderName+"/"+studRegNo+".jpg"); //StudPhoto
									}
									if(dollerName[dd].equalsIgnoreCase("$StudSign")){
										imgfile = new File(folderName+"/sign_"+studRegNo+".jpg"); //StudSignature
									}
									if(dollerName[dd].equalsIgnoreCase("$ImgPath1")){
										imgfile = new File(imageName[dd]); //ImgPath1
									}
									if(dollerName[dd].equalsIgnoreCase("$ImgPath2")){
										imgfile = new File(imageName[dd]); //ImgPath2
									}
									if(dollerName[dd].equalsIgnoreCase("$ImgPath3")){
										imgfile = new File(imageName[dd]); //ImgPath3
									}
									if(dollerName[dd].equalsIgnoreCase("$ImgPath4")){
										imgfile = new File(imageName[dd]); //ImgPath4
									}
									/*if(dollerName[dd].equalsIgnoreCase("$ImgPath5")){
										imgfile = new File(imageName[dd]); //ImgPath5
									}*/
									if(repited[dd].equalsIgnoreCase("Yes")){
										if(imgfile.exists() && !imgfile.isDirectory()) { 
											InputStream is = new FileInputStream(imgfile);
											byte[] bytes = IOUtils.toByteArray(is);
											int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
											CreationHelper helper = workbook.getCreationHelper();
											// Create the drawing patriarch. This is the top level container for all shapes. 
											Drawing drawing = sheet.createDrawingPatriarch();
											//add a picture shape		
											ClientAnchor anchor = helper.createClientAnchor();
											//set top-left corner of the picture,
											//subsequent call of Picture#resize() will operate relative to it
											anchor.setCol1(col1[dd]);
											anchor.setCol2(col2[dd]);
											anchor.setRow1(row1[dd]);
											anchor.setRow2(row2[dd]);				
											Picture pict = drawing.createPicture(anchor, pictureIdx);
											//auto-size picture relative to its top-left corner
											//pict.resize(1);
										}
										row1[dd]=row1[dd]+totalRow[dd];
										row2[dd]=row2[dd]+totalRow[dd];
									}else{
										if(imgfile.exists() && !imgfile.isDirectory()) { 
											InputStream is = new FileInputStream(imgfile);
											byte[] bytes = IOUtils.toByteArray(is);
											int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
											CreationHelper helper = workbook.getCreationHelper();
											// Create the drawing patriarch. This is the top level container for all shapes. 
											Drawing drawing = sheet.createDrawingPatriarch();
											//add a picture shape		
											ClientAnchor anchor = helper.createClientAnchor();
											//set top-left corner of the picture,
											//subsequent call of Picture#resize() will operate relative to it
											anchor.setCol1(col1[dd]);
											anchor.setCol2(col2[dd]);
											anchor.setRow1(row1[dd]);
											anchor.setRow2(row2[dd]);				
											Picture pict = drawing.createPicture(anchor, pictureIdx);
											//auto-size picture relative to its top-left corner
										//	pict.resize(1);
										}
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}			
			// End Set Latter Head Images
			
			
			
			if(exePath.size()>0) {
			    
			    //fileName="AdmissionExcel-"+studRegNo+"-"+std+"-"+yearComboVal+".xlsx";
		    	FileOutputStream out = new FileOutputStream(new File(absoluteExcelFilesPath+fileName));
		        workbook.write(out);
		        out.close();
			}
	      //workbook.close();
	        
	        /*Path file = Paths.get(absoluteExcelFilesPath, fileName);
	        if (Files.exists(file)) {
				response.setContentType("application/force-download");
				response.addHeader("Content-Disposition", "attachment; filename="+fileName);
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush(); 
	        }
	        File file1=new File(""+file);
	        file1.delete();*/
			
          //  -----------------------------  Refresh Excel File   ------------------------------
//			refreshExcelFile(new File(absoluteExcelFilesPath+"/cms/ExcelAdmissionFiles/"+schoolKey+"/"+yearComboVal+"/"+standardText+"/"+fileName), absoluteExcelFilesPath.replace("/MainDB/", ""));
		  //  -----------------------------  Refresh Excel File  END ------------------------------				
			
			
			return absoluteExcelFilesPath +"/#" + fileName;
//			return absoluteExcelFilesPath+"/cms/ExcelAdmissionFiles/"+schoolKey+"/"+yearComboVal+"/"+standardText+"/#"+fileName;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
	}
	
	
	
	/*@SuppressWarnings("null")
	@Override
	public String getDownloadPrint(String studRegNo,Integer schoolId,String yearComboVal, String std, String admissionDateSpan,String schoolName,HttpServletResponse response) {
		String listSize="0";
		try{	
			List<StudentAdmissionBean> exePath=printExcelStudentRepositoryImpl.getExeFolderPath();
			List<StudentAdmissionBean> studentDatalist=printExcelStudentRepositoryImpl.getstudValues(studRegNo,schoolId);
			List<StudentAdmissionBean> newDynamicFieldsWithValue=printExcelStudentRepositoryImpl.getNewDynamicFieldsValues(studRegNo,schoolId);
			List<StudentAdmissionBean> getAssignedSubjectToStudent=printExcelStudentRepositoryImpl.getAssignedSubjectToStudent(studRegNo,schoolId,yearComboVal,std);
			
			String absoluteExcelFilesPath = null;
			String fileName = null;
			
			String studentId=studentDatalist.get(0).getStudId();
			
		    absoluteExcelFilesPath= exePath.get(0).getFirstName();
		    fileName="AdmissionExcel-"+studentId+"-"+studRegNo+"-"+yearComboVal+".xlsx";
		    
		    Path file = Paths.get(absoluteExcelFilesPath+"/cms/ExcelAdmissionFiles/", fileName);
	        if (Files.exists(file)) {
				response.setContentType("application/force-download");
				response.addHeader("Content-Disposition", "attachment; filename="+fileName);
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush(); 
	        }
	        //File file1=new File(""+file);
	        //file1.delete();
		    
		    return "Error In Genetating Gadgets.~"+listSize;
		}
		catch (Exception e) {
            e.printStackTrace();
            return "Error In Genetating Gadgets.~"+listSize;
        }
	}*/
	
	private void refreshExcelFile(File file,String absoluteExcelFilesPath2) {
	    try {
	    	String vbs = "Set objExcel=CreateObject(\"Excel.Application\")\r\n"
					+"Set objWorkbook=objExcel.Workbooks.Open(WScript.arguments(0))\r\n"
					+"objExcel.Application.Visible=true\r\n"
					+"objWorkbook.Sheets(1).Select\r\n"
					+"objExcel.Rows(\"400:402\").EntireRow.Delete\r\n"
					+"objWorkbook.Sheets(2).Select\r\n"
					+"objExcel.Rows(\"400:402\").EntireRow.Delete\r\n"
					+"objWorkbook.Sheets(3).Select\r\n"
					+"objExcel.Rows(\"400:402\").EntireRow.Delete\r\n"
					+"objWorkbook.Save()\r\n"
					+"objExcel.Application.DisplayAlerts = False\r\n"
					+"objExcel.Application.Quit\r\n";
	      	    	    	
			String filePath = "" + absoluteExcelFilesPath2 + "\\vbScriptForRefreshExcelFile2.vbs";
			File vbScript = new File(filePath);
			if(!vbScript.exists()){
				vbScript.createNewFile();
			}
			FileWriter fw = new java.io.FileWriter(vbScript);
			fw.write(vbs);
			fw.close();
			
			Runtime rt = Runtime.getRuntime();                       
            Process pr = rt.exec("cmd /c cscript "+vbScript.getAbsolutePath()+" "+file.getAbsolutePath()+" 1"); 
            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line=null; 
            while((line=input.readLine()) != null) {
                
            } 
            int exitVal = pr.waitFor();
            
            if(exitVal==0){    

            }
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}

	@Override
	public ByteArrayInputStream getreport(Integer schoolId) {
		
		
		
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {

			

			PdfWriter.getInstance(document, out);
			document.open();
			 document.add(new Paragraph("Some content here"));
			 
			    //Set attributes here
			    document.addAuthor("Lokesh Gupta");
			    document.addCreationDate();
			    document.addCreator("HowToDoInJava.com");
			    document.addTitle("Set Attribute Example");
			    document.addSubject("An example to show how attributes can be added to pdf files.");
	

			document.close();

		} catch (DocumentException ex) {

			
		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public List<UserDetailsBean> getStudentInformation(String studRegNo, Integer schoolId, String yearComboVal) {
		List<UserDetailsBean> studentMasterList = studentMasterRepository.getStudentInformation(studRegNo,schoolId);
		return studentMasterList;
	}

	@Override
	public List<StudentAdmissionBean> getStudentAttachmentsforPrint(String studentId) {
		List<StudentAdmissionBean> attachmentList = studentMasterRepository.getStudentAttachmentsforPrint(Integer.valueOf(studentId));
		return attachmentList;
	}
		
	@Override
	public SubjectSelectionBean getSubjectList(Integer schoolId, Integer yearId, Integer standardId,Integer studentId) {
		SubjectSelectionBean subjectSelectionList = new SubjectSelectionBean();
		Integer totalSubjects = assignSubjectToStudentRepository.getTotalSubjectCount(schoolId, yearId, standardId);
		subjectSelectionList.setTotalSubjects(0);
		if(totalSubjects!=null) {
			subjectSelectionList.setTotalSubjects(totalSubjects);
		}
		subjectSelectionList.setCumpolsrySubjectList(assignSubjectToStudentRepository.getCumplosrySubjects(schoolId, yearId, standardId));
		List<ElectiveGroupBean> groupList = assignSubjectToStudentRepository.getGroups(schoolId, yearId, standardId);
		if(groupList.size()>0) {
			for(int i=0;i<groupList.size();i++) {
				List<SubGroupBean> subGroupList = assignSubjectToStudentRepository.getSubGroups(schoolId, groupList.get(i).getGroupId());
				for(int j=0;j<subGroupList.size();j++) {
					subGroupList.get(j).setSubjectList(assignSubjectToStudentRepository.getElectiveSubjects(schoolId, groupList.get(i).getGroupId(), subGroupList.get(j).getSubGroupId()));
				}
				groupList.get(i).setSubGroupList(subGroupList);
			}
		}
		subjectSelectionList.setElectiveGroupBean(groupList);
		List<StudentDynamicDetailsBean> studentList = assignSubjectToStudentRepository.getAssignedSubjects(""+studentId);
		if(CollectionUtils.isNotEmpty(studentList)) {
			subjectSelectionList.setSubjectsForEdit(studentList);
		}
		return subjectSelectionList;
	}

	@Override
	public boolean saveSubject(Integer schoolId, Integer yearId, Integer standardId, Integer studentId,String[] subjectArr) {
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
		String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
		SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
		schoolMasterModel.setSchoolid(schoolId);
		try {
			assignSubjectToStudentRepository.deleteByStudentId(""+studentId);
			if(subjectArr!=null && subjectArr.length>0) {
				for(int i=0;i<subjectArr.length;i++) {
					AssignSubjectToStudentModel assignSubjectToStudentModel = new AssignSubjectToStudentModel();
					
					assignSubjectToStudentModel.setSchoolMasterModel(schoolMasterModel);
					assignSubjectToStudentModel.setStandardId(standardId);
					assignSubjectToStudentModel.setYearId(yearId);
					assignSubjectToStudentModel.setStudentId(""+studentId);
					assignSubjectToStudentModel.setSubjectId(subjectArr[i]);
				//	AppUserRoleModel appUserRole = new AppUserRoleModel();
				//	appUserRole.setAppUserRoleId(46);
				//	assignSubjectToStudentModel.setUserId(appUserRole );
					String incrementedId = ""+assignSubjectToStudentRepository.getMaxId(schoolId);
					String assignSubjectId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					assignSubjectToStudentModel.setAssignSubjectId(Integer.parseInt(assignSubjectId));
					assignSubjectToStudentRepository.save(assignSubjectToStudentModel);
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	@Override
	@Transactional
	public StudentMasterModel saveNewRegistrationForCalendar(String newRegName, String yearId,
			String standardId,String passwordNewReg, String newRegUserName, String schoolId,String formNo,
			 String secondName, String lastName,HttpSession session,Integer divisionId,String fcm_id) {
		StudentMasterModel studentMasterModel = new StudentMasterModel();
		
//		List<StudentMasterModel> studentList = studentMasterRepository.findByMobileNo(newRegUserName,
//				Integer.valueOf(schoolId));
//		System.out.println(studentList.size());
//		if(studentList.size()!=0) {
//		studentList.stream().forEach(student->{
//			studentMasterRepository.deleteRenewRecord(Integer.parseInt(schoolId),student.getStudentId());
//		});
//		studentList.stream().forEach(student->{
//			studentMasterRepository.deleteStudentIdRecord(Integer.parseInt(schoolId),student.getStudentId());
//		});
//		appRoleRepository.deleteRecords(newRegUserName,Integer.valueOf(schoolId));
//		}
		
		try {
//			String newRegName = ;
//			String secondName = Utillity.convertInUTFFormat(secondName1);
//			String lastName = Utillity.convertInUTFFormat(lastName1);
			Integer  principaluserId = userRepository.getprincipalUserId(Integer.valueOf(schoolId));
			AppUserRoleModel appUserRoleModel =new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(principaluserId);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			LocalDate localDate = LocalDate.now();
			String date=(dtf.format(localDate)); //2016/11/16
			
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(Integer.valueOf(schoolId));
			String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
			String incrementedId = ""+studentMasterRepository.getMaxId(Integer.valueOf(schoolId));
			String studentId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			studentMasterModel.setStudentId(Integer.parseInt(studentId));
			String provisionalNo = studentMasterRepository.getMaxProvisionalNo(Integer.valueOf(schoolId));
			studentMasterModel.setStudentRegNo("P"+provisionalNo);
			studentMasterModel.setProvisionalRegNo("P"+provisionalNo);
			studentMasterModel.setStudentFormNo(formNo);
			studentMasterModel.setAdmissionDate(date);
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(Integer.valueOf(schoolId));
			studentMasterModel.setSchoolMasterModel(schoolMasterModel);
			String getMaxGrBookId=studentMasterRepository.getMaxGrBookId(Integer.valueOf(schoolId));
			GRBookNameModel gRBookNameModel=new GRBookNameModel();
			gRBookNameModel.setGrBookId(Integer.valueOf(getMaxGrBookId));
			studentMasterModel.setGrBookName(gRBookNameModel);
			studentMasterModel.setMmobile(newRegUserName);
			studentMasterModel.setStudInitialName("");
			studentMasterModel.setStudFName(newRegName);
			studentMasterModel.setStudLName(lastName);
			studentMasterModel.setStudMName(secondName);
			studentMasterModel.setIsOnlineAdm("1");
			studentMasterModel.setIsQuickAdmission(0);
			studentMasterModel.setIsApproval("1");
			studentMasterModel.setPreviousState(yearId);
			studentMasterModel.setAdmissionDate(date);
			studentMasterModel.setMedium("Select Medium");
			studentMasterModel.setIsOnlineAdm("1");
			studentMasterModel.setUserId(appUserRoleModel);
			
			Set<StudentStandardRenewModel> studentStandardRenewMap = new HashSet<>();
			
			YearMasterModel yearMasterModel = new YearMasterModel();
			yearMasterModel.setYearId(Integer.parseInt(yearId));
			StandardMasterModel standardMasterModel = new StandardMasterModel();
			standardMasterModel.setStandardId(Integer.parseInt(standardId));
			
			DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
			divisionMasterModel.setDivisionId(divisionId);
				StudentStandardRenewModel studentStandardRenewModel = new StudentStandardRenewModel();
				//studentStandardRenewModel.setRenewstudentId(studentAdmissionBean.getStudentRenewId());
				studentStandardRenewModel.setStudentMasterModel(studentMasterModel);
				studentStandardRenewModel.setYearMasterModel(yearMasterModel );
				studentStandardRenewModel.setStandardMasterModel(standardMasterModel );
				studentStandardRenewModel.setAlreadyRenew((short) 1);
				studentStandardRenewModel.setRenewAdmissionDate(date);
				studentStandardRenewModel.setStudFailDate(date);
				studentStandardRenewModel.setSchoolMasterModel(schoolMasterModel);
				studentStandardRenewModel.setFcm_id(fcm_id);
				studentStandardRenewModel.setIsOnlineAdmission("1");
				studentStandardRenewModel.setDivisionMasterModel(divisionMasterModel);
				String incrementedId1 = ""+studentMasterRepository.getMaxRenewId(Integer.valueOf(schoolId));
				String renewId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
				studentStandardRenewModel.setRenewStudentId(Integer.parseInt(renewId));
				studentStandardRenewModel.setSchoolMasterModel(schoolMasterModel);
				studentStandardRenewMap.add(studentStandardRenewModel);
//			
				studentMasterModel.setStudentStandardRenews(studentStandardRenewMap);
				studentMasterModel = studentMasterRepository.save(studentMasterModel);
				
				studentStandardRepository.save(studentStandardRenewModel);
				
				for(int i=0;i<3;i++) {
						String incrementedId11 = ""+userRepository.getMaxUserId(Integer.valueOf(schoolId));
						String maxUserId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId11);
						AppUserModel user  = new AppUserModel();
						user.setAppUserRoleId(Integer.parseInt(maxUserId));
						if(i==0) {
							user.setRoleName("ROLE_STUDENT");
							
						}
						if(i==1) {
							user.setRoleName("ROLE_PARENT1");
						}
						if(i==2) {
							user.setRoleName("ROLE_PARENT2");
						}
					
						user.setUsername(newRegUserName);
						user.setStaffId(studentMasterModel.getStudentId());
						user.setPassword(md5(passwordNewReg));
						user.setFirstName(newRegName);
						if(lastName.length()>0) {
							user.setLastName(lastName);
						}else {
							user.setLastName(newRegName);
						}
						user.setSchoolid(schoolMasterModel.getSchoolid());
						user.setSettingString("fjEHuisqm3ASr9F8oUudICFblqLrBExRMl4ywbLz8A/FEd9Uod5PQopFWu5+9C4oUlvghDncYonqPqEtO4SF/Guo+lq6LrpGXqMtRv4Pu9ZffpvBUFEUrUAloT62u5CLBW5LygVsBWEBzfrAlsn7qDncN0612klKx0/tKJ/3ulPvsmgdrt65tJJpFvntlKeo");
						userRepository.saveAndFlush(user);
						
						AppUserRoleModel appUserRole = new AppUserRoleModel();
						appUserRole.setAppUserRoleId(Integer.parseInt(maxUserId));
						appUserRole.setRoleName("ROLE_ADMIN");
						appUserRole.setUsername(newRegUserName);
						appUserRole.setSchoolid(schoolMasterModel.getSchoolid());
						appRoleRepository.saveAndFlush(appUserRole);
						if(i==0) {
							// for fee setting donot remove
							studentMasterModel.setUserId(appUserRole);
						}
						
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			return studentMasterModel;
		}
	


	@Override
	public Integer getrenewId(Integer studentId, Integer yearId) {
		List<StudentCountBean> renewList = studentMasterRepository.getrenewId(studentId,yearId);
		return renewList.get(0).getRenewId();
	}

	@Override
	public String getCertificatePrint(String studRegNo, Integer schoolId, String yearComboVal, String std,
			String standardText, String schoolName, String globalCertificateId, HttpServletResponse response,HttpSession session) {
		//String listSize="0";
		try{
			String filePathName="";
			String studRegNo1[]=studRegNo.split(",");
			String yearComboVal1[]=yearComboVal.split(",");
			String std1[]=std.split(",");
			String standardText1[]=standardText.split(",");
			
			for(int r=0;r<studRegNo1.length;r++) {
				
				List<StudentAdmissionBean> exePath=printExcelStudentRepositoryImpl.getExeFolderPath();
				List<StudentAdmissionBean> studentDatalist=printExcelStudentRepositoryImpl.getstudValues(studRegNo1[r],schoolId);
				List<StudentAdmissionBean> newDynamicFieldsWithValue=printExcelStudentRepositoryImpl.getNewDynamicFieldsValues(studRegNo1[r],schoolId, studentDatalist.get(0).getStudId());
				List<StudentAdmissionBean> getAssignedSubjectToStudent=printExcelStudentRepositoryImpl.getAssignedSubjectToStudent(studRegNo1[r],schoolId,yearComboVal1[r],std1[r]);
				List<SubjectBean> subList = printExcelStudentRepositoryImpl.getSubjectListForPrint(schoolId, studentDatalist.get(0).getStudId());
				List<StudentAdmissionBean> serialNoList=printExcelStudentRepositoryImpl.getSerialNo(studentDatalist.get(0).getStudentRenewId(),schoolId,globalCertificateId);
				List<StudentAdmissionBean> inputList=printExcelStudentRepositoryImpl.getInputList(globalCertificateId,schoolId,serialNoList.get(0).getSerialNo());
				String schoolKey =schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();

				String absoluteExcelFilesPath = null;
				String fileName = null;

				String studentId = studentDatalist.get(0).getStudId();
				Integer studentRenewId = studentDatalist.get(0).getStudentRenewId();

				absoluteExcelFilesPath = exePath.get(0).getFirstName();
				fileName = "CertificateExcel-" + studentId + "-" + studRegNo1[r] + "-" + yearComboVal1[r]+ "-" +serialNoList.get(0).getSerialNo()+ ".xlsx";
				Integer ifPresent=printExcelStudentRepositoryImpl.getExcelSchoolType(schoolId,inputList.get(0).getCertificateName());

				InputStream template = null;
				if(ifPresent!=0) {
					 template = new FileInputStream(absoluteExcelFilesPath + "/cms/CertificateFiles/"+ schoolKey
							+ "/" +  inputList.get(0).getCertificateName()+"/"+"Certificate.xlsx");
				}else {
					 template = new FileInputStream(absoluteExcelFilesPath + "/cms/CertificateFiles/"+  inputList.get(0).getCertificateName()+"/"+"Certificate.xlsx");
				}
				Workbook wb = WorkbookFactory.create(template);
				// Saving excel to a different location or filename.
				FileOutputStream out2 ;
				if(ifPresent!=0) {
					out2 = new FileOutputStream(absoluteExcelFilesPath + "/cms/CertificateFiles/"
							+ schoolKey + "/" +  inputList.get(0).getCertificateName()  + "/" + fileName);
				}else {
					out2 = new FileOutputStream(absoluteExcelFilesPath + "/cms/CertificateFiles/" + inputList.get(0).getCertificateName() + "/" + fileName);
				}
				 
				wb.write(out2);
				// wb.close();
				out2.close();
				template.close();
				FileInputStream in;
				if(ifPresent!=0) {
					in = new FileInputStream(absoluteExcelFilesPath + "/cms/CertificateFiles/" + schoolKey
							+ "/" +  inputList.get(0).getCertificateName()  + "/" + fileName);
				}else {
					in = new FileInputStream(absoluteExcelFilesPath + "/cms/CertificateFiles" + "/" +  inputList.get(0).getCertificateName()  + "/" + fileName);
				}
				 

				XSSFWorkbook workbook = new XSSFWorkbook(in);
				XSSFSheet sheet = workbook.getSheetAt(0);

				// XSSFWorkbook workbook = new XSSFWorkbook();
				// XSSFSheet sheet = workbook.createSheet("Student Details");
				// XSSFSheet sheet = workbook.getSheetAt(0);

				int noOfRow = 0;
				int noOfCell = 0;

				int noOfRow2 = 4;
				int noOfCell2 = 3;

				XSSFRow row = sheet.createRow(noOfRow++); // create 1 Row
				row.createCell(noOfCell++).setCellValue("ePrashasan");
				row.createCell(noOfCell++).setCellValue("");
				row.createCell(noOfCell++).setCellValue(schoolName);

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 2 Row
				row.createCell(noOfCell++).setCellValue("Year : ");
				row.createCell(noOfCell++).setCellValue(yearComboVal1[r]);
				row.createCell(noOfCell++).setCellValue("");
				row.createCell(noOfCell++).setCellValue("Standard : ");
				row.createCell(noOfCell++).setCellValue(standardText1[r]);
				row.createCell(noOfCell++).setCellValue("");
				row.createCell(noOfCell++).setCellValue("Date : ");
				row.createCell(noOfCell++).setCellValue("");

				// ---------- For Student Basic Details AND OLD Dynamic Fields OF Student
				// ---------------

				// if(studentDatalist.size()>0 && ! StringUtils.isEmpty(studentDatalist)) {

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row
				row.createCell(noOfCell++).setCellValue("Roll No");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getRollNo());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Reg No");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getRegistrationNumber());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Student Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getFirstName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row
				row.createCell(noOfCell++).setCellValue("Religion Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getReligionName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Category Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCategoryName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row
				row.createCell(noOfCell++).setCellValue("Caste Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCasteName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Gender");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getGender());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row
				row.createCell(noOfCell++).setCellValue("Mother Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getMotherName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Father Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getFatherName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row
				row.createCell(noOfCell++).setCellValue("Birth Date");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBirthDate());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Accademic Year");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getAcademicYearText());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row
				row.createCell(noOfCell++).setCellValue("Standard Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStandardName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Division Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getDivName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 3 Row
				row.createCell(noOfCell++).setCellValue("Mobile No");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getmMobile());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("AadhaarCard No.");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getfMobile());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Occupation ");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getOccupation());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Student Renew Id");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStudentRenewId());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("TextBox1");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox1());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("TextBox2");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox2());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("TextBox3");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox3());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("TextBox4");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox4());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("TextBox5");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getTextbox5());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("ComboBox1");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox1());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("ComboBox2");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox2());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("ComboBox3");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox3());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("ComboBox4");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox4());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("ComboBox5");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getCombobox5());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("SurName");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStudLName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("FirstName");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getStudFName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Mother Tongue");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getMothertongue());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Birth Place");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBirthPlace());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Permenant Address");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getPermanentAddress());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Permanent State");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getPermanentState());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Permanent PinCode");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getPermanentPinCode());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Local Address");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getLocalAddress());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Local State");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getLocalState());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Local PinCode");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getLocalPinCode());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Parent Email-ID");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getParentEmailID());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Student Email-ID");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getEmailID());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("AadhaarCard No.");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getAadhaarCardNo());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Bank Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBankName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Branch Name");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBranchName());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Bank IFSC No.");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getBankIFSCNo());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Bank Account No.");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getAccountNo());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Form No.");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getFormNumber());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Nationality");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getNationality());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Income");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getIncome());

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Qualification");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getEducation());
				
				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Medium");
				row.createCell(noOfCell++).setCellValue(studentDatalist.get(0).getMedium());
				
				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row
				row.createCell(noOfCell++).setCellValue("Certificate Serial No");
				row.createCell(noOfCell++).setCellValue(serialNoList.get(0).getSerialNo());
				// ---------- For Input List to Student ---------------
				row = sheet.createRow(noOfRow++); // create 4 Row

				row = sheet.createRow(noOfRow++); // create 4 Row
				
				if (inputList.size() > 0) {
					for (int i = 0; i < inputList.size(); i++) {
						/*
						 * //noOfCell = 0; row = sheet.getRow(noOfRow2 + i); // create 4 Row
						 * row.createCell(noOfCell++).setCellValue(inputList.get(i).getInputName()); row
						 * = sheet.getRow(noOfRow2 + i); // create 4 Row
						 * row.createCell(noOfCell++).setCellValue(inputList.get(i).getInputData());
						 */
						
						noOfCell = 0;
						row = sheet.createRow(noOfRow++); // create 4 Row
						row.createCell(noOfCell++).setCellValue(inputList.get(i).getInputName());
						row.createCell(noOfCell++).setCellValue(inputList.get(i).getInputData());
					}
				}

				// ---------- For Input List to Student END ---------------
				

				// }
				// ---------- For Student Basic Details AND OLD Dynamic Fields OF Student
				// END---------------

				// ---------- For Assign Subjects to Student ---------------
				if (getAssignedSubjectToStudent.size() > 0) {
					for (int i = 0; i < getAssignedSubjectToStudent.size(); i++) {
						noOfCell2 = 3;
						row = sheet.getRow(noOfRow2 + i); // create 4 Row
						row.createCell(noOfCell2).setCellValue(getAssignedSubjectToStudent.get(i).getFirstName());
					}
				}

				// ---------- For Assign Subjects to Student END ---------------

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row

				noOfCell = 0;
				row = sheet.createRow(noOfRow++); // create 4 Row

				// ---------- For Dynamic Fields OF Student ---------------

				if (newDynamicFieldsWithValue.size() > 0
						&& !org.springframework.util.StringUtils.isEmpty(newDynamicFieldsWithValue)) {
					for (int i = 0; i < newDynamicFieldsWithValue.size(); i++) {
						noOfCell = 0;
						row = sheet.createRow(noOfRow++); // create 4 Row
						row.createCell(noOfCell++).setCellValue(newDynamicFieldsWithValue.get(i).getFirstName());
						row.createCell(noOfCell++).setCellValue(newDynamicFieldsWithValue.get(i).getGender());
					}
				}
				
			
				// ---------- For Dynamic Fields OF Student END ---------------
				
				  //-------------start Roaster Form Details-------------------------- noOfCell
				  noOfCell= 0; row = sheet.createRow(noOfRow++); // create 3 Row noOfCell = 0; row =
				  sheet.createRow(noOfRow++); // create 3 Row
				  
				  row.createCell(noOfCell++).setCellValue("Subject Name ");
				  row.createCell(noOfCell++).setCellValue("Course Code ");
				  row.createCell(noOfCell++).setCellValue("Year ");
				  row.createCell(noOfCell++).setCellValue("Standard ");
				  row.createCell(noOfCell++).setCellValue("Semester ");
				  row.createCell(noOfCell++).setCellValue("Active Exam Name ");
				  row.createCell(noOfCell++).setCellValue("Group Name ");
				  row.createCell(noOfCell++).setCellValue("Sub Group Name ");
				  row.createCell(noOfCell++).setCellValue("RC Credit ");
				  row.createCell(noOfCell++).setCellValue("WRC Credit ");
				  row.createCell(noOfCell++).setCellValue("RC_RWC Flag ");
				  row.createCell(noOfCell++).setCellValue("Regular_Backlog Flag ");
				  row.createCell(noOfCell++).setCellValue("Is_Eligible_Grace Flag");
				  row.createCell(noOfCell++).setCellValue("Is_Eligible_Grade Flag");
				  row.createCell(noOfCell++).setCellValue("Status ");
				  row.createCell(noOfCell++).setCellValue("Grade Type ");
				  row.createCell(noOfCell++).setCellValue("Practical_Theory Flag ");
				  row.createCell(noOfCell++).setCellValue("Subject Credit ");
				  row.createCell(noOfCell++).setCellValue("Subject Credit_w_attendancet ");
				  row.createCell(noOfCell++).setCellValue("Compulsory_Elective Flag ");
				  row.createCell(noOfCell++).setCellValue("Year_Semester Flag ");
				  
				  if (CollectionUtils.isNotEmpty(subList)) { for (SubjectBean subjects :
				  subList) { noOfCell = 0; row = sheet.createRow(noOfRow++); // create 4 Row
				  row.createCell(noOfCell++).setCellValue(subjects.getSubjectName());
				  row.createCell(noOfCell++).setCellValue(subjects.getCourseCode());
				  row.createCell(noOfCell++).setCellValue(subjects.getYear());
				  row.createCell(noOfCell++).setCellValue(subjects.getStandard());
				  row.createCell(noOfCell++).setCellValue(subjects.getSemester());
				  row.createCell(noOfCell++).setCellValue(subjects.getActiveExamName());
				  row.createCell(noOfCell++).setCellValue(subjects.getGroupName());
				  row.createCell(noOfCell++).setCellValue(subjects.getSubGroupName());
				  row.createCell(noOfCell++).setCellValue(subjects.getRcCredit());
				  row.createCell(noOfCell++).setCellValue(subjects.getWrcCredit());
				  row.createCell(noOfCell++).setCellValue(subjects.getRcrwc());
				  row.createCell(noOfCell++).setCellValue(subjects.getBacklogFlag());
				  row.createCell(noOfCell++).setCellValue(subjects.getIseligiblegrace());
				  row.createCell(noOfCell++).setCellValue(subjects.getIseligibleGrade());
				  row.createCell(noOfCell++).setCellValue(subjects.getStatus());
				  row.createCell(noOfCell++).setCellValue(subjects.getGradeTypeStr());
				  row.createCell(noOfCell++).setCellValue(subjects.getPracticaltheoryFlag());
				  row.createCell(noOfCell++).setCellValue(subjects.getCredit());
				  row.createCell(noOfCell++).setCellValue(subjects.getCreditWithoutAttendance()); 
				  row.createCell(noOfCell++).setCellValue(subjects.getCompulElectiveFlag());
				  row.createCell(noOfCell++).setCellValue(subjects.getYearsemesterflag()); 
				  } 
				  
				  }
				  
				  //-------------end Roaster Form Details--------------------------
				 
			

				// Start Set Latter Head Images
				int col1[] = new int[11];
				int col2[] = new int[11];
				int row1[] = new int[11];
				int row2[] = new int[11];

				String dollerName[] = new String[11];
				String ifUse[] = new String[11];
				String from[] = new String[11];
				String imageName[] = new String[11];
				int sheetNo[] = new int[11];
				String repited[] = new String[11];
				int totalCol[] = new int[11];
				int totalRow[] = new int[11];

				int rowCount = 0;
				int collCount = 0;
				// Start Read Excel Setting
				// Iterate through each rows one by one
				sheet = workbook.getSheetAt(3); // Sheet Number 4
				Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) {
					collCount = 0;
					Row rows = rowIterator.next();
					if (rowCount == 0) {
						rows = rowIterator.next();
					}
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = rows.cellIterator();
					while (cellIterator.hasNext()) {
						Cell cell1 = cellIterator.next();
						collCount++;
						// Check the cell type and format accordingly
						switch (collCount) {
						case 1: // Doller Name
							dollerName[rowCount] = "" + cell1;
							break;
						case 2: // If Use
							ifUse[rowCount] = "" + cell1.toString();
							break;
						case 3: // From
							from[rowCount] = "" + cell1.toString();
							break;
						case 4: // Image Name
							imageName[rowCount] = "" + cell1.toString();
							break;
						case 5: // Sheet No
							if (cell1.toString().contains(".")) {
								sheetNo[rowCount] = Integer
										.parseInt("" + cell1.toString().substring(0, cell1.toString().lastIndexOf(".")));
							} else {
								sheetNo[rowCount] = Integer.parseInt("" + cell1.toString());
							}
							break;
						case 6: // Repited
							repited[rowCount] = "" + cell1.toString();
							break;
						case 7: // col1
							if (cell1.toString().contains(".")) {
								col1[rowCount] = Integer
										.parseInt("" + cell1.toString().substring(0, cell1.toString().lastIndexOf(".")));
							} else {
								col1[rowCount] = Integer.parseInt("" + cell1.toString());
							}
							break;
						case 8: // col2
							if (cell1.toString().contains(".")) {
								col2[rowCount] = Integer
										.parseInt("" + cell1.toString().substring(0, cell1.toString().lastIndexOf(".")));
							} else {
								col2[rowCount] = Integer.parseInt("" + cell1.toString());
							}
							break;
						case 9: // row1
							if (cell1.toString().contains(".")) {
								row1[rowCount] = Integer
										.parseInt("" + cell1.toString().substring(0, cell1.toString().lastIndexOf(".")));
							} else {
								row1[rowCount] = Integer.parseInt("" + cell1.toString());
							}
							break;
						case 10: // row2
							if (cell1.toString().contains(".")) {
								row2[rowCount] = Integer
										.parseInt("" + cell1.toString().substring(0, cell1.toString().lastIndexOf(".")));
							} else {
								row2[rowCount] = Integer.parseInt("" + cell1.toString());
							}
							break;
						case 11: // Total Cols
							if (cell1.toString().contains(".")) {
								totalCol[rowCount] = Integer
										.parseInt("" + cell1.toString().substring(0, cell1.toString().lastIndexOf(".")));
							} else {
								totalCol[rowCount] = Integer.parseInt("" + cell1.toString());
							}
							break;
						case 12: // Total Rows
							if (cell1.toString().contains(".")) {
								totalRow[rowCount] = Integer
										.parseInt("" + cell1.toString().substring(0, cell1.toString().lastIndexOf(".")));
							} else {
								totalRow[rowCount] = Integer.parseInt("" + cell1.toString());
							}
							break;
						}
					}
					rowCount++;
				}
				// End Read Excel Setting
	            
	            String folderName = ""+absoluteExcelFilesPath+"cms\\IcardImages"+File.separator+schoolKey+File.separator+schoolId;
	            
	            try { //Get Images From Folder
					for(int dd=0;dd<10;dd++)	{
						if(from[dd].equalsIgnoreCase("Folder") && ifUse[dd].equalsIgnoreCase("Yes")){
							if(totalRow[dd]!=0){
								sheet = workbook.getSheetAt((sheetNo[dd]-1));
								if(studentDatalist!=null && !studentDatalist.isEmpty()){
									for(int k=0;k<studentDatalist.size();k++)	{
										//Object [] obj1 = (Object[]) studentDatalist.get(k);
										File imgfile=null;
										if(dollerName[dd].equalsIgnoreCase("$Bonafide")){
											imgfile = new File(absoluteExcelFilesPath+"cms\\Certificates\\Student_Bonafied_Image"+File.separator+schoolKey+"/Bonafied_Image.jpg"); //BonafiedImg
										}
										if(dollerName[dd].equalsIgnoreCase("$TC")){
											imgfile = new File(absoluteExcelFilesPath+"cms\\Certificates\\Student_TC_Image"+File.separator+schoolKey+"/TC_Image.jpg"); //TCImg
										}
										if(dollerName[dd].equalsIgnoreCase("$PrincipalSignature")){
											imgfile = new File(absoluteExcelFilesPath+"cms\\Principal_Sign_Image"+File.separator+schoolKey+"/Principal_Sign.jpg"); //PrincipalImg
										}
										if(dollerName[dd].equalsIgnoreCase("$IdCardBackgroundImg")){
											imgfile = new File(absoluteExcelFilesPath+"cms\\ID_Card_Background_Image"+File.separator+schoolKey+"/ID_Card_Background.jpg"); //IdCardBackgroundImg
										}
										if(dollerName[dd].equalsIgnoreCase("$StudPhoto")){
											imgfile = new File(folderName+"/"+studRegNo+".jpg"); //StudPhoto
										}
										if(dollerName[dd].equalsIgnoreCase("$StudSign")){
											imgfile = new File(folderName+"/sign_"+studRegNo+".jpg"); //StudSignature
										}
										if(dollerName[dd].equalsIgnoreCase("$ImgPath1")){
											imgfile = new File(imageName[dd]); //ImgPath1
										}
										if(dollerName[dd].equalsIgnoreCase("$ImgPath2")){
											imgfile = new File(imageName[dd]); //ImgPath2
										}
										if(dollerName[dd].equalsIgnoreCase("$ImgPath3")){
											imgfile = new File(imageName[dd]); //ImgPath3
										}
										if(dollerName[dd].equalsIgnoreCase("$ImgPath4")){
											imgfile = new File(imageName[dd]); //ImgPath4
										}
										/*if(dollerName[dd].equalsIgnoreCase("$ImgPath5")){
											imgfile = new File(imageName[dd]); //ImgPath5
										}*/
										if(repited[dd].equalsIgnoreCase("Yes")){
											if(imgfile.exists() && !imgfile.isDirectory()) { 
												InputStream is = new FileInputStream(imgfile);
												byte[] bytes = IOUtils.toByteArray(is);
												int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
												CreationHelper helper = workbook.getCreationHelper();
												// Create the drawing patriarch. This is the top level container for all shapes. 
												Drawing drawing = sheet.createDrawingPatriarch();
												//add a picture shape		
												ClientAnchor anchor = helper.createClientAnchor();
												//set top-left corner of the picture,
												//subsequent call of Picture#resize() will operate relative to it
												anchor.setCol1(col1[dd]);
												anchor.setCol2(col2[dd]);
												anchor.setRow1(row1[dd]);
												anchor.setRow2(row2[dd]);				
												Picture pict = drawing.createPicture(anchor, pictureIdx);
												//auto-size picture relative to its top-left corner
												//pict.resize(1);
											}
											row1[dd]=row1[dd]+totalRow[dd];
											row2[dd]=row2[dd]+totalRow[dd];
										}else{
											if(imgfile.exists() && !imgfile.isDirectory()) { 
												InputStream is = new FileInputStream(imgfile);
												byte[] bytes = IOUtils.toByteArray(is);
												int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
												CreationHelper helper = workbook.getCreationHelper();
												// Create the drawing patriarch. This is the top level container for all shapes. 
												Drawing drawing = sheet.createDrawingPatriarch();
												//add a picture shape		
												ClientAnchor anchor = helper.createClientAnchor();
												//set top-left corner of the picture,
												//subsequent call of Picture#resize() will operate relative to it
												anchor.setCol1(col1[dd]);
												anchor.setCol2(col2[dd]);
												anchor.setRow1(row1[dd]);
												anchor.setRow2(row2[dd]);				
												Picture pict = drawing.createPicture(anchor, pictureIdx);
												//auto-size picture relative to its top-left corner
											//	pict.resize(1);
											}
										}
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}			
				// End Set Latter Head Images
				
				
				
				if(exePath.size()>0) {
					FileOutputStream out=null;
					if(ifPresent!=0) {
						//fileName="AdmissionExcel-"+studRegNo+"-"+std+"-"+yearComboVal+".xlsx";
				    	 out = new FileOutputStream(new File(absoluteExcelFilesPath+"/cms/CertificateFiles/"+schoolKey+"/"+inputList.get(0).getCertificateName()+"/"+fileName));
					}else {
						//fileName="AdmissionExcel-"+studRegNo+"-"+std+"-"+yearComboVal+".xlsx";
				    	 out = new FileOutputStream(new File(absoluteExcelFilesPath+"/cms/CertificateFiles/"+inputList.get(0).getCertificateName()+"/"+fileName));
					}
				    
			        workbook.write(out);
			        out.close();
				}
		      //workbook.close();
		        
				
	          //  -----------------------------  Refresh Excel File   ------------------------------
				if(ifPresent!=0) {
					refreshExcelFileForCertificate(new File(absoluteExcelFilesPath+"/cms/CertificateFiles/"+schoolKey+"/"+inputList.get(0).getCertificateName()+"/"+fileName), absoluteExcelFilesPath.replace("/MainDB/", ""));
				}else {
					refreshExcelFileForCertificate(new File(absoluteExcelFilesPath+"/cms/CertificateFiles/"+inputList.get(0).getCertificateName()+"/"+fileName), absoluteExcelFilesPath.replace("/MainDB/", ""));
				}
				
			  //  -----------------------------  Refresh Excel File  END ------------------------------				
				
				if(ifPresent!=0) {
					 filePathName+=absoluteExcelFilesPath+"/cms/CertificateFiles/"+schoolKey+"/"+inputList.get(0).getCertificateName()+"/#"+fileName + ",";
				}else {
					 filePathName+=absoluteExcelFilesPath+"/cms/CertificateFiles/"+inputList.get(0).getCertificateName()+"/#"+fileName + ",";
				}
				 
				 
			}
			return filePathName;
		}
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
	}

	private void refreshExcelFileForCertificate(File file, String absoluteExcelFilesPath2) {
	    try {
	    	String vbs = "Set objExcel=CreateObject(\"Excel.Application\")\r\n"
					+"Set objWorkbook=objExcel.Workbooks.Open(WScript.arguments(0))\r\n"
					+"objExcel.Application.Visible=true\r\n"
					+"objWorkbook.Sheets(1).Select\r\n"
					+"objExcel.Rows(\"400:402\").EntireRow.Delete\r\n"
					+"objWorkbook.Sheets(2).Select\r\n"
					+"objExcel.Rows(\"400:402\").EntireRow.Delete\r\n"
					+"objWorkbook.Sheets(3).Select\r\n"
					+"objExcel.Rows(\"400:402\").EntireRow.Delete\r\n"
					+"objWorkbook.Save()\r\n"
					+"objExcel.Application.DisplayAlerts = False\r\n"
					+"objExcel.Application.Quit\r\n";
	      	    	    	
			String filePath = "" + absoluteExcelFilesPath2 + "\\vbScriptForRefreshExcelFile2.vbs";
			File vbScript = new File(filePath);
			if(!vbScript.exists()){
				vbScript.createNewFile();
			}
			FileWriter fw = new java.io.FileWriter(vbScript);
			fw.write(vbs);
			fw.close();
			
			Runtime rt = Runtime.getRuntime();                       
            Process pr = rt.exec("cmd /c cscript "+vbScript.getAbsolutePath()+" "+file.getAbsolutePath()+" 1"); 
            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line=null; 
            while((line=input.readLine()) != null) {
            } 
            int exitVal = pr.waitFor();
            
            if(exitVal==0){    

            }
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}

	@Override
	public List<StudentDetailsBean> getStudentInformation(Integer schoolId,
			String searchFlag, String searchText) {
		// TODO Auto-generated method stub
		List<StudentDetailsBean> studentList = new ArrayList<>();
		if(searchFlag.equalsIgnoreCase("0")) { //Search by registration number only
			studentList=studentMasterRepository.StudentSearchByRegNo(schoolId,searchText);
		}else {
			studentList=studentMasterRepository.StudentSearchByStudentName(schoolId,Integer.parseInt(searchText));
		}

		return studentList;
	}

	@Override
	public List<StudentDetailsBean> getStudentInformation(List<SearchTextBean> searchTextBean1) {

		List<StudentDetailsBean> studentList = new ArrayList<>();
		
		for (SearchTextBean searchTextBean : searchTextBean1) {
		
			StudentDetailsBean studentList1 = new StudentDetailsBean();		
			if(searchTextBean.getSearchFlag().equalsIgnoreCase("0")) 
			{ //Search by registration number only
				studentList1 = studentMasterRepository.StudentSearchByRegNo1(searchTextBean.getSchoolId(),searchTextBean.getSearchText());
			}
			else {
				studentList1 = studentMasterRepository.StudentSearchByStudentName1(searchTextBean.getSchoolId(),Integer.parseInt(searchTextBean.getSearchText()));
			}
			if(studentList1 != null)
				studentList.add(studentList1);
		}
		
		return studentList;
	}
	
	@Override
	public List<StudentDetailsBean> getStudentNameAutoComplete(Integer schoolId, String searchText) {
		// TODO Auto-generated method stub
		return dynamicAdmissionFieldRepositoryImpl.getStudentList(schoolId,searchText);
	}

//	@Override
//	public boolean savemonthlyCatalog(StudentDetailsBean studentDetailsBean) {
//		// TODO Auto-generated method stub
//		
//		System.out.println("yearId - " + studentDetailsBean.getYearId());
//		System.out.println("RenewStudentId - " + studentDetailsBean.getRenewStudentId());
//		System.out.println("divisionId - " + studentDetailsBean.getDivisionId());
////		System.out.println("rollNo - " + studentDetailsBean.getRollNo());
//		System.out.println("studentRollNo - " + studentDetailsBean.getStudentRollNo());
//		System.out.println("studentId - " + studentDetailsBean.getStudentId());
//		System.out.println("schoolId - " + studentDetailsBean.getSchoolId());
//		System.out.println("monthId - " + studentDetailsBean.getMonthId());
//		System.out.println("UserId - " + studentDetailsBean.getUserId());
//		System.out.println("StandardId - " + studentDetailsBean.getStandardId());
//		
//		try {
//			String nextMonthId =Utillity.calculateNextMonth(studentDetailsBean.getMonthId());
//			String[] strArr = nextMonthId.split(",");
//		     Integer[] accadamicMonthArray = new Integer[strArr.length];
//		       for (int i = 0; i < strArr.length; i++) {
//		          String num = strArr[i].trim();
//		          accadamicMonthArray[i] = Integer.parseInt(num);
//		       }
//		    List<Integer> Monthlist = Arrays.stream(accadamicMonthArray).collect(Collectors.toList());
//		    
//			if(CollectionUtils.isNotEmpty(studentDetailsBean.getMonthlyCatelogList())) {
//				studentDetailsBean.getMonthlyCatelogList().stream().forEach(studentList->{
//					Integer renewId = 0;
//					StudentStandardRenewModel  studentStandardRenewModel = studentStandardRepository.getRenewModel(studentList.getRenewStudentId());
//					List<AttendanceMonthCatlogModel> attendanceMonthCatlogModel = attendanceMonthCatlogRepository.getNextMonthAvailableDetails(studentList.getRenewStudentId(),Monthlist);
//					if(studentStandardRenewModel != null) {
//						if(studentStandardRenewModel.getCollegeLeavingDate()!= null && !studentStandardRenewModel.getCollegeLeavingDate().equalsIgnoreCase(null) && !studentStandardRenewModel.getCollegeLeavingDate().equalsIgnoreCase("")) {
//							studentStandardRepository.updateCollegeLeavingDate(studentStandardRenewModel.getRenewStudentId(),studentDetailsBean.getUserId());
//						}
////						new
////						System.out.println("studentList standardId in String is - " + studentList.getStandardId().toString());
//					//	System.out.println("studentDetailsBean standardId in String is - " + studentDetailsBean.getStandardId().toString());
////						studentList.getStandardId().toString()
////						
//						if(studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString()) && studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString()) && attendanceMonthCatlogModel.size()==0) {
//							studentStandardRepository.updateDivisionAndRollNo(studentStandardRenewModel.getRenewStudentId(),studentList.getDivisionId(),Integer.parseInt(studentList.getRollNo()),studentDetailsBean.getUserId());
//						}else if(studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString()) && !studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString())) {
//							if(attendanceMonthCatlogModel.size()>0) {
//								studentStandardRepository.updateStandard(studentStandardRenewModel.getRenewStudentId(),studentList.getStandardId(),studentDetailsBean.getUserId());
//							}else {
//								studentStandardRepository.updateStandardDivisionAndRollNo(studentStandardRenewModel.getRenewStudentId(),studentList.getDivisionId(),Integer.parseInt(studentList.getRollNo()),studentList.getStandardId(),studentDetailsBean.getUserId());
//							}
//						}else if(!studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString()) && studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString())) {
////							new
////							System.out.println(studentStandardRenewModel.getStandardMasterModel().getStandardId());
//							System.out.println("studentDetailsBean standardId is - " + studentDetailsBean.getStandardId());
////							
//							studentStandardRepository.updateAllreadyRenewFlag(studentStandardRenewModel.getRenewStudentId(),studentDetailsBean.getUserId());
////							probably make changes here for renewDate and studFailDate
//							renewId = renewStudent(studentStandardRenewModel,studentList,studentDetailsBean.getUserId(),attendanceMonthCatlogModel.size(),studentList.getStandardId(),studentList.getRenewAdmissionDate());
//						}else if(!studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString()) && !studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString())) {
//							studentStandardRepository.updateAllreadyRenewFlag(studentStandardRenewModel.getRenewStudentId(),studentDetailsBean.getUserId());
////							probably make changes here for renewDate and studFailDate
//							renewId = renewStudent(studentStandardRenewModel,studentList,studentDetailsBean.getUserId(),attendanceMonthCatlogModel.size(),studentList.getStandardId(),studentList.getRenewAdmissionDate());
//						}
//						InsertIntoMonthlyCatlogTable(studentList,renewId,studentDetailsBean.getUserId(),studentDetailsBean.getMonthId());
//					}
//				});
//			}
////			saveStudentDataForOld
//			if(CollectionUtils.isNotEmpty(studentDetailsBean.getNewStudentCatelogList())) {
//				studentDetailsBean.getNewStudentCatelogList().stream().forEach(newStudentList->{
////					saveStudentData(newStudentList,studentDetailsBean.getUserId(),studentDetailsBean.getMonthId());
//					saveStudentDataForOld(newStudentList,studentDetailsBean.getUserId(),studentDetailsBean.getMonthId());
//				});
//			}
//			return true;
//		}catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//			return false;
//		}
//		
//	}

	@Override
	public boolean savemonthlyCatalog(StudentDetailsBean studentDetailsBean1) {

		
//		System.out.println("yearId - " + studentDetailsBean.getYearId());
//		System.out.println("RenewStudentId - " + studentDetailsBean.getRenewStudentId());
//		System.out.println("divisionId - " + studentDetailsBean.getDivisionId());
////		System.out.println("rollNo - " + studentDetailsBean.getRollNo());
//		System.out.println("studentRollNo - " + studentDetailsBean.getStudentRollNo());
//		System.out.println("studentId - " + studentDetailsBean.getStudentId());
//		System.out.println("schoolId - " + studentDetailsBean.getSchoolId());
//		System.out.println("monthId - " + studentDetailsBean.getMonthId());
//		System.out.println("UserId - " + studentDetailsBean.getUserId());
//		System.out.println("StandardId - " + studentDetailsBean.getStandardId());
		
		try {
			
			List<StudentDetailsBean> studentDetailsBean2 = new ArrayList<StudentDetailsBean>();
			
			if(studentDetailsBean1.getMonthlyCatelogList() != null)
			{
//				List<StudentDetailsBean> studentDetailsBean2 = studentDetailsBean1.getMonthlyCatelogList();
				studentDetailsBean2 = studentDetailsBean1.getMonthlyCatelogList();
				
				for (StudentDetailsBean studentDetailsBean : studentDetailsBean2) {
					
					studentDetailsBean.setMonthId(studentDetailsBean1.getMonthId());
					studentDetailsBean.setUserId(studentDetailsBean1.getUserId());
					studentDetailsBean.setMonthlyCatelogList(studentDetailsBean2);
					
					String nextMonthId = Utillity.calculateNextMonth(studentDetailsBean.getMonthId());
					String[] strArr = nextMonthId.split(",");
					Integer[] accadamicMonthArray = new Integer[strArr.length];
					for (int i = 0; i < strArr.length; i++) {
						String num = strArr[i].trim();
						accadamicMonthArray[i] = Integer.parseInt(num);
					}
					List<Integer> Monthlist = Arrays.stream(accadamicMonthArray).collect(Collectors.toList());
					if (CollectionUtils.isNotEmpty(studentDetailsBean.getMonthlyCatelogList())) {
						studentDetailsBean.getMonthlyCatelogList().stream().forEach(studentList -> {
							Integer renewId = 0;
							StudentStandardRenewModel studentStandardRenewModel = studentStandardRepository.getRenewModel(studentList.getRenewStudentId());
							List<AttendanceMonthCatlogModel> attendanceMonthCatlogModel = attendanceMonthCatlogRepository
									.getNextMonthAvailableDetails(studentList.getRenewStudentId(), Monthlist);
							if (studentStandardRenewModel != null) 
							{
								if (studentStandardRenewModel.getCollegeLeavingDate() != null && !studentStandardRenewModel.getCollegeLeavingDate().equalsIgnoreCase(null)
										&& !studentStandardRenewModel.getCollegeLeavingDate().equalsIgnoreCase("")) 
								{
									studentStandardRepository.updateCollegeLeavingDate(studentStandardRenewModel.getRenewStudentId(), studentDetailsBean.getUserId());
								}
								//						new
								//						System.out.println("studentList standardId in String is - " + studentList.getStandardId().toString());
								//	System.out.println("studentDetailsBean standardId in String is - " + studentDetailsBean.getStandardId().toString());
								//						studentList.getStandardId().toString()
								//						
								if (studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString())
										&& studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString())
										&& attendanceMonthCatlogModel.size() == 0) 
								{
//									studentStandardRepository.updateDivisionAndRollNo(studentStandardRenewModel.getRenewStudentId(), studentList.getDivisionId(),
//											Integer.parseInt(studentList.getRollNo()), studentDetailsBean.getUserId());
									studentStandardRepository.updateDivisionAndRollNo(studentStandardRenewModel.getRenewStudentId(), studentList.getDivisionId(),
											Integer.parseInt(studentList.getRollNo()));
								} 
								else if (studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString())
										&& !studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString())) 
								{
									if (attendanceMonthCatlogModel.size() > 0) 
									{
										studentStandardRepository.updateStandard(
												studentStandardRenewModel.getRenewStudentId(), studentList.getStandardId(), studentDetailsBean.getUserId());
									} 
									else {
										studentStandardRepository.updateStandardDivisionAndRollNo(studentStandardRenewModel.getRenewStudentId(), studentList.getDivisionId(),
												Integer.parseInt(studentList.getRollNo()), studentList.getStandardId(), studentDetailsBean.getUserId());
									}
								} 
								else if (!studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString())
										&& studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString())) {
									//							new
									//							System.out.println(studentStandardRenewModel.getStandardMasterModel().getStandardId());
									System.out.println("studentDetailsBean standardId is - " + studentDetailsBean.getStandardId());
									//							
									studentStandardRepository.updateAllreadyRenewFlag(studentStandardRenewModel.getRenewStudentId(), studentDetailsBean.getUserId());
									//							probably make changes here for renewDate and studFailDate
									renewId = renewStudent(studentStandardRenewModel, studentList, studentDetailsBean.getUserId(), attendanceMonthCatlogModel.size(),
											studentList.getStandardId(), studentList.getRenewAdmissionDate());
								} 
								else if (!studentList.getYearId().toString().equalsIgnoreCase(studentStandardRenewModel.getYearMasterModel().getYearId().toString())
										&& !studentList.getStandardId().toString().equalsIgnoreCase(studentStandardRenewModel.getStandardMasterModel().getStandardId().toString())) 
								{
									studentStandardRepository.updateAllreadyRenewFlag(studentStandardRenewModel.getRenewStudentId(), studentDetailsBean.getUserId());
									//							probably make changes here for renewDate and studFailDate
									renewId = renewStudent(studentStandardRenewModel, studentList, studentDetailsBean.getUserId(), attendanceMonthCatlogModel.size(),
											studentList.getStandardId(), studentList.getRenewAdmissionDate());
								}
								InsertIntoMonthlyCatlogTable(studentList, renewId, studentDetailsBean.getUserId(), studentDetailsBean.getMonthId());
							}
						});
					}
			}

//			for (int i = 0; i < array.length; i++) {

				
//				if(studentDetailsBean1.getNewStudentCatelogList() != null)
//				{
////					List<StudentDetailsBean> studentDetailsBean2 = studentDetailsBean1.getNewStudentCatelogList();
//					studentDetailsBean2 = studentDetailsBean1.getNewStudentCatelogList();
//				}
				//			saveStudentDataForOld
//				if (CollectionUtils.isNotEmpty(studentDetailsBean.getNewStudentCatelogList()))
//				if (CollectionUtils.isNotEmpty(studentDetailsBean1.getNewStudentCatelogList()))
			}
				if(studentDetailsBean1.getNewStudentCatelogList() != null)
				{
					studentDetailsBean1.getNewStudentCatelogList().stream().forEach(newStudentList -> {
						//					saveStudentData(newStudentList,studentDetailsBean.getUserId(),studentDetailsBean.getMonthId());
						saveStudentDataForOld(newStudentList, studentDetailsBean1.getUserId(), studentDetailsBean1.getMonthId());
					});
				}
				return true;
//			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
//		return true;		
	}
	
	private void InsertIntoMonthlyCatlogTable(StudentDetailsBean studentList, Integer renewId, Integer userId, Integer monthId) {
		// TODO Auto-generated method stub
		AppUserModel appUserRole = new AppUserModel();
		appUserRole.setAppUserRoleId(userId);
		
		SchoolMasterModel  schoolMasterModel =new SchoolMasterModel();
		schoolMasterModel.setSchoolid(studentList.getSchoolId());
		
        YearMasterModel yearMasterModel =new YearMasterModel();
        yearMasterModel.setYearId(studentList.getYearId());

		StandardMasterModel standardMasterModel=new StandardMasterModel();
		standardMasterModel.setStandardId(studentList.getStandardId());
		
		DivisionMasterModel divisionMasterModel=new DivisionMasterModel();
		divisionMasterModel.setDivisionId(studentList.getDivisionId());
		
		StudentMasterModel studentMasterModel=new StudentMasterModel();
		studentMasterModel.setStudentId(studentList.getStudentId());
		
		Integer studentRenewId;
		StudentStandardRenewModel studentStandardRenewModel=new StudentStandardRenewModel();
		if(renewId == 0 || renewId == 1) {
			studentStandardRenewModel.setRenewStudentId(studentList.getRenewStudentId());
			studentRenewId=studentList.getRenewStudentId();
		}else {
			studentStandardRenewModel.setRenewStudentId(renewId);
			studentRenewId=renewId;
		}
		
		
		AttendanceMonthCatlogModel attendanceMonthCatalogModel=new AttendanceMonthCatlogModel();
		attendanceMonthCatalogModel.setStudentMasterModel(studentMasterModel);
		attendanceMonthCatalogModel.setStudentStandardRenewModel(studentStandardRenewModel);
		attendanceMonthCatalogModel.setMonthId(Integer.valueOf(monthId));
		attendanceMonthCatalogModel.setYearMasterModel(yearMasterModel);
		attendanceMonthCatalogModel.setStandardMasterModel(standardMasterModel);
		attendanceMonthCatalogModel.setSchoolMasterModel(schoolMasterModel);
		attendanceMonthCatalogModel.setDivisionMasterModel(divisionMasterModel);
		attendanceMonthCatalogModel.setRollNo(Integer.valueOf(studentList.getRollNo()));
		
		AttendanceMonthCatlogModel monthlyCatelogModel = attendanceMonthCatlogRepository.getStudentDetails(studentRenewId,Integer.valueOf(monthId));
		if(monthlyCatelogModel != null) {
			attendanceMonthCatalogModel.setAttandenceMonthCatalogId(monthlyCatelogModel.getAttandenceMonthCatalogId());
			attendanceMonthCatalogModel.setIsEdit("1");	
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			attendanceMonthCatalogModel.setEditBy(appUserRoleModel);
			}else {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentList.getSchoolId());
			String globalDbSchoolKey = "1".concat(""+schoolModel.getSchoolKey());
			String incrementedId = ""+attendanceMonthCatlogRepository.getMaxId(studentList.getSchoolId());
			String attendanceId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			attendanceMonthCatalogModel.setAttandenceMonthCatalogId(Integer.parseInt(attendanceId));
		}
		
		attendanceMonthCatlogRepository.saveAndFlush(attendanceMonthCatalogModel);

	}

	private void saveStudentData(StudentDetailsBean studentList, Integer userId, Integer monthId) {
		// TODO Auto-generated method stub
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			LocalDate localDate = LocalDate.now();
			String date = (dtf.format(localDate)); // 2016/11/16
			
			StudentMasterModel studentMasterModel = new StudentMasterModel();
			
//			Integer principaluserId = userRepository.getprincipalUserId(studentList.getSchoolId());
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			
			if(!studentList.getStudentCategory().equalsIgnoreCase("0")) {
				CategoryMasterModel categoryMasterModel = new CategoryMasterModel();
				categoryMasterModel.setCategoryId(Integer.parseInt(studentList.getStudentCategory()));
				studentMasterModel.setCategory(categoryMasterModel);
			}
			
			if(!studentList.getStudentReligion().equalsIgnoreCase("0")) {
				ReligionMasterModel religionMasterModel = new ReligionMasterModel();
				religionMasterModel.setReligionId(Integer.parseInt(studentList.getStudentReligion()));
				studentMasterModel.setReligion(religionMasterModel);
			}
			
//			new
			if(!studentList.getStudentCast().equalsIgnoreCase("0")) {
				CasteMasterModel casteMasterModel = new CasteMasterModel();
				casteMasterModel.setCasteId(Integer.parseInt(studentList.getStudentCast()));
				studentMasterModel.setCastemaster(casteMasterModel);
			}
//			
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentList.getSchoolId());
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			String incrementedId = "" + studentMasterRepository.getMaxId(studentList.getSchoolId());
			String studentId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			studentMasterModel.setStudentId(Integer.parseInt(studentId));
			
			if(studentList.getStudentRegNo() == null || studentList.getStudentRegNo().equalsIgnoreCase("")) {
				String provisionalNo = studentMasterRepository.getMaxProvisionalNo(studentList.getSchoolId());
				studentMasterModel.setStudentRegNo("P" + provisionalNo);
				studentMasterModel.setProvisionalRegNo("P" + provisionalNo);
			}
			else {
				studentMasterModel.setStudentRegNo(studentList.getStudentRegNo());
				studentMasterModel.setProvisionalRegNo(studentList.getStudentRegNo());
			}
			String formNumber = studentMasterRepository.findMaxFormNumber(studentList.getSchoolId());
			formNumber = "F".concat(formNumber);
			studentMasterModel.setStudentFormNo(formNumber);
			studentMasterModel.setAdmissionDate(date);
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentList.getSchoolId());
			studentMasterModel.setSchoolMasterModel(schoolMasterModel);
			String getMaxGrBookId = studentMasterRepository.getMaxGrBookId(studentList.getSchoolId());
			GRBookNameModel gRBookNameModel = new GRBookNameModel();
			gRBookNameModel.setGrBookId(Integer.valueOf(getMaxGrBookId));
			studentMasterModel.setGrBookName(gRBookNameModel);
			studentMasterModel.setMmobile(studentList.getMobileNo());
			studentMasterModel.setStudInitialName(studentList.getInitialName());
			studentMasterModel.setStudFName(studentList.getFirstName());
			studentMasterModel.setStudLName(studentList.getLastName());
			studentMasterModel.setStudMName(studentList.getMiddleName());
			studentMasterModel.setStudGender(studentList.getStudentGender());
			studentMasterModel.setBirthDate(studentList.getBirth_date());
			studentMasterModel.setMotherName(studentList.getMotherName());
			studentMasterModel.setIsOnlineAdm("0");
			studentMasterModel.setIsQuickAdmission(0);
			studentMasterModel.setIsApproval("1");
			studentMasterModel.setPreviousState("" + studentList.getYearId());
			studentMasterModel.setAdmissionDate(date);
			studentMasterModel.setMedium("Select Medium");
			studentMasterModel.setUserId(appUserRoleModel);
			studentMasterModel = studentMasterRepository.save(studentMasterModel);
			
//			saveStudentStandardRenew(studentMasterModel,studentList,userId,monthId);
			saveStudentStandardRenewForOld(studentMasterModel,studentList,userId,monthId);
			List<AppUserRoleBean> appuserRoleList = new ArrayList<AppUserRoleBean>();
			for (int i = 0; i < 3; i++) {
				String incrementedId11 = "" + userRepository.getMaxUserId(studentList.getSchoolId());
				String maxUserId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId11);
				AppUserModel user = new AppUserModel();
				user.setAppUserRoleId(Integer.parseInt(maxUserId));
				if (i == 0) {
					user.setRoleName("ROLE_STUDENT");

				}
				if (i == 1) {
					user.setRoleName("ROLE_PARENT1");
				}
				if (i == 2) {
					user.setRoleName("ROLE_PARENT2");
				}

				user.setUsername(studentList.getMobileNo());
				user.setStaffId(studentMasterModel.getStudentId());
				user.setPassword(md5("ingenio123"));
				user.setFirstName(studentList.getFirstName());
				if (studentList.getLastName().length() > 0) {
					user.setLastName(studentList.getLastName());
				} else {
					user.setLastName(studentList.getLastName());
				}
				user.setSchoolid(schoolMasterModel.getSchoolid());
				user.setSettingString("fjEHuisqm3ASr9F8oUudICFblqLrBExRMl4ywbLz8A/FEd9Uod5PQopFWu5+9C4oUlvghDncYonqPqEtO4SF/Guo+lq6LrpGXqMtRv4Pu9ZffpvBUFEUrUAloT62u5CLBW5LygVsBWEBzfrAlsn7qDncN0612klKx0/tKJ/3ulPvsmgdrt65tJJpFvntlKeo");
				AppUserModel user1 = userRepository.saveAndFlush(user);
				AppUserRoleBean appUserRoleBean = new AppUserRoleBean();
				appUserRoleBean.setAppUserRoleId(user1.getAppUserRoleId());
				appUserRoleBean.setRoleName(user1.getRoleName());
				appUserRoleBean.setFirstName(user1.getFirstName());
				appUserRoleBean.setLastName(user1.getLastName());
				appUserRoleBean.setPassword(user1.getPassword());
				appUserRoleBean.setUserName(user1.getUsername());
				appuserRoleList.add(appUserRoleBean);

				AppUserRoleModel appUserRole = new AppUserRoleModel();
				appUserRole.setAppUserRoleId(Integer.parseInt(maxUserId));
				appUserRole.setRoleName("ROLE_ADMIN");
				appUserRole.setUsername(studentList.getMobileNo());
				appUserRole.setSchoolid(schoolMasterModel.getSchoolid());
				appRoleRepository.saveAndFlush(appUserRole);
				if (i == 0) {
					// for fee setting donot remove
					studentMasterModel.setUserId(appUserRole);
				}

			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveStudentStandardRenew(StudentMasterModel studentMasterModel, StudentDetailsBean studentList, Integer userId, Integer monthId) {
		// TODO Auto-generated method stub
		try {
			StudentStandardRenewModel studentRenewModel = new StudentStandardRenewModel();
			
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentList.getSchoolId());
            String year=yearMasterRepository.getYear(studentList.getYearId());
            String renewDate=dynamicAcademicMonthsRepository.getFromMonth(studentList.getSchoolId());
			String []ayear=year.split("-");
			
			if(studentList.getOldYearId()>0) {
				
				SchoolMasterModel schoolModel1 = schoolMasterRepository.findBySchoolid(studentList.getSchoolId());
				String globalDbSchoolKey1 = "1".concat("" + schoolModel1.getSchoolKey());
				String incrementedId11 = "" + studentMasterRepository.getMaxRenewId(studentList.getSchoolId());
				String renewId1 = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey1, incrementedId11);
	         
				
				String yearOld=yearMasterRepository.getYear(studentList.getOldYearId());
				String []oldYear=yearOld.split("-");
				String oldRenewAdmissiondate=renewDate.concat("-"+Utillity.convertOtherToEnglish(""+oldYear[0]));
				
				StudentStandardRenewModel studentRenewModel1 = new StudentStandardRenewModel();
				
				
				YearMasterModel yearMasterModel = new YearMasterModel();
				yearMasterModel.setYearId(studentList.getOldYearId());
				studentRenewModel1.setYearMasterModel(yearMasterModel);
				
				StandardMasterModel standrdMasterModel = new StandardMasterModel();
				standrdMasterModel.setStandardId(studentList.getOldStandardId());
				studentRenewModel1.setStandardMasterModel(standrdMasterModel);
				
//				DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
//				divisionMasterModel.setDivisionId(studentList.getDivisionId());
//				studentRenewModel1.setDivisionMasterModel(divisionMasterModel);
				
				//studentRenewModel1.setRollNo(Integer.parseInt(studentList.getRollNo()));
				
				studentRenewModel1.setStudentMasterModel(studentMasterModel);
				
				studentRenewModel1.setAlreadyRenew((short) 0);
				studentRenewModel.setRenewAdmissionDate(studentList.getAdmissionDate());
				
				studentRenewModel1.setIsOnlineAdmission("0");
				studentRenewModel1.setIsApproval("1");
				studentRenewModel1.setOnlineAdmissionApprovalFlag("0");
				studentRenewModel1.setSchoolMasterModel(schoolModel);
				studentRenewModel1.setIsRegistration("0");
				studentRenewModel1.setRenewStudentId(Integer.parseInt(renewId1));
				//studentRenewModel1.setStudFailDate(studentList.get);
				studentRenewModel1.setRenewAdmissionDate(oldRenewAdmissiondate);			
				AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
				appUserRoleModel.setAppUserRoleId(userId);
				studentRenewModel1.setUserId(appUserRoleModel);
				
				Integer studentRenewId = studentStandardRepository.saveAndFlush(studentRenewModel1).getRenewStudentId();
				
				
				
			}
			
			String renewAdmissionDate=yearMasterRepository.findDefaultRenewAdmissionDate(studentList.getYearId());
			
			if(renewAdmissionDate==null) {
			 renewAdmissionDate=renewDate.concat("-"+Utillity.convertOtherToEnglish(""+ayear[0]));
			}
			YearMasterModel yearMasterModel = new YearMasterModel();
			yearMasterModel.setYearId(studentList.getYearId());
			studentRenewModel.setYearMasterModel(yearMasterModel);
			
			StandardMasterModel standrdMasterModel = new StandardMasterModel();
			standrdMasterModel.setStandardId(studentList.getStandardId());
			studentRenewModel.setStandardMasterModel(standrdMasterModel);
			
			DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
			divisionMasterModel.setDivisionId(studentList.getDivisionId());
			studentRenewModel.setDivisionMasterModel(divisionMasterModel);
			
			studentRenewModel.setRollNo(Integer.parseInt(studentList.getRollNo()));
			
			studentRenewModel.setStudentMasterModel(studentMasterModel);
			
			studentRenewModel.setAlreadyRenew((short) 1);
			studentRenewModel.setRenewAdmissionDate(studentList.getAdmissionDate());
			
			studentRenewModel.setIsOnlineAdmission("0");
			studentRenewModel.setIsApproval("1");
			studentRenewModel.setOnlineAdmissionApprovalFlag("0");
			studentRenewModel.setSchoolMasterModel(schoolModel);
			studentRenewModel.setIsRegistration("0");
			
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			String incrementedId1 = "" + studentMasterRepository.getMaxRenewId(studentList.getSchoolId());
			String renewId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId1);

			studentRenewModel.setRenewStudentId(Integer.parseInt(renewId));
			studentRenewModel.setStudFailDate(studentList.getAdmissionDate());
			studentRenewModel.setRenewAdmissionDate(renewAdmissionDate);			
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			studentRenewModel.setUserId(appUserRoleModel);
			
			Integer studentRenewId = studentStandardRepository.saveAndFlush(studentRenewModel).getRenewStudentId();
			
			//insert into monthly catlog table
			AppUserModel appUserRole = new AppUserModel();
			appUserRole.setAppUserRoleId(userId);
			
			SchoolMasterModel  schoolMasterModel =new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentList.getSchoolId());
			
			StudentStandardRenewModel studentStandardRenewModel=new StudentStandardRenewModel();
			studentStandardRenewModel.setRenewStudentId(studentRenewId);
			
			AttendanceMonthCatlogModel attendanceMonthCatalogModel=new AttendanceMonthCatlogModel();
			attendanceMonthCatalogModel.setStudentMasterModel(studentMasterModel);
			attendanceMonthCatalogModel.setStudentStandardRenewModel(studentStandardRenewModel);
			attendanceMonthCatalogModel.setMonthId(Integer.valueOf(monthId));
			attendanceMonthCatalogModel.setYearMasterModel(yearMasterModel);
			attendanceMonthCatalogModel.setStandardMasterModel(standrdMasterModel);
			attendanceMonthCatalogModel.setSchoolMasterModel(schoolMasterModel);
			attendanceMonthCatalogModel.setDivisionMasterModel(divisionMasterModel);
			attendanceMonthCatalogModel.setRollNo(Integer.valueOf(studentList.getRollNo()));
			
			String incrementedId = ""+attendanceMonthCatlogRepository.getMaxId(studentList.getSchoolId());
			String attendanceId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			attendanceMonthCatalogModel.setAttandenceMonthCatalogId(Integer.parseInt(attendanceId));
			
			
			attendanceMonthCatlogRepository.save(attendanceMonthCatalogModel);


		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private Integer renewStudent(StudentStandardRenewModel studentStandardRenewModel, StudentDetailsBean studentList, Integer userId, int modelSize,Integer standardId, String renewAddDate) {
		// TODO Auto-generated method stub
		try {
			StudentStandardRenewModel studentRenewModel = new StudentStandardRenewModel();
			
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentStandardRenewModel.getSchoolMasterModel().getSchoolid());
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			String incrementedId1 = "" + studentMasterRepository.getMaxRenewId(studentStandardRenewModel.getSchoolMasterModel().getSchoolid());
			String renewId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
			
			String renewDate=dynamicAcademicMonthsRepository.getFromMonth(studentList.getSchoolId());
			String year=yearMasterRepository.getYear(studentList.getYearId());
			
			String []ayear=year.split("-");
			
			
			String renewAdmissionDate=yearMasterRepository.findDefaultRenewAdmissionDate(studentList.getSchoolId());
			
			if(renewAdmissionDate==null) {
			 renewAdmissionDate=renewDate.concat("-"+Utillity.convertOtherToEnglish(ayear[0]));
			}
			YearMasterModel yearMasterModel = new YearMasterModel();
			yearMasterModel.setYearId(studentList.getYearId());
			studentRenewModel.setYearMasterModel(yearMasterModel);
			
			System.out.println(standardId);
			
			StandardMasterModel standardmasterModel=new StandardMasterModel();
			standardmasterModel.setStandardId(standardId);
			studentRenewModel.setStandardMasterModel(standardmasterModel);
			if(modelSize==0) {
				DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
				divisionMasterModel.setDivisionId(studentList.getDivisionId());
				studentRenewModel.setDivisionMasterModel(divisionMasterModel);
				
				studentRenewModel.setRollNo(Integer.parseInt(studentList.getRollNo()));
			}
			
			studentRenewModel.setStudentMasterModel(studentStandardRenewModel.getStudentMasterModel());
			
			studentRenewModel.setAlreadyRenew((short) 1);

//			new 1
			if(studentList.getRenewAdmissionDate() != null)
			{
				studentRenewModel.setRenewAdmissionDate(studentList.getRenewAdmissionDate());
				studentRenewModel.setStudFailDate(studentList.getRenewAdmissionDate());
			}
//			new 1 end
			
			else if(renewAddDate!=null) {
				studentRenewModel.setRenewAdmissionDate(renewAddDate);
				studentRenewModel.setStudFailDate(renewAddDate);

			}else {
			studentRenewModel.setRenewAdmissionDate(renewAdmissionDate);
			studentRenewModel.setStudFailDate(renewAdmissionDate);
			}			
			
			studentRenewModel.setIsRegistration("0");
			
			studentRenewModel.setIsOnlineAdmission("0");
			studentRenewModel.setIsApproval("1");
			studentRenewModel.setOnlineAdmissionApprovalFlag("0");
			studentRenewModel.setSchoolMasterModel(schoolModel);
			
			studentRenewModel.setRenewStudentId(Integer.parseInt(renewId));
			
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			studentRenewModel.setUserId(appUserRoleModel);
			
			return studentStandardRepository.saveAndFlush(studentRenewModel).getRenewStudentId();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 1;
		}
		
		
		
		
	}

	@Override
	public Integer updateStudentRegNumber(String studentRegNumber, Integer schoolId, Integer studentId) {
		Integer id=0;
		try {
		 id=studentMasterRepository.updateStudentRegNumber(studentRegNumber,schoolId,studentId);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return id;
	}

//	public static String nFormate(double d) {
//	    NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
//	    nf.setMaximumFractionDigits(10);
//	    String st= nf.format(d);
//	    return st;
//	}

	private void saveStudentDataForOld(StudentDetailsBean studentList, Integer userId, Integer monthId) {
		// TODO Auto-generated method stub
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			LocalDate localDate = LocalDate.now();
			String date = (dtf.format(localDate)); // 2016/11/16
			
			StudentMasterModel studentMasterModel = new StudentMasterModel();
			
//			Integer principaluserId = userRepository.getprincipalUserId(studentList.getSchoolId());
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			
			if(!studentList.getStudentCategory().equalsIgnoreCase("0")) {
				CategoryMasterModel categoryMasterModel = new CategoryMasterModel();
				categoryMasterModel.setCategoryId(Integer.parseInt(studentList.getStudentCategory()));
				studentMasterModel.setCategory(categoryMasterModel);
			}
			
			if(!studentList.getStudentReligion().equalsIgnoreCase("0")) {
				ReligionMasterModel religionMasterModel = new ReligionMasterModel();
				religionMasterModel.setReligionId(Integer.parseInt(studentList.getStudentReligion()));
				studentMasterModel.setReligion(religionMasterModel);
			}
			
//			new
			if(!studentList.getStudentCast().equalsIgnoreCase("0")) {
				CasteMasterModel casteMasterModel = new CasteMasterModel();
				casteMasterModel.setCasteId(Integer.parseInt(studentList.getStudentCast()));
				studentMasterModel.setCastemaster(casteMasterModel);
			}
//			
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentList.getSchoolId());
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			String incrementedId = "" + studentMasterRepository.getMaxId(studentList.getSchoolId());
			String studentId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			studentMasterModel.setStudentId(Integer.parseInt(studentId));
			
			if(studentList.getStudentRegNo() == null || studentList.getStudentRegNo().equalsIgnoreCase("")) {
				String provisionalNo = studentMasterRepository.getMaxProvisionalNo(studentList.getSchoolId());
				studentMasterModel.setStudentRegNo("P" + provisionalNo);
				studentMasterModel.setProvisionalRegNo("P" + provisionalNo);
			}
			else {
				studentMasterModel.setStudentRegNo(studentList.getStudentRegNo());
				studentMasterModel.setProvisionalRegNo(studentList.getStudentRegNo());
			}
			String formNumber = studentMasterRepository.findMaxFormNumber(studentList.getSchoolId());
			formNumber = "F".concat(formNumber);
			studentMasterModel.setStudentFormNo(formNumber);
			
//			new
//			if(studentList.getOldAdmissionDate() == null)
			if ((studentList.getOldAdmissionDate() == null || (studentList.getOldAdmissionDate().equalsIgnoreCase("0"))
					|| studentList.getOldAdmissionDate().equalsIgnoreCase("")))
					{
				studentMasterModel.setAdmissionDate(studentList.getAdmissionDate());
					}
			else
			{
				studentMasterModel.setAdmissionDate(studentList.getOldAdmissionDate());
			}
//			studentMasterModel.setAdmissionDate(date);
//			
//			studentMasterModel.setAdmissionDate(date);
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentList.getSchoolId());
			studentMasterModel.setSchoolMasterModel(schoolMasterModel);
			String getMaxGrBookId = studentMasterRepository.getMaxGrBookId(studentList.getSchoolId());
			GRBookNameModel gRBookNameModel = new GRBookNameModel();
			gRBookNameModel.setGrBookId(Integer.valueOf(getMaxGrBookId));
			studentMasterModel.setGrBookName(gRBookNameModel);
			studentMasterModel.setMmobile(studentList.getMobileNo());
			studentMasterModel.setStudInitialName(studentList.getInitialName());
			studentMasterModel.setStudFName(studentList.getFirstName());
			studentMasterModel.setStudLName(studentList.getLastName());
			studentMasterModel.setStudMName(studentList.getMiddleName());
			studentMasterModel.setStudGender(studentList.getStudentGender());
			studentMasterModel.setBirthDate(studentList.getBirth_date());
			studentMasterModel.setMotherName(studentList.getMotherName());
			studentMasterModel.setIsOnlineAdm("0");
			studentMasterModel.setIsQuickAdmission(0);
//			new - 27-07-2023
			studentMasterModel.setIsApproval("0");
//			
//			studentMasterModel.setIsApproval("1");
			studentMasterModel.setPreviousState("" + studentList.getYearId());
//			new
//			studentMasterModel.setAdmissionDate(date);
//			new end
			studentMasterModel.setMedium("Select Medium");
			studentMasterModel.setUserId(appUserRoleModel);
			studentMasterModel = studentMasterRepository.save(studentMasterModel);
			
//			saveStudentStandardRenew(studentMasterModel,studentList,userId,monthId);
			saveStudentStandardRenewForOld(studentMasterModel,studentList,userId,monthId);
			List<AppUserRoleBean> appuserRoleList = new ArrayList<AppUserRoleBean>();
			for (int i = 0; i < 3; i++) {
				String incrementedId11 = "" + userRepository.getMaxUserId(studentList.getSchoolId());
				String maxUserId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId11);
				AppUserModel user = new AppUserModel();
				user.setAppUserRoleId(Integer.parseInt(maxUserId));
				if (i == 0) {
					user.setRoleName("ROLE_STUDENT");

				}
				if (i == 1) {
					user.setRoleName("ROLE_PARENT1");
				}
				if (i == 2) {
					user.setRoleName("ROLE_PARENT2");
				}

				user.setUsername(studentList.getMobileNo());
				user.setStaffId(studentMasterModel.getStudentId());
				user.setPassword(md5("ingenio123"));
				user.setFirstName(studentList.getFirstName());
				if (studentList.getLastName().length() > 0) {
					user.setLastName(studentList.getLastName());
				} else {
					user.setLastName(studentList.getLastName());
				}
				user.setSchoolid(schoolMasterModel.getSchoolid());
				user.setSettingString("fjEHuisqm3ASr9F8oUudICFblqLrBExRMl4ywbLz8A/FEd9Uod5PQopFWu5+9C4oUlvghDncYonqPqEtO4SF/Guo+lq6LrpGXqMtRv4Pu9ZffpvBUFEUrUAloT62u5CLBW5LygVsBWEBzfrAlsn7qDncN0612klKx0/tKJ/3ulPvsmgdrt65tJJpFvntlKeo");
				AppUserModel user1 = userRepository.saveAndFlush(user);
				AppUserRoleBean appUserRoleBean = new AppUserRoleBean();
				appUserRoleBean.setAppUserRoleId(user1.getAppUserRoleId());
				appUserRoleBean.setRoleName(user1.getRoleName());
				appUserRoleBean.setFirstName(user1.getFirstName());
				appUserRoleBean.setLastName(user1.getLastName());
				appUserRoleBean.setPassword(user1.getPassword());
				appUserRoleBean.setUserName(user1.getUsername());
				appuserRoleList.add(appUserRoleBean);

				AppUserRoleModel appUserRole = new AppUserRoleModel();
				appUserRole.setAppUserRoleId(Integer.parseInt(maxUserId));
				appUserRole.setRoleName("ROLE_ADMIN");
				appUserRole.setUsername(studentList.getMobileNo());
				appUserRole.setSchoolid(schoolMasterModel.getSchoolid());
				appRoleRepository.saveAndFlush(appUserRole);
				if (i == 0) {
					// for fee setting donot remove
					studentMasterModel.setUserId(appUserRole);
				}

			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void saveStudentStandardRenewForOld(StudentMasterModel studentMasterModel, StudentDetailsBean studentList, Integer userId, Integer monthId) {
		// TODO Auto-generated method stub
		try {
			StudentStandardRenewModel studentRenewModel = new StudentStandardRenewModel();
			
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(studentList.getSchoolId());
            String year=yearMasterRepository.getYear(studentList.getYearId());
            String renewDate=dynamicAcademicMonthsRepository.getFromMonth(studentList.getSchoolId());
			String []ayear=year.split("-");
			
			if(studentList.getOldYearId()>0) {
				
				SchoolMasterModel schoolModel1 = schoolMasterRepository.findBySchoolid(studentList.getSchoolId());
				String globalDbSchoolKey1 = "1".concat("" + schoolModel1.getSchoolKey());
				String incrementedId11 = "" + studentMasterRepository.getMaxRenewId(studentList.getSchoolId());
				String renewId1 = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey1, incrementedId11);
	         
//				new
				
//				String yearOld=yearMasterRepository.getYear(studentList.getOldYearId());
//				String []oldYear=yearOld.split("-");
//				String oldRenewAdmissiondate=renewDate.concat("-"+Utillity.convertOtherToEnglish(""+oldYear[0]));
				
				String oldRenewAdmissiondate = studentList.getOldAdmissionDate();
//				new end
				
				StudentStandardRenewModel studentRenewModel1 = new StudentStandardRenewModel();
				
				
				YearMasterModel yearMasterModel = new YearMasterModel();
				yearMasterModel.setYearId(studentList.getOldYearId());
				studentRenewModel1.setYearMasterModel(yearMasterModel);
				
				StandardMasterModel standrdMasterModel = new StandardMasterModel();
				standrdMasterModel.setStandardId(studentList.getOldStandardId());
				studentRenewModel1.setStandardMasterModel(standrdMasterModel);
				
//				DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
//				divisionMasterModel.setDivisionId(studentList.getDivisionId());
//				studentRenewModel1.setDivisionMasterModel(divisionMasterModel);
				
				//studentRenewModel1.setRollNo(Integer.parseInt(studentList.getRollNo()));
				
				studentRenewModel1.setStudentMasterModel(studentMasterModel);
				
				studentRenewModel1.setAlreadyRenew((short) 0);
//				new
//				studentRenewModel.setRenewAdmissionDate(studentList.getAdmissionDate());
//				new end
				studentRenewModel1.setIsOnlineAdmission("0");
//				new - 27-07-2023
				studentRenewModel1.setIsApproval("0");
//				
//				studentRenewModel1.setIsApproval("1");
				studentRenewModel1.setOnlineAdmissionApprovalFlag("0");
				studentRenewModel1.setSchoolMasterModel(schoolModel);
				studentRenewModel1.setIsRegistration("0");
				studentRenewModel1.setRenewStudentId(Integer.parseInt(renewId1));
				//studentRenewModel1.setStudFailDate(studentList.get);
//				new
				studentRenewModel1.setStudFailDate(oldRenewAdmissiondate);
//				new end
				studentRenewModel1.setRenewAdmissionDate(oldRenewAdmissiondate);			
				AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
				appUserRoleModel.setAppUserRoleId(userId);
				studentRenewModel1.setUserId(appUserRoleModel);
				
				Integer studentRenewId = studentStandardRepository.saveAndFlush(studentRenewModel1).getRenewStudentId();
				
				
				
			}
			
			String renewAdmissionDate=yearMasterRepository.findDefaultRenewAdmissionDate(studentList.getYearId());
			
			if(renewAdmissionDate==null) {
			 renewAdmissionDate=renewDate.concat("-"+Utillity.convertOtherToEnglish(""+ayear[0]));
			}
			YearMasterModel yearMasterModel = new YearMasterModel();
			yearMasterModel.setYearId(studentList.getYearId());
			studentRenewModel.setYearMasterModel(yearMasterModel);
			
			StandardMasterModel standrdMasterModel = new StandardMasterModel();
			standrdMasterModel.setStandardId(studentList.getStandardId());
			studentRenewModel.setStandardMasterModel(standrdMasterModel);
			
			DivisionMasterModel divisionMasterModel = new DivisionMasterModel();
			divisionMasterModel.setDivisionId(studentList.getDivisionId());
			studentRenewModel.setDivisionMasterModel(divisionMasterModel);
			
			studentRenewModel.setRollNo(Integer.parseInt(studentList.getRollNo()));
			
			studentRenewModel.setStudentMasterModel(studentMasterModel);
			
			studentRenewModel.setAlreadyRenew((short) 1);
			studentRenewModel.setRenewAdmissionDate(studentList.getAdmissionDate());
			
			studentRenewModel.setIsOnlineAdmission("0");
//			new - 27-07-2023
			studentRenewModel.setIsApproval("0");
//			
//			studentRenewModel.setIsApproval("1");
			studentRenewModel.setOnlineAdmissionApprovalFlag("0");
			studentRenewModel.setSchoolMasterModel(schoolModel);
			studentRenewModel.setIsRegistration("0");
			
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			String incrementedId1 = "" + studentMasterRepository.getMaxRenewId(studentList.getSchoolId());
			String renewId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId1);

			studentRenewModel.setRenewStudentId(Integer.parseInt(renewId));
			studentRenewModel.setStudFailDate(studentList.getAdmissionDate());
//			new
//			studentRenewModel.setRenewAdmissionDate(renewAdmissionDate);
//			new end
			
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			studentRenewModel.setUserId(appUserRoleModel);
			
			Integer studentRenewId = studentStandardRepository.saveAndFlush(studentRenewModel).getRenewStudentId();
			
			//insert into monthly catlog table
			AppUserModel appUserRole = new AppUserModel();
			appUserRole.setAppUserRoleId(userId);
			
			SchoolMasterModel  schoolMasterModel =new SchoolMasterModel();
			schoolMasterModel.setSchoolid(studentList.getSchoolId());
			
			StudentStandardRenewModel studentStandardRenewModel=new StudentStandardRenewModel();
			studentStandardRenewModel.setRenewStudentId(studentRenewId);
			
			AttendanceMonthCatlogModel attendanceMonthCatalogModel=new AttendanceMonthCatlogModel();
			attendanceMonthCatalogModel.setStudentMasterModel(studentMasterModel);
			attendanceMonthCatalogModel.setStudentStandardRenewModel(studentStandardRenewModel);
			attendanceMonthCatalogModel.setMonthId(Integer.valueOf(monthId));
			attendanceMonthCatalogModel.setYearMasterModel(yearMasterModel);
			attendanceMonthCatalogModel.setStandardMasterModel(standrdMasterModel);
			attendanceMonthCatalogModel.setSchoolMasterModel(schoolMasterModel);
			attendanceMonthCatalogModel.setDivisionMasterModel(divisionMasterModel);
			attendanceMonthCatalogModel.setRollNo(Integer.valueOf(studentList.getRollNo()));
			
			String incrementedId = ""+attendanceMonthCatlogRepository.getMaxId(studentList.getSchoolId());
			String attendanceId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			attendanceMonthCatalogModel.setAttandenceMonthCatalogId(Integer.parseInt(attendanceId));
			
			
			attendanceMonthCatlogRepository.save(attendanceMonthCatalogModel);


		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
}
