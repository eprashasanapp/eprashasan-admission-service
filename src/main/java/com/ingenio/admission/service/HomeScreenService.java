package com.ingenio.admission.service;

import java.util.List;

import com.ingenio.admission.bean.HomeScreenBean;

public interface HomeScreenService {

	List<HomeScreenBean> getHomeScreenDetails(Integer staffStudentId, String profileRole, Integer yearId, Integer schoolId, String language);

}