package com.ingenio.admission.service;

import java.util.List;

import com.ingenio.admission.bean.FeeBean;
import com.ingenio.admission.bean.FeeIdBean;
import com.ingenio.admission.bean.FeePayTypeBean;
import com.ingenio.admission.bean.FeePayementGatewayBean;
import com.ingenio.admission.bean.FeeProspectBean;
import com.ingenio.admission.bean.OrderBean;
import com.ingenio.admission.bean.PaymentRequestBean;
import com.ingenio.admission.bean.PaymentSettingBean;
import com.ingenio.admission.bean.SavePaidBean;
import com.ingenio.admission.bean.SchoolDetailBean;
import com.ingenio.admission.bean.SessionResponseBean;
import com.ingenio.admission.bean.UserDetailsBean;

public interface PaymentGatewayService {
	
	List<FeePayementGatewayBean> getPaymentGatewayReport(Integer renewStudentId, Integer schoolId, Integer standardId, String date);

	List<FeeBean> saveTemporaryFees(PaymentRequestBean paymentRequestBean);

	List<FeePayTypeBean> getPayTypeList(Integer schoolId, Double amount);

	List<PaymentRequestBean> getTransactions(Integer studentFeeId, String language);

	List<PaymentRequestBean> savePaidFees(SavePaidBean savePaidBean);

	List<OrderBean> getOrders(String orderId);

	Integer updatePaymentStatus(Integer schoolId, String processId, String statusCode, String status,
			String statusMessage);

	List<FeeIdBean> getStudentFeeId(Integer renewStudentId);

	Double getFeesForYearAndStandard(Integer schoolId, String standardId, String yearId);

	void setStudentFee(String yearId, String standardId, String schoolId, Integer studentId, Integer userId);
	
	List<SchoolDetailBean> getSchoolDetails(String schoolId);

	UserDetailsBean getStudentDetails(String studentID);

	List<SessionResponseBean> getSessionList(String razorpay_payment_id, String razorpay_order_id);

	void sendSuccessMessage( String renewId  ,String mobileNumber, String schoolkey, String schoolName, String studentName);

	void sendFailureMessage(String renewId  ,String mobileNumber, String schoolkey, String schoolName, String studentName);

	void updateStatus(String attribute);

	Boolean getFeesSettingFlag(int parseInt, int parseInt2);

	List<PaymentSettingBean> getPaymentSetting(String schoolId, String yearId, String standardId);

	void sendSuccessmail(String razorpay_payment_id, String renewId, UserDetailsBean userDetailsBean, String schoolkey,
			String schoolName);

	FeeProspectBean getProspectFee(Integer schoolId, Integer yearId, Integer standardId);

	List<FeeBean> saveTempForProspectFee(PaymentRequestBean paymentRequestBean);

	FeeProspectBean getRegistrationSetting(Integer schoolId);

}
