package com.ingenio.admission.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ingenio.admission.bean.AndroidMenuBean;
import com.ingenio.admission.bean.AppVersionForReactBean;
import com.ingenio.admission.bean.CurrentApkVersionBean;
import com.ingenio.admission.bean.StudentCountBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.UserDetailsBean;

public interface UserRoleService {


	public UserDetailsBean getStudentDetails(Integer userId, String deviceId, String iMEICode, String language, String mobNum, String deviceType);

	public String uploadProfilePhoto(MultipartFile file, String fileName, Integer schoolId, String regNo, Integer studentId, String role);

	public UserDetailsBean getUserDetails(Integer staffId, Integer schoolId);

	public UserDetailsBean getStaffDetails(Integer staffId, Integer schoolId);

	public List<StudentCountBean> getStudentStrengthForTeacher(Integer staffId, Integer schoolId, String profileRole, Integer yearId,
			String renewDate, String language);

	public List<StudentDetailsBean> getclassAndDivisionWiseStudentDetails(Integer standardId, Integer divisionId,
			Integer schoolId, String profileRole, Integer yearId, String renewDate);

	public Integer setShowHideAddButtton(Integer staffId, Integer schoolId);

	public Integer saveDeviceDetails(String userName, String deviceId, String IMEICode);

//	public UserDetailsBean getStudentDetails1(Integer userId, String deviceId, String iMEICode, String language,
//			String mobNum, String deviceType, Integer isReact, Integer schoolId);

	public String getCurrentApkVersion();

	public String getCurrentApkVersionForCalendar();

	//List<CurrentApkVersionBean> getCurrentApkVersionDetails(Integer schoolId, Integer userId);

	public List< AppVersionForReactBean> getCurrentApkVersionDetailsForReact(Integer schoolId, Integer userId, String isReact, String appType);

	public UserDetailsBean getStudentDetails1(Integer userId, String deviceId, String iMEICode, String language,
			String mobNum, String deviceType, Integer isReact, Integer schoolId, Integer priorityType, 
			Integer typeOfManagementId, Integer menuPolicy, Integer specificMenuCategory, Integer sansthaKey);



	



	


}
