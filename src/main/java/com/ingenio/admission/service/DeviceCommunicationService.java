package com.ingenio.admission.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.ingenio.admission.bean.DeviceCommunicationBean;

public interface DeviceCommunicationService {

//	DeviceCommunicationBean saveAttendanceRFIData(String rFiString);	

	DeviceCommunicationBean saveAttendanceRFIData(String rFiString, String latitude, String longitude, String ipAddress,
			String macAddress, String randomCode, String imei);
	
	boolean saveStaffAttendance(DeviceCommunicationBean list1);

	DeviceCommunicationBean sendSMSForAbsentStudent() throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, IOException;

	String rfidPunchToStaffAttendance(Integer schoolId, String fromDate, String toDate);

//	String rfidPunchToStaffAttendance(Integer schoolId, String date);
	
}
