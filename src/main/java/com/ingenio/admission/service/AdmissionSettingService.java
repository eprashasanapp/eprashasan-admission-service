package com.ingenio.admission.service;

import java.util.List;

import com.ingenio.admission.bean.AdmissionSettingBean;

public interface AdmissionSettingService {

	public List<AdmissionSettingBean> getTabsName(Integer schoolId);

	public Integer updateTabsName(String tabName,Integer schoolId, Integer tabId);

	public List<AdmissionSettingBean> getAllFeilds(Integer tabId, Integer schoolId, Integer legendId);

	public Integer saveLegendName(String legendName, Integer schoolId, Integer tabId);

	public List<AdmissionSettingBean> getlegendName(Integer tabId, Integer schoolId);

	public Integer deleteLegendName(Integer tabId, Integer legendId, Integer schoolId);

	public Integer updateField(AdmissionSettingBean admissionSettingBean);

	public List<AdmissionSettingBean> getOldDynamicFields(Integer schoolId);
	
	public Integer updateLegendName(String legendName, Integer schoolId, Integer legendId);

	public Integer saveOldFieldValueName(Integer tabId, Integer schoolId, Integer oldFieldType,String oldfieldName);

	public List<AdmissionSettingBean> getDynamicFieldComboValue(Integer dynamicField, Integer schoolId);

	public List<AdmissionSettingBean> getOldDynamicFieldId(Integer tabId, Integer schoolId);

	public List<AdmissionSettingBean> setOldDynamicFieldValue(Integer oldDynamicFieldId, Integer schoolId,String fieldTypeId);

	public List<AdmissionSettingBean> getOldDynamicFieldValueId(String fieldName, Integer schoolId);

	public Integer updateTabsDisplayOrNotFlag(Integer schoolId, Integer tabId, String isTabDisplayOrNot);

	public List<AdmissionSettingBean> checkStaticFieldIsDisplayOrNot(String staticFieldName, Integer schoolId);

	public void updateisYearlyField(String[] isYearlyDynamicFieldVal, Integer schoolId);

}
