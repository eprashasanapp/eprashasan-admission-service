package com.ingenio.admission.util;

import java.io.UnsupportedEncodingException;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.google.common.base.Strings;

@Component
public class Utillity {
	
	@Autowired
	AmazonS3 amazonS3 ;
	
	
	public static String generatePrimaryKey(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
		
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				newGenetatedKey=globalDbSchoolKey+("0000000000".substring(0, ("0000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
				System.out.println(newGenetatedKey);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
	
	public static String generatePrimaryKey1(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				return globalDbSchoolKey+("000000000000000".substring(0, ("000000000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
	
	public static String convertNumberToWord(String number) {
		String number1="";
		if(number!=null && number.length()>=5) {
			Double numberDouble =Double.parseDouble(number);
			number = "" + numberDouble.intValue(); 
			if(number.length()==5) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("K");
			}
			if(number.length()==6) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("L");
			}
			if(number.length()==7) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("L");
			}
				if(number.length()==8) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("Cr");
			}
			if(number.length()==9) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("Cr");
			}
			
			if(number1=="") {
				return number;
			}
			return number1;
		}else {
			return number;
		}
	}
	
	public static String convertEnglishToOther(String num,String language) {
		String newStr = new String();
		if (!Strings.isNullOrEmpty(num)) {
			if (language.equals("1")) {
				return num;
			}
			if (!language.equals("1")) {
				if (language.equals("2")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							case '-':
								newStr += "-";
								break;
								
							case '.':
								newStr += ".";
								break;
							// English to Marathi
							case '1':
								newStr += "१";
								break;
							
							case '2':
								newStr += "२";
								break;
							
							case '3':
								newStr += "३";
								break;
							
							case '4':
								newStr += "४";
								break;
							
							case '5':
								newStr += "५";
								break;
							
							case '6':
								newStr += "६";
								break;
							
							case '7':
								newStr += "७";
								break;
							
							case '8':
								newStr += "८";
								break;
							
							case '9':
								newStr += "९";
								break;
							
							case '0':
								newStr += "०";
								break;
							
							case '१':
								newStr += "१";
								break;
							
							case '२':
								newStr += "२";
								break;
							
							case '३':
								newStr += "३";
								break;
							
							case '४':
								newStr += "४";
								break;
							
							case '५':
								newStr += "५";
								break;
							
							case '६':
								newStr += "६";
								break;
							
							case '७':
								newStr += "७";
								break;
							
							case '८':
								newStr += "८";
								break;
							case '९':
								newStr += "९";
								break;
							
							case '०':
								newStr += "०";
								break;
						}
					}
				}
				
				if (language.equals("3")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Hindi
							case '1':
								newStr += "१";
								break;
							case '2':
								newStr += "२";
								break;
							case '3':
								newStr += "३";
								break;
							case '4':
								newStr += "४";
								break;
							case '5':
								newStr += "५";
								break;
							case '6':
								newStr += "६";
								break;
							case '7':
								newStr += "७";
								break;
							case '8':
								newStr += "८";
								break;
							case '9':
								newStr += "९";
								break;
							case '0':
								newStr += "०";
								break;
							case '१':
								newStr += "१";
								break;
							case '२':
								newStr += "२";
								break;
							case '३':
								newStr += "३";
								break;
							case '४':
								newStr += "४";
								break;
							case '५':
								newStr += "५";
								break;
							case '६':
								newStr += "६";
								break;
							case '७':
								newStr += "७";
								break;
							case '८':
								newStr += "८";
								break;
							case '९':
								newStr += "९";
								break;
							case '०':
								newStr += "०";
								break;
						}
					}
				}
				
				if (language.equals("4")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Gujrati
							case '1':
								newStr += "૧";
								break;
							
							case '2':
								newStr += "૨";
								break;
							
							case '3':
								newStr += "૩";
								break;
							
							case '4':
								newStr += "૪";
								break;
							
							case '5':
								newStr += "૫";
								break;
							
							case '6':
								newStr += "૬";
								break;
							
							case '7':
								newStr += "૭";
								break;
							
							case '8':
								newStr += "૮";
								break;
							
							case '9':
								newStr += "૯";
								break;
							
							case '0':
								newStr += "૦";
								break;
							
							case '૧':
								newStr += "૧";
								break;
							
							case '૨':
								newStr += "૨";
								break;
							
							case '૩':
								newStr += "૩";
								break;
							
							case '૪':
								newStr += "૪";
								break;
							
							case '૫':
								newStr += "૫";
								break;
							
							case '૬':
								newStr += "૬";
								break;
							
							case '૭':
								newStr += "૭";
								break;
							
							case '૮':
								newStr += "૮";
								break;
							case '૯':
								newStr += "૯";
								break;
							
							case '૦':
								newStr += "૦";
								break;
						}
					}
				}
			}
		}else {
			return num;
		}
		return newStr;
	}
	
	
	// Convert Global Language Number to English Number (Ex. input= १२३४, output=1234)
		public static String convertOtherToEnglish(String num) {
			String newStr = new String();
			if (StringUtils.isNotEmpty(num) && !num.equals("null")) {
				for (int i = 0; i < num.length(); i++ ) {
					char str = num.charAt(i);
					
					switch (str) {
						case '-':
							newStr += "-";
							break;
							
						case '.':
							newStr += ".";
							break;
						
						// Start English
						case '1':
							newStr += "1";
							break;
						
						case '2':
							newStr += "2";
							break;
						
						case '3':
							newStr += "3";
							break;
						
						case '4':
							newStr += "4";
							break;
						
						case '5':
							newStr += "5";
							break;
						
						case '6':
							newStr += "6";
							break;
						
						case '7':
							newStr += "7";
							break;
						
						case '8':
							newStr += "8";
							break;
						
						case '9':
							newStr += "9";
							break;
						
						case '0':
							newStr += "0";
							break;
						
						// Start Marathi to English
						case '१':
							newStr += "1";
							break;
						
						case '२':
							newStr += "2";
							break;
						
						case '३':
							newStr += "3";
							break;
						
						case '४':
							newStr += "4";
							break;
						
						case '५':
							newStr += "5";
							break;
						
						case '६':
							newStr += "6";
							break;
						
						case '७':
							newStr += "7";
							break;
						
						case '८':
							newStr += "8";
							break;
						
						case '९':
							newStr += "9";
							break;
						
						case '०':
							newStr += "0";
							break;
						
						// Start Gujrati to English
						case '૧':
							newStr += "1";
							break;
						
						case '૨':
							newStr += "2";
							break;
						
						case '૩':
							newStr += "3";
							break;
						
						case '૪':
							newStr += "4";
							break;
						
						case '૫':
							newStr += "5";
							break;
						
						case '૬':
							newStr += "6";
							break;
						
						case '૭':
							newStr += "7";
							break;
						
						case '૮':
							newStr += "8";
							break;
						
						case '૯':
							newStr += "9";
							break;
						
						case '૦':
							newStr += "0";
							break;
					}
				}
			}else {
				return "";
			}
			return newStr;
		}
		
		public static String convertEachWordInUppercase(String word){
			word=word.trim();
			String[] words = word.split(" ");
			String newSentence = "";
				for (int j = 0; j < words.length; j++){
			    	newSentence = newSentence+" "+words[j].substring(0,1).toUpperCase() + words[j].substring(1).toLowerCase();
			}
			return newSentence;
		}
		
		public static String convertInUTFFormat(String str) {
			if (StringUtils.isNotEmpty(str)&& !str.equals("null")) {
				try {
					str = new String(str.getBytes("iso-8859-1"), "UTF-8");
					return str;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			return str;
		}
		
		public String getS3PreSignedUrl(String imagePath) {
			String[] arrayStr = imagePath.split(".com/");
			try {
				java.util.Date expiration = new java.util.Date();
				long expTimeMillis = expiration.getTime();
				expTimeMillis += 1000 * 60 * 10;
				expiration.setTime(expTimeMillis);
//				String bucketName = "eprashasan1";
				String bucketName = "eprashasan2";
				GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName,
						arrayStr[1]).withMethod(HttpMethod.GET).withExpiration(expiration);
				URL url = amazonS3.generatePresignedUrl(generatePresignedUrlRequest);
				System.out.println(url);
				return url.toString();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

		}

		public static String calculateNextMonth(Integer monthId) {
			// TODO Auto-generated method stub
			String nextMonthId;
			if(monthId == 6) {
				nextMonthId="7,8,9,10,11,12,1,2,3,4,5";
			}else if(monthId == 7) {
				nextMonthId="8,9,10,11,12,1,2,3,4,5";
			}else if(monthId == 8) {
				nextMonthId="9,10,11,12,1,2,3,4,5";
			}else if(monthId == 9) {
				nextMonthId="10,11,12,1,2,3,4,5";
			}else if(monthId == 10) {
				nextMonthId="11,12,1,2,3,4,5";
			}else if(monthId == 11) {
				nextMonthId="12,1,2,3,4,5";
			}else if(monthId == 12) {
				nextMonthId="1,2,3,4,5";
			}else if(monthId == 1) {
				nextMonthId="2,3,4,5";
			}else if(monthId == 2) {
				nextMonthId="3,4,5";
			}else if(monthId == 3) {
				nextMonthId="4,5";
			}else if(monthId == 4) {
				nextMonthId="5";
			}else {
				nextMonthId="0";
			}
			return nextMonthId;
		}
}
