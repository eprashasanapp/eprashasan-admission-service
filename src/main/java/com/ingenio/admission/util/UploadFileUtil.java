package com.ingenio.admission.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.annotation.PostConstruct;

import org.springframework.web.multipart.MultipartFile;

public class UploadFileUtil {

	@PostConstruct
	public static String uploadImage(MultipartFile file, String filename, String folderName, String schoolKey,
			Integer language) {
		try {
			byte[] bytes = file.getBytes();

			String exepath="C:/ePrashasan/MainDB/";
			File dir = new File(exepath +folderName);
			if (!dir.exists()) {
				dir.mkdirs();
			}				
			String postUrl="http://115.124.111.61:4747/ePrashasan/" ;				
			File serverFile = new File(dir.getAbsolutePath()+File.separator+filename);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
			return postUrl+folderName+File.separator+filename;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
