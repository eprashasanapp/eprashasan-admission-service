package com.ingenio.admission.util;

import java.text.DateFormatSymbols;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateToWords {

	String string;
	String st1[] = { "", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh",
			"Eighth", "Ninth" };
	String st2[] = { "Hundred", "Thousand" };
	String st3[] = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
			"Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", };
	String st4[] = { "Twenty", "Thirty", "Fourty", "Fity", "Sixty", "Seventy",
			"Eighty", "Ninety" };

	String year[] = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
			"fifteen", "sixteen", "seventeen", "Eighteen", "nineteen",
			"Two Thosound", "Three Thousand", "Four Thousand", "Five Thousand",
			"Six Thousand", "Seven Thousand" };
	String strForLastDigitOfYear[] = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven",
			"Eight", "Nine" };
	public String getMonth(int month) {
		return new DateFormatSymbols().getMonths()[month - 1];
	}

	public String convert(int number) {
		int n = 1;
		int word;
		string = "";
		while (number != 0) {
			switch (n) {
			case 1:
				word = number % 100;
				pass(word);
				if (number > 100 && number % 100 != 0) {
					show("and ");
				}
				number /= 100;
				break;

			case 2:
				word = number % 10;
				if (word != 0) {
					show(" ");
					show(st2[0]);
					show(" ");
					pass(word);
				}
				number /= 10;
				break;

			case 3:
				word = number % 100;
				if (word != 0) {
					show(" ");
					show(st2[1]);
					show(" ");
					pass(word);
				}
				number /= 100;
				break;

			case 4:
				word = number % 100;
				if (word != 0) {
					show(" ");
					show(st2[2]);
					show(" ");
					pass(word);
				}
				number /= 100;
				break;

			case 5:
				word = number % 100;
				if (word != 0) {
					show(" ");
					show(st2[3]);
					show(" ");
					pass(word);
				}
				number /= 100;
				break;

			}
			n++;
		}
		return string;
	}
public String convertLastDigitofYear(int number)
{int n = 1;
int word;
string = "";
while (number != 0) {
	switch (n) {
	case 1:
		word = number % 100;
		passforLastDigitOfYear(word);
		if (number > 100 && number % 100 != 0) {
			show("and ");
		}
		number /= 100;
		break;

	case 2:
		word = number % 10;
		if (word != 0) {
			show(" ");
			show(st2[0]);
			show(" ");
			passforLastDigitOfYear(word);
		}
		number /= 10;
		break;

	case 3:
		word = number % 100;
		if (word != 0) {
			show(" ");
			show(st2[1]);
			show(" ");
			passforLastDigitOfYear(word);
		}
		number /= 100;
		break;

	case 4:
		word = number % 100;
		if (word != 0) {
			show(" ");
			show(st2[2]);
			show(" ");
			passforLastDigitOfYear(word);
		}
		number /= 100;
		break;

	case 5:
		word = number % 100;
		if (word != 0) {
			show(" ");
			show(st2[3]);
			show(" ");
			passforLastDigitOfYear(word);
		}
		number /= 100;
		break;

	}
	n++;
}
return string;
	}

public void passforLastDigitOfYear(int number) {
	int word, q;
	if (number < 10) {
		show(strForLastDigitOfYear[number]);
	}
	if (number > 9 && number < 20) {
		show(st3[number - 10]);
	}
	if (number > 19) {
		word = number % 10;
		if (word == 0) {

			q = number / 10;
			//
			show(st4[q - 2]);
		} else {
			q = number / 10;
			show(strForLastDigitOfYear[word]);
			show(" ");
			show(st4[q - 2]);
		}
	}
}
	public void pass(int number) {
		int word, q;
		if (number < 10) {
			show(st1[number]);
		}
		if (number > 9 && number < 20) {
			show(st3[number - 10]);
		}
		if (number > 19) {
			word = number % 10;
			if (word == 0) {

				q = number / 10;
			//	
				show(st4[q - 2]);
			} else {
				q = number / 10;
				show(st1[word]);
				show(" ");
				show(st4[q - 2]);
			}
		}
	}

	public void show(String s) {
	//	
		String st;
		st = string;
		string = s;
		string += st;
	}
	
	public String Convert1(int yearint) {
		int firsttwodigit = yearint / 100;
		String yearInWord = "";
		if (firsttwodigit == 18)
			yearInWord = "Eighteen";
		if (firsttwodigit == 19)
			yearInWord = "Ninteen";
		if (firsttwodigit == 20)
			yearInWord = "Two Thousand";
		if (firsttwodigit == 30)
			yearInWord = "Three Thousand";
		if (firsttwodigit == 40)
			yearInWord = "Four Thousand";
		if (firsttwodigit == 50)
			yearInWord = "Five Thousand";
		int secondtwodigit = yearint % 100;

		yearInWord = yearInWord + " " + convertLastDigitofYear(secondtwodigit);

		return yearInWord;
	}

	public String converttowordfull(String date) {
		try
		{
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormatter.parse(date, new ParsePosition(0)));
		int day = cal.get(Calendar.DATE);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		DateToWords num = new DateToWords();
		return num.convert(day) + " " + num.getMonth(month) + " "
				+ num.Convert1(year);
		}catch(Exception e)
		{
			e.printStackTrace();
			return " ";
		}
	}

	public static void main(String[] args) {
		DateToWords num = new DateToWords();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormatter.parse("26/5/2097", new ParsePosition(0)));
		int day = cal.get(Calendar.DATE);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);		
	}

	public String formatdate(String date) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateformaternew=new SimpleDateFormat("dd-MM-yyyy");
		String stringDate="";
		Date date1 = null;
		try {
			date1 = dateFormatter.parse(date);
			stringDate=dateformaternew.format(date1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stringDate;
	}
}
