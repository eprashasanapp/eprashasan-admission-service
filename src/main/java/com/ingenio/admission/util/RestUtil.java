package com.ingenio.admission.util;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.ingenio.admission.bean.FeeBean;
import com.ingenio.admission.bean.PaymentResponseBean;
import com.ingenio.admission.bean.PaymentResponseList;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.bean.StudentAdmissionBean;

public class RestUtil {
	
	public static <T> Object makeRestCall(String url) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			FeeBean responseBean = restTemplate.getForObject(url,FeeBean.class);
			return responseBean;
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public static <T> Object makeRestCallForExistingUser(String url) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseBodyBean<T> responseBean = restTemplate.getForObject(url,ResponseBodyBean.class);
			return responseBean;
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static <T> Object makeRestCallForRoutingTable(String url,StudentAdmissionBean studentAdmissionBean) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			Integer responseBean = restTemplate.postForObject(url,studentAdmissionBean, Integer.class);
			return responseBean;
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public static <T> Object makeRestCall1(String url) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.getForObject(url,PaymentResponseBean.class);
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static <T> Object makeCommonRestCall(String url) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.getForObject(url,PaymentResponseList.class);
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static <T> Object getCommissionList(String url) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.getForObject(url,PaymentResponseList.class);
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static <T> Object getPaymentList(String url) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.getForObject(url,PaymentResponseList.class);
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return null;
		
	}
   
}
