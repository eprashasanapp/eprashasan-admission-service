package com.ingenio.admission.util;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;

public class UTFConversionUtil {
	
	// Convert String parameter to UTF-8
	public static String convertInUTFFormat(String str) {
		if (StringUtils.isNotEmpty(str)&& !str.equals("null")) {
			try {
				str = new String(str.getBytes("iso-8859-1"), "UTF-8");
				return str;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	public static String generatePrimaryKey(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				newGenetatedKey=globalDbSchoolKey+("0000000000".substring(0, ("0000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
	
	
	public static String generatePrimaryKey1(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				return globalDbSchoolKey+("000000000000000".substring(0, ("000000000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
}
