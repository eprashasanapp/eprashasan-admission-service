package com.ingenio.admission.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.admission.config.ConfigurationProperties;

public class HttpClientUtil {

	@PostConstruct     
	public static String uploadImage(MultipartFile file,String filename,String folderName,String schoolKey) {
		try {
			byte[] bytes = file.getBytes();
			//String exepath = getExePath();
			String exepath="C:/ePrashasan/MainDB/";
			File dir = new File(exepath +folderName);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			
			//ResourceBundle resourceBundle = ResourceBundle.getBundle("CONNECTOR");
			//String postUrl=resourceBundle.getString("image.postUrl");
			String postUrl=ConfigurationProperties.postUrl;
			File serverFile = new File(dir.getAbsolutePath()+File.separator+filename);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
			return postUrl+folderName+File.separator+filename;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		/*HttpResponse response;
		StringBuffer result = new StringBuffer();
		String line = "";
		 try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(ConfigurationProperties.uploadImageUrl);
		//	post.setHeader("Content-type", "application/json");
		
			File convFile = new File(file.getOriginalFilename());
		    convFile.createNewFile();
		    FileOutputStream fos = new FileOutputStream(convFile);
		    fos.write(file.getBytes());
		    fos.close();
		    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		    StringBody fileNameString = new StringBody(filename, ContentType.MULTIPART_FORM_DATA);
		    StringBody folderNameString = new StringBody(folderName, ContentType.MULTIPART_FORM_DATA);
		    StringBody schoolkeyString = new StringBody(schoolKey, ContentType.MULTIPART_FORM_DATA);
		    builder.addPart("file", new FileBody(convFile));
		    builder.addPart("filename", fileNameString);
		    builder.addPart("folderName", folderNameString);
		    builder.addPart("schoolkey", schoolkeyString);
			post.setEntity(builder.build());
			
			response = client.execute(post);
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
			
			BufferedReader rd = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));
	
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
		
			//HttpEntity entity = response.getEntity();
			//Header encodingHeader = entity.getContentEncoding();
	
			//Charset encoding = encodingHeader == null ? StandardCharsets.UTF_8 : 
			//Charsets.toCharset(encodingHeader.getValue());
	
			// use org.apache.http.util.EntityUtils to read json as string
			//String json = EntityUtils.toString(entity, StandardCharsets.UTF_8);
	
			//JSONObject o = new JSONObject(json); 
			//convFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();*/
	}
	
	/*public static String getExePathDB() {
		HttpResponse response;
		StringBuffer result = new StringBuffer();
		String path=null;
		String line = "";
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(ConfigurationProperties.getExePathDB);
			response= client.execute(post);
			path=response.getEntity().getContent().toString();
			BufferedReader rd = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));
	
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}*/

}
