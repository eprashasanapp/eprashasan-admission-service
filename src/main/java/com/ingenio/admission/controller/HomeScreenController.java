package com.ingenio.admission.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.admission.bean.HomeScreenBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.service.HomeScreenService;

@RestController
@CrossOrigin
public class HomeScreenController {

	@Autowired 
	private HomeScreenService homeScreenService;
	
	@GetMapping(value="/homescreen")
	@Produces("application/json")
	 ResponseBodyBean<HomeScreenBean> getStaffAssigndSubjects(@RequestParam("staffStudentId") Integer staffStudentId,
			@RequestParam("profileRole") String profileRole,@RequestParam("yearId") Integer yearId,@RequestParam("schoolId") Integer schoolId,@RequestParam("language") String language){
		List<HomeScreenBean> staffSubjectList = new ArrayList<HomeScreenBean>();
		try {
			staffSubjectList=homeScreenService.getHomeScreenDetails(staffStudentId,profileRole,yearId,schoolId,language);
			if(CollectionUtils.isNotEmpty(staffSubjectList)) {
				return new ResponseBodyBean<HomeScreenBean>("200",HttpStatus.OK,staffSubjectList);
			}
			else {
				return new ResponseBodyBean<HomeScreenBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<HomeScreenBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/testingAdmission")
	  public String testing() {
		  return "new changes checking ci cd";
	  }
}


