package com.ingenio.admission.controller;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.CommonUtilityBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.bean.SearchTextBean;
import com.ingenio.admission.bean.StandardYearBean;
import com.ingenio.admission.bean.StudentAdmissionBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.StudentDynamicDetailsBean;
import com.ingenio.admission.bean.SubjectSelectionBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StudentMasterModel;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.service.CommonUtilityService;
import com.ingenio.admission.service.PaymentGatewayService;
import com.ingenio.admission.service.StudentAdmissionService;
import com.ingenio.admission.service.UserAuthenticationService;
import com.ingenio.admission.util.CryptographyUtil;
import com.spire.xls.Workbook;
import com.spire.xls.Worksheet;

//@Controller
@RestController
@CrossOrigin
public class StudentAdmissionController {
	
	@Autowired 
	StudentAdmissionService studentAdmissionService;
	
	@Autowired
	CommonUtilityService commonUtilityService;
	
	@Autowired
	PaymentGatewayService paymentGatewayService;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository ;
	
	@Autowired
	UserAuthenticationService userAuthenticationService;
	
	CryptographyUtil cry= new CryptographyUtil();
	
	 @Autowired
	 private Environment env;
	
	@GetMapping(value = "getTabsAndLegendsSetting")
	@ResponseBody
	public List<AdmissionSettingBean> getTabsAndLegendsSetting(@RequestParam("schoolId") Integer schoolId) {
		List<AdmissionSettingBean> tabsAndLegenedList = new ArrayList<>();
		try {
			tabsAndLegenedList=studentAdmissionService.getTabsAndLegendsSetting(schoolId);
			return tabsAndLegenedList;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tabsAndLegenedList;
	}
	
	@PostMapping(value = "saveStudentDetails")
	@ResponseBody
	public ResponseBodyBean<String> saveStudentDetails(@ModelAttribute("studentAdmissionBean") StudentAdmissionBean studentAdmissionBean,
			@RequestParam("file") List<MultipartFile> file, @RequestParam("fileName") List<String> filename,RedirectAttributes redirectAttributes,HttpSession session) {
		String regNo = "";
		try {
			StudentMasterModel studentMasterModel = studentAdmissionService.saveStudentDetails(studentAdmissionBean,file,filename,session,1);
			regNo = studentMasterModel.getStudentRegNo();
			session.setAttribute("studentId", studentMasterModel.getStudentId());			
			return new ResponseBodyBean<String>("200",HttpStatus.OK,regNo);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
		}
		
	}
	
	@PostMapping(value = "saveStudentDetailsImg")
	@ResponseBody
	public ResponseBodyBean<String> saveStudentDetailsImg(@ModelAttribute("studentAdmissionBean") StudentAdmissionBean studentAdmissionBean,
			@RequestParam("file") List<MultipartFile> file, @RequestParam("fileName") List<String> filename,RedirectAttributes redirectAttributes,HttpSession session) {
		String regNo = "";
		try {
			StudentMasterModel studentMasterModel = studentAdmissionService.saveStudentDetails(studentAdmissionBean,file,filename,session,2);
			regNo = studentMasterModel.getStudentRegNo();
			session.setAttribute("studentId", studentMasterModel.getStudentId());
			return new ResponseBodyBean<String>("200",HttpStatus.OK,regNo);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
		}
		
	}
	
	@GetMapping(value = "getValueToPopulateForDynamicField")
	@ResponseBody
	public List<AdmissionSettingBean> getValueToPopulateForDynamicField(@RequestParam("schoolId") Integer schoolId,@RequestParam("fieldId") Integer fieldId) {
		List<AdmissionSettingBean> dynamicValueList = new ArrayList<>();
		try {
			dynamicValueList = studentAdmissionService.getValueToPopulateForDynamicField(schoolId,fieldId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dynamicValueList;
	}
	 
	@GetMapping(value = "getValueToPopulateForDynamic2Field")
	@ResponseBody
	public List<AdmissionSettingBean> getValueToPopulateForDynamic2Field(@RequestParam("schoolId") Integer schoolId,@RequestParam("fieldName") String fieldName) {
		List<AdmissionSettingBean> dynamicValueList = new ArrayList<>();
		try {
			dynamicValueList = studentAdmissionService.getValueToPopulateForDynamic2Field(schoolId,fieldName);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dynamicValueList;
	}
	
	@GetMapping(value = "getStudentDetailsForEdit")
	@ResponseBody
	public ResponseBodyBean<StudentAdmissionBean> getStudentDetailsForEdit(HttpSession session,@RequestParam("studentId") Integer studentId) {
		try {
			List<StudentAdmissionBean> studentList=studentAdmissionService.getStudentDetailsForEdit(studentId);
			if(CollectionUtils.isNotEmpty(studentList)) {
				return new ResponseBodyBean<StudentAdmissionBean>("200",HttpStatus.OK,studentList);
			}
			else {
				return new ResponseBodyBean<StudentAdmissionBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentAdmissionBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value = "getStudentAssignedSubjectsForEdit")
	@ResponseBody
	public List<StudentDynamicDetailsBean> getStudentAssignedSubjectsForEdit(@RequestParam("studentId") String studentId, HttpSession session) {
		List<StudentDynamicDetailsBean> subjectList = new ArrayList<>();
		try {
			subjectList = studentAdmissionService.getStudentAssignedSubjectsForEdit(studentId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subjectList;
	}
	
	@GetMapping(value = "getAttachmentsForEdit")
	@ResponseBody
	public List<StudentDynamicDetailsBean> getAttachmentsForEdit(@RequestParam("studentId") Integer studentId, HttpSession session) {
		List<StudentDynamicDetailsBean> subjectList = new ArrayList<>();
		try {
			subjectList = studentAdmissionService.getAttachmentsForEdit(studentId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subjectList;
	}
	
	@GetMapping(value = "getAttachmentsForEditWithImages")
	@ResponseBody
	public List<StudentDynamicDetailsBean> getAttachmentsForEditWithImages(@RequestParam("studentId") Integer studentId, HttpSession session) {
		List<StudentDynamicDetailsBean> subjectList = new ArrayList<>();
		try {
			subjectList = studentAdmissionService.getAttachmentsForEditWithImages(studentId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subjectList;
	}
	
	
	
	
	@GetMapping(value = "getDynamicDetails2ForEdit")
	@ResponseBody
	public List<StudentDynamicDetailsBean> getDynamicDetails2ForEdit(@RequestParam("studentId") Integer studentId, HttpSession session) {
		List<StudentDynamicDetailsBean> subjectList = new ArrayList<>();
		try {
			String schoolId = (String) session.getAttribute("schoolId");
			subjectList = studentAdmissionService.getDynamicDetails2ForEdit(studentId,Integer.parseInt(schoolId));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subjectList;
	}
	
	@GetMapping(value = "getDynamicDetails1ForEdit")
	@ResponseBody
	public List<StudentDynamicDetailsBean> getDynamicDetails1ForEdit(@RequestParam("studentId") Integer studentId, HttpSession session) {
		List<StudentDynamicDetailsBean> subjectList = new ArrayList<>();
		try {
			subjectList = studentAdmissionService.getDynamicDetails1ForEdit(studentId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subjectList;
	}	
	
	@GetMapping(value = "getFormNumber")
	@ResponseBody
	public  ResponseBodyBean<String> getFormNumber(@RequestParam("schoolId") Integer schoolId, HttpSession session) {
		try {
			String formNumber = studentAdmissionService.getFormNumber(schoolId);
			formNumber = "F".concat(formNumber);
			return new ResponseBodyBean<String>("200",HttpStatus.OK,formNumber);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,"");
		}
	}


	@GetMapping(value = "getNewStudentFlag")
	public @ResponseBody String getAppLangage(HttpSession session) {
		return ""+session.getAttribute("newStudentString").toString();
	}
	
	/*@GetMapping(value = "downloadAttachment")
	public void downloadClassWiseGadget(HttpSession session, HttpServletResponse response,@RequestParam("fileName") String fileName,@RequestParam("filePath") String filePath) {
		try {	
			Path file = Paths.get(filePath);
	        if (Files.exists(file)) {
				response.setContentType("application/force-download");
				response.addHeader("Content-Disposition", "attachment; filename="+fileName);
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();   
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/	
	@GetMapping(value = "getPrint")
	public void getPrint(@RequestParam("studRegNo") String studRegNo,@RequestParam("schoolId") Integer schoolId,@RequestParam("yearComboVal") String yearComboVal,
			@RequestParam("std") String std,@RequestParam("standardText") String standardText,@RequestParam("AdmissionDateSpan") String AdmissionDateSpan,
			@RequestParam(name="schoolName",required=false) String schoolName,
			HttpSession session, HttpServletResponse response) {
		try {
			if(StringUtils.isEmpty(schoolName)) {
				schoolName = (String) session.getAttribute("schoolName");
			}
		
			String filePathName=studentAdmissionService.getAdmissionPrint(studRegNo,schoolId,yearComboVal,std,AdmissionDateSpan,schoolName,standardText,response);
			if(!"".equals(filePathName)) {
				String fileNameAndPath[] = filePathName.split("#"); 
				String fileFolderPath = fileNameAndPath[0];
				String fileName =fileNameAndPath[1];
				convertExcelToPDF(fileFolderPath,fileName,3,response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			//return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
		}
	}
	
	@GetMapping(value = "getPrintForDyanmicCertificate")
	public void getPrintForDyanmicCertificate(@RequestParam("studRegNumber") String studRegNo,@RequestParam("schoolId") Integer schoolId,@RequestParam("acadYearID") String yearComboVal,
			@RequestParam("standardId") String std,@RequestParam("standardName") String standardText,@RequestParam("globalCertificateId") String globalCertificateId,
			HttpSession session, HttpServletResponse response) {
		try {
			String schoolName="";
			if(StringUtils.isEmpty(schoolName)) {
				schoolName = (String) session.getAttribute("schoolName");
			}
		
			String filePathName=studentAdmissionService.getCertificatePrint(studRegNo,schoolId,yearComboVal,std,standardText,schoolName,globalCertificateId,response,session);
			String pathName[]=filePathName.split(",");
			for(int t=0;t<pathName.length;t++) {
				if(!"".equals(pathName[t])) {
					String fileNameAndPath[] = pathName[t].split("#"); 
					String fileFolderPath = fileNameAndPath[0];
					String fileName =fileNameAndPath[1];
					convertExcelToPDF(fileFolderPath,fileName,3,response);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			//return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
		}
	}
		
	
//	@GetMapping(value = "getreport")
//	public void getreport(@RequestParam("schoolId") Integer schoolId) {
//		try {			
//			studentAdmissionService.getreport(schoolId);			
//		} catch (Exception e) {
//			e.printStackTrace();
//			//return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
//		}
//		
//	}
	
	
	@GetMapping(value = "getStudentInformation")
	@ResponseBody
	public List<UserDetailsBean> getStudentInformation(@RequestParam("studRegNo") String studRegNo,@RequestParam("schoolId") Integer schoolId,@RequestParam("yearComboVal") String yearComboVal) {
		List<UserDetailsBean> studentList = new ArrayList<>();
		try {
			studentList=studentAdmissionService.getStudentInformation(studRegNo,schoolId,yearComboVal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return studentList;
	}
	
	
	
	@RequestMapping(value = "/getreport", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> citiesReport(@RequestParam("schoolId") Integer schoolId) throws IOException {

		

		ByteArrayInputStream bis = 	studentAdmissionService.getreport(schoolId);	

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}
	
	
	
	private void convertExcelToPDF(String excelPath,String excelName,int sheetNo,HttpServletResponse response) { 
		try {
			String pdfName=excelName.substring(0, excelName.lastIndexOf("."));
			 Workbook workbook = new Workbook();
	        workbook.loadFromFile(excelPath+"\\"+excelName);

	        //Fit to page
	        Worksheet worksheet = workbook.getWorksheets().get(2);

	        //Save as PDF document
	        worksheet.saveToPdf(excelPath+"\\"+pdfName+".pdf");
	        
	        File file = new File(excelPath+"\\"+pdfName+".pdf");
			
			if (file.exists()) {
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "inline; filename=\"" + pdfName + ".pdf\"");
				Path filePath = Paths.get(excelPath + "\\" + pdfName +".pdf");
				Files.copy(filePath, response.getOutputStream());
				response.getOutputStream().flush();
				

			}

		} catch(Exception e) {
		e.printStackTrace();
		
	}
 }
	
	/*@PostMapping(value = "getDownloadPrint")
	@ResponseBody
	public ResponseBodyBean<String> getDownloadPrint(@RequestParam("studRegNo") String studRegNo,@RequestParam("schoolId") Integer schoolId,@RequestParam("yearComboVal") String yearComboVal,
			@RequestParam("std") String std,@RequestParam("AdmissionDateSpan") String AdmissionDateSpan,HttpSession session, HttpServletResponse response) {
		String regNo = "";
		try {
			String schoolName = (String) session.getAttribute("schoolName");
			String s = studentAdmissionService.getDownloadPrint(studRegNo,schoolId,yearComboVal,std,AdmissionDateSpan,schoolName,response);
			return new ResponseBodyBean<String>("200",HttpStatus.OK,regNo);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
		}
		
	}*/
	
	@PostMapping(value = "saveNewRegistration")
	@ResponseBody
	public ResponseBodyBean<String> saveNewRegistration(@RequestParam("newRegName") String newRegName,@RequestParam("yearId") String yearId,@RequestParam("standardId") String standardId,
			@RequestParam("passwordNewReg") String passwordNewReg,@RequestParam("newRegUserName") String newRegUserName,@RequestParam("schoolId") String schoolId,@RequestParam("formNo") String formNo,
			@RequestParam("missMr") String missMr,@RequestParam("secondName") String secondName,@RequestParam("lastName") String lastName,@RequestParam("feeSettingAvailable") String feeSettingAvailable,
			RedirectAttributes redirectAttributes,HttpSession session) {
		String regNo = "";
		try {
			StudentMasterModel studentMasterModel = studentAdmissionService.saveNewRegistration(newRegName,yearId,standardId,passwordNewReg,newRegUserName,schoolId,formNo,missMr,secondName,lastName,session);
			System.out.println(feeSettingAvailable);
			if(feeSettingAvailable.equals("1")) {
				paymentGatewayService.setStudentFee(yearId,standardId,schoolId,studentMasterModel.getStudentId(),studentMasterModel.getUserId().getAppUserRoleId());
			}
			regNo = studentMasterModel.getStudentRegNo();
			session.setAttribute("studentId", studentMasterModel.getStudentId());
			session.setAttribute("welcomeName",newRegUserName);
			session.setAttribute("yearId", yearId);
			session.setAttribute("standardId", standardId);
			List<StudentAdmissionBean> studentList=new ArrayList<>();
			return new ResponseBodyBean<String>("200",HttpStatus.OK,String.valueOf(studentMasterModel.getStudentId()));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
		}		
	}
	
	
	@PostMapping(value = "calendarRegistration")
	@ResponseBody
	public ResponseBodyBean<String> saveNewRegistration(@RequestParam("newRegName") String firstName,
			@RequestParam("newRegUserName") String mobileNo,@RequestParam("schoolId") String schoolId,
			@RequestParam("secondName") String secondName,
			@RequestParam("lastName") String lastName,@RequestParam("standardId") Integer standardId1,
			@RequestParam("divisionId") Integer divisionId,@RequestParam("fcm_id") String fcm_id,
			RedirectAttributes redirectAttributes,HttpSession session) {
		String regNo = "";

		try {
			String yearId=schoolId;
			String standardId=""+standardId1;
			String formNo = null;
			String passwordNewReg="123456";
			StudentMasterModel studentMasterModel = studentAdmissionService.saveNewRegistrationForCalendar(firstName
					,yearId,standardId,passwordNewReg,mobileNo,schoolId,formNo,secondName,
					lastName,session,divisionId,fcm_id);
			if(studentMasterModel.getStudentId()!=0) {
				SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(Integer.valueOf(schoolId));
				Integer languageId = schoolModel.getLanguage().getLanguageId(); 
				int otp = userAuthenticationService.generateOTP(mobileNo);
				String message = "";
				System.out.println(otp);
				String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(studentAdmissionService.getSchoolName(Integer.valueOf(schoolId)).replaceAll("[\\r\\n]+", "")));
				if(languageId ==2) {
				message= env.getProperty("mr.otp1")+" "+otp+ " " +env.getProperty("mr.otp2")+" - "+schoolName+"";
				}else {
				 message = "" +otp+ " use this OTP to verify your mobile number for SGBAU calendar 2021 app. - "+schoolName+"";
				}
				System.out.println(message);
				userAuthenticationService.sendOtpMessage(mobileNo, message,Integer.valueOf(schoolId),languageId); //898
				userAuthenticationService.sendWhatsAppSMS(mobileNo,message,Integer.valueOf(schoolId),3);
				return new ResponseBodyBean<String>("200",HttpStatus.OK,""+otp);
			}else {
				return new ResponseBodyBean<String>("200",HttpStatus.OK,"0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,regNo);
		}		
	}
	
	
	@GetMapping(value = "getAcademicYear")
	@ResponseBody
	public ResponseBodyBean<String> getAcademicYear(@RequestParam("schoolId") String schoolId,RedirectAttributes redirectAttributes,HttpSession session) {
		String accademiYear = "";
		try {
		    accademiYear= commonUtilityService.getAcademicYear(schoolId);
			return new ResponseBodyBean<String>("200",HttpStatus.OK,accademiYear);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,accademiYear);
		}
		
	}
	
	

	@GetMapping(value = "getStandardandYear")
	@ResponseBody
	public ResponseBodyBean<StandardYearBean> getStandardYear(@RequestParam("studentId") String studentId,RedirectAttributes redirectAttributes,HttpSession session) {
		StandardYearBean standardYearBean = new StandardYearBean();
		List<StandardYearBean> commonUtilityBeanList = new ArrayList<>();
		try {
			 standardYearBean= commonUtilityService.getStandardYear(studentId);
			 commonUtilityBeanList.add(standardYearBean);
			return new ResponseBodyBean<StandardYearBean>("200",HttpStatus.OK,commonUtilityBeanList);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StandardYearBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		
	}
	
	
	
	
	@GetMapping(value = "getStudentImageSignature")
	@ResponseBody
	public ResponseBodyBean<CommonUtilityBean> getStudentImageSignature(@RequestParam("schoolId") Integer schoolId,@RequestParam("regNo") String regNo,@RequestParam("type") Integer type) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			commonUtilityBeanList=commonUtilityService.getStudentImageSignature(schoolId,regNo,type);
			if(CollectionUtils.isNotEmpty(commonUtilityBeanList)) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else
			{
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	@GetMapping(value = "getStudentAttachmentsforPrint")
	@ResponseBody
	public ResponseBodyBean<StudentAdmissionBean> getStudentAttachmentsforPrint(@RequestParam("studentId") String studentId) {
		List<StudentAdmissionBean> attachmentList = new ArrayList<>();
		try {
			attachmentList= studentAdmissionService.getStudentAttachmentsforPrint(studentId);
			return new ResponseBodyBean<StudentAdmissionBean>("200",HttpStatus.OK,attachmentList);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentAdmissionBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	

	@GetMapping(value = "getSubjectList")
	@ResponseBody
	public ResponseBodyBean<SubjectSelectionBean> getSubjectList(@RequestParam("schoolId") Integer schoolId,@RequestParam("yearId") Integer yearId,@RequestParam("standardId") Integer standardId,
			@RequestParam("studentId") Integer studentId, RedirectAttributes redirectAttributes,HttpSession session) {
		try {
			SubjectSelectionBean subjectSelectionBean= studentAdmissionService.getSubjectList(schoolId,yearId,standardId,studentId);
			return new ResponseBodyBean<SubjectSelectionBean>("200",HttpStatus.OK,subjectSelectionBean);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SubjectSelectionBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new SubjectSelectionBean());
		}
		
	}
	
	@PostMapping(value = "saveSubject")
	@ResponseBody
	public ResponseBodyBean<Boolean> saveSubject(@RequestParam("schoolId") Integer schoolId,@RequestParam("yearId") Integer yearId,@RequestParam("standardId") Integer standardId,
			@RequestParam("studentId") Integer studentId,@RequestParam("subjectArr[]") String[] subjectArr,RedirectAttributes redirectAttributes,HttpSession session) {
		try {
			boolean isSaveSuccessful= studentAdmissionService.saveSubject(schoolId,yearId,standardId,studentId,subjectArr);
			return new ResponseBodyBean<Boolean>("200",HttpStatus.OK,isSaveSuccessful);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Boolean>("500",HttpStatus.INTERNAL_SERVER_ERROR,false);
		}
		
	}
	
	
	@GetMapping(value = "getStudentInfoForCatelog")
	@ResponseBody
	public ResponseBodyBean<StudentDetailsBean> getStudentInfoForCatelog(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("searchFlag") String searchFlag,@RequestParam("searchText") String searchText) {
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try {
			studentList=studentAdmissionService.getStudentInformation(schoolId,searchFlag,searchText);
			if(studentList.size()>0) {
				return new ResponseBodyBean<StudentDetailsBean>("200",HttpStatus.OK,studentList);
			}else {
				return new ResponseBodyBean<StudentDetailsBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}

	}
	@PostMapping(value = "getStudentInfoForCatelogNew")
	@ResponseBody
	public ResponseBodyBean<StudentDetailsBean> getStudentInfoForCatelogNew(@RequestBody SearchTextBean searchTextBean) {
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try {
			studentList=studentAdmissionService.getStudentInformation(searchTextBean.getSchoolId(),searchTextBean.getSearchFlag(),searchTextBean.getSearchText());
			if(studentList.size()>0) {
				return new ResponseBodyBean<StudentDetailsBean>("200",HttpStatus.OK,studentList);
			}else {
				return new ResponseBodyBean<StudentDetailsBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}

	@PostMapping(value = "getStudentInfoForCatelogNew1")
	@ResponseBody
	public ResponseBodyBean<StudentDetailsBean> getStudentInfoForCatelogNew(@RequestBody List<SearchTextBean> searchTextBean) {
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try {
//			studentList=studentAdmissionService.getStudentInformation(searchTextBean.getSchoolId(),searchTextBean.getSearchFlag(),searchTextBean.getSearchText());
			studentList=studentAdmissionService.getStudentInformation(searchTextBean);
			if(studentList.size()>0) {
				return new ResponseBodyBean<StudentDetailsBean>("200",HttpStatus.OK,studentList);
			}else {
				return new ResponseBodyBean<StudentDetailsBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value = "getStudentNameAutoComplete")
	@ResponseBody
	public ResponseBodyBean<StudentDetailsBean> getStudentNameAutoComplete(@RequestParam("searchText") String searchText,@RequestParam("schoolId") Integer schoolId) {
		List<StudentDetailsBean> studentList = new ArrayList<>();
		try {
			studentList=studentAdmissionService.getStudentNameAutoComplete(schoolId,searchText);
			if(studentList.size()>0) {
				return new ResponseBodyBean<StudentDetailsBean>("200",HttpStatus.OK,studentList);
			}else {
				return new ResponseBodyBean<StudentDetailsBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StudentDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}

	}
	
	@PostMapping(value = "savemonthlyCatalog")
	@ResponseBody
	public ResponseBodyBean<Boolean> savemonthlyCatalog(@RequestBody StudentDetailsBean studentDetailsBean) {
		try {
	        System.out.println("Received StudentDetailsBean: " + studentDetailsBean);
			boolean isSaveSuccessful= studentAdmissionService.savemonthlyCatalog(studentDetailsBean);
			return new ResponseBodyBean<Boolean>("200",HttpStatus.OK,isSaveSuccessful);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Boolean>("500",HttpStatus.INTERNAL_SERVER_ERROR,false);
		}
		
	}

//	@PostMapping(value = "savemonthlyCatalog")
//	@ResponseBody
//	public ResponseBodyBean<Boolean> savemonthlyCatalog(@RequestBody List<StudentDetailsBean> studentDetailsBean) {
//		try {
//			boolean isSaveSuccessful= studentAdmissionService.savemonthlyCatalog(studentDetailsBean);
//			return new ResponseBodyBean<Boolean>("200",HttpStatus.OK,isSaveSuccessful);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseBodyBean<Boolean>("500",HttpStatus.INTERNAL_SERVER_ERROR,false);
//		}
//	}
	
	@PostMapping(value = "updateStudentRegNumber")
	@ResponseBody
	public ResponseBodyBean<Integer> updateStudentRegNumber(@RequestParam("studentRegNumber") String studentRegNumber,@RequestParam("schoolId") Integer schoolId,@RequestParam("studentId") Integer studentId) {
		//List<StudentDetailsBean> studentList = new ArrayList<>();
		try {
			Integer id=studentAdmissionService.updateStudentRegNumber(studentRegNumber,schoolId,studentId);
			if(id>0) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,id);
			}else {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,new ArrayList<>(),"regsitration number is duplicate please try another registration number");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}

	}
	
}
