package com.ingenio.admission.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.admission.bean.AppVersionForReactBean;
import com.ingenio.admission.bean.CurrentApkVersionBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.bean.StudentCountBean;
import com.ingenio.admission.bean.StudentDetailsBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.service.UserRoleService;


@RestController
@CrossOrigin
public class UserRoleController {
	
	@Autowired
	UserRoleService userRoleService;
	
	@PostMapping(value ="updateUserDeviceDetails")
	public @ResponseBody ResponseBodyBean<UserDetailsBean> updateUserDeviceDetails(@RequestParam("userId") Integer userId,@RequestParam("deviceId") String deviceId,
			@RequestParam("IMEICode")String IMEICode , @RequestParam("language") String language,
			@RequestParam(value ="mobNum", required=false)String mobNum ,@RequestParam(value ="deviceType", required=false)String deviceType) {
		ResponseBodyBean<UserDetailsBean> responseBodyBean = new ResponseBodyBean<UserDetailsBean>();
		try {
			List<UserDetailsBean> reponseList = new ArrayList<>();
			UserDetailsBean userDetailsBean = userRoleService.getStudentDetails(userId,deviceId,IMEICode,language,mobNum,deviceType);
			if(userDetailsBean!=null){
				reponseList.add(userDetailsBean);
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("200",HttpStatus.OK,reponseList);
			}else{
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("404",HttpStatus.BAD_REQUEST,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<UserDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
	
	
	@PostMapping(value ="newUpdateUserDeviceDetails")
	public @ResponseBody ResponseBodyBean<UserDetailsBean> updateUserDeviceDetails1(@RequestParam("userId") Integer userId,@RequestParam("deviceId") String deviceId,
			@RequestParam("IMEICode")String IMEICode , @RequestParam("language") String language,
			@RequestParam(value ="mobNum", required=false)String mobNum ,
			@RequestParam(value ="deviceType", required=false)String deviceType,
			@RequestParam(value ="schoolId", required=false)Integer schoolId,
			@RequestParam(value ="isReact", required=false,defaultValue="0")Integer isReact, 
			@RequestParam(value="priorityType", required=false, defaultValue="0") Integer priorityType, 
			@RequestParam(value="typeOfManagementId", required=false, defaultValue="0") Integer typeOfManagementId,
			@RequestParam(value="menuPolicy", required=false, defaultValue="0") Integer menuPolicy,
			@RequestParam(value="specificMenuCategory", required=false, defaultValue="0") Integer specificMenuCategory,
			@RequestParam(value="sansthaKey", required=false, defaultValue="0") Integer sansthaKey) {
		
		ResponseBodyBean<UserDetailsBean> responseBodyBean = new ResponseBodyBean<UserDetailsBean>();
		try {
			System.out.println("userId"+userId + "deviceId"+deviceId+"IMEICode"+IMEICode+"mobNum"+mobNum + "deviceType"+deviceType+"language"+language);
			List<UserDetailsBean> reponseList = new ArrayList<>();
			System.out.println("userId"+userId + "deviceId"+deviceId + "IMEICode"+IMEICode+"language"+language+"mobNum"+mobNum+"deviceType"+deviceType);
			UserDetailsBean userDetailsBean = userRoleService.getStudentDetails1(userId,deviceId,IMEICode,language,mobNum,
					deviceType,isReact,schoolId,priorityType, typeOfManagementId, menuPolicy, specificMenuCategory, sansthaKey);
			if(userDetailsBean!=null){
				reponseList.add(userDetailsBean);
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("200",HttpStatus.OK,reponseList);
			}else{
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("404",HttpStatus.BAD_REQUEST,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<UserDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
		
	}
	
	@PostMapping(value ="uploadProfilePhoto")
	public @ResponseBody ResponseBodyBean<String> uploadStudentPhoto(MultipartFile file, String fileName,Integer schoolId,String regNo,Integer studentId,String role ) {
		ResponseBodyBean<String> responseBodyBean = new ResponseBodyBean<String>();
		try {
			String saveId = userRoleService.uploadProfilePhoto(file,fileName,schoolId,regNo,studentId,role);
			if(saveId!=null){
				responseBodyBean = new ResponseBodyBean<String>("200",HttpStatus.OK,saveId);
			}else{
				responseBodyBean = new ResponseBodyBean<String>("404",HttpStatus.BAD_REQUEST,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}

		return responseBodyBean;
		
	}
	
	
	@PostMapping(value ="studentDetails")
	public @ResponseBody ResponseBodyBean<UserDetailsBean> getUserDetails(@RequestParam("studentId") Integer studentId,
			@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<UserDetailsBean> responseBodyBean = new ResponseBodyBean<UserDetailsBean>();
		try {
			List<UserDetailsBean> reponseList = new ArrayList<>();	
			UserDetailsBean userDetailsBean = userRoleService.getUserDetails(studentId,schoolId);
			if(userDetailsBean!=null){
				reponseList.add(userDetailsBean);
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("200",HttpStatus.OK,reponseList);
			}else{
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("404",HttpStatus.BAD_REQUEST,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<UserDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;		
	}
	
	@PostMapping(value ="staffDetails")
	public @ResponseBody ResponseBodyBean<UserDetailsBean> getStaffDetails(@RequestParam("staffId") Integer staffId,
			@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<UserDetailsBean> responseBodyBean = new ResponseBodyBean<UserDetailsBean>();
		try {
			List<UserDetailsBean> reponseList = new ArrayList<>();	
			UserDetailsBean userDetailsBean = userRoleService.getStaffDetails(staffId,schoolId);
			if(userDetailsBean!=null){
				reponseList.add(userDetailsBean);
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("200",HttpStatus.OK,reponseList);
			}else{
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("404",HttpStatus.BAD_REQUEST,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<UserDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;		
	}
	
	@PostMapping(value ="StudentStrengthForTeacher")
	public @ResponseBody ResponseBodyBean<StudentCountBean> getStudentStrengthForTeacher(@RequestParam("staffId") Integer staffId,
			@RequestParam("schoolId") Integer schoolId ,@RequestParam("profileRole") String profileRole,
			@RequestParam("yearId") Integer yearId,
			@RequestParam("renewAdmissionDate") String renewDate,@RequestParam("language") String language) {
		ResponseBodyBean<StudentCountBean> responseBodyBean = new ResponseBodyBean<StudentCountBean>();
		List<StudentCountBean> studentCountList = new ArrayList<StudentCountBean>();
		try {
			studentCountList =userRoleService.getStudentStrengthForTeacher(staffId,schoolId,profileRole,yearId,renewDate,language);
			if(CollectionUtils.isNotEmpty(studentCountList)) {
				responseBodyBean = new ResponseBodyBean<StudentCountBean>("200",HttpStatus.OK,studentCountList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<StudentCountBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<StudentCountBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
	
	@PostMapping(value ="classAndDivisionWiseStudentDetails")
	public @ResponseBody ResponseBodyBean<StudentDetailsBean> getclassAndDivisionWiseStudentDetails(@RequestParam("standardId") Integer standardId,
			@RequestParam("divisionId") Integer divisionId,@RequestParam("schoolId") Integer schoolId ,@RequestParam("profileRole") String profileRole,
			@RequestParam("yearId") Integer yearId,
			@RequestParam("renewAdmissionDate") String renewDate) {
		ResponseBodyBean<StudentDetailsBean> responseBodyBean = new ResponseBodyBean<StudentDetailsBean>();
		List<StudentDetailsBean> studetnDetailsbeanList = new ArrayList<StudentDetailsBean>();
		try {
			studetnDetailsbeanList =userRoleService.getclassAndDivisionWiseStudentDetails(standardId,divisionId,schoolId,profileRole,yearId,renewDate);
			if(CollectionUtils.isNotEmpty(studetnDetailsbeanList)) {
				responseBodyBean = new ResponseBodyBean<StudentDetailsBean>("200",HttpStatus.OK,studetnDetailsbeanList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<StudentDetailsBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<StudentDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
	
	@GetMapping(value="/showHideAddButton")
	@Produces("application/json")	
	public @ResponseBody ResponseBodyBean<Integer> setShowHideAddButtton(@RequestParam("staffId") Integer staffId,
				@RequestParam("schoolId") Integer schoolId) {
		try {
			Integer count = userRoleService.setShowHideAddButtton(staffId,schoolId);
			if(count!=null && count==1) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,count,"Show");
			}
			else if(count!=null && count==0) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,count,"Hide");
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@PostMapping(value ="saveDeviceDetails")
	public @ResponseBody ResponseBodyBean<Integer> saveDeviceDetails(@RequestParam("userName") String userName,@RequestParam("deviceType") String deviceType,
			@RequestParam("IMEICode") String IMEICode) {
		ResponseBodyBean<Integer> responseBodyBean = new ResponseBodyBean<Integer>();
		try {
			Integer saveId=0; 			
			saveId = userRoleService.saveDeviceDetails(userName,deviceType,IMEICode);
			if(saveId!=0){
				responseBodyBean = new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
			}else{
				responseBodyBean = new ResponseBodyBean<Integer>("404",HttpStatus.BAD_REQUEST,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
		
	}
	
	
	@GetMapping(value ="CurrentApkVersion")
	public @ResponseBody ResponseBodyBean<String> getCurrentApkVersion() {
		ResponseBodyBean<String> responseBodyBean = new ResponseBodyBean<String>();
		try {
			System.out.println("current version");
			String CurrentApkVersion=""; 			
			CurrentApkVersion = userRoleService.getCurrentApkVersion();
			if(CurrentApkVersion!=null && !CurrentApkVersion.equalsIgnoreCase("")){
				responseBodyBean = new ResponseBodyBean<String>("200",HttpStatus.OK,CurrentApkVersion);
			}else{
				responseBodyBean = new ResponseBodyBean<String>("404",HttpStatus.BAD_REQUEST,CurrentApkVersion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
		
	}
	
	@GetMapping(value ="CurrentApkVersionForCalendar")
	public @ResponseBody ResponseBodyBean<String> getCurrentApkVersionForCalendar() {
		ResponseBodyBean<String> responseBodyBean = new ResponseBodyBean<String>();
		try {
			String CurrentApkVersion=""; 			
			CurrentApkVersion = userRoleService.getCurrentApkVersionForCalendar();
			if(CurrentApkVersion!=null && !CurrentApkVersion.equalsIgnoreCase("")){
				responseBodyBean = new ResponseBodyBean<String>("200",HttpStatus.OK,CurrentApkVersion);
			}else{
				responseBodyBean = new ResponseBodyBean<String>("404",HttpStatus.BAD_REQUEST,CurrentApkVersion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<String>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
		
	}

	@GetMapping("/getCurrentApkVersionDetails")
	public @ResponseBody ResponseBodyBean<AppVersionForReactBean> getCurrentApkVersionForReactDetails(@RequestParam(value="schoolId",required=false ) Integer schoolId,@RequestParam(value="userId",required=false) Integer userId,@RequestParam(value="isReact" ,required=false)  String isReact,@RequestParam(value="appType" ,required=false,defaultValue = "0")  String appType)
	{
		
		try {
			List<AppVersionForReactBean> appVersionForReactBean=userRoleService.getCurrentApkVersionDetailsForReact( schoolId,  userId,  isReact,appType);
				if(CollectionUtils.isNotEmpty(appVersionForReactBean)) {
					return new ResponseBodyBean<AppVersionForReactBean>("200",HttpStatus.OK,appVersionForReactBean);
				}
				else {
					return new ResponseBodyBean<AppVersionForReactBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseBodyBean<AppVersionForReactBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			}
	}

	
}	
