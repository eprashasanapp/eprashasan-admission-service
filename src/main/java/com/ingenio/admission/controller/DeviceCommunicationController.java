package com.ingenio.admission.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ingenio.admission.bean.DeviceCommunicationBean;
import com.ingenio.admission.service.DeviceCommunicationService;

@Controller
@CrossOrigin
public class DeviceCommunicationController {
	@Autowired
	DeviceCommunicationService deviceCommunicationService;

	private ExecutorService executor = Executors.newSingleThreadExecutor();

//	@GetMapping(value = "saveAttendanceRFIData")
//	@ResponseBody
//	public String saveAttendanceRFIData(RedirectAttributes redirectAttributes, HttpSession session,
//			HttpServletRequest request) {
//		String Rfid = "";
//		try {
//			String RFiString = request.getQueryString();
//			DeviceCommunicationBean list1 = deviceCommunicationService.saveAttendanceRFIData(RFiString);
//			executor.submit(() -> deviceCommunicationService.saveStaffAttendance(list1));
//
//			Rfid = "$RFID=0#";
//			session.setAttribute("rfiId", Rfid);
//			return Rfid;
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "0";
//		}
//
//	}
	
	@GetMapping(value = "saveAttendanceRFIData")
	@ResponseBody
	public String saveAttendanceRFIData(RedirectAttributes redirectAttributes, HttpSession session, HttpServletRequest request,
			@RequestParam (required = false) String latitude, @RequestParam (required = false) String longitude, @RequestParam (required = false) String ipAddress, @RequestParam (required = false) String macAddress, 
			@RequestParam (required = false) String randomCode, @RequestParam (required = false) String imei) {
		String Rfid = "";
		try {
			String RFiString = request.getQueryString();
			DeviceCommunicationBean list1 = deviceCommunicationService.saveAttendanceRFIData(RFiString, latitude, longitude, ipAddress, macAddress, randomCode, imei);
			executor.submit(() -> deviceCommunicationService.saveStaffAttendance(list1));

			Rfid = "$RFID=0#";
			session.setAttribute("rfiId", Rfid);
			return Rfid;

		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}

	}
	

	//  @Scheduled(cron = "0 15 16,30 ? * MON-SAT", zone = "GMT+5:30")

//	Disbaled the scheduler line below
//	  @Scheduled(cron = "0 15 16 ? * MON-SAT", zone = "GMT+5:30")
	/*
	 * @GetMapping(value = "sendsmsAttendanceRFIData")
	 * 
	 * @ResponseBody
	 */

//	public void sendSMSForAbsentStudent() {
//		try {
//			DeviceCommunicationBean list1 = deviceCommunicationService.sendSMSForAbsentStudent();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		//return "";
//	}
	  

//	Use date in yyyy-mm-dd format
	
	@PostMapping(value = "/rfidPunchToStaffAttendance")
	public String rfidPunchToStaffAttendance(@RequestParam Integer schoolId, @RequestParam String fromDate, @RequestParam String toDate)
	{
		return deviceCommunicationService.rfidPunchToStaffAttendance(schoolId, fromDate, toDate);
	}
	
//	@PostMapping(value = "/rfidPunchToStaffAttendance")
//	public String rfidPunchToStaffAttendance(@RequestParam Integer schoolId, @RequestParam String date)
//	{
//		return deviceCommunicationService.rfidPunchToStaffAttendance(schoolId, date);
//	}
	
}
