package com.ingenio.admission.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;
import com.ingenio.admission.bean.AppRoleBean;
import com.ingenio.admission.bean.CommonUtilityBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.bean.StudentAdmissionBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.service.CommonUtilityService;
import com.ingenio.admission.service.StudentAdmissionService;
import com.ingenio.admission.service.UserAuthenticationService;
import com.ingenio.admission.util.CryptographyUtil;

@Controller
@CrossOrigin


/*
 * @PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
 * 
 * @PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
 * 
 * @PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")
 */
  
 

public class UserAuthenticationController {

	@Autowired
	public UserAuthenticationService userAuthenticationService;

	@Autowired
	public StudentAdmissionService studentAdmissionService;

	@Autowired
	public CommonUtilityService commonUtilityService;

	@Autowired
	public SchoolMasterRepository schoolMasterRepository;

	CryptographyUtil cry = new CryptographyUtil();

	@Autowired
	private Environment env;

	@GetMapping("/login")
	public String login(@RequestParam String schoolkey, Model model) {
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(Integer.parseInt(schoolkey));
		if (schoolModel != null) {
			Integer languageId = schoolModel.getLanguage().getLanguageId();
			String language = "en";
			if (languageId == 2) {
				language = "mr";
			}
			model.addAttribute("schoolKey", schoolkey);
			model.addAttribute("online", env.getProperty(language + "." + "online"));
			model.addAttribute("eprashasan", env.getProperty(language + "." + "eprashasan"));
			model.addAttribute("enterMobileNO", env.getProperty(language + "." + "enterMobileNO"));
			model.addAttribute("aleadyRegistered", env.getProperty(language + "." + "aleadyRegistered"));
			model.addAttribute("next", env.getProperty(language + "." + "next"));
			model.addAttribute("newnumber", env.getProperty(language + "." + "newnumber"));
			model.addAttribute("forgot", env.getProperty(language + "." + "forgot"));
			model.addAttribute("password", env.getProperty(language + "." + "password"));
			model.addAttribute("login", env.getProperty(language + "." + "login"));
			model.addAttribute("verifyotponmobile", env.getProperty(language + "." + "verifyotponmobile"));
			model.addAttribute("verifyOTP", env.getProperty(language + "." + "verifyOTP"));
			model.addAttribute("enterotp", env.getProperty(language + "." + "enterotp"));
			model.addAttribute("username", env.getProperty(language + "." + "username"));
			model.addAttribute("newpassword", env.getProperty(language + "." + "newpassword"));
			model.addAttribute("renewpassword", env.getProperty(language + "." + "renewpassword"));
			model.addAttribute("succssfulotp", env.getProperty(language + "." + "succssfulotp"));
			model.addAttribute("submit", env.getProperty(language + "." + "submit"));
			model.addAttribute("acadenicyear", env.getProperty(language + "." + "acadenicyear"));
			model.addAttribute("standard", env.getProperty(language + "." + "standard"));
			model.addAttribute("selectinitialname", env.getProperty(language + "." + "selectinitialname"));
			model.addAttribute("initialname", env.getProperty(language + "." + "initialname"));
			model.addAttribute("selectstandard", env.getProperty(language + "." + "selectstandard"));
			model.addAttribute("firstname", env.getProperty(language + "." + "firstname"));
			model.addAttribute("secondname", env.getProperty(language + "." + "secondname"));
			model.addAttribute("lastname", env.getProperty(language + "." + "lastname"));
			model.addAttribute("confirmpassword", env.getProperty(language + "." + "confirmpassword"));
			model.addAttribute("academicyear", env.getProperty(language + "." + "academicyear"));
			model.addAttribute("confirmpassword", env.getProperty(language + "." + "confirmpassword"));
			model.addAttribute("formno", env.getProperty(language + "." + "formno"));
			model.addAttribute("regno", env.getProperty(language + "." + "regno"));
			return "login";
		} else {
			return "pagenotfound";
		}
	}

	@PostMapping("generateOtp")
	public @ResponseBody ResponseBodyBean<CommonUtilityBean> generateOtp(@RequestParam("mobileNum") String username,
			@RequestParam("schoolId") Integer schoolId, HttpSession session) {
		String language = "";
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<UserDetailsBean> userDetails = commonUtilityService.checkExistingUser(username, schoolId);
		if (userDetails.size() > 0 && (StringUtils.isEmpty("" + session.getAttribute("studentId"))
				|| ("null").equalsIgnoreCase("" + session.getAttribute("studentId")))) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("1000", HttpStatus.OK, commonUtilityBeanList);
			return responseBodyBean;
		}
		try {
			if (schoolId == 0) {
				List<UserDetailsBean> userDetailBean = commonUtilityService.getUserRoleAndSchoolDetails(username,
						language);
				if (CollectionUtils.isNotEmpty(userDetailBean)) {
					int otp = userAuthenticationService.generateOTP(username);
					System.out.println(otp);
					String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils
							.escapeJava(studentAdmissionService.getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));
					String message = "" + otp
							+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes -" + schoolName
							+ " - INGENIO TECHNOLOGIES.";
					userAuthenticationService.sendOtpMessage(username, message, schoolId, 1); // 8983379099
					userAuthenticationService.sendWhatsAppSMS(username,message,schoolId,3);
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setOtp("" + otp);
					commonUtilityBean.setMobileNo(username);
					commonUtilityBeanList.add(commonUtilityBean);
					responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200", HttpStatus.OK,
							commonUtilityBeanList);
				} else {
					responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404", "User Not found", null);
				}

			} else {
				SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
				Integer languageId = schoolModel.getLanguage().getLanguageId();
				int otp = userAuthenticationService.generateOTP(username);
				String message = "";
				System.out.println(otp);
				String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils
						.escapeJava(studentAdmissionService.getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));

				if (languageId == 2) {
//					message = env.getProperty("mr.otp1") + " " + otp + " " + env.getProperty("mr.otp2") + " - "
//							+ schoolName + "";
					message = "" + otp
							+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes  - INGENIO TECHNOLOGIES.";
				} else {
//					message = "" + otp + " is the OTP for Online Admission Year 2020-2021.Valid for 10 min. - "
//							+ schoolName +- "";
					message = "" + otp
							+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes  - INGENIO TECHNOLOGIES.";
				}
				System.out.println(message);
				userAuthenticationService.sendOtpMessage(username, message, schoolId, languageId); // 8983379099
				userAuthenticationService.sendWhatsAppSMS(username,message,schoolId,3);
				CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
				commonUtilityBean.setOtp("" + otp);
				commonUtilityBean.setMobileNo(username);
				commonUtilityBeanList.add(commonUtilityBean);
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200", HttpStatus.OK, commonUtilityBeanList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
		return responseBodyBean;
	}

	@PostMapping("generateOtpForLogIn")
	public @ResponseBody ResponseBodyBean<CommonUtilityBean> generateOtpForLogIn(
			@RequestParam("mobileNum") String username, @RequestParam("schoolId") Integer schoolId,
			HttpSession session) {
		String language = "";
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<UserDetailsBean> userDetails = commonUtilityService.checkExistingUser(username, schoolId);
		System.out.println(session.getAttribute("studentId"));
		if (userDetails.size() > 0) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("1000", HttpStatus.OK, commonUtilityBeanList);
			return responseBodyBean;
		} else {
			try {
				if (schoolId == 0) {
					List<UserDetailsBean> userDetailBean = commonUtilityService.getUserRoleAndSchoolDetails(username,
							language);
					if (CollectionUtils.isNotEmpty(userDetailBean)) {
						int otp = userAuthenticationService.generateOTP(username);
						System.out.println(otp);
						String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(
								studentAdmissionService.getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));
						String message = "" + otp
								+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes - INGENIO TECHNOLOGIES.";
						userAuthenticationService.sendOtpMessage(username, message, schoolId, 1); // 8983379099
						userAuthenticationService.sendWhatsAppSMS(username,message,schoolId,3);
						CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
						commonUtilityBean.setOtp("" + otp);
						commonUtilityBean.setMobileNo(username);
						commonUtilityBeanList.add(commonUtilityBean);
						responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200", HttpStatus.OK,
								commonUtilityBeanList);
					} else {
						responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404", "User Not found", null);
					}
				} else {
					SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
					Integer languageId = schoolModel.getLanguage().getLanguageId();
					int otp = userAuthenticationService.generateOTP(username);
					String message = "";
					System.out.println(otp);
					String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils
							.escapeJava(studentAdmissionService.getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));
					if (languageId == 2) {
//						message = env.getProperty("mr.otp1") + " " + otp + " " + env.getProperty("mr.otp2") + " - "
//								+ schoolName + "";
						message = "" + otp
								+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes - INGENIO TECHNOLOGIES.";
					} else {
//						message = "" + otp + " is the OTP for Online Admission Year 2021-2022.Valid for 10 min. - "
//								+ schoolName + "";
						message = "" + otp
								+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes - INGENIO TECHNOLOGIES.";
					}
					System.out.println(message);
					userAuthenticationService.sendOtpMessage(username, message, schoolId, languageId); // 8983379099
					userAuthenticationService.sendWhatsAppSMS(username,message,schoolId,3);

					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setOtp("" + otp);
					commonUtilityBean.setMobileNo(username);
					commonUtilityBeanList.add(commonUtilityBean);
					responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200", HttpStatus.OK,
							commonUtilityBeanList);
				}

			} catch (Exception e) {
				e.printStackTrace();
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
						new ArrayList<>());
			}
		}
		return responseBodyBean;
	}

	@PostMapping("generateOtpForForgotPassword")
	public @ResponseBody ResponseBodyBean<CommonUtilityBean> generateOtpForForgotPassword(
			@RequestParam("mobileNum") String username, @RequestParam("schoolId") Integer schoolId,
			HttpSession session) {
		String language = "";
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<UserDetailsBean> userDetails = commonUtilityService.checkExistingUser(username, schoolId);
		System.out.println(session.getAttribute("studentId"));
		if (userDetails.size() > 0) {
			try {
				if (schoolId == 0) {
					List<UserDetailsBean> userDetailBean = commonUtilityService.getUserRoleAndSchoolDetails(username,
							language);
					if (CollectionUtils.isNotEmpty(userDetailBean)) {
						int otp = userAuthenticationService.generateOTP(username);
						System.out.println(otp);
						String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils.escapeJava(
								studentAdmissionService.getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));
//						String message = "Please use OTP - " + otp + " to verify your mobile number for ePrashasan.";
						String message = "" +otp+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes-INGENIO TECHNOLOGIES." ;

						userAuthenticationService.sendOtpMessage(username, message, schoolId, 1); // 8983379099
						userAuthenticationService.sendWhatsAppSMS(username,message,schoolId,3);
						CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
						commonUtilityBean.setOtp("" + otp);
						commonUtilityBean.setMobileNo(username);
						commonUtilityBeanList.add(commonUtilityBean);
						responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200", HttpStatus.OK,
								commonUtilityBeanList);
					} else {
						responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404", "User Not found", null);
					}
				} else {
					int otp = userAuthenticationService.generateOTP(username);
					System.out.println(otp);
					String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils
							.escapeJava(studentAdmissionService.getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));
					String message = "" +otp+ " is the OTP for Online Admission  Year 2021-2022 Valid  for 10 minutes-INGENIO TECHNOLOGIES." ;
					userAuthenticationService.sendOtpMessage(username, message, schoolId, 1); // 8983379099
					userAuthenticationService.sendWhatsAppSMS(username,message,schoolId,3);
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setOtp("" + otp);
					commonUtilityBean.setMobileNo(username);
					commonUtilityBeanList.add(commonUtilityBean);
					responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200", HttpStatus.OK,
							commonUtilityBeanList);
				}
			} catch (Exception e) {
				e.printStackTrace();
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
						new ArrayList<>());
			}
		}
		return responseBodyBean;
	}

	@GetMapping("/validateOtp")
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody ResponseBodyBean<UserDetailsBean> validateOtp(@RequestParam("otpnum") int otpnum,
			@RequestParam("mobileNum") String username, @RequestParam("schoolId") String schoolId, HttpSession session)
			throws InvalidKeyException, NumberFormatException, NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException, IOException {
		ResponseBodyBean<UserDetailsBean> responseBodyBean = new ResponseBodyBean<UserDetailsBean>();
		String language = "";
		System.out.println("otp-----" + otpnum + " username----" + username + "schoolId" + schoolId);
		try {
			int serverOtp = userAuthenticationService.getOtp(username);
			CryptographyUtil cry = new CryptographyUtil();
			if (otpnum == serverOtp) {
				userAuthenticationService.clearOTP(username);
				if (StringUtils.isEmpty((String) session.getAttribute("welcomeName"))) {
					session.setAttribute("welcomeName", username);
				}

				List<UserDetailsBean> userDetailBean = commonUtilityService.getUserRoleAndSchoolDetails(username,
						language);

				if (StringUtils.isNotEmpty(schoolId) && !schoolId.equals("0")) {
					session.setAttribute("schoolId", schoolId);
					session.setAttribute("userName", username);
					session.setAttribute("standardList",
							commonUtilityService.getStandardCombo(Integer.parseInt(schoolId)).size());
					session.setAttribute("tabListSize", commonUtilityService.getTabsCombo(Integer.parseInt(schoolId)));
					String schoolName = cry.decrypt("qwertyschoolname",
							StringEscapeUtils.escapeJava(studentAdmissionService
									.getSchoolName(Integer.parseInt(schoolId)).replaceAll("[\\r\\n]+", "")));
					session.setAttribute("schoolName", schoolName);
				}
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("200", HttpStatus.OK, userDetailBean);
			} else {
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("404", "Invalid OTP", null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<UserDetailsBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}

		return responseBodyBean;
	}

	@GetMapping("/loginLink")
	public @ResponseBody ResponseBodyBean<StudentAdmissionBean> login(@RequestParam("userName") String userName,
			@RequestParam("password") String password, @RequestParam("schoolId") String schoolId, HttpSession session) {
		session.setAttribute("schoolId", schoolId);
		ResponseBodyBean<StudentAdmissionBean> responseBodyBean = new ResponseBodyBean<StudentAdmissionBean>();
//		session.invalidate();
		List<StudentAdmissionBean> studentAdmissionBean = userAuthenticationService.getStudentDetails(userName,
				password, Integer.parseInt(schoolId));
		List<StudentAdmissionBean> studentMasterList = userAuthenticationService.findByUser(userName,
				Integer.parseInt(schoolId));
		try {
			if (CollectionUtils.isNotEmpty(studentAdmissionBean)) {
				CryptographyUtil cry = new CryptographyUtil();
				if (StringUtils.isNotEmpty(schoolId) && !schoolId.equals("0")) {
					session.setAttribute("schoolId", schoolId);
					session.setAttribute("userName", userName);
					session.setAttribute("standardList",
							commonUtilityService.getStandardCombo(Integer.parseInt(schoolId)).size());
					session.setAttribute("tabListSize", commonUtilityService.getTabsCombo(Integer.parseInt(schoolId)));
					String schoolName = cry.decrypt("qwertyschoolname",
							StringEscapeUtils.escapeJava(studentAdmissionService
									.getSchoolName(Integer.parseInt(schoolId)).replaceAll("[\\r\\n]+", "")));
					session.setAttribute("schoolName", schoolName);
				}
				session.setAttribute("studentId", studentAdmissionBean.get(0).getStudentId());
				session.setAttribute("welcomeName", studentAdmissionBean.get(0).getFirstName());
				session.setAttribute("mobileNo", "");
				if (StringUtils.isNotEmpty(studentAdmissionBean.get(0).getContactNumber1ForSMS())) {
					session.setAttribute("mobileNo", studentAdmissionBean.get(0).getContactNumber1ForSMS());
				}
				responseBodyBean = new ResponseBodyBean<StudentAdmissionBean>("200", HttpStatus.OK,
						studentAdmissionBean);
			} else {
				if (CollectionUtils.isNotEmpty(studentMasterList)) {
					responseBodyBean = new ResponseBodyBean<StudentAdmissionBean>("404", HttpStatus.BAD_REQUEST,
							new ArrayList<>());
				} else {
					responseBodyBean = new ResponseBodyBean<StudentAdmissionBean>("101", HttpStatus.BAD_REQUEST,
							new ArrayList<>());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<StudentAdmissionBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
		return responseBodyBean;
	}

	@PostMapping("/logout")
	public RedirectView logout(HttpSession session) {
		String schoolId = (String) session.getAttribute("schoolId");
		if (session != null) {
			session.invalidate();
		}
		return new RedirectView("login?schoolkey=" + schoolId);
	}

	@GetMapping("/getUserDetailsWithoutOTP")
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody ResponseBodyBean<UserDetailsBean> getUserDetailsWithoutOTP(
			@RequestParam("mobileNum") String username, HttpSession session)
			throws InvalidKeyException, NumberFormatException, NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException, IOException {
		String language = "";
		ResponseBodyBean<UserDetailsBean> responseBodyBean = new ResponseBodyBean<UserDetailsBean>();
		try {
			List<UserDetailsBean> userDetailBean = commonUtilityService.getUserRoleAndSchoolDetails(username, language);
			if (CollectionUtils.isNotEmpty(userDetailBean)) {
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("200", HttpStatus.OK, userDetailBean);
			} else {
				responseBodyBean = new ResponseBodyBean<UserDetailsBean>("404", HttpStatus.NOT_FOUND, userDetailBean);
			}

		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<UserDetailsBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}

		return responseBodyBean;
	}

	@GetMapping("/adminLoginLink")
	public @ResponseBody ResponseBodyBean<AppRoleBean> adminLogin(@RequestParam("userName") String userName,
			@RequestParam("password") String password, @RequestParam("schoolId") String schoolId, HttpSession session) {
		session.setAttribute("schoolId", schoolId);
		ResponseBodyBean<AppRoleBean> responseBodyBean = new ResponseBodyBean<AppRoleBean>();
		AppRoleBean appRoleBean = userAuthenticationService.getAdminDetails(userName, password,
				Integer.parseInt(schoolId));

		try {
			if (appRoleBean != null) {
				session.setAttribute("schoolId", schoolId);
				if (StringUtils.isNotEmpty(appRoleBean.getFirstName())) {
					session.setAttribute("welcomeName", appRoleBean.getFirstName());
				} else {
					session.setAttribute("welcomeName", userName);
				}
				responseBodyBean = new ResponseBodyBean<AppRoleBean>("200", HttpStatus.OK, appRoleBean);
			} else {
				responseBodyBean = new ResponseBodyBean<AppRoleBean>("404", HttpStatus.BAD_REQUEST, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<AppRoleBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
		return responseBodyBean;
	}

	@GetMapping("/changePassword")
	public @ResponseBody ResponseBodyBean<Integer> changePassword(@RequestParam("studentId") Integer studentId,
			@RequestParam("userName") String userName, @RequestParam("oldPassword") String oldpassword,
			@RequestParam("newPassword") String newPassword, @RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<Integer> responseBodyBean = new ResponseBodyBean<Integer>();
		Integer responseValue = userAuthenticationService.changePassword(oldpassword, newPassword, studentId, schoolId,
				userName);
		try {
			if (responseValue > 0) {
				responseBodyBean = new ResponseBodyBean<Integer>("200", HttpStatus.OK, responseValue);
			} else {
				responseBodyBean = new ResponseBodyBean<Integer>("404", HttpStatus.BAD_REQUEST, responseValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, 0);
		}
		return responseBodyBean;
	}

	@GetMapping("/forgotPassword")
	public @ResponseBody ResponseBodyBean<Integer> forgotPassword(@RequestParam("password") String password,
			@RequestParam("userName") String userName, @RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<Integer> responseBodyBean = new ResponseBodyBean<Integer>();
		Integer responseValue = userAuthenticationService.forgotPassword(userName, password, schoolId);
		try {
			if (responseValue > 0) {
				responseBodyBean = new ResponseBodyBean<Integer>("200", HttpStatus.OK, responseValue);
			} else {
				responseBodyBean = new ResponseBodyBean<Integer>("404", HttpStatus.BAD_REQUEST, responseValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, 0);
		}
		return responseBodyBean;
	}

	@PostMapping("generateOtpForCalendarRegistration")
	public @ResponseBody ResponseBodyBean<CommonUtilityBean> generateOtpForCalendarReistration(
			@RequestParam("mobileNum") String username, @RequestParam("schoolId") Integer schoolId) throws Exception {
		String language = "";
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<UserDetailsBean> userDetails = commonUtilityService.checkExistingUser(username, schoolId);
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
		Integer languageId = schoolModel.getLanguage().getLanguageId();
		int otp = userAuthenticationService.generateOTP(username);
		String message = "";
		System.out.println(otp);
		String schoolName = cry.decrypt("qwertyschoolname", StringEscapeUtils
				.escapeJava(studentAdmissionService.getSchoolName(schoolId).replaceAll("[\\r\\n]+", "")));

		if (languageId == 2) {
//			message = env.getProperty("mr.otp1") + " " + otp + " " + env.getProperty("mr.otp2") + " - " + schoolName
//					+ "";
			message = "" + otp + " use this OTP to verify your mobile number for SGBAU calendar 2021 app. - "
					+ schoolName + "";
		} else {
			message = "" + otp + " use this OTP to verify your mobile number for SGBAU calendar 2021 app. - "
					+ schoolName + "";

		}
		System.out.println(message);
		userAuthenticationService.sendOtpMessage(username, message, schoolId, languageId); // 8983379099
		userAuthenticationService.sendWhatsAppSMS(username,message,schoolId,3);
		CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
		commonUtilityBean.setOtp("" + otp);
		commonUtilityBean.setMobileNo(username);
		commonUtilityBeanList.add(commonUtilityBean);
		responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200", HttpStatus.OK, commonUtilityBeanList);

		return responseBodyBean;
	}

}
