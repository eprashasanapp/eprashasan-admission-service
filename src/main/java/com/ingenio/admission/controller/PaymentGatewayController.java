package com.ingenio.admission.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ingenio.admission.bean.FeeBean;
import com.ingenio.admission.bean.FeeIdBean;
import com.ingenio.admission.bean.FeePayTypeBean;
import com.ingenio.admission.bean.FeePayementGatewayBean;
import com.ingenio.admission.bean.FeeProspectBean;
import com.ingenio.admission.bean.OrderBean;
import com.ingenio.admission.bean.PaymentRequestBean;
import com.ingenio.admission.bean.PaymentSettingBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.bean.SavePaidBean;
import com.ingenio.admission.bean.SchoolDetailBean;
import com.ingenio.admission.bean.SessionResponseBean;
import com.ingenio.admission.bean.UserDetailsBean;
import com.ingenio.admission.model.DummyRegistrationModel;
import com.ingenio.admission.model.FeePaidProspectFeesModel;
import com.ingenio.admission.model.SchoolMasterModel;
import com.ingenio.admission.model.StandardMasterModel;
import com.ingenio.admission.model.StudentMasterModel;
import com.ingenio.admission.model.StudentStandardRenewModel;
import com.ingenio.admission.model.YearMasterModel;
import com.ingenio.admission.repository.DummyRegistrationRepository;
import com.ingenio.admission.repository.FeePaidProspectFeeRepository;
import com.ingenio.admission.repository.FeePaymentGatewayRepository;
import com.ingenio.admission.repository.FeeRepository;
import com.ingenio.admission.repository.SchoolMasterRepository;
import com.ingenio.admission.service.PaymentGatewayService;
import com.ingenio.admission.service.StudentAdmissionService;
import com.ingenio.admission.util.UTFConversionUtil;
import com.ingenio.admission.util.Utillity;

@Controller
@CrossOrigin
public class PaymentGatewayController {

	@Autowired
	PaymentGatewayService paymentGatewayService;

	@Autowired
	StudentAdmissionService studentAdmissionService;

	@Autowired
	DummyRegistrationRepository dummyRegistrationRepository;

	@Autowired
	FeePaymentGatewayRepository feePaymentGatewayRepository;

	@Autowired
	FeePaidProspectFeeRepository feePaidProspectFeeRepository;

	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	FeeRepository feeRepository;

	@GetMapping(value = "/paymentPage")
	public @ResponseBody Boolean loginMessage(Model model, @RequestParam String schoolId, @RequestParam String renewId,
			@RequestParam String newYearId, HttpSession session) {
		session.setAttribute("schoolkeys", schoolId);
		session.setAttribute("schoolId", schoolId);
		session.setAttribute("renewId", renewId);
		session.setAttribute("newYearId", newYearId);
		session.setAttribute("paymentflag", "0");
		List<SchoolDetailBean> schoolList = paymentGatewayService.getSchoolDetails(schoolId);
		UserDetailsBean userDetailsBean = paymentGatewayService.getStudentDetails(renewId);
		session.setAttribute("schoolName", schoolList.get(0).getSchoolName());
		session.setAttribute("studentName", userDetailsBean.getFirstName() + " " + userDetailsBean.getLastName());
		session.setAttribute("standardId", "" + userDetailsBean.getStandardId());
		session.setAttribute("firstName", userDetailsBean.getFirstName());
		session.setAttribute("mobileNo", userDetailsBean.getMobileNumber());
		return true;
	}

	@PostMapping(value = "/payment")
	public @ResponseBody ModelAndView getPaymentGatewayReport(
			@RequestParam(value = "razorpay_payment_id", required = false) String razorpay_payment_id,
			@RequestParam(value = "razorpay_order_id", required = false) String razorpay_order_id,
			@RequestParam(value = "razorpay_signature", required = false) String razorpay_signature,
			HttpSession session, Model model) {
		if (razorpay_payment_id == null && razorpay_order_id == null) {
			session.setAttribute("paymentflag", "2");
			paymentGatewayService.updateStatus((String) session.getAttribute("renewId"));
			paymentGatewayService.sendFailureMessage((String) session.getAttribute("renewId"),
					(String) session.getAttribute("mobileNo"), (String) session.getAttribute("schoolkeys"),
					(String) session.getAttribute("schoolName"), (String) session.getAttribute("firstName"));
		} else {
			SavePaidBean savePaidBean = new SavePaidBean();
			savePaidBean.setDeviceType("2");
			savePaidBean.setIpAddress("123");
			savePaidBean.setOrderId(razorpay_order_id);
			savePaidBean.setTransactionId(razorpay_payment_id);
			List<SessionResponseBean> sessionList = paymentGatewayService.getSessionList(razorpay_payment_id,
					razorpay_order_id);
			String schoolkey = "" + sessionList.get(0).getSchoolId();
			String renewId = "" + sessionList.get(0).getRenewId();
			String newYearId = "" + sessionList.get(0).getYearId();
			savePaidBean.setYearId(newYearId);
			List<PaymentRequestBean> feePayTypeList = paymentGatewayService.savePaidFees(savePaidBean);
			List<SchoolDetailBean> schoolList = paymentGatewayService.getSchoolDetails(schoolkey);
			UserDetailsBean userDetailsBean = paymentGatewayService.getStudentDetails(renewId);
			session.setAttribute("schoolName", schoolList.get(0).getSchoolName());
			session.setAttribute("studentName", userDetailsBean.getFirstName() + " " + userDetailsBean.getLastName());
			session.setAttribute("mobileNo", userDetailsBean.getMobileNumber());
			paymentGatewayService.sendSuccessMessage(renewId, userDetailsBean.getMobileNumber(), schoolkey,
					schoolList.get(0).getSchoolName(), userDetailsBean.getFirstName());
			paymentGatewayService.sendSuccessmail(razorpay_payment_id, renewId, userDetailsBean, schoolkey,
					schoolList.get(0).getSchoolName());
			session.setAttribute("schoolkeys", schoolkey);
			session.setAttribute("schoolId", schoolkey);
			session.setAttribute("renewId", renewId);
			session.setAttribute("standardId", "" + userDetailsBean.getStandardId());
			session.setAttribute("newYearId", newYearId);
			session.setAttribute("schoolId", schoolkey);
			session.setAttribute("studentId", renewId);
			if (feePayTypeList.size() > 0) {
				session.setAttribute("paymentflag", "1");
			} else {
				session.setAttribute("paymentflag", "0");
			}
		}
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("payment");
		return modelAndView;
	}

	@PostMapping(value = "/paymentprospectFee")
	public @ResponseBody ModelAndView getPaymentProspectResult(
			@RequestParam(value = "razorpay_payment_id", required = false) String razorpay_payment_id,
			@RequestParam(value = "razorpay_order_id", required = false) String razorpay_order_id,
			@RequestParam(value = "razorpay_signature", required = false) String razorpay_signature,
			HttpSession session, Model model) {
		System.out.println(razorpay_payment_id);
		System.out.println(razorpay_order_id);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@GetMapping(value = "/paymentGatewayReport")
	public @ResponseBody ResponseBodyBean<FeePayementGatewayBean> getPaymentGatewayReport(
			@RequestParam("renewStudentId") Integer renewStudentId, @RequestParam("schoolId") Integer schoolId,
			@RequestParam("yearId") Integer yearId, @RequestParam("date") String date) {
		List<FeePayementGatewayBean> overViewBean = new ArrayList<>();
		try {
			overViewBean = paymentGatewayService.getPaymentGatewayReport(renewStudentId, schoolId, yearId, date);
			if (CollectionUtils.isNotEmpty(overViewBean)) {
				return new ResponseBodyBean<FeePayementGatewayBean>("200", HttpStatus.OK, overViewBean);
			} else {
				return new ResponseBodyBean<FeePayementGatewayBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeePayementGatewayBean>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
	}

	@PostMapping(value = "/saveTemp", consumes = "application/json")
	public @ResponseBody ResponseBodyBean<FeeBean> saveTemporaryFees(@RequestBody PaymentRequestBean paymentRequestBean,
			HttpSession session) {
		try {
			List<FeeBean> feeBean = paymentGatewayService.saveTemporaryFees(paymentRequestBean);
			if (feeBean != null) {
				session.setAttribute("feeBeanList", feeBean);
				return new ResponseBodyBean<FeeBean>("200", HttpStatus.OK, feeBean, "Ok");
			} else {
				return new ResponseBodyBean<FeeBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>(),
						"Payment Cant't be done. Please contact Administration");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeeBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/saveTempForProspectFee", consumes = "application/json")
	public @ResponseBody ResponseBodyBean<FeeBean> saveTempForProspectFee(
			@RequestBody PaymentRequestBean paymentRequestBean, HttpSession session) {
		try {
			List<FeeBean> feeBean = paymentGatewayService.saveTempForProspectFee(paymentRequestBean);
			if (feeBean != null) {
				session.setAttribute("feeBeanList", feeBean);
				return new ResponseBodyBean<FeeBean>("200", HttpStatus.OK, feeBean, "Ok");
			} else {
				return new ResponseBodyBean<FeeBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>(),
						"Payment Cant't be done. Please contact Administration");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeeBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/payTypeList")
	public @ResponseBody ResponseBodyBean<FeePayTypeBean> getPayTypeList(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("amount") Double amount) {
		try {
			List<FeePayTypeBean> feePayTypeList = paymentGatewayService.getPayTypeList(schoolId, amount);
			if (CollectionUtils.isNotEmpty(feePayTypeList)) {
				return new ResponseBodyBean<FeePayTypeBean>("200", HttpStatus.OK, feePayTypeList);
			} else {
				return new ResponseBodyBean<FeePayTypeBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeePayTypeBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/transactions")
	public @ResponseBody ResponseBodyBean<PaymentRequestBean> getTransactions(
			@RequestParam("studentFeeId") Integer studentFeeId, @RequestParam("language") String language) {
		try {
			List<PaymentRequestBean> feePayTypeList = paymentGatewayService.getTransactions(studentFeeId, language);
			if (CollectionUtils.isNotEmpty(feePayTypeList)) {
				return new ResponseBodyBean<PaymentRequestBean>("200", HttpStatus.OK, feePayTypeList);
			} else {
				return new ResponseBodyBean<PaymentRequestBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<PaymentRequestBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PostMapping(value = "/savePaidFees")
	public @ResponseBody ResponseBodyBean<PaymentRequestBean> savePaidFees(@RequestBody SavePaidBean savePaidBean) {
		try {
			System.out.println("schoolId"+savePaidBean.getSchoolId());
			List<PaymentRequestBean> feePayTypeList = paymentGatewayService.savePaidFees(savePaidBean);
			if (CollectionUtils.isNotEmpty(feePayTypeList)) {
				return new ResponseBodyBean<PaymentRequestBean>("200", HttpStatus.OK, feePayTypeList);
			} else {
				return new ResponseBodyBean<PaymentRequestBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<PaymentRequestBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/orders")
	public @ResponseBody ResponseBodyBean<OrderBean> getOrders(@RequestParam("orderId") String orderId) {
		try {
			List<OrderBean> feePayTypeList = paymentGatewayService.getOrders(orderId);
			if (CollectionUtils.isNotEmpty(feePayTypeList)) {
				return new ResponseBodyBean<OrderBean>("200", HttpStatus.OK, feePayTypeList);
			} else {
				return new ResponseBodyBean<OrderBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<OrderBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/paymentSetting")
	public @ResponseBody ResponseBodyBean<PaymentSettingBean> getPaymentSetting(
			@RequestParam("schoolId") String schoolId, @RequestParam("yearId") String yearId,
			@RequestParam("standardId") String standardId) {
		try {
			List<PaymentSettingBean> feePayTypeList = paymentGatewayService.getPaymentSetting(schoolId, yearId,
					standardId);
			if (CollectionUtils.isNotEmpty(feePayTypeList)) {
				return new ResponseBodyBean<PaymentSettingBean>("200", HttpStatus.OK, feePayTypeList);
			} else {
				return new ResponseBodyBean<PaymentSettingBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<PaymentSettingBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/FeeId")
	public @ResponseBody ResponseBodyBean<FeeIdBean> getStudentFeeId(
			@RequestParam("renewStudentId") Integer renewStudentId) {
		try {
			List<FeeIdBean> feePayTypeList = paymentGatewayService.getStudentFeeId(renewStudentId);
			if (CollectionUtils.isNotEmpty(feePayTypeList)) {
				return new ResponseBodyBean<FeeIdBean>("200", HttpStatus.OK, feePayTypeList);
			} else {
				return new ResponseBodyBean<FeeIdBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeeIdBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@PutMapping(value = "/paymentStatus")
	@ResponseBody
	public ResponseBodyBean<Integer> updatePaymentStatus(@RequestParam(value = "schoolId") Integer schoolId,
			@RequestParam(value = "processId") String processId, @RequestParam(value = "statusCode") String statusCode,
			@RequestParam(value = "status") String status,
			@RequestParam(value = "statusMessage") String statusMessage) {
		try {
			Integer count = paymentGatewayService.updatePaymentStatus(schoolId, processId, statusCode, status,
					statusMessage);
			if (count != null) {
				return new ResponseBodyBean<Integer>("200", HttpStatus.OK, count);
			} else {
				return new ResponseBodyBean<Integer>("404", HttpStatus.NOT_FOUND, 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500", HttpStatus.INTERNAL_SERVER_ERROR, 0);
		}
	}

	@GetMapping(value = "/getFeesForYearAndStandard")
	@ResponseBody
	public ResponseBodyBean<Double> getFeesForYearAndStandard(@RequestParam(value = "schoolId") String schoolId,
			@RequestParam(value = "standardId") String standardId, @RequestParam(value = "yearId") String yearId) {
		try {
			Double count = paymentGatewayService.getFeesForYearAndStandard(Integer.parseInt(schoolId), standardId,
					yearId);
			if (count != null) {
				return new ResponseBodyBean<Double>("200", HttpStatus.OK, count);
			} else {
				return new ResponseBodyBean<Double>("404", HttpStatus.NOT_FOUND, 0.0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Double>("500", HttpStatus.INTERNAL_SERVER_ERROR, 0.0);
		}
	}

	@GetMapping(value = "/showPaymentButton")
	@ResponseBody
	public ResponseBodyBean<Boolean> showPaymentButton(@RequestParam(value = "schoolId") String schoolId,
			@RequestParam(value = "studentId") String studentId) {
		try {
			Boolean count = paymentGatewayService.getFeesSettingFlag(Integer.parseInt(schoolId),
					Integer.parseInt(studentId));
			if (count != null) {
				return new ResponseBodyBean<Boolean>("200", HttpStatus.OK, count);
			} else {
				return new ResponseBodyBean<Boolean>("404", HttpStatus.NOT_FOUND, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Boolean>("500", HttpStatus.INTERNAL_SERVER_ERROR, false);
		}
	}

	@GetMapping(value = "/prospectFee")
	@ResponseBody
	public ResponseBodyBean<FeeProspectBean> getProspectFee(@RequestParam("schoolId") Integer schoolId,
			@RequestParam("yearId") Integer yearId, @RequestParam("standardId") Integer standardId,
			@RequestParam("mobileNo") String mobileNo, HttpSession session) {
		FeeProspectBean feeProspectBean = new FeeProspectBean();
		System.out.println(yearId);
		try {
			feeProspectBean = paymentGatewayService.getProspectFee(schoolId, yearId, standardId);
			if (feeProspectBean != null) {
				DummyRegistrationModel dummyRegistrationModel = dummyRegistrationRepository.findByMobileNo(mobileNo);
				dummyRegistrationModel.setTotalPaidFee(feeProspectBean.getAllTotalFee());
				dummyRegistrationRepository.save(dummyRegistrationModel);
				return new ResponseBodyBean<FeeProspectBean>("200", HttpStatus.OK, feeProspectBean);
			} else {
				return new ResponseBodyBean<FeeProspectBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeeProspectBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/registrationSetting")
	@ResponseBody
	public ResponseBodyBean<FeeProspectBean> getRegistrationSetting(@RequestParam("schoolId") Integer schoolId,
			HttpSession session) {
		System.out.println(schoolId);
		FeeProspectBean feeProspectBean = new FeeProspectBean();
		try {
			feeProspectBean = paymentGatewayService.getRegistrationSetting(schoolId);
			if (feeProspectBean != null) {
				return new ResponseBodyBean<FeeProspectBean>("200", HttpStatus.OK, feeProspectBean);
			} else {
				return new ResponseBodyBean<FeeProspectBean>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FeeProspectBean>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
	}

	@GetMapping(value = "/registrationPaymentpage")
	@ResponseBody
	public Boolean saveNewRegistration(Model model, @RequestParam("newRegName") String firstName,
			@RequestParam("yearId") String yearId, @RequestParam("standardId") String standardId,
			@RequestParam("passwordNewReg") String passwordNewReg,
			@RequestParam("newRegUserName") String newRegUserName, @RequestParam("schoolId") String schoolId,
			@RequestParam("formNo") String formNo, @RequestParam("missMr") String missMr,
			@RequestParam("secondName") String secondName, @RequestParam("lastName") String lastName,
			@RequestParam("feeSettingAvailable") String feeSettingAvailable, RedirectAttributes redirectAttributes,
			HttpSession session) {
		DummyRegistrationModel dummyRegistrationModel = new DummyRegistrationModel();
		dummyRegistrationModel.setMobileNo(newRegUserName);
		dummyRegistrationModel.setFirstName(firstName);
		dummyRegistrationModel.setYearId(Integer.parseInt(yearId));
		dummyRegistrationModel.setStandardId(Integer.parseInt(standardId));
		dummyRegistrationModel.setPassword(passwordNewReg);
		dummyRegistrationModel.setSchoolId(Integer.parseInt(schoolId));
		dummyRegistrationModel.setFormNo(formNo);
		dummyRegistrationModel.setMrormisss(missMr);
		dummyRegistrationModel.setMiddleName(secondName);
		dummyRegistrationModel.setLastName(lastName);
		dummyRegistrationRepository.save(dummyRegistrationModel);
		return true;
	}

	@PostMapping(value = "/registrationPayment")
	public String getRegistrationPayment(
			@RequestParam(value = "razorpay_payment_id", required = false) String razorpay_payment_id,
			@RequestParam(value = "razorpay_order_id", required = false) String razorpay_order_id,
			@RequestParam(value = "razorpay_signature", required = false) String razorpay_signature,
			HttpSession session, Model model) {
		session.setAttribute("successflag", "false");
		if (razorpay_payment_id == null && razorpay_order_id == null) {
			session.setAttribute("successflag", "false");
			return "login";
		} else {

			try {
				DummyRegistrationModel dummyRegistrationModel = dummyRegistrationRepository
						.findByOrderId(razorpay_order_id);
				String newRegName = dummyRegistrationModel.getFirstName();
				String yearId = "" + dummyRegistrationModel.getYearId();
				String standardId = "" + dummyRegistrationModel.getStandardId();
				String passwordNewReg = dummyRegistrationModel.getPassword();
				String newRegUserName = dummyRegistrationModel.getMobileNo();
				String schoolId = "" + dummyRegistrationModel.getSchoolId();
				String formNo = dummyRegistrationModel.getFormNo();
				String missMr = dummyRegistrationModel.getMrormisss();
				String secondName = dummyRegistrationModel.getMiddleName();
				String lastName = dummyRegistrationModel.getLastName();
				StudentMasterModel studentMasterModel = studentAdmissionService.saveNewRegistration(newRegName, yearId,
						standardId, passwordNewReg, newRegUserName, schoolId, formNo, missMr, secondName, lastName,
						session);

				Integer renewId = studentAdmissionService.getrenewId(studentMasterModel.getStudentId(),
						Integer.valueOf(yearId));

				String fullName = missMr + newRegName + secondName + lastName;
				List<FeeProspectBean> prospectFeeList = feePaymentGatewayRepository.getProspectFee(
						Integer.valueOf(schoolId), Integer.valueOf(yearId), Integer.valueOf(standardId));
				SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
				schoolMasterModel.setSchoolid(Integer.valueOf(schoolId));

				YearMasterModel yearMasterModel = new YearMasterModel();
				yearMasterModel.setYearId(Integer.valueOf(yearId));

				StandardMasterModel standardMasterModel = new StandardMasterModel();
				standardMasterModel.setStandardId(Integer.valueOf(standardId));

				StudentStandardRenewModel studentStandardRenewModel = new StudentStandardRenewModel();
				studentStandardRenewModel.setRenewStudentId(renewId);

				SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(Integer.valueOf(schoolId));
				String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());

				for (int i = 0; i < prospectFeeList.size(); i++) {
					FeeProspectBean feeProspectBean = prospectFeeList.get(i);
					FeePaidProspectFeesModel feePaidProspectFeesModel = new FeePaidProspectFeesModel();
					feePaidProspectFeesModel.setStudentName(fullName);
					feePaidProspectFeesModel.setStudentStandardRenewModel(studentStandardRenewModel);
					feePaidProspectFeesModel.setSchoolMasterModel(schoolMasterModel);
					feePaidProspectFeesModel.setYearMasterModel(yearMasterModel);
					feePaidProspectFeesModel.setStandardMasterModel(standardMasterModel);
					feePaidProspectFeesModel.setCGST(feeProspectBean.getCgst());
					feePaidProspectFeesModel.setSGST(feeProspectBean.getSgst());
					feePaidProspectFeesModel.setProspectFee(feeProspectBean.getProspectFee());
					feePaidProspectFeesModel.setTotalPaidFee(
							feeProspectBean.getCgst() + feeProspectBean.getSgst() + feeProspectBean.getProspectFee());
					feePaidProspectFeesModel.setTranscation_id(razorpay_payment_id);
					feePaidProspectFeesModel.setOrder_id(razorpay_order_id);
					feePaidProspectFeesModel.setHeadId(feeProspectBean.getHeadId());
					feePaidProspectFeesModel.setHeadName(feeProspectBean.getHeadName());

					String incrementedId1 = "" + feePaidProspectFeeRepository.getMaxId(Integer.valueOf(schoolId));
					String prospectfeeId = UTFConversionUtil.generatePrimaryKey(globalDbSchoolKey, incrementedId1);

					feePaidProspectFeesModel.setFeePaidProspectFeeId(Integer.valueOf(prospectfeeId));
					feePaidProspectFeeRepository.save(feePaidProspectFeesModel);
				}

//				paymentGatewayService.setStudentFee(yearId, standardId, schoolId, studentMasterModel.getStudentId(),
//						studentMasterModel.getUserId().getAppUserRoleId());
				session.setAttribute("studentId", studentMasterModel.getStudentId());
				session.setAttribute("welcomeName", newRegUserName);
				session.setAttribute("yearId", yearId);
				session.setAttribute("standardId", standardId);
				session.setAttribute("schoolId", schoolId);
				session.setAttribute("successflag", "true");
				return "login1";
			} catch (Exception e) {
				return "login1";
			}
		}
	}
	
	
	@GetMapping (value = "/nextFeeId")
	public @ResponseBody String getNextSchoolId(@RequestParam(value = "schoolId", required = false) Integer schoolId) {
		try {
			String incrementedId = "" + feeRepository.getMaxPaidId(schoolId);
			String schoolKey = schoolMasterRepository.findBySchoolid(schoolId).getSchoolKey();
			String globalDbSchoolKey = "1".concat("" + schoolKey);
			String paidId = Utillity.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			return paidId;
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}


}
