package com.ingenio.admission.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.admission.bean.FCMUpdateBean;
import com.ingenio.admission.bean.NotificationBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.service.FCMPushNotificationService;

@RestController
@CrossOrigin
public class FCMPushNotificationController {


	@Autowired 
	private FCMPushNotificationService notificationService;
	
	@GetMapping(value="/pushNotification")
	@Produces("application/json")
	String pushNotification(@RequestParam("tokenList") List<String> deviceTokenList,
			 @RequestParam("title") String title,@RequestParam("message") String message){
		String status="";
		try {
			status=notificationService.pushNotification(deviceTokenList,title,message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	@GetMapping(value="/updateFCMToken")
//	@Produces("application/json")
	ResponseBodyBean<Integer> updateFCMToken(@RequestParam("token") String token,
			@RequestParam("mobileNo") String mobileNo,@RequestParam(value="deviceType",required = false,defaultValue="2") String deviceType,
			@RequestParam(value="schoolId",required = false) Integer schoolId){
		Integer status=0;
		try {
				status=notificationService.updateFCMToken(token,mobileNo,deviceType,schoolId);
				if(status !=0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,status);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,status);
				}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,status);
		}
	}
	
	@GetMapping(value="/getAllNotifications")
	@Produces("application/json")
	ResponseBodyBean<NotificationBean> getAllNotifications(@RequestParam("renewId") Integer renewId,
			@RequestParam("role") String role,@RequestParam(value="offset",required = false,defaultValue = "0") Integer offset) {
		try {
			List<NotificationBean> notificationList=notificationService.getAllNotifications(renewId,role,offset);
			if(CollectionUtils.isNotEmpty(notificationList)) {
				return new ResponseBodyBean<NotificationBean>("200",HttpStatus.OK,notificationList);
			}
			else {
				return new ResponseBodyBean<NotificationBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<NotificationBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value="/updateFCMToken1")
//	@Produces("application/json")
	ResponseBodyBean<Integer> updateFCMToken1(@RequestBody FCMUpdateBean fcmUpdateBean){
		Integer status=0;
		try {
				status=notificationService.updateFCMToken1(fcmUpdateBean);
				if(status !=0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,status);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,status);
				}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,status);
		}
	}
	
}
