package com.ingenio.admission.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.service.AdmissionSettingService;

@Controller
@CrossOrigin
public class AdmissionSettingController {
	
	@Autowired 
	private AdmissionSettingService admissionSettingService;
	
	@GetMapping(value="/getTabsName")
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> getTabsName(@RequestParam("schoolId") Integer schoolId){
		List<AdmissionSettingBean> legendList = new ArrayList<AdmissionSettingBean>();
		try {
			legendList=admissionSettingService.getTabsName(schoolId);
			if(CollectionUtils.isNotEmpty(legendList)) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,legendList);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value = "/updateTabsName")
	public @ResponseBody ResponseBodyBean<AdmissionSettingBean> updateTabsName(@RequestParam("tabId") Integer tabId,@RequestParam("tabName") String tabName,@RequestParam("schoolId") Integer schoolId) {
		try {
			Integer count=admissionSettingService.updateTabsName(tabName,schoolId,tabId);
			if(count>0) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,new ArrayList<>());
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			
		}
	}
	
	@GetMapping(value="/getAllFeilds")
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> getAllFeilds(@RequestParam("tabId") Integer tabId,@RequestParam("schoolId") Integer schoolId,@RequestParam("legendId") Integer legendId){
		List<AdmissionSettingBean> allFeildsList = new ArrayList<AdmissionSettingBean>();
		try {
			allFeildsList=admissionSettingService.getAllFeilds(tabId,schoolId,legendId);
			if(CollectionUtils.isNotEmpty(allFeildsList)) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,allFeildsList);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value = "/saveLegendName")
	@ResponseBody
	public ResponseBodyBean<Integer> saveLegendName(@RequestParam("tabId") Integer tabId,@RequestParam("legendName") String legendName,@RequestParam("schoolId") Integer schoolId) {
		Integer id = null;
		try {
			 id=admissionSettingService.saveLegendName(legendName,schoolId,tabId);
		 	if(id>0) {
			 return new ResponseBodyBean<Integer>("200",HttpStatus.OK,id);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
		}
	}
	
	@GetMapping(value="/getlegendName")
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> getlegendName(@RequestParam("tabId") Integer tabId,@RequestParam("schoolId") Integer schoolId){
		List<AdmissionSettingBean> alllegendNameList = new ArrayList<AdmissionSettingBean>();
		try {
			alllegendNameList=admissionSettingService.getlegendName(tabId,schoolId);
			if(alllegendNameList.size()>0) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,alllegendNameList);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value="/deleteLegendName")
	public @ResponseBody ResponseBodyBean<Integer> deleteLegendName(@RequestParam("tabId") Integer tabId,@RequestParam("legendId") Integer legendId,@RequestParam("schoolId") Integer schoolId){
		try {
			Integer count=admissionSettingService.deleteLegendName(tabId,legendId,schoolId);
			if(count>0) {
				 return new ResponseBodyBean<Integer>("200",HttpStatus.OK,count);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
		}
	}
	
	@PostMapping(value="/updateField")
	public @ResponseBody RedirectView updateField(@ModelAttribute("admissionSettingBean") AdmissionSettingBean admissionSettingBean,HttpSession session){
		try {
			admissionSettingService.updateField(admissionSettingBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new RedirectView("AdmissionFormSetting.jsp?schoolkey="+admissionSettingBean.getSchoolId());
	}

	
	/*@PostMapping(value = "/saveIsYearly")
	public @ResponseBody ResponseBodyBean<AdmissionSettingBean> saveIsYearly(@RequestParam("isYearlyDynamicFieldVal") String[] isYearlyDynamicFieldVal,@RequestParam("schoolId") Integer schoolId) {
		try {
				admissionSettingService.updateisYearlyField(isYearlyDynamicFieldVal,schoolId);
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,new ArrayList<>());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}*/
	
	@GetMapping(value="/getOldDynamicFields")
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> getOldDynamicFields(@RequestParam("schoolId") Integer schoolId){
		List<AdmissionSettingBean> oldDynamicFieldList = new ArrayList<AdmissionSettingBean>();
		try {
			oldDynamicFieldList=admissionSettingService.getOldDynamicFields(schoolId);
			if(CollectionUtils.isNotEmpty(oldDynamicFieldList)) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,oldDynamicFieldList);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value = "/updateLegendName")
	public @ResponseBody ResponseBodyBean<AdmissionSettingBean> updateLegendName(@RequestParam("legendId") Integer legendId,@RequestParam("legendName") String legendName,@RequestParam("schoolId") Integer schoolId) {
		try {
			Integer count=admissionSettingService.updateLegendName(legendName,schoolId,legendId);
			if(count>0) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,new ArrayList<>());
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value="/saveOldFieldValueName")
	@ResponseBody ResponseBodyBean<Integer> saveOldFieldValueName(@RequestParam("tabId") Integer tabId,@RequestParam("schoolId") Integer schoolId,@RequestParam("oldFieldType") Integer oldFieldType,
			@RequestParam("oldfieldName") String oldfieldName){
		Integer count=0;
		try {
			count=admissionSettingService.saveOldFieldValueName(tabId,schoolId,oldFieldType,oldfieldName);
			if(count>0) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,count);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,count);
		}
	}
	
	@GetMapping(value="/getDynamicFieldComboValue")
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> getDynamicFieldComboValue(@RequestParam("dynamicField") Integer dynamicField,@RequestParam("schoolId") Integer schoolId){
		List<AdmissionSettingBean> dynamicFieldList = new ArrayList<AdmissionSettingBean>();
		try {
			dynamicFieldList=admissionSettingService.getDynamicFieldComboValue(dynamicField,schoolId);
			if(CollectionUtils.isNotEmpty(dynamicFieldList)) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,dynamicFieldList);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value="/setOldDynamicFieldValue")  
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> setOldDynamicFieldValue(@RequestParam("oldDynamicFieldId") Integer oldDynamicFieldId,@RequestParam("schoolId") Integer schoolId,@RequestParam("fieldTypeId") String fieldTypeId){
		List<AdmissionSettingBean> oldDynamicFieldList = new ArrayList<AdmissionSettingBean>();
		try {
			oldDynamicFieldList=admissionSettingService.setOldDynamicFieldValue(oldDynamicFieldId,schoolId,fieldTypeId);
			if(CollectionUtils.isNotEmpty(oldDynamicFieldList)) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,oldDynamicFieldList);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/getOldDynamicFieldValueId")
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> getOldDynamicFieldValueId(@RequestParam("fieldName") String fieldName,@RequestParam("schoolId") Integer schoolId){
		List<AdmissionSettingBean> oldDynamicFieldList = new ArrayList<AdmissionSettingBean>();
		try {
			oldDynamicFieldList=admissionSettingService.getOldDynamicFieldValueId(fieldName,schoolId);
			if(CollectionUtils.isNotEmpty(oldDynamicFieldList)) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,oldDynamicFieldList);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@PostMapping(value = "/updateTabsDisplayOrNotFlag")
	public @ResponseBody ResponseBodyBean<AdmissionSettingBean> updateTabsDisplayOrNotFlag(@RequestParam("tabId") Integer tabId,@RequestParam("schoolId") Integer schoolId,@RequestParam("isTabDisplayOrNot") String isTabDisplayOrNot) {
		try {
			Integer count=admissionSettingService.updateTabsDisplayOrNotFlag(schoolId,tabId,isTabDisplayOrNot);
			if(count>0) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,new ArrayList<>());
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/checkStaticFieldIsDisplayOrNot")
	@ResponseBody ResponseBodyBean<AdmissionSettingBean> checkStaticFieldIsDisplayOrNot(@RequestParam("staticFieldName") String staticFieldName,@RequestParam("schoolId") Integer schoolId){
		List<AdmissionSettingBean> fieldIsDisplayOrNot = new ArrayList<AdmissionSettingBean>();
		try {
			fieldIsDisplayOrNot=admissionSettingService.checkStaticFieldIsDisplayOrNot(staticFieldName,schoolId);
			if(CollectionUtils.isNotEmpty(fieldIsDisplayOrNot)) {
				return new ResponseBodyBean<AdmissionSettingBean>("200",HttpStatus.OK,fieldIsDisplayOrNot);
			}
			else {
				return new ResponseBodyBean<AdmissionSettingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AdmissionSettingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
}
