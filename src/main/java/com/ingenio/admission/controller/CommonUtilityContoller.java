package com.ingenio.admission.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.admission.bean.AdmissionSettingBean;
import com.ingenio.admission.bean.BannerImages;
import com.ingenio.admission.bean.CheckBalanceBean;
import com.ingenio.admission.bean.CommonUtilityBean;
import com.ingenio.admission.bean.FrontPageImagesBean;
import com.ingenio.admission.bean.ResponseBodyBean;
import com.ingenio.admission.bean.SchoolMappingBean;
import com.ingenio.admission.model.AttachmentMasterModel;
import com.ingenio.admission.model.CasteMasterModel;
import com.ingenio.admission.model.CategoryMasterModel;
import com.ingenio.admission.model.DivisionMasterModel;
import com.ingenio.admission.model.GRBookNameModel;
import com.ingenio.admission.model.MinorityMasterModel;
import com.ingenio.admission.model.ReligionMasterModel;
import com.ingenio.admission.model.StandardMasterModel;
import com.ingenio.admission.model.YearMasterModel;
import com.ingenio.admission.service.CommonUtilityService;
import com.ingenio.admission.service.impl.CommonUtilittyServiceImpl;

@RestController
@CrossOrigin
public class CommonUtilityContoller {
	
	@Autowired 
	private CommonUtilityService commonUtilityService;
	@Autowired 
	private CommonUtilittyServiceImpl commonUtilittyServiceImpl;
	
	@GetMapping(value = "getAcademicYearCombo")
	public ResponseBodyBean<CommonUtilityBean> getAcademicYearCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<YearMasterModel> yearMasterModelList = commonUtilityService.getAcademicYearCombo(schoolId);
			if(CollectionUtils.isNotEmpty(yearMasterModelList)) {
				for(YearMasterModel yearMasterModel : yearMasterModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setYearId(yearMasterModel.getYearId());
					commonUtilityBean.setYearName(yearMasterModel.getYear());
					int currentAcademicYear=commonUtilittyServiceImpl.getCurrentYear(schoolId);
					if(yearMasterModel.getYearId()==currentAcademicYear)
					{
						commonUtilityBean.setCurrentAcademicYear("1");	
					}else {
						commonUtilityBean.setCurrentAcademicYear("0");
					}
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	@GetMapping(value = "getStandardCombo")
	public ResponseBodyBean<CommonUtilityBean> getStandardCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<StandardMasterModel> standardMasterModelList = commonUtilityService.getStandardCombo(schoolId);
			if(CollectionUtils.isNotEmpty(standardMasterModelList)) {
				for(StandardMasterModel standardMasterModel : standardMasterModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setStandardId(""+standardMasterModel.getStandardId());
					commonUtilityBean.setStandardName(standardMasterModel.getStandardName());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}

	
	@GetMapping(value = "getCategoryCombo")
	public ResponseBodyBean<CommonUtilityBean> getCategoryCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<CategoryMasterModel> categoryMasterModelList = commonUtilityService.getCategoryCombo(schoolId);
			if(CollectionUtils.isNotEmpty(categoryMasterModelList)) {
				for(CategoryMasterModel categoryMasterModel : categoryMasterModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setCategoryId(""+categoryMasterModel.getCategoryId());
					commonUtilityBean.setCategoryName(categoryMasterModel.getCategoryName());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	
	

	@GetMapping(value = "getCasteCombo")
	public ResponseBodyBean<CommonUtilityBean> getCasteCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<CasteMasterModel> casteMasterModelList = commonUtilityService.getCasteCombo(schoolId);
			if(CollectionUtils.isNotEmpty(casteMasterModelList)) {
				for(CasteMasterModel casteMasterModel : casteMasterModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setCasteId(""+casteMasterModel.getCasteId());
					commonUtilityBean.setCasteName(casteMasterModel.getCastName());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	
	
	@GetMapping(value = "getMinorityCombo")
	public ResponseBodyBean<CommonUtilityBean> getMinorityCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<MinorityMasterModel> minorityMasterModelList = commonUtilityService.getMinorityCombo(schoolId);
			if(CollectionUtils.isNotEmpty(minorityMasterModelList)) {
				for(MinorityMasterModel minorityMasterModel : minorityMasterModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setMinorityId(""+minorityMasterModel.getMinorityId());
					commonUtilityBean.setMinorityType(minorityMasterModel.getMinorityType());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	
	
	@GetMapping(value = "getReligionCombo")
	public ResponseBodyBean<CommonUtilityBean> getReligionCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<ReligionMasterModel> religionMasterModelList = commonUtilityService.getReligionCombo(schoolId);
			if(CollectionUtils.isNotEmpty(religionMasterModelList)) {
				for(ReligionMasterModel religionMasterModel : religionMasterModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setReligionId(""+religionMasterModel.getReligionId());
					commonUtilityBean.setReligionName(religionMasterModel.getReligionName());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	
	
	@GetMapping(value = "getDivisionCombo")
	public ResponseBodyBean<CommonUtilityBean> getDivisionCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<DivisionMasterModel> divisionMasterModelList = commonUtilityService.getDivisionCombo(schoolId);
			if(CollectionUtils.isNotEmpty(divisionMasterModelList)) {
				for(DivisionMasterModel divisionMasterModel : divisionMasterModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setDivisionId(""+divisionMasterModel.getDivisionId());
					commonUtilityBean.setDivisionName(divisionMasterModel.getDivisionName());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	@GetMapping(value = "getGRCombo")
	public ResponseBodyBean<CommonUtilityBean> getGRCombo(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<GRBookNameModel> grBookNameModelList = commonUtilityService.getGRCombo(schoolId);
			if(CollectionUtils.isNotEmpty(grBookNameModelList)) {
				for(GRBookNameModel grBookModel : grBookNameModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setGrBookId(""+grBookModel.getGrBookId());
					commonUtilityBean.setGrBookName(grBookModel.getGrBookName());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	@GetMapping(value = "getStudentAttachments")
	public ResponseBodyBean<CommonUtilityBean> getStudentAttachments(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<AttachmentMasterModel>attachmentModelList = commonUtilityService.getStudentAttachments(schoolId);
			if(CollectionUtils.isNotEmpty(attachmentModelList)) {
				for(AttachmentMasterModel attachmentModel : attachmentModelList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setAttachmentId(""+attachmentModel.getAttachmentMasterId());
					commonUtilityBean.setAttachmentName(attachmentModel.getAttachment());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	@GetMapping(value = "getLetterHead")
	public ResponseBodyBean<CommonUtilityBean> getLetterHead(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			commonUtilityBeanList=commonUtilityService.getLetterHeadImg(schoolId);
			if(CollectionUtils.isNotEmpty(commonUtilityBeanList)) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else
			{
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	
	@GetMapping(value = "/getSmsBalance")
	public ResponseBodyBean<CheckBalanceBean> getSmsBalance(@RequestParam("schoolId") Integer schoolId) {
		ResponseBodyBean<CheckBalanceBean> responseBodyBean = new ResponseBodyBean<>();
		List<CheckBalanceBean> smsBalanceList =new ArrayList<CheckBalanceBean>();
		try {
			smsBalanceList = commonUtilityService.getSmsBalance(schoolId);
			if (CollectionUtils.isNotEmpty(smsBalanceList)) {
				responseBodyBean = new ResponseBodyBean<>("200", HttpStatus.OK, smsBalanceList);
			} else {
				responseBodyBean = new ResponseBodyBean<>("404", HttpStatus.NOT_FOUND, new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<>("500", HttpStatus.INTERNAL_SERVER_ERROR, new ArrayList<>());
		}
		return responseBodyBean;
	}
	
	@GetMapping(value = "getAssignedSubjects")
	public ResponseBodyBean<CommonUtilityBean> getAssignedSubjects(@RequestParam("schoolId") Integer schoolId,@RequestParam("yearId") Integer yearId,@RequestParam("standardId") Integer standardId) {
		ResponseBodyBean<CommonUtilityBean> responseBodyBean = new ResponseBodyBean<CommonUtilityBean>();
		List<CommonUtilityBean> commonUtilityBeanList = new ArrayList<>();
		try {
			
			List<AdmissionSettingBean> admissionSettingBeanList = commonUtilityService.getAssignedSubjects(schoolId,yearId,standardId);
			if(CollectionUtils.isNotEmpty(admissionSettingBeanList)) {
				for(AdmissionSettingBean admissionSettingBean : admissionSettingBeanList) {
					CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
					commonUtilityBean.setSubjectCategoryId(admissionSettingBean.getSubjectCategoryId());
					commonUtilityBean.setSubjectCategoryName(admissionSettingBean.getSubjectCategoryName());
					commonUtilityBean.setSubjectId(admissionSettingBean.getSubjectId());
					commonUtilityBean.setSubjectName(admissionSettingBean.getSubjectName());
					commonUtilityBeanList.add(commonUtilityBean);
				}
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
			}
			else {
				responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
			
		} catch (Exception e) {
			responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			e.printStackTrace();
		}
		return responseBodyBean;
	}
	
	
	@GetMapping(value="/allBannerList")
	public @ResponseBody ResponseBodyBean<FrontPageImagesBean> getAllBannerList(@RequestParam("schoolId") Integer schoolId){
		List<BannerImages> bannerImagesList = new ArrayList<BannerImages>();
		FrontPageImagesBean frontPageImagesBean =new FrontPageImagesBean();
		try {
			frontPageImagesBean=commonUtilityService.getAllBannerList(schoolId);
			
			if(frontPageImagesBean!=null) {
				return new ResponseBodyBean<FrontPageImagesBean>("200",HttpStatus.OK,frontPageImagesBean);	
			}
			else {
				return new ResponseBodyBean<FrontPageImagesBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<FrontPageImagesBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
//	@GetMapping(value="/schoolMappingList")
//	public @ResponseBody ResponseBodyBean<SchoolMappingBean> getAllSchoolMappingList(@RequestParam("schoolId") Integer schoolId){
//		List<SchoolMappingBean> schoolMappingList = new ArrayList<SchoolMappingBean>();
//		try {
//			schoolMappingList=commonUtilityService.getAllSchoolMappingList(schoolId);
//			if(schoolMappingList.size()>0) {
//				return new ResponseBodyBean<SchoolMappingBean>("200",HttpStatus.OK,schoolMappingList);	
//			}
//			else {
//				return new ResponseBodyBean<SchoolMappingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseBodyBean<SchoolMappingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
//		}
//	}
	
	@GetMapping(value="/schoolMappingList")
	public @ResponseBody ResponseBodyBean<SchoolMappingBean> getAllSchoolMappingList(@RequestParam("schoolId") Integer schoolId, @RequestParam(required = false) Integer onlineAdmissionFlag){
		List<SchoolMappingBean> schoolMappingList = new ArrayList<SchoolMappingBean>();
		try {
			schoolMappingList=commonUtilityService.getAllSchoolMappingList(schoolId, onlineAdmissionFlag);
			if(schoolMappingList.size()>0) {
				return new ResponseBodyBean<SchoolMappingBean>("200",HttpStatus.OK,schoolMappingList);	
			}
			else {
				return new ResponseBodyBean<SchoolMappingBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SchoolMappingBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
}




//File dir = new File(f);
//if (!dir.exists()) {
//	dir.mkdirs();
//}
//List<BonafideTCImageModel> bonafideTCImageModelList = commonUtilityService.getLetterHead(schoolId);
//if(CollectionUtils.isNotEmpty(bonafideTCImageModelList)) {
//	for(BonafideTCImageModel attachmentModel : bonafideTCImageModelList) {
//		CommonUtilityBean commonUtilityBean = new CommonUtilityBean();
//		commonUtilityBean.setImageData(attachmentModel.getBonafiedImgs());
//		commonUtilityBean.setFlag(""+attachmentModel.getFlag());
//		commonUtilityBeanList.add(commonUtilityBean);
//		String filename="LetterHead"+schoolId+".png";
//		if(attachmentModel.getBonafiedImgs() !=null || attachmentModel.getBonafiedImgs().length !=0) {
//			File serverFile = new File(dir.getAbsolutePath()+ File.separator+filename);
//			if(!serverFile.exists() && !serverFile.isDirectory()) { 
//				//serverFile.delete();
//				FileOutputStream fout=new FileOutputStream(new File(f,filename));
//				fout.write(attachmentModel.getBonafiedImgs()); 
//				fout.close();
//			}
//		}
//	}
//	responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("200",HttpStatus.OK,commonUtilityBeanList);
//}
//else {
//	responseBodyBean = new ResponseBodyBean<CommonUtilityBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
//}
