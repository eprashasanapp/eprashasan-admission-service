package com.ingenio.admission.enums;

public enum DiscountLabels {
	PAY("pay"),
	FULLAMOUNT("Full Amount"),
	HALFAMOUNT("Half Amount"),
	MINIMUMRS("Minimum Rs."),
	BY("by"),
	TOAVAIL("to avail"),
	RS("Rs."),
	DISCOUNT("discount"),
	NODISCOUNT("No Discount"),
	PERCENT("%"),
	INR("INR");
	
	private final String discountLabel;
	
	private DiscountLabels(String discountLabel) {
        this.discountLabel = discountLabel;
    }

    public String getDiscountLabels() {
        return discountLabel;
    } 
}
