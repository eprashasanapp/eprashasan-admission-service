package com.ingenio.admission.enums;

public enum RestUrlConfigs {
	POST("POST"),
	GET("GET"),
	AUTHORIZATION("Authorization"),
	CONTENTTYPE("Content-Type"),
	APPLICATIONJSON("application/json");
	
	private final String restUrlConfigs;
	
	private RestUrlConfigs(String restUrlConfigs) {
        this.restUrlConfigs = restUrlConfigs;
    }

    public String getRestUrlConfigs() {
        return restUrlConfigs;
    }
}
