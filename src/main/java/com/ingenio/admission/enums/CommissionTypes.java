package com.ingenio.admission.enums;

public enum CommissionTypes {

	SCHOOLCOMMISSION("School Commisssion"),
	EPCOMMISSION("E-prashashan Commisssion"),
	TPCOMMISSION("Third Party Commission"),
	ROUTINGCHARGES("Routing Charges");

	private final String commissionTypes;
	
	private CommissionTypes(String commissionTypes) {
        this.commissionTypes = commissionTypes;
    }

    public String getCommissionTypes() {
        return commissionTypes;
    }
}
