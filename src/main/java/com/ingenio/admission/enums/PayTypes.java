package com.ingenio.admission.enums;

public enum PayTypes {

	DEBITCARD("Debit Card"),
	CREDITCARD("Credit Card"),
	NETBANKING("Net Banking"),
	UPI("UPI"),
	WALLET("Wallet");
	
	private final String payTypes;
	
	private PayTypes(String payTypes) {
        this.payTypes = payTypes;
    }

    public String getPayTypes() {
        return payTypes;
    }
}
