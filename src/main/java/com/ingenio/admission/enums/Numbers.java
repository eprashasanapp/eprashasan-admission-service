package com.ingenio.admission.enums;

public enum Numbers {
	
	Zero("0"),
	One("1"),
	Two("2");
	
	private final String number;
	
	private Numbers(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

}
