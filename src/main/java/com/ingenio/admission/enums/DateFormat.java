package com.ingenio.admission.enums;

public enum DateFormat {
	ddmmmyyyy("dd MMM yyyy"),
	ddmmyyyy("dd-MM-yyyy");
	
	private final String dateFormat;
	
	private DateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getDateFormat() {
        return dateFormat;
    } 
}
