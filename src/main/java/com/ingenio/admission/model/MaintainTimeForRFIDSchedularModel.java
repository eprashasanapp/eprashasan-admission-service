package com.ingenio.admission.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "maintain_Time_for_RFID_schedular")
public class MaintainTimeForRFIDSchedularModel {
	private Integer schedularTimeId;
	private SchoolMasterModel schoolMasterModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
    Date date = new Date(); 
	private Date Time = new java.sql.Timestamp(new java.util.Date().getTime());

	@Id
	@Column(name = "schedularTimeId", unique = true, nullable = false)
	public Integer getSchedularTimeId() {
		return schedularTimeId;
	}

	public void setSchedularTimeId(Integer schedularTimeId) {
		this.schedularTimeId = schedularTimeId;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolId", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	@Column(name = "Time")
	public Date getTime() {
		return Time;
	}

	public void setTime(Date time) {
		Time = time;
	}

	
	
	
}
