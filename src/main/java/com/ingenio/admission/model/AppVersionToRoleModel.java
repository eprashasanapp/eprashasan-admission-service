package com.ingenio.admission.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "app_version_torole")
public class AppVersionToRoleModel {

	private Integer appVersionToRoleId;
	private AppVersionMasterModel appVersionMasterModel;
	private String roleName;
	
	@Id
	@Column(name = "app_version_torole_id", nullable = false)
	public Integer getAppVersionToRoleId() {
		return appVersionToRoleId;
	}
	public void setAppVersionToRoleId(Integer appVersionToRoleId) {
		this.appVersionToRoleId = appVersionToRoleId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "versionID")
	public AppVersionMasterModel getAppVersionMasterModel() {
		return appVersionMasterModel;
	}
	public void setAppVersionMasterModel(AppVersionMasterModel appVersionMasterModel) {
		this.appVersionMasterModel = appVersionMasterModel;
	}
	public String getRoleName() {
		return roleName;
	}
	@Column(name="rolename ")
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
}
