package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@Table(name="sanstha_user_registration")

public class SansthaUserRegistrationModel {
	
	private Integer sansthaUserId;
	private String sansthaUserRegNo;
	private SansthaRegistrationModel sansthaRegistrationModel;
	private String initialName;
	private String firstName;
	private String secondName;
	private String lastName;
	private String birthDate;
	private String gender;
	private String contactNoSms;
	private String contactNo;
	private String emailId;
	private String address;
	private String city;
	private String state;
	private String pincode;
	private String country;
	private String joiningDate;
	private byte[] sansthaUserImg;
	private AppUserRoleModel AppUserRoleModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private AppUserRoleModel deleteBy;
	private Date delDate;
	private String isEdit = "0";
	private AppUserRoleModel editBy;
	private Date editDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
	public SansthaUserRegistrationModel() {
		super();
	}
	
	@Id
	@Column(name = "sansthaUserId", nullable = false)
	public Integer getSansthaUserId() {
		return sansthaUserId;
	}
	public void setSansthaUserId(Integer sansthaUserId) {
		this.sansthaUserId = sansthaUserId;
	}
	
	@Column(name="sansthaUserRegNo", length=50)
	public String getSansthaUserRegNo() {
		return sansthaUserRegNo;
	}
	public void setSansthaUserRegNo(String sansthaUserRegNo) {
		this.sansthaUserRegNo = sansthaUserRegNo;
	}
	
	@Column(name="initialName", length=10)
	public String getInitialName() {
		return initialName;
	}
	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}
	
	@Column(name="firstName", length=100)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="secondName", length=100)
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	
	@Column(name="lastName", length=100)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name="birthDate", length=10)
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	
	@Column(name="gender", length=10)
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(name="contactNoSms", length=20)
	public String getContactNoSms() {
		return contactNoSms;
	}
	public void setContactNoSms(String contactNoSms) {
		this.contactNoSms = contactNoSms;
	}
	
	@Column(name="contactNo", length=20)
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	
	@Column(name="emailId", length=50)
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	@Column(name="address", length=500)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name="city", length=50)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="state", length=50)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="pincode", length=10)
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	@Column(name="country", length=50)
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="joiningDate", length=10)
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	
	@Column(name="sansthaUserImg")
	public byte[] getSansthaUserImg() {
		return sansthaUserImg;
	}
	public void setSansthaUserImg(byte[] sansthaUserImg) {
		this.sansthaUserImg = sansthaUserImg;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return AppUserRoleModel;
	}
	public void setAppUserRoleModel(AppUserRoleModel AppUserRoleModel) {
		this.AppUserRoleModel = AppUserRoleModel;
	}
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "religionId")
	public Religion getReligion() {
		return religion;
	}
	public void setReligion(Religion religion) {
		this.religion = religion;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoryId")
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	public Yearmaster getYearmaster() {
		return yearmaster;
	}
	public void setYearmaster(Yearmaster yearmaster) {
		this.yearmaster = yearmaster;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "casteId")
	public Castemaster getCastemaster() {
		return castemaster;
	}
	public void setCastemaster(Castemaster castemaster) {
		this.castemaster = castemaster;
	}*/
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sansthaId")
	public SansthaRegistrationModel getSansthaRegistrationModel() {
		return sansthaRegistrationModel;
	}

	public void setSansthaRegistrationModel(SansthaRegistrationModel sansthaRegistrationModel) {
		this.sansthaRegistrationModel = sansthaRegistrationModel;
	}
	
}
