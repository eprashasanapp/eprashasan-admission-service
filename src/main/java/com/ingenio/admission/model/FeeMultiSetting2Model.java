package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


/**
 * FeeSettingMulti2 entity. @author MyEclipse Persistence Tools
 */
@Entity
@DynamicUpdate
@Table(name = "fee_setting_multi2")
public class FeeMultiSetting2Model implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer feeSetMulti2id;
	private AppUserRoleModel appUserRole;
	private SchoolMasterModel schoolMaster;
	private FeeSettingMultiNarrationModel feeSettingMultiNarrationModel;
	private FeeHeadMasterModel feeHeadMasterModel;
	private YearMasterModel yearmasterModel;
	private FeeTypeModel feeTypeModel;
	private StandardMasterModel standardMasterModel;
	private FeeSubheadMasterModel feeSubheadMasterModel;
	private Double assignFee;
	private Double fine;
	private String cdate;
	private String dueDate;
	private Integer fineIncreament;
	private Integer isConsolation;
	private Integer isDel=0;
	private Integer isEdit;
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";

	
	@Id
	@Column(name = "feeSetMulti2ID", unique = true, nullable = false)
	public Integer getFeeSetMulti2id() {
		return this.feeSetMulti2id;
	}

	public void setFeeSetMulti2id(Integer feeSetMulti2id) {
		this.feeSetMulti2id = feeSetMulti2id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return this.appUserRole;
	}

	public void setAppUserRoleModel(AppUserRoleModel appUserRole) {
		this.appUserRole = appUserRole;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID")
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMaster;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMaster) {
		this.schoolMaster = schoolMaster;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "narrationID")
	public FeeSettingMultiNarrationModel getFeeSettingMultiNarrationModel() {
		return this.feeSettingMultiNarrationModel;
	}

	public void setFeeSettingMultiNarrationModel(
			FeeSettingMultiNarrationModel feeSettingMultiNarrationModel) {
		this.feeSettingMultiNarrationModel = feeSettingMultiNarrationModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "headID")
	public FeeHeadMasterModel getFeeHeadMasterModel() {
		return this.feeHeadMasterModel;
	}

	public void setFeeHeadMasterModel(FeeHeadMasterModel feeHeadMasterModel) {
		this.feeHeadMasterModel = feeHeadMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	public YearMasterModel getYearMasterModel() {
		return this.yearmasterModel;
	}

	public void setYearMasterModel(YearMasterModel yearmasterModel) {
		this.yearmasterModel = yearmasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "feeTypeID")
	public FeeTypeModel getFeeTypeModel() {
		return this.feeTypeModel;
	}

	public void setFeeTypeModel(FeeTypeModel feeTypeModel) {
		this.feeTypeModel = feeTypeModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId")
	public StandardMasterModel getStandardMasterModel() {
		return this.standardMasterModel;
	}

	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subheadID")
	public FeeSubheadMasterModel getFeeSubheadMasterModel() {
		return this.feeSubheadMasterModel;
	}

	public void setFeeSubheadMasterModel(FeeSubheadMasterModel feeSubheadMasterModel) {
		this.feeSubheadMasterModel = feeSubheadMasterModel;
	}

	@Column(name = "assignFee", precision = 22, scale = 0)
	public Double getAssignFee() {
		return this.assignFee;
	}

	public void setAssignFee(Double assignFee) {
		this.assignFee = assignFee;
	}

	@Column(name = "fine", precision = 22, scale = 0)
	public Double getFine() {
		return this.fine;
	}

	public void setFine(Double fine) {
		this.fine = fine;
	}

	@Column(name = "cDate", updatable= false)
	public String getCdate() {
		return this.cdate;
	}

	public void setCdate(String cdate) {
		this.cdate = cdate;
	}

	@Column(name = "dueDate", length = 50)
	public String getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	@Column(name = "fineIncreament")
	public Integer getFineIncreament() {
		return this.fineIncreament;
	}

	public void setFineIncreament(Integer fineIncreament) {
		this.fineIncreament = fineIncreament;
	}

	@Column(name = "isConsolation")
	public Integer getIsConsolation() {
		return this.isConsolation;
	}

	public void setIsConsolation(Integer isConsolation) {
		this.isConsolation = isConsolation;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit")
	public Integer getIsEdit() {
		return this.isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", columnDefinition="default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
}