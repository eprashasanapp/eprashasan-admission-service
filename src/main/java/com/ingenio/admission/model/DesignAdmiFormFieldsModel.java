package com.ingenio.admission.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "design_admi_form_fields")
public class DesignAdmiFormFieldsModel {

	private Integer fieldId;
	private DesignAdmiFormTabsModel designAdmiFormTabsModel;
	private String fieldName;
	private String isDisplay;
	private String isRequired;
	private String isyearly;
	private String fieldTypeFlag;   
	private DesignAdmiFormLegendModel designAdmiFormLegendModel;
	private AppUserRoleModel userId;
	private SchoolMasterModel schoolMasterModel;
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private String isStaticOrDynamic;
	private Integer priority;
	private String validationFlag="0";
	
	private Set<DesignAdmiDynamicFieldValueModel> designAdmiDyanmicFieldValue = new HashSet<DesignAdmiDynamicFieldValueModel>(0);
	
	@Id
	
	@Column(name = "fieldId", unique = true, nullable = false,columnDefinition = "INT(11)")
	public Integer getFieldId() {
		return fieldId;
	}
	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tabsId",nullable = false,columnDefinition = "INT(11)" ,referencedColumnName="tabsId")
	public DesignAdmiFormTabsModel getDesignAdmiFormTabsModel() {
		return designAdmiFormTabsModel;
	}
	public void setDesignAdmiFormTabsModel(DesignAdmiFormTabsModel designAdmiFormTabsModel) {
		this.designAdmiFormTabsModel = designAdmiFormTabsModel;
	}
	
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Column(name = "isDisplay", nullable = false,length=1, columnDefinition="CHAR default 0")
	public String getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(String isDisplay) {
		this.isDisplay = isDisplay;
	}
	
	@Column(name = "isRequired", nullable = false,length=1, columnDefinition="CHAR default 0")
	public String getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(String isRequired) {
		this.isRequired = isRequired;
	}
	
	@Column(name = "isYearly", nullable = false,length=1, columnDefinition="CHAR default 0")
	public String getIsyearly() {
		return isyearly;
	}
	public void setIsyearly(String isyearly) {
		this.isyearly = isyearly;
	}
	
	@Column(name = "fieldTypeFlag",length=1, columnDefinition="CHAR default 0")
	public String getFieldTypeFlag() {
		return fieldTypeFlag;
	}
	public void setFieldTypeFlag(String fieldTypeId) {
		this.fieldTypeFlag = fieldTypeId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "legendId",nullable = false,columnDefinition = "INT(11)" ,referencedColumnName="legendId")
	public DesignAdmiFormLegendModel getDesignAdmiFormLegendModel() {
		return designAdmiFormLegendModel;
	}
	public void setDesignAdmiFormLegendModel(DesignAdmiFormLegendModel designAdmiFormLegendModel) {
		this.designAdmiFormLegendModel = designAdmiFormLegendModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	//@Column(name = "cDate",nullable = true, updatable= false,columnDefinition="Timestamp DEFAULT CURRENT_TIMESTAMP")
	@Column(name = "cDate",nullable = true, updatable= false)
	@Type(type="date")
	public Date getCdate() {
		return this.cDate;
	}

	public void setCdate(Date cDate) {
		this.cDate = cDate;
	}

	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isStaticOrDynamic",length=1, columnDefinition="CHAR default 0")
	public String getIsStaticOrDynamic() {
		return isStaticOrDynamic;
	}
	public void setIsStaticOrDynamic(String isStaticOrDynamic) {
		this.isStaticOrDynamic = isStaticOrDynamic;
	}
	
	@Column(name = "priority",columnDefinition = "INT(11) default 0")
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	@Column(name="validationFlag", length=1, columnDefinition="CHAR default 0")
	public String getValidationFlag() {
		return validationFlag;
	}
	public void setValidationFlag(String validationFlag) {
		this.validationFlag = validationFlag;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "designAdmiFormTabsModel")
	public Set<DesignAdmiDynamicFieldValueModel> getdesignAdmiDyanamicFieldValue() {
		return designAdmiDyanmicFieldValue;
	}
	public void setdesignAdmiDyanamicFieldValue(Set<DesignAdmiDynamicFieldValueModel> designAdmiDyanmicFieldValue) {
		this.designAdmiDyanmicFieldValue = designAdmiDyanmicFieldValue;
	}
	
	
}
