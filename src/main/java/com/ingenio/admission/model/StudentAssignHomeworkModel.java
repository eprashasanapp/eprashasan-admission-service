package com.ingenio.admission.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;


@Entity
@Table(name = "student_assign_homework")

public class StudentAssignHomeworkModel {

	private Integer assignHomeworkId;
	private SchoolMasterModel schoolMasterModel;
	private YearMasterModel yearMasterModel;
	private StandardMasterModel standardMasterModel;
	private DivisionMasterModel divisionMasterModel;
	private StaffSubjectDetailsModel staffSubjectDetailsModel;
	private StaffBasicDetailsModel staffBasicDetailsModel;
	private SubjectModel subjectModel;
	private String homeworkTitle;
	private String homework;
	private AppUserRoleModel userId;
	private Timestamp cDate;
	private String isDel="0";
	private String isEdit="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	private Date endDate;
	private Date startDate;
	
	
	Set<StudentHomeworkAttachmentModel> studentHomeworkAttachmentMap = new HashSet<StudentHomeworkAttachmentModel>(0);
	Set<StudentHomeworkAssignToModel> studentHomeworkAssignedToMap = new HashSet<StudentHomeworkAssignToModel>(0);
	
	@Id
	@Column(name = "assignHomeworkId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getAssignHomeworkId() {
		return assignHomeworkId;
	}

	public void setAssignHomeworkId(Integer assignHomeworkId) {
		this.assignHomeworkId = assignHomeworkId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="yearId")
	@ElementCollection(targetClass=YearMasterModel.class)
	public YearMasterModel getYearMasterModel() {
		return this.yearMasterModel;
	}
	
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId",  columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="standardId")
	@ElementCollection(targetClass=StandardMasterModel.class)
	public StandardMasterModel getStandardMasterModel() {
		return this.standardMasterModel;
	}
	
	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divisionId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="divisionId")
	@ElementCollection(targetClass=DivisionMasterModel.class)
	public DivisionMasterModel getDivisionMasterModel() {
		return this.divisionMasterModel;
	}
	
	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subjectId", columnDefinition = "INT(11) NOT NULL" ,referencedColumnName="subject_id")
	@ElementCollection(targetClass=SubjectModel.class)
	public SubjectModel getSubjectModel() {
		return subjectModel;
	}

	public void setSubjectModel(SubjectModel subjectModel) {
		this.subjectModel = subjectModel;
	}
	
	@Column(name="homeworkTitle",nullable = false, length = 50)
	@Length(max = 50)
	public String getHomeworkTitle() {
		return homeworkTitle;
	}

	public void setHomeworkTitle(String homeworkTitle) {
		this.homeworkTitle = homeworkTitle;
	}
	
	@Column(name="homework",nullable = false, length = 500)
	@Length(max = 500)
	public String getHomework() {
		return homework;
	}

	public void setHomework(String homework) {
		this.homework = homework;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "cDate",nullable = true, updatable= false,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	public Timestamp getCDate() {
		return this.cDate;
	}

	public void setCDate(Timestamp cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", length=1, columnDefinition="CHAR")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	@Type(type="date")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "staffSubjectId",columnDefinition = "INT(11) NOT NULL" ,referencedColumnName="staffSubId")
	@ElementCollection(targetClass=StaffSubjectDetailsModel.class)
	public StaffSubjectDetailsModel getStaffSubjectDetailsModel() {
		return staffSubjectDetailsModel;
	}

	public void setStaffSubjectDetailsModel(StaffSubjectDetailsModel staffSubjectDetailsModel) {
		this.staffSubjectDetailsModel = staffSubjectDetailsModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "staffId",columnDefinition = "INT(11) UNSIGNED NOT NULL" ,referencedColumnName="staaffId")
	@ElementCollection(targetClass=StaffBasicDetailsModel.class)
	public StaffBasicDetailsModel getStaffBasicDetailsModel() {
		return staffBasicDetailsModel;
	}

	public void setStaffBasicDetailsModel(StaffBasicDetailsModel staffBasicDetailsModel) {
		this.staffBasicDetailsModel = staffBasicDetailsModel;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "studentAssignHomeworkModel")
	public Set<StudentHomeworkAttachmentModel> getStudentHomeworkAttachmentMap() {
		return studentHomeworkAttachmentMap;
	}

	public void setStudentHomeworkAttachmentMap(Set<StudentHomeworkAttachmentModel> studentHomeworkAttachmentMap) {
		this.studentHomeworkAttachmentMap = studentHomeworkAttachmentMap;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "studentAssignHomeworkModel")
	public Set<StudentHomeworkAssignToModel> getStudentHomeworkAssignedToMap() {
		return studentHomeworkAssignedToMap;
	}

	public void setStudentHomeworkAssignedToMap(Set<StudentHomeworkAssignToModel> studentHomeworkAssignedToMap) {
		this.studentHomeworkAssignedToMap = studentHomeworkAssignedToMap;
	}

	@Column(name="endDate")
	@Type(type="date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name="startDate")
	@Type(type="date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
}
