package com.ingenio.admission.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

@Table(name = "certificate_global_type_master")
public class CertificateGlobalTypeMasterModel {
	private static final long serialVersionUID = 1L;
	private Integer globalCertificateId;
	private String certificateName;
	
	@Id
	@Column(name = "globalCertificateId", unique = true, nullable = false)
	public Integer getGlobalCertificateId() {
		return globalCertificateId;
	}
	public void setGlobalCertificateId(Integer globalCertificateId) {
		this.globalCertificateId = globalCertificateId;
	}
	
	@Column(name = "certificate_name")
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	
	
}
