package com.ingenio.admission.model;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "design_admi_form_dynamic_field_value")
public class DesignAdmiDynamicFieldValueModel {

	private Integer dynamicFieldId;
	private String fieldValue;
	private String fieldTypeFlag;
	private DesignAdmiFormTabsModel designAdmiFormTabsModel;
	private DesignAdmiFormLegendModel designAdmiFormLegendModel;
	private DesignAdmiFormTabsMultiModel designAdmiFormTabsMultiModel;
	
	private AppUserRoleModel userId;
	private SchoolMasterModel schoolMasterModel;
	private Timestamp cDate;
	private String isDel="0";
	private String isEdit="0";
	private String sinkingFlag="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;

	
	@Id
	
	@Column(name = "dynamicFieldId", unique = true, nullable = false,columnDefinition = "INT(11)")
	public Integer getDynamicFieldId() {
		return dynamicFieldId;
	}
	public void setDynamicFieldId(Integer dynamicFieldId) {
		this.dynamicFieldId = dynamicFieldId;
	}
	
	@Column(name="fieldValue",nullable = false, length = 30)
	@Length(max = 30)
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	
	@Column(name = "fieldTypeFlag", nullable = false,columnDefinition="CHAR default '0'", length=1)
	public String getFieldTypeFlag() {
		return fieldTypeFlag;
	}
	public void setFieldTypeFlag(String fieldTypeFlag) {
		this.fieldTypeFlag = fieldTypeFlag;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tabsId",nullable = false,columnDefinition = "INT(11)" ,referencedColumnName="tabsId")
	@ElementCollection(targetClass=DesignAdmiFormTabsModel.class)
	public DesignAdmiFormTabsModel getDesignAdmiFormTabsModel() {
		return designAdmiFormTabsModel;
	}
	public void setDesignAdmiFormTabsModel(DesignAdmiFormTabsModel designAdmiFormTabsModel) {
		this.designAdmiFormTabsModel = designAdmiFormTabsModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "legendId",nullable = false,columnDefinition = "INT(11)" ,referencedColumnName="legendId")
	@ElementCollection(targetClass=DesignAdmiFormLegendModel.class)
	public DesignAdmiFormLegendModel getDesignAdmiFormLegendModel() {
		return designAdmiFormLegendModel;
	}
	public void setDesignAdmiFormLegendModel(DesignAdmiFormLegendModel designAdmiFormLegendModel) {
		this.designAdmiFormLegendModel = designAdmiFormLegendModel;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	@ElementCollection(targetClass=SchoolMasterModel.class)
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "cDate",nullable = true, updatable= false,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	public Timestamp getCdate() {
		return this.cDate;
	}

	public void setCdate(Timestamp cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", length=1, columnDefinition="CHAR")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	@ElementCollection(targetClass=AppUserRoleModel.class)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	@Type(type="date")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fieldId",nullable = false,columnDefinition = "INT(11)" ,referencedColumnName="fieldId")
	public DesignAdmiFormTabsMultiModel getDesignAdmiFormTabsMultiModel() {
		return designAdmiFormTabsMultiModel;
	}
	public void setDesignAdmiFormTabsMultiModel(DesignAdmiFormTabsMultiModel designAdmiFormTabsMultiModel) {
		this.designAdmiFormTabsMultiModel = designAdmiFormTabsMultiModel;
	}
	
	
	
}
