package com.ingenio.admission.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "eprashasan_images")
public class EprashsanImagesModel {

	private Integer eprashasanImageId;		
	private Timestamp cDate;
	private String fileName;
	private String imagePath;
	
	@Id
	@Column(name = "eprashasanImageId", nullable = false,columnDefinition = "INT(11) UNSIGNED")
	public Integer getEprashasanImageId() {
		return eprashasanImageId;
	}
	public void setEprashasanImageId(Integer eprashasanImageId) {
		this.eprashasanImageId = eprashasanImageId;
	}
	
	@Column(name = "cDate")
	public Timestamp getcDate() {
		return cDate;
	}
	public void setcDate(Timestamp cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "fileName")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name = "imagePath")
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	

	
	
}
