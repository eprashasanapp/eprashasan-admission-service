package com.ingenio.admission.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "android_menu_not_for_school")
public class AndroidMenuNotForSchoolModel {

	private Integer menuNotforSchId;
	private Integer schoolid;
	private AndroidMenuMasterModel androidMenuMasterModel;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getMenuNotforSchId() {
		return menuNotforSchId;
	}
	public void setMenuNotforSchId(Integer menuNotforSchId) {
		this.menuNotforSchId = menuNotforSchId;
	}
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "schoolid",nullable = false)
//	public SchoolMasterModel getSchoolMasterModel() {
//		return schoolMasterModel;
//	}
//	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
//		this.schoolMasterModel = schoolMasterModel;
//	}
		
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menuId",nullable = false)
	public AndroidMenuMasterModel getAndroidMenuMasterModel() {
		return androidMenuMasterModel;
	}
	
	
	public void setAndroidMenuMasterModel(AndroidMenuMasterModel androidMenuMasterModel) {
		this.androidMenuMasterModel = androidMenuMasterModel;
	}
	
		
	
}
