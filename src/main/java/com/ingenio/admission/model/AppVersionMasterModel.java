package com.ingenio.admission.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "app_version_master")
public class AppVersionMasterModel {

private  Integer versionID;
private  Integer versionCode;
private String versionName;
private String instruction;
private String forRole;
private String forSchool;
private String isMandatoryToRole;
private String isMandatoryToSchool;
private String isReact;
private String appType;


@Id
@Column(name = "versionID", nullable = false)
public Integer getVersionID() {
	return versionID;
}
public void setVersionID(Integer versionID) {
	this.versionID = versionID;
}
@Column(name="version_code ")
public Integer getVersionCode() {
	return versionCode;
}
public void setVersionCode(Integer versionCode) {
	this.versionCode = versionCode;
}
@Column(name="version_name")
public String getVersionName() {
	return versionName;
}
public void setVersionName(String versionName) {
	this.versionName = versionName;
}
@Column(name="instruction")
public String getInstruction() {
	return instruction;
}
public void setInstruction(String instruction) {
	this.instruction = instruction;
}
@Column(name="for_role")
public String getForRole() {
	return forRole;
}
public void setForRole(String forRole) {
	this.forRole = forRole;
}
@Column(name="for_school ")
public String getForSchool() {
	return forSchool;
}
public void setForSchool(String forSchool) {
	this.forSchool = forSchool;
}
@Column(name="is_mandatory_to_role ")
public String getIsMandatoryToRole() {
	return isMandatoryToRole;
}
public void setIsMandatoryToRole(String isMandatoryToRole) {
	this.isMandatoryToRole = isMandatoryToRole;
}
@Column(name="is_mandatory_to_school")
public String getIsMandatoryToSchool() {
	return isMandatoryToSchool;
}
public void setIsMandatoryToSchool(String isMandatoryToSchool) {
	this.isMandatoryToSchool = isMandatoryToSchool;
}
@Column(name="isreact")
public String getIsReact() {
	return isReact;
}
public void setIsReact(String isReact) {
	this.isReact = isReact;
}
public String getAppType() {
	return appType;
}
public void setAppType(String appType) {
	this.appType = appType;
}


 
 
 




	
}
