package com.ingenio.admission.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;



@Entity
@Table(name = "fee_paid_prospect_fee")
public class FeePaidProspectFeesModel {

	private static final long serialVersionUID = 1L;
	private Integer feePaidProspectFeeId;
	private SchoolMasterModel schoolMasterModel;
	private YearMasterModel yearMasterModel;
	private StandardMasterModel standardMasterModel;
	private Double prospectFee;
	private Double CGST;
	private Double SGST;
	private StudentStandardRenewModel studentStandardRenewModel;
	private String studentName;
	private Double totalPaidFee;
	private String isStudent;
	private FeePayTypeModel feePayTypeModel;
	private Challanbankdetails challanbankdetails;
	private String Check_DD_Challan;
	private Integer receiptNo;
	private AppUserRoleModel appUserRoleModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private String sinkingFlag = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private Integer headId;
	private String headName;
	private String order_id;
	private String transcation_id;
	

	@Id
	@Column(name = "feePaidProspectFeeId", unique = true, nullable = false)
	public Integer getFeePaidProspectFeeId() {
		return feePaidProspectFeeId;
	}

	public void setFeePaidProspectFeeId(Integer feePaidProspectFeeId) {
		this.feePaidProspectFeeId = feePaidProspectFeeId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "renewStudentId")
	public StudentStandardRenewModel getStudentStandardRenewModel() {
		return studentStandardRenewModel;
	}

	public void setStudentStandardRenewModel(StudentStandardRenewModel studentStandardRenewModel) {
		this.studentStandardRenewModel = studentStandardRenewModel;
	}

	@Column(name = "studentName")
	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	@Column(name = "totalPaidFee")
	public Double getTotalPaidFee() {
		return totalPaidFee;
	}

	public void setTotalPaidFee(Double totalPaidFee) {
		this.totalPaidFee = totalPaidFee;
	}

	@Column(name = "isStudent")
	public String getIsStudent() {
		return isStudent;
	}

	public void setIsStudent(String isStudent) {
		this.isStudent = isStudent;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "feePayType")
	public FeePayTypeModel getFeePayTypeModel() {
		return feePayTypeModel;
	}

	public void setFeePayTypeModel(FeePayTypeModel feePayTypeModel) {
		this.feePayTypeModel = feePayTypeModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "challanBankId")
	public Challanbankdetails getChallanbankdetails() {
		return challanbankdetails;
	}

	public void setChallanbankdetails(Challanbankdetails challanbankdetails) {
		this.challanbankdetails = challanbankdetails;
	}

	@Column(name = "Check_DD_Challan")
	public String getCheck_DD_Challan() {
		return Check_DD_Challan;
	}

	public void setCheck_DD_Challan(String check_DD_Challan) {
		Check_DD_Challan = check_DD_Challan;
	}

	@Column(name = "receiptNo")
	public Integer getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(Integer receiptNo) {
		this.receiptNo = receiptNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	public YearMasterModel getYearMasterModel() {
		return yearMasterModel;
	}

	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "standardId")
	public StandardMasterModel getStandardMasterModel() {
		return standardMasterModel;
	}

	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}

	@Column(name = "prospectFee")
	public Double getProspectFee() {
		return prospectFee;
	}

	public void setProspectFee(Double prospectFee) {
		this.prospectFee = prospectFee;
	}

	@Column(name = "CGST")
	public Double getCGST() {
		return CGST;
	}

	public void setCGST(Double cGST) {
		CGST = cGST;
	}

	@Column(name = "SGST")
	public Double getSGST() {
		return SGST;
	}

	public void setSGST(Double sGST) {
		SGST = sGST;
	}

	@Column(name = "isDel", columnDefinition = "default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID")
	public AppUserRoleModel getAppUserRoleModel() {
		return appUserRoleModel;
	}

	public void setAppUserRoleModel(AppUserRoleModel appUserRoleModel) {
		this.appUserRoleModel = appUserRoleModel;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "headId")
	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	
	@Column(name = "headName")
	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	@Column(name = "order_id")
	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	
	@Column(name = "transcationId")
	public String getTranscation_id() {
		return transcation_id;
	}

	public void setTranscation_id(String transcation_id) {
		this.transcation_id = transcation_id;
	}
	
	
	
	
}