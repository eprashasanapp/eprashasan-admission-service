package com.ingenio.admission.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity

@Table(name = "certificate_excel_schoolwise")
public class CertificateExcelSchoolWiseModel {

	private static final long serialVersionUID = 1L;
	private Integer cerExcelSchoolWiseId;
	private String excelPath;
	private SchoolMasterModel schoolMasterModel;
	private CertificateGlobalTypeMasterModel certificateGlobalTypeMasterModel;
	
	@Id
	
	@Column(name = "cerExcelSchoolWiseId", unique = true, nullable = false)
	public Integer getCerExcelSchoolWiseId() {
		return cerExcelSchoolWiseId;
	}
	public void setCerExcelSchoolWiseId(Integer cerExcelSchoolWiseId) {
		this.cerExcelSchoolWiseId = cerExcelSchoolWiseId;
	}
	@Column(name = "excelPath", length = 1000)
	public String getExcelPath() {
		return excelPath;
	}
	public void setExcelPath(String excelPath) {
		this.excelPath = excelPath;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "globalCertificateId", nullable = false)
	public CertificateGlobalTypeMasterModel getCertificateGlobalTypeMasterModel() {
		return certificateGlobalTypeMasterModel;
	}
	public void setCertificateGlobalTypeMasterModel(CertificateGlobalTypeMasterModel certificateGlobalTypeMasterModel) {
		this.certificateGlobalTypeMasterModel = certificateGlobalTypeMasterModel;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
