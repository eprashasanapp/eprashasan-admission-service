package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "group_of_school")

public class GroupOfSchoolModel {
	
	private int groupOfSchoolId;	
	private SchoolMasterModel schoolMasterModel;
	private SansthaRegistrationModel sansthaRegistrationModel;
	private String groupOfSchoolName;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private String isEdit = "0";
	private Date delDate;
	private String sinkingFlag="0";
	private Date editDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;	
	private AppUserRoleModel userId;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private AppUserRoleModel approvalBy;
	private String isApproval = "0";
	private Date approvalDate;
	
	@Id
	@Column(name = "groupOfSchoolId")
	public int getGroupOfSchoolId() {
		return groupOfSchoolId;
	}

	public void setGroupOfSchoolId(int groupOfSchoolId) {
		this.groupOfSchoolId = groupOfSchoolId;
	}

	@Column(name = "groupOfSchoolName")
	public String getGroupOfSchoolName() {
		return groupOfSchoolName;
	}

	public void setGroupOfSchoolName(String groupOfSchoolName) {
		this.groupOfSchoolName = groupOfSchoolName;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}
	
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name = "isApproval", columnDefinition = "default '0'")
	public String getIsApproval() {
		return isApproval;
	}
	
	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}
	
	@Column(name = "isDel", columnDefinition = "default '0'")
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}
	
	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sansthaId", nullable = false)
	public SansthaRegistrationModel getSansthaRegistrationModel() {
		return sansthaRegistrationModel;
	}

	public void setSansthaRegistrationModel(SansthaRegistrationModel sansthaRegistrationModel) {
		this.sansthaRegistrationModel = sansthaRegistrationModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	


}