package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "principal_menu_assignto_staff")
public class PrincipalMenuAssigntoStaffModel {

	private Integer principalMenuAssignToStaffId;
	private Integer staffId;
	private Integer schoolid;
	private Integer principalMenuMasterId;
	private String flag = "0";
	private Integer userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "principalMenuAssignToStaffId")
	public Integer getPrincipalMenuAssignToStaffId() {
		return principalMenuAssignToStaffId;
	}

	public void setPrincipalMenuAssignToStaffId(Integer principalMenuToStaffId) {
		this.principalMenuAssignToStaffId = principalMenuToStaffId;
	}

	@Column(name = "staffId")
	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	@Column(name = "schoolid")
	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	@Column(name = "principalMenuMasterId")
	public Integer getPrincipalMenuMasterId() {
		return principalMenuMasterId;
	}

	public void setPrincipalMenuMasterId(Integer principalMenuMasterId) {
		this.principalMenuMasterId = principalMenuMasterId;
	}

	@Column(name = "flag", nullable = true)
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Column(name = "userId")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "isDel", nullable = true)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

}
