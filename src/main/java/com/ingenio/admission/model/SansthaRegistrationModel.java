package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@Table(name="sanstha_registration")

public class SansthaRegistrationModel {
	
	private Integer sansthaId;
	private String sansthaName;
	private String sansthaNameGL;
	private String regNum;
	private String sansthaKey;
	private String email;
	private String website;
	private String address;
	private String addressGL;
	private String city;
	private String cityGL;
	private String state;
	private String stateGL;
	private String country;
	private String countryGL;
	private Integer pincode;
	private String contactNo;
	private Integer faxNo;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
	public SansthaRegistrationModel() {
		super();
	}

	public SansthaRegistrationModel(Integer sansthaId, String sansthaName, String sansthaNameGL, String regNum, String email, String website, String address, String addressGL, String city, String cityGL, String state, String stateGL, Integer pincode, String contactNo, Integer faxNo, AppUserRoleModel userId, Date cDate, String isDel, String isEdit, Date delDate, AppUserRoleModel deleteBy, Date editDate, AppUserRoleModel editBy, String deviceType, String ipAddress, String macAddress, String sinkingFlag) {
		super();
		this.sansthaId = sansthaId;
		this.sansthaName = sansthaName;
		this.sansthaNameGL = sansthaNameGL;
		this.regNum = regNum;
		this.email = email;
		this.website = website;
		this.address = address;
		this.addressGL = addressGL;
		this.city = city;
		this.cityGL = cityGL;
		this.state = state;
		this.stateGL = stateGL;
		this.pincode = pincode;
		this.contactNo = contactNo;
		this.faxNo = faxNo;
		this.userId = userId;
		this.cDate = cDate;
		this.isDel = isDel;
		this.isEdit = isEdit;
		this.delDate = delDate;
		this.deleteBy = deleteBy;
		this.editDate = editDate;
		this.editBy = editBy;
		this.deviceType = deviceType;
		this.ipAddress = ipAddress;
		this.macAddress = macAddress;
		this.sinkingFlag = sinkingFlag;
	}
	
	@Id
	@Column(name = "sansthaId", unique = true, nullable = false)
	public Integer getSansthaId() {
		return sansthaId;
	}

	public void setSansthaId(Integer sansthaId) {
		this.sansthaId = sansthaId;
	}
	
	@Column(name = "sansthaName", length = 500)
	public String getSansthaName() {
		return sansthaName;
	}

	public void setSansthaName(String sansthaName) {
		this.sansthaName = sansthaName;
	}

	@Column(name = "sansthaNameGL")
	public String getSansthaNameGL() {
		return sansthaNameGL;
	}

	public void setSansthaNameGL(String sansthaNameGL) {
		this.sansthaNameGL = sansthaNameGL;
	}

	@Column(name = "country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "countryGL")
	public String getCountryGL() {
		return countryGL;
	}

	public void setCountryGL(String countryGL) {
		this.countryGL = countryGL;
	}
	
	@Column(name = "regNum", length = 200)
	public String getRegNum() {
		return this.regNum;
	}
	
	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}
	
	@Column(name = "sansthaKey", length = 200)
	public String getSansthaKey() {
		return sansthaKey;
	}

	public void setSansthaKey(String sansthaKey) {
		this.sansthaKey = sansthaKey;
	}
	
	@Column(name = "address", length = 500)
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "city", length = 50)
	public String getCity() {
		return this.city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name = "state", length = 50)
	public String getState() {
		return this.state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name = "pincode")
	public Integer getPincode() {
		return this.pincode;
	}
	
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
	
	@Column(name = "contactNo", length = 15)
	public String getContactNo() {
		return this.contactNo;
	}
	
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	
	@Column(name = "faxNo")
	public Integer getFaxNo() {
		return this.faxNo;
	}
	
	public void setFaxNo(Integer faxNo) {
		this.faxNo = faxNo;
	}
	
	@Column(name = "email", length = 50)
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "website", length = 50)
	public String getWebsite() {
		return this.website;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "addressGL")
	public String getAddressGL() {
		return addressGL;
	}
	
	public void setAddressGL(String addressGL) {
		this.addressGL = addressGL;
	}
	
	@Column(name = "cityGL")
	public String getCityGL() {
		return cityGL;
	}
	
	public void setCityGL(String cityGL) {
		this.cityGL = cityGL;
	}
	
	@Column(name = "stateGL")
	public String getStateGL() {
		return stateGL;
	}
	
	public void setStateGL(String stateGL) {
		this.stateGL = stateGL;
	}
	
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}	
}
