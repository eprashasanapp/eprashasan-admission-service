package com.ingenio.admission.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@Table(name = "attendence_month_catalog")

public class AttendanceMonthCatlogModel {
	
	private static final long serialVersionUID = 1L;
	private Integer attandenceMonthCatalogId;
	private SchoolMasterModel schoolMasterModel;
	private StudentMasterModel studentMasterModel;
	private StudentStandardRenewModel studentStandardRenewModel;
	private Integer monthId;
	private Integer rollNo;
	private DivisionMasterModel divisionMasterModel;
	private YearMasterModel yearMasterModel;
	private StandardMasterModel standardMasterModel;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;	
	private String isEdit = "0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval = "0";
	private AppUserRoleModel approvalBy;
	private Date approvalDate;	
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	
	
	@Id
//	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "attandenceMonthCatalogId", unique = true, nullable = false)
	public Integer getAttandenceMonthCatalogId() {
		return attandenceMonthCatalogId;
	}

	public void setAttandenceMonthCatalogId(Integer attandenceMonthCatalogId) {
		this.attandenceMonthCatalogId = attandenceMonthCatalogId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "renewstudentId")
	public StudentStandardRenewModel getStudentStandardRenewModel() {
		return studentStandardRenewModel;
	}

	public void setStudentStandardRenewModel(StudentStandardRenewModel studentStandardRenewModel) {
		this.studentStandardRenewModel = studentStandardRenewModel;
	}

	@Column(name = "monthId")
	public Integer getMonthId() {
		return monthId;
	}

	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divId")
	public DivisionMasterModel getDivisionMasterModel() {
		return this.divisionMasterModel;
	}
	
	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", nullable = false)
	public YearMasterModel getYearMasterModel() {
		return this.yearMasterModel;
	}
	
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stdId", nullable = false)
	public StandardMasterModel getStandardMasterModel() {
		return this.standardMasterModel;
	}
	
	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stud_id", nullable = false)
	public StudentMasterModel getStudentMasterModel() {
		return this.studentMasterModel;
	}
	
	public void setStudentMasterModel(StudentMasterModel studentMasterModel) {
		this.studentMasterModel = studentMasterModel;
	}
	
	@Column(name = "rollNo")
	public Integer getRollNo() {
		return this.rollNo;
	}
	
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	public Date getApprovalDate() {
		return approvalDate;
	}
	
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}
	
	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}
	
	@Column(name = "isApproval", columnDefinition = "default '0'")
	public String getIsApproval() {
		return isApproval;
	}
	
	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}
	
}