package com.ingenio.admission.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dummy_registration")
public class DummyRegistrationModel {

	
	private Integer dummyRegistrationId;
	private Integer schoolId;
	private Integer yearId;
	private Integer standardId;
	private Integer renewStudentId;
	private String studentName;
	private String order_id;
	private String transcationId;
	private String mobileNo;
	private String firstName;
	private String middleName;
	private String lastName;
	private String password;
	private Double totalPaidFee;
	private String formNo;
	private String mrormisss;
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "dummyRegistrationId", nullable = false)
	public Integer getDummyRegistrationId() {
		return dummyRegistrationId;
	}
	public void setDummyRegistrationId(Integer dummyRegistrationId) {
		this.dummyRegistrationId = dummyRegistrationId;
	}
	
	@Column(name = "schoolId")
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	
	
	@Column(name = "yearId")
	public Integer getYearId() {
		return yearId;
	}
	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}
	
	@Column(name = "standardId")
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	
	
	@Column(name = "renewStudentId")
	public Integer getRenewStudentId() {
		return renewStudentId;
	}
	public void setRenewStudentId(Integer renewStudentId) {
		this.renewStudentId = renewStudentId;
	}
	
	
	@Column(name = "studentName")
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	
	
	@Column(name = "order_id")
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	
	
	
	@Column(name = "transcationId")
	public String getTranscationId() {
		return transcationId;
	}
	public void setTranscationId(String transcationId) {
		this.transcationId = transcationId;
	}
	
	
	@Column(name = "mobileNo")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	
	@Column(name = "firstName")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	
	@Column(name = "middleName")
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	@Column(name = "lastName")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name = "password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "totalPaidFee")
	public Double getTotalPaidFee() {
		return totalPaidFee;
	}
	public void setTotalPaidFee(Double totalPaidFee) {
		this.totalPaidFee = totalPaidFee;
	}
	
	@Column(name = "formNo")
	public String getFormNo() {
		return formNo;
	}
	public void setFormNo(String formNo) {
		this.formNo = formNo;
	}

	
	@Column(name = "missmr")
	public String getMrormisss() {
		return mrormisss;
	}
	public void setMrormisss(String mrormisss) {
		this.mrormisss = mrormisss;
	}
	
	
	
	
}



