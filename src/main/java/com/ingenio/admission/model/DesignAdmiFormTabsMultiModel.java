package com.ingenio.admission.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "design_admi_form_tabs_multi")
public class DesignAdmiFormTabsMultiModel {

	private Integer fieldId;
	private DesignAdmiFormTabsModel designAdmiFormTabsModel;
	private String fieldName;
	private String isDisplay="0";
	private String isRequired="0";
	private String isyearly="0";
	private String fieldTypeFlag;   
	private DesignAdmiFormLegendModel designAdmiFormLegendModel;
	
	private AppUserRoleModel userId;
	private SchoolMasterModel schoolMasterModel;
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private String isEdit="0";
	private String sinkingFlag="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String isStaticOrDynamic;
	private Integer priority;
	private String validationFlag="0";
	
	private Set<DesignAdmiDynamicFieldValueModel> designAdmiDyanamicFieldValueModel = new HashSet<DesignAdmiDynamicFieldValueModel>(0);
	
	@Id
	@Column(name = "fieldId", unique = true, nullable = false,columnDefinition = "INT(11)")
	public Integer getFieldId() {
		return fieldId;
	}
	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tabsId",nullable = false,columnDefinition = "INT(11)" ,referencedColumnName="tabsId")
	public DesignAdmiFormTabsModel getDesignAdmiFormTabsModel() {
		return designAdmiFormTabsModel;
	}
	public void setDesignAdmiFormTabsModel(DesignAdmiFormTabsModel designAdmiFormTabsModel) {
		this.designAdmiFormTabsModel = designAdmiFormTabsModel;
	}
	
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Column(name = "isDisplay", nullable = false,length=1, columnDefinition="CHAR default 0")
	public String getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(String isDisplay) {
		this.isDisplay = isDisplay;
	}
	
	@Column(name = "isRequired", nullable = false,length=1, columnDefinition="CHAR default 0")
	public String getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(String isRequired) {
		this.isRequired = isRequired;
	}
	
	@Column(name = "isYearly", nullable = false,length=1, columnDefinition="CHAR default 0")
	public String getIsyearly() {
		return isyearly;
	}
	public void setIsyearly(String isyearly) {
		this.isyearly = isyearly;
	}
	
	@Column(name = "fieldTypeFlag",length=1, columnDefinition="CHAR default 0")
	public String getFieldTypeFlag() {
		return fieldTypeFlag;
	}
	public void setFieldTypeFlag(String fieldTypeId) {
		this.fieldTypeFlag = fieldTypeId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "legendId",nullable = false,columnDefinition = "INT(11)" ,referencedColumnName="legendId")
	public DesignAdmiFormLegendModel getDesignAdmiFormLegendModel() {
		return designAdmiFormLegendModel;
	}
	public void setDesignAdmiFormLegendModel(DesignAdmiFormLegendModel designAdmiFormLegendModel) {
		this.designAdmiFormLegendModel = designAdmiFormLegendModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return this.schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@Column(name = "cDate",nullable = true, updatable= false)
	@Type(type="date")
	public Date getCdate() {
		return this.cDate;
	}

	public void setCdate(Date cDate) {
		this.cDate = cDate;
	}

	
	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="isApproval", length=1, columnDefinition="CHAR")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name="approvalDate")
	@Type(type="date")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name = "isStaticOrDynamic",length=1, columnDefinition="CHAR default 0")
	public String getIsStaticOrDynamic() {
		return isStaticOrDynamic;
	}
	public void setIsStaticOrDynamic(String isStaticOrDynamic) {
		this.isStaticOrDynamic = isStaticOrDynamic;
	}
	
	@Column(name = "priority",columnDefinition = "INT(11) default 0")
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	@Column(name="validationFlag", length=1, columnDefinition="CHAR default 0")
	public String getValidationFlag() {
		return validationFlag;
	}
	public void setValidationFlag(String validationFlag) {
		this.validationFlag = validationFlag;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "designAdmiFormTabsModel")
	public Set<DesignAdmiDynamicFieldValueModel> getDesignAdmiDyanamicFieldValueModel() {
		return designAdmiDyanamicFieldValueModel;
	}
	public void setDesignAdmiDyanamicFieldValueModel(Set<DesignAdmiDynamicFieldValueModel> designAdmiDyanamicFieldValueModel) {
		this.designAdmiDyanamicFieldValueModel = designAdmiDyanamicFieldValueModel;
	}
	
}