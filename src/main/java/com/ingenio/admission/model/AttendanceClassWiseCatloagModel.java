package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "attendance_classwise_catalog")
public class AttendanceClassWiseCatloagModel {
	private Long attandenceClasswiseId;
	private Integer monthId;
	private Integer rollNo;
	private DivisionMasterModel divisionMasterModel;
	private YearMasterModel yearMasterModel;
	private StandardMasterModel standardMasterModel;
	private StudentMasterModel studentMasterModel;
	private StudentStandardRenewModel studentStandardRenewModel;
	private String attendanceDate;
	private String inTime;
	private String outTime;
	private String p_AFlag;
	private PresentAbsentMasterModel presentAbsentMasterModel;
	private AttendanceNumOFTimeModel noOfTimeAttendance;
	private String remark;
	private String hardwareType;
	private String isDel="0";
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit="0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval="0";
    private AppUserRoleModel approvalBy;
    private Date approvalDate;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";
	private Integer inTimeAttendanceRfid;
	private Integer outTimeAttendanceRfid;
	
	@Id
	@Column(name = "attandenceClasswiseId", unique = true, nullable = false)
	public Long getAttandenceClasswiseId() {
		return attandenceClasswiseId;
	}
	public void setAttandenceClasswiseId(Long attandenceClasswiseId) {
		this.attandenceClasswiseId = attandenceClasswiseId;
	}
	@Column(name = "monthId")
	public Integer getMonthId() {
		return monthId;
	}
	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}
	@Column(name = "rollNo")
	public Integer getRollNo() {
		return rollNo;
	}
	public void setRollNo(Integer rollNo) {
		this.rollNo = rollNo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "divId")
	public DivisionMasterModel getDivisionMasterModel() {
		return divisionMasterModel;
	}
	public void setDivisionMasterModel(DivisionMasterModel divisionMasterModel) {
		this.divisionMasterModel = divisionMasterModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", nullable = false)
	public YearMasterModel getYearMasterModel() {
		return yearMasterModel;
	}
	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stdId", nullable = false)
	public StandardMasterModel getStandardMasterModel() {
		return standardMasterModel;
	}
	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stud_id", nullable = false)
	public StudentMasterModel getStudentMasterModel() {
		return studentMasterModel;
	}
	public void setStudentMasterModel(StudentMasterModel studentMasterModel) {
		this.studentMasterModel = studentMasterModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "renewstudentId", nullable = false)
	public StudentStandardRenewModel getStudentStandardRenewModel() {
		return studentStandardRenewModel;
	}
	public void setStudentStandardRenewModel(StudentStandardRenewModel studentStandardRenewModel) {
		this.studentStandardRenewModel = studentStandardRenewModel;
	}
	@Column(name = "attendanceDate")
	public String getAttendanceDate() {
		return attendanceDate;
	}
	public void setAttendanceDate(String attendanceDate) {
		this.attendanceDate = attendanceDate;
	}
	@Column(name = "inTime")
	public String getInTime() {
		return inTime;
	}
	public void setInTime(String inTime) {
		this.inTime = inTime;
	}
	@Column(name = "outTime")
	public String getOutTime() {
		return outTime;
	}
	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	@Column(name = "p_AFlag")
	public String getP_AFlag() {
		return p_AFlag;
	}
	public void setP_AFlag(String p_AFlag) {
		this.p_AFlag = p_AFlag;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "p_AType", nullable = false)
	public PresentAbsentMasterModel getPresentAbsentMasterModel() {
		return presentAbsentMasterModel;
	}
	public void setPresentAbsentMasterModel(PresentAbsentMasterModel presentAbsentMasterModel) {
		this.presentAbsentMasterModel = presentAbsentMasterModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "noOfTimeAttendance", nullable = false)
	public AttendanceNumOFTimeModel getNoOfTimeAttendance() {
		return noOfTimeAttendance;
	}
	public void setNoOfTimeAttendance(AttendanceNumOFTimeModel noOfTimeAttendance) {
		this.noOfTimeAttendance = noOfTimeAttendance;
	}
	@Column(name = "remark")
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Column(name = "hardwareType")
	public String getHardwareType() {
		return hardwareType;
	}
	public void setHardwareType(String hardwareType) {
		this.hardwareType = hardwareType;
	}
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	@Column(name = "isApproval", columnDefinition = "default '0'")
	public String getIsApproval() {
		return isApproval;
	}
	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}
	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}
	@Column(name = "approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	@Column(name = "inTimeAttendanceRfid")
	public Integer getInTimeAttendanceRfid() {
		return inTimeAttendanceRfid;
	}
	public void setInTimeAttendanceRfid(Integer inTimeAttendanceRfid) {
		this.inTimeAttendanceRfid = inTimeAttendanceRfid;
	}
	
	@Column(name = "outTimeAttendanceRfid")
	public Integer getOutTimeAttendanceRfid() {
		return outTimeAttendanceRfid;
	}
	public void setOutTimeAttendanceRfid(Integer outTimeAttendanceRfid) {
		this.outTimeAttendanceRfid = outTimeAttendanceRfid;
	}
	
	
}
