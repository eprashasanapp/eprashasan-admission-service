package com.ingenio.admission.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "school_mapping_for_online_admission")
public class SchoolMappingOnlineAdmissionModel {
	private static final long serialVersionUID = -9004909181952908073L;

	private Integer mappingId;
	private Integer schoolId;
	private SchoolMasterModel schoolMasterModel;
	private Integer forOnlineAdmissionFlag;
	
	@Id
	@Column(name = "mappingId")
	public Integer getMappingId() {
		return mappingId;
	}

	public void setMappingId(Integer mappingId) {
		this.mappingId = mappingId;
	}

	@Column(name = "schoolId")
	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mappingSchoolId", columnDefinition = "INT(11) UNSIGNED DEFAULT NULL", referencedColumnName = "schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	@Column(name = "for_online_admission")
	public Integer getForOnlineAdmissionFlag() {
		return forOnlineAdmissionFlag;
	}

	public void setForOnlineAdmissionFlag(Integer forOnlineAdmissionFlag) {
		this.forOnlineAdmissionFlag = forOnlineAdmissionFlag;
	}

	
}