package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@Table(name="sanstha_user_mapp_to_school")

public class SanthaUserMappToSchoolModel {
	private Integer sansthaUserMappId;
	private SansthaRegistrationModel sansthaRegistrationModel;
	private SansthaUserRegistrationModel sansthaUserRegistrationModel;
	private SchoolMasterModel sansthaSchoolid;
	private AppUserRoleModel AppUserRoleModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private Date delDate;
	private String isEdit="0";
	private Date editDate;
	private String sinkingFlag="0";
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;	
	
	public SanthaUserMappToSchoolModel() {
		super();
	}
	
	@Id
	@Column(name = "sansthaUserMappId", nullable = false)
	public Integer getSansthaUserMappId() {
		return sansthaUserMappId;
	}
	public void setSansthaUserMappId(Integer sansthaUserMappId) {
		this.sansthaUserMappId = sansthaUserMappId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sansthaUserId", nullable = false)
	public SansthaUserRegistrationModel getSansthaUserRegistrationModel() {
		return sansthaUserRegistrationModel;
	}
	public void setSansthaUserRegistrationModel(SansthaUserRegistrationModel sansthaUserRegistrationModel) {
		this.sansthaUserRegistrationModel = sansthaUserRegistrationModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sanstha_schoolid", nullable = false)
	public SchoolMasterModel getSansthaSchoolid() {
		return sansthaSchoolid;
	}
	public void setSansthaSchoolid(SchoolMasterModel sansthaSchoolid) {
		this.sansthaSchoolid = sansthaSchoolid;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getAppUserRoleModel() {
		return AppUserRoleModel;
	}
	public void setAppUserRoleModel(AppUserRoleModel AppUserRoleModel) {
		this.AppUserRoleModel = AppUserRoleModel;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate(){
		return cDate;
	}

	public void setcDate(Date cDate){
		this.cDate = cDate;
	}

	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel(){
		return isDel;
	}

	public void setIsDel(String isDel){
		this.isDel = isDel;
	}
	
	@Column(name="delDate")
	public Date getDelDate(){
		return delDate;
	}

	public void setDelDate(Date delDate){
		this.delDate = delDate;
	}
	
	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit(){
		return isEdit;
	}

	public void setIsEdit(String isEdit){
		this.isEdit = isEdit;
	}
	
	@Column(name="editDate")
	public Date getEditDate(){
		return editDate;
	}

	public void setEditDate(Date editDate){
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag(){
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag){
		this.sinkingFlag = sinkingFlag;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sansthaId", nullable = false)
	public SansthaRegistrationModel getSansthaRegistrationModel() {
		return sansthaRegistrationModel;
	}
	public void setSansthaRegistrationModel(SansthaRegistrationModel sansthaRegistrationModel) {
		this.sansthaRegistrationModel = sansthaRegistrationModel;
	}
}
