package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity

@Table(name = "attendance")
public class AttendanceModel {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "attendance_id", unique = true, nullable = false)
	private Long attendanceId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "classid")
	private StandardMasterModel standardMasterModel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId")
	private YearMasterModel yearMasterModel;
	
	@Column(name = "absent_student_id")
	private String absentStudentId;
	
	@Column(name = "attended_date", length = 50)
	private String attendedDate;
	
	@Column(name = "status", length = 50)
	private String status;
	
	@Column(name = "divId")
	private Integer divId;
	
	@Column(name = "monthId")
	private Integer monthId;
	
	@Column(name = "present_student_id")
	private String presentStudentId;
	
	@Column(name = "absent_student_roll_no")
	private String absentStudentRollNo;
	
	@Column(name = "present_student_roll_no")
	private String presentStudentRollNo;
	
	@Column(name = "exempted_student_id")
	private String exemptedStudentId;
	
	@Column(name = "exempted_student_roll_no")
	private String exemptedStudentRollNo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	private SchoolMasterModel schoolMasterModel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	private AppUserModel userId;
	
	
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable= false)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name="isDel", columnDefinition="default '0'")
	private String isDel="0";
	
	@Column(name="delDate")
	private Date delDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	private AppUserModel deleteBy;
	
	@Column(name="isEdit", columnDefinition="default '0'")
	private String isEdit="0";
	
	@Column(name="editDate")
	private Date editDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	private AppUserModel editBy;
	
	@Column(name="deviceType", columnDefinition="default '0'")
	private String deviceType="0";
	
	@Column(name="ipAddress", length=100)
	private String ipAddress;
	
	@Column(name="macAddress", length=50)
	private String macAddress;
	
	@Column(name="sinkingFlag", columnDefinition="default '0'")
	private String sinkingFlag="0";

	public Long getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(Long attendanceId) {
		this.attendanceId = attendanceId;
	}

	public StandardMasterModel getStandardMasterModel() {
		return standardMasterModel;
	}

	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}

	public YearMasterModel getYearMasterModel() {
		return yearMasterModel;
	}

	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}

	public String getAbsentStudentId() {
		return absentStudentId;
	}

	public void setAbsentStudentId(String absentStudentId) {
		this.absentStudentId = absentStudentId;
	}

	public String getAttendedDate() {
		return attendedDate;
	}

	public void setAttendedDate(String attendedDate) {
		this.attendedDate = attendedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getDivId() {
		return divId;
	}

	public void setDivId(Integer divId) {
		this.divId = divId;
	}

	public Integer getMonthId() {
		return monthId;
	}

	public void setMonthId(Integer monthId) {
		this.monthId = monthId;
	}

	public String getPresentStudentId() {
		return presentStudentId;
	}

	public void setPresentStudentId(String presentStudentId) {
		this.presentStudentId = presentStudentId;
	}

	public String getAbsentStudentRollNo() {
		return absentStudentRollNo;
	}

	public void setAbsentStudentRollNo(String absentStudentRollNo) {
		this.absentStudentRollNo = absentStudentRollNo;
	}

	public String getPresentStudentRollNo() {
		return presentStudentRollNo;
	}

	public void setPresentStudentRollNo(String presentStudentRollNo) {
		this.presentStudentRollNo = presentStudentRollNo;
	}


	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	public AppUserModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserModel userId) {
		this.userId = userId;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	public AppUserModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public AppUserModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserModel editBy) {
		this.editBy = editBy;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public String getExemptedStudentId() {
		return exemptedStudentId;
	}

	public void setExemptedStudentId(String exemptedStudentId) {
		this.exemptedStudentId = exemptedStudentId;
	}

	public String getExemptedStudentRollNo() {
		return exemptedStudentRollNo;
	}

	public void setExemptedStudentRollNo(String exemptedStudentRollNo) {
		this.exemptedStudentRollNo = exemptedStudentRollNo;
	}

}