package com.ingenio.admission.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_version_toschool")
public class AppVersionToSchoolModel {

	private Integer appVersionToSchoolId;
	private AppVersionMasterModel appVersionMasterModel;
	private SchoolMasterModel schoolMasterModel;
	
	@Id
	@JoinColumn(name = "app_version_toschool_id")
	public Integer getAppVersionToSchoolId() {
		return appVersionToSchoolId;
	}
	public void setAppVersionToSchoolId(Integer appVersionToSchoolId) {
		this.appVersionToSchoolId = appVersionToSchoolId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "versionID")
	public AppVersionMasterModel getAppVersionMasterModel() {
		return appVersionMasterModel;
	}
	public void setAppVersionMasterModel(AppVersionMasterModel appVersionMasterModel) {
		this.appVersionMasterModel = appVersionMasterModel;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	
}
