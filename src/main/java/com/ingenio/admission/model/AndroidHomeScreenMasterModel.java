package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "android_home_screen_master")
public class AndroidHomeScreenMasterModel {
	
	@Id
	@Column(name = "homeScreenId")
	private Integer homeScreenId;
	
	@Column(name = "flag")
	private String flag;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "completedTitle")
	private String completedTitle;
	
	@Column(name = "remainingTitle")
	private String remainingTitle;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "messageCategoryId")
	private AndroidHomeMessageCategoryModel androidHomeMessageCategoryModel;
	
	@Column(name = "completedColor")
	private String completedColor;
	
	@Column(name = "remainingColor")
	private String remainingColor;

	@Column(name = "screenType")
	private String screenType;
	
	@Column(name = "role")
	private String role;
	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable = false)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name = "isDel", columnDefinition = "default '0'")
	private String isDel = "0";
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "deleteBy")
	private AppUserRoleModel deleteBy;
	
	@Column(name = "isEdit", columnDefinition = "default '0'")
	private String isEdit = "0";
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "editBy")
	private AppUserRoleModel editBy;
	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	private String deviceType = "0";
	
	@Column(name = "ipAddress", length = 100)
	private String ipAddress;
	
	@Column(name = "macAddress", length = 50)
	private String macAddress;
	
	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	private String sinkingFlag = "0";
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	private SchoolMasterModel schoolMasterModel;
	
	@Column(name = "priority")
	private Integer priority;

	public Integer getHomeScreenId() {
		return homeScreenId;
	}

	public void setHomeScreenId(Integer homeScreenId) {
		this.homeScreenId = homeScreenId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompletedTitle() {
		return completedTitle;
	}

	public void setCompletedTitle(String completedTitle) {
		this.completedTitle = completedTitle;
	}

	public String getRemainingTitle() {
		return remainingTitle;
	}

	public void setRemainingTitle(String remainingTitle) {
		this.remainingTitle = remainingTitle;
	}

	public String getCompletedColor() {
		return completedColor;
	}

	public void setCompletedColor(String completedColor) {
		this.completedColor = completedColor;
	}

	public String getRemainingColor() {
		return remainingColor;
	}

	public void setRemainingColor(String remainingColor) {
		this.remainingColor = remainingColor;
	}

	public String getScreenType() {
		return screenType;
	}

	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	public AndroidHomeMessageCategoryModel getAndroidHomeMessageCategoryModel() {
		return androidHomeMessageCategoryModel;
	}

	public void setAndroidHomeMessageCategoryModel(AndroidHomeMessageCategoryModel androidHomeMessageCategoryModel) {
		this.androidHomeMessageCategoryModel = androidHomeMessageCategoryModel;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	
}
