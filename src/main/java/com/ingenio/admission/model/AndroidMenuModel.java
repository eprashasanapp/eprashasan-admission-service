package com.ingenio.admission.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "android_menu")

public class AndroidMenuModel {
	private static final long serialVersionUID = -9004909181952908073L;


	private Integer menuId;
	private AndroidMenuMasterModel androidMenuMasterModel;	
	private SchoolMasterModel schoolMasterModel;
	private AppRoleModel appRoleModel;
	private AppUserRoleModel userId;
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	private Collection<AppRoleModel> roles;

	@Id	
	@Column(name = "menuId")
	public Integer getMenuId() {
		return menuId;
	}
	
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "masterMenuId", nullable = false)
	@Length(max = 30)
	public AndroidMenuMasterModel getAndroidMenuMasterModel() {
		return androidMenuMasterModel;
	}
	
	public void setAndroidMenuMasterModel(AndroidMenuMasterModel androidMenuMasterModel) {
		this.androidMenuMasterModel = androidMenuMasterModel;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolID",columnDefinition = "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}


	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleName", nullable = false)
	public AppRoleModel getAppRoleModel() {
		return appRoleModel;
	}


	public void setAppRoleModel(AppRoleModel appRoleModel) {
		this.appRoleModel = appRoleModel;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false ,columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate",nullable = true, updatable= false,columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name="isDel", columnDefinition="CHAR default '0'", length=1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name="delDate")
	@Type(type="date")
	public Date getDelDate() {
		return delDate;
	}


	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}


	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}


	@Column(name="isEdit", columnDefinition="CHAR default '0'", length=1)
	public String getIsEdit() {
		return isEdit;
	}


	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}


	@Column(name="editDate")
	@Type(type="date")
	public Date getEditDate() {
		return editDate;
	}


	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL",referencedColumnName="appuserroleid")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}


	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	@Column(name="deviceType", length=1,columnDefinition="CHAR")
	public String getDeviceType() {
		return deviceType;
	}


	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", length=1, columnDefinition="CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}


	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "APP_USER_ROLE", joinColumns = @JoinColumn(name = "USERNAME"), inverseJoinColumns = @JoinColumn(name = "ROLENAME"))
	public Collection<AppRoleModel> getRoles() {
		return roles;
	}


	public void setRoles(Collection<AppRoleModel> roles) {
		this.roles = roles;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}