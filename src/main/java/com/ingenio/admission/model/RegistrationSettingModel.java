package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "registration_setting")
public class RegistrationSettingModel {

	private static final long serialVersionUID = 1L;
	private Integer registrationSettingId;
	private SchoolMasterModel schoolMasterModel;
	private Integer registrationFlag;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	

	@Id	
	@Column(name = "registrationSettingId", unique = true, nullable = false)
	public Integer getRegistrationSettingId() {
		return registrationSettingId;
	}

	public void setRegistrationSettingId(Integer registrationSettingId) {
		this.registrationSettingId = registrationSettingId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "registrationFlag")
	public Integer getRegistrationFlag() {
		return registrationFlag;
	}

	public void setRegistrationFlag(Integer registrationFlag) {
		this.registrationFlag = registrationFlag;
	}

	
	
	

}