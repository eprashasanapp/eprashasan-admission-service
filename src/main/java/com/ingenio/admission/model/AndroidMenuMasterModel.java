package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "android_menu_master")
public class AndroidMenuMasterModel {


	private Integer masterMenuId;	
	private String menuName;
	private String imageFilePath;
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType = "0";
	private String ipAddress;	
	private String macAddress;
	private String sinkingFlag = "0";
	private AndroidMenuMasterGroupModel androidMenuMasterGroupModel;
	private String isReact; 
	private String isWebViewForReact="0";
	private String isWebViewForAndroid="0";
	private String webViewURL;
	private String globalForRole;
	private String isGlobal="0";
	private Integer parentMenuId;
	@Id
	@Column(name = "masterMenuId")
	public Integer getMasterMenuId() {
		return masterMenuId;
	}

	public void setMasterMenuId(Integer masterMenuId) {
		this.masterMenuId = masterMenuId;
	}

	@Column(name = "menuName",nullable = false, length = 30)
	@Length(max = 50)
	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "isDel", columnDefinition = "default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@Column(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@Column(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@Column(name = "imageFilePath")
	@Length(max = 200)
	public String getImageFilePath() {
		return imageFilePath;
	}

	public void setImageFilePath(String imageFilePath) {
		this.imageFilePath = imageFilePath;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "androidMenuGroupId", nullable = false)
	public AndroidMenuMasterGroupModel getAndroidMenuMasterGroupModel() {
		return androidMenuMasterGroupModel;
	}

	public void setAndroidMenuMasterGroupModel(AndroidMenuMasterGroupModel androidMenuMasterGroupModel) {
		this.androidMenuMasterGroupModel = androidMenuMasterGroupModel;
	}
	@Column(name = "isReact")
	public String getIsReact() {
		return isReact;
	}

	public void setIsReact(String isReact) {
		this.isReact = isReact;
	}

	public String getIsWebViewForReact() {
		return isWebViewForReact;
	}

	public void setIsWebViewForReact(String isWebViewForReact) {
		this.isWebViewForReact = isWebViewForReact;
	}

	public String getIsWebViewForAndroid() {
		return isWebViewForAndroid;
	}

	public void setIsWebViewForAndroid(String isWebViewForAndroid) {
		this.isWebViewForAndroid = isWebViewForAndroid;
	}

	public String getWebViewURL() {
		return webViewURL;
	}

	public void setWebViewURL(String webViewURL) {
		this.webViewURL = webViewURL;
	}

	public String getGlobalForRole() {
		return globalForRole;
	}

	public void setGlobalForRole(String globalForRole) {
		this.globalForRole = globalForRole;
	}

	public String getIsGlobal() {
		return isGlobal;
	}

	public void setIsGlobal(String isGlobal) {
		this.isGlobal = isGlobal;
	}

	public Integer getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Integer parentMenuId) {
		this.parentMenuId = parentMenuId;
	}
	
	
}
