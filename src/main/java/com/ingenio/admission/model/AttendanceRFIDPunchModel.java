package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "attendance_rfid_punch")
public class AttendanceRFIDPunchModel {
	private Integer attendanceRfid;
	// private SchoolMasterModel schoolMasterModel;
	// private StudentMappToRFIDModel studentMappToRFIDModel;
	private Integer schoolId;
	private String rfid;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date dateOfTransection;
	private Integer deviceId;
	private String inTimeStatus = "0";
	private String outTimeStatus = "0";

//	new 1
	private String latitude;
	private String longitude;
	private String ipAddress;
	private String macAddress;
	private String randomCode;
	private String imagePath;
	private String imei;
//	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "attendanceRfid", nullable = false, columnDefinition = "INT(10) UNSIGNED")
	public Integer getAttendanceRfid() {
		return attendanceRfid;
	}

	public void setAttendanceRfid(Integer attendanceRfid) {
		this.attendanceRfid = attendanceRfid;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "schoolId",columnDefinition = "INT(10) UNSIGNED DEFAULT NULL" ,referencedColumnName="schoolId")
//	public SchoolMasterModel getSchoolMasterModel() {
//		return schoolMasterModel;
//	}
//	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
//		this.schoolMasterModel = schoolMasterModel;
//	}

	/*
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "studMappToRfid",columnDefinition =
	 * "INT(11) UNSIGNED DEFAULT NULL" ,referencedColumnName="studMappToRfid")
	 * public StudentMappToRFIDModel getStudentMappToRFIDModel() { return
	 * studentMappToRFIDModel; } public void
	 * setStudentMappToRFIDModel(StudentMappToRFIDModel studentMappToRFIDModel) {
	 * this.studentMappToRFIDModel = studentMappToRFIDModel; }
	 */
	@Column(name = "deviceId")
	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	@Column(name = "rfid", nullable = true, length = 255)
	public String getRfid() {
		return rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	@Column(name = "dateOfTransection")
	public Date getDateOfTransection() {
		return dateOfTransection;
	}

	public void setDateOfTransection(Date dateOfTransection) {
		this.dateOfTransection = dateOfTransection;
	}

	@Column(name = "schoolId")
	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	@Column(name = "inTimeStatus", nullable = false)
	public String getInTimeStatus() {
		return inTimeStatus;
	}

	public void setInTimeStatus(String inTimeStatus) {
		this.inTimeStatus = inTimeStatus;
	}

	@Column(name = "outTimeStatus", nullable = false)
	public String getOutTimeStatus() {
		return outTimeStatus;
	}

	public void setOutTimeStatus(String outTimeStatus) {
		this.outTimeStatus = outTimeStatus;
	}

//	new 1
	
	@Column(name = "latitude")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "longitude")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "ip_address")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "mac_address")
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name = "random_code")
	public String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(String randomCode) {
		this.randomCode = randomCode;
	}

	@Column(name = "image_path")
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Column(name = "imei")
	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

//	end new 1
	
}
