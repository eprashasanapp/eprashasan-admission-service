package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity

@Table(name = "dynamicfieldstudinformation")
public class DynamicFieldStudentInformationModel  {

	private Integer infoId;
	private DynamicCombobox5 dynamiccombobox5;
	private DynamicCombobox4 dynamiccombobox4;
	private StudentMasterModel studentMasterModel;
	private DynamicCombobox1 dynamiccombobox1;
	private DynamicCombobox2 dynamiccombobox2;
	private DynamicCombobox3 dynamiccombobox3;
	private String textbox1;
	private String textbox2;
	private String textbox3;
	private String textbox4;
	private String textbox5;
	private Integer isRenewedFlag;
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel="0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit="0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";

	@Id
	
	@Column(name = "infoId", unique = true, nullable = false)
	public Integer getInfoId() {
		return this.infoId;
	}

	public void setInfoId(Integer infoId) {
		this.infoId = infoId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combobox5")
	public DynamicCombobox5 getDynamiccombobox5() {
		return this.dynamiccombobox5;
	}

	public void setDynamiccombobox5(DynamicCombobox5 dynamiccombobox5) {
		this.dynamiccombobox5 = dynamiccombobox5;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combobox4")
	public DynamicCombobox4 getDynamiccombobox4() {
		return this.dynamiccombobox4;
	}

	public void setDynamiccombobox4(DynamicCombobox4 dynamiccombobox4) {
		this.dynamiccombobox4 = dynamiccombobox4;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "studId")
	public StudentMasterModel getStudentMasterModel() {
		return this.studentMasterModel;
	}

	public void setStudentMasterModel(StudentMasterModel studentMasterModel) {
		this.studentMasterModel = studentMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combobox1")
	public DynamicCombobox1 getDynamiccombobox1() {
		return this.dynamiccombobox1;
	}

	public void setDynamiccombobox1(DynamicCombobox1 dynamiccombobox1) {
		this.dynamiccombobox1 = dynamiccombobox1;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combobox2")
	public DynamicCombobox2 getDynamiccombobox2() {
		return this.dynamiccombobox2;
	}

	public void setDynamiccombobox2(DynamicCombobox2 dynamiccombobox2) {
		this.dynamiccombobox2 = dynamiccombobox2;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combobox3")
	public DynamicCombobox3 getDynamiccombobox3() {
		return this.dynamiccombobox3;
	}

	public void setDynamiccombobox3(DynamicCombobox3 dynamiccombobox3) {
		this.dynamiccombobox3 = dynamiccombobox3;
	}

	@Column(name = "textbox1", length = 120)
	public String getTextbox1() {
		return this.textbox1;
	}

	public void setTextbox1(String textbox1) {
		this.textbox1 = textbox1;
	}

	@Column(name = "textbox2", length = 120)
	public String getTextbox2() {
		return this.textbox2;
	}

	public void setTextbox2(String textbox2) {
		this.textbox2 = textbox2;
	}

	@Column(name = "textbox3", length = 120)
	public String getTextbox3() {
		return this.textbox3;
	}

	public void setTextbox3(String textbox3) {
		this.textbox3 = textbox3;
	}

	@Column(name = "textbox4", length = 120)
	public String getTextbox4() {
		return this.textbox4;
	}

	public void setTextbox4(String textbox4) {
		this.textbox4 = textbox4;
	}

	@Column(name = "textbox5", length = 120)
	public String getTextbox5() {
		return this.textbox5;
	}

	public void setTextbox5(String textbox5) {
		this.textbox5 = textbox5;
	}

	@Column(name = "isRenewedFlag")
	public Integer getIsRenewedFlag() {
		return this.isRenewedFlag;
	}

	public void setIsRenewedFlag(Integer isRenewedFlag) {
		this.isRenewedFlag = isRenewedFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel(){
		return isDel;
	}

	public void setIsDel(String isDel){
		this.isDel = isDel;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="isEdit", columnDefinition="default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
}