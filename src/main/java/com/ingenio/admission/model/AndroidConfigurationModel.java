package com.ingenio.admission.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name="android_configuration")

public class AndroidConfigurationModel {

	@Id
    @Column(name="androidId", nullable=false)
	private Integer androidId;
	
	@Column(name = "offset")
	private Integer offset;
	
	@Column(name = "currentVersion",length=50)
	private String currentVersion;
	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable = false)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name = "isDel", columnDefinition = "default '0'")
	private String isDel = "0";
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "deleteBy")
	private AppUserRoleModel deleteBy;
	
	@Column(name = "isEdit", columnDefinition = "default '0'")
	private String isEdit = "0";
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "editBy")
	private AppUserRoleModel editBy;
	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	private String deviceType = "0";
	
	@Column(name = "ipAddress", length = 100)
	private String ipAddress;
	
	@Column(name = "macAddress", length = 50)
	private String macAddress;
	
	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	private String sinkingFlag = "0";
	
	
	public Integer getAndroidId() {
		return androidId;
	}
	public void setAndroidId(Integer androidId) {
		this.androidId = androidId;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	public String getCurrentVersion() {
		return currentVersion;
	}
	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}
	
	
}
