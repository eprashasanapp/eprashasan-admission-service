function convertEachWordInUppercase(word){
	var words = word.split(" ");
	var newSentence = "";
		for (var j = 0; j < words.length; j++){
	    	newSentence = newSentence+" "+words[j].charAt(0).toUpperCase() + words[j].substring(1).toLowerCase();
	}
	return newSentence;
}