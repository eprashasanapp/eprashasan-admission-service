<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4">
<head>
<meta charset="utf-8">
<title>OTP Verification</title>
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<style>
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/login.css'/>">
<script type="text/javascript">
function validateOtp() {
		var mobileNo = $("#mobileNum").val()
		if(mobileNo.length!=10) {
			alert("Please Enter Valid 10 digit Mobile No");
			document.getElementById("mobileNum").focus;
			return false;
		}
		var otp = $("#otpnum").val();
		if(otp.length!=6) {
			alert("Please Enter Valid 6 digit OTP");
			document.getElementById("otpnum").focus;
			return false;
			
		}
		var schoolId = document.getElementById("schoolKey").innerHTML;
        var data  = 'otpnum='+$("#otpnum").val()+'&mobileNum='+$("#mobileNum").val()+'&schoolId='+schoolId;
        $.ajax({
            type: "GET",
            url:  "/validateOtp",
            data: data,
            headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success : function(response) {
                 if(response.statusCode=="200") {
                	 window.location.href = "StudentAdmissionForm.jsp";
                 }
                 else if(response.statusCode=="404") { 
                	 alert("Invalid OTP! Please Try Again");
                 }
                 else if(response.statusCode=="500") { 
                	 alert("Invalid url! please try again with proper url")
                 }
                
            },
            error : function(xhr, status, error) {
            	alert("Invalid url! please try again with proper url")
            }
        });
}

function generateOTP() {
		var schoolId = document.getElementById("schoolKey").innerHTML;
		if(schoolId==null || schoolId=='' || schoolId==undefined || schoolId=='null') {
			alert("Invalid url! please try again with proper url")
			return false;
		}
		var mobileNo = $("#mobileNum").val()
		if(mobileNo.length!=10) {
			alert("Please Enter Valid Mobile No");
			document.getElementById("mobileNum").focus;
			return false;
			
		}
		else {
	        var data  = 'mobileNum='+$("#mobileNum").val()+'&schoolId='+schoolId;
	        $.ajax({
	            type: "POST",
	            url:  "/generateOtp",
	            data: data,
	            dataType: 'json',
	            cache: false,
	            timeout: 600000,
	            success : function(response) {
	            	if(response.statusCode=="1000") {
		            alert("User Already Exist.Please login and continue");
		            window.location.href = "login.jsp?schoolkey="+schoolId;
		            }
	            	else if(response.statusCode=="200") {
	            	  alert("OTP is sent.");
	            	}
	            	else {
	            		alert("Error While sending otp please try again")
	            	}
	            },
	            error : function(xhr, status, error) {
	                alert(xhr.responseText);
	            }
	        });
		}
	}

function validate(evt) {
	  var theEvent = evt || window.event;

	  // Handle paste
	  if (theEvent.type === 'paste') {
	      key = event.clipboardData.getData('text/plain');
	  } else {
	  // Handle key press
	      var key = theEvent.keyCode || theEvent.which;
	      key = String.fromCharCode(key);
	  }
	  var regex = /[0-9]|\./;
	  if( !regex.test(key) ) {n
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
	}
	
function getLetterHead() {
	//$("#letterHead").append('<img style="background-image:url(resources/img/bonafiedImgs.png);background-repeat:no-repeat;background-size:590px 200px;   width: 590px; height: 200px;"   />');
	var schoolId= document.getElementById('schoolKey').innerHTML;
	var data = 'schoolId=' + schoolId;
	//$("#letterHead").empty();
	 $.ajax({
       type: "GET",
       url : '/getLetterHead',
       data:data,
       async : false,
       dataType: 'json',
       timeout: 600000,
       success : function(response) {
     		var data = response.responseList;
			for(var i=0;i<data.length;i++){
				if(data[i].flag == 1){
					$("#letterHead").empty();
					$("#letterHead").append('<img style="border: 0px solid red;width:100%;height:200px;" src="data:image/png;base64,'+data[i].letterHeadImg+'"  />');
				}
			}
       },
       error : function(xhr, status, error) {
      	
       }
   }); 
}
</script>
</head>
<body onload="getLetterHead()">
  <div class="login-form" id="wrapper">
  	<div id="generateOtp">
		<table style="text-align: center;  margin: 0 auto;">
			<tr>
				<td colspan="2" id="letterHead">
					<div class="header">
						<h1><img style="border: 0px solid red;" src="<c:url value="/resources/img/newlogoTM.gif"/>" width="100%" height="110px"/></h1>
						<span><h3>Welcome to e-PRASHASAN</h3></span>
					</div>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
	 		<tr class="content">
	 			<td>
	 				<%
	 					String schoolKey= ""+request.getParameter("schoolkey");
	 				%>
	 				 <span id="schoolKey" style="display: none;"><%=schoolKey%></span>
	 				 <input type="text" class="input" name="mobileNum" id="mobileNum" value="${sessionScope.mobileNo}" onkeypress='validate(event)'  placeholder="Mobile No" />
	 			</td>
	 		</tr>
	 		<tr><td style="line-height: 1px;">&nbsp;</td></tr>
	 		<tr class="content" style="text-align: center;">
	 			<td>
	 				 <u><a onclick="generateOTP()" ><font size="2px" style="margin-left: 30% !important;color:#1717b5; cursor: pointer;">Send OTP</font></a></u>
	 			</td>
	 		</tr>
	 		<tr><td style="line-height: 1px;">&nbsp;</td></tr>
		  	<tr class="content">
		  		<td>
		  			<input type="text" class="input" name="otpnum" id="otpnum" onkeypress='validate(event)' placeholder="OTP"/>
		  		</td>
		  	</tr>
	  	</table>
	  	<br/><br/>
	  	<div class="footer" style="padding-bottom:59px;">
	  		<input type="submit" value="Submit" class="button" onclick="validateOtp()"/>
	  	</div>
	</div>
  </div>
  </body>
</html>
</body>
  