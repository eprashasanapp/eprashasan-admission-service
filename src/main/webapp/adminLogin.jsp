<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>User Login</title>
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<style>
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/login.css'/>">
<script type="text/javascript">
function adminLogin() {
		var schoolId = document.getElementById("schoolKey").innerHTML;
		var username = $("#userName").val();
		if(userName.length==0) {
			alert("Please Enter Username");
			document.getElementById("userName").focus;
			return false;
		}
		
		var password = $("#password").val();
		if(password.length==0) {
			alert("Please Enter Password");
			document.getElementById("password").focus;
			return false;
		}
		
		
        var data  = 'userName='+$("#userName").val()+'&password='+$("#password").val()+'&schoolId='+schoolId;
        $.ajax({
            type: "GET",
            url:  "/adminLoginLink",
            data: data,
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success : function(response) {
            	if(response.statusCode=='200') {
            		window.location.href = "AdmissionFormSetting.jsp";
            	}
            	else if(response.statusCode=='404') {
            		alert("Invalid UserName and Password. Please Try Again");
            	}
            	else {
            		alert("Invalid URL. Please Try Again");
            	}
                 
            },
            error : function(xhr, status, error) {
            	alert("Invalid URL. Please Try Again");
            }
        });
}

function getLetterHead() {
	var schoolId= document.getElementById('schoolKey').innerHTML;
	var data = 'schoolId=' + schoolId;
	$.ajax({
       type: "GET",
       url : '/getLetterHead',
       data:data,
       async : false,
       dataType: 'json',
       timeout: 600000,
       success : function(response) {
     		var data = response.responseList;
			for(var i=0;i<data.length;i++){
				if(data[i].flag == 1){
					$("#letterHead").empty();
					$("#letterHead").append('<img style="border: 0px solid red;width:100%;height:200px;" src="data:image/png;base64,'+data[i].letterHeadImg+'"  />');
				}
			}
 			
       },
       error : function(xhr, status, error) {
      	
       }
   }); 
}

</script>
</head>
<body onload="getLetterHead()">
  <div class="login-form" id="wrapper">
  	<div id="generateOtp">
		<table style="text-align: center;  margin: 0 auto;">
			<tr>
				<td colspan="2" id="letterHead">
					<div class="header">
						<h1><img style="border: 0px solid red;" src="<c:url value="/resources/img/newlogoTM.gif"/>" width="100%" height="110px"/></h1>
						<span><h3>Welcome to e-PRASHASAN</h3></span>
					</div>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
	 		<tr class="content">
	 			<td>
	 				<%
	 					String schoolKey= ""+request.getParameter("schoolkey");
	 				%>
	 				 <span id="schoolKey"  style="display: none;"><%=schoolKey%></span>
	 				 <input type="text" class="input" name="userName" id="userName" placeholder="User Name" required="required"/>
	 			</td>
	 		</tr>
	 		<tr class="content" style="text-align: right">
	 			<td>
	 				&nbsp;
	 			</td>
	 		</tr>
		  	<tr class="content">
		  		<td>
		  			<input type="password" class="input" name="password" id="password" placeholder="Password" required="required"/>
		  		</td>
		  	</tr>
	  	</table>
	  	<br/><br/>
	  	<div class="footer" style="padding-bottom: 59px;">
	  		<input type="submit" value="Login"  class="button" onclick="adminLogin()"/>
	  		<table style="width: 100%; margin-top: 50px;">
	  			
	  		</table>
	  	</div>
	</div>
  </div>
  </body>
</html>
  