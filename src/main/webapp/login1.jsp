<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">

<title>User Login</title>
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<style>

 
@media only screen 
and (max-device-width: 600px){
.regForm{
        margin-left: 150px !important;
    	width: 565px !important;
   }
   .regForm-tab{
   		width: 479px !important;
   }
} 

.regForm table .input {
    width: 188px;
    padding: 15px 25px;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-weight: 400;
    font-size: 14px;
    color: #9d9e9e;
    text-shadow: 1px 1px 0 rgba(255, 255, 255, 1);
    background: #fff;
    border: 1px solid #fff;
    border-radius: 5px;
}

.regForm{
	padding: 20px;
    width: 608px;
    display: block;
    /* margin-left: 550px; */
    /* height: 536px; */
    background-color: #d4dedf;
    margin-top: 50px;
    margin-left: 550px;
}

.center {
    width: 500px;
	height: 100px;
	/* background-color: white; */	
	position: absolute;
	top:0;
	bottom: 0;
	left: 0;
	right: 0;  	
	margin: auto;
}
.input-select{
    width: 239px !important;
 }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script src="<c:url value='/resources/js/ingenioJs/tcreview.js' />"></script>
<script src="<c:url value='/resources/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/login.css'/>">
<script type="text/javascript" src='<c:url value="/resources/js/Forgot.js"/>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/UtilityClass.js"/>'></script>

<script type="text/javascript">


function b(){
	var successflag1= document.getElementById("successflag").innerHTML;
	if(successflag1 == 'true'){
	alert("Your payment and Registration Successful . Login with Mobile no . and Password .")
	}else{
	alert("Something went wrong .registration Failed please try again.")			
	}
	var schoolId = document.getElementById("schoolKey").innerHTML;
	 window.location.href = "login?schoolkey="+schoolId;	
}

 </script>

</head>
<body onload="b();">
	<%
	    String successflag = (String) session.getAttribute("successflag");
		String schoolKey = (String) session.getAttribute("schoolId");
	    String newStudentvalue = (String)session.getAttribute("newStudent"); 
	%>
	
  <div class="center" > 
  	 <span id="schoolKey"  style="display: none;"><%=schoolKey%></span>
  	  <span id="successflag"  style="display: none;"><%=successflag%></span>
			</div>	
		</body>
		</html>
  