<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link rel="stylesheet"
	href='<c:url value="/resources/css/jquery.ui.all.css"/>'>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4">

<head>
<meta charset="utf-8">

<title>Student Admission Form</title>
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<style>
</style>
<script src="${contextPath}/resources/js/jquery-1.10.2.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script src='<c:url value="/resources/js/jquery-1.10.2.js"/>'></script>
<script src='<c:url value="/resources/js/jquery.ui.core.js"/>'></script>
<script src='<c:url value="/resources/js/jquery.ui.widget.js"/>'></script>
<script src='<c:url value="/resources/js/jquery.ui.datepicker.js"/>'></script>
<script src='<c:url value="/resources/css/bootstrap.min.css"/>'></script>
<script src='<c:url value="/resources/js/bootstrap.min.js"/>'></script>

<style type="text/css">

.myButton {
	border-radius: 6px !important;
	width: 100px !important;
}

body, html {
	height: 100%;
	margin: 0;
	font-family: Arial, Helvetica, sans-serif;
}

.fieldset {
	margin-bottom: 10px;
}

.tablink, .stdlink {
	/* background-color: #678889; */
	background-color: #337e96;
	color: white;
	float: left;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 0px;
	font-size: 17px;
	width: 20%;
}

.stdlink {
	width: 9%;
}

.tablink:hover {
	background-color: rgb(167, 176, 177);
}

.tabcontent {
	color: white;
	display: none;
	padding: 20px 20px;
	min-height: 550px;
	height: 100%;
	background-color: #f3f3f3;
	text-align: center;
}

.hide {
	display: none;
}

.legend {
	color: rgb(160, 5, 11);
	font-size: 17px;
	font-weight: bold;
}

.inputSelect {
	width: 375px;
	height: 35px;
	font-size: 20px;
	background-color: #f7f7f7;
    font-size: 17px;
}

.inputText {
	width: 375px;
	height: 35px;
	font-size: 20px;
	background-color: #f7f7f7;
    font-size: 17px;
}

.textArea {
	width: 375px;
    font-size: 17px;
}

.dd, .mm, .yy {
	height: 35px;
	width: 121px;
	font-size: 20px;
	background-color: #f7f7f7;
    font-size: 17px;
}

.legendContent {
	line-height: 2;
	margin-bottom: 10px;
	margin-left: 245px;
}

.submit {
	width: 100px;
	border: 1px solid #25729a;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	font-size: 12px;
	font-family: arial, helvetica, sans-serif;
	padding: 10px 10px 10px 10px;
	text-decoration: none;
	display: inline-block;
	text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.3); /* font-weight:bold; */
	color: #FFFFFF;
	background-color: #3093c7;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#3093c7),
		to(#1c5a85));
	background-image: -webkit-linear-gradient(top, #3093c7, #1c5a85);
	background-image: -moz-linear-gradient(top, #3093c7, #1c5a85);
	background-image: -ms-linear-gradient(top, #3093c7, #1c5a85);
	background-image: -o-linear-gradient(top, #3093c7, #1c5a85);
	background-image: linear-gradient(to bottom, #3093c7, #1c5a85);
	filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,
		startColorstr=#3093c7, endColorstr=#1c5a85);
}

.submit:hover {
	border: 1px solid #1c5675;
	background-color: #26759e;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#26759e),
		to(#133d5b));
	background-image: -webkit-linear-gradient(top, #26759e, #133d5b);
	background-image: -moz-linear-gradient(top, #26759e, #133d5b);
	background-image: -ms-linear-gradient(top, #26759e, #133d5b);
	background-image: -o-linear-gradient(top, #26759e, #133d5b);
	background-image: linear-gradient(to bottom, #26759e, #133d5b);
	filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,
		startColorstr=#26759e, endColorstr=#133d5b);
}

.required {
	color: rgb(216, 13, 21);
	font-size: 20px;
	font-weight: bold;
}

.attachmentLabels {
	font-size: 14px;
	color: #052396;
	font-weight: bold;
}

.fileName {
	background-color: transparent;
	border: 0px;
	height: 20px !important;
	font-size: 14px;
	color: rgb(216, 13, 21);
	outline: 0;
	cursor: pointer
}

.logout {
	color: #c10d0d;
	background-color: transparent;
	border: 0px;
	font-size: 18px;
	font-weight: bold;
	cursor: pointer;
}

#title {
	width: 250px;
}

.modalCenter {
	top: 50% !important;
	transform: translateY(-50%) !important;
}

.modal-backdrop {
    display:none;
}

.newStyle{
	font-size: 18px;
	margin-top: 20px;
}
.standardData{
    margin-top: -5px;
    width: 1000px;
    margin-left: -3px;
}
</style>

<script type="text/javascript">
var standardList =[];
var dyncamicCheckBoxTitles=[];
var dyncamicCheckBoxValueArray=[];
var value=0;
var formNoValue=0;
var globalyearId=0;
var globalstandardId=0;
var subjectStandardId=0;
var newYearId;
var renewId;
var attachmentImageUrl ;

function getStandardAndYearCombo() {
/* 	 $('#changepasswordmodel').addClass('hide'); */
	var studentIdForPayment = '${sessionScope.studentId}';
	var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML+'&studentId='+studentIdForPayment;
	$.ajax({
         type: "GET",
         url : 'showPaymentButton',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
        	 if(!response.responseData) {
        		document.getElementById("PaymentButton").style.display='none';
        	 }
         },
         error : function(xhr, status, error) {
         }
     });
	
	data  = 'schoolId='+document.getElementById("schoolKey").innerHTML;
	
	var globalAccademicYearId;
	$.ajax({
         type: "GET",
         url : 'getAcademicYear',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
        	 globalAccademicYearId = response.responseData;
         },
         error : function(xhr, status, error) {
         }
     });
	 $.ajax({
         type: "GET",
         url : 'getAcademicYearCombo',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
       		var data = response.responseList;
      			$(data).each(function() {
      			if(this.yearId==globalAccademicYearId){
      				//if(this.yearName=="2020-2021"){	
	      				var option = $('<option/>');
	      				option.attr('value', this.yearId).text(this.yearName);
	      				$(".AcademicYear").append(option);
	      			}
      				var option1 = $('<option/>');
      				option1.attr('value', this.yearId).text(this.yearName);
      				$(".PreviousYear").append(option);
      				
      			});
         },
         error : function(xhr, status, error) {
         }
     });
	 
	$.ajax({
        type: "GET",
        url : '/getStandardCombo',
        data:data,
        async : false,
        dataType: 'json',
        timeout: 600000,
        success : function(response) {
      		var data = response.responseList;
      		standardList = response.responseList;
      		var i=0;
  			 $(data).each(function() {
  				var option = $('<option/>');
  				option.attr('value', this.standardId).text(this.standardName);
  				$("#Standard").append(option);
	  				
  			}); 
        },
        error : function(xhr, status, error) {
        }
    }); 
	
	//var i1=1;
	//$("#letterHead").append('<img style="background-image:url(resources/img/bonafiedImgs'+i1+'.png);background-repeat:no-repeat;background-size:1000px 200px;   width: 1000px; height: 200px;"   />');
	$("#letterHead").empty();
	 $.ajax({
        type: "GET",
        url : '/getLetterHead',
        data:data,
        async : false,
        dataType: 'json',
        timeout: 600000,
        success : function(response) {
      		var data = response.responseList;
			for(var i=0;i<data.length;i++){
				if(data[i].flag == 1){
					$("#letterHead").append('<img style="border: 0px solid red;width:100%;height:175px;" src="data:image/png;base64,'+data[i].letterHeadImg+'"  />');
				}
			}
  			
        },
        error : function(xhr, status, error) {
       	
        }
    }); 
	 
	 
	 var studentID = '${sessionScope.studentId}';
		
		


	 var data  = 'studentId='+studentID;
		
		$.ajax({
	        type: "GET",
	        url : '/getStandardandYear',
	        data:data,
	        async : false,
	        dataType: 'json',
	        timeout: 600000,
	        success : function(response) {
	        	console.log(response);        
	             
	        	var data = response.responseList;   	
				 var yearName =data[0].yearName;
				 renewId =data[0].renewId;
				 newYearId=data[0].yearId;
				 globalyearId=data[0].yearId;
				var globalstandardId= data[0].standardId; 
				subjectStandardId=data[0].standardId; 
				document.getElementById("academicYear").innerHTML = data[0].yearName;
				
				document.getElementById("standardId").innerHTML= data[0].standardName;
				 getTabsAndLegendsSetting1(globalyearId,globalstandardId); 
				 
		  				
	  			
	        },
	        error : function(xhr, status, error) {
	        }
	    });   
	     
	 
	 var nowdate = new Date();
	 var days = ("0" + nowdate.getDate()).slice(-2);
	 var month = ("0" + (nowdate.getMonth() + 1)).slice(-2);
	 currentDate = days + "-" + month + "-"+ nowdate.getFullYear();
	document.getElementById("AdmissionDateSpan").innerHTML=currentDate;
	document.getElementById("AdmissionDate").value=currentDate;
	
}
var allAttachmentLength;
	 
function getCombos() {
	
	$(".inputDate").datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		yearRange : "1920:2050",
		dateFormat : "dd-mm-yy"
	}) ;
	var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML;
	
	/* $("#letterHead").empty();
	 $.ajax({
         type: "GET",
         url : '/getLetterHead',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
       		var data = response.responseList;
			for(var i=0;i<data.length;i++){
				if(data[i].flag == 1){
					$("#letterHead").append('<img style="border: 0px solid red;width:100%;height:220px;" src="data:image/jpg;base64,'+data[i].imageData+'"  />');
				}
			}
   			
         },
         error : function(xhr, status, error) {
        	
         }
     }); */
	 
	
	/*  $.ajax({
         type: "GET",
         url : '/getGRCombo',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
       		var data = response.responseList;
       			if(data.length==1) {
       				$(".GRBookName").empty();
       			}
      			$(data).each(function() {
      				var option = $('<option/>');
      				option.attr('value', this.grBookId).text(this.grBookName);
      				$(".GRBookName").append(option);
      			});
   			
         },
         error : function(xhr, status, error) {
         }
     }); */
	 
	 $.ajax({
         type: "GET",
         url : '/getCategoryCombo',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
       		var data = response.responseList;
   			$(data).each(function() {
   				var option = $('<option/>');
   				option.attr('value', this.categoryId).text(this.categoryName);
   				$(".Category").append(option);
   			});
         },
         error : function(xhr, status, error) {
         }
     }); 
	 
	 
	 $.ajax({
         type: "GET",
         url : '/getCasteCombo',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
       		var data = response.responseList;
   			$(data).each(function() {
   				var option = $('<option/>');
   				option.attr('value', this.casteId).text(this.casteName);
   				$(".Caste").append(option);
   			});
         },
         error : function(xhr, status, error) {
         }
     }); 
	 
	 $.ajax({
         type: "GET",
         url : '/getReligionCombo',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
       		var data = response.responseList;
   			$(data).each(function() {
   				var option = $('<option/>');
   				option.attr('value', this.religionId).text(this.religionName);
   				$(".Religion").append(option);
   			});
         },
         error : function(xhr, status, error) {
         }
     }); 
	 $.ajax({
         type: "GET",
         url : '/getStudentAttachments',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
       		var data = response.responseList;
       		attachmentImageUrl = data;
       		var table="";
       		table+='<table>';
			table+='<input type="file" id="file" name="file" style="display: none;" >';
			table+='<a style="cursor:pointer;" style="display: none;" class="attachmentLabels" title=""></a></label> ';
			table+='<input style="display: none;" name="attachmentArray" type="checkbox" id="attachmentCheckBox" disabled="disabled" value="" /> '; 
			table+='<input type="hidden"  id="fileName" name="fileName" class="fileName"/> ';
			table+='<input type="hidden" id="attachmentName" name="attachmentIdNameArr" value="default"/> ';
			table+='<input type="hidden" id="attachmentID" name="attachmentIdArr" value="default"/>';
			table+='<input type="hidden" id="filePath" name="filePath"/>';
			table+='<input type="hidden" id="studentFileAttachmentID" name="studentFileAttachmentIDArray" value="default"/>';	
   			if(data.length>0) {
   				allAttachmentLength=data.length;
	   			for(var i=0;i<data.length;i++){
	   				table+='<tr><td><label>';
	   				table+='<input type="file" id="file'+data[i].attachmentId+'" name="file" style="display: none;" ';
	   				table+='onchange="checkAttachment('+data[i].attachmentId+');checkfileAttachment(this,'+data[i].attachmentId+',\''+data[i].attachmentName+'\'); "> ';
	   				table+='<a style="cursor:pointer;" class="attachmentLabels" title="'+data[i].attachmentName+'"><u>'+data[i].attachmentName+'&nbsp;&nbsp;</u></a></label> ';
					table+='</td><td><input name="attachmentArray" type="checkbox" id="attachmentCheckBox'+data[i].attachmentId+'" disabled="disabled" ';
					table+='value="'+data[i].attachmentId+'" onFocus="checkCkeckBox(this,'+data[i].attachmentId+',\''+data[i].attachmentName+'\');"/> '; 
					table+='</td><td><input type="text" onClick="openFile('+data[i].attachmentId+');" style="width:300px;" id="fileName'+data[i].attachmentId+'" name="fileName" class="fileName"/> ';
					table+='<input type="hidden" id="attachmentName'+data[i].attachmentId+'" name="attachmentIdNameArr" value="default"/> ';
					table+='<input type="hidden" id="attachmentID'+data[i].attachmentId+'" name="attachmentIdArr" value="default"/>';
					table+='<input type="hidden" id="filePath'+data[i].attachmentId+'" name="filePath" class="filePath" />';
					table+='<input type="hidden" id="studentFileAttachmentID'+data[i].attachmentId+'" name="studentFileAttachmentIDArray" value="default"/></td></tr>';	
	   			}
	   			
   			}
   			 else {
   				table+='<input type="file" name="file" id="defaultFile" style="display: none;">';
				table+='<input type="text" name="fileName" id="defaultName" style="display: none;">';
   			} 
            
       		table+='<tr><td colspan="3" id="loading"><img style="background-color: transparent; width:100px;height:100px" src="resources/img/img.gif"  />'
       	    table+='</td></tr>'
       		table+='<tr><td colspan="3"><table id ="attachmentImages"></table>'
       		table+='</td></tr></table>'
   			
   			var className = document.getElementsByClassName("Attachment");
   		 	for(var i=0;i<className.length;i++) {
   				className[i].innerHTML= table;
   			} 
         },
         error : function(xhr, status, error) {
         }
     });
	 
	var yearId = document.getElementById("globalAcademicYear").value;
	var standardId = document.getElementById("Standard").value;
	var schoolId  = document.getElementById("schoolKey").innerHTML;
	var data  = 'schoolId='+schoolId+'&yearId='+yearId+'&standardId='+standardId;
	 $.ajax({
        type: "GET",
        url:  "/getAssignedSubjects",
        data: data,
        dataType: 'json',
        async:false,
        cache: false,
        timeout: 600000,
        async:false,
        success : function(response) {
        	data=response.responseList;
        	var firstCategory="";
        	var assignSubjectTable="<table><tr>";
        	for(var i=0;i<data.length;i++) {
				
				if(firstCategory!=data[i].subjectCategoryName && firstCategory!="") {
					assignSubjectTable+='</table>';
					assignSubjectTable+='</tr>';
				}
				if(firstCategory!=data[i].subjectCategoryName) {
					assignSubjectTable+='<td>';
					assignSubjectTable+='<table style="float:left;width:600px; ">'
					assignSubjectTable+='<tr><td style="color : #2a6496;">';
					assignSubjectTable+='<h4>'+data[i].subjectCategoryName+':</h4>'; 
					assignSubjectTable+='</td></tr><tr>';
				}
			
				if(i%5==0) {
					
					assignSubjectTable+='</tr>';
					assignSubjectTable+='<tr>'
				}
				assignSubjectTable+='<td style="vertical-align:inherit !important; width:50px;word-wrap: break-word; text-align:right;">';
				assignSubjectTable+='<input type="checkbox" name="selectedSubjectArr" id="selectedSubjectId'+data[i].subjectId+'" value="'+data[i].subjectId+'" />'
				assignSubjectTable+='</td>';
				assignSubjectTable+='<td style="vertical-align:inherit !important;width:70px! important;">'; 
				assignSubjectTable+=data[i].subjectName; 
				assignSubjectTable+='</td>';
				
				firstCategory = data[i].subjectCategoryName;
			}
        	assignSubjectTable+="</tr></table>";
        	var className = document.getElementsByClassName("Subjects");
   		 	for(var i=0;i<className.length;i++) {
   				className[i].innerHTML= assignSubjectTable;
   			} 
        },
        error : function(xhr, status, error) {
        }
    });
	 
		
	 for(var i=1;i<=31;i++) {
		 var option = $('<option/>');
		 if(i<10) {
			 option.attr('value',"0"+i).text("0"+i); 
		 }
		 else {
			 option.attr('value',i).text(i);
		 }
		
		 $(".dd").append(option);
	 }
	 
	 for(var i=1;i<=12;i++) {
		 var option = $('<option/>');
		 if(i<10) {
			 option.attr('value',"0"+i).text("0"+i); 
		 }
		 else {
			 option.attr('value',i).text(i);
		 }
		 $(".mm").append(option);
	 }
	 
	 for (i = new Date().getFullYear(); i > 1900; i--)
	 {
	     $('.yy').append($('<option />').val(i).html(i));
	 }
	 
	 var medium = ['English','Semi English','Marathi','Hindi','Gujrati'];
	 for(var i=0;i<medium.length;i++) {
		 var option = $('<option/>');
		 option.attr('value', medium[i]).text(medium[i]);
		 $(".Medium").append(option);
	 }
	 
	 var initialName = ['Mr.','Miss.','Ku.','Kumar','Kumari','Master','Mrs'];
	 for(var i=0;i<initialName.length;i++) {
		 var option = $('<option/>');
		 option.attr('value', initialName[i]).text(initialName[i]);
		 $(".MissMr").append(option);
	 }
	 
	 var gender = ['Male','Female'];
	 for(var i=0;i<gender.length;i++) {
		 var option = $('<option/>');
		 option.attr('value', gender[i]).text(gender[i]);
		 $(".Gender").append(option);
	 }
	 
	 var yesNo = ['Yes','No'];
	 for(var i=0;i<yesNo.length;i++) {
		 var option = $('<option/>');
		 option.attr('value', yesNo[i]).text(yesNo[i]);
	 }

}


function checkCkeckBox(sender,attachmentId,attachmentName) {

	if (document.getElementById("attachmentCheckBox"+attachmentId).checked) {
		document.getElementById("attachmentName"+attachmentId).value=attachmentName;
		document.getElementById("attachmentID"+attachmentId).value=attachmentId;
	} else {
		
		document.getElementById("attachmentName"+attachmentId).value="";
		document.getElementById("attachmentID"+attachmentId).value="default";
		document.getElementById("fileName"+attachmentId).value="";
	}
}

function changepassword(id){
	
	if(id==1)
	{	var oldpassword="default123";	
		var newpass = document.getElementById("changepassword1").value;
		var newpass1 =document.getElementById("confirmpassword1").value;
	}
	else{
	var oldpassword = $('input:password[name="oldpassword"]').val();
	var newpass = $('input:password[name="changepassword"]').val();
	var newpass1 = $('input:password[name="confirmpassword"]').val(); 
	}
	
	if(oldpassword==""){
		alert("Old password Should not be Empty.")
		document.getElementById("oldpassword").focus;
		return false;
	}
	if(newpass=="") {
		alert("New password Should not be Empty.")
		document.getElementById("changepassword").focus;
		return false;
	}
	if(newpass!=newpass1){
		alert("New Password and Confirm Password does not match.")
		document.getElementById("changepassword").focus;
		return false;
	}
	
	var studentID = '${sessionScope.studentId}';
	var userName='${sessionScope.userName}'; 
	var schoolId = document.getElementById("schoolKey").innerHTML;
	var data  = 'studentId='+studentID+'&userName='+userName+'&oldPassword='+oldpassword+'&newPassword='+newpass+ '&schoolId='+schoolId;
	
 $.ajax({
     type: "GET",
 	 url : '/changePassword', 			 
     data:data,
     async : false,
     dataType: 'json',
     timeout: 600000,
     success : function(response) {
    	 if(response.statusCode=="200") {
			 alert("Password Changed Successfully.")
			$(".close").trigger("click");

         }
         else if(response.statusCode=="404") { 
        	 alert( "Please Try Again");
         }
         else if(response.statusCode=="500") { 
        	 alert("Invalid url! please try again with proper url")
         }
    	
     },
     error : function(xhr, status, error) {
     }
 });
 	
}



function checkfileAttachment(sender,attachmentId,attachmentName) {

	    var validExts = new Array(".xls", ".xlsx",".docx",".doc",".rtf",".jpeg",".tif", ".tiff",".png",".jpg",".indd", ".ai",".orf",".nef",".eps", ".cr2","psd",".pdf",".txt");
	    var fileExt = sender.value;
	    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
	    alert(fileExt);
	    if (validExts.indexOf(fileExt) < 0) {    
	      $('#fileName'+attachmentId).val('');
	      document.getElementById("attachmentCheckBox"+attachmentId).checked=false;
	      $('#attachmentCheckBox'+attachmentId).attr('checked', false);
	      alert("Please select a valid file format");
	      document.getElementById("stdentFile55").value="default";
	      return false;
	    }
	    else {
	    	
	    	document.getElementById("attachmentCheckBox"+attachmentId).checked=true;
	    	document.getElementById("stdentFile55").value=document.getElementById("file55").value;
	    	$('#attachmentCheckBox'+attachmentId).attr('checked', true);
	    	document.getElementById("attachmentCheckBox"+attachmentId).disabled = false;
	    	document.getElementById("attachmentName"+attachmentId).value=attachmentName;
	    	document.getElementById("attachmentID"+attachmentId).value=attachmentId;
	    	return true;
	    }
	}

$('#file').change(function() {
    $('#fileName').val($(this).val().split(/\\|\//).pop());
});


function openFile(attachmentId) {
	 var fileName =  document.getElementById("fileName"+attachmentId).value;
	 var filePath =  document.getElementById("filePath"+attachmentId).value;
	 
	 if(filePath!='') {
		 if(fileName.charAt(0)==' ') {
			 fileName = fileName.substr(1);
		 }
		 file = fileName;
		 var validExts = new Array(".png", ".jpeg",".jpg",".pdf",".xls", ".xlsx",".docx",".doc",".rtf", ".txt");
		 fileName = fileName.substring(fileName.lastIndexOf('.'));
		  if (validExts.indexOf(fileName) < 0) {
		      alert("Please select a valid file format");
		      return false;
		 } 
		window.open(filePath);
	 }
	
}
function checkAttachment(attachmentId){
	var fullPath = document.getElementById('file'+attachmentId).value;
	if (fullPath) {
	    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
	    var filename = fullPath.substring(startIndex);
	    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
	        filename = filename.substring(1);
	    }
	    $('#fileName'+attachmentId).val(filename);
	}
}

var groupsubjectsCountMap = new Object();
var groupTotalSubCountMap = new Object();
var totalSelectedsubjects=0;
var selectedSubArray=[];
var selectedCumpolsrySubDetails = [];
var selectedElectiveSubDetails = [];

var cumpolsrySubjectsCount=0;
function openSubject(pageName, elmnt,className,linkId,i,tabId) {
	 var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName(className);
		
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }
	  document.getElementById("subjectContent").style.display = "block";
	  var studentIdForEdit = '${sessionScope.studentId}';
	  var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML+'&yearId='+globalyearId+'&standardId='+subjectStandardId+'&studentId='+studentIdForEdit;
	  $("#electiveSubjects").empty();
	  $("#cumpolsrySubjects").empty();
	  $.ajax({
	        type: "GET",
	        url : '/getSubjectList',
	        data:data,
	        async : false,
	        dataType: 'json',
	        timeout: 600000,
	        success : function(response) {
	        	var cumpolsrySubjectList = response.responseData.cumpolsrySubjectList;
	        	var cumpolsryIndex=1;
	        	var table="";
	        	table+='<tr><td colspan="2"><b>Total Subjects :</b><input type="text" style="border:0;background-color:transparent;" id="grandTotalSubjects" value="'+response.responseData.totalSubjects+'">&nbsp;&nbsp;'
	        	table+='<span id="grandSubjectsError" style="color:red;"></span></td></tr>'
	        	table+='<tr><td colspan="2">&nbsp;</td></tr>'
	        	table+='<tr><td colspan="2"><b>Cumpolsry Subject/Course</b></td></tr><tr><td colspan="2">&nbsp;</td></tr>'

	        	 for(var i=0;i<cumpolsrySubjectList.length;i++) {
	        		cumpolsrySubjectsCount++;
	        		table+='<tr>';
	        		table+='<td  colspan="2">'+(cumpolsryIndex)+'.&nbsp;&nbsp;&nbsp;';
	        		table+=cumpolsrySubjectList[i].subjectCode+'&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;'+cumpolsrySubjectList[i].subjectName+'</td>';
	        		table+='</tr>';
	        		selectedSubArray.push(cumpolsrySubjectList[i].subjectId);
	        		selectedCumpolsrySubDetails.push(cumpolsryIndex + ". "+cumpolsrySubjectList[i].subjectCode +" " + cumpolsrySubjectList[i].subjectName);
	        		cumpolsryIndex++;
	        	 } 
	 			$("#cumpolsrySubjects").append(table);
	 			
	 			
	 			var electiveSubjectList = response.responseData.electiveGroupBean;
	        	var electiveIndex=1;
	        	table="";
	        	table+='<tr><td colspan="2"><b>Elective Subject/Course</b></td></tr><tr><td  colspan="2">&nbsp;</td></tr>'
	        	
	        	for(var i=0;i<electiveSubjectList.length;i++) {
	        		groupsubjectsCountMap[electiveSubjectList[i].groupId]=0;
	        		groupTotalSubCountMap[electiveSubjectList[i].groupId]=electiveSubjectList[i].totalSubjects;
	        		table+='<tr>';
	        	    table+='<td colspan="100%"><label><input type="checkbox" id="groupCheck'+electiveSubjectList[i].groupId+'" class="groupCheck" name="groupCheck"></input>&nbsp;'+electiveSubjectList[i].groupName+'</label>';
	        		table+='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight:bold;">Total Subjects:&nbsp;&nbsp;</span><span ="totalGrpSubCount'+electiveSubjectList[i].groupId+'">'+electiveSubjectList[i].totalSubjects+'</span>'
	        		table+='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:red;" id="totalError'+electiveSubjectList[i].groupId+'" class="groupError"></span></td>'
	        		table+='</tr>';
	        		
	        		for(var j=0;j<electiveSubjectList[i].subGroupList.length;j++) {
	        		   var subGroup = electiveSubjectList[i].subGroupList[j];
	        		   if(j%2==0) {
	        			   table+='<tr>'
	        		   }
	        		   table+='<td style="vertical-align:top;"><table style="width:100%;margin-left:50px;margin-right:50px;"><tr>'
	        		   if(electiveSubjectList[i].selectionType=="0") {
	        		   		table+='<td><label><input type="checkbox"  value="'+subGroup.subGroupId+'" id="subGroupCheck'+subGroup.subGroupId+'" class="subGroupCheck'+electiveSubjectList[i].groupId+'" name="subGroupCheck"></input>&nbsp;&nbsp;&nbsp;'+subGroup.subGroupName+'</label></td>';
	        		    }
		        	    else {
		        	    	table+='<td><label><input type="radio" value="'+subGroup.subGroupId+'" onclick="changeSubgroupChecks('+subGroup.subGroupId+','+electiveSubjectList[i].groupId+')" id="subGroupCheck'+subGroup.subGroupId+'" class="subGroupCheck'+electiveSubjectList[i].groupId+'" name="subGroupCheck"></input>&nbsp;&nbsp;&nbsp;'+subGroup.subGroupName+'</label></td>';
		        	    }
	        		   if(subGroup.subjectList!= undefined && subGroup.subjectList.length>0) {
	        			  for(var k=0;k<subGroup.subjectList.length;k++) {
	  		        		   var electiveSubject = subGroup.subjectList[k];
	  		        		   table+='<tr><td><table style="width:100%;margin-left:50px;"><tr>';
	  		        		   if(subGroup.selectionType=="0") {
	  		        			 table+='<td><label><input type="checkbox" value="'+electiveSubject.subjectId+'" onclick="changeSelectedSubNumber('+subGroup.subGroupId+','+electiveSubjectList[i].groupId+','+electiveSubject.subjectId+',0)" id="electiveSubject'+electiveSubject.subjectId+'" class="electiveSubjectCheckBox" name="electiveSubject'+electiveSubjectList[i].groupId+''+subGroup.subGroupId+'"></input>&nbsp;';
	  		        		   }
	  		        		   else {
	  		        			 table+='<td><label><input type="radio" value="'+electiveSubject.subjectId+'" onclick="changeSelectedSubNumber('+subGroup.subGroupId+','+electiveSubjectList[i].groupId+','+electiveSubject.subjectId+',1)"  id="electiveSubject'+electiveSubject.subjectId+'" class="electiveSubjectRadio" name="electiveSubject'+electiveSubjectList[i].groupId+''+subGroup.subGroupId+'"></input>&nbsp;';
	  		        		   }
	  		        		  
	  		        		   table+='<span id="subjectCode'+electiveSubject.subjectId+'">'+electiveSubject.subjectCode+'</span>&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<span id="subjectName'+electiveSubject.subjectId+'">'+electiveSubject.subjectName+'</span></label></td>';
	  		        		   table+='</tr></table></td></tr>';
	  	        		   }
	        		  }  
	        		   table+='</tr></table></td>'
	        		   if(j%2!=0) {
	        			   table+='</tr>'
	        		   }
	        		  
	        		}
	        	}
	 			$("#electiveSubjects").append(table);
	 			checkSubjectsForEdit(response.responseData.subjectsForEdit);
	        },
	        error : function(xhr, status, error) {
	        }
	    });  
}

function checkSubjectsForEdit(subjectsForEditList) {
	
	
	  for(var i=0;i<subjectsForEditList.length;i++) {
		  $('#electiveSubject'+subjectsForEditList[i].subjectId).trigger('click');
	  
		//  document.getElementById("electiveSubject"+subjectsForEditList[i].subjectId).checked=true;
	  }

}

function changeSelectedSubNumber(subGroupId,groupId,subjectId,type) {
	if(type==0) {
		if(document.getElementById("electiveSubject"+subjectId).checked==true) {
			groupsubjectsCountMap[groupId] = parseInt(++groupsubjectsCountMap[groupId]);
			++totalSelectedsubjects;
		}
		else {
			groupsubjectsCountMap[groupId] = parseInt(--groupsubjectsCountMap[groupId]);
			--totalSelectedsubjects;
		}
	}
	
	if(type==1) {
		if($('#electiveSubject'+subjectId).attr("checked", "checked")) {
			groupsubjectsCountMap[groupId] = parseInt(++groupsubjectsCountMap[groupId]);
			++totalSelectedsubjects;
		}
		else {
			groupsubjectsCountMap[groupId] = parseInt(--groupsubjectsCountMap[groupId]);
			--totalSelectedsubjects;
		}
	}
	if(groupsubjectsCountMap[groupId]>parseInt(groupTotalSubCountMap[groupId])) {
		document.getElementById("totalError"+groupId).innerHTML='<span style="font-weight:bold;">Error : </span> Selected subjects are greater than total subjects allowed in a group'
		//	document.getElementById("electiveSubject"+subjectId).checked=false;
	}
	else {
		document.getElementById("totalError"+groupId).innerHTML="";
	}
	
	var subjectChecked=false;
	var checkedsubjects = document.getElementsByName("electiveSubject"+groupId+subGroupId);
	for(var i=0;i<checkedsubjects.length;i++) {
		if(checkedsubjects[i].checked==true) {
			document.getElementById("subGroupCheck"+subGroupId).checked=true;
			document.getElementById("groupCheck"+groupId).checked=true;
			subjectChecked=true;
		}
	}
	if(!subjectChecked) {
		document.getElementById("subGroupCheck"+subGroupId).checked=false;
		document.getElementById("groupCheck"+groupId).checked=false;
	}
	if((cumpolsrySubjectsCount+totalSelectedsubjects)>document.getElementById("grandTotalSubjects").value) {
		document.getElementById("grandSubjectsError").innerHTML='<span style="font-weight:bold;">Error : </span> Selected subjects are greater than total subjects allowed'
	}
	else {
		document.getElementById("grandSubjectsError").innerHTML='';
	}
	changeSubgroupChecks(subGroupId,groupId)
}

function changeSubgroupChecks(subgroupId,groupId) {

	var unselectedSubGroups = document.getElementsByClassName("subGroupCheck"+groupId)
	for(var p=0;p<unselectedSubGroups.length;p++) {
		if(unselectedSubGroups[p].checked==false) {
			var checkedsubjects = document.getElementsByName("electiveSubject"+groupId+unselectedSubGroups[p].value);
			for(var i=0;i<checkedsubjects.length;i++) {
			    if(checkedsubjects[i].checked==true) {
			    	checkedsubjects[i].checked=false;
			    	--totalSelectedsubjects;
			    	groupsubjectsCountMap[groupId] = parseInt(--groupsubjectsCountMap[groupId]);
			    	 if(groupsubjectsCountMap[groupId]>parseInt(groupTotalSubCountMap[groupId])) {
			    		document.getElementById("totalError"+groupId).innerHTML='<span style="font-weight:bold;">Error : </span> Selected subjects are greater than total subjects allowed in a group'
			    		//	document.getElementById("electiveSubject"+subjectId).checked=false;
			    	}
			    	else {
			    		document.getElementById("totalError"+groupId).innerHTML="";
			    	} 
			   }
				
			}
		}
		
	}
	
	
}

function saveSubjectsData() {
	$("#subjectPopupTable").empty();
	var errorClass=document.getElementsByClassName("groupError");
	for(var i=0;i<errorClass.length;i++){
		if(errorClass[i].innerHTML!='') {
			alert("Please clear all the errors");
			return false;
		}
	}
	if(document.getElementById("grandSubjectsError").innerHTML!='') {
		alert("Please clear all the errors");
		return false;
	}
	
	selectedElectiveSubDetails=[];
	var selectSubCheckClass= document.getElementsByClassName("electiveSubjectCheckBox");
	for(var i=0;i<selectSubCheckClass.length;i++) {
		if(selectSubCheckClass[i].checked==true) {
			selectedSubArray.push(selectSubCheckClass[i].value);
			selectedElectiveSubDetails.push(document.getElementById("subjectCode"+selectSubCheckClass[i].value).innerHTML + " " + document.getElementById("subjectName"+selectSubCheckClass[i].value).innerHTML);
		} 
	}
	var selectSubCheckClass= document.getElementsByClassName("electiveSubjectRadio");
	for(var i=0;i<selectSubCheckClass.length;i++) {
		if(selectSubCheckClass[i].checked==true) {
			selectedSubArray.push(selectSubCheckClass[i].value);
			selectedElectiveSubDetails.push(document.getElementById("subjectCode"+selectSubCheckClass[i].value).innerHTML + " " + document.getElementById("subjectName"+selectSubCheckClass[i].value).innerHTML);
		}
	}
	/* 
		var totalSubject = document.getElementById("grandTotalSubjects").value;
		if(selectedSubArray.length!=totalSubject) {
			alert("Please select "+(totalSubject-selectedSubArray)+" more subjects");
		}
	}*/

	$("#subjectConfirmationPopup").trigger('click');
	var table="";
	table+="<tr><td><b>Cumpolsry subjects</b></td></tr>"
	table+="<tr><td>&nbsp;</td></tr>"
	for(var i=0;i<selectedCumpolsrySubDetails.length;i++) {
		table+="<tr><td>"+selectedCumpolsrySubDetails[i]+"</td></tr>";
	}
	table+="<tr><td>&nbsp;</td></tr>"
	table+="<tr><td><b>Elective subjects</b></td></tr>"
	table+="<tr><td>&nbsp;</td></tr>"
	for(var i=0;i<selectedElectiveSubDetails.length;i++) {
		table+="<tr><td>"+(i+1) + ". " + selectedElectiveSubDetails[i]+"</td></tr>";
	}
	$("#subjectPopupTable").append(table);
}

function confirmSubject() {

	
	var studentIdForSave=studentId = '${sessionScope.studentId}';
	var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML+'&yearId='+globalyearId+'&standardId='+subjectStandardId+'&studentId='+studentIdForSave+'&subjectArr[]='+selectedSubArray;
	$.ajax({
        type: "POST",
        url : '/saveSubject',
        data:data,
        async : false,
        dataType: 'json',
        timeout: 600000,
        success : function(response) {
        	alert("Data saved successfully");
        	
        },
        error : function(xhr, status, error) {
        	alert("Error Saving data. please try again")
        }
    });  
	
	 $("#subjectPopup").modal('hide'); 
}

function closeSubjectPopup() {
	 $("#subjectPopup").modal('hide'); 
}
function openPage55(pageName, elmnt,className,linkId,i,tabId) {
	 var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName(className);
		
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	}
	  document.getElementById("Home55").style.display = "block";
	  
	  $("#sImg").empty();
	  $("#sSignImg").empty();
	  
	  var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML+'&regNo='+document.getElementById("RegNo").innerHTML+'&type='+1;
		$.ajax({
	        type: "GET",
	        url : '/getStudentImageSignature',
	        data:data,
	        async : false,
	        dataType: 'json',
	        timeout: 600000,
	        success : function(response) {
	      		var data = response.responseList;
				for(var i=0;i<data.length;i++){
					if(data[i].flag == 1){
						$("#emptyImg").css('display','none');
						$("#sImg").append('<img style="border: 0px solid red;width:20%;height:110px;" src="data:image/png;base64,'+data[i].letterHeadImg+'"  />');
					}
				}
	        },
	        error : function(xhr, status, error) {
	        }
	    });  
		
		var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML+'&regNo='+document.getElementById("RegNo").innerHTML+'&type='+2;
		$.ajax({
	        type: "GET",
	        url : '/getStudentImageSignature',
	        data:data,
	        async : false,
	        dataType: 'json',
	        timeout: 600000,
	        success : function(response) {
	      		var data = response.responseList;
				for(var i=0;i<data.length;i++){
					if(data[i].flag == 1){
						$("#sSignImg").append('<img style="border: 0px solid red;width:20%;height:65px;" src="data:image/png;base64,'+data[i].letterHeadImg+'"  />');
					}
				}
	        },
	        error : function(xhr, status, error) {
	        }
	    });  
	}

function openPage(pageName, elmnt,className,linkId,i,tabId) {

	var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName(className);
		
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	}

	  tablinks = document.getElementsByClassName(linkId);
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].style.backgroundColor = "";
	  }

	  document.getElementById(pageName).style.display = "block";

	  elmnt.style.backgroundColor = 'rgb(167, 176, 177)';
	  
	  
	  if(document.getElementById("attachmentStatus"+tabId).value=="1")
	  {
		  getAttachmentImagesForEdit();
	  }
	  //   Complete InComplete Progress Bar 
	  var noOfTab=document.getElementById("noOfTabHidden").value;
		  /* for(var k=1;k<=noOfTab;k++) {
		     var ids=[];
			 $('#legendContent'+k).find("select, textarea, input").each(function(){
				 ids.push($(this).attr('id'))
			 })
			 
			 var count=0;
			  for(var i=0;i<ids.length;i++) {
					  var aa=ids[i];
					// document.getElementsByName(aa)[0].setAttribute("id", aa);
					 // if(document.getElementById(aa).classList.contains("requiredField")) {
					//	  if(document.getElementById(aa).value=='' || document.getElementById(aa).value == 'default'){
					//			count++;
					 //	}  
			 		}    
			  }
			  if(count==0){
				document.getElementById("tab4"+k).style.backgroundColor = "#337e96";
			  }
	 	}  */
		  
		  
	 for(var k=1;k<=noOfTab;k++) { 
		  var ids=[];
			 $('#legendContent'+k).find("select, textarea, input").each(function(){
				 ids.push($(this).attr('name'))
			 })
			 var count=0;
			 var cusid_ele = document.getElementsByClassName('requiredField');
			 for (var i = 0; i < cusid_ele.length; ++i) {
				 var item = cusid_ele[i]; 
				  if(ids.includes(item.name)){
				    if(document.getElementsByName(item.name)[0].value=='' || document.getElementsByName(item.name)[0].value == 'default'){
				    	count++;
					 }
			 	}
			}
			 var dynamicChckBox_ele = document.getElementsByClassName('dynamicValueCheckbox');
			 for (var i = 0; i < dynamicChckBox_ele.length; ++i) {
				 var item = dynamicChckBox_ele[i]; 
				  if(ids.includes(item.name)){
				   	if($('input[name="'+item.name+'"]').is(':checked'))
				   	{
				   	}else
				   	{
				   		count++;
				   	}
			 	}
			}
		 if(count==0){
			document.getElementById("tab4"+k).style.backgroundColor = "#337e96";
		}
	} 
		  
		 var c=0;
		var ctn=0;
		if(isAttachmentFlag==1 && attachmentFlag==1){
			 $('#legendContent'+attachmentFlagtab).find('input[type=checkbox]').each(function(){
				 if(document.getElementById($(this).attr('id')).checked) {}
				 else{ 
					 	if(c!=0){
							 ctn=ctn+1;
					 	}
					}
				 c=c+1;
			 })
			 if(ctn==0){
				 document.getElementById("tab4"+attachmentFlagtab).style.backgroundColor = "#337e96";
			 }else{
				 document.getElementById("tab4"+attachmentFlagtab).style.backgroundColor = "#d1e0e4";
			 }
		} 
	//   Complete InComplete Progress Bar End
	// stud Img Signature 
		 if(studImgSignatureFlag ==2 ){
				document.getElementById("tab55").style.backgroundColor = "#337e96";
			}
	// stud Img Signature End 
	}
	
function getAttachmentImagesForEdit(){
 var data  = 'studentId='+encodeURIComponent(studentId);	
 $("#attachmentImages").empty();
 $('#loading').show();
 
	$.ajax({
        type: "GET",
        url:  "/getAttachmentsForEditWithImages",
        data:data,
        dataType: 'json',
        complete: function() {
        $('#loading').hide(); 
        },
    success : function(data1) {
        	var attachmentdata=data1;
        	var attachmentTable="";
        	attachmentTable = '<tr><td>';
				for(var i=0;i<data1.length;i++) {
					if(i!=0 && i%6==0) {
					
						attachmentTable+= '</td>';
						attachmentTable+='</tr>';
						attachmentTable+='<tr>';
						attachmentTable+='<td>';
					}
				
				attachmentTable+= '<img style="border: 0px solid red;width:80px;height:100px ; padding:5px" src="data:image/png;base64,'+data1[i].studentDocImageString+'"  />'; 
				$('#attachmentImages1').append('<div id="tatt1'+i+'",  style="background-color:#b9b3b3;   margin: 50px;   padding-bottom: 15px; page-break-before: always;" >');
				$('#tatt1'+i).append('<img style="border: 0px solid red;width:1000px;height:550px" src="data:image/png;base64,'+data1[i].studentDocImageString+'"  />'); 
				
				}
			attachmentTable+='</td>';
   			attachmentTable+='</tr>';
   		
   			$('#attachmentImages').append(attachmentTable);
   		},
        error : function(xhr, status, error) {
        }
   	}); 
}

var responseGlobal;
var attachmentFlag=0;
var attachmentFlagtab=0;
var isAttachmentFlag=0;

/* function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var fileName = input.files[0].name;
        extension = fileName.split('.').pop(); 
        alert(fileName);
        $('#fileName').val('studentPhoto'.extension);
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
} */





function getTabsAndLegendsSetting1(globalyearId,globalstandardId){
	document.getElementById("progressBarGo").style.display = "table-cell" ; 
/* 	 $('#progressBarGo').show(); */
	//document.getElementById("headerTable").style.minHeight = "10px";
	/* var year = document.getElementById('globalAcademicYear').value
	var standard = document.getElementById('Standard').value */;

	var year =globalyearId;
	var standard = globalstandardId;
	if(year=='' || standard=='') {
		return false;
		
	}
	document.getElementById("standardData").style.display='block';
	
	var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML;
	$(".studentContents").empty();
	 $.ajax({
            type: "GET",
            url:  "/getTabsAndLegendsSetting",
            data: data,
            dataType: 'json',
            cache: false,
            timeout: 600000,
            async:false,
            success : function(response) {
            	responseGlobal=response;
           		var tab=0;
           		var legend=0;
               	var tabsId="default";
               	var legendId="default";
               	var counter = 0;
                for(var i=0;i<response.length;i++) {
            		var table="";
                	if(tabsId!=response[i].tabsId) {
               			tab++;
                		legend++;
                   		table+= "<fieldset class='fieldset' style='text-align: center;'><legend class='legend'>"+response[i].legendName+"</legend>";
                   		table+= "<table id='legendContent"+legend+"' class='legendContent'><tr>";
                   		var style='';
                   		if(response[i].fieldName=='Attachment' && response[i].isRequired=='1') {
                   			attachmentFlag=1;
                   			attachmentFlagtab=tab;
                		}
                   		if(response[i].fieldName=='Attachment') {
                			style = "style='vertical-align:top !important'";
                			document.getElementById("attachmentStatus"+tab).value="1";
                			attachmentFlagtab=tab;
                			isAttachmentFlag=1;
                		}
                   		else if(response[i].fieldName=='Subjects') {
                			style = "style='vertical-align:top !important'";
                		}
                   		if(response[i].fieldTypeId=='4'){
                       		table+= "<td id='title' "+style+" ><span class='newStyle'>"+response[i].fieldName+"</span><span id='requiredSpan"+i+"' class='required'>*</span></td></tr>";

                   		}else{
                       		table+= "<td id='title' "+style+" ><span class='newStyle dynamicTitles"+response[i].isStaticOrDynamic+"Tab"+tab+"'>"+response[i].fieldName+"</span><span id='requiredSpan"+i+"' class='required'>*</span></td></tr>";

                   		}
                   		/* if(response[i].tabsName=='Basic Information' && counter==0){
                        	table+= createInputFields(response[i].fieldTypeId,response[i].fieldName,response[i].isRequired,response[i].isStaticOrDynamic,tab,response[i].validationFlag);
                        	table+="<td>";
                        	table+='<input type="file" onchange="readURL(this); " />';
                        	table+='<img id="blah" src="" alt="photo image" />';
                        	table+="</td>"
                        	counter = 2;
                   		}else{
                    	table+= createInputFields(response[i].fieldTypeId,response[i].fieldName,response[i].isRequired,response[i].isStaticOrDynamic,tab,response[i].validationFlag);
                   		} */
                   		
                    	table+= createInputFields(response[i].fieldTypeId,response[i].fieldName,response[i].isRequired,response[i].isStaticOrDynamic,tab,response[i].validationFlag);

                    	$("#tabContent"+tab).append(table);
                   		if(response[i].isRequired=='0') {
                   			document.getElementById("requiredSpan"+i).style.display="none";
                   		} 
                   		if(response[i].isStaticOrDynamic=='1' && (response[i].fieldTypeId=='2' || response[i].fieldTypeId=='4')) {
           					getCombosToAppendToDynamicFieldType(response[i].fieldName,response[i].fieldId,response[i].fieldTypeId,tab,response[i].validationFlag,response[i].isRequired);
                   		}
           				if(response[i].isStaticOrDynamic=='2' && (response[i].fieldTypeId=='2')) {
                   			getCombosToAppendToDynamic2FieldType(response[i].fieldName,response[i].fieldId,response[i].fieldTypeId,tab,response[i].validationFlag);
                   		}
                   		document.getElementById("yearId"+tab).value=document.getElementById("globalAcademicYear").value;
                   		var yearText = document.getElementById("globalAcademicYear");
                   		document.getElementById("yearText"+tab).value=yearText.options[yearText.selectedIndex].text;
                   		document.getElementById("standardId"+tab).value=standard;
                   		var standardText = document.getElementById("Standard");
                   		document.getElementById("standardText"+tab).value=standardText.options[standardText.selectedIndex].text;
                   		document.getElementById("schoolId"+tab).value=document.getElementById("schoolKey").innerHTML; 
                   		document.getElementById("mobileNo"+tab).value='${sessionScope.userName}';    
                   		document.getElementById("yearId"+55).value=document.getElementById("globalAcademicYear").value;
                   		var yearText = document.getElementById("globalAcademicYear");
                   		document.getElementById("yearText"+55).value=yearText.options[yearText.selectedIndex].text;
                   		document.getElementById("standardId"+55).value=standard;
                   		var standardText = document.getElementById("Standard");
                   		document.getElementById("standardText"+55).value=standardText.options[standardText.selectedIndex].text;
                   		document.getElementById("schoolId"+55).value=document.getElementById("schoolKey").innerHTML; 
                   		document.getElementById("mobileNo"+55).value='${sessionScope.userName}';     
                	}
               		else {
               			if(legendId!= response[i].legendId) {
               				legend++;
               				table+= "</table></fieldset>";
               				table+="<fieldset class='fieldset' style='text-align: center;'><legend class='legend'>"+response[i].legendName+"</legend>";
               				var style='';
               				if(response[i].fieldName=='Attachment' || response[i].fieldName=='Subjects') {
                    			style = "style='vertical-align:top !important'";
                    		}
               				if(response[i].fieldTypeId=='4'){
               					table+= "<table id='legendContent"+legend+"' class='legendContent'><tr><td  id='title' "+style+"><span class='newStyle'>"+response[i].fieldName+"</span><span id='requiredSpan"+i+"' class='required'>*</span></td></tr>";
               				}else{
               					table+= "<table id='legendContent"+legend+"' class='legendContent'><tr><td  id='title' "+style+"><span class=' newStyle dynamicTitles"+response[i].isStaticOrDynamic+"Tab"+tab+"'>"+response[i].fieldName+"</span><span id='requiredSpan"+i+"' class='required'>*</span></td></tr>";

               				}
               				table+= createInputFields(response[i].fieldTypeId,response[i].fieldName,response[i].isRequired,response[i].isStaticOrDynamic,tab,response[i].validationFlag);
               				$("#tabContent"+tab).append(table);
               				if(response[i].isRequired=='0') {
                       			document.getElementById("requiredSpan"+i).style.display="none";
                       		}
               				if(response[i].isStaticOrDynamic=='1' && (response[i].fieldTypeId=='2' || response[i].fieldTypeId=='4')) {
               					getCombosToAppendToDynamicFieldType(response[i].fieldName,response[i].fieldId,response[i].fieldTypeId,tab,response[i].isRequired);
                       		}
               				if(response[i].isStaticOrDynamic=='2' && (response[i].fieldTypeId=='2')) {
                       			getCombosToAppendToDynamic2FieldType(response[i].fieldName,response[i].fieldId,response[i].fieldTypeId,tab);
                       		}
               			}
               			else {
               				var style='';
                    		if(response[i].fieldName=='Attachment' || response[i].fieldName=='Subjects') {
                    			style = "style='vertical-align:top !important'";
                    		}
                    		if(response[i].fieldTypeId=='4'){
               			 		var tableContent= "<tr><td id='title' "+style+"><span class='newStyle'>"+response[i].fieldName+"</span><span id='requiredSpan"+i+"' class='required'>*</span></td></tr>";
                    		}else{
               			 		var tableContent= "<tr><td id='title' "+style+"><span class=' newStyle dynamicTitles"+response[i].isStaticOrDynamic+"Tab"+tab+"'>"+response[i].fieldName+"</span><span id='requiredSpan"+i+"' class='required'>*</span></td></tr>";

                    		}
               			 	tableContent+= createInputFields(response[i].fieldTypeId,response[i].fieldName,response[i].isRequired,response[i].isStaticOrDynamic,tab,response[i].validationFlag);
               			 	$("#legendContent"+legend).append(tableContent);
               				$("#tabContent"+tab).append(table);
               				if(response[i].isRequired=='0') {
                       			document.getElementById("requiredSpan"+i).style.display="none";
                       		}
               				if(response[i].isStaticOrDynamic=='1' && (response[i].fieldTypeId=='2' || response[i].fieldTypeId=='4')) {
               					getCombosToAppendToDynamicFieldType(response[i].fieldName,response[i].fieldId,response[i].fieldTypeId,tab,response[i].isRequired);
                       		}
               				if(response[i].isStaticOrDynamic=='2' && (response[i].fieldTypeId=='2')) {
                       			getCombosToAppendToDynamic2FieldType(response[i].fieldName,response[i].fieldId,response[i].fieldTypeId,tab);
                       		}
               			}
               		} 
                	tabsId = response[i].tabsId;
                	legendId = response[i].legendId; 
                	counter = 0;
            	 } 
            	 $('#tab41').click();
            },
            
           // 
           //
           // 

           error : function(xhr, status, error) {
            }
        });
	document.getElementById("progressBarGo").style.display='none';  
/* 	 $('#progressBarGo').hide();  */
		getCombos();
		getStudentDetailsForEdit();
}
 

function createInputFields(fieldTypeId,fieldName,isRequired,isStaticOrDynamic,tab,validationFlag) {
	fieldName = fieldName.replace(/ /g,'');  
	fieldName = fieldName.replace('.','');  
	fieldName = fieldName.replace(':','');  
	fieldName = fieldName.replace('/','');  
	fieldName = fieldName.replace('-','');
	fieldName = fieldName.replace('_','');
	fieldName = fieldName.replace('@','');
	fieldName = fieldName.replace('#','');
	fieldName = fieldName.replace('%','');
	fieldName = fieldName.replace('!','');
	fieldName = fieldName.replace('~','');
	fieldName = fieldName.replace('&','');
	fieldName = fieldName.replace('*','');
	//fieldName = fieldName.replace('`','');
	fieldName = fieldName.replace('?','');
	fieldName = fieldName.replace('$','');
	fieldName = fieldName.replace(',','');
	var tableContent="";
	var req="";
	var pattern="";
	var pattern1="";
	var editable='';
	
	if(isRequired=='1') {
		req="requiredField";
	}
	if(fieldName=='FormNo') {
		editable='readOnly'
	}
	pattern="text";
	if(validationFlag==2) {
		pattern1="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)";
	}
	else if(validationFlag==3) {
		pattern="number";
	}
	if(fieldTypeId==1) {
		tableContent+= "<td><input type='"+pattern+"' onkeypress='"+pattern1+"' value='' name='"+fieldName+"' class=' "+req+" inputText  dynamicValue"+isStaticOrDynamic+"Tab"+tab+"' "+req+" /></td></tr>";
		
	}
	else if(fieldTypeId==2) {
		tableContent+= "<td><select class=' "+req+" "+fieldName+" dynamicValue"+isStaticOrDynamic+"Tab"+tab+" inputSelect' name='"+fieldName+"' "+req+"><option value=''>Select</option></select></td></tr>";
	}
	else if(fieldTypeId==3) {
		if(isStaticOrDynamic==1) {
			tableContent+= "<td><input type='text' "+editable+" value='' name='"+fieldName+"' class=' "+req+" inputDate dynamicValue"+isStaticOrDynamic+"Tab"+tab+" ' "+req+" /></td></tr>";
		}else {
			tableContent+= "<td><select class=' "+req+" dd "+fieldName+" ' name='dd"+fieldName+"' "+req+"><option value=''>dd</option></select> ";
			tableContent+= "<select class=' "+req+" mm "+fieldName+" ' name='mm"+fieldName+"' "+req+"><option value=''>mm</option></select> ";
			tableContent+= "<select class=' "+req+" yy "+fieldName+" '  name='yy"+fieldName+"' "+req+"><option value=''>yyyy</option></select></td></tr>";
		}
	}
	else if(fieldTypeId==4) {
	
		tableContent+= "<td class='"+fieldName +" '></td></tr>";
	}
	else if(fieldTypeId==5) {
		if(isStaticOrDynamic==1) {
			tableContent+= "<td><textArea id='dynamicTextArea"+fieldName+"' class=' "+req+" textArea textArea"+fieldName+" dynamicValue"+isStaticOrDynamic+"Tab"+tab+" ' name='"+fieldName+"' "+req+"/></td></tr>";
		}
		else {
			tableContent+= "<td><textArea  class=' "+req+" textArea textArea"+fieldName+" dynamicValue"+isStaticOrDynamic+"Tab"+tab+" ' name='"+fieldName+"' "+req+"/></td></tr>";
		}
	} 
	return tableContent;
	
}

function getCombosToAppendToDynamic2FieldType(fieldName,fieldId,fieldTypeId,tab) {
	var schoolId  = document.getElementById("schoolKey").innerHTML;
	var data  = 'schoolId='+schoolId+'&fieldName='+encodeURIComponent(fieldName);
	 $.ajax({
         type: "GET",
         url:  "/getValueToPopulateForDynamic2Field",
         data: data,
         dataType: 'json',
         async:false,
         cache: false,
         timeout: 600000,
         async:false,
         success : function(response) {
        		
        		fieldName = fieldName.replace(/ /g,'');  
        		fieldName = fieldName.replace('.','');  
        		fieldName = fieldName.replace(':','');  
        		fieldName = fieldName.replace('/','');  
        		fieldName = fieldName.replace('-','');
        		fieldName = fieldName.replace('_','');
        		fieldName = fieldName.replace('@','');
        		fieldName = fieldName.replace('#','');
        		fieldName = fieldName.replace('%','');
        		fieldName = fieldName.replace('!','');
        		fieldName = fieldName.replace('~','');
        		fieldName = fieldName.replace('&','');
        		fieldName = fieldName.replace('*','');
        		//fieldName = fieldName.replace('`','');
        		fieldName = fieldName.replace('?','');
        		fieldName = fieldName.replace('$','');
        		fieldName = fieldName.replace(',','');
        	 var table="";
        	 if(fieldTypeId=='2') {
        		 $("."+fieldName).empty();
            	 var option = $('<option/>');
    			 option.attr('value','').text("Select");
    			 $("."+fieldName).append(option);
            
    			 var i=0;
    			 $(response).each(function() {
    				 i++;
    	        	var option = $('<option/>');
    				option.attr('value', this.dynamicFieldId).text(this.fieldValue);
    				$("."+fieldName).append(option);
            	 });
        	 }
        	 
         },
         error : function(xhr, status, error) {
         }
     });
}


function getCombosToAppendToDynamicFieldType(fieldName,fieldId,fieldTypeId,tab,isRequired) {
	$("."+fieldName).empty();
	
	fieldName = fieldName.replace(/ /g,'');  
	fieldName = fieldName.replace('.','');  
	fieldName = fieldName.replace(':','');  
	fieldName = fieldName.replace('/','');  
	fieldName = fieldName.replace('-','');
	fieldName = fieldName.replace('_','');
	fieldName = fieldName.replace('@','');
	fieldName = fieldName.replace('#','');
	fieldName = fieldName.replace('%','');
	fieldName = fieldName.replace('!','');
	fieldName = fieldName.replace('~','');
	fieldName = fieldName.replace('&','');
	fieldName = fieldName.replace('*','');
	//fieldName = fieldName.replace('`','');
	fieldName = fieldName.replace('?',''); 
	fieldName = fieldName.replace('$','');
	fieldName = fieldName.replace(',','');
	var schoolId  = document.getElementById("schoolKey").innerHTML;
	var data  = 'schoolId='+schoolId+'&fieldId='+encodeURIComponent(fieldId);
	 $.ajax({
         type: "GET",
         url:  "/getValueToPopulateForDynamicField",
         data: data,
         dataType: 'json',
         async:false,
         cache: false,
         timeout: 600000,
         async:false,
         success : function(response) {
        	 var table="";
        	 if(fieldTypeId=='2') {
        		 $("."+fieldName).empty();
            	 var option = $('<option/>');
    			 option.attr('value','').text("Select");
    			 $("."+fieldName).append(option);
            	 $(response).each(function() {
    	        	var option = $('<option/>');
    				option.attr('value', this.dynamicFieldId).text(this.fieldValue);
    				$("."+fieldName).append(option);
            	 });
        	 }
        	 if(fieldTypeId=='4') {
        		 dyncamicCheckBoxTitles.push(fieldName);
        		 $(response).each(function() {
        			 if(isRequired==1){
            			 table+="<label><input type='checkbox' id='dynamicCheckBox"+fieldName+this.dynamicFieldId+"' value='"+this.dynamicFieldId+"' name='"+fieldName+"' class='requiredField  dynamicValueCheckbox fieldName dynamicValueCheckbox1Tab"+tab+"' >"+this.fieldValue+"</label>";
        			 }else{
        			 	table+="<label><input type='checkbox' id='dynamicCheckBox"+fieldName+this.dynamicFieldId+"' value='"+this.dynamicFieldId+"' name='"+fieldName+"' class='fieldName  dynamicValueCheckbox dynamicValueCheckbox1Tab"+tab+"' >"+this.fieldValue+"</label>";
        			 }
        			});
        		 $("."+fieldName).append(table);
        	 }
         },
         error : function(xhr, status, error) {
         }
     });
}

var studentListGlobal;
var studImgSignatureFlag=0;

function getStudentDetailsForEdit() {
	studentId = '${sessionScope.studentId}';
	var data = 'studentId='+encodeURIComponent(studentId);
	 $.ajax({
         type: "GET",
         url:  "/getStudentDetailsForEdit",
         data:data,
         dataType: 'json',
         async:false,
         cache: false,
         timeout: 600000,
         async:false,
         success : function(response) {
        	 studentListGlobal=response;
        	 if(response.responseList.length>0){
        	 response = response.responseList[0];
        	 $('input[name="FormNumber"]').attr("value", response.formNumber);
        	 document.getElementById("FormNo").innerHTML=response.formNumber;
       	   	 $('select[name="Medium"] option[value="'+response.medium+'"]').attr("selected","selected");
       	  	 $('select[name="MissMr"] option[value="'+response.missMr+'"]').attr("selected","selected");
        	 $('input[name="FirstName"]').attr("value", response.firstName); 
        	 $('input[name="MiddleName"]').attr("value", response.middleName); 
        	 $('input[name="LastName"]').attr("value", response.lastName); 
        	 $('input[name="BirthPlace"]').attr("value", response.birthPlace); 
        	 $('select[name="Gender"] option[value="'+response.gender+'"]').attr("selected","selected");
        	 $('select[name="Caste"] option[value="'+response.caste+'"]').attr("selected","selected");
        	 $('select[name="Category"] option[value="'+response.category+'"]').attr("selected","selected");
        	 $('select[name="Religion"] option[value="'+response.religion+'"]').attr("selected","selected");
        	 $('input[name="Mothertongue"]').attr("value", response.mothertongue); 
        	 $('input[name="Nationality"]').attr("value", response.nationality); 
        	 $('input[name="FatherName"]').attr("value", response.fatherName); 
        	 $('input[name="MotherName"]').attr("value", response.motherName); 
        	 $('input[name="Education"]').attr("value", response.education); 
        	 $('input[name="Occupation"]').attr("value", response.occupation); 
        	 $('input[name="Income"]').attr("value", response.income); 
        	 $('input[name="Age"]').attr("value", response.age); 
        	 $('input[name="ParentEmailID"]').attr("value", response.parentEmailID); 
        	 $('.textAreaLocalAddress').html(response.localAddress);
        	 $('input[name="LocalDistrict"]').attr("value", response.localDistrict); 
        	 $('input[name="LocalPin Code"]').attr("value", response.localPinCode); 
        	 $('input[name="LocalState"]').attr("value", response.localState); 
        	 $('input[name="LocalCountry"]').attr("value", response.localCountry); 
         	 $('.textAreaPermanentAddress').html(response.permanentAddress);
        	 $('input[name="PermanentDistrict"]').attr("value", response	.permanentDistrict); 
        	 $('input[name="PermanentPinCode"]').attr("value", response.permanentPinCode); 
        	 $('input[name="PermanentState"]').attr("value", response.permanentState); 
        	 $('input[name="PermanentCountry"]').attr("value", response.permanentCountry); 
        	 $('input[name="ContactNumber1ForSMS"]').attr("value", response.contactNumber1ForSMS); 
        	 $('input[name="ContactNumber2"]').attr("value", response.contactNumber2); 
        	 $('input[name="EmailID"]').attr("value", response.emailID); 
        	 $('input[name="BankName"]').attr("value", response.bankName); 
        	 $('input[name="BranchName"]').attr("value", response.branchName); 
        	 $('input[name="AccountNo"]').attr("value", response.accountNo); 
        	 $('input[name="BankIFSCNo"]').attr("value", response.bankIFSCNo); 
        	 $('input[name="AadhaarCardNo"]').attr("value", response.aadhaarCardNo); 
         	 $('input[name="PreviousSchoolName"]').attr("value", response.previousSchoolName); 
        	 $('input[name="PreviousClass"]').attr("value", response.previousClass); 
        	 $('select[name="PreviousYear"] option[value="'+response.previousYear+'"]').attr("selected","selected");
        	 $('input[name="ReasonForLeavingPreviousSchool"]').attr("value", response.reasonForLeavingPreviousSchool); 
        	 document.getElementById("RegNo").innerHTML=response.registrationNumber;
        	 if(response.birthDate!='null' && response.birthDate!=null) {
        		 var date = [];
        		 date = response.birthDate.split("-");
        	   	 $('select[name="ddBirthDate"] option[value="'+date[0]+'"]').attr("selected","selected"); 
            	 $('select[name="mmBirthDate"] option[value="'+date[1]+'"]').attr("selected","selected"); 
            	 $('select[name="yyBirthDate"] option[value="'+date[2]+'"]').attr("selected","selected");  
        	 } 
        	}
        	 if(document.getElementById("FormNo").innerHTML=='' || document.getElementById("FormNo").innerHTML=='undefined' || document.getElementById("FormNo").innerHTML==null || document.getElementById("FormNo").innerHTML=='0') {
        		 getFormNo();
        	 } 
        	 var data  = 'studentId='+encodeURIComponent(studentId);	
        	$.ajax({
        	        type: "GET",
        	        url:  "/getAttachmentsForEdit",
        	        data:data,
        	        dataType: 'json',
        	 	    success : function(data1) {
        	 	    	
        	        	        for(var i=0;i<data1.length;i++) {
        						document.getElementById("attachmentCheckBox"+data1[i].attachmentId).checked = true;
        							$('#attachmentCheckBox'+data1[i].attachmentId).attr('checked', 'checked');
        						document.getElementById("fileName"+data1[i].attachmentId).value = data1[i].fileName;
        						//document.getElementById("attachmentName"+data1[i].attachmentId).value = data1[i].attachmentNameList[i];
        						document.getElementById("attachmentID"+data1[i].attachmentId).value = data1[i].attachmentId;
        						document.getElementById("studentFileAttachmentID"+data1[i].attachmentId).value = data1[i].studentAttachmentId;
        						document.getElementById("filePath"+data1[i].attachmentId).value = data1[i].filePath;
        					
        					}
              	   		},
        	        error : function(xhr, status, error) {
        	        }
        	   	}); 
	       	 
	       	 $.ajax({
	                type: "GET",
	                url:  "/getDynamicDetails2ForEdit",
	                data:data,
	                dataType: 'json',
	                async:false,
	                cache: false,
	                timeout: 600000,
	                async:false,
	                success : function(data2) {
	                	for(var i=0;i<data2.length;i++) {
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace(/ /g,'');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('.','');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace(':','');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('/','');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('-','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('_','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('@','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('#','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('%','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('!','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('~','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('^','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('&','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('*','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('`','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('?','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('$','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace(',','');
	  						$('input[name="'+data2[i].dynamicTitle+'"]').attr("value", data2[i].dynamicValue); 
	  		        	   	$('select[name="'+data2[i].dynamicTitle+'"] option[value="'+data2[i].dynamicValue+'"]').attr("selected","selected"); 
	  		        	//    $('select[name="'+data2[i].dynamicTitle+'"] option[value="'+data2[i].dynamicValue+'"]').attr("selected","selected"); 
	  					}
		       		},
			        error : function(xhr, status, error) {
			        }
	        });
	       	  $.ajax({
	                type: "GET",
	                url:  "/getDynamicDetails1ForEdit",
	                data:data,
	                dataType: 'json',
	                async:false,
	                cache: false,
	                timeout: 600000,
	                async:false,
	                success : function(data2) {
	  					for(var i=0;i<data2.length;i++) {
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace(/ /g,'');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('.','');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace(':','');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('/','');  
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('-','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('_','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('@','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('#','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('%','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('!','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('~','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('^','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('&','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('*','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('`','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('?','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace('$','');
	  						data2[i].dynamicTitle = data2[i].dynamicTitle.replace(',','');
	  		        	    if(document.getElementById("dynamicTextArea"+data2[i].dynamicTitle)){
	  		        	    	$('#dynamicTextArea'+data2[i].dynamicTitle).html(data2[i].dynamicValue);
	  		        	    }
	  						if(document.getElementById("dynamicCheckBox"+data2[i].dynamicTitle+data2[i].dynamicValue)){
	  							$("#dynamicCheckBox"+data2[i].dynamicTitle+data2[i].dynamicValue).attr("checked", "checked");
	       	 				}
	  						else {
	  							$('input[name="'+data2[i].dynamicTitle+'"]').attr("value", data2[i].dynamicValue);
	  						 	$('select[name="'+data2[i].dynamicTitle+'"] option[value="'+data2[i].dynamicValue+'"]').attr("selected","selected"); 
	  						 	//$('input:checkbox[name="'+data2[i].dynamicTitle+'"][value="' + v + '"]').prop('checked',true);
	  						}
	  					}
		       		},
			        error : function(xhr, status, error) {
			        }
	        	}); 
	       	var standard = document.getElementById("Standard").value;
	   	 $.ajax({
	            type: "GET",
	            url:  "/getStudentAssignedSubjectsForEdit",
	            data:data,
	            dataType: 'json',
	            async:false,
	            cache: false,
	            timeout: 600000,
	            async:false,
	            success : function(data) {
	   			for(var i=0;i<data.length;i++) {
	   				document.getElementById("selectedSubjectId"+data[i].subjectId).checked=true;
	   				$('#selectedSubjectId'+data[i].subjectId).attr('checked', 'checked');
	   		  	}
	       	 },
	   	     error : function(xhr, status, error) {
	   	     }
	    	 });
         },
       	
     });
	 
	 
//   Complete InComplete Progress Bar 
	
		// stud Img Signature
		var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML+'&regNo='+document.getElementById("RegNo").innerHTML+'&type='+1;
		$.ajax({
		    type: "GET",
		    url : '/getStudentImageSignature',
		    data:data,
		    async : false,
		    dataType: 'json',
		    timeout: 600000,
		    success : function(response) {
		  		var data = response.responseList;
				for(var i=0;i<data.length;i++){
					if(data[i].flag == 1){
						studImgSignatureFlag=1;
					}
				}
		    },
		    error : function(xhr, status, error) {
		    }
		});  
		
		var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML+'&regNo='+document.getElementById("RegNo").innerHTML+'&type='+2;
		$.ajax({
		    type: "GET",
		    url : '/getStudentImageSignature',
		    data:data,
		    async : false,
		    dataType: 'json',
		    timeout: 600000,
		    success : function(response) {
		  		var data = response.responseList;
				for(var i=0;i<data.length;i++){
					if(data[i].flag == 1){
						studImgSignatureFlag=2;
					}
				}
		    },
		    error : function(xhr, status, error) {
		    }
		});
		if(studImgSignatureFlag ==2 ){
			document.getElementById("tab55").style.backgroundColor = "#337e96";
		}
		// stud Img Signature End
	 var noOfTab=document.getElementById("noOfTabHidden").value;
	 /* for(var k=1;k<=noOfTab;k++) {
	     var ids=[];
		 $('#legendContent'+k).find("select, textarea, input").each(function(){
			 ids.push($(this).attr('name'))
		 })
		 var count=0;
		  for(var i=0;i<ids.length;i++) {
				 var aa=ids[i];
				 document.getElementsByName(aa)[0].setAttribute("id", aa);
				 if(document.getElementById(aa).classList.contains("requiredField")) {
						 if(document.getElementById(aa).value=='' || document.getElementById(aa).value == 'default'){
								count++;
					 	} 
		 		}  
		  }
		  if(count==0){
			document.getElementById("tab4"+k).style.backgroundColor = "#337e96";
		  }
 	}  */
 	
 	for(var k=1;k<=noOfTab;k++) { 
		  var ids=[];
			 $('#legendContent'+k).find("select, textarea, input").each(function(){
				 ids.push($(this).attr('name'))
			 })
			 var count=0;
			 var cusid_ele = document.getElementsByClassName('requiredField');
			 for (var i = 0; i < cusid_ele.length; ++i) {
				 var item = cusid_ele[i]; 
				  if(ids.includes(item.name)){
				    if(document.getElementsByName(item.name)[0].value=='' || document.getElementsByName(item.name)[0].value == 'default'){
				    	count++;
					 }
			 	}
			}
			 var dynamicChckBox_ele = document.getElementsByClassName('dynamicValueCheckbox');
			 for (var i = 0; i < dynamicChckBox_ele.length; ++i) {
				 var item = dynamicChckBox_ele[i]; 
				  if(ids.includes(item.name)){
				   	if($('input[name="'+item.name+'"]').is(':checked'))
				   	{
				   	}else
				   	{
				   		count++;
				   	}
			 	}
			}
		 if(count==0){
			document.getElementById("tab4"+k).style.backgroundColor = "#337e96";
		}
	} 
 	
	 var editAttachmentImg;
	 var data3  = 'studentId='+encodeURIComponent(studentId);
	 $.ajax({
	        type: "GET",
	        url:  "/getAttachmentsForEditWithImages",
	        data:data3,
	        async:false,
	        dataType: 'json',
	        complete: function() {
	        },
	    success : function(data1) {
	    	editAttachmentImg=data1.length;
	   		},
	        error : function(xhr, status, error) {
	        }
	   	}); 
	 
	 if(isAttachmentFlag==1 && attachmentFlag==1){
	 	if(editAttachmentImg==allAttachmentLength){
		 	document.getElementById("tab4"+attachmentFlagtab).style.backgroundColor = "#337e96";
		}else{
			document.getElementById("tab4"+attachmentFlagtab).style.backgroundColor = "#337e96";
		}
	}
	 
	// Attachment
		/* var checkedAttachmentCount=0;
		$("[name='attachmentArray']:checked").each(function () {
			checkedAttachmentCount++;
		});
		if(checkedAttachmentCount==allAttachmentLength){
			document.getElementById("tab4"+attachmentFlagtab).style.backgroundColor = "#337e96";
		}else{
			document.getElementById("tab4"+attachmentFlagtab).style.backgroundColor = "#337e96";
		} */
		 var c=0;
		var ctn=0;
		
		// Attachment End 
//   Complete InComplete Progress Bar  
	 
	// $('input[name="FormNumber"]').attr("value", response.responseData); 
			var tab=0;
			var legend=0;
			var tabsId="default";
			var legendId="default";
		    for(var i=0;i<responseGlobal.length;i++) {
				var table="";
		    	if(tabsId!=responseGlobal[i].tabsId) {
		   			tab++;
		        	 if(studentListGlobal.responseList.length>0){
		        		 //document.getElementById("FormNo").innerHTML=response2.FormNumber;
			        	 var response2 = studentListGlobal.responseList[0];
			        	 document.getElementById("studentIdGlobal"+tab).value=response2.studentId;
			        	 document.getElementById("studentRegNoGlobal"+tab).value=response2.registrationNumber;
			        	 document.getElementById("renewIdGlobal"+tab).value=response2.studentRenewId; 
			        	 document.getElementById("appUserRoleIdGlobal"+tab).value=response2.appUserRoleId; 
		        	 }
		    	}
		    } 
}
/* function getFormNo(){
	var data  = 'schoolId='+document.getElementById("schoolKey").innerHTML;
	 $.ajax({
         type: "GET",
         url : '/getFormNumber',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
        	     formNoValue=response.responseData;
       		 	 $('input[name="FormNumber"]').attr("value", response.responseData); 
           	  	 document.getElementById("FormNo").innerHTML=response.responseData;
         },
         error : function(xhr, status, error) {
         }
     }); 
} */

function checkData(tab) {
	//$("input[name='"+dyncamicCheckBoxTitles[i]+"']").val('');
	document.getElementById("progressBar").style.display="table-cell";
 	var dynamicArrTitle=[];
	var dynamicValueArr=[];
	var dynamicArrTitleClass= document.getElementsByClassName("dynamicTitles1Tab"+tab); 
	for(var i=0;i<dynamicArrTitleClass.length;i++) {
		dynamicArrTitle.push(dynamicArrTitleClass[i].innerHTML);
	}
	var finalDynamicTitles=[];
	for(var i=0;i<dynamicArrTitle.length;i++) {
		var presentFlag=false;
		for(var j=0;j<dyncamicCheckBoxTitles.length;j++) {
			if(dyncamicCheckBoxTitles[j]==dynamicArrTitle[i]) {
				presentFlag=true;
				break;
			}
		}
		if(!presentFlag) {
			finalDynamicTitles.push(dynamicArrTitle[i]);
		}
	}
	var dynamicArrValueClass= document.getElementsByClassName("dynamicValue1Tab"+tab); 
	for(var i=0;i<dynamicArrValueClass.length;i++) {
		dynamicValueArr.push(dynamicArrValueClass[i].value);
	}
	//alert("1st   "+tab+"    "+finalDynamicTitles+"     "+dynamicValueArr);
	document.getElementById("dynamicTitleArr"+tab).value=finalDynamicTitles;
	document.getElementById("dynamicValueArr"+tab).value=dynamicValueArr;
	var dynamicArrTitle=[];
	var dynamicValueArr=[];
	var dynamicArrTitleClass= document.getElementsByClassName("dynamicTitles2Tab"+tab); 
	//var dynamicArrTitleClass= document.getElementsByClassName("dynamicValueCheckbox1Tab"+tab); 
	for(var i=0;i<dynamicArrTitleClass.length;i++) {
		//dynamicArrTitle.push(dynamicArrTitleClass[i].innerHTML);
		dynamicArrTitle.push(dynamicArrTitleClass[i].name);
	}
	var dynamicArrValueClass= document.getElementsByClassName("dynamicValue2Tab"+tab); 
	//var dynamicArrValueClass= document.getElementsByClassName("dynamicValueCheckbox1Tab"+tab); 
	for(var i=0;i<dynamicArrValueClass.length;i++) {
		//alert(dynamicArrValueClass[i].value);
		dynamicValueArr.push(dynamicArrValueClass[i].value);
	}
	//alert("2nd   "+tab+"    "+dynamicArrTitle+"     "+dynamicValueArr);
	document.getElementById("dynamicTitleArr2"+tab).value=dynamicArrTitle;
	document.getElementById("dynamicValueArr2"+tab).value=dynamicValueArr;
	if(document.getElementById("isAttachment"+tab).value==1) {
		var table="";
		table+='<input type="file" name="file" id="defaultFile" style="display: none;">';
		table+='<input type="text" name="fileName" id="defaultName" value="NoFile" style="display: none;">';
		$("#tabContent"+tab).append(table);
	}
	
	getDyanamicCheckBoxValues(tab);
	
}


function getDyanamicCheckBoxValues(tab) {
	 var dyncamicCheckBoxTitleArray =[];
	 var dyncamicCheckBoxValueArray=[];
	var dynamicArrValueClass=[];
	dynamicArrValueClass= document.getElementsByClassName("dynamicValueCheckbox1Tab"+tab); 
	//for(var i=0;i<dyncamicCheckBoxTitles.length;i++) {
	for(var i=0;i<dynamicArrValueClass.length;i++) {
		 var temp='';
		 var temp2='';
		// $.each($("input[name='"+dyncamicCheckBoxTitles[i]+"']:checked"), function(){  
		 $.each($("input[name='"+dynamicArrValueClass[i].name+"']:checked"), function(){     	
				 temp+=$(this).val();
				 temp+='-';
				 //dyncamicCheckBoxTitleArray.push(dynamicArrValueClass[i].name);
				 //dyncamicCheckBoxValueArray.push(temp);
				 temp2+=dynamicArrValueClass[i].name;
				 temp2+='-';
         });
		 if(temp!='') {
			 dyncamicCheckBoxTitleArray.push(temp2);
			 dyncamicCheckBoxValueArray.push(temp);			 
		 }
	}
	//alert("3nd   "+tab+"    "+dyncamicCheckBoxTitleArray+"     "+dyncamicCheckBoxValueArray);
	document.getElementById("dynamicCheckTitleArr"+tab).value=dyncamicCheckBoxTitleArray;
	document.getElementById("dynamicCheckValueArr"+tab).value=dyncamicCheckBoxValueArray;
	//alert(document.getElementById("dynamicCheckTitleArr"+tab).value);
	//alert(document.getElementById("dynamicCheckValueArr"+tab).value);
}

function saveFormData(contentId) {
 	//document.getElementById("DefaultFormNo").value=formNoValue; 
 	document.getElementById("DefaultFormNo").value=document.getElementById("FormNo").innerHTML;
	/*  var requiredFields = document.getElementsByClassName("requiredField");
	 for(var i=0;i<requiredFields.length;i++) {
		   if(requiredFields[i].value=='') {
			alert("Please Fill Required Fields");
			requiredFields[i].focus();
			document.getElementById("progressBar").style.display="none";
			return false;
		}   
	}    */
	
	
	
	if(isAttachmentFlag==1 && attachmentFlag==1){
		 if(attachmentFlagtab==contentId){
			var checkedAttachmentCount=0;
			$("[name='attachmentArray']:checked").each(function () {
				checkedAttachmentCount++;
			});
			if(checkedAttachmentCount==allAttachmentLength){
			}else{
				 alert("Please Fill Required Fields");
				 document.getElementById("progressBar").style.display="none";
				 return false;
			}
		 }
	}
	
	 var ids=[];
	 $('#legendContent'+contentId).find("select, textarea, input").each(function(){
		 ids.push($(this).attr('name'))
	 }) 
	 
	  /* for(var i=0;i<ids.length;i++) {
		var aa=ids[i];
		 document.getElementsByName(aa)[0].setAttribute("id", aa);
		 if(document.getElementById(aa).classList.contains("requiredField")) {
			 if(document.getElementById(aa).value==''){
				 alert("Please Fill Required Fields");
				 document.getElementById("progressBar").style.display="none";
				 return false;
			 }
		 } 
	 }  */
	 
	  var cusid_ele = document.getElementsByClassName('requiredField');
	 for (var i = 0; i < cusid_ele.length; ++i) {
		 var item = cusid_ele[i]; 
		  if(ids.includes(item.name)){
		    if(document.getElementsByName(item.name)[0].value==''){
				 alert("Please Fill Required Fields");
				 document.getElementById("progressBar").style.display="none";
				 return false;
			 }
	 	}
	}
	 var requiredCheckBoxIds=[];
	 for (var i = 0; i < cusid_ele.length; ++i) {
		 var item = cusid_ele[i]; 
		 requiredCheckBoxIds.push(item.name);
	 }
	 var dynamicChckBox_ele = document.getElementsByClassName('dynamicValueCheckbox');
	 for (var i = 0; i < dynamicChckBox_ele.length; ++i) {
		 var item = dynamicChckBox_ele[i]; 
		  if(ids.includes(item.name)){
		   	if($('input[name="'+item.name+'"]').is(':checked'))
		   	{
		   	}else
		   	{
				 if(requiredCheckBoxIds.includes(item.name)){
				 	 alert("Please Fill Required Fields");
					 document.getElementById("progressBar").style.display="none";
					 return false; 
				 }
		   		 
		   	}
	 	}
	} 
	 
	var myform = document.getElementById("StudentDetailsForm"+contentId);
    var formData = new FormData(myform);
    
    console.log(formData);
	  $.ajax({
         type: "POST",
         url:  "/saveStudentDetails",
         enctype: "multipart/form-data",
         data:formData,
         processData: false,  
         contentType: false,
         dataType: 'json',
         async:false,
         cache: false,
         timeout: 600000,
         async:false,
         success : function(data) {
			if(data.statusCode=='200') {
				document.getElementById("progressBar").style.display="none";
				 //alert("Data Saved Succesfully With Reg No: "+ data.responseData);
				 alert("Data Saved Succesfully");
				 document.getElementById("RegNo").innerHTML=data.responseData;
				 //getStudentDetailsForEdit();
				 
				 	$.ajax({
						url : '/getNewStudentFlag',
						async:false,
						type : 'GET',
						success : function(data) {	
						var newstudent = data;
						if(newstudent==1){
							//$("#mymodelid1").trigger('click')
		                 }
						}
				});
				 	// tab change
				 	if((contentId+1) <= document.getElementById("noOfTabHidden").value){
				 		var c=contentId+1;
				 		var pageName="Home"+c;
				 		var elmnt=document.getElementById("tab4"+c);
				 		openPage(pageName, elmnt,'tabcontent','tablink',0,c) ;
				 	}else if(document.getElementById("lastTabHiddenField").value == contentId){
				 		var c=55;
				 		var pageName="Home"+c;
				 		var elmnt=document.getElementById("tab4"+c);
				 		openPage(pageName, elmnt,'tabcontent','tablink',0,c) ;
				 	}
			}
    	 },
	     error : function(xhr, status, error) {
	     }
 	 });  
}

function saveFormDataImg(contentId) {
	document.getElementById("studRegNo").value=document.getElementById("RegNo").innerHTML;
	
    var myform = document.getElementById("StudentDetailsForm55");
    var formData = new FormData(myform);
    $('#imgSpinner').css('display','block');
    $.ajax({
         type: "POST",
         url:  "/saveStudentDetailsImg",
         enctype: "multipart/form-data",
         data:formData,
         processData: false,  
         contentType: false,
         dataType: 'json',
         async:false,
         cache: false,
         timeout: 600000,
         async:false,
         success : function(data) {
			if(data.statusCode=='200') {
				$('#emptyImg').css('display','none');
				$('#imgSpinner').css('display','none');
				//$(".close").trigger("click");
				alert("your form has been submited.");
				openPage55('Home55', null, 'tabcontent','tablink',0,55);
			}
    	 },
	     error : function(xhr, status, error) {
	    	 $('#imgSpinner').css('display','none'); 
	     }
 	 });   
}

function printReport() {
			
	  var studRegNo=document.getElementById("RegNo").innerHTML;
	  var schoolId  = document.getElementById("schoolKey").innerHTML;
	  var t = document.getElementById("globalAcademicYear");
	  var yearComboVal = document.getElementById("academicYear").innerHTML;
	  var data  = 'studRegNo='+studRegNo+'&schoolId='+schoolId+'&yearComboVal='+yearComboVal;
	  var response1 ;
	  $.ajax({
	         type: "GET",
	         url : 'getStudentInformation',
	         data:data,
	         async : false,
	         dataType: 'json',
	         timeout: 600000,
	         success : function(response) {
	        	 response1=response;
	        	 console.log(response);
	        	 getreport(response)
	         },
	         error : function(xhr, status, error) {
	         }
	     });
	  
}

function downloadReport() {

		var studRegNo=document.getElementById("RegNo").innerHTML;
		 var schoolId  = document.getElementById("schoolKey").innerHTML;
		 var t = document.getElementById("globalAcademicYear");
		 var yearComboVal = document.getElementById("academicYear").innerHTML;
		 var std = document.getElementById("Standard");
		 var standardval = std.options[std.selectedIndex].text;
		 var standardText =document.getElementById("standardId").innerHTML;
		 var RegNo = document.getElementById("RegNo").innerHTML;
		 var FormNo = document.getElementById("FormNo").innerHTML;
		 var AdmissionDateSpan = document.getElementById("AdmissionDateSpan").innerHTML;
	  	
		 var data  = 'studRegNo='+studRegNo+'&schoolId='+schoolId+'&yearComboVal='+yearComboVal+'&std='+standardval+'&standardText='+standardText+'&AdmissionDateSpan='+AdmissionDateSpan;	
		 window.open("getPrint?studRegNo="+studRegNo+"&schoolId="+schoolId+"&yearComboVal="+yearComboVal+"&std="+standardval+"&standardText="+standardText+"&AdmissionDateSpan="+AdmissionDateSpan,"_blank");
}

function getreport(response){
	
	var attachmentdata ;

	 var data  = 'studentId='+response[0].studentId;  
	  $.ajax({
	         type: "GET",
	         url : '/getStudentAttachmentsforPrint',
	         data:data,
	         async : false,
	         dataType: 'json',
	         timeout: 600000,
	         success : function(response) {
	        	 attachmentdata = response.responseList;
	       	
        },
        error : function(xhr, status, error) {
        }
    });

	var prtwin = window.open('','PrintGridViewData','left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
	prtwin.document.write("<html><head><style type='text/css' media='print'> @page { size: portrait A4; margin:2mm 10mm 2mm 10mm} </style></head>");
	prtwin.document.write(document.getElementById("letterHead").outerHTML);
	prtwin.document.write( "<br/>");
	prtwin.document.write( "<br/>");
	prtwin.document.write(document.getElementById("sImg").outerHTML);
	prtwin.document.write( "<br/>");
	prtwin.document.write( "<br/>");
	prtwin.document.write( "<table style='width:50%;'>");
	prtwin.document.write( "<tr><td><b><font size=4px>Registration No. :</font></b></td><td><font size=4px>"+response[0].registartionNo+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Full Name :</font></b></td><td><font size=4px>"+response[0].initialName+" "+response[0].firstName+" "+response[0].middleName+" "+response[0].lastName+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Standard Name :</font></b></td><td><font size=4px>"+response[0].standardName+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Division Name :</font></b></td><td><font size=4px>"+response[0].divisionName+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Student Id :</font></b></td><td><font size=4px>"+response[0].studentId+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Admission Date :</font></b></td><td><font size=4px>"+response[0].admissionDate+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Academic Year :</font></b></td><td><font size=4px>"+2020-2021+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Form No :</font></b></td><td><font size=4px>"+response[0].studentFormNo+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Birth Date :</font></b></td><td><font size=4px>"+response[0].birthDate+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Birth Place :</font></b></td><td><font size=4px>"+response[0].birthPlace+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Gender : </font></b></td><td><font size=4px>"+response[0].gender+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Previous School Name :</font></b></td><td><font size=4px>"+response[0].registartionNo+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Father Name :</font></b></td><td><font size=4px>"+response[0].fatherName+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Mother Name :</font></b></td><td><font size=4px>"+response[0].motherName+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Contact No. :</font></b></td><td><font size=4px>"+response[0].mobileNumber+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Caste :</font></b></td><td><font size=4px>"+response[0].caste+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Category :</font></b></td><td><font size=4px>"+response[0].category+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Religion :</font></b></td><td><font size=4px>"+response[0].religion+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Mothertongue : :</font></b></td><td><font size=4px>"+response[0].mothertonge+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Nationality: :</font></b></td><td><font size=4px>"+response[0].nationality+"</font></td></tr>");
	prtwin.document.write( "<tr><td><b><font size=4px>Local Address:  :</font></b></td><td><font size=4px>"+response[0].localAddress+"</font></td></tr>");
	prtwin.document.write( "</table>");
	prtwin.document.write( "<br/>");
	prtwin.document.write("<b><font size=3px>Attachment Images</font></b>");
	prtwin.document.write( "<br/>");
	prtwin.document.write( "<br/>");
	for(var i= 0; i<attachmentdata.length ;i++){
		var j =i+1
		prtwin.document.write( "<br/>");
		prtwin.document.write( j+ ")"+"<a href="  +attachmentdata[i].attachmentFilePath+">"+attachmentdata[i].attachmentFilePath+"</a>");
	}
/* 	prtwin.document.write( "<br/>");
	prtwin.document.write(document.getElementById("attachmentImages").outerHTML); */
	prtwin.document.write( "<br/>");
	prtwin.document.write( "<br/>");
	prtwin.document.write(document.getElementById("instructions").outerHTML); 
	prtwin.document.write( "<br/>");
	prtwin.document.write( "<br/>");
	prtwin.document.write(document.getElementById("declaration").outerHTML);
	prtwin.document.write( "<br/>");
	prtwin.document.write( "</html>");
	prtwin.document.close();
	prtwin.focus();
	prtwin.print();
	prtwin.close();

	/*  var std = document.getElementById("Standard");
	 var standardval = std.options[std.selectedIndex].text;
	 var standardText = $('#Standard :selected').val();
	 var RegNo = document.getElementById("RegNo").innerHTML;
	 var FormNo = document.getElementById("FormNo").innerHTML;
	 var AdmissionDateSpan = document.getElementById("AdmissionDateSpan").innerHTML;
	 var data  = 'studRegNo='+studRegNo+'&schoolId='+schoolId+'&yearComboVal='+yearComboVal+'&std='+standardval+'&AdmissionDateSpan='+AdmissionDateSpan;	
	 /* window.open("getPrint?studRegNo="+studRegNo+"&schoolId="+schoolId+"&yearComboVal="+yearComboVal+"&std="+standardval+"&standardText="+standardText+"&AdmissionDateSpan="+AdmissionDateSpan,"_blank");
	   $.ajax({
       type: "POST",
       url : '/getPrint',
       data:data,
       async : false,
      	dataType: 'application/pdf',
       timeout: 600000,
       success : function(response) {
       },
       error : function(xhr, status, error) {
       }
   });  
	
	
	 $.ajax({
		  dataType: 'native',
		  url: "/file.pdf",
		  xhrFields: {
		    responseType: 'blob'
		  },
		  success: function(blob){
			  alert(blob);
		    console.log(blob.size);
		      var link=document.createElement('a');
		      link.href=window.URL.createObjectURL(blob);
		      link.download="AdmissionExcel-1010001382-P10094-2019-2020.pdf";
		      link.click();
		  }
		}); 
	
	 $.ajax({
       type: "POST",
       url : '/getDownloadPrint',
       data:data,
       async : false,
       dataType: 'json',
       timeout: 600000,
       success : function(response) {
       },
       error : function(xhr, status, error) {
       }
   });   
	 $('#loading').css('display','none');
	var tabListSize = document.getElementById("tabListSize").innerHTML;
	var prtwin = window.open('','PrintGridViewData','left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
	prtwin.document.write("<html><head><style type='text/css' media='print'> @page { size: portrait A4; margin:2mm 10mm 2mm 10mm} </style></head>");
	prtwin.document.write(document.getElementById("letterHead").outerHTML);
	prtwin.document.write( "</br>");
	prtwin.document.write( "</br>");
	var t = document.getElementById("globalAcademicYear");
	var yearComboVal = t.options[t.selectedIndex].text;
	var std = document.getElementById("standardId");
	//var standardval = std.options[std.selectedIndex].text;
	var standardval = $('.stdText').text();
	var RegNo = document.getElementById("RegNo").innerHTML;
	var FormNo = document.getElementById("FormNo").innerHTML;
	var AdmissionDateSpan = document.getElementById("AdmissionDateSpan").innerHTML;
	prtwin.document.write( "<table style='width:100%;'>");
	prtwin.document.write( "<tr>");
	prtwin.document.write( "<td><b><font size=3px>Academic Year : "+ yearComboVal+ "&nbsp;&nbsp;&nbsp;&nbsp; Standard : "+standardval+"</font></b></td><td style='text-align: right;'><b><font size=3px><span style='color:red;'>Form No. : </span>"+FormNo+"&nbsp;&nbsp;&nbsp;&nbsp;<span style='color:red;'>	 Reg. No. : </span>"+RegNo+" </font></b></td>");
	prtwin.document.write( "</tr>");
	prtwin.document.write( "</table>");
	prtwin.document.write( "<br/>");
	document.getElementById('instructions').style.display = "block";
	//prtwin.document.write(document.getElementById("instructions").outerHTML);
	prtwin.document.write( "<br/>");
	for (var i=1;i<=tabListSize;i++){
		prtwin.document.write( "</br>");
		prtwin.document.write( "<table style='background-color: #f3f3f3;width:100%;'>");
		prtwin.document.write( "<tr>");
		prtwin.document.write( "<td style='color: #880010;font-size: 24px;'> ");
		var tabName = document.getElementById("tabLabel"+i).innerHTML;
		prtwin.document.write(tabName);
		prtwin.document.write( "<br/>");
		prtwin.document.write( "</td>");
		prtwin.document.write( "</tr>");
		prtwin.document.write( "<tr>");
		prtwin.document.write( "<td>");
		document.getElementById('submit'+i).style.display = "none";
		document.getElementById('Home'+i).style.display = "block";
		prtwin.document.write(document.getElementById("tabContent"+i).outerHTML);
		prtwin.document.write( "</td>");
		prtwin.document.write( "</tr>");
		prtwin.document.write( "</table>");
		prtwin.document.write( "<br/>");		
	} 
	//prtwin.document.write( "<br/>");

	//prtwin.document.write(document.getElementById("attachmentImages").outerHTML); 
	
	document.getElementById('declaration').style.display = "block";
	//prtwin.document.write(document.getElementById("declaration").outerHTML);
	prtwin.document.write( "<br/>");
	prtwin.document.write( "</html>");
	prtwin.document.close();
	prtwin.focus();
	prtwin.print();
	prtwin.close();  
	var tabListSize = document.getElementById("tabListSize").innerHTML;
	for (var i=1;i<=tabListSize;i++){
		document.getElementById('submit'+i).style.display = "table-cell";
		document.getElementById('Home'+i).style.display = "none";
	}
	document.getElementById('declaration').style.display = "none";
	document.getElementById('instructions').style.display = "none";
	document.getElementById('Home1').style.display = "none";
	 $('#tab41').click();  
	 
	 $('#loading').css('display','block');  */
}



function goPaymentPage(){
	
	var studentID = '${sessionScope.studentId}';
	var userName='${sessionScope.userName}'; 
	var schoolId = document.getElementById("schoolKey").innerHTML;	

	 var requestdata="schoolId="+schoolId+"&renewId="+renewId+"&newYearId="+newYearId ;
	
	
	
 
 $.ajax({
     type: "GET",
     url : '/paymentPage',
     data:requestdata,
     async : false,
     dataType: 'json',
     timeout: 600000,
     success : function(response) {   	 
    	 window.location.href = "payment.jsp?schoolId="+schoolId+"&renewId="+renewId+"&newYearId="+newYearId;    	
     },
     error : function(xhr, status, error) {
    	 alert(error)    
    	 }
     
 });
 
 }

</script>

</head>


<body onload="getStandardAndYearCombo();">
	<%
		String schoolKey = (String) session.getAttribute("schoolId");
	    String newStudentvalue = (String)session.getAttribute("newStudent"); 
	%>
	
<div class="container">
		<input type="hidden" id="studentIdValue" value="${sessionScope.studentId}" /> 
		
		
		
		<span id="schoolKey" style="display: none;"><%=schoolKey%></span>
		<table style="width: 100%; border: solid 0px; padding-left: 10%; background: #337e96;">
			<tr>
				<td>
					<div style="width: 1000px;">
						<div id="header" style="width: 100%; background-color: #678889;">
							<table style="width: 100%; background: #fff; color: #337e96;">
								<tr>
									<td colspan="3"><span id='userMobile' style="font-weight: bold; margin-left: 30px;">Welcome ${sessionScope.welcomeName}</span>&nbsp;
									</td>
									<td style="float: right;">
										<form action="/logout" method="post">
											<input class="logout" type="submit" style="color: #337e96; margin-right: 30px;"
												value="Logout" />
										</form>
									</td>
								</tr>
								<tr>
									<td colspan="4" id="letterHead" ></td>
								</tr>
								<tr>
									<td>
										<td style="border: 1px solid; width: 170px; cursor: pointer;"><a onclick="downloadReport();">Admission Form Print</a></td>
									</td>
								</tr>
								<!-- <tr style="line-height: 30px; text-align: center;">
									<td style="width: 170px; cursor: pointer;">Online Admission Form</td>
									<td style="border: 1px solid; width: 170px; cursor: pointer;"><a onclick="printReport();">Admission Form Print</a></td>
									<td style="width: 170px; cursor: pointer;"><a data-toggle="modal" data-target="#myModal">Change Password</a></td>
								<td></td>
								</tr> -->
							</table>
						</div>
						
						<table style="line-height: 30px; width: 100%; background-color: #D24040;">
							<tr>
								<td  class="hide" style="width: 0%; vertical-align: top;"><b>Academic Year :</b> &nbsp;
									<select id="globalAcademicYear" class="AcademicYear" style="margin-right: 15px;">
									</select>
								</td>
								<td  class="hide" style="width: 0%; vertical-align: top;"><b>Standard:</b> &nbsp;
									<select id="Standard" class="Standard" style="margin-right: 15px;">
										<option value="">Select Standard</option>
									</select>
								</td>
								<td  class="hide" style="width: 0%; vertical-align: top;"><button
										class="submit" style="width: 55px; height:31px; padding: 1px;"
										onclick="getTabsAndLegendsSetting()">GO</button></td> 
										
		<td style="vertical-align: top;"><b style="color: white; margin-left:40px ">Academic year :&nbsp;</b><span id="academicYear" style="color: white;"></span></td>												
			<td style="vertical-align: top;"><b style="color: white;  ">Standard :&nbsp;</b><span id="standardId" class="stdText" style="color: white;"></span></td>											
							   	<td style="vertical-align: top;"><b style="color: white;  ">Form No :&nbsp;</b><span id="FormNo" style="color: white;"></span></td>			
								 <td style="vertical-align: top;"><b style="color: white; ">Reg No :&nbsp;</b><span id="RegNo" style="color: white;"></span></td>
							<td>
			                	 <button style="cursor: pointer;" class="submit"  id="PaymentButton" onclick="goPaymentPage()">Pay Fees</button>
									 
									 </td>
							<!-- 	 <td style="float: right; vertical-align: top;"><b> Date
										:&nbsp;</b><span id="AdmissionDateSpan"></span></td>  -->
							</tr>
							
						</table>
						
						<table id="headerTable;"
							style="line-height: 30px; width: 100%; min-height: 550px; background-color: #337e96;">
							<!-- <tr>
								<td  class="hide" style="width: 0%; vertical-align: top;"><b>Academic Year :</b> &nbsp;
									<select id="globalAcademicYear" class="AcademicYear" style="margin-right: 15px;">
									</select>
								</td>
								<td  class="hide" style="width: 0%; vertical-align: top;"><b>Standard:</b> &nbsp;
									<select id="Standard" class="Standard" style="margin-right: 15px;">
										<option value="">Select Standard</option>
									</select>
								</td>
								<td  class="hide" style="width: 0%; vertical-align: top;"><button
										class="submit" style="width: 55px; height:31px; padding: 1px;"
										onclick="getTabsAndLegendsSetting()">GO</button></td> 
										
		<td style="vertical-align: top;"><b style="color: white; margin-left:40px ">Academic year :&nbsp;</b><span id="academicYear" style="color: white;"></span></td>												
			<td style="vertical-align: top;"><b style="color: white;  ">Standard :&nbsp;</b><span id="standardId" style="color: white;"></span></td>											
							   	<td style="vertical-align: top;"><b style="color: white;  ">Form No :&nbsp;</b><span id="FormNo" style="color: white;"></span></td>			
								 <td style="vertical-align: top;"><b style="color: white; ">Reg No :&nbsp;</b><span id="RegNo" style="color: white;"></span></td>
								 <td style="float: right; vertical-align: top;"><b> Date
										:&nbsp;</b><span id="AdmissionDateSpan"></span></td> 
							</tr> -->
							<tr>
							
							<td id="progressBarGo" colspan="100%" style="text-align: center; display: none;">
								<img  style="margin: auto;" alt="Saving...." src="<c:url value="/resources/img/upload_bar.gif"/>" /> 
					    <!--    <div id="progressBarGo" class="hide"><p><img src="resources/img/upload_bar.gif" /> Please Wait...</p></div> -->
							</td>	
								
								
							</tr>
							<tr>
								<td colspan="100%;">
									<div id="standardTabs"></div>
									<div id="standardData" class="standardData" style="background: #fff;display: none;">
										<!-- style="display: none;"> -->
										<table id="" style="width: auto;margin: 0px auto !important;">
											<tr>
												<td><c:set var="tabId" value="0" />  <c:set var="lastTabId" value="0" scope='page' /> 
													<c:if test="${!empty  sessionScope.tabListSize}">
														<c:forEach items="${sessionScope.tabListSize}" var="tabs">
															<c:set var="tabId" value="${tabId+1}" />
															<c:set var="lastTabId" value="${lastTabId+1}" />
															<div style="width: 134px;float:left;text-align: center;">
																<table>
																	<tr>
																		<td style="padding-left: 20px;">
																			<u><a onclick="openPage('Home${tabId}', this, 'tabcontent','tablink',0,${tabId})" ><button class="tablink" id="tab4${tabId}" style="border-radius: 50%;width: 28px;height: 28px;color: blue;font-size: 14px;"></button></a></u>
																			<%-- <span id="tabLabel${tabId}">${tabs.tabsName}</span> --%>
																		</td>
																		<td colspan="2">
																			<span id="tabId${tabId}" style="display: none;">${tabs.tabsId}</span>
																			<hr style="width: 100px;margin-top: 10px;">
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2"><u><a onclick="openPage('Home${tabId}', this, 'tabcontent','tablink',0,${tabId})" ><span style="color: blue;font-size: 14px;" id="tabLabel${tabId}">${tabs.tabsName}</span></a></u></td><td></td>
																	</tr>
																</table>
															</div>
														</c:forEach>
													</c:if>
													<div style="width: 50px;float:left;margin-right: 50px;">
														<table>
															<tr>
																<td style="padding-left: 10px;">
																	<u><a onclick="openPage55('Home55', this, 'tabcontent','tablink',0,55)"><button class="tablink" id="tab55" style="border-radius: 63%;width: 30px;height: 30px;color: blue;font-size: 14px;"></button></a></u>
																</td>
															</tr>
															<tr>
																<td><u><a onclick="openPage55('Home55', this, 'tabcontent','tablink',0,55)" ><span style="color: blue;font-size: 14px;">Student  Photograph</span></a></u></td>
															</tr>
														</table>
													</div>
													<div style="width: 50px;float:left;margin-right: 50px;">
														<table>
															<tr>
																<td style="padding-left: 10px;">
																	<u><a onclick="openSubject('HomeSubject', this, 'tabcontent','tablink',0,55)"><button class="tablink" id="tab55" style="border-radius: 63%;width: 30px;height: 30px;color: blue;font-size: 14px;"></button></a></u>
																</td>
																<td colspan="2">
																	<span id="tabId${tabId}" style="display: none;">${tabs.tabsId}</span>
																	<hr style="width: 100px;margin-top: 10px;">
																</td>
															</tr>
															<tr>
																<td><u><a onclick="openSubject('HomeSubject', this, 'tabcontent','tablink',0,66)" ><span style="color: blue;font-size: 14px;">Subjects</span></a></u></td>
															</tr>
														</table>
													</div>
												</td>
												
										
											</tr>
											</table>
											<input type="hidden" id="lastTabHiddenField" value="${lastTabId}"/>
											<input type="hidden" id="AdmissionDateSpan" value=""/>
											
										
										<table  id="tableid" style="width: 100%;">
											<%-- <tr>
												<td><c:set var="tabId" value="0" /> 
													<c:if test="${!empty  sessionScope.tabListSize}">
														<c:forEach items="${sessionScope.tabListSize}" var="tabs">
															<c:set var="tabId" value="${tabId+1}" />
															<div style="width: 134px;float:left;text-align: center;">
																<table>
																	<tr>
																		<td style="padding-left: 10px;"><button class="tablink" id="tab4${tabId}" onclick="openPage('Home${tabId}', this, 'tabcontent','tablink',0,${tabId})" style="border-radius: 50%;width: 28px;height: 28px;"></button>
																			<span id="tabLabel${tabId}">${tabs.tabsName}</span>
																		</td>
																		<td colspan="2">
																			<span id="tabId${tabId}" style="display: none;">${tabs.tabsId}</span>
																			<hr style="width: 100px;margin-top: 10px;">
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2"><span id="tabLabel${tabId}">${tabs.tabsName}</span></td><td></td>
																	</tr>
																</table>
															</div>
														</c:forEach>
													</c:if>
													<div style="width: 50px;float:left;margin-right: 50px;">
														<table>
															<tr>
																<td>
																	<button class="tablink" id="tab410" style="border-radius: 63%;width: 30px;height: 30px;"></button>
																</td>
															</tr>
															<tr>
																<td><span>Approve</span></td>
															</tr>
														</table>
													</div>
													</td>
											</tr> --%>
											<tr>
												<td><span id="tabListSize" style="display: none;">${tabId}</span>
													<c:forEach var="contentId" begin="1" end="${tabId}">
												
														<div id="Home${contentId}" style="background: #fff;display: block;" class="tabcontent">
															<form id="StudentDetailsForm${contentId}">
																<input type="hidden" class="AcademicYearValue" name="AcademicYear" id="yearId${contentId}" /> 
																<input type="hidden" class="AcademicYearText" name="AcademicYearText" id="yearText${contentId}" /> 
																<input type="hidden" name="standard" id="standardId${contentId}" /> 
																<input type="hidden" class="standardText" name="standardText" id="standardText${contentId}" />
																<input type="hidden" class="schoolId" name="schoolId" id="schoolId${contentId}" />
																<input type="hidden" name="dynamicTitleArr" id="dynamicTitleArr${contentId}" />
																<input type="hidden" name="dynamicValueArr" id="dynamicValueArr${contentId}" /> 
																<input type="hidden" name="dynamicTitleArr2" id="dynamicTitleArr2${contentId}" /> 
																<input type="hidden" name="dynamicValueArr2" id="dynamicValueArr2${contentId}" />
																<input type="hidden" name="dynamicCheckTitleArr" id="dynamicCheckTitleArr${contentId}" />
																<input type="hidden" name="dynamicCheckValueArr" id="dynamicCheckValueArr${contentId}" />
																<input type="hidden" class="mobileNo" name="userName" id="mobileNo${contentId}" /> 
																<input type="hidden" class="isAttachment" name="isAttachment" id="isAttachment${contentId}" value="1" /> 
																<input type="hidden" class="attachmentStatus" name="attachmentStatus" id="attachmentStatus${contentId}" value="0" /> 
																<input type="hidden" class="admissionDate" name="AdmissionDate" id="AdmissionDate" />
																<input type="hidden" class="DefaultFormNo" name="DefaultFormNo" id="DefaultFormNo" />
																<input type="hidden" class="studentIdGlobal" name="studentId" id="studentIdGlobal${contentId}" />
																<input type="hidden" class="studentRegNoGlobal" name="RegistrationNumber" id="studentRegNoGlobal${contentId}" />
																<input type="hidden" class="renewIdGlobal" name="studentRenewId" id="renewIdGlobal${contentId}" />
																<input type="hidden" class="appUserRoleIdGlobal" name="appUserRoleId" id="appUserRoleIdGlobal${contentId}" />
																<%-- <input type="hidden" class="formNumberGlobal" name="FormNumber" id="formNumberGlobal${contentId}" /> --%>
																<div id="tabContent${contentId}" class="studentContents" style="text-align: left;">
																</div>
																<input type="hidden" class="noOfTabHidden" name="noOfTabHidden" id="noOfTabHidden" value="${tabId}"/> 
																<br /> 
																	<img id="progressBar"
																	style="display: none; margin: auto;" alt="Saving...."
																	src="<c:url value="/resources/img/upload_bar.gif"/>" />
																	<br /> 
																	<%-- <c:if test="${contentId==tabId}">
																	<input type="button" style="cursor: pointer;"
																	class="submit" id="submit${contentId}"
																	onclick="checkData(${contentId});saveFormData(${contentId});"
																	value="Submit" />
																	<!-- <a onclick="printReport();">Admission Form Print</a> -->
																	<input type="button" style="cursor: pointer;width:149px;" class="submit" onclick="printReport();" value="Admission Form Print" />
																	</c:if>
																	<c:if test="${contentId != tabId}"> --%>
																	<input type="button" style="cursor: pointer;"
																	class="submit" id="submit${contentId}"
																	onclick="checkData(${contentId});saveFormData(${contentId});"
																	value="Save & Next" />
																	<%-- </c:if> --%>
															</form>
														</div>
													</c:forEach>
												
	 												<div id="Home55" class="tabcontent" style="display: none;">
														<form id="StudentDetailsForm55">
															<div id="tabContent55" style="text-align: left;"> 
																<fieldset class='fieldset' style="text-align: center;"><legend class='legend'>Student Image/Signature</legend>
																
																<input type="file" name="file" id="file" style="display: none;">
																<input type="text" name="fileName" id="fileNameD" style="display: none;">
																
																<!-- <input type="checkbox" id="attachmentCheckBox50" style="display: none;" onFocus="checkCkeckBox(this,50,'Image');"/> -->
																<input type="text" onClick="openFile(50);" style="width:300px;" id="fileName50" name="studImgFilePath" class="fileName"/> 
																<input type="hidden" id="attachmentName50" name="attachmentIdNameArr" value="default"/> 
																<input type="hidden" id="attachmentID50" name="stdentAttachment" value="default"/>
																<input type="hidden" id="filePath50" name="filePath"/>
																<input type="hidden" id="stdentFile50" name="stdentFile"/>
																<input type="hidden" id="studentFileAttachmentID50" name="studentFileAttachmentIDArray" value="default"/> 
																
																<table id='legendContent55' class='legendContent' style="width:600px;margin: 0px auto;">
																	<tr style="text-align: center;">
																		<td>
																			<img id="emptyImg" src="/resources/img/placehold.png"/>
																			<div id="sImg" style=""></div>
																			<img id="imgSpinner" style=" margin-left: 278px; background-color: transparent; width:50px;height:50px; margin-top:10px; display: none;" src="resources/img/img.gif"/>
																		</td>
																	</tr>
																	<tr style="text-align: center;">
																		<td>
																			<label style="margin-left: 199px;">
														   						<input type="file" id="file55" name="file" style="display: none;" onchange="checkAttachment(55);checkfileAttachment(this,55,'Image'); "> 
														   						<a style="cursor:pointer;" id="salink" class="attachmentLabels" title="Image"><u>Upload Student Photograph&nbsp;&nbsp;</u></a>
														   						<input type="text" onClick="openFile(55);" style="width:300px;" id="fileName55" name="fileName" class="fileName"/> 
														   					</label> <br>
														   					<!-- <div id="sImg" style=""></div> -->
														   					
																			<!-- <input type="checkbox" id="attachmentCheckBox55" disabled="disabled" onFocus="checkCkeckBox(this,55,'Image');"/> -->
																			<input type="hidden" id="attachmentName55" name="attachmentIdNameArr" value="default"/> 
																			<input type="hidden" id="attachmentID55" name="stdentAttachment" value="default"/>
																			<input type="hidden" id="filePath55" name="filePath"/>
																			<input type="hidden" id="stdentFile55" name="stdentFile"/>
																			<input type="hidden" id="studentFileAttachmentID55" name="studentFileAttachmentIDArray" value="default"/> 
																	<!-- </tr>
																	<tr style="text-align: center;"> -->
																		</td>
																	</tr>	
																	<!-- <tr style="text-align: center;">
																		<td>
																		    <label style="">
														   						<input type="file" id="file56" name="file" style="display: none;" onchange="checkAttachment(56);checkfileAttachment(this,56,'Image'); "> 
														   						<a style="cursor:pointer;" class="attachmentLabels" title="Image"><u>Student Signature&nbsp;&nbsp;</u></a>
														   						<input type="text" onClick="openFile(56);" style="width:300px;" id="fileName56" name="fileName" class="fileName"/> 
														   					</label><br>
														   					<div id="sSignImg" style=""></div> 
																			<input type="checkbox" id="attachmentCheckBox56" disabled="disabled" onFocus="checkCkeckBox(this,56,'Image');"/>
																			<input type="hidden" id="attachmentName56" name="attachmentIdNameArr" value="default"/> 
																			<input type="hidden" id="attachmentID56" name="stdentAttachment" value="default"/>
																			<input type="hidden" id="filePath56" name="filePath"/>
																			<input type="hidden" id="stdentFile56" name="stdentFile"/>
																			<input type="hidden" id="studentFileAttachmentID56" name="studentFileAttachmentIDArray" value="default"/> 
																		</td>
																	</tr>	 -->
																</table>
																<input type="hidden" class="AcademicYearValue" name="AcademicYear" id="yearId55" /> 
																<input type="hidden" class="AcademicYearText" name="AcademicYearText" id="yearText55" /> 
																<input type="hidden" name="standard" id="standardId55" /> 
																<input type="hidden" class="standardText" name="standardText" id="standardText55" />
																<input type="hidden" class="schoolId" name="schoolId" id="schoolId55" />
																<input type="hidden" name="dynamicTitleArr" id="dynamicTitleArr55" />
																<input type="hidden" name="dynamicValueArr" id="dynamicValueArr55" /> 
																<!-- <input type="hidden" name="dynamicTitleArr2" id="dynamicTitleArr255" /> 
																<input type="hidden" name="dynamicValueArr2" id="dynamicValueArr255" /> -->
																<input type="hidden" name="dynamicCheckTitleArr" id="dynamicCheckTitleArr55" />
																<input type="hidden" name="dynamicCheckValueArr" id="dynamicCheckValueArr55" />
																<input type="hidden" class="mobileNo" name="userName" id="mobileNo55" /> 
																<input type="hidden" class="isAttachment" name="isAttachment" id="isAttachment55" value="1" /> 
																<input type="hidden" class="attachmentStatus" name="attachmentStatus" id="attachmentStatus55" value="0" /> 
																<input type="hidden" class="admissionDate" name="AdmissionDate" id="AdmissionDate" />
																<input type="hidden" class="DefaultFormNo" name="DefaultFormNo" id="DefaultFormNo" />
																<input type="hidden" class="studentIdGlobal" name="studentId" id="studentIdGlobal55" />
																<input type="hidden" class="studentRegNoGlobal" name="RegistrationNumber" id="studentRegNoGlobal55" />
																<input type="hidden" class="renewIdGlobal" name="studentRenewId" id="renewIdGlobal55" />
																<input type="hidden" class="appUserRoleIdGlobal" name="appUserRoleId" id="appUserRoleIdGlobal55" />
																<input type="hidden" name="studRegNo" id="studRegNo" />
																	
																<table style="margin: 0px auto;">
																	<tr>
																		<td>
																			<input type="button" style="cursor: pointer;" class="submit" id="submit55" onclick="saveFormDataImg(55);" value="Submit" />
																			<input type="button" style="cursor: pointer;width:149px;" class="submit" onclick="printReport();" value="Admission Form Print" /> 
																		</td>
																	</tr>
																</table>
																</fieldset>
															</div>
														</form> 
													</div>
													<div id="subjectContent" class="tabcontent" style="display: none;">
														<div id="tabContent66" style="text-align: left;"> 
															<fieldset class='fieldset' style="text-align: center;"><legend class='legend'>Subjects</legend>
														        <table id="cumpolsrySubjects" style="width:100%;"></table>
																<table id="electiveSubjects" style="margin-top:20px;width:100%;"></table>
																<input type="button" value="Submit" style="cursor: pointer;" class="submit" onclick="saveSubjectsData()"></input>
															</fieldset>
														</div>
													</div>
													</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
		             <!--    <div id="loading" class="hide"><p><img src="resources/img/imageloader.gif" /> Loading Images...</p></div> -->
		     
		     
		     
						<div id ="attachmentImages1"   class="hide"></div>
						
						<div id="footer" style="width: 100%;">
							<table
								style="width: 100%; background: #337e96; color: white; height: 50px; padding-bottom: 10px">
								<tr>
									<td style="text-align: center; color: white;">POWERED BY �
										INGENIO TECHNOLOGIES PVT. LTD. (V. 1.17) Call Us: 7276004262</td>
								</tr>
							</table>
						</div>
						
			
						<div id="instructions" class="hide">
							<b>INSTRUCTIONS: </b>
							<ul>
								<li>Application form should be procured from the college
									office.</li>
								<li>Application form is to be filled and signed by the
									candidate in his/her own handwriting.</li>
								<li>Duly filled application form should be submitted to
									office of the college with prescribed application form fee in
									cash.</li>
								<li>Application without required certificates shall be
									rejected.</li>
								<li>Tick {&radic;} on whatever is applicable.</li>
							</ul>
						</div>

						<div id="declaration" class="hide" style="page-break-before: always; margin-top :50px ;">

							<b>DECLARATION BY THE APPLICANT</b>
							<ul
								style="list-style-type: none; line-height: 25px;">
								<li>I,
									_______________________________________________________________
									hereby declare that, I have understood all the Rules of
									Admission for the current year and on understanding these
									rules, I have filled in this form of application for the
									current year.</li>
								<li>The information given by me in my application is true
									to the best of my knowledge and belief.</li>
								<li>I have not been debarred from appearing at any
									examination held by any Government constituted or statutory
									examination authority in India.</li>
								<li>I fully understand the offer of ADMISSION will be made
									to be depending on my merit inter-se and availability of a seat
									at the time of my application, when I will report to the
									admission authority according to the schedule of admission.</li>
								<li>I hereby undertake that if admitted, I will abide by
									the provisions of Maharashtra prohibition of ragging act 1999
									for the action against ragging within or outside of any
									educational institute.</li>
								<li>I understand that no other document, other than
									attached to the application form will be entertained for the
									purpose of claims/connections etc. in connection with my
									admission.</li>
								<li>I hereby agree to confirm to any rules, acts and laws
									enforced by Government and I hereby undertake that so long as I
									am a student of the college, I will do nothing either inside or
									outside the college which may result in disciplinary action
									against me under act and laws.</li>
								<li>I am fully aware of the rules & regulation of admission
									laid down by DTE.</li>
								<li>I will not claim for refund of fees if admission will
									be cancelled after last date of cancellation of admission.<br />
								</li>
								<li>Date :</li>
								<li>Place :</li>

								<li style="float: right">Signature of the Applicant</li>
							</ul>
						</div>

					</div>
				</td>
			</tr>
		</table>
		
		
	<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="color: white; background-color:#678889">
          <button type="button" class="close" data-dismiss="modal"  style ="float: right;">&times;</button>
          <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body">
         <table>
         
						 <tr>
							<td style="padding: 12px;">Old Password</td>
							<td style="padding: 12px;"><input type="password" name="oldpassword"  id="oldpassword" placeholder="Enter New Password "
								></td>
						</tr> 
						<tr>
							<td style="padding: 12px;">New Password</td>
							<td style="padding: 12px;"><input type="password" name="changepassword" id="changepassword" placeholder=" Enter New Password "
								></td>
						</tr>
						<tr>
							<td style="padding: 12px;">Confirm Password</td>
							<td style="padding: 12px;"><input type="password" name="confirmpassword" id="confirmpassword" placeholder=" Confirm New Password "
								></td>
						<tr>	
							<td style="padding: 12px;text-align: center;" colspan="100%"><input type="button"  style="cursor: pointer;"  class="submit"  value="Submit" onclick="changepassword(2);"  id="changepass" /></td>
						</tr>
					</table>
				 </div>
      
      </div>
    </div>
    </div>
    
    
    <a href="#" class="hide" id="mymodelid1" class="myButton" data-toggle="modal" data-target="#myModal1">Click here</a> 
 	<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="color: white; background-color:#678889">
          <button type="button" class="close" data-dismiss="modal"  style ="float: right;">&times;</button>
          <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body">
         <table>
         				<tr>
							<td style="padding: 12px;">New Password</td>
							<td style="padding: 12px;"><input type="password" name="changepassword" id="changepassword1" placeholder="Enter New Password"
								></td>
						</tr>
						<tr>
							<td style="padding: 12px;">Confirm Password</td>
							<td style="padding: 12px;"><input type="password" name="confirmpassword" id="confirmpassword1" placeholder=" Confirm New Password "
								></td>
						<tr>	
							<td style="padding: 12px;text-align: center;" colspan="100%"><input type="button"  style="cursor: pointer;"  class="submit"  value="Submit" onclick="changepassword(1);"  id="changepass" /></td>
						</tr>
					</table>
				 </div>
      
      </div>
    </div>
    </div>
      
  
     <a href="#" class="hide" id="subjectConfirmationPopup" class="myButton" data-toggle="modal" data-target="#subjectPopup">Click here</a> 
 	<div class="modal fade" id="subjectPopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="color: white; background-color:#678889">
          <button type="button" class="close" data-dismiss="modal"  style ="float: right;">&times;</button>
          <h4 class="modal-title">You have selected following Subjects</h4>
        </div>
        <div class="modal-body">
         <table id="subjectPopupTable"></table>
         <input type="button" value="Confirm" class="Submit" onClick="confirmSubject()" style="margin-top:10px;"></input>
         <input type="button" value="Edit" class="Submit" onclick="closeSubjectPopup()"></input>
  	 	</div>
      </div>
    </div>
    </div>
      
  

</div>

</body>
</html>