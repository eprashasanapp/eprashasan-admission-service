<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4">

<head>
<meta charset="utf-8">
<title>Admission Form Setting</title>
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<style>
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<style type="text/css">
body, html {
	height: 100%;
	margin: 0;
	font-family: Arial;
}
/* Style tab links */
.tablink {
	background-color: #678889;
	color: white;
	float: left;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 16px;
	font-size: 17px;
	/* width: 25%; */
}
.tablink:hover {
	background-color: rgb(167, 176, 177);
}
/* Style the tab content (and add height:100% for full page content) */
.tabcontent {
	color: white;
	background-color:#f3f3f3;
	display: none;
	padding: 20px 20px;
	min-height: 550px;
	height: 100%;
	width: 100%;
}
#Home {
	background-color:  #f3f3f3;
}
#News {
	background-color:  #f3f3f3;
}
#Contact {
	background-color:  #f3f3f3;
}
#About {
	background-color:  #f3f3f3;
}
#Details {
	background-color:  #f3f3f3;
}
.hide {
	display: none;
}
.show {
	display: block;
}
</style>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modalPopup {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 51px; /* Location of the box */
  left: 0;
  top: 0;
  width: 30%; /* Full width */
  height: 100%; /* Full height */
  /* overflow: auto; */ /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modalPopup-content {
  background-color: #fefefe;
  margin: auto;
  padding: 10px;
  border: 1px solid #888;
  width: 50%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.close1 {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close1:hover,
.close1:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.myButton{
border:1px solid #25729a; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 7px;margin:5px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);/* font-weight:bold; */ color: #FFFFFF;
 background-color: #3093c7; background-image: -webkit-gradient(linear, left top, left bottom, from(#3093c7), to(#1c5a85));
 background-image: -webkit-linear-gradient(top, #3093c7, #1c5a85);
 background-image: -moz-linear-gradient(top, #3093c7, #1c5a85);
 background-image: -ms-linear-gradient(top, #3093c7, #1c5a85);
 background-image: -o-linear-gradient(top, #3093c7, #1c5a85);
 background-image: linear-gradient(to bottom, #3093c7, #1c5a85);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#3093c7, endColorstr=#1c5a85);
}

.logout {
    color: #c10d0d;
    background-color: transparent;
    border: 0px;
    font-size: 18px;
    font-weight: bold;
    cursor: pointer;
}
</style>

<script type="text/javascript">
	function openPage(pageName, elmnt, color) {
		// Hide all elements with class="tabcontent" by default */
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		
		// Remove the background color of all tablinks/buttons
		tablinks = document.getElementsByClassName("tablink");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].style.backgroundColor = "";
		}
		// Show the specific tab content
		document.getElementById(pageName).style.display = "block";
		// Add the specific color to the button used to open the tab content
		elmnt.style.backgroundColor = color;
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
<script type="text/javascript">

var dataResponse;

	function tabsName() {
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var data = 'schoolId=' + schoolId;
		
		//$("#letterHead").append('<img style="background-image:url(resources/img/bonafiedImgs.png);background-repeat:no-repeat;background-size:1000px 200px;   width: 1000px; height: 200px;"   />');
	//	$("#letterHead").empty();
	 	$.ajax({
        type: "GET",
        url : '/getLetterHead',
        data:data,
        async : false,
        dataType: 'json',
        timeout: 600000,
        success : function(response) {
      		var data = response.responseList;
			for(var i=0;i<data.length;i++){
				if(data[i].flag == 1){
					$("#letterHead").append('<img style="border: 0px solid red;width:100%;height:220px;" src="data:image/png;base64,'+data[i].letterHeadImg+'"  />');
				}
			}
        },
        error : function(xhr, status, error) {
       	
        }
    }); 
		$.ajax({
			type : "GET",
			url : "/getTabsName",
			data : data,
			dataType : 'json',
			cache : false,
			async:false,
			timeout : 600000,
			success : function(response) {
				$('#tableTabsDiv').empty();
				$('#tableid').empty();
				response=response.responseList;
				var table = "";
				table += '<tr>';
				table += '<td>';
				for (i = 0; i < response.length; i++) {
					table += '<button id="tab'+response[i].tabsId+'" class="tablink" onclick="openPage(\''+response[i].tabsName+'\', this,\'rgb(167, 176, 177)\');getOldDynamicFields('+response[i].tabsId+');getlegendName('+response[i].tabsId+');">';
					var istabdisplay=response[i].isTabDisplayOrNot;
					if(istabdisplay==1){
						table += '<input name="" type="checkbox" id="tabCheckBox'+response[i].tabsId+'" onchange="checkisTabDisplayOrNot('+response[i].tabsId+');" checked />';
						}else{
							table += '<input name="" type="checkbox" id="tabCheckBox'+response[i].tabsId+'" onchange="checkisTabDisplayOrNot('+response[i].tabsId+');"  />';
						}
					table += '<input id="tabName'+response[i].tabsId+'" type="text" name="username" value="'+response[i].tabsName+'" onkeypress="return updatetabs(event,'+response[i].tabsId+');" style="width:100px;"/>';
					table+='<input type="hidden" id="isTabDisplayOrNot'+response[i].tabsId+'" name="isTabDisplayOrNot" value="0" />';
					table += '</button>';
				}
				table += '</td>';
				table += '</tr>';
				$('#tableid').append(table);
				var table2 = "";
				for (i = 0; i < response.length; i++) {
					table2 += '<tr>';
					table2 += '<td>';
					table2 += '<div id="'+response[i].tabsName+'" class="tabcontent">';							
					table2 += '<input type="button" onclick="addInput('+response[i].tabsId+')" value="Add Legend" class="myButton"/><br/>';
					table2 += '<table style="width: 37%;" id="legendName'+response[i].tabsId+'"></table>	';	
					table2 += '<span id="responce'+response[i].tabsId+'" class="responseLink"></span><br/>';
					table2 += '</div>';
					table2 += '</td>';
					table2 += '</tr>';
				}
				$('#tableTabsDiv').append(table2); 
				document.getElementById("currentTabName").value=response[0].tabsId;	
				 $(function() {
					$('#tab'+response[0].tabsId).click();
				}); 
			},
			error : function(xhr, status, error) {
				alert("Invalid url please try again");
			}
		});
	}
	
	function checkisTabDisplayOrNot(tabId){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		if (document.getElementById("tabCheckBox"+tabId).checked) {
			document.getElementById("isTabDisplayOrNot"+tabId).value="1";
			var isTabDisplayOrNot=document.getElementById("isTabDisplayOrNot"+tabId).value;
			var data = 'tabId=' + tabId +'&schoolId=' + schoolId+'&isTabDisplayOrNot='+isTabDisplayOrNot;
			$.ajax({
				type : "POST",
				url : "/updateTabsDisplayOrNotFlag",
				data : data,
				async:false,
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(response) {
				},
				error : function(xhr, status, error) {
					return false;
				}
			});
		}else{
			document.getElementById("isTabDisplayOrNot"+tabId).value="0";
			var isTabDisplayOrNot=document.getElementById("isTabDisplayOrNot"+tabId).value;
			var data = 'tabId=' + tabId +'&schoolId=' + schoolId+'&isTabDisplayOrNot='+isTabDisplayOrNot;
			$.ajax({
				type : "POST",
				url : "/updateTabsDisplayOrNotFlag",
				data : data,
				async:false,
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(response) {
				},
				error : function(xhr, status, error) {
					return false;
				}
			});
		}
	}
	
	function updatetabs(e,tabId) {
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var tabName = document.getElementById('tabName' + tabId).value;
		var data = 'tabId=' + tabId + '&tabName=' + tabName + '&schoolId=' + schoolId;
		if (e.keyCode == 13) {
			$.ajax({
				type : "POST",
				url : "/updateTabsName",
				data : data,
				async:false,
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(response) {
					alert("Tabs Name successfully Changed !");
				},
				error : function(xhr, status, error) {
					return false;
				}
			});
		}
		return true;
	}
	function getAllFeilds(tabId) {
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var legendId=document.getElementById("currentLegendId").value;
		document.getElementById("currentLegendIdFinal").value=legendId;
		var data = 'tabId=' + tabId + '&schoolId=' + schoolId +'&legendId='+legendId;
		$.ajax({
			type : "GET",
			url : "/getAllFeilds",
			data : data,
			async:false,
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(response) {
				response=response.responseList;	
				dataResponse=response;	
				$('#staffAttendenceReport').empty();
				var table = "";				
				table += '<tr style="width:100%;background-color:#678889;;height:50px;">';
				table += '<th>Is Display</th>';
				table += '<th>Field Label Name</th>';
				table += '<th>Is Required</th>';
				table += '<th>Is Yearly</th>';
				table += '<th>Field Validation</th>';
				table += '<th>Field Type</th>';
				table += '<th>Field Priority</th>';
				table+='<input type="hidden" id="currentLegendId" name="currentLegendId" value=""/>';
				table += '<th>Add Field value</th>';
				table += '</tr>';				
				for (i = 0; i < response.length; i++) {
					if(response[i].isStaticOrDynamic==1 || response[i].isStaticOrDynamic==2){
						updateDynamicFieldVal.push(response[i].fieldId+"@"+response[i].isyearly);
						document.getElementById("updateDynamicFieldVal").value=updateDynamicFieldVal.join(", ");
						isYearlyDynamicFieldVal.push(response[i].fieldId+"@"+response[i].isyearly);
						
						updateIsRequiredDynamicFieldVal.push(response[i].fieldId+"@"+response[i].isyearly);
						document.getElementById("updateIsRequiredDynamicFieldVal").value=updateIsRequiredDynamicFieldVal.join(", ");
						isRequiredDynamicFieldVal.push(response[i].fieldId+"@"+response[i].isRequired);
						
						updateIsDisplayValDynamicFieldVal.push(response[i].fieldId+"@"+response[i].isyearly);
						document.getElementById("updateIsDisplayValDynamicFieldVal").value=updateIsDisplayValDynamicFieldVal.join(", ");
						isDisplayDynamicFieldVal.push(response[i].fieldId+"@"+response[i].isDisplay);
					}
					
					table += '<tr style="width:100%;background-color:   #f3f3f3;">';
					if(response[i].isDisplay==1){  
						table+='<td><input name="CheckBox" type="checkbox" id="CheckBox'+response[i].fieldId+'" value="0" onchange="check(\''+response[i].fieldId+'\',\''+response[i].fieldName+'\',\''+response[i].isStaticOrDynamic+'\');" checked /></td>';
						table+='<input type="hidden" id="isDisplayArray2'+response[i].fieldId+'" name="isDisplayArray2" value="1"/></td>';
					}else{
						table+='<td><input name="CheckBox" type="checkbox" id="CheckBox'+response[i].fieldId+'" value="0" onchange="check(\''+response[i].fieldId+'\',\''+response[i].fieldName+'\',\''+response[i].isStaticOrDynamic+'\');"/></td>';
						table+='<input type="hidden" id="isDisplayArray2'+response[i].fieldId+'" name="isDisplayArray2" value="0"/></td>';
					}
					table += '<td><span id="fieldNameSpan'+response[i].fieldId+'">'+response[i].fieldName+'</span></td>';
					table+='<input type="hidden" id="fieldName'+response[i].fieldId+'" name="fieldNameArray" value="'+response[i].fieldName+'"/></td>';
					table+='<input type="hidden" id="fieldId'+response[i].fieldId+'" name="fieldIdArray" value="'+response[i].fieldId+'"/>';
					table+='<input type="hidden" id="noOfPriorityArray'+response[i].fieldId+''+response[i].fieldName+'" name="noOfPriorityArray" value="'+response[i].priority+'"/></td>';
					
					if(response[i].isRequired==1){
						table+='<td><input name="isRequired1" type="checkbox" id="isRequired1'+response[i].fieldId+'" value="'+response[i].isRequired+'" checked  onchange="checkisRequired('+response[i].fieldId+','+response[i].isStaticOrDynamic+');"  /></td>';
						table+='<input type="hidden" id="isRequiredArray'+response[i].fieldId+'" name="isRequiredArray" value="1"/></td>';
					}
					else{
						table+='<td><input name="isRequired1" type="checkbox" id="isRequired1'+response[i].fieldId+'" value="'+response[i].isRequired+'" onchange="checkisRequired('+response[i].fieldId+','+response[i].isStaticOrDynamic+');"  /></td>';
						table+='<input type="hidden" id="isRequiredArray'+response[i].fieldId+'" name="isRequiredArray" value="0"/></td>';
					}
					if(response[i].isyearly==1){
						table+='<td><input name="isYearly1" type="checkbox" id="isYearly1'+response[i].fieldId+'" value="'+response[i].isyearly+'" checked onchange="checkisyearly('+response[i].fieldId+','+response[i].isStaticOrDynamic+');" />';
						table+='<input type="hidden" id="isYearlyArray'+response[i].fieldId+'" name="isYearlyArray" value="1"/></td>';
					}
					else{
						table+='<td><input name="isYearly1" type="checkbox" id="isYearly1'+response[i].fieldId+'" value="'+response[i].isyearly+'"  onchange="checkisyearly('+response[i].fieldId+','+response[i].isStaticOrDynamic+'); "/>';
						table+='<input type="hidden" id="isYearlyArray'+response[i].fieldId+'" name="isYearlyArray" value="0"/></td>';
					}
					table+='<td><select name="validationFlag" id="validationFlag'+response[i].fieldId+''+response[i].fieldName+'" onchange="checkvalidationFlag(\''+response[i].fieldId+'\',\''+response[i].fieldName+'\')">';
					table+='<option value="0" >Selet validation</option>';
					table+='<option value="1" >Alphabets and Numericals Both</option>';
					table+='<option value="2" >Alphabets Only</option>';
					table+='<option value="3" >Numericals Only</option>';
					table+='</select></td>';
					table+='<input type="hidden" id="validationFlagArray'+response[i].fieldId+''+response[i].fieldName+'" name="validationFlagArray" value="0"/>';
					table+='<input type="hidden" id="dynamicFieldValuePOpUpArrayList2'+response[i].fieldId+'" name="dynamicFieldValuePOpUpArrayList2" value="-"/>';
					table+='<input type="hidden" id="getoldDynamicValue'+response[i].fieldId+'" name="getoldDynamicValue" />';
					table+='<input type="hidden" id="isStaticOrDynamicArray'+response[i].fieldId+'" value="'+response[i].isStaticOrDynamic+'" name="isStaticOrDynamicArray" />';
					 if(response[i].isStaticOrDynamic==0 || response[i].isStaticOrDynamic==2){
						 table+='<input type="hidden" id="fieldTypeIdArray2'+response[i].fieldId+'" name="fieldTypeIdArray2" value="'+response[i].fieldTypeId+'"/>';
						 if(response[i].fieldTypeId==1){
							table+='<td><span>Text Field </span></td>';
						 }
						 if(response[i].fieldTypeId==2){
							table+='<td><span>Combo </span></td>';
						 }
						 if(response[i].fieldTypeId==3){
							table+='<td><span>Date Picker</span></td>';
						 }
						 if(response[i].fieldTypeId==4){
							table+='<td><span>CheckBox </span></td>';
						 }
						 if (response[i].fieldTypeId==5){
							table+='<td><span>Text Area </span></td>';
						 }
						 table+='<td><select name="item" id="priority1'+response[i].fieldId+''+response[i].fieldName+'" onchange="changePriority(\''+response[i].fieldId+'\',\''+response[i].priority+'\',\''+response[i].fieldName+'\')">';
						 table+='<option value="0" >Selet Priority</option>';
						 for(var k=1;k<200;k++){
						  table+='<option value="'+k+'" >'+k+'</option>';
						 }
						 table+='</select></td>';
						 if(response[i].isStaticOrDynamic==2 && (response[i].fieldTypeId==2 || response[i].fieldTypeId==4)){
							table += '<td><input type="button" class="" id="oldDynamicFieldBtn3'+response[i].fieldId+'" onclick="addOldDynamicFieldValue3(\''+response[i].fieldId+'\',\''+response[i].fieldTypeId+'\',\''+response[i].fieldName+'\');" value="Open Model"/></td>';
						 }else{
							table+='<td></td>';
						 }
						table+='</tr>';  
					}  
					 else{
								table+='<td><select name="fieldTypeIdArray2" id="fieldCombo'+response[i].fieldId+'" >'+
								'<option value="0">Select Field</option>'+
								'<option value="1">Text Field</option>'+
							    '<option value="2">Combo</option>'+
							    '<option value="3">Date Picker</option>'+
							    '<option value="4">CheckBox</option>'+
							    '<option value="5">Text Area</option>'+
							    '</select></td>';
								table+='<td><select name="item" id="priority1'+response[i].fieldId+''+response[i].fieldName+'" onchange="changePriority(\''+response[i].fieldId+'\',\''+response[i].priority+'\',\''+response[i].fieldName+'\')">';
								table+='<option value="0" >Selet Priority</option>';
								for(var k=1;k<200;k++){
								 table+='<option value="'+k+'" >'+k+'</option>';
								}
								table+='</select></td>';
								if(response[i].fieldTypeId==2 || response[i].fieldTypeId==4){ 
									table += '<td><input type="button" class="" id="dynamicFieldBtn2'+response[i].fieldId+'" onclick="addDynamicFieldValue2('+response[i].fieldId+','+response[i].fieldTypeId+');" value="Open Model"/></td>';
								}else{
									table+='<td></td>';
								}
								table+='</tr>'; 
					}   
				}
				$('#staffAttendenceReport').append(table);
				 for (i = 0; i < response.length; i++) {
					document.getElementById("priority1"+response[i].fieldId+response[i].fieldName).value=document.getElementById("noOfPriorityArray"+response[i].fieldId+response[i].fieldName).value;
					document.getElementById("validationFlag"+response[i].fieldId+response[i].fieldName).value=response[i].dynamicvalidationFlag;
					document.getElementById("validationFlagArray"+response[i].fieldId+response[i].fieldName).value=response[i].dynamicvalidationFlag;
					if(response[i].isStaticOrDynamic=='1') {
						document.getElementById("fieldCombo"+response[i].fieldId).value=response[i].fieldTypeId;
					}
				}  
			},
			error : function(xhr, status, error) {
			}
		});
	}
	function addOldDynamicFieldValue3(fieldId,fieldTypeId,fieldName){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var data = 'fieldName=' + fieldName+'&schoolId=' + schoolId;
		$.ajax({
			type : "GET",
			url : "/getOldDynamicFieldValueId",
			data : data,
			dataType : 'json',
			async:false,
			cache : false,
			timeout : 600000,
			async:false,
			success : function(response) {
				 response=response.responseList;
				 oldDynamicFieldId=response[0].fieldId;
				fieldTypeId=response[0].fieldName;
				setOldDynamicFieldValue(oldDynamicFieldId,fieldTypeId);
			},
			error : function(xhr, status, error) {
			}
		});	
	}
	function setOldDynamicFieldValue(oldDynamicFieldId,fieldTypeId){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var name=null;
		var data = 'oldDynamicFieldId=' + oldDynamicFieldId + '&schoolId=' + schoolId+ '&fieldTypeId=' + fieldTypeId ;
		$.ajax({
			type : "POST",
			url : "/setOldDynamicFieldValue",
			data : data,
			async:false,
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(response) {
				response=response.responseList;
				name=response[0].fieldValue;
				for (i = 1; i < response.length; i++) {
					name+=','+response[i].fieldValue;
				}
				var oldDynamicComboVal="";
				oldDynamicComboVal = prompt("Please Enter Value for Dynamic CheckBox In Coma-Coma.",name);
			},
			error : function(xhr, status, error) {
			}
		});  
	}  
	
	function addDynamicFieldValue2(dynamicFieldId,fieldTypeId) {
			var schoolId= document.getElementById('schoolKey').innerHTML;
			var data = 'dynamicField=' +encodeURIComponent(dynamicFieldId)+'&schoolId='+schoolId;
			var name=null;
			$.ajax({
				type : "GET",
				url : "/getDynamicFieldComboValue",
				data : data,
				async:false,
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(response) {
					response=response.responseList;
					name=response[0].dynamicFieldComboValue;
					for (i = 1; i < response.length; i++) {
						name+=','+response[i].dynamicFieldComboValue;
					}
				},
				error : function(xhr, status, error) {
				}
			});	
			var person2="";
			person2 = prompt("Please Enter Value for Dynamic CheckBox In Coma-Coma.",name);
			if (person2 != "") {
				var newStr2 = person2.replace(/,/g, '$');
			 	document.getElementById("dynamicFieldValuePOpUpArrayList2"+dynamicFieldId).value=newStr2;
			}
	}
	function changePriority(fieldId,previousPriority,fieldName){
			var priority = document.getElementById("priority1"+fieldId+fieldName).value;
			var count=0;
			for (i = 0; i < dataResponse.length; i++) {
				var pr=document.getElementById("priority1"+dataResponse[i].fieldId+dataResponse[i].fieldName).value;
				 if(priority==pr){
					count=count+1;
				} 
			}
			if(count>1){
				alert("Priority Number Repeted ! ");
				document.getElementById("noOfPriorityArray"+fieldId+fieldName).value="0";
				document.getElementById("priority1"+fieldId+fieldName).value=previousPriority;
			}else{
				document.getElementById("noOfPriorityArray"+fieldId+fieldName).value=priority;
			}
	}
	 function check(fieldId,staticFieldName,isDynamicOrStaticField){
			if (document.getElementById("CheckBox"+fieldId).checked) {
				var count=0;
				var schoolId= document.getElementById('schoolKey').innerHTML;
				var data = 'staticFieldName=' + staticFieldName+'&schoolId='+schoolId;
				$.ajax({
					type : "GET",
					url : "/checkStaticFieldIsDisplayOrNot",
					data : data,
					async:false,
					dataType : 'json',
					cache : false,
					timeout : 600000,
					success : function(response) {
						response=response.responseList;
						if(response.length>0){
							alert("Field "+staticFieldName+" is already selected in '"+ response[0].legendName+"' Legend of '" + response[0].tabsName+ "' Tab .");
							document.getElementById("CheckBox"+fieldId).checked=false;
							count=count+1;
						}
					},
					error : function(xhr, status, error) {
					}
				});	
				
			if(count=="0"){
				document.getElementById("CheckBox"+fieldId).value="1";
				document.getElementById("isDisplayArray2"+fieldId).value="1";
				var priorityvalue = document.getElementById("priority1"+fieldId+staticFieldName);
				var dynamicpriorityvalue =priorityvalue.options[priorityvalue.selectedIndex].value;
				document.getElementById("isDisplayArray2"+fieldId).value="1";
				 if(dynamicpriorityvalue != "0"){
					document.getElementById("noOfPriorityArray"+fieldId+staticFieldName).value=dynamicpriorityvalue;
				}else{
					document.getElementById("noOfPriorityArray"+fieldId+staticFieldName).value="0";
					document.getElementById("CheckBox"+fieldId).checked=false;
					alert("Please Select Priority First !");
				} 
			}
			}else{
				document.getElementById("CheckBox"+fieldId).value="0";
				document.getElementById("isDisplayArray2"+fieldId).value="0";
			}
			if(isDynamicOrStaticField==1 || isDynamicOrStaticField==2){
				for (i = 0; i < isDisplayDynamicFieldVal.length; i++) {
					var entryArray = isDisplayDynamicFieldVal[i].split("@");
					if(fieldId==entryArray[0]){
						if(entryArray[1]==0){
							isDisplayDynamicFieldVal[i]=fieldId+"@1";
						}else{
							isDisplayDynamicFieldVal[i]=fieldId+"@0";
						}
					}
				}
			}
		} 
				
	function checkisRequired(dynamicFieldId,isStaticOrDynamic){
		if(isStaticOrDynamic==1 || isStaticOrDynamic==2){
			for (i = 0; i < isRequiredDynamicFieldVal.length; i++) {
				var entryArray = isRequiredDynamicFieldVal[i].split("@");
				if(dynamicFieldId==entryArray[0]){
					if(entryArray[1]==0){
						isRequiredDynamicFieldVal[i]=dynamicFieldId+"@1";
					}else{
						isRequiredDynamicFieldVal[i]=dynamicFieldId+"@0";
					}
				}
			}
		} else{
			if (document.getElementById("isRequired1"+dynamicFieldId).checked) {
				document.getElementById("isRequiredArray"+dynamicFieldId).value="1";
			}else{
				document.getElementById("isRequiredArray"+dynamicFieldId).value="0";
			}
		}
	}	
	
	var updateDynamicFieldVal = [];
	var isYearlyDynamicFieldVal = [];
	
	var updateIsRequiredDynamicFieldVal = [];
	var isRequiredDynamicFieldVal = [];
	
	var updateIsDisplayValDynamicFieldVal = [];
	var isDisplayDynamicFieldVal = [];
	
	function checkisyearly(dynamicFieldId,isStaticOrDynamic){
		if(isStaticOrDynamic==1 || isStaticOrDynamic==2){
			for (i = 0; i < isYearlyDynamicFieldVal.length; i++) {
				var entryArray = isYearlyDynamicFieldVal[i].split("@");
				if(dynamicFieldId==entryArray[0]){
					if(entryArray[1]==0){
						isYearlyDynamicFieldVal[i]=dynamicFieldId+"@1";
					}else{
						isYearlyDynamicFieldVal[i]=dynamicFieldId+"@0";
					}
				}
			}
		} else{
			if (document.getElementById("isYearly1"+dynamicFieldId).checked) {
				document.getElementById("isYearlyArray"+dynamicFieldId).value="1";
			}else{
				document.getElementById("isYearlyArray"+dynamicFieldId).value="0";
			}
		}
	} 
	function checkvalidationFlag(dynamicFieldId,dynamicFieldName){     
			var fieldvalue = document.getElementById("validationFlag"+dynamicFieldId+dynamicFieldName);
			var validationFlag=fieldvalue.options[fieldvalue.selectedIndex].value;
			document.getElementById("validationFlagArray"+dynamicFieldId+dynamicFieldName).value=validationFlag;
	} 
	function checkDynamic(dynamicFieldId){
		if (document.getElementById("dynamicIsDisplay"+dynamicFieldId).checked) {
			document.getElementById("currentLegendId1"+dynamicFieldId).value=document.getElementById("currentLegendId").value;
			var table = "";
			var fieldvalue = document.getElementById("dynamicFieldType"+dynamicFieldId);
			var dynamicfieldvalue=fieldvalue.options[fieldvalue.selectedIndex].value;
			var priorityvalue = document.getElementById("dynamicPriority"+dynamicFieldId);
			var dynamicpriorityvalue =priorityvalue.options[priorityvalue.selectedIndex].value;
			var dynamicFieldNameArray= document.getElementById("dynamicFieldNameArray"+dynamicFieldId).value;
			if(dynamicFieldNameArray=="" || dynamicFieldNameArray==null){
				document.getElementById("dynamicIsDisplay"+dynamicFieldId).checked= false;
				alert("Please Insert Field Name !");
			}
			else if(dynamicfieldvalue != "0" && dynamicpriorityvalue != "0"){
				document.getElementById("dynamicIsDisplayArray2"+dynamicFieldId).value="1";
				document.getElementById("dynamicFieldTypeArray"+dynamicFieldId).value=dynamicfieldvalue;
				document.getElementById("dynamicPriorityArray"+dynamicFieldId).value=dynamicpriorityvalue;
				var dynamicFieldValuePOpUpArrayList = document.getElementById("dynamicFieldValuePOpUpArrayList"+dynamicFieldId).value;
				if(dynamicfieldvalue=="2" || dynamicfieldvalue=="4"){
					if(dynamicFieldValuePOpUpArrayList=="" || dynamicFieldValuePOpUpArrayList==null){
						document.getElementById("dynamicIsDisplay"+dynamicFieldId).checked= false;
						alert("Please Insert Atleast One Value For ComboBox Or CheckBox !");
					}
				}	
			}else{
				document.getElementById("dynamicIsDisplay"+dynamicFieldId).checked= false;
				alert("Please Select Text Field and Priority First !");
			}
		}else{
			document.getElementById("dynamicIsDisplayArray2"+dynamicFieldId).value="0";
			document.getElementById("dynamicFieldTypeArray"+fieldId).value="0";
			document.getElementById("dynamicPriorityArray"+fieldId).value="0";
		}
	}
	function dynamicIsRequiredCheck(dynamicFieldId){
		if (document.getElementById("dynamicIsRequired"+dynamicFieldId).checked) {
			document.getElementById("dynamicIsRequiredArray"+dynamicFieldId).value="1";
		}else{
			document.getElementById("dynamicIsRequiredArray"+dynamicFieldId).value="0";
		}
	}
	function dynamicIsYearlyCheck(dynamicFieldId){
		if (document.getElementById("dynamicIsYearly"+dynamicFieldId).checked) {
			document.getElementById("dynamicIsYearlyArray"+dynamicFieldId).value="1";
		}else{
			document.getElementById("dynamicIsYearlyArray"+dynamicFieldId).value="0";
		}
	}
	
	var addDynamicFieldValueCount = 1;
	function addDynamicFieldValuePopUp(){
		var table = "";
		table += '<tr style="width:100%">';
		table+='<td><input type="text" id="dynamicFieldValueArray'+addDynamicFieldValueCount+'" name="dynamicFieldValueArray"/>';
		table+='<input type="hidden" id="dynamicFieldValueArray2'+addDynamicFieldValueCount+'" name="dynamicFieldValueArray2" value="0"/></td>';
		table+='</tr>';
		$('#dynamicFieldValueTable').append(table);
		addDynamicFieldcount += 1;
	}
	function saveOldFieldValue(oldFieldType,oldfieldName,staticOrDynamic,tabId){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var data = 'tabId=' + 1 + '&schoolId=' +schoolId+ '&oldFieldType=' + oldFieldType + '&oldfieldName=' + oldfieldName;
		$.ajax({
			type : "POST",
			url : "/saveOldFieldValueName",
			data : data,
			dataType : 'json',
			cache : false,
			async:false,
			timeout : 600000,
			success : function(response) {
				fieldId=response.responseData;
			},
			error : function(xhr, status, error) {
			}
		});
	}
	function getOldDynamicFields(tabId){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var data = 'schoolId=' +schoolId;
		$.ajax({
			type : "GET",
			url : "/getOldDynamicFields",
			data : data,
			dataType : 'json',
			async:false,
			cache : false,
			timeout : 600000,
			success : function(response) {
				response=response.responseList;
				var oldFieldType="0";
				 for (i = 0; i < response.length; i++) {	
					var fieldTypeId=response[i].fieldTypeId;
					var fieldName=response[i].fieldName;
					saveOldFieldValue(fieldTypeId,fieldName,2,tabId);
				} 
			},
			error : function(xhr, status, error) {
			}
		});
	}   
	function checkOldField(fieldId){
		if (document.getElementById("OldFieldCheckBox"+fieldId).checked) {
			document.getElementById("OldFieldCheckBox"+fieldId).value="1";
			document.getElementById("isOldDisplayArray2"+fieldId).value="1";
			var priorityvalue = document.getElementById("oldPriority1"+fieldId);
			var dynamicpriorityvalue =priorityvalue.options[priorityvalue.selectedIndex].value;
			document.getElementById("isOldPriorityArray2"+fieldId).value="1";
			 if(dynamicpriorityvalue != "0"){
				document.getElementById("isOldPriorityArray2"+fieldId).value=dynamicpriorityvalue;
			}else{
				document.getElementById("isOldPriorityArray2"+fieldId).value="0";
				document.getElementById("OldFieldCheckBox"+fieldId).checked=false;
				alert("Please Select Priority First !");
			} 
		}else{
			document.getElementById("isOldDisplayArray2"+fieldId).value="0";
			document.getElementById("isOldPriorityArray2"+fieldId).value="0";
		}
	} 
	function checkisRequiredOldField(dynamicFieldId){
		if (document.getElementById("isOldRequiredArray"+dynamicFieldId).checked) {
			document.getElementById("isOldRequiredArray2"+dynamicFieldId).value="1";
		}else{
			document.getElementById("isOldRequiredArray2"+dynamicFieldId).value="0";
		}
	}	
	function checkisyearlyOldField(dynamicFieldId){
		if (document.getElementById("isOldYearlyArray"+dynamicFieldId).checked) {
			document.getElementById("isOldYearlyArray2"+dynamicFieldId).value="1";
		}else{
			document.getElementById("isOldYearlyArray2"+dynamicFieldId).value="0";
		}
	} 
	function checkPriorityOldField(fieldId){
		var x = document.getElementById("isOldPriorityArray"+fieldId).value;
		document.getElementById("isOldPriorityArray2"+fieldId).value=x;
	}
	
	var addDynamicFieldcount = 1;
	function addDynamicField() {
		var table = "";
		table += '<tr style="width:100%;background-color:  #f3f3f3;">';
	    table+='<td><input name="dynamicIsDisplay" type="checkbox" id="dynamicIsDisplay'+addDynamicFieldcount+'" value="0" onchange="checkDynamic('+addDynamicFieldcount+');"/></td>';
		table+='<td><input type="text" id="dynamicFieldNameArray'+addDynamicFieldcount+'" name="dynamicFieldNameArray" maxlength="255"/>';
		table+='<input type="hidden" id="dynamicIsDisplayArray2'+addDynamicFieldcount+'" name="dynamicIsDisplayArray2" value="0"/></td>';
		table+='<td><input name="dynamicIsRequired" type="checkbox" id="dynamicIsRequired'+addDynamicFieldcount+'"  value="0" onchange="dynamicIsRequiredCheck('+addDynamicFieldcount+');"/></td>';
		table+='<td><input name="dynamicIsYearly" type="checkbox" id="dynamicIsYearly'+addDynamicFieldcount+'" value="0" onchange="dynamicIsYearlyCheck('+addDynamicFieldcount+');" /></td>';
		table+='<td><select name="dynamicvalidationFlag" id="dynamicvalidationFlag'+addDynamicFieldcount+'" onchange="dynamiccheckvalidationFlag('+addDynamicFieldcount+')">';
		table+='<option value="0" >Selet validation</option>';
		table+='<option value="1" >Alphabets and Numericals Both</option>';
		table+='<option value="2" >Alphabets Only</option>';
		table+='<option value="3" >Numericals Only</option>';
		table+='</select></td>';
		table+='<input type="hidden" id="dynamicvalidationFlagArray'+addDynamicFieldcount+'" name="dynamicvalidationFlagArray" value="0"/>';
		table+='<input type="hidden" id="dynamicIsRequiredArray'+addDynamicFieldcount+'" name="dynamicIsRequiredArray" value="0"/>';
		table+='<input type="hidden" id="dynamicIsYearlyArray'+addDynamicFieldcount+'" name="dynamicIsYearlyArray" value="0"/>';
		table+='<input type="hidden" id="dynamicFieldTypeArray'+addDynamicFieldcount+'" name="dynamicFieldTypeArray" value="0"/>';
		table+='<input type="hidden" id="dynamicPriorityArray'+addDynamicFieldcount+'" name="dynamicPriorityArray" value="0"/>';
		table+='<input type="hidden" id="currentLegendId1'+addDynamicFieldcount+'" name="dynamicCurrentLegendId" value=""/>';
		table+='<input type="hidden" id="dynamicFieldValuePOpUpArrayList'+addDynamicFieldcount+'" name="dynamicFieldValuePOpUpArrayList" />';
		table+='<td><select name="item" id="dynamicFieldType'+addDynamicFieldcount+'" onchange="showPopUpBtn('+addDynamicFieldcount+')">'+
		'<option value="0" >Selet Field Type</option>'+
		'<option value="1">Text Field</option>'+
	    '<option value="2" >Combo</option>'+
	    '<option value="3">Date Picker</option>'+
	    '<option value="4">CheckBox</option>'+
	    '<option value="5">Text Area</option>'+
	    '</select></td>'; 
	    table+='<td><select name="item" id="dynamicPriority'+addDynamicFieldcount+'" onchange="changeDynamicPriority('+addDynamicFieldcount+')" >';
		table+='<option value="0" >Selet Priority</option>';
		for(var k=1;k<200;k++){
		 table+='<option value="'+k+'" >'+k+'</option>';
		}
		table+='</select></td>';
		table += '<td><input type="button" class="hide" id="dynamicFieldBtn'+addDynamicFieldcount+'" onclick="addDynamicFieldValue('+addDynamicFieldcount+');" value="Open Model"/></td>';
	    table+='</tr>'; 
	    $('#staffAttendenceReport').append(table);
	    document.getElementById("dynamicFieldBtn"+addDynamicFieldcount).style.display = "none"; 
	    addDynamicFieldcount++;
	}
	
	function showPopUpBtn(dynamicFieldId){
		var fieldvalue = document.getElementById("dynamicFieldType"+dynamicFieldId);
		var dynamicfieldvalue=fieldvalue.options[fieldvalue.selectedIndex].value;
		if(dynamicfieldvalue != "0"){
			document.getElementById("dynamicIsDisplayArray2"+dynamicFieldId).value="1";
			document.getElementById("dynamicFieldTypeArray"+dynamicFieldId).value=dynamicfieldvalue;
			if(dynamicfieldvalue =="2" || dynamicfieldvalue=="4"){
				document.getElementById("dynamicFieldBtn"+dynamicFieldId).style.display = "block";
			}else{
				document.getElementById("dynamicFieldBtn"+dynamicFieldId).style.display = "none";
			}
		}
	}
	
	function changeDynamicPriority(fieldId){
		var priority = document.getElementById("dynamicPriority"+fieldId).value;
		//var fieldName=document.getElementById("fieldName"+fieldId).value;
		var count=0;
		for (i = 0; i < dataResponse.length; i++) {
			var pr=document.getElementById("priority1"+dataResponse[i].fieldId+dataResponse[i].fieldName).value;
			 if(priority==pr){
				count=count+1;
			} 
		}
		if(count>0){
			alert("Priority Number Repeted !");
			document.getElementById("dynamicPriorityArray"+fieldId).value="0";
			document.getElementById("dynamicPriority"+fieldId).value="0";
		}else{
			document.getElementById("dynamicPriorityArray"+fieldId).value=priority;
		}
	}
	function dynamiccheckvalidationFlag(dynamicFieldId){     
		var fieldvalue = document.getElementById("dynamicvalidationFlag"+dynamicFieldId);
		var validationFlag=fieldvalue.options[fieldvalue.selectedIndex].value;
		document.getElementById("dynamicvalidationFlagArray"+dynamicFieldId).value=validationFlag;
	} 
	function addDynamicFieldValue(dynamicFieldId) {
		var person="";
		var fieldvalue = document.getElementById("dynamicFieldType"+dynamicFieldId);
		var dynamicfieldvalue=fieldvalue.options[fieldvalue.selectedIndex].value;
		 if(dynamicfieldvalue == "2") {
			person = prompt("Please Enter Value for Dynamic Combo In Coma-Coma.");
		}
		if(dynamicfieldvalue == "4"){
			person = prompt("Please Enter Value for Dynamic CheckBox In Coma-Coma.");
		} 
		if (person != "") {
			var newStr = person.replace(/,/g, '$');
		 	document.getElementById("dynamicFieldValuePOpUpArrayList"+dynamicFieldId).value=newStr;
		}
	}
	function getlegendName(tabId){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		document.getElementById("schoolId").value=schoolId;
		document.getElementById("currentTabName").value=tabId;
		$('#staffAttendenceReport').empty();
		var data = 'tabId=' + tabId + '&schoolId=' + schoolId;
		$.ajax({
			type : "GET",
			url : "/getlegendName",
			data : data,
			dataType : 'json',
			async:false,
			cache : false,
			timeout : 600000,
			success : function(response) {  
				response=response.responseList;
				$('#legendName').empty();
				var table = "";
				for (i = 0; i < response.length; i++) {
					table += '<br/>';
					table += '<tr>';
					table += '<td><input type="text" id="dblegendName'+response[i].legendId+'" value="'+response[i].legendName+'" maxlength="200" onchange="updateLegendName('+response[i].legendId+');"/><input type="hidden" id="dblegendId'+response[i].legendId+'" value="'+response[i].legendId+'"/></td>';
					table += '<td><button id="popUPLegendBtn'+response[i].legendId+'" onclick="getLegendId('+response[i].legendId+','+tabId+');" class="myButton">Open Modal</button></td>';	
					table += '<td><input type="button" id="dbremovelegendName'+response[i].legendId+'" onclick="removeInput('+response[i].legendId+',1,'+tabId+')" value="remove" class="myButton"/></td><br/>';	
					table += '</tr>';
				}
					$('#legendName'+tabId).empty();
					$('#legendName'+tabId).append(table);
			},
			error : function(xhr, status, error) {
			}
		});
	}
	function updateLegendName(legendId){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var legendName=document.getElementById("dblegendName"+legendId).value; 
		var data = 'legendId=' + legendId + '&legendName=' + legendName + '&schoolId=' + schoolId;
			$.ajax({
				type : "POST",
				url : "/updateLegendName",
				data : data,
				dataType : 'json',
				async:false,
				cache : false,
				timeout : 600000,
				success : function(response) {
					alert("Legend Name successfully Changed !");
				},
				error : function(xhr, status, error) {
					return false;
				}
			});
		}
	function getLegendId(legendId,tabId){
		//document.getElementById("legendId1").value=document.getElementById("dblegendId"+legendId).value; 
		document.getElementById("currentLegendId").value=document.getElementById("dblegendId"+legendId).value;
		// Get the modalPopup
		var modalPopup = document.getElementById("myModal");
		// Get the button that opens the modalPopup
		var btn = document.getElementById("popUPLegendBtn"+legendId);
		// Get the <span> element that closes the modalPopup
		var span = document.getElementsByClassName("close")[0];
		// When the user clicks the button, open the modalPopup 
		getAllFeilds(tabId);
		modalPopup.style.display = "block";
		// When the user clicks on <span> (x), close the modalPopup
		span.onclick = function() {
		  modalPopup.style.display = "none";
		}
		// When the user clicks anywhere outside of the modalPopup, close it
		window.onclick = function(event) {
		  if (event.target == modalPopup) {
		    modalPopup.style.display = "none";
		  }
		} 
	}
	
	function getDynamicLegendId(legendId,tabId){
		//document.getElementById("legendId1").value=document.getElementById("dblegendId"+legendId).value; 
		document.getElementById("currentLegendId").value=document.getElementById("legendId"+legendId).value;
		// Get the modalPopup
		var modalPopup = document.getElementById("myModal");
		// Get the button that opens the modalPopup
		var btn = document.getElementById("myBtn"+legendId);
		// Get the <span> element that closes the modalPopup
		var span = document.getElementsByClassName("close")[0];
		// When the user clicks the button, open the modalPopup 
	      getAllFeilds(tabId);
		  modalPopup.style.display = "block";
		// When the user clicks on <span> (x), close the modalPopup
		span.onclick = function() {
		  modalPopup.style.display = "none";
		}
		// When the user clicks anywhere outside of the modalPopup, close it
		window.onclick = function(event) {
		  if (event.target == modalPopup) {
		    modalPopup.style.display = "none";
		  }
		} 
	}
	
	var countBox = 1;
	var boxName = 0;
	function addInput(tabId) {
		document.getElementById("currentTabName").value=tabId;
		var boxName = "textBox" + countBox;
		document.getElementById("responce"+tabId).innerHTML+='<br/>'+
		'<input type="text" id="'+boxName+'" maxlength="200"  onblur="saveLegendName('+countBox+','+tabId+');" /><input type="hidden" id="legendId'+countBox+'"/>'+
		'<button id="myBtn'+countBox+'" class="myButton"  onclick="getDynamicLegendId('+countBox+','+tabId+');">Open Modal</button>'+
		'<input type="button" id="removebtn'+countBox+'" onclick="removeInput('+countBox+',0,'+tabId+')" value="remove" class="myButton"/><br/>';
		countBox += 1;
		getlegendName(tabId);
	}
	function saveLegendName(countBox,tabId){
		var schoolId= document.getElementById('schoolKey').innerHTML;
		var legendName=document.getElementById("textBox" + countBox).value;
		var data = 'tabId=' + tabId +'&legendName=' +legendName+ '&schoolId=' + schoolId;
		$.ajax({
			type : "POST",
			url : "/saveLegendName",
			data : data,
			dataType : 'json',
			async:false,
			cache : false,
			timeout : 600000,
			success : function(response) {
				response=response.responseData;
				document.getElementById("legendId" + countBox).value=response;
				alert("Legend Added successfully")
			},
			error : function(xhr, status, error) {
			}
		});
	}
	function removeInput(boxName1,type,tabId) {
		//document.getElementById("myBtn"+boxName1).style.display="None";
		var schoolId= document.getElementById('schoolKey').innerHTML;
		if(type==0){
			var legendId=document.getElementById("legendId" + boxName1).value;
		}else{
			var legendId=document.getElementById("dblegendId" + boxName1).value;
		}
		var data = 'tabId=' + tabId +'&legendId=' +legendId+ '&schoolId=' + schoolId;
		$.ajax({
			type : "POST",
			url : "/deleteLegendName",
			data : data,
			async:false,
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(response) {
				if(response.statusCode=='200') {
					alert("Legend Removed Successfully.");
					if(type==0){
						document.getElementById("textBox" + boxName1).style.display = 'none';
						document.getElementById("removebtn" + boxName1).style.display = 'none';
						document.getElementById("myBtn"+ boxName1).style.display = 'none';
					}else{
						document.getElementById("dblegendName" + boxName1).style.display = 'none';
						document.getElementById("dbremovelegendName" + boxName1).style.display = 'none';
						document.getElementById("popUPLegendBtn" + boxName1).style.display = 'none';
					}
				}
				else {
					alert("Legend Already In Use.");
				}
			},
			error : function(xhr, status, error) {
				ALERT(1);
				alert("Legend Already In Use.");
			}
		});
		
	}
	function checkData() {
		var priorityArray= document.getElementsByName("noOfPriorityArray");
		var displayArray = document.getElementsByName("isDisplayArray2");
		for(var i=0;i<priorityArray.length;i++) {
			if(displayArray[i].value=='1' && (priorityArray[i].value== null || priorityArray[i].value=='' || priorityArray[i].value=='null')){
				alert("Please select priority")
				return false;
			}
		}
		
		var yearly="";
		 for (i = 0; i < isYearlyDynamicFieldVal.length; i++) {
			 if(i==0){
				 yearly=isYearlyDynamicFieldVal[i];
			 }else{
				 yearly=yearly+","+isYearlyDynamicFieldVal[i];
			 }
		} 
		 $('.updateisYearlyVal').val(yearly);
		 
		 var req="";
		 for (i = 0; i < isRequiredDynamicFieldVal.length; i++) {
			 if(i==0){
				 req=isRequiredDynamicFieldVal[i];
			 }else{
				 req=req+","+isRequiredDynamicFieldVal[i];
			 }
		} 
		 $('.updateisRequiredVal').val(req);
		 
		 var display="";
		 for (i = 0; i < isDisplayDynamicFieldVal.length; i++) {
			 if(i==0){
				 display=isDisplayDynamicFieldVal[i];
			 }else{
				 display=display+","+isDisplayDynamicFieldVal[i];
			 }
		} 
		 $('.updateisDisplayVal').val(display);
	}
</script>
</head>
<body onload="tabsName();">
	<%
	String schoolKey = (String) session.getAttribute("schoolId");
	%>
	
	<span id="schoolKey" style="display: none;"><%=schoolKey %></span>
	<table style="width: 100%; border: solid 0px; padding-left: 10%;">
		<tr>
			<td>
				<div style="width: 1000px;">
					<div id="header" style="width: 100%;background-color: #678889;">
						<table style="width: 100%;background: #678889;color: white;">
							<tr>	
								<td colspan="3">
									Welcome <span id='userMobile'>${sessionScope.welcomeName}</span>&nbsp;
								</td>
								<td style="float: right;">
									<form action="/logout" method="post">
			    						<input class="logout" type="submit" style="color: red;" value="Logout" />
									</form>
								</td>
							</tr>
							<tr>
								<td colspan="4" id="letterHead"></td>
							</tr>
							<tr style="line-height: 30px;text-align: center;">
								<td style="border: 1px solid;width: 170px;cursor: pointer;">Admission Form Setting</td>
								<td></td>
							</tr>
						</table>
					</div>
					<table id="tableid"></table>
					<table id="tableTabsDiv" style="width: 96%;"></table>
					<div id="footer" style="width: 100%;">
						<table style="width: 100%; background: #678889; color: white; height: 50px;">
							<tr>
								<td><a class="navbar-brand" href="https://eprashasan.com" target="_blank"><font size="4" color="orange">e</font><font size="4" color="white">-Prashasan</font></a></td>
								<td style="text-align: right; color: white;">2013-2019 � INGENIO Technologies Pvt. Ltd. (V. 1.17) Call Us: 7276004262</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
	</table>	
	 <!-- ***************** POP-UP **************************** -->	
	<div id="myModal" class="modalPopup" style="width:100% !important;text-align: center;padding-left: 0%;">
	  <!-- Modal content -->
	  <div class="modalPopup-content">
	    <span class="close">&times;</span>
		   <div id="staffAttendenceReportDiv2" class="" style="max-height:500px; overflow-y:auto;">
		   <form action="/updateField" method="post"  modelAttribute="admissionSettingBean" id="myForm">  
				<table style="width: 100%;" border="1" id="staffAttendenceReport"></table><br/>
				<input type="hidden" id="currentTabName" name="currentTabName"/>
				<input type="hidden" id="updateisYearlyVal" name="updateisYearlyVal" class="updateisYearlyVal" />	
				<input type="hidden" id="updateisRequiredVal" name="updateisRequiredVal" class="updateisRequiredVal" />	
				<input type="hidden" id="updateisDisplayVal" name="updateisDisplayVal" class="updateisDisplayVal" />	
				<input type="hidden" id="currentLegendId" name="currentLegendId"/>
				<input type="hidden" id="currentLegendIdFinal" name="currentLegendIdFinal"/>
				<input type="hidden" id="schoolId" name="schoolId"/>
				<input type="submit" id="saveLegendField" onclick="return checkData();" value="Save" class="myButton" /><br/><br/><br/>
			</form>
			 	<input type="button" onclick="addDynamicField()" value="Add Dynamic Field" class="myButton"/><br/><br/>
			</div>
	  </div>
	</div>	
	<input type="hidden" id="updateDynamicFieldVal" />	
	<input type="hidden" id="updateIsRequiredDynamicFieldVal" />	
	<input type="hidden" id="updateIsDisplayValDynamicFieldVal" />	 
</body>
</html>