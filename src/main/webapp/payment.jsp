
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link rel="stylesheet" href='<c:url value="/resources/css/jquery.ui.all.css"/>'>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4">

<head>
<meta charset="utf-8">
	
<title>Student Admission Form</title>
<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<style>
</style>
<script src="${contextPath}/resources/js/jquery-1.10.2.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script src='<c:url value="/resources/js/jquery-1.10.2.js"/>'></script>
<script src='<c:url value="/resources/js/jquery.ui.core.js"/>'></script>
<script src='<c:url value="/resources/js/jquery.ui.widget.js"/>'></script>
<script src='<c:url value="/resources/js/jquery.ui.datepicker.js"/>'></script>
<script src='<c:url value="/resources/css/bootstrap.min.css"/>'></script>
<script src='<c:url value="/resources/js/bootstrap.min.js"/>'></script>

<style type="text/css">

.myButton {
	border-radius: 6px !important;
	width: 100px !important;
}

body, html {
	height: 100%;
	margin: 0;
	font-family: Arial, Helvetica, sans-serif;
}

.fieldset {
	margin-bottom: 10px;
}

.tablink, .stdlink {
	/* background-color: #678889; */
	background-color: #337e96;
	color: white;
	float: left;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 0px;
	font-size: 17px;
	width: 20%;
}

 .CheckBoxLabelClass{
    background: url("uncheck222.png") no-repeat;
    padding-left: 5px;
    padding-top: 3px;
    padding-right: 10px;
    margin: 5px;
    height: 60px;   
    width: 60px;
    display: block;
}

.stdlink {
	width: 9%;
}

.tablink:hover {
	background-color: rgb(167, 176, 177);
}

.tabcontent {
	color: white;
	display: none;
	padding: 20px 20px;
	min-height: 550px;
	height: 100%;
	background-color: #f3f3f3;
	text-align: center;
}

.hide {
	display: none;
}

.legend {
	color: rgb(160, 5, 11);
	font-size: 17px;
	font-weight: bold;
}

.inputSelect {
	width: 375px;
	height: 35px;
	font-size: 20px;
	background-color: #f7f7f7;
    font-size: 17px;
}

.inputText {
	width: 375px;
	height: 35px;
	font-size: 20px;
	background-color: #f7f7f7;
    font-size: 17px;
}

.textArea {
	width: 375px;
    font-size: 17px;
}

.dd, .mm, .yy {
	height: 35px;
	width: 121px;
	font-size: 20px;
	background-color: #f7f7f7;
    font-size: 17px;
}

.legendContent {
	line-height: 2;
	margin-bottom: 10px;
	margin-left: 245px;
}

.submit {
	width: 150px;
	border: 1px solid #25729a;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	font-size: 14px;
	font-family: arial, helvetica, sans-serif;
	padding: 10px 10px 10px 10px;
	text-decoration: none;
	display: inline-block;
	text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.3); /* font-weight:bold; */
	color: #FFFFFF;
	background-color: #3093c7;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#3093c7),
		to(#1c5a85));
	background-image: -webkit-linear-gradient(top, #3093c7, #1c5a85);
	background-image: -moz-linear-gradient(top, #3093c7, #1c5a85);
	background-image: -ms-linear-gradient(top, #3093c7, #1c5a85);
	background-image: -o-linear-gradient(top, #3093c7, #1c5a85);
	background-image: linear-gradient(to bottom, #3093c7, #1c5a85);
	filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,
		startColorstr=#3093c7, endColorstr=#1c5a85);
}

.submit:hover {
	border: 1px solid #1c5675;
	background-color: #26759e;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#26759e),
		to(#133d5b));
	background-image: -webkit-linear-gradient(top, #26759e, #133d5b);
	background-image: -moz-linear-gradient(top, #26759e, #133d5b);
	background-image: -ms-linear-gradient(top, #26759e, #133d5b);
	background-image: -o-linear-gradient(top, #26759e, #133d5b);
	background-image: linear-gradient(to bottom, #26759e, #133d5b);
	filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,
		startColorstr=#26759e, endColorstr=#133d5b);
}

.regForm table .input {
    width: 188px;
    padding: 15px 25px;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-weight: 400;
    font-size: 14px;
    color: #9d9e9e;
    text-shadow: 1px 1px 0 rgba(255, 255, 255, 1);
    background: #fff;
    border: 1px solid #fff;
    border-radius: 5px;
}

.regForm{
	padding: 20px;
    width: 608px;
    display: block;
    /* margin-left: 550px; */
    /* height: 536px; */
    background-color: #d4dedf;
    margin-top: 50px;
    margin-left: 550px;
}
.input-select{
    width: 239px !important;
        background-color: #f7f7f7;
 }
 
 .vid{
    margin-left: 450px;
    margin-top: 25px;
    margin-bottom: 20px;
 }

.required {
	color: rgb(216, 13, 21);
	font-size: 20px;
	font-weight: bold;
}

.attachmentLabels {
	font-size: 14px;
	color: #052396;
	font-weight: bold;
}

.fileName {
	background-color: transparent;
	border: 0px;
	height: 20px !important;
	font-size: 14px;
	color: rgb(216, 13, 21);
	outline: 0;
	cursor: pointer
}

.logout {
	color: #c10d0d;
	background-color: transparent;
	border: 0px;
	font-size: 18px;
	font-weight: bold;
	cursor: pointer;
}

#title {
	width: 250px;
}

.modalCenter {
	top: 50% !important;
	transform: translateY(-50%) !important;
}

.modal-backdrop {
    display:none;
}

.newStyle{
	font-size: 18px;
	margin-top: 20px;
}
.standardData{
    margin-top: -5px;
    width: 600px;
    margin-left: -3px;
}

.tab td{
border : 1px solid black;
}
.tab table{
border-collapse: collapse;
}
</style>
<script src="https://ajax.googleapi.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script src="<c:url value='/resources/js/ingenioJs/tcreview.js' />"></script>
<script src="<c:url value='/resources/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/login.css'/>">
<script type="text/javascript" src='<c:url value="/resources/js/Forgot.js"/>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/UtilityClass.js"/>'></script>

<script type="text/javascript">
var fullamount=0;
var paytypeResponse ;
var reportResponse;
var payId;
var feeId;
var discountArray ;
var payTypelength;
var schoolName;
var studentName;
var mobileNo;
var fee_setting;
var minimum_amount;
var payTypeSettingLength;
var installmentflagvalue;


 function getpayTypeList(){
	 
	 var amount = document.getElementById("amount").value ;
	 var schoolId = document.getElementById("schoolKey").innerHTML; 
	 if(fee_setting == "1" && installmentflagvalue != "1"){  
		 alert("Please Select Installment Type .")
	 } else if(amount=="" || amount=="0"){
		 alert("Please Enter the Amount");
	 }else if (fullamount < amount ){
		 alert("Please Enter Valid Amount");
	 }
	 else if (minimum_amount > amount  && fee_setting == 0 ){
	document.getElementById("minimumamountmsg").innerHTML="<b>You must pay at least "+minimum_amount+" for proceed.</b>";
	 }else{
	 
 
 	var data  = 'amount='+amount+'&schoolId='+schoolId; 
  

	 $.ajax({
        type: "GET",
        url : '/payTypeList',
        data:data,
        async : false,
        dataType: 'json',
        timeout: 600000,
        success : function(response) {
        	var data1 = response.responseList;
        	if(data1.length > 0){
        		var firstDiv = document.getElementById("transcationsDiv12");
                var loginDiv = document.getElementById("transcationsDiv1");
                firstDiv.style.display = "block";
                loginDiv.style.display = "none";
                $("#maintab").hide(); 
            	$("#attachmentImages2").empty();
        	payTypelength = data1.length; 
        	paytypeResponse = response.responseList;
			var paytypetable="<table style='text-align: center;  margin: 0 auto;''>"
			for(var i=0;i<data1.length;i++){
			paytypetable+='<tr><td><input type ="checkbox"  id="paytype'+i+'"  onclick="checkCkeckBox('+i+');"</td><td style="font-size:18px;">'+data1[i].payTypeName+'</td></tr>'
			}
			paytypetable+="</table>";
			$("#attachmentImages2").append(paytypetable);	
        	}else{
        		alert("Pay Type Setting not completed .");
        	}
        },
        error : function(xhr, status, error) {
        }
    });
}
	
} 

 
 function checkCkeckBox (paytypeId){
	 
	 for (var i = 0;i < payTypelength; i++)
	    {
	        document.getElementById("paytype" + i).checked = false;
	    }
	  document.getElementById("paytype" + paytypeId).checked = true; 
	    
	 payId = paytypeId;
      var conviencefee = paytypeResponse[paytypeId].convinienceFee;
   	document.getElementById("convinenece_fee").innerHTML = conviencefee ;
   	if(paytypeResponse[paytypeId].payTypeName == "Debit Card"){
	document.getElementById("paymentType").value = "card";
   	}else if(paytypeResponse[paytypeId].payTypeName == "Net Banking"){
   		document.getElementById("paymentType").value = "netbanking";
   	}else if(paytypeResponse[paytypeId].payTypeName == "Wallet"){
   		document.getElementById("paymentType").value = "wallet";
   	}else if(paytypeResponse[paytypeId].payTypeName == "EMI"){
   		document.getElementById("paymentType").value = "emi";
   	}else if(paytypeResponse[paytypeId].payTypeName == "UPI"){
   		document.getElementById("paymentType").value = "upi";
   	}else if(paytypeResponse[paytypeId].payTypeName == "Credit Card"){
	document.getElementById("paymentType").value = "card";
   	}
   	
 }
 

 
 function createOrderId() {
	
	 var currentTime = new Date()
	 var month = currentTime.getMonth() + 1
	 var day = currentTime.getDate()
	 var year = currentTime.getFullYear()
	 var paidDate = day + "-" + month + "-" + year
	 var schoolId = document.getElementById("schoolKey").innerHTML;
	 var paymentRequestBean = {
		      "payType" : paytypeResponse[payId].payTypeId,
		      "userID" :"1011700023",
		      "schoolId" :schoolId,
		      "paidDate" : paidDate,
		      "deviceType" : "2",
			  "ipAddress" :"1",
			  "totalPaidAmount" : document.getElementById("amount").value,
			  "totalDiscount" : "0",
			  "discountPercentage" :"0",
			  "receiptNo":"",
			  "receiptId":"0",
			  "transactionId" : "",			
			  "paymentStatus" :"",
			  "paymentMessage" : "",
			  "convenienceFee" : paytypeResponse[payId].convinienceFee,
			  "paymentGatewayRateId" : paytypeResponse[payId].paymentGatewayRateId,
			   "payTypeCharges" :  paytypeResponse[payId].payTypeCharges, 
			   "payTypeGst" :  paytypeResponse[payId].payTypeGst,
			 	"payTypeRouteCharges" : paytypeResponse[payId].payTypeRouteCharges,
				"payTypeRouteGst" : paytypeResponse[payId].payTypeRouteGst,
				"epCommission" : paytypeResponse[payId].epCommission,
				"schoolCommission" : paytypeResponse[payId].schoolCommission,
				"tpCommission" : paytypeResponse[payId].tpCommission,
				"epCommRouteCharges" : paytypeResponse[payId].epCommRouteCharges,
				"schCommRouteCharges" : paytypeResponse[payId].schCommRouteCharges,
				 "tpCommRouteCharges" : paytypeResponse[payId].tpCommRouteCharges,
				 "epGst" : paytypeResponse[payId].epGst,
				 "tpGst" : paytypeResponse[payId].tpGst,
				  "schGST" : paytypeResponse[payId].schGST,
				"epCommRouteGst" : paytypeResponse[payId].epCommRouteGst,
				"tpCommRouteGst" : paytypeResponse[payId].tpCommRouteGst,
				"schCommRouteGst" : paytypeResponse[payId].schCommRouteGst,
				 "studFeeId" : feeId,
				 "headFeeBeanList" : reportResponse[0].headFeeBeanList
		   };
	//alert(JSON.stringify(paymentRequestBean));
	 console.log(paymentRequestBean);
 	 $.ajax({
          type: "POST",
          url : '/saveTemp',
          data: JSON.stringify(paymentRequestBean),
          contentType : 'application/json; charset=utf-8',
          async : false,
          dataType: 'json',
          timeout: 600000,
          success : function(response) {
        	  console.log(response);
         var orderResponse = response.responseList;       
        	 var feeBeanList = '${sessionScope.feeBeanList}' ;
			document.getElementById("orderId").innerHTML = orderResponse[0].orderId;
			document.getElementById("orderIdrazourpay").value = orderResponse[0].orderId;
			document.getElementById("schoolName").value = document.getElementById("schoolName1").innerHTML;
			document.getElementById("studentName").value = document.getElementById("studentName1").innerHTML;
			document.getElementById("mobileNo").value = document.getElementById("mobileNo1").innerHTML;
			$("#paymentGatewaydiv").show();
          },
          error : function(xhr, status, error) {
          }
      });
 }
 
 
 
function getpaymentgateWayReport() {
	
	document.getElementById("minimumamountmsg").innerHTML="";
	var renewId = document.getElementById("renewId").innerHTML;	
	var yearId = document.getElementById("yearId").innerHTML;
	var schoolId = document.getElementById("schoolKey").innerHTML;
	var currentTime = new Date()
	 var month = currentTime.getMonth() + 1
	 var day = currentTime.getDate()
	 var year = currentTime.getFullYear()
	 var currentDate = day + "-" + month + "-" + year
	
 	 var data  = 'renewStudentId='+renewId+'&schoolId='+schoolId+'&yearId='+yearId+'&date='+currentDate ;  
	
/* var data  = 'renewStudentId=1011700018&schoolId=1011700001&yearId=1011700002&date=20-05-2020' ; */ 

	 $.ajax({
         type: "GET",
         url : '/paymentGatewayReport',
         data:data,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
			 var data1 = response.responseList;
			reportResponse = response.responseList;
			fullamount = data1[0].grandRemainingTotal;
			discountArray = data1[0].discountBean;
			var totalfee=data1[0].totalAssignedFee;
			var table1="<table  class='tab' style='text-align: center;  margin: 0 auto;  width:500px; border-collapse: collapse;'>"
				table1+="<tr bgcolor= '#D4D4BF'><td style='text-align:center !important;font-weight:bold;'>Summary</td>"
					table1+="<td style='text-align:center !important;font-weight:bold;'>Amount</td></tr>"
/* 					table1+="<td style='text-align:center !important;font-weight:bold;'>Paid </td>"
					table1+="<td style='text-align:center !important;font-weight:bold;'>Remaining </td> */
			table1+="<tr><td style='text-align: center !important;'>Actual Fee </td><td>"+data1[0].totalAssignedFee+"</td></tr>"	
			if(data1[0].assignedLateFee!='0'){
			table1+="<tr><td style='text-align: center !important;'> Late Fee Charges</td><td>"+data1[0].assignedLateFee+"</td></tr>"	
			}
			table1+="<tr><td style='text-align: center !important;'> Net fee Payble</td><td>"+data1[0].grandAssignedTotal+"</td></tr>"	
			table1+="</table><br>";
			var table="<table  class='tab' style='text-align: center;  margin: 0 auto;  width:500px; border-collapse: collapse;'>"
			table+="<tr bgcolor= '#D4D4BF'><td style='text-align:center !important;font-weight:bold;'>FeeType</td>"
				table+="<td style='text-align:center !important;font-weight:bold;'>Amount</td></tr>"
				/* table+="<td style='text-align:center !important;font-weight:bold;'>Paid </td>"
				table+="<td style='text-align:center !important;font-weight:bold;'>Remaining </td></tr>" */
				for(var i=0;i<data1[0].headFeeBeanList.length;i++){
		    var headListArray =data1[0].headFeeBeanList[i];	
			table+="<tr><td style='text-align:left !important;'>"+headListArray.headName+"</td><td></td></tr>"	
			var subheadSize = headListArray.subHeadList;
			for(var j=0;j<subheadSize.length;j++){
			var subHeadListArray = subheadSize[j];
			table+="<tr><td style='text-align:center !important;'>"+subHeadListArray.subHeadName+"</td><td>"+subHeadListArray.fee+"</td></tr>"	
			    }
			}
			table+="</table>";
			$("#attachmentImages1").append(table1);
			$("#attachmentImages1").append(table);	
			getpaymentSetting();
		/* 	$("#discountmessage").text(discountArray[0].discountLabel); */
         },
         error : function(xhr, status, error) {
     
         }
     });
	 
 	var requestdata  = 'renewStudentId='+renewId;  

	 /* 	 var requestdata  = 'renewStudentId=1011700018'	 */ 						;

	 
	 $.ajax({
         type: "GET",
         url : '/FeeId',
         data:requestdata,
         async : false,
         dataType: 'json',
         timeout: 600000,
         success : function(response) {
        	 
        	 feeId = response.responseList[0].feeId;
        	 
         },
         error : function(xhr, status, error) {
         }
     });
}


/* function fullPayments(){
	var finalAmount ;
	if(discountArray[0].percentFlag == 1 ) {
		finalAmount = (discountArray[0].discountPercent/100) * fullamount ;
	}else{
		finalAmount = fullamount - discountArray[0].minimumAmount ;
	}
	document.getElementById("amount").value = finalAmount ;
}

function paymentsByParts(){
	document.getElementById("amount").value = "" ;
} */

function getTranscations(){
	$("#historyDiv").hide();
	$("#transcationsDiv1").show();
	
}

function gonewTranscations(){
	$("#transcationsDiv12").hide();
	$("#paymentGatewaydiv").hide();	
	$("#transcationsDiv1").show();
	  $("#maintab").show(); 
}

function getShowHistory(){

	var data  = 'studentFeeId='+feeId+'&language=en';
	 $.ajax({
        type: "GET",
        url : '/transactions',
        data:data,
        async : false,
        dataType: 'json',
        timeout: 600000,
        success : function(response) {
        	$("#attachmentImages3").empty();
             console.log(response);
			var data1 = response.responseList;
			if(data1.length > 0){
				$("#transcationsDiv1").hide();
	        	$("#historyDiv").show(); 
				var table="<table class='tab' style='text-align: center;  margin: 0 auto; width:550px; border-collapse: collapse;'>"	
				table+="<tr><td style='text-align:center !important;font-weight:bold;'>Date</td>"
/* 				table+="<td style='text-align:center !important;font-weight:bold;'>time</td>"
 */				table+="<td style='text-align:center !important;font-weight:bold;'>Paid Amount</td>"
				table+="<td style='text-align:center !important;font-weight:bold;'>PaymentStatus</td>"
				table+="<td style='text-align:center !important;font-weight:bold;'>Transcation Id</td>"
				table+="<td style='text-align:center !important;font-weight:bold;'>ReceiptNo</td></tr>"
			for(var i =0 ;i<data1.length ;i++){				
						table+="<tr><td style='text-align:center !important;'>"+data1[i].paidDate+"</td>"	
/* 						table+="<td style='text-align:center !important;'>"+data1[i].time+"</td>" */	
						table+="<td style='text-align:center !important;'>"+data1[i].totalPaidAmount+"</td>"
						table+="<td style='text-align:center !important;'>"+data1[i].paymentStatus+"</td>"
						table+="<td style='text-align:center !important;'>"+data1[i].transactionId+"</td>"
						table+="<td style='text-align:center !important;'>"+data1[i].receiptNo+"</td></tr>"

				
			}
				table+="</table>";
				$("#attachmentImages3").append(table);	
			}
       	 else{
				alert("No Transcations Found");
			}
			
        },
        error : function(xhr, status, error) {
        }
    });
}

function getpaymentflag(){
	if(document.getElementById("paymentflag").innerHTML=="1"){
		alert("Payment Done Successfully.")
	}else if(document.getElementById("paymentflag").innerHTML=="2"){
		alert("Payment Failed.")
}
}

function goback(){
	if(document.getElementById("paymentflag").innerHTML=="0"){
		window.location.href = "StudentAdmissionForm.jsp";	
	}else{
		window.history.go(-4);
	}
 }

function getpaymentSetting(){
	
	 var yearId = document.getElementById("yearId").innerHTML;
	var schoolId = document.getElementById("schoolKey").innerHTML;
	var standardId = document.getElementById("standardId").innerHTML;

 
	var data  = 'schoolId='+schoolId+'&yearId='+yearId+'&standardId='+standardId;
	
 $.ajax({
        type: "GET",
        url : '/paymentSetting',
        data:data,
        async : false,
        dataType: 'json',
        timeout: 600000,
        success : function(response) {
        	if(response.statusCode == "200"){
        		installmentflagvalue = "0";
        	var data1 = response.responseList;
        	payTypeSettingLength =data1;
        	minimum_amount = data1[0].minimum_fee_payment;
        	fee_setting = data1[0].installment_wise_fee;    
        	var finalinstallment =  Math.round(fullamount/data1[0].amountOrFormula);         	
        	 if(fee_setting == "0"){        			
        			$("#installmentdiv").hide();
        		}else{        				
        			$("#installmentdiv").show(); 
        			var paytypetable="<table style='text-align: center;  margin: 0 auto;''>"
        				for(var i=0;i<data1.length;i++){
        				paytypetable+='<tr><td><input type ="radio"  id="radio'+i+'"  name ="radios"  onclick="setFee('+i+');"</td><td style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;'+data1[i].installmentTypeDescription+'</td></tr>'
        				}
        				paytypetable+="</table>";
        				$("#installmentdiv").append(paytypetable);
        			/* document.getElementById("rd1").checked = true;
        			document.getElementById("amount").value = finalinstallment;
        			document.getElementById("amount").setAttribute("readonly", "readonly"); */
        		} 
        	}else{
        		$("#installmentdiv").hide();
        	}
        },
        error : function(xhr, status, error) {
        }
    }); 
	
}

function setFee (value) {
	installmentflagvalue = "1";
	 for (var i = 0;i <= payTypeSettingLength.length; i++)
	    {
	        if(value==payTypeSettingLength[i].installmentTypeFlag){	        	
	        	var finalinstallment =  Math.round(fullamount/payTypeSettingLength[i].amountOrFormula);  
	        	document.getElementById("amount").value = finalinstallment;
    			document.getElementById("amount").setAttribute("readonly", "readonly");
	        }
	    }
}

</script>

</head>


 <body onload="getpaymentflag();getpaymentgateWayReport();"> 
 
  <% 
 	 String paymentflag ="0";
 	String schoolKey= (String) session.getAttribute("schoolkeys"); 
 	String studentID= (String) session.getAttribute("renewId");
	String standardId= (String) session.getAttribute("standardId");
 	String newYearId= (String) session.getAttribute("newYearId"); 
 	paymentflag= (String) session.getAttribute("paymentflag");
 	String schoolName= (String) session.getAttribute("schoolName"); 
 	String studentName= (String) session.getAttribute("studentName"); 
 	String mobileNo= (String) session.getAttribute("mobileNo"); 
 	
 %> 	 

 <div class="login-form" id="wrapper" style="margin-left:60	0px; height: 550px;">
 <span id="schoolKey"  style="display: none;"><%=schoolKey%></span>
 <span id="renewId"  style="display: none;"><%=studentID%></span>
 <span id="yearId"  style="display: none;"><%=newYearId%></span> 
 <span id="paymentflag"  style="display: none;"><%=paymentflag%></span> 
 <span id="schoolName1"  style="display: none;"><%=schoolName%></span>
 <span id="studentName1"  style="display: none;"><%=studentName%></span>
  <span id="mobileNo1"  style="display: none;"><%=mobileNo%></span>
  <span id="standardId"  style="display: none;"><%=standardId%></span>
 
 <div id="maintab" style="width: 600px;background-color: #f3f3f3;margin-top: 20px;">

		<table style="text-align: center;  margin: 0 auto;">
		    <tr>
		    <td>
			          <input type="button" style="cursor: pointer; background-image:/resources/img/backArrow.png " class="submit" id="back1" onclick="goback();" value="Back" />
				</td>
				
				<td>
			          <input type="button" style="cursor: pointer;" class="submit" id="history1" onclick="getTranscations();" value="New Transcations" />
				</td>				
				<td>
			          <input type="button" style="cursor: pointer;" class="submit" id="history" onclick="getShowHistory();" value="History" />
				</td>
		    </tr>
		    
			</table>
			
			<hr style="height:2px;border-width:2;color:#3093c7;">
 </div>

 <div id="transcationsDiv1" style="width: 600px;background-color: #f3f3f3;margin-top: -1px;">
 
 <div id="fullpaymentDiv" style="width: 600px;background-color: #f3f3f3;margin-top: -1px;">
   	<div id ="attachmentImages1" style="width: 600px;background-color: #f3f3f3;margin-top: -1px;"></div>
   	<div id ="installmentdiv" style="width: 600px;background-color: #f3f3f3;margin-top: 20px;">
   <!-- 	<table style="text-align: center;  margin: 0 auto;">
   	<tr>
   	<td>
   	<input type="radio" id="rd1" name="myRadios"  value="0" onclick="setFee(value)"/>&nbsp;Full Payment &nbsp;&nbsp;&nbsp;
  	<input type="radio" id="rd2" name="myRadios"  value="1" onclick="setFee(value)"/>&nbsp;Half Payment &nbsp;&nbsp;&nbsp; 
	<input type="radio" id="rd3" name="myRadios"  value="2" onclick="setFee(value)"/>&nbsp;Quarterly Payment &nbsp;&nbsp;&nbsp;
	</td></tr></table> -->
	</div>
  	<div id="generateOtp" style="width: 600px;background-color: #f3f3f3;margin-top: -1px;">
  	 <p style="text-align:center"></p> 
		<table style="text-align: center;  margin: 0 auto;">
		   <!--  <tr>
				<td>
			     	<input type="button" style="cursor: pointer;" class="submit" id="fullpayment" onclick="fullPayments();" value="Full Payment" />
				</td>
				<td>
			    	<input type="button" style="cursor: pointer;" class="submit" id="paymentinparts" onclick="paymentsByParts();" value="Payment In parts" />
				</td>
		    </tr> -->
		    <tr>
				<td style="margin-top: 20px;"></b><span id="discountmessage"></span></td>	
			</tr>
		    <tr>
				<td colspan="2">
					<input type="text" tabindex="5" autocomplete="dummy-no-auto-complete-fix" class="inputtext"  id="amount"  pattern="\d{10}" placeholder="Enter Amount" style="width: 250px !important ;margin-top: 20px;" />
				
				</td>
			</tr>
			<tr>
				<td colspan="2">
			<input type="button" style="cursor: pointer;margin-top: 20px;" class="submit" id="fullpayment" onclick="getpayTypeList();" value="Next" />
				</td>
			</tr>
			<tr></tr>
		<tr></tr>
			<tr>
			<td colspan="2" align="center">
			<div id ="installmentdiv" style="width: 600px;background-color: #f3f3f3;margin-top: 40px;">
			<span id="minimumamountmsg" style="color:red;"></span></div></td></tr>
		</table>
 	</div>
  </div> 
  
  <!-- <div id="installmentdiv" style="width: 600px;background-color: #f3f3f3;margin-top: -1px;">
   	<div id ="attachmentImages1" style="width: 600px;background-color: #f3f3f3;margin-top: -1px;"></div>
  	<div id="generateOtp" style="width: 600px;background-color: #f3f3f3;margin-top: -1px;">
  	
  	 <p style="text-align:center"></p> 
		<table style="text-align: center;  margin: 0 auto;">
		    <tr>
				<td>
			     	<input type="button" style="cursor: pointer;" class="submit" id="fullpayment" onclick="fullPayments();" value="Full Payment" />
				</td>
				<td>
			    	<input type="button" style="cursor: pointer;" class="submit" id="paymentinparts" onclick="paymentsByParts();" value="Payment In parts" />
				</td>
		    </tr>
		    <tr>
				<td style="margin-top: 20px;"></b><span id="discountmessage"></span></td>	
			</tr>
		  
		    <tr>
				<td colspan="2">
					<input type="text" tabindex="5" autocomplete="dummy-no-auto-complete-fix" class="inputtext"  id="amount"  pattern="\d{10}" placeholder="Enter Amount" style="width: 250px !important ;margin-top: 20px;" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
			<input type="button" style="cursor: pointer;margin-top: 20px;" class="submit" id="fullpayment" onclick="getpayTypeList();" value="Next" />
				</td>
			</tr>
		</table>
 	</div>
  </div>
  
   -->
  </div>
  
  
  <div id="historyDiv" style="width: 600px;background-color: #f3f3f3;margin-top: 20px;">
   		<div id ="attachmentImages3" style="width: 600px;background-color: #f3f3f3;margin-top: 20px;"></div>
   		<p id="notranscation" style="display: none;">No Transcations Found</p>
  </div>
  
  
  	<div id="transcationsDiv12" style="width: 600px;background-color: #f3f3f3;margin-top: 20px; display: none;">
  	 	 <table style="text-align: center;  margin: 0 auto;">
			<tr>
				<td>
			          <input type="button" style="cursor: pointer; background-image:/resources/img/backArrow.png " class="submit" id="back1" onclick="gonewTranscations();" value="Back" />
				</td>
  	 	 		 <td>
			          <input type="button" style="cursor: pointer; background-image:/resources/img/backArrow.png " class="submit" id="back1" onclick="goback();" value="Cancel Transcations" />
				</td>
  	 	    </tr>
  	 	   </table>
  	 	   <hr style="height:2px;border-width:2;color:#3093c7;">
  	 	 <p style="text-align:center; font-weight: bold;">Select Payment Mode</p>
   <div id ="attachmentImages2" style="width: 600px;background-color: #f3f3f3;margin-top: 20px;"></div>
  	<div id="new1" style="width: 600px;background-color: #f3f3f3;margin-top: 20px;">	
	 <table style="text-align: center;  margin: 0 auto;">
			<tr>
				 <td style="vertical-align: top;">Convience Fee :&nbsp;</b><span id="convinenece_fee"></span></td>	
			</tr>
			<tr>
				<td>
			<input type="button" style="cursor: pointer;margin-top: 20px;" class="submit" id="fullpayment" onclick="createOrderId();" value="Confirm" />
				</td>
			</tr>
			<tr>
				<td>
			<input type="hidden"  style="cursor: pointer;margin-top: 20px;" id="orderId"  />
				</td>
 			</tr>
			</table>
			</div>
	
 <div id="paymentGatewaydiv" style="width: 600px;background-color: #f3f3f3;margin-top: 20px; display: none;">
  <table style="text-align: center;  margin: 0 auto;">
  <tr>
  <td>
 <p style="text-align:center; font-weight: bold;">Click 'Pay Now' button for payment.</p>
<form method="POST" action="https://api.razorpay.com/v1/checkout/embedded" >  
<input type="hidden" name="key_id" value=rzp_live_X3gxf36eqngjHt> 
<!-- <input type="hidden" name="key_id" value=rzp_test_izEiUuxaTv6KAj> -->
  <input type="hidden" name="order_id" id="orderIdrazourpay" value="">   
 <input type="hidden" name="name" id="schoolName"  value="">  
  <input type="hidden" name="method" id="paymentType" value=""> 
 <input type="hidden" name="description"  value="">  
 <input type="hidden" name="image" value="https://cdn.razorpay.com/logos/BUVwvgaqVByGp2_large.png"> 
 <input type="hidden" name="prefill[name]" id="studentName" value=""> 
 <input type="hidden" name="prefill[contact]"  id="mobileNo" value="">  
 <input type="hidden" name="prefill[email]" value="gaurav.kumar@example.com"> 
 <input type="hidden" name="notes[shipping address]" value="L-16, The Business Centre, 61 Wellfield Road, New Delhi - 110001">  
 <input type="hidden" name="callback_url" value="http://13.233.210.164:8081/payment">
 <input type="hidden" name="cancel_url" value="http://13.233.210.164:8081/StudentAdmissionForm.jsp">  
 <button class="submit"  style="text-align: center;  margin: 0 auto;">Pay Now</button></form>
 </td>
 </tr>
 </table>
</div>					
						

  	</div>
 												
 </div>

	
				 

</body>
</html>