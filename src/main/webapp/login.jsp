<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">

<title>User Login</title>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<style>
@media only screen and (max-device-width: 600px) {
	.regForm {
		margin-left: 150px !important;
		width: 565px !important;
	}
	.regForm-tab {
		width: 479px !important;
	}
	.vid {
		margin-left: 150px !important;
	}
	.login-form {
		margin-left: 150px !important;
	}
}

.regForm table .input {
	width: 188px;
	padding: 15px 25px;
	font-family: "HelveticaNeue-Light", "Helvetica Neue Light",
		"Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
	font-weight: 400;
	font-size: 14px;
	color: #9d9e9e;
	text-shadow: 1px 1px 0 rgba(255, 255, 255, 1);
	background: #fff;
	border: 1px solid #fff;
	border-radius: 5px;
}

.regForm {
	padding: 20px;
	width: 608px;
	display: block;
	/* margin-left: 550px; */
	/* height: 536px; */
	background-color: #d4dedf;
	margin-top: 50px;
	margin-left: 550px;
}

.input-select {
	width: 239px !important;
	background-color: #f7f7f7;
}

.vid {
	margin-left: 450px;
	margin-top: 190px;
	margin-bottom: 20px;
}
</style>
<script
	src="https://ajax.googleapi.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script src="<c:url value='/resources/js/ingenioJs/tcreview.js' />"></script>
<script src="<c:url value='/resources/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/login.css'/>">
<script type="text/javascript"
	src='<c:url value="/resources/js/Forgot.js"/>'></script>
<script type="text/javascript"
	src='<c:url value="/resources/js/UtilityClass.js"/>'></script>

<script type="text/javascript">
	/* document.onkeydown = function (t) {
	 if(t.which == 9){
	 return false;
	 }
	 } */

	var smsBalance;
	var registrationFlag;
	function getData() {
		var schoolId = document.getElementById("schoolKey").innerHTML;
		if (schoolId == null || schoolId == '' || schoolId == undefined
				|| schoolId == 'null') {
			alert("Invalid URL! Please Try Again With Proper URL")
			return false;
		}
		var mobileNo = $("#mobileNum2").val();
		if (mobileNo.length != 10) {
			alert("Please Enter Valid Mobile No");
			document.getElementById("mobileNum2").focus;
			return false;
		} else {
			var data = 'mobileNum=' + $("#mobileNum2").val().toString()
					+ '&schoolId=' + schoolId;
			$.ajax({
				type : "POST",
				url : "/generateOtpForForgotPassword",
				data : data,
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(response) {
					$("#table1").hide();
					$("#table2").show();
					$("#submit").show();
					document.getElementById('otpnum2').focus();
				},
				error : function(xhr, status, error) {
					alert("error")
					alert(xhr.responseText);
				}
			});
		}
	}

	function validateOtp2() {
		var mobileNo = $("#mobileNum2").val();
		if (mobileNo.length != 10) {
			alert("Please Enter Valid 10 Digit Mobile No");
			document.getElementById("mobileNum2").focus;
			return false;
		}
		var otp = $("#otpnum2").val();
		if (otp.length != 6) {
			alert("Please Enter Valid 6 Digit OTP");
			document.getElementById("otpnum2").focus;
			return false;
		}
		var schoolId = document.getElementById("schoolKey").innerHTML;
		var data = 'otpnum=' + $("#otpnum2").val() + '&mobileNum='
				+ $("#mobileNum2").val() + '&schoolId=' + schoolId;
		$.ajax({
			type : "GET",
			url : "/validateOtp",
			data : data,
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(response) {
				if (response.statusCode == "200") {
					$("#table3").show();
					$("#submit1").show();
					$("#table2").hide();
					$("#submit").hide();
					document.getElementById('passwordBox1').focus();
				} else if (response.statusCode == "404") {
					alert("Invalid OTP! Please Try Again");
				} else if (response.statusCode == "500") {
					alert("Invalid URL! Please Try Again With Proper URL")
				}
			},
			error : function(xhr, status, error) {
				alert("Invalid URL! Please Try Again With Proper URL")
			}
		});
	}

	function updatePassword() {
		var pass = document.getElementById("passwordBox1").value;
		var ReEnterPass = document.getElementById("passwordBox3").value;
		if (document.getElementById("passwordBox1").value.length == 0) {
			alert("Password Should Not Be Empty.")
			document.getElementById("passwordBox1").focus;
			return false;
		}
		if (pass != ReEnterPass) {
			alert("Passwords Do Not Match.")
			document.getElementById("passwordBox3").focus;
			return false;
		}
		var schoolId = document.getElementById("schoolKey").innerHTML;
		var data = 'password=' + $("#passwordBox1").val() + '&userName='
				+ $("#mobileNum2").val() + '&schoolId=' + schoolId;
		$.ajax({
			type : "GET",
			url : "/forgotPassword",
			data : data,
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(response) {
				if (response.statusCode == "200") {
					alert("Password Updated Successfully.")
					$(".close").trigger("click");
					location.reload();
				} else if (response.statusCode == "404") {
					alert("Please Try Again");
				} else if (response.statusCode == "500") {
					alert("Invalid URL! Please Try Again With Proper URL")
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert("error :" + errorThrown);
			}
		});
	}

	var isprintall;
	function precheckauthentication(isprintall1) {
		$('input:text[name="userName"]').val("");
		$('input:password[name="password"]').val("");
		$('#h4id').text = "strings['PleaseFillDetail']";
		$("#getusernamepasswordPopId").trigger('click');
		if (confirm(strings['forgot.are you sure'])) {
			$('#usernametextId').focus();
		} else {
			$('#cancelauthenticationId').trigger('click');
		}
		isprintall = isprintall1;
	}

	function hideAndSeek() {
		$("#table2").hide();
		$("#submit").hide();
		$("#table3").hide();
		$("#submit1").hide();
	}

	function login() {
		var schoolId = document.getElementById("schoolKey").innerHTML;
		var data = 'userName=' + $("#userName").val() + '&password='
				+ $("#password").val() + '&schoolId=' + schoolId;
		$
				.ajax({
					type : "GET",
					url : "/loginLink",
					data : data,
					dataType : 'json',
					cache : false,
					timeout : 600000,
					success : function(response) {
						if (response.statusCode == '200') {
							//window.location.href = "welcome.jsp?schoolkey="+schoolId;
							window.location.href = "StudentAdmissionForm.jsp";
						} else if (response.statusCode == '404') {
							alert("Invalid UserName and Password. Please Try Again");
						} else if (response.statusCode == '101') {
							alert("Registration By This UserName Is Not Exist. Please Create New Registration.");
						} else {
							alert("Invalid URL. Please Try Again");
						}
					},
					error : function(xhr, status, error) {
						alert("Invalid URL. Please Try Again");
					}
				});
	}

	function b() {
		var schoolId = document.getElementById("schoolKey").innerHTML;
		window.location.href = "login?schoolkey=" + schoolId;
	}

	function getLetterHead() {

		var schoolId = document.getElementById('schoolKey').innerHTML;
		var data = 'schoolId=' + schoolId;
		$
				.ajax({
					type : "GET",
					url : '/registrationSetting',
					data : data,
					async : false,
					dataType : 'json',
					timeout : 600000,
					success : function(response) {
						var data1 = response.responseData;
						registrationFlag = data1.registrationFlag;
						if (registrationFlag == '1') {
							document.getElementById("registrationbutton").style.visibility = "visible";
							document.getElementById("newregistration").style.visibility = "hidden";
						} else {
							document.getElementById("registrationbutton").style.visibility = "hidden";
							document.getElementById("newregistration").style.visibility = "visible";
						}
					},
					error : function(xhr, status, error) {
					}
				});

		var firstDiv = document.getElementById("firstDiv");
		firstDiv.style.display = "block";
		var schoolId = document.getElementById('schoolKey').innerHTML;
		var data = 'schoolId=' + schoolId;
		$
				.ajax({
					type : "GET",
					url : '/getLetterHead',
					data : data,
					async : false,
					dataType : 'json',
					timeout : 600000,
					success : function(response) {
						var data = response.responseList;
						for (var i = 0; i < data.length; i++) {
							if (data[i].flag == 1) {
								$("#letterHead").empty();
								$("#letterHead")
										.append(
												'<img style="border: 0px solid red;width:100%;height:200px;" src="data:image/png;base64,'
														+ data[i].letterHeadImg
														+ '"  />');
							} else {
								alert("Invalid URL! Please Try Again With Proper URL")

							}
						}
					},
					error : function(xhr, status, error) {
					}
				});
	}

	function getSmsBalance() {
		var schoolId = document.getElementById('schoolKey').innerHTML;
		var data = 'schoolId=' + schoolId;
		$.ajax({
			type : "GET",
			url : '/getSmsBalance',
			data : data,
			async : false,
			dataType : 'json',
			timeout : 600000,
			success : function(response) {
				var data = response.responseList;
				for (var i = 0; i < data.length; i++) {
					smsBalance = data[i].balance

				}
			},
			error : function(xhr, status, error) {
			}
		});

	}

	function validateOtp() {
		var mobileNo = $("#mobileNum").val()
		if (mobileNo.length != 10) {
			alert("Please Enter Valid 10 Digit Mobile No");
			document.getElementById("mobileNum").focus;
			return false;
		}
		var otp = $("#otpnum").val();
		if (otp.length != 6) {
			alert("Please Enter Valid 6 Digit OTP");
			document.getElementById("otpnum").focus;
			return false;
		}
		var schoolId = document.getElementById("schoolKey").innerHTML;
		var data = 'otpnum=' + $("#otpnum").val() + '&mobileNum='
				+ $("#mobileNum").val() + '&schoolId=' + schoolId;
		$.ajax({
			type : "GET",
			url : "/validateOtp",
			data : data,
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			dataType : 'json',
			cache : false,
			timeout : 600000,
			success : function(response) {
				if (response.statusCode == "200") {
					$("#newRegUserName").val(mobileNo);
					// document.getElementById('newRegistrationPopUp').click();
					// window.location.href = "StudentAdmissionForm.jsp";

					$('.regForm').css('display', 'block');
					$('.login-form').css('display', 'none');
					$('.vid').css('display', 'none');
					getStandardAndYearCombo();
				} else if (response.statusCode == "404") {
					alert("Invalid OTP! Please Try Again");
				} else if (response.statusCode == "500") {
					alert("Invalid URL! Please Try Again With Proper URL")
				}
			},
			error : function(xhr, status, error) {
				alert("Invalid URL! Please Try Again With Proper URL")
			}
		});
	}

	function generateOTP() {
		/*  if(smsBalance == 0){
		 	alert("Insufficient Sms Balance");
		 }else{ */
		var schoolId = document.getElementById("schoolKey").innerHTML;
		if (schoolId == null || schoolId == '' || schoolId == undefined
				|| schoolId == 'null') {
			alert("Invalid URL! Please Try Again With Proper URL")
			return false;
		}
		var mobileNo = $("#mobileNum").val()
		if (mobileNo.length != 10) {
			alert("Please Enter Valid Mobile No");
			document.getElementById("mobileNum").focus;
			document.getElementById("close").click();
			return false;
		} else {
			var data = 'mobileNum=' + $("#mobileNum").val() + '&schoolId='
					+ schoolId;
			$.ajax({
				type : "POST",
				url : "/generateOtpForLogIn",
				data : data,
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(response) {
					if (response.statusCode == "1000") {
						var mo = $("#mobileNum").val();
						$("#userName").val(mo);
						$("#mobileNum").val("");
						/* 	            alert("User Already Exist. Please Login And Continue"); */
						var firstDiv = document.getElementById("firstDiv");
						var loginDiv = document.getElementById("loginDiv");
						loginDiv.style.display = "block";
						firstDiv.style.display = "none";

						// window.location.href = "login.jsp?schoolkey="+schoolId;
					} else if (response.statusCode == "200") {
						var mo = $("#mobileNum").val();
						/*        	  alert("OTP Has Been Sent To Your Device."); */
						var firstDiv = document.getElementById("firstDiv");
						var generateOtpDiv = document
								.getElementById("generateOtpDiv");
						generateOtpDiv.style.display = "block";
						firstDiv.style.display = "none";
						$("#mobileNum").val(mo);
					} else {
						alert("Error While Sending OTP Please Try Again")
					}
				},
				error : function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		}
		/* 	} */
	}

	function validate(evt) {
		/* if($("#mobileNum").val()<10){
			alert("Please Enter Mobile Number");
			$("#otpnum").val("");
			return false;
		} */
		var theEvent = evt || window.event;
		// Handle paste
		if (theEvent.type === 'paste') {
			key = event.clipboardData.getData('text/plain');
		} else {
			// Handle key press
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode(key);
		}
		var regex = /[0-9]|\./;
		if (!regex.test(key)) {
			theEvent.returnValue = false;
			if (theEvent.preventDefault)
				theEvent.preventDefault();
		}
	}

	function getStandardAndYearCombo() {
		var initialName = [ 'Mr.', 'Miss.', 'Ku.', 'Kumar', 'Kumari', 'Master',
				'Mrs' ];
		for (var i = 0; i < initialName.length; i++) {
			var option = $('<option/>');
			option.attr('value', initialName[i]).text(initialName[i]);
			$("#missMr").append(option);
		}
		var data = 'schoolId=' + document.getElementById("schoolKey").innerHTML;
		var globalAccademicYearId;
		$.ajax({
			type : "GET",
			url : 'getAcademicYear',
			data : data,
			async : false,
			dataType : 'json',
			timeout : 600000,
			success : function(response) {
				globalAccademicYearId = response.responseData;
			},
			error : function(xhr, status, error) {
			}
		});
		$.ajax({
			type : "GET",
			url : 'getAcademicYearCombo',
			data : data,
			async : false,
			dataType : 'json',
			timeout : 600000,
			success : function(response) {
				var data = response.responseList;
				$(data).each(function() {
					//if(this.yearId==globalAccademicYearId){
			/* 		if (this.yearName == "2020-2021") { */
						var option = $('<option/>');
						option.attr('value', this.yearId).text(this.yearName);
						$(".AcademicYear").append(option);
				/* 	} */
					var option1 = $('<option/>');
					option1.attr('value', this.yearId).text(this.yearName);
					$(".PreviousYear").append(option);

				});
			},
			error : function(xhr, status, error) {
			}
		});

		$.ajax({
			type : "GET",
			url : '/getStandardCombo',
			data : data,
			async : false,
			dataType : 'json',
			timeout : 600000,
			success : function(response) {
				var data = response.responseList;
				standardList = response.responseList;
				var i = 0;
				$(data).each(
						function() {
							var option = $('<option/>');
							option.attr('value', this.standardId).text(
									this.standardName);
							$("#Standard").append(option);

						});
			},
			error : function(xhr, status, error) {
			}
		});
	}

	function newPayment() {
		
		var yearId = document.getElementById("globalAcademicYear").value;
		var standardId = document.getElementById("Standard").value;
		var schoolId = document.getElementById("schoolKey").innerHTML;

		var data2 = 'schoolId='
			+ document.getElementById("schoolKey").innerHTML;
		
	var formNo;
	$.ajax({
		type : "GET",
		url : '/getFormNumber',
		data : data2,
		async : false,
		dataType : 'json',
		timeout : 600000,
		success : function(response) {
			formNo = response.responseData;
		},
		error : function(xhr, status, error) {
		}
	});

	var yearId = document.getElementById("globalAcademicYear").value;
	var standardId = document.getElementById("Standard").value;
	var passwordNewReg = $("#passwordNewReg").val();
	var confirmpasswordNewReg = $("#confirmpasswordNewReg").val();
	var newRegName = $("#newRegName").val();
	var missMr = $("#missMr").val();
	var secondName = $("#secondName").val();
	secondName = convertEachWordInUppercase(secondName);
	var lastName = $("#lastName").val();
	lastName = convertEachWordInUppercase(lastName);
	if (missMr == 0) {
		alert("Enter Initial Name");
		document.getElementById("missMr").focus;
		return false;
	}
	if (newRegName == null || newRegName == '') {
		alert("Enter First Name");
		document.getElementById("newRegName").focus;
		return false;
	}
	newRegName = convertEachWordInUppercase(newRegName);
	/* if(secondName==null || secondName==''){
		alert("Enter Second Name");
		document.getElementById("secondName").focus;
		return false;
	}
	if(lastName==null || lastName==''){
		alert("Enter Last Name");
		document.getElementById("lastName").focus;
		return false;
	} */
	if (passwordNewReg == null || passwordNewReg == '') {
		alert("Enter Password");
		document.getElementById("passwordNewReg").focus;
		return false;
	}
	if (confirmpasswordNewReg == null || confirmpasswordNewReg == '') {
		alert("Enter Confirm Password");
		document.getElementById("confirmpasswordNewReg").focus;
		return false;
	}
	if (yearId == '') {
		alert("Please Select Year");
		return false;
	}
	if (standardId == '') {
		alert("Please Select Standard");
		return false;
	}
	feeSettingAvailable = document.getElementById("feeSettingAvailable").value;
	if (passwordNewReg == confirmpasswordNewReg) {
		$('#spinner').css('display', 'block');
		$('.submitBtn').prop('disabled', true);
		var data = 'newRegName=' + newRegName + '&yearId=' + yearId
				+ '&standardId=' + standardId + '&passwordNewReg='
				+ passwordNewReg + '&newRegUserName='
				+ $("#newRegUserName").val() + '&schoolId='
				+ document.getElementById("schoolKey").innerHTML
				+ '&formNo=' + formNo + '&missMr=' + missMr
				+ '&secondName=' + secondName + '&lastName=' + lastName
				+ '&feeSettingAvailable=' + feeSettingAvailable;		
	
	 $.ajax({
	     type: "GET",
	     url : '/registrationPaymentpage',
	     data:data,
	     async : false,
	     dataType: 'json',
	     timeout: 600000,
	     success : function(response) {   	 
	    	 window.location.href = "payment2.jsp?yearId=" + yearId + "&standardId="
				+ standardId + "&schoolId=" + schoolId +"&mobileNo=" + $("#newRegUserName").val() ;   	
	     },
	     error : function(xhr, status, error) {
	    	 alert(error)    
	    	 }
	     
	 });
		
}
	}

	function validatePassword() {

		var data2 = 'schoolId='
				+ document.getElementById("schoolKey").innerHTML;
		var formNo;
		$.ajax({
			type : "GET",
			url : '/getFormNumber',
			data : data2,
			async : false,
			dataType : 'json',
			timeout : 600000,
			success : function(response) {
				formNo = response.responseData;
			},
			error : function(xhr, status, error) {
			}
		});

		var yearId = document.getElementById("globalAcademicYear").value;
		var standardId = document.getElementById("Standard").value;
		var passwordNewReg = $("#passwordNewReg").val();
		var confirmpasswordNewReg = $("#confirmpasswordNewReg").val();
		var newRegName = $("#newRegName").val();
		var missMr = $("#missMr").val();
		var secondName = $("#secondName").val();
		secondName = convertEachWordInUppercase(secondName);
		var lastName = $("#lastName").val();
		lastName = convertEachWordInUppercase(lastName);
		if (missMr == 0) {
			alert("Enter Initial Name");
			document.getElementById("missMr").focus;
			return false;
		}
		if (newRegName == null || newRegName == '') {
			alert("Enter First Name");
			document.getElementById("newRegName").focus;
			return false;
		}
		newRegName = convertEachWordInUppercase(newRegName);
		/* if(secondName==null || secondName==''){
			alert("Enter Second Name");
			document.getElementById("secondName").focus;
			return false;
		}
		if(lastName==null || lastName==''){
			alert("Enter Last Name");
			document.getElementById("lastName").focus;
			return false;
		} */
		if (passwordNewReg == null || passwordNewReg == '') {
			alert("Enter Password");
			document.getElementById("passwordNewReg").focus;
			return false;
		}
		if (confirmpasswordNewReg == null || confirmpasswordNewReg == '') {
			alert("Enter Confirm Password");
			document.getElementById("confirmpasswordNewReg").focus;
			return false;
		}
		if (yearId == '') {
			alert("Please Select Year");
			return false;
		}
		if (standardId == '') {
			alert("Please Select Standard");
			return false;
		}
		feeSettingAvailable = document.getElementById("feeSettingAvailable").value;
		if (passwordNewReg == confirmpasswordNewReg) {
			$('#spinner').css('display', 'block');
			$('.submitBtn').prop('disabled', true);
			var data = 'newRegName=' + newRegName + '&yearId=' + yearId
					+ '&standardId=' + standardId + '&passwordNewReg='
					+ passwordNewReg + '&newRegUserName='
					+ $("#newRegUserName").val() + '&schoolId='
					+ document.getElementById("schoolKey").innerHTML
					+ '&formNo=' + formNo + '&missMr=' + missMr
					+ '&secondName=' + secondName + '&lastName=' + lastName
					+ '&feeSettingAvailable=' + feeSettingAvailable;
			$.ajax({
				type : "POST",
				url : "/saveNewRegistration",
				data : data,
				dataType : 'json',
				cache : false,
				timeout : 600000,
				success : function(response) {
					if (response.statusCode == "200") {
						// alert(response.responseData);
						alert("Registration Has Been Successfully Completed");
						window.location.href = "StudentAdmissionForm.jsp";
						/* 	            	  window.location.href = "login.jsp?schoolkey="+document.getElementById("schoolKey").innerHTML;
						 */} else {
						alert("Error While Sending OTP Please Try Again")
					}
					$('#spinner').css('display', 'none');
					$('.submitBtn').prop('disabled', false);
				},
				error : function(xhr, status, error) {
					alert(xhr.responseText);
				}
			});
		} else {
			alert('Enter Confirm Password Same As Password');
			document.getElementById("confirmpasswordNewReg").focus;
			return false;
		}
	}
	function a() {
		var mo = $("#userName").val();
		$("#mobileNum2").val(mo);
	}

	function showFees() {
		var yearId = document.getElementById("globalAcademicYear").value;
		var standardId = document.getElementById("Standard").value;
		var schoolId = document.getElementById("schoolKey").innerHTML;
		var data = 'schoolId=' + schoolId + '&yearId=' + yearId
				+ '&standardId=' + standardId
		$
				.ajax({
					type : "GET",
					url : '/getFeesForYearAndStandard',
					async : false,
					data : data,
					dataType : 'json',
					success : function(response) {
						if (response.statusCode == "200") {
							if (parseInt(response.responseData) > 0) {
								document.getElementById("feeMsg").innerHTML = "<b>You must pay "
										+ response.responseData
										+ " to get your admission confirmed.</b>";
								document.getElementById("feeSettingAvailable").value = '1'
							}

						}

					},
					error : function(xhr, status, error) {
					}
				});
	}
</script>

</head>
<body onload="getLetterHead(); getSmsBalance();hideAndSeek();"
	onkeypress="capLock(event)">
	<%
		if (session != null) {
			session.invalidate();
		}
	%>

	<div class="login-form" id="wrapper"
		style="margin-left: 450px; height: 450px;">
		<div id="generateOtp"
			style="width: 1000px; background-color: #f3f3f3; margin-top: -1px;">
			<table style="text-align: center; margin: 0 auto;">
				<tr>
					<td colspan="3">
						<h1>${online}</h1>
					</td>
				</tr>
				<tr>
					<td colspan="3" id="letterHead">
						<div class="header">
							<h1>
								<img style="border: 0px solid red;"
									src="<c:url value="/resources/img/whiteimage.jpg"/>"
									width="100%" height="110px" />
							</h1>
							<span><h3>Welcome to e-PRASHASAN</h3></span>
						</div>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
			</table>

			<div id="firstDiv" style="display: + none;">
				<table style="text-align: center; margin: 0 auto;">
					<tr>
						<td><span><b style="color: #678889;">${enterMobileNO}</b></span></td>
					</tr>
					<!--<td><span><b style="color: #678889;">New User Registration</b></span></td> -->
					<tr class="content">
						<td>
							<%
								String schoolKey = "" + request.getParameter("schoolkey");
							%> <span id="schoolKey" style="display: none;"><%=schoolKey%></span>
							<input type="text" tabindex="5"
							autocomplete="dummy-no-auto-complete-fix" class="input"
							name="mobileNum" id="mobileNum" onkeypress='validate(event)'
							maxlength="10" pattern="\d{10}" placeholder="Mobile No"
							style="width: 250px !important" />
						</td>
					</tr>
				</table>
				<br /> <br />
				<div class="footer" style="padding-bottom: 25px;">
					<table style="text-align: center; margin: 0 auto;">
						<tr>
							<td><input tabindex="3" style="width: 300px;" type="submit"
								value="${next}" class="button" onclick="generateOTP()" /></td>
						</tr>
					</table>
				</div>
			</div>


			<div id="loginDiv" style="display: none;">
				<table style="text-align: center; margin: 0 auto;">
					<tr>
						<td colspan="2"><span><b style="color: #678889;">
									${aleadyRegistered} </b></span></td>
					</tr>
					<!--<td><span><b style="color: #678889;">New User Registration</b></span></td> -->
					<tr>
						<td style="display: none;" colspan="2"><span id="schoolKey"
							style="display: none;"><%=schoolKey%></span> <input tabindex="1"
							type="text" autocomplete="dummy-no-auto-complete-fix"
							class="input" name="userName" id="userName" maxlength="10"
							pattern="\d{10}" placeholder="Mobile No" required="required"
							style="width: 250px !important" /></td>
					</tr>

					<tr class="content">
						<td colspan="2"><input tabindex="2"
							autocomplete="dummy-no-auto-complete-fix" type="password"
							class="input" name="password" id="password"
							placeholder="Password" required="required" /></td>
					</tr>
					<tr>
						<td style="text-align: center;"><br /> <a href="#"
							tabindex="4" onclick="a();" data-toggle="modal"
							data-target="#basicModal">Forgot Password</a></td>
						<td style="text-align: center;"><br /> <a href="#"
							tabindex="4" onclick="b();"> ${newnumber} </a></td>

					</tr>
				</table>
				<br /> <br />
				<div class="footer" style="padding-bottom: 25px;">
					<table style="text-align: center; margin: 0 auto;">
						<tr>
							<td><input tabindex="3" style="width: 300px;" type="submit"
								value="${login}" class="button" onclick="login()" /></td>
						</tr>

					</table>
				</div>
			</div>


			<div id="generateOtpDiv" style="display: none;">
				<table style="text-align: center; margin: 0 auto;">
					<tr>
						<td><span><b style="color: #678889;">
									${verifyotponmobile} </b></span></td>
					</tr>
					<!--<td><span><b style="color: #678889;">New User Registration</b></span></td> -->
					<tr class="content">
						<td style="display: none;"><span id="schoolKey"
							style="display: none;"><%=schoolKey%></span> <input type="text"
							tabindex="5" autocomplete="dummy-no-auto-complete-fix"
							class="input" name="mobileNum" id="mobileNum"
							onkeypress='validate(event)' maxlength="10" pattern="\d{10}"
							placeholder="Mobile No" style="width: 250px !important" /></td>
					</tr>

					<tr class="content">
						<td><input type="text" tabindex="7" class="input"
							name="otpnum" id="otpnum" onkeypress='validate(event)'
							placeholder="Enter OTP here" /></td>
					<tr>
						<td style="text-align: center;"><br /> <a href="#"
							tabindex="4" onclick="b();">${newnumber}</a></td>
					</tr>
				</table>
				<br /> <br />
				<div class="footer" style="padding-bottom: 25px;">
					<table style="text-align: center; margin: 0 auto;">
						<tr>
							<td><input tabindex="3" style="width: 300px;" type="submit"
								value="${verifyOTP}" class="button"
								onclick="return validateOtp()" /></td>
						</tr>
					</table>
				</div>
			</div>

		</div>

	</div>

	<!-- youTube frame  -->
	<div class="vid">
		<iframe width="1000" height="515"
			src="https://www.youtube.com/embed/ny436fOn4zc" frameborder="0"
			allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
			allowfullscreen></iframe>
	</div>
	<!-- youTube frame  -->



	<!--  Popup For Forgot Password -->
	<div class="modal fade" id="basicModal" tabindex="-1" role="dialog"
		aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #f3f3f3;">
					<table>
						<tr>
							<td style="width: 100%;"><h4 class="modal-title"
									id="myModalLabel">
									<span><b style="color: #678889;"><spring:message
												code="ForgotPassword" text="Forgot Password" /></b></span>
								</h4></td>
							<td><span style="float: right;"><button type="button"
										style="color: red;" class="close" data-dismiss="modal"
										aria-hidden="true" onclick="location.reload(true)">&times;</button></span></td>
						</tr>
					</table>
				</div>
				<div class="modal-body" style="background-color: #f3f3f3;">
					<table id=table1>
						<tr>
							<td><span><b style="color: #678889;"> ${userNmae}
								</b></span></td>
						</tr>
						<tr>
							<td><input type="text" name="username2" style="float: left;"
								id="mobileNum2" autocomplete="off" maxlength="10"
								onkeypress="return allowOnlyNumbers(event)"></td>
							<td><input type="button" class="btn btn-primary"
								style="width: 65px; height: 30px; color: white; background: #3f9db8; border: 1px solid rgba(256, 256, 256, 0.75); border: 1px solid #46b3d3; border-radius: 2px;"
								value="<spring:message code="Next" text="Next"  />"
								name="FirstPhase" onclick="getData();"></td>
						</tr>
					</table>
					<table id="table2">
						<tr>
							<td><span><b style="color: #678889;"> ${enterotp}
								</b></span></td>
							<td><input type=text name="otpnum2" id="otpnum2"
								autocomplete="off"></td>
							<td><input type="button" onclick="validateOtp2()"
								id="submit" class="btn btn-primary"
								style="width: 60px; height: 30px; color: white; background: #3f9db8; border: 1px solid rgba(256, 256, 256, 0.75); border: 1px solid #46b3d3; border-radius: 2px;"
								value="<spring:message code="Submit" text="Submit"  />">
							</td>
						</tr>
					</table>
					<table id="table3">
						<tr>
							<td><span><b style="padding: 12px; color: #678889;">${newpassword}</b></span></td>
							<td style="padding: 12px;"><input type="password"
								name=passwordBox1 id="passwordBox1" autocomplete="off"></td>
						</tr>
						<tr>
							<td><span><b style="padding: 12px; color: #678889;">
										${renewpassword} </b></span></td>
							<td style="padding: 12px;"><input type="password"
								name=passwordBox1 id="passwordBox3" autocomplete="off"></td>
						</tr>
					</table>
					<center>
						<input type="button"
							style="width: 105px; height: 40px; color: white; background: #3f9db8; border: 1px solid rgba(256, 256, 256, 0.75); border: 1px solid #46b3d3; border-radius: 2px;"
							onclick="updatePassword()" class="btn btn-primary" id="submit1"
							value="${submit}">
					</center>
				</div>
			</div>
		</div>
	</div>

	<!--  Registration Form -->
	<div id="regForm" class="regForm" style="display: none;">
		<table id="newRegTable" class="regForm-tab">
			<tr style="">
				<td colspan="2"><h2 class="modal-title"
						style="text-align: center; padding-bottom: 30px;">
						<span><b>${succssfulotp}</b></span>
					</h2></td>
			</tr>

			<tr>
				<td style="width: 50%; vertical-align: top;"><span><b
						style="color:;">${acadenicyear}</b></span></td>
				<td><select id="globalAcademicYear"
					class="AcademicYear input input-select" onchange="showFees()">
						<!-- <option value="">Select Year</option> -->
				</select></td>
			</tr>
			<tr>
				<td style="width: 50%; vertical-align: top;"><span><b
						style="color:;">${standard}</b></span></td>
				<td><select id="Standard" class="Standard input input-select"
					onchange="showFees()">
						<option value="">${selectstandard}</option>
				</select> <br /></td>
			</tr>
			<tr>
				<td><span><b style="color:;">${initialname}</b></span></td>
				<td><select class="input input-select" name="missMr"
					id="missMr"><option value="0">${selectinitialname}</option></select></td>
			</tr>
			<tr>
				<td><span><b style="color:;">${firstname}</b></span></td>
				<td><input type="text"
					autocomplete="dummy-no-auto-complete-fix" class="input newRegName"
					name="newRegName" id="newRegName"
					style="text-transform: capitalize;"> <!-- <input type="text" tabindex="5" autocomplete="dummy-no-auto-complete-fix" class="input" name="mobileNum" id="mobileNum" maxlength="10" placeholder="First Name"> -->
				</td>
			</tr>
			<tr>
				<td><span><b style="color:;">${secondname}</b></span></td>
				<td><input type="text"
					autocomplete="dummy-no-auto-complete-fix" class="input"
					name="secondName" id="secondName"
					style="text-transform: capitalize;"></td>
			</tr>
			<tr>
				<td><span><b style="color:;">${lastname}</b></span></td>
				<td><input type="text"
					autocomplete="dummy-no-auto-complete-fix" class="input"
					name="lastName" id="lastName" style="text-transform: capitalize;"></td>
			</tr>
			<tr>
				<td><span><b style="color:;">${username}</b></span></td>
				<td><input type="text" name="newRegUserName"
					id="newRegUserName" class="input" disabled="disabled"></td>
			</tr>
			<tr>
				<td><span><b style="color:;">${password}</b></span></td>
				<td><input size="5" class="input" type="password"
					name="passwordNewReg" id="passwordNewReg" /></td>
			</tr>
			<tr>
				<td><span><b style="color:;">${confirmpassword}</b></span></td>
				<td><input size="5" class="input" type="password"
					name="confirmpasswordNewReg" id="confirmpasswordNewReg" /></td>
			</tr>


			<tr>
				<td colspan="2" align="center"><span id="feeMsg"
					style="color: red;"></span><input type="hidden"
					id="feeSettingAvailable" value='0'></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><center>
						<input type="button" class="submitBtn"
							style="margin-top: 10px; width: 125px; height: 40px; color: white; background: #3f9db8; border: 1px solid rgba(256, 256, 256, 0.75); border: 1px solid #46b3d3; border-radius: 2px;"
							value="Submit" id="registrationbutton"
							onClick="return newPayment();" />
					</center></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><center>
						<input type="button" class="submitBtn"
							style="margin-top: 10px; width: 125px; height: 40px; color: white; background: #3f9db8; border: 1px solid rgba(256, 256, 256, 0.75); border: 1px solid #46b3d3; border-radius: 2px;"
							value="Submit" id="newregistration" onClick="return validatePassword();" />
					</center></td>
			</tr>
			<tr>
				<td colspan="2" id="loading" align="center"><img id="spinner"
					style="background-color: transparent; width: 100px; height: 100px; margin-top: 10px; display: none;"
					src="resources/img/img.gif" /></td>
			</tr>
		</table>
	</div>
	<!--  Registration Form -->


	<!--  Popup For Forgot Password -->
	<%--  <a href="#" data-toggle="modal" id="newRegistrationPopUp" data-target="#basicModal2" class="" style="padding:0px !important;  margin-right: 0px !important; display: "></a>
  <div class="modal fade" id="basicModal2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	  <div class="modal-dialog">
		  <div class="modal-content" style="width: 405px;">
		  <div class="modal-header" style="background-color: #f3f3f3;">
		  	<table>
				<tr>
					<td style="width: 100%;"><h4 class="modal-title" id="myModalLabel"><span><b style="color: #678889;"><span><b>${succssfulotp}</b></span></h2></td>
					<!-- <td></td><td></td><td></td><td></td><td></td><td></td> -->
					<td><span style="text-align: right;"><button type="button" style="color: red;" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.reload(true)">&times;</button></span></td>
				</tr>
			</table>
	  </div>   
  <div class="modal-body" style="background-color: #f3f3f3;">
  <table id="newRegTable">
  
   <tr>
		<td style="width: 50%; vertical-align: top;"><span><b style="color: #678889;">${acadenicyear}</b></span></td>
		<td><select id="globalAcademicYear" class="AcademicYear" style="margin-right: 15px;width: 174px;height: 24px;">
			<!-- <option value="">Select Year</option> -->
		</select>
		</td>
	</tr>	
	<tr>	
		<td style="width: 50%; vertical-align: top;"><span><b style="color: #678889;">${standard}</b></span></td>
		<td><select id="Standard" class="Standard" style="margin-right: 15px;width: 174px;height: 24px;">
				<option value="">${selectstandard}</option>
			</select>
			<br/>
		</td>
	</tr>
    <tr>
	  	<td><span><b style="color: #678889;">${initialname}</b></span></td>
		<td><select style="width: 173px;height: 25px;" class="input" name="missMr" id="missMr" ><option value="0">Select Initial Name</option></select></td>
	</tr>
  	<tr>
	  	<td><span><b style="color: #678889;">${firstname}</b></span></td>
		<td><input type="text" autocomplete="dummy-no-auto-complete-fix" class="input newRegName" name="newRegName" id="newRegName" style="text-transform: capitalize;"></td>
	</tr>
	<tr>
	  	<td><span><b style="color: #678889;">${secondname}</b></span></td>
		<td><input type="text" autocomplete="dummy-no-auto-complete-fix" class="input" name="secondName" id="secondName" style="text-transform: capitalize;"></td>
	</tr>
	<tr>
	  	<td><span><b style="color: #678889;">${lastname}</b></span></td>
		<td><input type="text" autocomplete="dummy-no-auto-complete-fix" class="input" name="lastName" id="lastName" style="text-transform: capitalize;"></td>
	</tr>
	<tr>
		<td><span><b style="color: #678889;">${username}</b></span></td>
		<td><input type="text" name="newRegUserName" id="newRegUserName" disabled="disabled"></td>
	</tr>
	<tr>
		<td><span><b style="color: #678889;">${password}</b></span></td>
        <td><input size="5" class="input" style="width: 170px;" type="password" name="passwordNewReg" id="passwordNewReg" /></td>
    </tr>
    <tr>
    	<td><span><b style="color: #678889;">${confirmpassword}</b></span></td>
        <td><input size="5" class="input" style="width: 170px;" type="password" name="confirmpasswordNewReg" id="confirmpasswordNewReg" /></td>
    </tr>
   
	<tr></tr>																				
    <tr>
        <td colspan="2" align="center"><center><input type="button" style="margin-top: 10px;width: 125px;height: 40px;color: white;background: #3f9db8;border: 1px solid rgba(256,256,256,0.75);border: 1px solid #46b3d3;border-radius: 2px;" value="New Registartion" onClick="return validatePassword();" /></center></td>
    </tr>
  </table>
</div>
</div>
</div>
</div> --%>
	<!--  Popup For Forgot Password -->



</body>
</html>
